﻿<cfprocessingdirective suppresswhitespace=true pageEncoding="UTF-8">
<cfsetting enableCFoutputOnly = "Yes" showdebugoutput="yes">
<cftry>
<cfparam name="x_nm" default="">
<cfparam name="form.resort_code" default="">
<cfparam name="form.login_id" default="">

<cfset dt_check = "datacheck">

<cffile action="read" file="#expandpath("xml")#\datasource.xml" variable="XMLFileText">
<cfset xmlfile=XmlParse(XMLFileText)>
<!---XSS対策--->
	<cfinvoke component="#dt_check#" method="xss_check" returnVariable="x_rcode">
		<cfinvokeargument name="ch_str" value="#form.resort_code#">
	</cfinvoke>
	<cfinvoke component="#dt_check#" method="xss_check" returnVariable="x_id">
		<cfinvokeargument name="ch_str" value="#form.login_id#">
	</cfinvoke>
	<cfinvoke component="#dt_check#" method="xss_check" returnVariable="x_pw">
		<cfinvokeargument name="ch_str" value="#form.login_pw#">
	</cfinvoke>
	<cfinvoke component="#dt_check#" method="xss_check" returnVariable="x_nm">
		<cfinvokeargument name="ch_str" value="#form.login_nm#">
	</cfinvoke>
	
	<cfinvoke component="#dt_check#" method="xss_check" returnVariable="x_div">
		<cfinvokeargument name="ch_str" value="#form.login_div#">
	</cfinvoke>
	
	<cfinvoke component="#dt_check#" method="xss_check" returnVariable="x_rdiv">
		<cfinvokeargument name="ch_str" value="#form.regist_div#">
	</cfinvoke>

<!---POSTデータの有無をチェック--->
<cfif Isdefined("form")>
	<!---POSTされてきた施設IDが数値かどうかチェック--->
	<cfif isnumeric(Trim(x_rcode)) neq true and len(Trim(x_rcode)) neq 0>
		<cfset error_msg = "施設IDが数値ではありません。">
		<cfthrow message = "#error_msg#" detail = "#error_msg#">
	</cfif>

	<!---POSTされてきたログインIDが空かどうかチェック--->
	<cfif len(Trim(x_id)) eq 0>
		<cfset error_msg = "ログインIDが空です。">
		<cfthrow message = "#error_msg#" detail = "#error_msg#">
	</cfif>
	<!---POSTされてきたログインPWが空かどうかチェック--->
	<cfif len(Trim(x_pw)) eq 0 and x_rdiv neq 2>
		<cfset error_msg = "ログインPWが空です。">
		<cfthrow message = "#error_msg#" detail = "#error_msg#">
	</cfif>
	
	
	<!---POSTされてきたログイン名が空かどうかチェック（新規登録時のみ）--->
	<cfif x_rdiv eq 0 and len(Trim(x_nm)) eq 0>
		<cfset error_msg = "ログイン名が空です。">
		<cfthrow message = "#error_msg#" detail = "#error_msg#">
	</cfif>
	<!---POSTされてきた権限区分が空かどうかチェック（新規登録時のみ）--->
	<cfif x_rdiv eq 0 and len(Trim(x_div)) eq 0 >
		<cfset error_msg = "権限区分が空です。">
		<cfthrow message = "#error_msg#" detail = "#error_msg#">
	</cfif>
	
	<!---POSTされてきた登録区分が数値かどうかチェック--->
	<cfif isnumeric(Trim(x_rdiv)) neq true and len(Trim(x_rdiv)) neq 0>
		<cfset error_msg = "登録区分が数値ではありません。">
		<cfthrow message = "#error_msg#" detail = "#error_msg#">
	</cfif>
	
	<!---POSTされてきたid,pwをJAVA正規表現でチェック--->

	<cfset c_id = x_id>
	<cfset a_id = c_id.matches("[0-9a-zA-Z]+")>
	

	<!---id,pwは1～12文字以内で無ければならない。--->
	<cfif len(x_id) gte 12>
		<cfset error_msg = "IDの入力可能文字数がオーバーしてます。">
		<cfthrow message = "#error_msg#" detail = "#error_msg#">
	</cfif>
	
	
	<cfif len(x_id) eq 0>
		<cfset error_msg = "IDの入力がありません。">
		<cfthrow message = "#error_msg#" detail = "#error_msg#">
	</cfif>
	
	<cfif x_rdiv neq 2>
		<cfset c_pw = x_pw>
		<cfset a_pw = c_pw.matches("[0-9a-zA-Z]+")>
		<cfif a_pw neq true>
			<cfset error_msg = "PWは半角で入力してください。">
			<cfthrow message = "#error_msg#" detail = "#error_msg#">
		</cfif>
	
	
		<cfif a_id neq true>
			<cfset error_msg = "IDは半角で入力してください。">
			<cfthrow message = "#error_msg#" detail = "#error_msg#">
		</cfif>
		
		<cfif len(x_pw) gte 12>
			<cfset error_msg = "PWの入力可能文字数がオーバーしてます。">
			<cfthrow message = "#error_msg#" detail = "#error_msg#">
		</cfif>
	
		<cfif len(x_pw) eq 0>
			<cfset error_msg = "PWの入力がありません。">
			<cfthrow message = "#error_msg#" detail = "#error_msg#">
		</cfif>
	</cfif>
	
	
	<!---javaを使い文字列長（byte）をチェック--->
	<cfif x_rdiv eq 0>
		<cfset l_nm = javacast("String","#x_nm#")>
		<cfset l_nm = l_nm.getBytes()/>
		<cfset nm_len = len(#l_nm#)>
		<cfif nm_len gt 48>
			<cfset error_msg = "ログイン名の可能入力文字数がオーバーしてます。">
			<cfthrow message = "#error_msg#" detail = "#error_msg#">
		</cfif>
	</cfif>
	
	<!---施設IDの値がXMLに存在するかチェック--->
	<cfinvoke component="#dt_check#" method="check_resort" returnVariable="rs">
		<cfinvokeargument name="resort_code" value="#x_rcode#">
	</cfinvoke>
	
	<cfif rs eq 1>
		<cfset error_msg = "施設IDの値が不正です。">
		<cfthrow message = "#error_msg#" detail = "#error_msg#">
	</cfif>

	
	<!---権限の値がその施設に存在するかチェック--->
	<cfif x_rdiv eq 0>
		<cfinvoke component="#dt_check#" method="check_div" returnVariable="rs3">
			<cfinvokeargument name="resort_code" value="#x_rcode#">
			<cfinvokeargument name="login_div" value="#x_div#">
		</cfinvoke>
		
		<cfif rs3 eq 1>
			<cfset error_msg = "権限の値が不正です。">
			<cfthrow message = "#error_msg#" detail = "#error_msg#">
		</cfif>
	</cfif>
	
	<cfset chk_msg2 = "">
	<cfif x_rcode eq 0 or x_rcode eq 999>
		<cfinvoke component="#dt_check#" method="cnt_data" returnVariable="cnt_check">
		<cfinvokeargument name="resort_code" value="#x_rcode#">
		<cfinvokeargument name="login_id" value="#x_id#">
		<cfinvokeargument name="login_pw" value="#x_pw#">
		<cfinvokeargument name="login_nm" value="#x_nm#">
		<cfinvokeargument name="login_div" value="#x_div#">
		</cfinvoke>
		
		<cfif cnt_check.rs eq 1>
			<cfset error_msg = "重複登録の確認処理に失敗しました。| #cnt_check.er_msg#">
			<cfthrow message = "#error_msg#" detail = "#error_msg#">
		<cfelse>
			<cfset chk_msg2 = "#cnt_check.chk_msg#">
		</cfif>
	</cfif>		

	<cfif x_rcode neq 0 and x_rcode neq 999>
		<cfinvoke component="#dt_check#" method="cnt_data2" returnVariable="cnt_check2">
		<cfinvokeargument name="resort_code" value="#x_rcode#">
		<cfinvokeargument name="login_id" value="#x_id#">
		<cfinvokeargument name="login_pw" value="#x_pw#">
		<cfinvokeargument name="login_nm" value="#x_nm#">
		<cfinvokeargument name="login_div" value="#x_div#">
		</cfinvoke>
		
		<cfif cnt_check2.rs eq 1>
			<cfset error_msg = "#cnt_check2.er_msg#">
			<cfthrow message = "#error_msg#" detail = "#error_msg#">
		</cfif>
	</cfif>		

	<!---データの新規、更新、削除処理へ--->
	<cfinvoke component="#dt_check#" method="check_data" returnVariable="r_message">
		<cfinvokeargument name="resort_code" value="#x_rcode#">
		<cfinvokeargument name="login_id" value="#x_id#">
		<cfinvokeargument name="login_pw" value="#x_pw#">
		<cfinvokeargument name="login_nm" value="#x_nm#">
		<cfinvokeargument name="login_div" value="#x_div#">
		<cfinvokeargument name="regist_div" value="#x_rdiv#">
	</cfinvoke>
	
	
	<!---施設IDから施設名を取得する--->
	<cfinvoke component="#dt_check#" method="getresort" returnVariable="r_name1">
		<cfinvokeargument name="resort_code" value="#x_rcode#">
	</cfinvoke>	


	<cfif r_message.rs eq 1>
		<cfset error_msg = r_message.er_msg>
		<cfthrow message = "#error_msg#" detail = "#error_msg#">
	<cfelse>
		<cfset returnmark = #chr(10)# & #chr(13)#>
		<!---成功メール処理--->
		<cfset mail_body = "">
		<cfset subj = "">
		<cfif x_rdiv eq 0>
			<cfset mail_body = "下記、登録処理が完了いたしました"&returnmark>
			<cfset mail_body 
				= mail_body&"【登録施設】#r_name1#へ登録"&returnmark>
			<cfset subj = "アカウント登録処理完了のご連絡">
		<cfelseif x_rdiv eq 1>
			<cfset mail_body = "下記、変更処理が完了いたしました"&returnmark>
			<cfset mail_body 
				= mail_body&"【PassWord】の変更完了"&returnmark>
			<cfset subj = "変更処理完了のご連絡">
		<cfelseif x_rdiv eq 2>
			<cfset mail_body = "下記、削除処理が完了いたしました"&returnmark>
			<cfset mail_body 
				= mail_body&"【削除施設】#r_name1#"&returnmark>
			<cfset subj = "アカウント削除処理完了のご連絡">
		</cfif>
		
		<cfset mail_body = mail_body&"【ID】#x_id#"&returnmark>
		<cfif x_rdiv eq 0>
			<cfset mail_body = mail_body&"【ログイン名】#x_nm#"&returnmark>
		</cfif>
		
		<cfset mail_body = mail_body&returnmark&"#chk_msg2#"&returnmark>


		<cfmail to="#xmlfile.datasource.maildata.to_add.xmltext#"
			from="#xmlfile.datasource.maildata.from_add.xmltext#"
			bcc="#xmlfile.datasource.maildata.bcc_add.xmltext#"
			subject="#subj#"
			type="#xmlfile.datasource.maildata.type.xmltext#"
			server="#xmlfile.datasource.maildata.smtp.xmltext#"
			port="#xmlfile.datasource.maildata.port.xmltext#"
			charset="#xmlfile.datasource.maildata.char.xmltext#"
			password="#xmlfile.datasource.maildata.password.xmltext#"
			username="#xmlfile.datasource.maildata.username.xmltext#">#mail_body#</cfmail>

	</cfif>

</cfif>


<!---成功　Success表示--->
<cfif r_message.rs eq 0>
	<cfoutput><html><body>SUCCESS</body></html></cfoutput>
</cfif>


<cfcatch type="any">
<cfdump var=#cfcatch#>
<cfabort>
	<!---施設IDから施設名を取得する--->
	<cfinvoke component="#dt_check#" method="getresort" returnVariable="r_name1">
		<cfinvokeargument name="resort_code" value="#x_rcode#">
	</cfinvoke>	

	<cfset returnmark = #chr(10)# & #chr(13)#>
	<!---失敗メール処理--->
	<cfset mail_body = "処理中にエラーが発生しました"&returnmark>
	<cfset mail_body = mail_body&"【施設】#r_name1#"&returnmark>
	<cfset mail_body = mail_body&"【ID】#x_id#"&returnmark>
	<cfset mail_body = mail_body&"【ログイン名】#x_nm#"&returnmark>
	<cfset mail_body = mail_body&"【エラー内容】#cfcatch.detail#"&returnmark>

	<cfmail to="#xmlfile.datasource.maildata.bcc_add.xmltext#"
		from="#xmlfile.datasource.maildata.from_add.xmltext#"
		bcc="#xmlfile.datasource.maildata.bcc_add.xmltext#"
		subject="処理中にエラーが発生しました。"
		type="#xmlfile.datasource.maildata.type.xmltext#"
		server="#xmlfile.datasource.maildata.smtp.xmltext#"
		port="#xmlfile.datasource.maildata.port.xmltext#"
		charset="#xmlfile.datasource.maildata.char.xmltext#"
		password="#xmlfile.datasource.maildata.password.xmltext#"
		username="#xmlfile.datasource.maildata.username.xmltext#">#mail_body#</cfmail>
		<cfif len(form.resort_code) neq 0 and len(form.login_id) neq 0>
			<cfoutput><html><body>ERROR:#form.resort_code#:#form.login_id#:#cfcatch.detail#</body></html></cfoutput>
		<cfelse>
			<cfoutput><html><body>ERROR: : :POSTデータがありません。</body></html></cfoutput>
		</cfif>
</cfcatch>
</cftry>
</cfprocessingdirective>