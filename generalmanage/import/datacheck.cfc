﻿<!---
09.04.15-16
	UTF-8へファイルを変更
	altswinterへの登録方法（別サーバへの登録）変更。
	各登録・削除のデータソースを統一、sqlをDB名.スキーマ名.テーブル名へ変更
	エラーへ番号と詳細を記載
	登録区分の不正な場合を追加
--->
<cfcomponent access="public">
<!---データをチェックし、各処理へ振り分ける関数--->
<cffunction name="check_data" returntype="any">
	<cfargument name="resort_code">
	<cfargument name="login_id">
	<cfargument name="login_pw">
	<cfargument name="login_nm">
	<cfargument name="login_div">
	<cfargument name="regist_div">

	<cfset r_message = StructNew()>

	<cftry>

		<!--- 新規登録処理 regist_div＝０ --->
		<cfif arguments.regist_div eq 0>
			<cfinvoke method="regist_data" returnVariable="s_check">
				<cfinvokeargument name="resort_code" value="#arguments.resort_code#">
				<cfinvokeargument name="login_id" value="#arguments.login_id#">
				<cfinvokeargument name="login_pw" value="#arguments.login_pw#">
				<cfinvokeargument name="login_nm" value="#arguments.login_nm#">
				<cfinvokeargument name="login_div" value="#arguments.login_div#">
			</cfinvoke>

			<cfif s_check.rs eq 1>
				<cfset error_msg = "sql01-登録処理に失敗しました。| #s_check.er_msg#">
				<cfthrow message = "#error_msg#" detail = "#error_msg#">
			</cfif>
		<!---<cfelseif arguments.regist_div eq 1>
			<cfinvoke method="update_data" returnVariable="s_check">
					<cfinvokeargument name="resort_code" value="#arguments.resort_code#">
					<cfinvokeargument name="login_id" value="#arguments.login_id#">
					<cfinvokeargument name="login_pw" value="#arguments.login_pw#">
			</cfinvoke>

			<cfif s_check.rs eq 1>
				<cfset error_msg = "変更処理が失敗しました。">
				<cfthrow message = "#error_msg#" detail = "#error_msg#">
			</cfif>--->

		<!--- 削除処理 regist_div＝２ --->
		<cfelseif arguments.regist_div eq 2>
			<cfinvoke method="delete_data" returnVariable="s_check">
				<cfinvokeargument name="resort_code" value="#arguments.resort_code#">
				<cfinvokeargument name="login_id" value="#arguments.login_id#">
				<cfinvokeargument name="login_pw" value="#arguments.login_pw#">
			</cfinvoke>

			<cfif s_check.rs eq 1>
				<cfset error_msg = "sql02-削除処理が失敗しました。| #s_check.er_msg#">
				<cfthrow message = "#error_msg#" detail = "#error_msg#">
			</cfif>
		<cfelse>
			<cfset error_msg = "sql03-登録区分が不正なため登録・削除をできません。">
			<cfthrow message = "#error_msg#" detail = "#error_msg#">
		</cfif>

		<cfcatch>
			<cfset rs = 1>
			<cfset structinsert(r_message, "rs", rs)>
			<cfset structinsert(r_message, "er_msg", cfcatch.detail)>

			<cfreturn r_message>
		</cfcatch>
	</cftry>
		<cfset rs = 0>
		<cfset structinsert(r_message, "rs", rs)>

		<cfreturn r_message>
</cffunction>


<!---新規登録処理--->
<cffunction name="regist_data" returntype="any">
	<cfargument name="resort_code">
	<cfargument name="login_id">
	<cfargument name="login_pw">
	<cfargument name="login_nm">
	<cfargument name="login_div">

	<cfset s_check = StructNew()>

	<cffile action="read" file="#expandpath("xml")#\datasource.xml" variable="XMLFileText">
	<cfset xmlfile = XmlParse(XMLFileText)>

	<cftry>

		<!---CRMKitchen(ALL権限：全てに登録)設定--->
		<!--- <cfif arguments.resort_code eq 999> --->
		<cfif arguments.resort_code eq 999 or arguments.resort_code eq 0>

			<!---crmpublic--->
			<!--- 2010/10/02 統合版で全権限の場合は全レコード削除する --->
			<!--- <cfquery datasource="#xmlfile.datasource.crmpublic.XmlAttributes.dsn#" name="delDoubleAccount">
				delete
				from	 #xmlfile.datasource.crmpublic.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			</cfquery> --->

			<cfquery datasource="#xmlfile.datasource.crmpublic.XmlAttributes.dsn#" name="query1">
				insert into	#xmlfile.datasource.crmpublic.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser (
					login_id,
					resort_code,
					login_pw,
					login_nm,
					login_dv
				) values (
					<cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">,
					0,
					<cfqueryparam value="#arguments.login_pw#" cfsqltype="CF_SQL_VARCHAR" maxlength="50">,
					<cfqueryparam value="#arguments.login_nm#" cfsqltype="CF_SQL_VARCHAR" maxlength="128">,
					<cfqueryparam value="#arguments.login_div#" cfsqltype="CF_SQL_VARCHAR" maxlength="2">
					)
			</cfquery>

			<!---alts--->
			<!---
			<cfquery datasource="#xmlfile.datasource.alts.XmlAttributes.dsn#" name="getAccount">
				select	 count(*) as cnt
				from	 #xmlfile.datasource.alts.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			</cfquery>
			<cfif getAccount.cnt neq 0>
				<cfquery datasource="#xmlfile.datasource.alts.XmlAttributes.dsn#" name="delAccount">
					delete
					from	 #xmlfile.datasource.alts.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
					where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
				</cfquery>
			</cfif>
			<cfquery datasource="#xmlfile.datasource.alts.XmlAttributes.dsn#" name="query2">
				insert into #xmlfile.datasource.alts.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser (
					login_id,
					login_pw,
					login_nm,
					login_dv
				) values (
					<cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">,
					<cfqueryparam value="#arguments.login_pw#" cfsqltype="CF_SQL_VARCHAR" maxlength="50">,
					<cfqueryparam value="#arguments.login_nm#" cfsqltype="CF_SQL_VARCHAR" maxlength="128">,
					's'
					)
			</cfquery>
			 --->

			<!---bandai(RESORTPUBLIC)--->
			<!--- 2010/10/02 統合版で全権限の場合は全レコード削除する --->
			<!--- <cfquery datasource="#xmlfile.datasource.bandai.XmlAttributes.dsn#" name="delDoubleAccount">
				delete
				from	 #xmlfile.datasource.bandai.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			</cfquery> --->

			<cfquery datasource="#xmlfile.datasource.bandai.XmlAttributes.dsn#" name="query2">
				insert into #xmlfile.datasource.bandai.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser (
					login_id,
					resort_code,
					login_pw,
					login_nm,
					login_dv
				) values (
					<cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">,
					0,
					<cfqueryparam value="#arguments.login_pw#" cfsqltype="CF_SQL_VARCHAR" maxlength="50">,
					<cfqueryparam value="#arguments.login_nm#" cfsqltype="CF_SQL_VARCHAR" maxlength="128">,
					<cfqueryparam value="#arguments.login_div#" cfsqltype="CF_SQL_VARCHAR" maxlength="2">
					)
			</cfquery>

			<!---hnypublic--->
			<!--- 2010/10/02 統合版で全権限の場合は全レコード削除する --->
			<cfquery datasource="#xmlfile.datasource.hnypublic.XmlAttributes.dsn#" name="delAccount">
				delete
				from	 #xmlfile.datasource.hnypublic.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			</cfquery>

			<cfquery datasource="#xmlfile.datasource.hnypublic.XmlAttributes.dsn#" name="query2">
				insert into #xmlfile.datasource.hnypublic.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser (
					login_id,
					resort_code,
					login_pw,
					login_nm,
					login_dv
				) values (
					<cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">,
					0,
					<cfqueryparam value="#arguments.login_pw#" cfsqltype="CF_SQL_VARCHAR" maxlength="50">,
					<cfqueryparam value="#arguments.login_nm#" cfsqltype="CF_SQL_VARCHAR" maxlength="128">,
					<cfqueryparam value="#arguments.login_div#" cfsqltype="CF_SQL_VARCHAR" maxlength="2">
					)
			</cfquery>

			<!---hbc--->
			<cfquery datasource="#xmlfile.datasource.hbc.XmlAttributes.dsn#" name="getAccount">
				select	 count(*) as cnt
				from	 #xmlfile.datasource.hbc.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			</cfquery>
			<cfif getAccount.cnt neq 0>
				<!--- <cfquery datasource="#xmlfile.datasource.hbc.XmlAttributes.dsn#" name="delAccount">
					delete
					from	 #xmlfile.datasource.hbc.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
					where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
				</cfquery> --->
			</cfif>
			<cfquery datasource="#xmlfile.datasource.hbc.XmlAttributes.dsn#" name="query4">
				insert into #xmlfile.datasource.hbc.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser (
					login_id,
					login_pw,
					login_nm,
					login_dv
				) values (
					<cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">,
					<cfqueryparam value="#arguments.login_pw#" cfsqltype="CF_SQL_VARCHAR" maxlength="50">,
					<cfqueryparam value="#arguments.login_nm#" cfsqltype="CF_SQL_VARCHAR" maxlength="128">,
					<cfqueryparam value="#arguments.login_div#" cfsqltype="CF_SQL_VARCHAR" maxlength="2">
					)
			</cfquery>

			<!--- hbc ⇔ crmwedding --->
			<cfquery datasource="#xmlfile.datasource.crmwedding.XmlAttributes.dsn#" name="getAccount">
				select	 count(*) as cnt
				from	 #xmlfile.datasource.crmwedding.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			</cfquery>
			<cfif getAccount.cnt neq 0>
				<!--- <cfquery datasource="#xmlfile.datasource.crmwedding.XmlAttributes.dsn#" name="delAccount">
					delete
					from	 #xmlfile.datasource.crmwedding.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
					where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
				</cfquery> --->
			</cfif>
			<cfquery datasource="#xmlfile.datasource.crmwedding.XmlAttributes.dsn#" name="query4">
				insert into #xmlfile.datasource.crmwedding.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser (
					login_id,
					login_pw,
					login_nm,
					login_dv
				) values (
					<cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">,
					<cfqueryparam value="#arguments.login_pw#" cfsqltype="CF_SQL_VARCHAR" maxlength="50">,
					<cfqueryparam value="#arguments.login_nm#" cfsqltype="CF_SQL_VARCHAR" maxlength="128">,
					<cfqueryparam value="#arguments.login_div#" cfsqltype="CF_SQL_VARCHAR" maxlength="2">
					)
			</cfquery>

			<!---rnr--->
			<cfquery datasource="#xmlfile.datasource.rnr.XmlAttributes.dsn#" name="getAccount">
				select	 count(*) as cnt
				from	 #xmlfile.datasource.rnr.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			</cfquery>
			<cfif getAccount.cnt neq 0>
				<!--- <cfquery datasource="#xmlfile.datasource.rnr.XmlAttributes.dsn#" name="delAccount">
					delete
					from	 #xmlfile.datasource.rnr.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
					where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
				</cfquery> --->
			</cfif>
			<cfquery datasource="#xmlfile.datasource.rnr.XmlAttributes.dsn#" name="query5">
				insert into #xmlfile.datasource.rnr.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser (
					login_id,
					login_pw,
					login_nm,
					login_dv
				) values (
					<cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">,
					<cfqueryparam value="#arguments.login_pw#" cfsqltype="CF_SQL_VARCHAR" maxlength="50">,
					<cfqueryparam value="#arguments.login_nm#" cfsqltype="CF_SQL_VARCHAR" maxlength="128">,
					<cfqueryparam value="#arguments.login_div#" cfsqltype="CF_SQL_VARCHAR" maxlength="2">
					)
			</cfquery>

		<!--- HBC設定 --->
		<cfelseif arguments.resort_code eq 16>
			<cfquery datasource="#xmlfile.datasource.hbc.XmlAttributes.dsn#" name="getAccount">
				select	 count(*) as cnt
				from	 #xmlfile.datasource.hbc.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			</cfquery>
			<cfif getAccount.cnt neq 0>
				<cfquery datasource="#xmlfile.datasource.hbc.XmlAttributes.dsn#" name="delAccount">
					delete
					from	 #xmlfile.datasource.hbc.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
					where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
				</cfquery>
			</cfif>
			<cfquery datasource="#xmlfile.datasource.hbc.XmlAttributes.dsn#" name="query1">
				insert into #xmlfile.datasource.hbc.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser (
					login_id,
					login_pw,
					login_nm,
					login_dv
				) values (
					<cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">,
					<cfqueryparam value="#arguments.login_pw#" cfsqltype="CF_SQL_VARCHAR" maxlength="50">,
					<cfqueryparam value="#arguments.login_nm#" cfsqltype="CF_SQL_VARCHAR" maxlength="128">,
					<cfqueryparam value="#arguments.login_div#" cfsqltype="CF_SQL_VARCHAR" maxlength="2">
					)
			</cfquery>

			<!--- hbc ⇔ crmwedding --->
			<cfquery datasource="#xmlfile.datasource.crmwedding.XmlAttributes.dsn#" name="getAccount">
				select	 count(*) as cnt
				from	 #xmlfile.datasource.crmwedding.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			</cfquery>
			<cfif getAccount.cnt neq 0>
				<cfquery datasource="#xmlfile.datasource.crmwedding.XmlAttributes.dsn#" name="delAccount">
					delete
					from	 #xmlfile.datasource.crmwedding.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
					where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
				</cfquery>
			</cfif>
			<cfquery datasource="#xmlfile.datasource.crmwedding.XmlAttributes.dsn#" name="query1">
				insert into #xmlfile.datasource.crmwedding.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser (
					login_id,
					login_pw,
					login_nm,
					login_dv
				) values (
					<cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">,
					<cfqueryparam value="#arguments.login_pw#" cfsqltype="CF_SQL_VARCHAR" maxlength="50">,
					<cfqueryparam value="#arguments.login_nm#" cfsqltype="CF_SQL_VARCHAR" maxlength="128">,
					<cfqueryparam value="#arguments.login_div#" cfsqltype="CF_SQL_VARCHAR" maxlength="2">
					)
			</cfquery>


		<!--- RNR設定 --->
		<cfelseif arguments.resort_code eq 17>
			<cfquery datasource="#xmlfile.datasource.rnr.XmlAttributes.dsn#" name="getAccount">
				select	 count(*) as cnt
				from	 #xmlfile.datasource.rnr.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			</cfquery>
			<cfif getAccount.cnt neq 0>
				<cfquery datasource="#xmlfile.datasource.rnr.XmlAttributes.dsn#" name="delAccount">
					delete
					from	 #xmlfile.datasource.rnr.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
					where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
				</cfquery>
			</cfif>
			<cfquery datasource="#xmlfile.datasource.rnr.XmlAttributes.dsn#" name="query1">
				insert into #xmlfile.datasource.rnr.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser (
					login_id,
					login_pw,
					login_nm,
					login_dv
				) values (
					<cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">,
					<cfqueryparam value="#arguments.login_pw#" cfsqltype="CF_SQL_VARCHAR" maxlength="50">,
					<cfqueryparam value="#arguments.login_nm#" cfsqltype="CF_SQL_VARCHAR" maxlength="128">,
					<cfqueryparam value="#arguments.login_div#" cfsqltype="CF_SQL_VARCHAR" maxlength="2">
					)
			</cfquery>

		<!--- ALTS設定 --->
		<cfelseif arguments.resort_code eq 18>
			<!---
			<cfquery datasource="#xmlfile.datasource.alts.XmlAttributes.dsn#" name="getAccount">
				select	 count(*) as cnt
				from	 #xmlfile.datasource.alts.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			</cfquery>
			<cfif getAccount.cnt neq 0>
				<cfquery datasource="#xmlfile.datasource.alts.XmlAttributes.dsn#" name="delAccount">
					delete
					from	#xmlfile.datasource.alts.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
					where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
				</cfquery>
			</cfif>
			<cfquery datasource="#xmlfile.datasource.alts.XmlAttributes.dsn#" name="query1">
				insert into #xmlfile.datasource.alts.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser (
					login_id,
					login_pw,
					login_nm,
					login_dv
				) values (
					<cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">,
					<cfqueryparam value="#arguments.login_pw#" cfsqltype="CF_SQL_VARCHAR" maxlength="50">,
					<cfqueryparam value="#arguments.login_nm#" cfsqltype="CF_SQL_VARCHAR" maxlength="128">,
					<cfqueryparam value="#arguments.login_div#" cfsqltype="CF_SQL_VARCHAR" maxlength="2">
					)
			</cfquery>
 			--->

		<!--- ALTSWinter設定 --->
		<cfelseif arguments.resort_code eq 19>
			<!---altswinter（webサービスを利用し登録）--->


		<!---TOMAMGreen & Winter設定 (tomamuは２つのDBを同期)--->
		<cfelseif arguments.resort_code eq 20>

		<!--- RESORTPUBLIC設定 --->
		<cfelseif arguments.resort_code eq 201 or arguments.resort_code eq 210 or arguments.resort_code eq 220 or arguments.resort_code eq 202 or arguments.resort_code eq 203 or arguments.resort_code eq 230 or arguments.resort_code eq 231 or arguments.resort_code eq 232 or arguments.resort_code eq 233 or arguments.resort_code eq 234>
			<cfquery datasource="#xmlfile.datasource.bandai.XmlAttributes.dsn#" name="getAccount">
				select	 count(*) as cnt
				from	 #xmlfile.datasource.bandai.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
				and		 resort_code = <cfqueryparam value="#arguments.resort_code#" cfsqltype="CF_SQL_SMALLINT">
			</cfquery>
			<cfif getAccount.cnt neq 0>
				<cfquery datasource="#xmlfile.datasource.bandai.XmlAttributes.dsn#" name="delAccount">
					delete
					from	 #xmlfile.datasource.bandai.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
					where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
					and		 resort_code = <cfqueryparam value="#arguments.resort_code#" cfsqltype="CF_SQL_SMALLINT">
				</cfquery>
			</cfif>

			<!--- 2010/10/02 統合版で全施設参照アカウントが存在する場合、個別参照アカウントを削除する --->
			<cfquery datasource="#xmlfile.datasource.bandai.XmlAttributes.dsn#" name="getDoubleAccount">
				select	 count(*) as cnt
				from	 #xmlfile.datasource.bandai.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
				and		 resort_code = 0
			</cfquery>
			<cfif getDoubleAccount.cnt neq 0>
				<cfquery datasource="#xmlfile.datasource.bandai.XmlAttributes.dsn#" name="delDoubleAccount">
					delete
					from	 #xmlfile.datasource.bandai.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
					where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
					and		 resort_code <> 0
				</cfquery>
			</cfif>

			<cfquery datasource="#xmlfile.datasource.bandai.XmlAttributes.dsn#" name="query1">
				insert into #xmlfile.datasource.bandai.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser (
					login_id,
					resort_code,
					login_pw,
					login_nm,
					login_dv
				) values (
					<cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">,
					<cfqueryparam value="#arguments.resort_code#" cfsqltype="CF_SQL_SMALLINT">,
					<cfqueryparam value="#arguments.login_pw#" cfsqltype="CF_SQL_VARCHAR" maxlength="50">,
					<cfqueryparam value="#arguments.login_nm#" cfsqltype="CF_SQL_VARCHAR" maxlength="128">,
					<cfqueryparam value="#arguments.login_div#" cfsqltype="CF_SQL_VARCHAR" maxlength="2">
					)
			</cfquery>




		<!--- HNYPUBLIC設定 --->
		<cfelseif arguments.resort_code eq 101 or arguments.resort_code eq 102 or arguments.resort_code eq 105 or arguments.resort_code eq 106 or arguments.resort_code eq 107 or arguments.resort_code eq 108 or arguments.resort_code eq 503>
			<cfquery datasource="#xmlfile.datasource.hnypublic.XmlAttributes.dsn#" name="getAccount">
				select	 count(*) as cnt
				from	 #xmlfile.datasource.hnypublic.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
				and		 resort_code = <cfqueryparam value="#arguments.resort_code#" cfsqltype="CF_SQL_SMALLINT">
			</cfquery>
			<cfif getAccount.cnt neq 0>
				<cfquery datasource="#xmlfile.datasource.hnypublic.XmlAttributes.dsn#" name="delAccount">
					delete
					from	 #xmlfile.datasource.hnypublic.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
					where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
					and		 resort_code = <cfqueryparam value="#arguments.resort_code#" cfsqltype="CF_SQL_SMALLINT">
				</cfquery>
			</cfif>

			<!--- 2010/10/02 統合版で全施設参照アカウントが存在する場合、個別参照アカウントを削除する --->
			<cfquery datasource="#xmlfile.datasource.hnypublic.XmlAttributes.dsn#" name="getDoubleAccount">
				select	 count(*) as cnt
				from	 #xmlfile.datasource.hnypublic.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
				and		 resort_code = 0
			</cfquery>
			<cfif getDoubleAccount.cnt neq 0>
				<cfquery datasource="#xmlfile.datasource.hnypublic.XmlAttributes.dsn#" name="delDoubleAccount">
					delete
					from	 #xmlfile.datasource.hnypublic.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
					where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
					and		 resort_code <> 0
				</cfquery>
			</cfif>

			<cfquery datasource="#xmlfile.datasource.hnypublic.XmlAttributes.dsn#" name="query1">
				insert into #xmlfile.datasource.hnypublic.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser (
					login_id,
					resort_code,
					login_pw,
					login_nm,
					login_dv
				) values (
					<cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">,
					<cfqueryparam value="#arguments.resort_code#" cfsqltype="CF_SQL_SMALLINT">,
					<cfqueryparam value="#arguments.login_pw#" cfsqltype="CF_SQL_VARCHAR" maxlength="50">,
					<cfqueryparam value="#arguments.login_nm#" cfsqltype="CF_SQL_VARCHAR" maxlength="128">,
					<cfqueryparam value="#arguments.login_div#" cfsqltype="CF_SQL_VARCHAR" maxlength="2">
					)
			</cfquery>

		<!--- CRMPUBLIC設定 --->
		<cfelse>
			<cfquery datasource="#xmlfile.datasource.crmpublic.XmlAttributes.dsn#" name="getAccount">
				select	 count(*) as cnt
				from	 #xmlfile.datasource.crmpublic.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
				and		 resort_code = <cfqueryparam value="#arguments.resort_code#" cfsqltype="CF_SQL_SMALLINT">
			</cfquery>
			<cfif getAccount.cnt neq 0>
				<cfquery datasource="#xmlfile.datasource.crmpublic.XmlAttributes.dsn#" name="delAccount">
					delete
					from	 #xmlfile.datasource.crmpublic.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
					where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
					and		 resort_code = <cfqueryparam value="#arguments.resort_code#" cfsqltype="CF_SQL_SMALLINT">
				</cfquery>
			</cfif>

			<!--- 2010/10/02 統合版で全施設参照アカウントが存在する場合、個別参照アカウントを削除する --->
			<cfquery datasource="#xmlfile.datasource.crmpublic.XmlAttributes.dsn#" name="getDoubleAccount">
				select	 count(*) as cnt
				from	 #xmlfile.datasource.crmpublic.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
				and		 resort_code = 0
			</cfquery>
			<cfif getDoubleAccount.cnt neq 0>
				<cfquery datasource="#xmlfile.datasource.crmpublic.XmlAttributes.dsn#" name="delDoubleAccount">
					delete
					from	 #xmlfile.datasource.crmpublic.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
					where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
					and		 resort_code <> 0
				</cfquery>
			</cfif>

			<cfquery datasource="#xmlfile.datasource.crmpublic.XmlAttributes.dsn#" name="query1">
				insert into #xmlfile.datasource.crmpublic.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser (
					login_id,
					resort_code,
					login_pw,
					login_nm,
					login_dv
				) values (
					<cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">,
					<cfqueryparam value="#arguments.resort_code#" cfsqltype="CF_SQL_SMALLINT">,
					<cfqueryparam value="#arguments.login_pw#" cfsqltype="CF_SQL_VARCHAR" maxlength="50">,
					<cfqueryparam value="#arguments.login_nm#" cfsqltype="CF_SQL_VARCHAR" maxlength="128">,
					<cfqueryparam value="#arguments.login_div#" cfsqltype="CF_SQL_VARCHAR" maxlength="2">
					)
			</cfquery>


		</cfif>


	<cfcatch>
		<cfset rs = 1>

		<cfset chatch_dt = "#cfcatch.message# ： #cfcatch.detail#">
		<cfset structinsert(s_check, "rs", rs)>
		<cfset structinsert(s_check, "er_msg", chatch_dt)>

		<cfreturn s_check>
	</cfcatch>
	</cftry>
		<cfset rs = 0>
		<cfset structinsert(s_check, "rs", rs)>

		<cfreturn s_check>
</cffunction>

<!---削除処理--->
<cffunction name="delete_data" returntype="any">
	<cfargument name="resort_code">
	<cfargument name="login_id">
	<cfargument name="login_pw">

	<cfset s_check = StructNew()>

	<cffile action="read" file="#expandpath("xml")#\datasource.xml" variable="XMLFileText">
	<cfset xmlfile=XmlParse(XMLFileText)>

	<cftry>
		<!---CRMKitchenの場合--->
		<!--- <cfif arguments.resort_code eq 999> --->
		<cfif arguments.resort_code eq 999 or arguments.resort_code eq 0>

			<!---全てに登録--->

			<!---crmpublic--->
			<!--- 全権限の場合は全レコード削除する 2010/10/02  --->
			<cfquery datasource="#xmlfile.datasource.crmpublic.XmlAttributes.dsn#" name="query1">
				delete
				from	 #xmlfile.datasource.crmpublic.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
				and		 resort_code = 0
			</cfquery>

			<!---alts--->
			<!---
			<cfquery datasource="#xmlfile.datasource.alts.XmlAttributes.dsn#" name="query2">
				delete
				from	 #xmlfile.datasource.alts.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			</cfquery>
 			--->

			<!---bandai--->
			<!--- 全権限の場合は全レコード削除する 2010/10/02  --->
			<cfquery datasource="#xmlfile.datasource.bandai.XmlAttributes.dsn#" name="query2">
				delete
				from	 #xmlfile.datasource.bandai.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			</cfquery>

			<!---hnypublic--->
			<!--- 全権限の場合は全レコード削除する 2010/10/02  --->
			<cfquery datasource="#xmlfile.datasource.hnypublic.XmlAttributes.dsn#" name="query2">
				delete
				from	 #xmlfile.datasource.hnypublic.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			</cfquery>

			<!---hbc--->
			<cfquery datasource="#xmlfile.datasource.hbc.XmlAttributes.dsn#" name="query4">
				delete
				from	 #xmlfile.datasource.hbc.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			</cfquery>
			<!--- hbc ⇔ crmwedding --->
			<cfquery datasource="#xmlfile.datasource.crmwedding.XmlAttributes.dsn#" name="query4">
				delete
				from	 #xmlfile.datasource.crmwedding.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			</cfquery>

			<!---rnr--->
			<cfquery datasource="#xmlfile.datasource.rnr.XmlAttributes.dsn#" name="query5">
				delete
				from	 #xmlfile.datasource.rnr.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			</cfquery>

			<!---tomamuは２つのDBへ登録--->
			<!--- <cfquery datasource="#xmlfile.datasource.tomamu_g.XmlAttributes.dsn#" name="query6">
				delete
				from	 #xmlfile.datasource.tomamu_g.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			</cfquery>
			<cfquery datasource="#xmlfile.datasource.tomamu_w.XmlAttributes.dsn#" name="query7">
				delete
				from	 #xmlfile.datasource.tomamu_w.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			</cfquery> --->

			<!---altswinter（webサービスを利用し削除）--->
			<!--- <cfinvoke webservice = "#xmlfile.datasource.web_serv.xmltext#" method = "alts_del" returnVariable = "rsquery">
				<cfinvokeargument name="login_id" value="#arguments.login_id#">
				<!---↓altswinter(別サーバデータソース)--->
				<cfinvokeargument name="ds_nm" value="#xmlfile.datasource.links_db.xmltext#">
				<cfinvokeargument name="dbu" value="#xmlfile.datasource.loginuser.xmltext#">
			</cfinvoke>

			<cfif rsquery.rs_a eq 1>
				<cfset error_msg = "#rsquery.er_msg#">
				<cfthrow message = "#error_msg#" detail = "#error_msg#">
			</cfif> --->
			<!---altswinter
			<cfquery datasource="#xmlfile.datasource.altswinter.xmltext#" name="query3">
				delete
				from
					#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where
					login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			</cfquery>--->


		<!---HBC削除--->
		<cfelseif arguments.resort_code eq 16>
			<cfquery datasource="#xmlfile.datasource.hbc.XmlAttributes.dsn#" name="query１">
				delete
				from	 #xmlfile.datasource.hbc.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			</cfquery>
			<!--- hbc ⇔ crmwedding --->
			<cfquery datasource="#xmlfile.datasource.crmwedding.XmlAttributes.dsn#" name="query１">
				delete
				from	 #xmlfile.datasource.crmwedding.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			</cfquery>


		<!---RNR削除--->
		<cfelseif arguments.resort_code eq 17>
			<cfquery datasource="#xmlfile.datasource.rnr.XmlAttributes.dsn#" name="query1">
				delete
				from	 #xmlfile.datasource.rnr.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			</cfquery>


		<!---ALTS削除--->
		<cfelseif arguments.resort_code eq 18>
			<!--- <cfquery datasource="#xmlfile.datasource.alts.XmlAttributes.dsn#" name="query1">
				delete
				from	 #xmlfile.datasource.alts.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			</cfquery> --->

		<!---ALTSWinter削除（webサービスを利用し削除）--->
		<cfelseif arguments.resort_code eq 19>

		<!---TOMAMGreen & Winter削除 (tomamuは２つのDBを同期)--->

		<cfelseif arguments.resort_code eq 20>


		<!--- RESORTPUBLIC削除 --->
		<cfelseif arguments.resort_code eq 201 or arguments.resort_code eq 210 or arguments.resort_code eq 220 or arguments.resort_code eq 202 or arguments.resort_code eq 203 or arguments.resort_code eq 230 or arguments.resort_code eq 231 or arguments.resort_code eq 232 or arguments.resort_code eq 233 or arguments.resort_code eq 234>
			<cfquery datasource="#xmlfile.datasource.bandai.XmlAttributes.dsn#" name="query1">
				delete
				from	 #xmlfile.datasource.bandai.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
				and		 resort_code = <cfqueryparam value="#arguments.resort_code#" cfsqltype="CF_SQL_SMALLINT">
			</cfquery>

		<!--- HNYPUBLIC削除 --->
		<cfelseif arguments.resort_code eq 101 or arguments.resort_code eq 102 or arguments.resort_code eq 105 or arguments.resort_code eq 106 or arguments.resort_code eq 107 or arguments.resort_code eq 108 or arguments.resort_code eq 503>
			<!---hnypublic--->
			<cfquery datasource="#xmlfile.datasource.hnypublic.XmlAttributes.dsn#" name="query1">
				delete
				from	 #xmlfile.datasource.hnypublic.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
				and		 resort_code = <cfqueryparam value="#arguments.resort_code#" cfsqltype="CF_SQL_SMALLINT">
			</cfquery>

		<!--- CRMPUBLIC削除 --->
		<cfelse>
			<cfquery datasource="#xmlfile.datasource.crmpublic.XmlAttributes.dsn#" name="query1">
				delete
				from	 #xmlfile.datasource.crmpublic.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
				and		 resort_code = <cfqueryparam value="#arguments.resort_code#" cfsqltype="CF_SQL_SMALLINT">
			</cfquery>
		</cfif>
	<cfcatch>
		<cfset rs = 1>

		<cfset chatch_dt = "#cfcatch.message# ： #cfcatch.detail#">

		<cfset structinsert(s_check, "rs", rs)>
		<cfset structinsert(s_check, "er_msg", chatch_dt)>

		<cfreturn s_check>
	</cfcatch>
	</cftry>
		<cfset rs = 0>
		<cfset structinsert(s_check, "rs", rs)>

		<cfreturn s_check>
</cffunction>


<!---旅館名を取得する--->
<cffunction name="getresort" returntype="any">
	<cfargument name="resort_code">
	<cffile action="read" file="#expandpath("xml")#\datasource.xml" variable="XMLFileText">
	<cfset xmlfile=XmlParse(XMLFileText)>
	<cfset r_val = "">


	<cfif len(arguments.resort_code) eq 0>
		<cfset r_val = "なし">
	<cfelse>
		<!---旅館コードを見て、引数と同じものがあったら旅館名を返す--->
		<cfset lp_cnt = listlen("#xmlfile.datasource.resortdata.reg.xmltext#")>
		<cfloop index = "i" from = "1" to = "#lp_cnt#">
			<cfset idx = listgetat("#xmlfile.datasource.resortdata.reg.xmltext#",#i#)>

			<cfif arguments.resort_code eq idx>
				<cfset r_val = listgetat("#xmlfile.datasource.resortdata.reg_rs.xmltext#",#i#)>
				<cfbreak>
			</cfif>
		</cfloop>
	</cfif>

	<cfreturn r_val>
</cffunction>

<!---登録された旅館が存在するかチェック--->
<cffunction name="check_resort" returntype="any">
	<cfargument name="resort_code">
	<cffile action="read" file="#expandpath("xml")#\datasource.xml" variable="XMLFileText">
	<cfset xmlfile=XmlParse(XMLFileText)>
	<cfset bl_rs = 1>

	<!---旅館コードが存在した場合、戻り値で0を返す--->
	<cfif len(arguments.resort_code) neq 0>
		<cfloop index = "i" list="#xmlfile.datasource.resortdata.reg.xmltext#">
			<cfset idx = #i#>

			<cfif arguments.resort_code eq idx>
				<cfset bl_rs = 0>
				<cfbreak>
			</cfif>
		</cfloop>
	<cfelse>
		<cfset bl_rs = 0>
	</cfif>

	<cfreturn bl_rs>
</cffunction>

<!---登録された権限が存在するかチェック--->
<cffunction name="check_div" returntype="any">
	<cfargument name="resort_code">
	<cfargument name="login_div">

	<cffile action="read" file="#expandpath("xml")#\datasource.xml" variable="XMLFileText">
	<cfset xmlfile=XmlParse(XMLFileText)>
	<cfset bl_rs = 1>

	<!---旅館コードごとに割り振られた権限が存在すれば戻り値で0を返す--->
	<cfif arguments.resort_code eq 0 or arguments.resort_code eq 999>
		<cfloop index = "i" list="#xmlfile.datasource.resortdata.auth1.xmltext#">
			<cfset idx = #i#>
			<cfif arguments.login_div eq idx>
				<cfset bl_rs = 0>
				<cfbreak>
			</cfif>
		</cfloop>
	<cfelseif arguments.resort_code eq 20>
		<cfloop index = "i" list="#xmlfile.datasource.resortdata.auth4.xmltext#">
			<cfset idx = #i#>
			<cfif arguments.login_div eq idx>
				<cfset bl_rs = 0>
				<cfbreak>
			</cfif>
		</cfloop>
	<cfelseif arguments.resort_code eq 16 or arguments.resort_code eq 17 or arguments.resort_code eq 18 or arguments.resort_code eq 19>
		<cfloop index = "i" list="#xmlfile.datasource.resortdata.auth3.xmltext#">
			<cfset idx = #i#>
			<cfif arguments.login_div eq idx>
				<cfset bl_rs = 0>
				<cfbreak>
			</cfif>
		</cfloop>
	<cfelse>
		<cfloop index = "i" list="#xmlfile.datasource.resortdata.auth2.xmltext#">
			<cfset idx = #i#>
			<cfif arguments.login_div eq idx>
				<cfset bl_rs = 0>
				<cfbreak>
			</cfif>
		</cfloop>
	</cfif>


	<cfreturn bl_rs>
</cffunction>
<!---XSS対策--->
<cffunction name="xss_check" returntype="any">
	<cfargument name="ch_str">
	<cfset ch_str1 = arguments.ch_str>
	<cfset ch_str1 = replace("#ch_str1#","&","&amp;","all")>
	<cfset ch_str1 = replace("#ch_str1#","<","&lt;","all")>
	<cfset ch_str1 = replace("#ch_str1#",">","&gt;","all")>
	<cfset ch_str1 = replace("#ch_str1#","'","&##39;","all")>
	<cfset ch_str1 = replace("#ch_str1#","\","&yen;","all")>

	<cfreturn ch_str1>
</cffunction>

<cffunction name="cnt_data" returntype="any">
	<cfargument name="resort_code">
	<cfargument name="login_id">
	<cfargument name="login_pw">
	<cfargument name="login_nm">
	<cfargument name="login_div">

	<cffile action="read" file="#expandpath("xml")#\datasource.xml" variable="XMLFileText">
	<cfset xmlfile=XmlParse(XMLFileText)>

	<cfset cnt_check = StructNew()>
	<cfset check_msg = "">

	<cftry>
		<cfquery datasource="#xmlfile.datasource.ds.xmltext#" name="query1">
			select
				count(*) as cnt
			from
				#xmlfile.datasource.crmpublic.xmltext#.
				#xmlfile.datasource.loginuser.xmltext#.m_loginuser
			where
				login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			and
				resort_code != 0
		</cfquery>

		<cfif query1.cnt neq 0>

			<cfquery datasource="#xmlfile.datasource.ds.xmltext#" name="query2">
				select
					resort_code
				from
					#xmlfile.datasource.crmpublic.xmltext#.
					#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where
					login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
				and
					resort_code != 0
			</cfquery>

			<cfset check_msg = "※旅館：">

			<cfloop query="query2">
				<cfinvoke method="getresort" returnVariable="r_name1">
					<cfinvokeargument name="resort_code" value="#query2.resort_code#">
				</cfinvoke>

				<cfset check_msg = check_msg & " " & r_name1>
			</cfloop>

			<cfset check_msg = check_msg & "に登録がありますが、旅館ALLへ登録を変更しました。">
			<cfquery datasource="#xmlfile.datasource.ds.xmltext#" name="query3">
				delete
				from
					#xmlfile.datasource.crmpublic.xmltext#.
					#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where
					login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			</cfquery>
		</cfif>
	<cfcatch>
		<cfset rs = 1>
		<cfset structinsert(cnt_check, "rs", rs)>
		<cfset structinsert(cnt_check, "er_msg", cfcatch.detail)>

		<cfreturn cnt_check>
	</cfcatch>
	</cftry>
		<cfset rs = 0>
		<cfset structinsert(cnt_check, "rs", rs)>
		<cfset structinsert(cnt_check, "chk_msg", check_msg)>
		<cfreturn cnt_check>
</cffunction>

<cffunction name="cnt_data2" returntype="any">
	<cfargument name="resort_code">
	<cfargument name="login_id">
	<cfargument name="login_pw">
	<cfargument name="login_nm">
	<cfargument name="login_div">

	<cffile action="read" file="#expandpath("xml")#\datasource.xml" variable="XMLFileText">
	<cfset xmlfile=XmlParse(XMLFileText)>

	<cfset cnt_check = StructNew()>

	<cftry>
		<cfquery datasource="#xmlfile.datasource.ds.xmltext#" name="query1">
			select	 count(*) as cnt
			from	 #xmlfile.datasource.crmpublic.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
			where	 login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			and		 resort_code = 0
		</cfquery>
		<cfif query1.cnt neq 0>
			<cfset error_msg = "すでに旅館ALLの登録があるので登録できません。">
			<cfthrow message = "#error_msg#" detail = "#error_msg#">
		</cfif>
	<cfcatch>
		<cfset rs = 1>
		<cfset structinsert(cnt_check, "rs", rs)>
		<cfset structinsert(cnt_check, "er_msg", cfcatch.detail)>

		<cfreturn cnt_check>
	</cfcatch>
	</cftry>
		<cfset rs = 0>
		<cfset structinsert(cnt_check, "rs", rs)>
		<cfreturn cnt_check>
</cffunction>

<cffunction name="cnt_data3" returntype="any">
	<cfargument name="resort_code">
	<cfargument name="login_id">

	<cffile action="read" file="#expandpath("xml")#\datasource.xml" variable="XMLFileText">
	<cfset xmlfile=XmlParse(XMLFileText)>

	<cfset cnt_check3 = StructNew()>

	<cftry>
		<cfset error_msg = "指定されたリゾートコードには登録が存在しないので削除できません。">
		<!--- <cfif arguments.resort_code eq 999> --->
		<cfif arguments.resort_code eq 999 or arguments.resort_code eq 0>

			<cfquery datasource="#xmlfile.datasource.ds.xmltext#" name="query1">
				select
					count(*) as cnt
				from
					#xmlfile.datasource.crmpublic.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where
					login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
					and resort_code = 0
			</cfquery>

			<cfif query1.cnt eq 0>
				<cfthrow message = "#error_msg#" detail = "#error_msg#">
			</cfif>

			<cfquery datasource="#xmlfile.datasource.ds.xmltext#" name="query1">
				select
					count(*) as cnt
				from
					#xmlfile.datasource.hbc.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where
					login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			</cfquery>

			<cfif query1.cnt eq 0>
				<cfthrow message = "#error_msg#" detail = "#error_msg#">
			</cfif>

			<cfquery datasource="#xmlfile.datasource.ds.xmltext#" name="query1">
				select
					count(*) as cnt
				from
					#xmlfile.datasource.rnr.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where
					login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			</cfquery>

			<cfif query1.cnt eq 0>
				<cfthrow message = "#error_msg#" detail = "#error_msg#">
			</cfif>

			<cfquery datasource="#xmlfile.datasource.ds.xmltext#" name="query1">
				select
					count(*) as cnt
				from
					#xmlfile.datasource.alts.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where
					login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			</cfquery>

			<cfif query1.cnt eq 0>
				<cfthrow message = "#error_msg#" detail = "#error_msg#">
			</cfif>

			<cfquery datasource="#xmlfile.datasource.ds.xmltext#" name="query1">
				select
					count(*) as cnt
				from
					#xmlfile.datasource.tomamu_g.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where
					login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			</cfquery>

			<cfif query1.cnt eq 0>
				<cfthrow message = "#error_msg#" detail = "#error_msg#">
			</cfif>

			<cfquery datasource="#xmlfile.datasource.ds.xmltext#" name="query1">
				select
					count(*) as cnt
				from
					#xmlfile.datasource.tomamu_w.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where
					login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			</cfquery>

			<cfif query1.cnt eq 0>
				<cfthrow message = "#error_msg#" detail = "#error_msg#">
			</cfif>

			<cfinvoke webservice = "#xmlfile.datasource.web_serv.xmltext#" method = "alts_chk" returnVariable = "rsquery">
				<cfinvokeargument name="login_id" value="#arguments.login_id#">
				<!---↓altswinter(別サーバデータソース)--->
				<cfinvokeargument name="ds_nm" value="#xmlfile.datasource.links_db.xmltext#">
				<cfinvokeargument name="dbu" value="#xmlfile.datasource.loginuser.xmltext#">
			</cfinvoke>

			<cfif rsquery.rs_cnt eq 0>
				<cfthrow message = "#error_msg#" detail = "#error_msg#">
			</cfif>

		<cfelseif arguments.resort_code eq 16>
			<cfquery datasource="#xmlfile.datasource.ds.xmltext#" name="query1">
				select
					count(*) as cnt
				from
					#xmlfile.datasource.hbc.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where
					login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			</cfquery>

			<cfif query1.cnt eq 0>
				<cfthrow message = "#error_msg#" detail = "#error_msg#">
			</cfif>
		<cfelseif arguments.resort_code eq 17>
			<cfquery datasource="#xmlfile.datasource.ds.xmltext#" name="query1">
				select
					count(*) as cnt
				from
					#xmlfile.datasource.rnr.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where
					login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			</cfquery>

			<cfif query1.cnt eq 0>
				<cfthrow message = "#error_msg#" detail = "#error_msg#">
			</cfif>
		<cfelseif arguments.resort_code eq 18>
			<!--- <cfquery datasource="#xmlfile.datasource.ds.xmltext#" name="query1">
				select
					count(*) as cnt
				from
					#xmlfile.datasource.alts.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where
					login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			</cfquery> --->

			<cfif query1.cnt eq 0>
				<cfthrow message = "#error_msg#" detail = "#error_msg#">
			</cfif>
		<cfelseif arguments.resort_code eq 19>
<!---
			<cfinvoke webservice = "#xmlfile.datasource.web_serv.xmltext#" method = "alts_chk" returnVariable = "rsquery">
				<cfinvokeargument name="login_id" value="#arguments.login_id#">
				<cfinvokeargument name="ds_nm" value="#xmlfile.datasource.links_db.xmltext#">
				<cfinvokeargument name="dbu" value="#xmlfile.datasource.loginuser.xmltext#">
			</cfinvoke>

			<cfif rsquery.rs_cnt eq 0>
				<cfthrow message = "#error_msg#" detail = "#error_msg#">
			</cfif> --->

		<cfelseif arguments.resort_code eq 20>
			<cfquery datasource="#xmlfile.datasource.ds.xmltext#" name="query1">
				select
					count(*) as cnt
				from
					#xmlfile.datasource.tomamu_g.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where
					login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			</cfquery>

			<cfif query1.cnt eq 0>
				<cfthrow message = "#error_msg#" detail = "#error_msg#">
			</cfif>

			<cfquery datasource="#xmlfile.datasource.ds.xmltext#" name="query1">
				select
					count(*) as cnt
				from
					#xmlfile.datasource.tomamu_w.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where
					login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
			</cfquery>

			<cfif query1.cnt eq 0>
				<cfthrow message = "#error_msg#" detail = "#error_msg#">
			</cfif>
		<cfelse>
			<cfquery datasource="#xmlfile.datasource.ds.xmltext#" name="query1">
				select
					count(*) as cnt
				from
					#xmlfile.datasource.crmpublic.xmltext#.#xmlfile.datasource.loginuser.xmltext#.m_loginuser
				where
					login_id = <cfqueryparam value="#arguments.login_id#" cfsqltype="CF_SQL_VARCHAR" maxlength="12">
					and	resort_code = <cfqueryparam value="#arguments.resort_code#" cfsqltype="CF_SQL_SMALLINT">
			</cfquery>

			<cfif query1.cnt eq 0>
				<cfthrow message = "#error_msg#" detail = "#error_msg#">
			</cfif>

		</cfif>
	<cfcatch>
		<cfset rs = 1>
		<cfset structinsert(cnt_check3, "rs", rs)>
		<cfset structinsert(cnt_check3, "er_msg", cfcatch.detail)>

		<cfreturn cnt_check3>
	</cfcatch>
	</cftry>
		<cfset rs = 0>
		<cfset structinsert(cnt_check3, "rs", rs)>
		<cfreturn cnt_check3>
</cffunction>
</cfcomponent>
