<cfcomponent name="members_nonconnectedition" access="public" description="Responds to Flash remoting requests">

<cffunction name="editMember_nonconnectedition" output="false" description="" access="remote" returntype="any">
	<cfargument name="login_nm" default="" type="string">
	<cfargument name="login_id" default="" type="string">
	<cfargument name="login_pw" default="" type="string">
	<cfargument name="resort_code" default="" type="string">
	<cfargument name="login_dv" default="" type="string">

	<cfset aaa=false>
	<cftry>
		<cftransaction action="begin">
			<!--- データファイル読込 --->
			<cfinclude template="inc_setting.cfm">
			<!--- 削除処理 --->
			<cfquery dbtype="query" name="q_dsn">
				select dsn from q_resort group by dsn
			</cfquery>
			<cfloop query="q_dsn">
				<cfquery datasource="#application.datasource#" name="q_login" result="deb_1">
					delete from #dsn#m_loginuser
					where login_id = '#arguments.login_id#'
				</cfquery>
			</cfloop>
			<!--- データソース切替 --->
			<cfquery dbtype="query" name="q_logintable">
				select dsn,application_nm from q_resort where resort_code = #arguments.resort_code# group by dsn,application_nm
			</cfquery>
			<cfset wk_login_dv = #arguments.login_dv#>
			<cfloop query="q_logintable">
				<!--- CRM全権限 トマムは型変更させる --->
				<cfif application_nm eq "tomamu">
					<cfset wk_login_dv = #arguments.login_dv#>
				</cfif>
				<cfquery datasource="#application.datasource#" name="q_login" result="deb_1">
					select * from #dsn#m_loginuser
					where login_id = '#arguments.login_id#'
					<cfif application_nm eq "crmpublic">
					and	  resort_code = #arguments.resort_code#
					</cfif>
				</cfquery>
				<cfif q_login.recordcount eq 0>
					<cfquery datasource="#application.datasource#" name="u_m_login" result="deb_1">
						insert into #dsn#m_loginuser (
							 login_id
							,login_pw
							,login_nm
							,login_dv
							<cfif application_nm eq "crmpublic">
							,resort_code
							</cfif>
						)values(
							 '#arguments.login_id#'
							,'#arguments.login_pw#'
							,'#arguments.login_nm#'
							<cfif application_nm eq "tomamu">
							, #wk_login_dv# 
							<cfelse>
							,'#wk_login_dv#'
							</cfif>
							<cfif application_nm eq "crmpublic">
							,#arguments.resort_code#
							</cfif>				
						)
					</cfquery>
				<cfelse>
					<cfquery datasource="#application.datasource#" name="u_m_login" result="deb_1">
						update #dsn#m_loginuser set
							 login_id =  '#arguments.login_id#'
							,login_pw =  '#arguments.login_pw#'
							,login_nm =  '#arguments.login_nm#'
							,login_dv =  '#wk_login_dv#'
						where login_id = '#arguments.login_id#'
						<cfif application_nm eq "crmpublic">
						and	  resort_code = #arguments.resort_code#
						</cfif>
					</cfquery>
				</cfif>
			</cfloop>
					
				
		<cfset aaa=true>
					
		</cftransaction>
		<cfcatch>
			<cfreturn  cfcatch.detail/>
		</cfcatch>
	</cftry>
	<cfreturn  aaa/>
</cffunction>	
				
				


<cffunction name="delMember_nonconnectedition" output="false" description="" access="remote" returntype="any">
	<cfargument name="login_id" default="" type="string">
	<cftry>
		<cftransaction action="begin">
			<!--- データファイル読込 --->
			<cfinclude template="inc_setting.cfm">
			<!--- データソース切替 --->
			<cfquery dbtype="query" name="q_logintable">
				select dsn,application_nm from q_resort group by dsn,application_nm
			</cfquery>
			<cfloop query="q_logintable">
				<cfquery datasource="#application.datasource#" name="u_m_login" result="deb_1">
					delete #dsn#m_loginuser
					where login_id = '#arguments.login_id#'
				</cfquery>
			</cfloop>
		</cftransaction>
		<cfcatch>
			<cfreturn  cfcatch.detail/>
		</cfcatch>
	</cftry>
	<cfreturn  arguments/>
</cffunction>
</cfcomponent>