﻿<cfif not isdefined("session.username")>
	<cflocation url="login.cfm" addtoken="no">
<cfelse>
	<cfset StructClear(Session)>
</cfif>


<cfprocessingdirective suppresswhitespace=true pageEncoding="UTF-8">
<cfsetting enableCFoutputOnly = "Yes" showdebugoutput="yes">
<cfoutput>
<cftry>
<!--- パラメータ初期設定 --->
<cfparam name="errormsg" default="">
<cfparam name="searchflg" default="false">
<!--- 
<cfparam name="form.reserve_date" default="2007/01/01">
 --->

<!--- 検索項目 --->
<cfparam name="form.login_nm" default="">
<cfparam name="form.login_id" default="">
<cfparam name="form.resort_code" default="">

<!--- 登録項目 --->
<cfparam name="form.ins_resort_code" default="">
<cfparam name="form.ins_login_nm" default="">
<cfparam name="form.ins_login_id" default="">
<cfparam name="form.ins_login_pw" default="">

<cfsavecontent variable="getData">
	var connection:mx.remoting.Connection = mx.remoting.NetServices.createGatewayConnection("http://#cgi.HTTP_HOST#/flashservices/gateway/");
	var myService:mx.remoting.NetServiceProxy;
	var responseHandler = {};

	var login_nm = login_nm
	var lk_nm = lk_nm
	var login_id = login_id
	var resort_code = resort_code
	var g_member = g_member;
	var refreshflg = false
	responseHandler.onResult = function( results: Object ):Void {
		//g_member.removeColumnAt(5)
		//g_member.addColumnAt(5, "login_dv")
		//g_member.getColumnAt(5).headerText="権限"
		//g_member.getColumnAt(5).cellRenderer = cbCell;
		g_member.dataProvider = results.items;
		for(var i=0; i<results.mRecordsAvailable;i++){
			g_member.editField(i,g_member.getColumnAt(0).columnName,i+1)
		}
		if(!refreshflg){
			alert(results.mRecordsAvailable+"件です",'検索結果')
		}
	}
   
	responseHandler.onStatus  = function( stat: Object ):Void {
		alert("サービスエラーです:" + stat.description,'検索不可');
	}
	myService = connection.getService("crmpublic.generalmanage.members", responseHandler );
	myService.getMembers(login_nm.text,login_id.text,resort_code.selectedItem.data);
</cfsavecontent>

<cfsavecontent variable="updData">
	var connection:mx.remoting.Connection = mx.remoting.NetServices.createGatewayConnection("http://#cgi.HTTP_HOST#/flashservices/gateway/");
	var myService:mx.remoting.NetServiceProxy;
	var responseHandler = {};

	responseHandler.onResult = function( results: Object ):Void {
		alert(" データ更新が完了しました",'更新完了')
	}
   
	responseHandler.onStatus  = function( stat: Object ):Void {
		alert("サービスエラーです:" + stat.description,'更新できませんでした');
	}
	myService = connection.getService("crmpublic.generalmanage.members", responseHandler );
	myService.setMembers(g_member.dataProvider);
</cfsavecontent>

<cfsavecontent variable="setData">
	if(g_member.selectedIndex==undefined){
			alert("変更したいデータ行を選択してください",'　　　変更できません　　　');
	} else {
		/*
		if(g_member.getItemAt(g_member.selectedIndex).resort_code != 0){
			alert("このデータは変更できません",'　　　変更できません　　　');
		} else {
			tnav.selectedIndex=1
			ins_login_nm.text = g_member.getItemAt(g_member.selectedIndex).login_nm;
			ins_login_id.text = g_member.getItemAt(g_member.selectedIndex).login_id;
			ins_login_pw.text = g_member.getItemAt(g_member.selectedIndex).login_pw;
			ins_login_id.enabled = false;
		}
		*/
		
		tnav.selectedIndex=1
		for(var i=0;i<ins_resort_code.dataProvider.length;i++){
			if(ins_resort_code.dataProvider[i].data==g_member.getItemAt(g_member.selectedIndex).resort_code){
				ins_resort_code.selectedIndex = i;
			}
		}
		ins_login_nm.text = g_member.getItemAt(g_member.selectedIndex).login_nm;
		ins_login_id.text = g_member.getItemAt(g_member.selectedIndex).login_id;
		ins_login_pw.text = g_member.getItemAt(g_member.selectedIndex).login_pw;
		ins_login_id.enabled = false;

		var rcd=g_member.getItemAt(g_member.selectedIndex).resort_code;
		//	--CRMKitchen管理下＆旅館ＡＬＬアカウント
		if(rcd==0 || rcd==999){
			var cbprovider : Array = [
																	{label: "フルアクセス", data: "s"}
																];
		}
		//	--旅館
		if(rcd==1 || rcd==2 || rcd==3 || rcd==4 || rcd==5 || rcd==8 || rcd==9 || rcd==11 || rcd==14 || rcd==15){
			var cbprovider : Array = [
																	{label: "フルアクセス", data: "s"},
																	{label: "メール可", data: "b"}, 
																	{label:"CSのみ", data:"z"}
																];
		}
		//	--トマム
		if(rcd==501){
			var cbprovider : Array = [
																	{label: "フルアクセス", data: "2"},
																	{label:"CSのみ", data:"0"}
																];
		}
		//	--HBC＆リゾ
		if(rcd==502 || rcd==505){
			var cbprovider : Array = [
																	{label: "フルアクセス", data: "s"},
																	{label: "宿泊", data: "a"}, 
																	{label: "宿泊メール", data: "b"}, 
																	{label: "新婦", data: "c"}, 
																	{label: "新婦メール", data: "d"}, 
																	{label: "CSのみ", data: "z"} 
																];
		}
		//	--アルツ
		if(rcd==503){
			var cbprovider : Array = [
																	{label: "フルアクセス", data: "s"},
																	{label: "BOH", data: "a"}, 
																	{label: "BOHメール", data: "b"}, 
																	{label: "RIB", data: "c"}, 
																	{label: "RIBメール", data: "d"}, 
																	{label: "CSのみ", data: "z"} 
																];
		}
		ins_login_dv.dataProvider = cbprovider;
		for(var i=0;i<ins_login_dv.dataProvider.length;i++){
			if(ins_login_dv.dataProvider[i].data==g_member.getItemAt(g_member.selectedIndex).login_dv){
				ins_login_dv.selectedIndex = i;
			}
		}
			
	}
</cfsavecontent>
<cfsavecontent variable="changedv">
		var rcd=ins_resort_code.selectedItem.data;
		//	--CRMKitchen管理下＆旅館ＡＬＬアカウント
		if(rcd==0 || rcd==999){
			var cbprovider : Array = [
																	{label: "フルアクセス", data: "s"}
																];
		}
		//	--旅館
		if(rcd==1 || rcd==2 || rcd==3 || rcd==4 || rcd==5 || rcd==8 || rcd==9 || rcd==11 || rcd==14 || rcd==15){
			var cbprovider : Array = [
																	{label: "フルアクセス", data: "s"},
																	{label: "メール可", data: "b"}, 
																	{label:"CSのみ", data:"z"}
																];
		}
		//	--トマム
		if(rcd==501){
			var cbprovider : Array = [
																	{label: "フルアクセス", data: "2"},
																	{label:"CSのみ", data:"0"}
																];
		}
		//	--HBC＆リゾ
		if(rcd==502 || rcd==505){
			var cbprovider : Array = [
																	{label: "フルアクセス", data: "s"},
																	{label: "宿泊", data: "a"}, 
																	{label: "宿泊メール", data: "b"}, 
																	{label: "新婦", data: "c"}, 
																	{label: "新婦メール", data: "d"}, 
																	{label: "CSのみ", data: "z"} 
																];
		}
		//	--アルツ
		if(rcd==503){
			var cbprovider : Array = [
																	{label: "フルアクセス", data: "s"},
																	{label: "BOH", data: "a"}, 
																	{label: "BOHメール", data: "b"}, 
																	{label: "RIB", data: "c"}, 
																	{label: "RIBメール", data: "d"}, 
																	{label: "CSのみ", data: "z"} 
																];
		}
		ins_login_dv.dataProvider = cbprovider;	
		for(var i=0;i<ins_login_dv.dataProvider.length;i++){
		if(ins_login_dv.dataProvider[i].data==g_member.getItemAt(g_member.selectedIndex).login_dv){
			ins_login_dv.selectedIndex = i;
		}
	}
</cfsavecontent>

<cfsavecontent variable="delData">
	var connection:mx.remoting.Connection = mx.remoting.NetServices.createGatewayConnection("http://#cgi.HTTP_HOST#/flashservices/gateway/");
	var myService:mx.remoting.NetServiceProxy;
	var responseHandler = {};
	var login_nm = g_member.getItemAt(g_member.selectedIndex).login_nm;
	var login_id = g_member.getItemAt(g_member.selectedIndex).login_id;
	var resort_code = g_member.getItemAt(g_member.selectedIndex).resort_code;
	
	var confirm = function (evt){
        if (evt.detail == mx.controls.Alert.OK){
			myService = connection.getService("crmpublic.generalmanage.members", responseHandler );
			myService.delMember(login_id,resort_code);
        }
	}
	responseHandler.onResult = function( results: Object ):Void {
		alert(" データを削除しました",'削除完了')
	}
   
	responseHandler.onStatus  = function( stat: Object ):Void {
		alert("サービスエラーです:" + stat.description,'削除できませんでした');
	}
	if(g_member.selectedIndex==undefined){
			alert("削除したいデータ行を選択してください",'　　　削除できません　　　');
	} else {
		/*
		if(g_member.getItemAt(g_member.selectedIndex).resort_code != 0){
			alert("このデータは削除できません",'　　　削除できません　　　');
		} else {
			alert("ⅠＤ：" + login_id + "\r" + "名前：" + login_nm + "\r" + "を削除します。宜しいですか", "　　　データ削除　　　", 
			mx.controls.Alert.OK | mx.controls.Alert.CANCEL,  confirm);
		}
		*/
		alert("ⅠＤ：" + login_id + "\r" + "名前：" + login_nm + "\r" + "を削除します。宜しいですか", "　　　データ削除　　　", 
		mx.controls.Alert.OK | mx.controls.Alert.CANCEL,  confirm);
	}	
</cfsavecontent>
	

<cfsavecontent variable="editMember">
	var connection:mx.remoting.Connection = mx.remoting.NetServices.createGatewayConnection("http://#cgi.HTTP_HOST#/flashservices/gateway/");
	var myService:mx.remoting.NetServiceProxy;
	var responseHandler = {};
	var resort_code = ins_resort_code;
	var login_nm = ins_login_nm;
	var login_id = ins_login_id;
	var login_pw = ins_login_pw;
	var login_dv = ins_login_dv;

	var confirm = function (evt){
        if (evt.detail == mx.controls.Alert.OK){
			myService = connection.getService("crmpublic.generalmanage.members", responseHandler );
			myService.editMember(login_nm.text,login_id.text,login_pw.text,resort_code.selectedItem.data,login_dv.selectedItem.data);
        }
	}
	responseHandler.onResult = function( results: Object ):Void {
		ins_login_nm.text=""
		ins_login_id.text=""
		ins_login_pw.text=""
		alert(" データ更新が完了しました",'更新完了')
	}
   
	responseHandler.onStatus  = function( stat: Object ):Void {
		alert("サービスエラーです:" + stat.description,'更新できませんでした');
	}
	
	if(ins_login_id.text.length==0){
		alert('IDは必須です','入力項目を確認してください')
	} else if(ins_login_pw.text.length==0){
		alert('パスワードは必須です','入力項目を確認してください')
	} else {
		alert("名前：" + ins_login_nm.text + "\r" + "ⅠＤ：" + ins_login_id.text + "\r"  + "ＰＷ：" + ins_login_pw.text + "\r\r" + "のデータを登録します。宜しいですか", "　　　データ登録　　　", 
		mx.controls.Alert.OK | mx.controls.Alert.CANCEL,  confirm);
	}
</cfsavecontent>
	
<!---

 --->
<cfif IsDefined("form.getSearch")>
	<cfset searchflg=true>
</cfif>
<!--- 予約ステータスマスタ --->
<cfinvoke component="members" method="getMembers" returnVariable="q_rmember">
	<cfinvokeargument name="login_nm" value="#form.login_nm#">
	<cfinvokeargument name="login_id" value="#form.login_id#">
	<cfinvokeargument name="resort_code" value="#form.resort_code#">
</cfinvoke>
		
<cfsilent>

</cfsilent>
<cfif len(errormsg) neq 0>
	<cfoutput><font color="##FF0000">#errormsg#</font></cfoutput>
</cfif>
<html lang="ja">
<head>
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<TITLE>-+ CRMKitchen 総合アカウント管理 +-</TITLE>
</head>
<body text="##333333" bgcolor="##ffffff" link="##cc3300" alink="##cc3300" vlink="##cc3300" topmargin="0" leftmargin="0" marginheight="0" marginwidth="0">
<cfform name="m_reserve" action="index.cfm" method="post" format="Flash" skin="haloBlue" onload="setCellRenderer();" width="1000" style="fontfamily:Verdana; borderSyle:solid; fontSize:12; ">
	<cfformitem type="script">
	function setCellRenderer(){
		g_member.getColumnAt(5).cellRenderer = cbCell;	
		//g_member.getColumnAt(0).width = 60;
		if(#searchflg#==true){
			alert(#q_rmember.recordcount#+"件です",'検索結果')
		}
	}
	</cfformitem>
	<cfformitem type="spacer" height="10"/>
	<cfformgroup type="TabNavigator" name="tnav" id="tnav" width="900" height="540"
		style="headerColors:##FFFF00, ##FF0000;"
		>
		<!--- ア　カ　ウ　ン　ト　検　索 --->
		<cfformgroup type="Page" label="　ア　カ　ウ　ン　ト　検　索　"  width="900" >
			<cfformgroup type="vertical">
				<cfformitem type="spacer" height="4"/>
				<!--- 検索項目 --->
				<cfformgroup type="horizontal" width="100%">
					<cfinput type="text" name="login_nm" value="#form.login_nm#" size="12" maxlength="12" label = "名　前">
					<cfinput type="text" name="login_id" value="#form.login_id#" size="12" maxlength="12" label = "Ⅰ　Ｄ">
					<!--- 所属一覧 --->
					<cfinvoke component="members" method="getresortlist" returnVariable="m_resort">
					</cfinvoke>
					<cfselect name = "resort_code" width="124" label="所属">
						<option value=""></option>
						<cfloop query="m_resort">
							<option value="#resort_code#" <cfif form.resort_code eq resort_code>selected</cfif>>#resort_nm#</option>
						</cfloop>
					</cfselect>
				</cfformgroup>
	
				<cfformitem type="hrule"/>
				<cfformitem type="spacer" height="4"/>

				<!--- ボタン --->
				<cfformgroup type="horizontal" width="840">
					<cfinput type="button" name="getSearch" value=" 検 索 " width="60" onClick="#getData#">
					<cfinput type="reset" name="getReset" value=" クリア " width="60" >
					<cfformitem type="spacer" width="40"/>
					<cfinput type="button" name="gridEntered" value=" 権限設定 " width="80" onClick="#updData#">
					<cfinput type="button" name="getEntry" value="選択行データ変更 " width="120" onClick="#setData#">
					<cfinput type="button" name="rowdel" value=" 選択行データ削除 " width="120" onClick="#delData#">
					<cfinput type="button" name="newEntry" value=" 新規登録" width="120" onClick="tnav.selectedIndex=1;ins_login_nm.text='';ins_login_id.text='';ins_login_pw.text='';ins_login_id.enabled=true;">
				</cfformgroup>
				<cfformitem type="spacer" height="4"/>

				<!--- グリッド --->
				<cfformgroup type="horizontal" width="640" align="left">
					<cfformitem type="spacer" width="60"/>
					<cfgrid name = "g_member" format="Flash" height = "332"  query="q_rmember"
						selectMode = "Edit" autoWidth="yes" width="100%"
						selectColor = "##99cccc"
						font="Verdana"
						fontSize="12" 
						insert = "no" delete = "no" gridLines = "yes" rowHeight = "28" colHeaders = "yes">
						<cfgridcolumn name = "login_id" header = "ログインID" width="100" select="no">
						<cfgridcolumn name = "login_pw" header = "パスワード" width="100" select="no">
						<cfgridcolumn name = "login_nm" header = "名前" width="120" select="no">
						<cfgridcolumn name = "resort_nm" header = "所属" width="120" select="no">
						<cfgridcolumn name = "login_dv" header = "権限" width="120" select="yes">
						<cfgridcolumn name = "resort_code"  display="no" select="no">
					</cfgrid>
				</cfformgroup>
			</cfformgroup>
		</cfformgroup>
		<!--- ア　カ　ウ　ン　ト　検　索 --->
		<!--- 新　規　登　録 --->
		<cfformgroup type="Page" label="　編　集　"  width="100%" >
			<cfformgroup type="vertical">
				<cfformitem type="spacer" height="10"/>
				<!--- 登録項目 --->
				<cfformgroup type="vbox">
					<!--- 所属一覧 --->
					<cfinvoke component="members" method="getresortlist" returnVariable="m_resort">
					</cfinvoke>
					<cfselect name = "ins_resort_code" width="124" label="所属" onChange="#changedv#">
						<cfloop query="m_resort">
							<option value="#resort_code#" <cfif form.ins_resort_code eq resort_code>selected</cfif>>#resort_nm#</option>
						</cfloop>
					</cfselect>
					<cfinput type="text" name="ins_login_nm" value="#form.ins_login_nm#" size="24" maxlength="24" label = "　名　前　 ">
					<cfinput type="text" name="ins_login_id" value="#form.ins_login_id#" size="24" maxlength="24" label = "　Ⅰ　Ｄ　  " required="Yes">
					<cfinput type="text" name="ins_login_pw" value="#form.ins_login_pw#" size="24" maxlength="24" label = "パスワード" required="Yes">
					<cfselect name = "ins_login_dv" width="124" label="権限"></cfselect>
				</cfformgroup>
				<!--- ボタン --->
				<cfformgroup type="horizontal">
					<cfinput type="button" name="setMember" value=" 登　録 " width="80" onClick="#editMember#">
					<cfinput type="reset" name="getins_Reset" value=" クリア " width="80" onClick="ins_login_id.enabled=true">
				</cfformgroup>
			</cfformgroup>
		</cfformgroup>
		<!--- 新　規　登　録 --->

	</cfformgroup>
</cfform>
<cfcatch>
	<cftransaction action="rollback"/>
	<cfabort>
</cfcatch>
</cftry>
</body>
</html>
</cfoutput>
</cfprocessingdirective>