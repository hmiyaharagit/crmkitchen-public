<cfcomponent>
	<cffunction name="login_check" access="public" returntype="boolean">
		<cfargument name="username" type="string" required="yes">
		<cfargument name="password" type="string" required="yes">

		<cfquery name="login_check" datasource="#Application.datasource#">
			select
				*
			from
				#Application.app00.dbu#.account_manage
			where
				login_id =<cfqueryparam value='#username#' CFSQLTYPE='CF_SQL_VARCHAR'>
			and
				login_pw =<cfqueryparam value='#password#' CFSQLTYPE='CF_SQL_VARCHAR'>
			and
				active_flg = 1
		
		</cfquery>

	   <cfif login_check.recordcount eq 1>
			<cfset loginFailed = false>
		<cfelse>
			<cfset loginFailed = true>
		</cfif>
		<cfreturn loginFailed>
	</cffunction>
	
	<cffunction name="xss_check" access="public" returntype="string">
		<cfargument name="chk_str" type="string" required="yes">
			
		<cfset chk_str = replace(chk_str,"&","&amp;","all")>
		<cfset chk_str = replace(chk_str,"\","&yen;","all")>
		<cfset chk_str = replace(chk_str,"'","&rsquo;","all")>
		<cfset chk_str = replace(chk_str,"<","&lt;","all")>
		<cfset chk_str = replace(chk_str,">","&gt;","all")>

		<cfreturn chk_str>
	</cffunction>
</cfcomponent>