<cfcomponent name="flashRemotingResponder" access="public" description="Responds to Flash remoting requests">
<cffunction name="getMembers" output="false" description="Returns a query with names (id,name,age,gender)" access="remote" returntype="query">
	<cfargument name="login_nm" default="" type="string">
	<cfargument name="login_id" default="" type="string">
	<cfargument name="resort_code" default="" type="string">

	<cfquery name="q_rmember" datasource = "#application.datasource#">
		select	 *
		from	 #Application.app00.dbu#.crm_userslist as a
		where	 0 = 0
		<cfif len(arguments.login_nm) neq 0>
		and		 a.login_nm like '%#arguments.login_nm#%'
		</cfif>
		<cfif len(arguments.login_id) neq 0>
		and		 a.login_id like '%#arguments.login_id#%'
		</cfif>
		<cfif len(arguments.resort_code) neq 0>
		and		 a.resort_code = #arguments.resort_code#
		</cfif>
		order by a.seq
				,a.login_id
	</cfquery>
	<cfreturn q_rmember /> 
</cffunction>
	
	
<cffunction name="getresortlist" output="false" description="" access="remote" returntype="query">
	<cfquery name="q_rmember" datasource = "#application.datasource#">
		select distinct
				 resort_code
				,resort_nm
				,seq from (
	 				select	 resort_code
							,resort_nm
							,resort_code * 100 as seq
					from	#Application.app00.dbu#.crm_allusers
					group by resort_code
							,resort_nm
							,resort_code * 100
					union all
					select	 999 as resort_code
							,'CRMKitchen' as resort_nm
							,-1 as seq
					union all
					select	 resort_code
							,resort_nm
							,resort_code * 100 as seq
					from	 #Application.app00.dbu#.mst_resort
					group by resort_code
							,resort_nm
							,resort_code * 100
					) wk_a
		 order by wk_a.seq
	</cfquery>
	<cfreturn q_rmember /> 
</cffunction>	
	
<cffunction name="setMembers" output="false" description="" access="remote" returntype="any">
	<cfargument name="members" default="" type="any">
	<cftry>
		<cftransaction action="begin">
			<!--- データファイル読込 --->
			<cfinclude template="inc_setting.cfm">
			<cfloop index="i" from="1" to="#ArrayLen(arguments.members)#">
				<!--- データソース切替 --->
				<cfquery dbtype="query" name="q_logintable">
					select dsn,application_nm from q_resort
					<cfif arguments.members[i].resort_code neq 0>
					where resort_code = #arguments.members[i].resort_code#
					<cfelse>
					where application_nm = 'crmpublic'
					</cfif>
				</cfquery>
				<cfloop query="q_logintable">
					<!--- --->
					<cfif arguments.members[i].resort_code neq 999>
						<cfquery datasource="#application.datasource#" name="u_m_login" result="deb_1">
							update #dsn#m_loginuser set
								 login_dv =  '#arguments.members[i].login_dv#'
							where login_id = '#arguments.members[i].login_id#'
							<cfif application_nm eq "crmpublic">
							and	  resort_code = #arguments.members[i].resort_code#
							</cfif>
						</cfquery>
					 <cfelse>
						<!--- CRM全権限 トマムは型変更させる --->
						<cfif application_nm eq "tomamu">
							<cfset arguments.members[i].login_dv = 2>
						</cfif>
						<cfquery datasource="#application.datasource#" name="u_m_login" result="deb_1">
							update #dsn#m_loginuser set
								 login_dv =  '#arguments.members[i].login_dv#'
							where login_id = '#arguments.members[i].login_id#'
							<cfif application_nm eq "crmpublic">
							and	  resort_code = #arguments.members[i].resort_code#
							</cfif>
						</cfquery>
					 </cfif>
				</cfloop>
			</cfloop>
		</cftransaction>
		<cfcatch>
			<cfreturn  cfcatch.detail/>
		</cfcatch>
	</cftry>
	<cfreturn  />
</cffunction>



<cffunction name="editMember" output="false" description="" access="remote" returntype="any">
	<cfargument name="login_nm" default="" type="string">
	<cfargument name="login_id" default="" type="string">
	<cfargument name="login_pw" default="" type="string">
	<cfargument name="resort_code" default="" type="string">
	<cfargument name="login_dv" default="" type="string">
	
	<cfif arguments.resort_code neq 0 and arguments.resort_code neq 999>
		<cfinvoke method="editMember_nonconnectedition" component="members_nonconnectedition" 
			login_nm="#arguments.login_nm#"
			login_id="#arguments.login_id#"
			login_pw="#arguments.login_pw#"
			resort_code = #arguments.resort_code#
			login_dv="#arguments.login_dv#"
			returnVariable="q_member">
			<cfreturn q_member>
			<cfabort>
	</cfif>
	<cfset retval=false>
	<cftry>
		<cftransaction action="begin">

			<!--- データファイル読込 --->
			<cfinclude template="inc_setting.cfm">
			<!--- 削除処理 --->
			<cfquery dbtype="query" name="q_dsn">
				select dsn from q_resort group by dsn
			</cfquery>
			<cfloop query="q_dsn">
				<cfquery datasource="#application.datasource#" name="q_login" result="deb_1">
					delete from #dsn#m_loginuser
					where login_id = '#arguments.login_id#'
				</cfquery>
			</cfloop>

			<!--- データソース切替 --->
			<cfquery dbtype="query" name="q_logintable">
				select dsn,application_nm from q_resort 
				<cfif arguments.resort_code eq 0>
				where application_nm = 'crmpublic'
				</cfif>
				group by dsn,application_nm
			</cfquery>
				
			<!--- 全権ユーザー対応 --->	
			<cfif arguments.resort_code eq 999>
				<cfset arguments.resort_code = 0>
			</cfif>

			<cfset wk_login_dv = "s">
			<cfloop query="q_logintable">
				<!--- CRM全権限 トマムは型変更させる --->
				<cfif application_nm eq "tomamu">
					<cfset wk_login_dv = 2>
				</cfif>
				<cfquery datasource="#application.datasource#" name="q_login" result="deb_1">
					select * from #dsn#m_loginuser
					where login_id = '#arguments.login_id#'
					<cfif application_nm eq "crmpublic">
					and	  resort_code = #arguments.resort_code#
					</cfif>
				</cfquery>
				<cfif q_login.recordcount eq 0>
					<cfquery datasource="#application.datasource#" name="u_m_login" result="deb_1">
						insert into #dsn#m_loginuser (
							 login_id
							,login_pw
							,login_nm
							,login_dv
							<cfif application_nm eq "crmpublic">
							,resort_code
							</cfif>
						)values(
							 '#arguments.login_id#'
							,'#arguments.login_pw#'
							,'#arguments.login_nm#'
							<cfif application_nm eq "tomamu">
							, #wk_login_dv# 
							<cfelse>
							,'#wk_login_dv#'
							</cfif>
							<cfif application_nm eq "crmpublic">
							,#arguments.resort_code#
							</cfif>				
						)
					</cfquery>
				<cfelse>
					<cfquery datasource="#application.datasource#" name="u_m_login" result="deb_1">
						update #dsn#m_loginuser set
							 login_id =  '#arguments.login_id#'
							,login_pw =  '#arguments.login_pw#'
							,login_nm =  '#arguments.login_nm#'
							,login_dv =  '#wk_login_dv#'
						where login_id = '#arguments.login_id#'
						<cfif application_nm eq "crmpublic">
						and	  resort_code = #arguments.resort_code#
						</cfif>
					</cfquery>
					<cfset retval=true>
				</cfif>
			</cfloop>
		</cftransaction>
		<cfcatch><cfabort>
			<cfreturn  cfcatch.detail/>
		</cfcatch>
	</cftry>
	<cfreturn  />
</cffunction>	
				
				


<cffunction name="delMember" output="false" description="" access="remote" returntype="any">
	<cfargument name="login_id" default="" type="string">
	<cfargument name="resort_code" default="" type="string">

	
	<cfif arguments.resort_code neq 0 and arguments.resort_code neq 999>
		<cfinvoke method="delMember_nonconnectedition" component="members_nonconnectedition" 
			login_id="#arguments.login_id#"
			returnVariable="q_member">
			<cfreturn q_member>
			<cfabort>
	</cfif>	
	<cftry>
		<cftransaction action="begin">
			<!--- データファイル読込 --->
			<cfinclude template="inc_setting.cfm">
			<!--- データソース切替 --->
			<cfquery dbtype="query" name="q_logintable">
				select dsn,application_nm from q_resort 
				group by dsn,application_nm
			</cfquery>
			<cfloop query="q_logintable">
				<cfquery datasource="#application.datasource#" name="u_m_login" result="deb_1">
					delete #dsn#m_loginuser
					where login_id = '#arguments.login_id#'
					<cfif application_nm eq "crmpublic">
					and	  resort_code = 0
					</cfif>
				</cfquery>
			</cfloop>
		</cftransaction>
		<cfcatch>
			<cfreturn  cfcatch.detail/>
		</cfcatch>
	</cftry>
	<cfreturn  />
</cffunction>
</cfcomponent>