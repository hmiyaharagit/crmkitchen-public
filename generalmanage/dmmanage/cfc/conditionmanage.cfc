﻿<cfcomponent hint="">
	<!---
	*************************************************************************************************
	* Class				: 
	* Method			: getresort
	* Description		: 
	* Custom Attributes	: none
	* 					: 
	* Return Paramters	: retQuery(Query)
	* History			: 1) Coded by	2009/06/12 Miyahara
	*					: 2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getresort" access="remote" returntype="any">
		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<!--- 施設一覧取得 --->
			<cfquery name="m_resort" datasource = "#Application.datasource#">
				select	 0 as data
						,'選択してください' as label
						,'' as dsn
						,'' dschema
						,0 as seq
				from	mst_resort
				union 
				select	 a.resort_code as data
						,a.resort_nm as label
						,a.dsn
						,a.dschema
						,a.seq
				from	 mst_resort a
				order by seq
			</cfquery>
			<cfreturn m_resort>
			<cfsetting enablecfoutputonly="No">
			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	<!---
	*************************************************************************************************
	* Class				: 
	* Method			: getmaster
	* Description		: 
	* Custom Attributes	: none
	* 					: 
	* Return Paramters	: retQuery(Query)
	* History			: 1) Coded by	2009/06/12 Miyahara
	*					: 2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getmaster" access="remote" returntype="any">
		<cfargument name="dsn" type="String" default="">
		<cfargument name="scm" type="String" default="">
		<cfargument name="rcd" type="Numeric" default="">


		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<!--- 戻値クエリ作成 --->
			<cfset retQuery = QueryNew("controldiv_id,controldiv_nm, control_nm")>
			<!--- 未送信対象データ一覧 --->
			<cfquery name="m_control" datasource = "#arguments.dsn#">
				select	  controldiv_id
						 ,controldiv_nm
						 ,control_id
						 ,control_nm
						 ,seq
				from	 #arguments.scm#.mst_control
				where	 except_flg = 1
				<cfif arguments.dsn eq "crmpublic">
				and		 resort_code = #arguments.rcd#
				</cfif>
				order by controldiv_id,seq
			</cfquery>
			<cfif m_control.recordcount gt 0>
				<cfquery name="m_controldiv" dbtype = "query">
					select	 controldiv_id
							,controldiv_nm
					from	 m_control
					group by controldiv_id,controldiv_nm
					order by controldiv_id
				</cfquery>

				<cfloop query="m_controldiv">
					<cfset Queryaddrow(retQuery,1)>
					<cfquery name="m_controldiv" dbtype = "query">
						select	 control_nm
						from	 m_control
						where	 controldiv_id = #controldiv_id#
						order by seq
					</cfquery>
					<cfset control_nm_list = valuelist(m_controldiv.control_nm)>
		 			<cfset QuerySetCell(retQuery, "controldiv_id", #controldiv_id#)>
		 			<cfset QuerySetCell(retQuery, "controldiv_nm", #controldiv_nm#)>
		 			<cfset QuerySetCell(retQuery, "control_nm", #control_nm_list#)>
				</cfloop>
			</cfif>

			<cfreturn retQuery>
			<cfsetting enablecfoutputonly="No">
			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	<!---
	*************************************************************************************************
	* Class				: 
	* Method			: getmaster
	* Description		: 
	* Custom Attributes	: none
	* 					: 
	* Return Paramters	: retQuery(Query)
	* History			: 1) Coded by	2009/06/12 Miyahara
	*					: 2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getcontorl" access="remote" returntype="any">
		<cfargument name="dsn" type="String" default="">
		<cfargument name="scm" type="String" default="">
		<cfargument name="rcd" type="Numeric" default="">
		<cfargument name="controldiv_id" type="Numeric" default="">


		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<!--- 戻値クエリ作成 --->
			<cfset retStruct = StructNew()>

			<!--- 未送信対象データ一覧 --->
			<cfquery name="m_control" datasource = "#arguments.dsn#">
				select	  control_id
						 ,control_nm
						 ,except_flg
				from	 #arguments.scm#.mst_control
				where	 controldiv_id = #arguments.controldiv_id#
				and		 active_flg = 1
				<cfif arguments.dsn eq "crmpublic">
				and		 resort_code = #arguments.rcd#
				</cfif>
				order by control_id
			</cfquery>


			<cfquery name="exist_control" datasource = "#arguments.dsn#">
				select	  controldiv_id
				from	 #arguments.scm#.mst_control
				where	 except_flg = 1
				<cfif arguments.dsn eq "crmpublic">
				and		 resort_code = #arguments.rcd#
				</cfif>
				group by controldiv_id
			</cfquery>


			<cfquery name="m_controldiv" datasource = "#arguments.dsn#">
				<cfif arguments.controldiv_id eq 0>
				select	 0 as data
						,'選択してください' as label
				union
				</cfif>
				select	  controldiv_id as data
						 ,controldiv_nm as label
				from	 #arguments.scm#.mst_control
				where	 0=0
				<cfif arguments.controldiv_id neq 0>
				and		 controldiv_id = #arguments.controldiv_id#
				<cfelse>
					<cfif exist_control.recordcount gt 0>
						and		 controldiv_id not in (#valuelist(exist_control.controldiv_id)#)
					</cfif>
				</cfif>
				<cfif arguments.dsn eq "crmpublic">
				and		 resort_code = #arguments.rcd#
				</cfif>
				group by controldiv_id,controldiv_nm
			</cfquery>

			<cfset StructInsert(retStruct, "q_control", #m_control#)>
			<cfset StructInsert(retStruct, "q_controldiv", #m_controldiv#)>

			<cfreturn retStruct>
			<cfsetting enablecfoutputonly="No">
			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<!---
	*************************************************************************************************
	* Class				: 
	* Method			: getmaster
	* Description		: 
	* Custom Attributes	: none
	* 					: 
	* Return Paramters	: retQuery(Query)
	* History			: 1) Coded by	2009/06/12 Miyahara
	*					: 2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="setcontorl" access="remote" returntype="any">
		<cfargument name="dsn" type="String" default="">
		<cfargument name="scm" type="String" default="">
		<cfargument name="rcd" type="Numeric" default="">
		<cfargument name="controldiv_id" type="Numeric" default="">
		<cfargument name="dp" type="Array" default="">

		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<!--- 戻値クエリ作成 --->
			<cfset retStruct = StructNew()>
			<cfloop from="1" to="#ArrayLen(arguments.dp)#" index="i">

				<cfset wk_except_flg = 0>
				<cfif arguments.dp[i].check is "YES">
					<cfset wk_except_flg = 1>
				</cfif>

				<cfquery name="u_control" datasource = "#arguments.dsn#">
					update	 #arguments.scm#.mst_control
					set		 except_flg = #wk_except_flg#
					where	 controldiv_id = #arguments.controldiv_id#
					and		 control_id = '#arguments.dp[i].control_id#'
					<cfif arguments.dsn eq "crmpublic">
					and		 resort_code = #arguments.rcd#
					</cfif>
				</cfquery>
			</cfloop>
			<cfreturn true>
			<cfsetting enablecfoutputonly="No">
			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<!---
	*************************************************************************************************
	* Class				: 
	* Method			: getmaster
	* Description		: 
	* Custom Attributes	: none
	* 					: 
	* Return Paramters	: retQuery(Query)
	* History			: 1) Coded by	2009/06/12 Miyahara
	*					: 2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="resetcontorl" access="remote" returntype="any">
		<cfargument name="dsn" type="String" default="">
		<cfargument name="scm" type="String" default="">
		<cfargument name="rcd" type="Numeric" default="">
		<cfargument name="controldiv_id" type="Numeric" default="">

		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<!--- 戻値クエリ作成 --->
			<cfset retStruct = StructNew()>
			<cfquery name="u_control" datasource = "#arguments.dsn#">
				update	 #arguments.scm#.mst_control
				set		 except_flg = 0
				where	 controldiv_id = #arguments.controldiv_id#
				<cfif arguments.dsn eq "crmpublic">
				and		 resort_code = #arguments.rcd#
				</cfif>
			</cfquery>
			<cfreturn true>
			<cfsetting enablecfoutputonly="No">
			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>


	<!---
	*************************************************************************************************
	* Class				: 
	* Method			: getmaster
	* Description		: 
	* Custom Attributes	: none
	* 					: 
	* Return Paramters	: retQuery(Query)
	* History			: 1) Coded by	2009/06/12 Miyahara
	*					: 2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="sethistory" access="remote" returntype="any">
		<cfargument name="dsn" type="String" default="">
		<cfargument name="scm" type="String" default="">
		<cfargument name="rcd" type="Numeric" default="">
		<cfargument name="controldiv_id" type="Array" default="">

		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<!--- キー生成 --->
			<cfset wk_condition_id = createuuid()>

			<!--- 過去データ締 --->
			<cfquery name="u_unsendcondition" datasource = "#arguments.dsn#">
				update	 #arguments.scm#.hst_unsendcondition
				set		 end_date = getdate()
				where	 end_date is null
				<cfif arguments.dsn eq "crmpublic">
				and		 resort_code = #arguments.rcd#
				</cfif>
			</cfquery>

			<cfloop from="1" to="#ArrayLen(arguments.controldiv_id)#" index="i">
				<cfquery name="g_control" datasource = "#arguments.dsn#">
					select	 controldiv_id
							,control_id
							,resort_code
					from	 #arguments.scm#.mst_control
					where	 controldiv_id = #arguments.controldiv_id[i]#
					and		 except_flg = 1
					<cfif arguments.dsn eq "crmpublic">
					and		 resort_code = #arguments.rcd#
					</cfif>
				</cfquery>
				<cfloop query="g_control">
					<cfquery name="u_control" datasource = "#arguments.dsn#">
						insert into #arguments.scm#.hst_unsendcondition(
							 condition_id
							,controldiv_id
							,control_id
							,resort_code
							,start_date
							,end_date
						) values (
							 '#wk_condition_id#'
							,#g_control.controldiv_id#
							,'#g_control.control_id#'
							,#g_control.resort_code#
							,getdate()
							,null
						)
					</cfquery>
				</cfloop>
			</cfloop>
			<!--- 戻値設定 --->		
			<cfscript>
				executeinformation = StructNew();
				StructInsert(executeinformation, "headertxt",	"履歴登録完了");
				StructInsert(executeinformation, "messagetxt",	"履歴を登録しました。");
			</cfscript>
			<cfreturn executeinformation>
			<cfsetting enablecfoutputonly="No">
			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	<!---
	*************************************************************************************************
	* Class				: 
	* Method			: getmaster
	* Description		: 
	* Custom Attributes	: none
	* 					: 
	* Return Paramters	: retQuery(Query)
	* History			: 1) Coded by	2009/06/12 Miyahara
	*					: 2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="outputhistory" access="remote" returntype="any">
		<cfargument name="dsn" type="String" default="">
		<cfargument name="scm" type="String" default="">
		<cfargument name="rcd" type="Numeric" default="">

		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<cfset rootpath="/#listgetat(cgi.SCRIPT_NAME,1,'/')#/generalmanage/dmmanage/">
			<cfset outputDirectory = "#rootpath#csvfiles/">

			<!--- 設定履歴 --->
			<cfquery name="u_unsendcondition" datasource = "#arguments.dsn#">
				select	 a.condition_id
						,b.controldiv_nm
						,b.control_nm
						,isnull(convert(varchar,a.start_date,111),'')  + '~' + isnull(convert(varchar,a.end_date,111),'') as term
				from	 #arguments.scm#.hst_unsendcondition as a
						,#arguments.scm#.mst_control as b 
				where	 a.controldiv_id = b.controldiv_id
				and		 a.control_id = b.control_id
				<cfif arguments.dsn eq "crmpublic">
				and		 a.resort_code = #arguments.rcd#
				</cfif>
			</cfquery>

			<!--- ディレクトリ＆ファイル情報 --->
			<cfset filePath = ExpandPath(#outputDirectory#)>
			<cfset Dircheck = DirectoryExists( filePath & "\" & #Replace(cgi.remort_addr,".","","all")# )>
			
			<!--- ディレクトリ存在チェック --->
			<cfif Dircheck eq "no">
				<cfdirectory action = "create" directory = "#filePath#\#Replace(cgi.remort_addr,'.','','all')#">
			</cfif>
			
			<cfset UserDir = #Replace(cgi.remort_addr,".","","all")# & "\">
			<cfset datepart = #DateFormat(Now(),'YYMMDD')# & #TimeFormat(Now(),'HHMMSS')#>
			<cfset filename = filePath & UserDir & datepart & "list.csv">
			<cfset filecheck = FileExists( filename )>
			
			<!--- ディレクトリ内ファイル数チェック --->
			<cfdirectory 
				directory="#filePath#\#Replace(cgi.remort_addr,'.','','all')#" 
				name="myCSVDirectory"
				sort="DATELASTMODIFIED ASC">
				
			<!--- 指定ファイル数以上は作らない。古いものから削除 --->
			<cfset FileHistory = 10>
			<cfif myCSVDirectory.RecordCount GTE FileHistory>
				<cffile action = "delete" file = "#filePath##UserDir##myCSVDirectory.Name#">
			</cfif>	
		
			<!--- ファイル存在チェック --->
			<cfif filecheck eq "no">
				<cfset condData = " 設定履歴 ">	
				<cfset captionData = condData & "設定期間" & "," & "設定区分" & "," & "項目">	
				<cffile action = "write" file=#filename# output=#captionData#>
				<!--- 出力内容整形 --->
				<cfloop query="u_unsendcondition">
					<cfset outputData = 
						#term# & "," & #controldiv_nm# & "," & #control_nm#>
					<cffile action = "append" file=#filename# output=#outputData#>
				</cfloop>
			</cfif>

			<!--- 戻値設定 --->	
			<cfscript>
				executeinformation = StructNew();
				StructInsert(executeinformation, "filename", "http://#cgi.server_name#/#outputDirectory#" & #Replace(UserDir,"\","/","all")# & datepart & "list.csv" );
			</cfscript>
			<cfreturn executeinformation>
			<cfsetting enablecfoutputonly="No">
			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>



</cfcomponent>