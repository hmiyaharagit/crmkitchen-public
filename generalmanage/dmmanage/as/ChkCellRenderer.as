﻿package {

	import fl.controls.ButtonLabelPlacement;
	import fl.controls.DataGrid;
	import fl.controls.dataGridClasses.DataGridColumn;
	import fl.controls.listClasses.ListData;
	import fl.controls.listClasses.ICellRenderer;
	import fl.controls.LabelButton;
	import fl.core.UIComponent;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import fl.controls.CheckBox;
	import fl.controls.listClasses.CellRenderer;
	import flash.display.DisplayObject;
	
	import fl.core.InvalidationType;
	import fl.data.DataProvider;
	import flash.system.IME;

	[Style(name="icon", type="Class")]
	[Style(name="upIcon", type="Class")]
	[Style(name="downIcon", type="Class")]
	[Style(name="overIcon", type="Class")]
	[Style(name="disabledIcon", type="Class")]
	[Style(name="selectedDisabledIcon", type="Class")]
	[Style(name="selectedUpIcon", type="Class")]
	[Style(name="selectedDownIcon", type="Class")]
	[Style(name="selectedOverIcon", type="Class")]
	[Style(name="upSkin", type="Class")]
	[Style(name="downSkin", type="Class")]
	[Style(name="overSkin", type="Class")]
	[Style(name="disabledSkin", type="Class")]
	[Style(name="selectedDisabledSkin", type="Class")]
	[Style(name="selectedUpSkin", type="Class")]
	[Style(name="selectedDownSkin", type="Class")]
	[Style(name="selectedOverSkin", type="Class")]
	[Style(name="textFormat", type="flash.text.TextFormat")]
	[Style(name="disabledTextFormat", type="flash.text.TextFormat")]
	[Style(name="textPadding", type="Number", format="Length")]

	public class ChkCellRenderer extends CheckBox implements ICellRenderer {
	
		protected var _listData:ListData;
		protected var _data:Object;
		protected var _rowSelected:Boolean = true;
		protected var _dataField:String;
		
		public function ChkCellRenderer():void {
			super();
			focusEnabled = false;
		}

		private static var defaultStyles:Object = { icon:null,
			upIcon:"CheckBox_upIcon",downIcon:"CheckBox_downIcon",overIcon:"CheckBox_overIcon",
			disabledIcon:"CheckBox_disabledIcon",
			selectedDisabledIcon:"CheckBox_selectedDisabledIcon",
			focusRectSkin:null,
			focusRectPadding:null,
			selectedUpIcon:"CheckBox_selectedUpIcon",selectedDownIcon:"CheckBox_selectedDownIcon",selectedOverIcon:"CheckBox_selectedOverIcon",
			upSkin:"CellRenderer_upSkin",downSkin:"CellRenderer_downSkin",overSkin:"CellRenderer_overSkin",
			disabledSkin:"CellRenderer_disabledSkin",
			selectedDisabledSkin:"CellRenderer_selectedDisabledSkin",
			selectedUpSkin:"CellRenderer_selectedUpSkin",selectedDownSkin:"CellRenderer_selectedDownSkin",selectedOverSkin:"CellRenderer_selectedOverSkin",
			textFormat:null,
			disabledTextFormat:null,

			embedFonts:null,
			textPadding:5 };
		public static function getStyleDefinition():Object { return defaultStyles; }

		override public function setSize(width:Number,height:Number):void {
			super.setSize(width, height);
		}

		public function get listData():ListData {
			return _listData;
		}

		public function set listData(value:ListData):void {
			_listData = value;
			label = _listData.label;
			setStyle("icon", _listData.icon);
		}

		public function get data():Object {
			return _data;
		}
		
		public function set data(value:Object):void {
			_data = value;
			_dataField = value.dataField;
			super.selected = _data[_dataField]
			super.drawNow();
		}
		
		override public function get selected():Boolean {
			return super.selected;
		}
		
		override public function set selected(value:Boolean):void {
			_rowSelected = value;
		}
		
		public function get imeMode():String {
			return _imeMode;
		}
		public function set imeMode(value:String):void {
			_imeMode = value;
		}
		
		override protected function toggleSelected(event:MouseEvent):void {
			( _data.check == "true" || _data.check == 1 || _data.check == true )?	_data.check = false : _data.check = true;
			super.selected = _data[_dataField]
			super.drawNow();
		}

		override protected function drawLayout():void {
			var textPadding:Number = Number(getStyleValue("textPadding"));
			var textFieldX:Number = 0;
			if (icon != null) {
				icon.x = textPadding;
				icon.y = Math.round((height-icon.height)>>1);
				textFieldX = icon.width + textPadding;
			}
			if (label.length > 0) {
				textField.visible = true;
				var textWidth:Number = Math.max(0, width - textFieldX - textPadding*2);
				textField.width = textWidth;
				textField.height = textField.textHeight + 4;
				textField.x = textFieldX + textPadding
				textField.y = Math.round((height-textField.height)>>1);
			} else {
				textField.visible = false;
			}
			background.width = width;
			background.height = height;
		}
	
		override protected function drawBackground():void{
			var styleName:String = (enabled) ? mouseState : "disabled";
			if (_rowSelected) {
				styleName = "selected"+styleName.substr(0,1).toUpperCase()+styleName.substr(1);
			}
			styleName += "Skin";
			var bg:DisplayObject = background;
			background = getDisplayObjectInstance(getStyleValue(styleName));
			addChildAt(background, 0);
			if (bg != null && bg != background) { removeChild(bg); }
		}
	}
}
