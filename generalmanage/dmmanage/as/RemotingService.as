﻿package {
	import flash.net.*;
    import flash.events.*;

	public class RemotingService extends NetConnection {
		public var cfc_classpath:String;		//CFCクラスパス
		public var myXMLLoader:XMLloader;		//定義ファイル

		/**********************************************************************
		/	FLASHRemoting接続確立コンストラクタ
		/
		**********************************************************************/
		public function RemotingService(url) {
			cfc_classpath = "crmpublic.generalmanage.dmmanage.cfc."
			//	--リモート接続先設定
			//objectEncoding = ObjectEncoding.AMF3;

			//	--オブジェクトが送信できないので↓↓AMF0で。要調査
			objectEncoding = ObjectEncoding.AMF0;
			var connectionurl = url
			connect(connectionurl);
		}

	}
}