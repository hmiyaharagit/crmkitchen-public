package 
{
    import fl.controls.*;
	import fl.data.DataProvider;
    import flash.display.Sprite;
    import flash.display.MovieClip;
    import flash.events.*;
    import flash.text.TextFieldAutoSize;
	import flash.net.*; 
	import flash.display.*;
	import flash.text.*;
	import fl.managers.StyleManager;
	import com.yahoo.astra.fl.managers.AlertManager;
	import caurina.transitions.Tweener;
	import ArrayCollectionDP;
	import globalsetting;
	import DownLoader;
	
    public class Conditionmanage extends Sprite
    {
		//public var registerpanel:MovieClip;		//登録用パネル
		private var myService:NetConnection;		//コネクション
		private var cfc_classpath:String;			//CFCクラスパス
//		public var program_cb:ComboBox				//施設選択
		public var myXMLLoader:XMLloader			//定義ファイル
		public var rs:RemotingService				//FlashRemotingObject
		public var panesource:panecontent			//ScrollPaneContent
		public var my_mc:dgCellRendererSample
		public var controldiv_id:Number
		public var delbar:valuebar					//削除対象オブジェクト
		public var fileToDownload:FileReference;
		public var Downloadfile:String;

		/**********************************************************************
		/	予約取得インターフェース
		/
		**********************************************************************/
		public function Conditionmanage() {
			//	--共通スタイル
			var tf:TextFormat = new TextFormat();
			tf.font = "Tahoma";
			StyleManager.setStyle("textFormat", tf);
			stage.focus = this;
			
			controldiv_id = 0
			
			myXMLLoader = new XMLloader("setting.xml");
			myXMLLoader.addEventListener(XMLloader.LOAD_COMPLETE, onXMLloaded);
			
			addChild(program_cb);
		}

		/**********************************************************************
		/	定義情報取得
		/
		**********************************************************************/
		function onXMLloaded(event:Event):void{
			//ロードしたXMLを取得
			var	myXML = myXMLLoader.getXML();
			var connectionurl = myXML.protocol + "://" + myXML.hostaddress + "/flashservices/gateway/"
			//	--リモーティングオブジェクト
			rs =new RemotingService(connectionurl)
			initmanage();
		}


		/**********************************************************************
		/	プログラムコンボボックス設定
		/
		**********************************************************************/
		public function setupProgramcombo():void {
			var responder = new Responder(setupComboDp,onFault);

			//	--マスタデータ取得コンポーネント接続
			rs.call(rs.cfc_classpath + "conditionmanage.getresort",responder );
			
			program_cb.addEventListener(Event.CHANGE, 
				function(me:Event):void{
//					trace(me.target.selectedItem.data + " : " + me.target.selectedItem.dschema)

					var responder = new Responder(getCondition,onFault);
					var args:Object = new Object()
					args.dsn = me.target.selectedItem.dsn
					args.scm = me.target.selectedItem.dschema
					args.rcd = me.target.selectedItem.data
					if(args.rcd != 0){	
						//	--マスタデータ取得コンポーネント接続
						rs.call(rs.cfc_classpath + "conditionmanage.getmaster",responder 
						,args.dsn
						,args.scm
						,args.rcd
						);
					}
				}
			);
		}

		/**********************************************************************
		/	コンボボックスデータソース設定
		/
		**********************************************************************/
		public function getCondition(results):void {
			//	--設定済み条件一覧
			var rinfo:DataProvider = new DataProvider();
			rinfo = ArrayCollectionDP.toDataProvider(results);
			
			//	--Pane割当初期設定
			var start_y = 8
			var start_x = 0
			var mh = 4
			var var_h = 90

			//	スクロールペイン内オブジェクト削除
			var cnt = panesource.numChildren;
			for (var d=0;d<cnt;d++){
				var delobj = panesource.getChildAt(0)
				panesource.removeChild(delobj)
			}
			conditionpane.refreshPane();
//			panesource.removeChild()
			for(var i=0; i<rinfo.length; i++){
				var uniqname = "rec_" + rinfo.getItemAt(i).controldiv_id;
				//trace(rinfo.getItemAt(i).controldiv_nm)
				var seq = i
				var v_value:valuebar = new valuebar();
				v_value.name = uniqname;
				panesource.addChild(v_value);
				panesource.setChildIndex(Sprite(v_value), i);
				var wk_var = panesource.getChildByName(uniqname);
				v_value.y= seq * var_h + (seq*mh);
				v_value.x= start_x;

				v_value.controldiv_nm_txt.text = rinfo.getItemAt(i).controldiv_nm
				v_value.control_nm_txt.text = rinfo.getItemAt(i).control_nm
				v_value.controldiv_id = rinfo.getItemAt(i).controldiv_id

				v_value.editbutton.addEventListener(MouseEvent.CLICK, my_mc.conditionEdit);
				v_value.deletebutton.addEventListener(MouseEvent.CLICK, deleteConfirm);
			}
			conditionpane.update();
		}

		/**********************************************************************
		//	削除確認
		//
		**********************************************************************/
		public function deleteConfirm(event:Event):void{
			delbar = event.target.parent;
			var buttons = new Array();
			buttons.push("OK")
			buttons.push("NO")
			ShowAlerts(this, "条件を全削除します宜しいですか？\r", "送信不可条件設定削除", buttons, delexec);
		}


		
		/**********************************************************************
		//	削除実行
		//
		**********************************************************************/
		public function delexec(event:MouseEvent) {
			if(event.target.name=="OK"){
				var responder = new Responder(delComplete,onFault);
				var args:Object = new Object()
				args.dsn = program_cb.selectedItem.dsn
				args.scm = program_cb.selectedItem.dschema
				args.rcd = program_cb.selectedItem.data
				args.controldiv_id = delbar.controldiv_id

				if(args.rcd != 0){	
					//	--マスタデータ取得コンポーネント接続
					rs.call(rs.cfc_classpath + "conditionmanage.resetcontorl",responder 
					,args.dsn
					,args.scm
					,args.rcd
					,args.controldiv_id
					);
				}
			}
		}

		/**********************************************************************
		//	スクロールペインリフレッシュ
		//
		**********************************************************************/
		function completeHandler(event:Event):void {
		    conditionpane.update();
		}		
		

		/**********************************************************************
		/	コンボボックスデータソース設定
		/
		**********************************************************************/
		public function setupComboDp(results):void {
			program_cb.dataProvider = ArrayCollectionDP.toDataProvider(results);
			program_cb.selectedIndex = 0;
		}

		/**********************************************************************
		/	コンボボックスデータソース設定
		/
		**********************************************************************/
		public function delComplete(results):void {
			var responder = new Responder(getCondition,onFault);
			var args:Object = new Object()
			args.dsn = program_cb.selectedItem.dsn
			args.scm = program_cb.selectedItem.dschema
			args.rcd = program_cb.selectedItem.data
			if(args.rcd != 0){	
				//	--マスタデータ取得コンポーネント接続
				rs.call(rs.cfc_classpath + "conditionmanage.getmaster",responder 
				,args.dsn
				,args.scm
				,args.rcd
				);
			}
		}
		
		

		
		/**********************************************************************
		//	ボタンクリックイベント設定
		//
		**********************************************************************/
		public function sethistoryEvent(event_obj:MouseEvent) {
			if(panesource.numChildren>0){
				var buttons = new Array();
				buttons.push("履歴登録する")
				buttons.push("履歴登録はしない")
				ShowAlerts(this, "現在の設定条件の履歴を登録します。\r宜しいですか？", "履歴登録", buttons, historyexec);			
			}
		}

		
		/**********************************************************************
		//	履歴設定
		//
		**********************************************************************/
		public function historyexec(event:MouseEvent) {
			var controldiv_id:Array = new Array();
			if(event.target.name=="履歴登録"){
				var i:int = 0;
				while(i < panesource.numChildren) {
					var wk_val = panesource.getChildAt(i)
//				    trace(wk_val.controldiv_id);
				    controldiv_id.push(wk_val.controldiv_id)
					i++;
				}

				var responder = new Responder(onResult,onFault);
				var args:Object = new Object()
				args.dsn = program_cb.selectedItem.dsn
				args.scm = program_cb.selectedItem.dschema
				args.rcd = program_cb.selectedItem.data
				args.controldiv_id = controldiv_id
				if(args.rcd != 0){	
					rs.call(rs.cfc_classpath + "conditionmanage.sethistory",responder 
					,args.dsn
					,args.scm
					,args.rcd
					,args.controldiv_id
					);
				}
			
			}
		}

		/**********************************************************************
		/	ボタンイベント設定
		/
		**********************************************************************/
		public function setupButton():void {
			//	ボタン初期設定
			newbtn.enabled = true;
			//	ボタンイベント割当
			newbtn.addEventListener(MouseEvent.CLICK, my_mc.conditionEdit);
			historybtn.addEventListener(MouseEvent.CLICK, outputhistoryEvent);	
		}

		/**********************************************************************
		//	履歴出力
		//
		**********************************************************************/
		public function outputhistoryEvent(event:Event):void{
			var buttons = new Array();
			if(program_cb.selectedIndex == 0){
				buttons.push("OK")
				ShowAlerts(this,"対象施設を選択して下さい", "施設未選択", buttons, null);
			} else {
				var responder = new Responder(onOutputResult,onFault);
				var args:Object = new Object()
				args.dsn = program_cb.selectedItem.dsn
				args.scm = program_cb.selectedItem.dschema
				args.rcd = program_cb.selectedItem.data
				if(args.rcd != 0){	
					rs.call(rs.cfc_classpath + "conditionmanage.outputhistory",responder 
					,args.dsn
					,args.scm
					,args.rcd
					);
				}
			}
		}
		/**********************************************************************
		//	履歴出力
		//
		**********************************************************************/
		public function outputexec(event:MouseEvent) {
			if(event.target.name=="OK"){

				fileToDownload = new FileReference();
				var request:URLRequest = new URLRequest(Downloadfile);
				fileToDownload.download(request);
			}
		}

		/**********************************************************************
		/	onResult
		/
		**********************************************************************/
		public function onOutputResult(result:Object):void {
			Downloadfile = result.filename

			var buttons = new Array();
			buttons.push("OK")
			buttons.push("NO")
			ShowAlerts(this, "これまで条件設定の履歴を出力します。\r宜しいですか？", "条件設定履歴出力", buttons, outputexec);
		}

		/**********************************************************************
		/	ArrayToList
		/
		**********************************************************************/
		public function ArraytoList(arr:Array,obj:Object):String {
			var retarray:Array = new Array();
			for(var i=0; i<arr.length;i++){
				retarray.push(arr[i][obj])
			}
			return retarray.toString()
		}


		/**********************************************************************
		//	アラート表示
		//
		**********************************************************************/
		public function ShowAlerts(container:DisplayObject, message:String, title:String, buttons:Array, callBackFunction:Function):void {
			AlertManager.minWidth = 160
			AlertManager.createAlert(container, message, title, buttons, callBackFunction, null, true);
		}


		/**********************************************************************
		/	onFault
		/
		**********************************************************************/
		public function onFault(fault:Object):void {
			var buttons = new Array();
			buttons.push("OK")
			ShowAlerts(this, fault.description, "正常に登録を行えませんでした", buttons, null);
			trace("onFaultError:" + fault.description);
		}


		/**********************************************************************
		/	onResult
		/
		**********************************************************************/
		public function onResult(result:Object):void {
			var buttons = new Array();
			buttons.push("OK")
			ShowAlerts(this, result.headertxt, result.messagetxt, buttons, null);

		}

		/**********************************************************************
		/	定義情報設定初期処理
		/
		**********************************************************************/
		function initmanage():void{
			//	--コンポーネント読込処理
			setupProgramcombo();

			//	--ScrollPane
			panesource = new panecontent()
			conditionpane.source = panesource;
			conditionpane.addEventListener(Event.COMPLETE, completeHandler);
			conditionpane.setStyle("contentPadding", 8);
			
			my_mc = new dgCellRendererSample(this);
			addChild(my_mc);
//			my_mc.x = (stage.stageWidth - my_mc.width)/2;
//			my_mc.y = (stage.stageHeight - my_mc.height)/2;
			my_mc.x = 1200
			my_mc.y = 50
			//	--ボタンイベント設定
			setupButton();
		}
	}
}
