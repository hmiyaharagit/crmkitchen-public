package {
	import fl.data.DataProvider;

	public class ArrayCollectionDP {
		//	for DataGrid
		public static function toDataProvider(myArrayCollection:Object):DataProvider {
			var values:Array = myArrayCollection.serverInfo.initialData;
			var category:Array = myArrayCollection.serverInfo.columnNames;
			var aArr:Array = new Array();
		
			for (var i:Number=0; i<values.length; i++) {
				aArr[i] = new Object();
				for (var aIndex:* in category) {
					aArr[i][category[aIndex]] = values[i][aIndex];
				}
			}
			var dp:DataProvider = new DataProvider(aArr);
			return dp;
		}

	}
}