﻿package 
{
	import flash.display.Sprite;
    import flash.display.MovieClip;
	import fl.controls.DataGrid;
	import fl.data.DataProvider;
	import fl.events.ListEvent;
	import flash.net.*; 
    import flash.events.*;
	import com.yahoo.astra.fl.managers.AlertManager;
	import caurina.transitions.Tweener;
	import ArrayCollectionDP;
	import globalsetting;
	import flash.geom.Point;
		
	public class dgCellRendererSample extends Sprite{
		var myDP:DataProvider = new DataProvider;
		var myDG:DataGrid = new DataGrid();
		public function dgCellRendererSample(parentobj) {

			//	dg size
			myDG.width = 210;
			myDG.height = 200;
			myDG.x =  6;
			myDG.y = 38;

			myDG.addColumn("control_id");
			myDG.addColumn("control_nm");
			myDG.addColumn("except_flg");
			myDG.getColumnAt(0).headerText = "ID";
			myDG.getColumnAt(0).width = 80;
			myDG.getColumnAt(1).headerText = "項目名称";
			myDG.getColumnAt(1).width = 120;
			myDG.getColumnAt(2).headerText = " ";
			myDG.getColumnAt(2).cellRenderer = ChkCellRenderer;
			addChild(myDG);

			closebtn.addEventListener(MouseEvent.CLICK, conditionClose);
			registbtn.addEventListener(MouseEvent.CLICK, conditionRegist);
			controldiv_cb.addEventListener(Event.CHANGE, 
				function(me:Event):void{
//					trace(me.target.selectedItem.data + " : " + me.target.selectedItem.dschema)
					var rs = parent["rs"]
					var responder = new Responder(setupControldiv,parent["onFault"]);
					var args:Object = new Object()
					args.dsn = parent["program_cb"].selectedItem.dsn
					args.scm = parent["program_cb"].selectedItem.dschema
					args.rcd = parent["program_cb"].selectedItem.data
					args.controldiv_id = me.target.selectedItem.data
					if(args.rcd != 0){	
						//	--マスタデータ取得コンポーネント接続
						rs.call(rs.cfc_classpath + "conditionmanage.getcontorl",responder
						,args.dsn
						,args.scm
						,args.rcd
						,args.controldiv_id
						);
					}
				}
			);
		}
		
		/**********************************************************************
		//	編集ウインドウ表示
		//
		**********************************************************************/
		public function conditionEdit(event:Event):void{
			var rs = parent["rs"]
			var responder = new Responder(setupControldiv,parent["onFault"]);
			//	--マスタデータ取得コンポーネント接続
			var args:Object = new Object()
			args.dsn = parent["program_cb"].selectedItem.dsn
			args.scm = parent["program_cb"].selectedItem.dschema
			args.rcd = parent["program_cb"].selectedItem.data
			args.controldiv_id = 0
			if(event.target.parent.controldiv_id!=undefined){
				args.controldiv_id = event.target.parent.controldiv_id
			}
			
			if(parent["program_cb"].selectedIndex == 0){
				var buttons = new Array();
				buttons.push("OK")
				parent["ShowAlerts"](parent,"対象施設を選択して下さい", "施設未選択", buttons, null);
			} else {
/*
				trace(args.dsn)
				trace(args.scm)
				trace(args.rcd)
				trace(args.controldiv_id)
*/
				rs.call(rs.cfc_classpath + "conditionmanage.getcontorl",responder
				,args.dsn
				,args.scm
				,args.rcd
				,args.controldiv_id
				);
				
				//this.enabled = this.visible = true;
				//this.x = 720;	//event.target.x + event.target.width
				//this.y = 50;	//event.target.y
				Tweener.addTween(this, {x:720, transition:"liner", time:1.5} ); 
			}
		}
		
		/**********************************************************************
		//	編集ウインドウクローズ
		//
		**********************************************************************/
		public function conditionClose(event:Event):void{
			Tweener.addTween(this, {x:1200, transition:"liner", time:0.5} ); 
		}
		
		/**********************************************************************
		//	内容更新
		//
		**********************************************************************/
		public function conditionRegist(event:Event):void{
/*
			for( var c=0; c<myDG.dataProvider.length; c++ ){
				trace(myDG.getItemAt(c).control_id + "::" + myDG.getItemAt(c).check)
			}
*/

			var rs = parent["rs"]
			var responder = new Responder(regCompelte,parent["onFault"]);
			//	--マスタデータ取得コンポーネント接続
			var args:Object = new Object()
			args.dsn = parent["program_cb"].selectedItem.dsn
			args.scm = parent["program_cb"].selectedItem.dschema
			args.rcd = parent["program_cb"].selectedItem.data
			args.controldiv_id = event.target.parent.controldiv_cb.selectedItem.data
			args.dp = myDG.dataProvider.toArray();
			rs.call(rs.cfc_classpath + "conditionmanage.setcontorl",responder
			,args.dsn
			,args.scm
			,args.rcd
			,args.controldiv_id
			,args.dp
			);
		}



		/**********************************************************************
		/	コンボボックスデータソース設定
		/
		**********************************************************************/
		public function regCompelte(results):void {
			var buttons = new Array();
			buttons.push("履歴登録")
			buttons.push("終　　了")
			parent["ShowAlerts"](parent, "条件設定が完了しました。\r現在の条件設定を履歴に残しますか？", "条件設定完了", buttons, parent["historyexec"]);
			var rs = parent["rs"]
			var responder = new Responder(parent["getCondition"],parent["onFault"]);
			var args:Object = new Object()
			args.dsn = parent["program_cb"].selectedItem.dsn
			args.scm = parent["program_cb"].selectedItem.dschema
			args.rcd = parent["program_cb"].selectedItem.data

			if(args.rcd != 0){	
				//	--マスタデータ取得コンポーネント接続
				rs.call(rs.cfc_classpath + "conditionmanage.getmaster",responder
				,args.dsn
				,args.scm
				,args.rcd
				);
			}
		}

		/**********************************************************************
		/	コンボボックスデータソース設定
		/
		**********************************************************************/
		public function setupControldiv(results):void {
			//	--controldiv
			controldiv_cb.dataProvider = ArrayCollectionDP.toDataProvider(results.q_controldiv);

			//	--control
			var wkDP:DataProvider = ArrayCollectionDP.toDataProvider(results.q_control);
			var myDP:DataProvider = new DataProvider();
			for( var i=0; i<wkDP.length; i++ ){
				var wkflg = false
				if(wkDP.getItemAt(i).except_flg==1){
					wkflg = true
				}
				myDP.addItem({	 control_id:wkDP.getItemAt(i).control_id
								,control_nm:wkDP.getItemAt(i).control_nm
								,check:wkflg, dataField:"check" });
			}
			myDG.dataProvider = myDP
		}

	}
}
