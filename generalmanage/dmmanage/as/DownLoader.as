﻿package {
    import flash.net.FileReference;
    import flash.net.URLRequest;

    public class DownLoader extends FileReference {

        public var pass:String;
        public var file:FileReference = new FileReference();
        public function DownLoader():void {
        }
        public function getDown(pass):void{
            var ref:URLRequest = new URLRequest();
            ref.url=pass;
            file.download(ref);
        }
    }
}