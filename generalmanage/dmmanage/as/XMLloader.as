package {
	import flash.events.*;
	import flash.net.URLLoader;
	import flash.net.URLLoaderDataFormat;
	import flash.net.URLRequest;
	import flash.system.System;
	//クラス定義
	public class XMLloader extends EventDispatcher{
		//クラスプロパティー
		public static const LOAD_COMPLETE:String = "load_complete";
		private var myLoader:URLLoader;
		private var myXML:XML;

		//コンストラクタ
		public function XMLloader(url:String){
			//URLLoader作成
			myLoader = new URLLoader();
			myLoader.dataFormat = URLLoaderDataFormat.TEXT;
			myLoader.addEventListener(Event.COMPLETE, onXMLloaded);

			//URL
			var urlReq:URLRequest = new URLRequest(url);
			myLoader.load(urlReq);
		}

		//読み込み完了
		public function onXMLloaded(event:Event):void{
			try {
				//XMLオブジェクトに変換する
				myXML = new XML(myLoader.data);
				//カスタムイベントを配信する
				dispatchEvent(new Event(LOAD_COMPLETE));
			} catch (err:TypeError) {
				trace(err.message);
			}
		}
		//XMLオブジェクトを返す（他クラスからに対応）
		public function getXML():XML{
			return myXML;
		}
	}
}