﻿import mx.core.UIComponent
import mx.controls.ComboBox
import mx.controls.Alert;

class cbCell extends UIComponent
{
	var combo : MovieClip;
	var owner : MovieClip;
	var listOwner : MovieClip;
	var getCellIndex : Function;
	var getDataLabel : Function;

	function ComboBoxCellRenderer(){}
	function createChildren(Void) : Void{
		combo = createObject("ComboBox", "Combo", 0, {styleName:this, owner:this,rowCount:4});
		combo.addEventListener("change", this);
		var foo = combo.dropdown
//		combo.dataProvider = cbprovider;
		size();
	}
	function size(Void) : Void{
		var h = __height;
		var w = __width; 
		combo.setSize(w - 2, Math.max(h, listOwner.rowHeight - 2));   
	}
	public function setValue(str:String, item:Object, sel:Boolean) : Void{

		var drawCombo:Boolean = true;
		//combo.dataProvider = this["cbprovider" + item["resort_code"]];
		combo.dataProvider = getcombodata(item["resort_code"]);
		
		if (item[getDataLabel()]!=undefined){

			for(var i:Number = 0; i < combo.length; i++){
				if( combo.getItemAt(i).data == item[getDataLabel()] ){
					combo.selectedIndex = i;
					if(combo.getItemAt(i).data == 5){combo.enabled = true;}else{combo.enabled = true;}
					break;
				}
				if ( i == combo.length - 1 ){
				}
			}
		}else{
			combo.selectedIndex = 0;
		}
		combo._visible = drawCombo;

	}

	function getPreferredHeight(Void) : Number{
		return owner.__height;
	}
	function getPreferredWidth(Void) : Number{
		return owner.__width;
	}
	function reorder(datos:Array, choice:String):Array {
		var index:Number = 0
		var newArray = new Array()
		for(var i=0; i<datos.length; i++){      
			if(datos[i].label!=choice){
				index++
				newArray[index] = datos[i]
			} else newArray[0] = datos[i]
		}
		return newArray
	}
	public function change(){
		listOwner.dataProvider.editField(getCellIndex().itemIndex, getDataLabel(), combo.selectedItem.data);   
		listOwner.selectedIndex = getCellIndex().itemIndex;
	}

	public function getcombodata(rcd){
		//	--CRMKitchen管理下＆旅館ＡＬＬアカウント
		if(rcd==0 || rcd==999){
			var cbprovider : Array = [
																	{label: "フルアクセス", data: "s"}
																];
		}
		//	--旅館
		if(rcd==1 || rcd==2 || rcd==3 || rcd==4 || rcd==5 || rcd==8 || rcd==9 || rcd==11 || rcd==14 || rcd==15){
			var cbprovider : Array = [
																	{label: "フルアクセス", data: "s"},
																	{label: "メール可", data: "b"}, 
																	{label:"CSのみ", data:"z"}
																];
		}
		//	--トマム
		if(rcd==501){
			var cbprovider : Array = [
																	{label: "フルアクセス", data: "2"},
																	{label:"CSのみ", data:"0"}
																];
		}
		//	--HBC＆リゾ
		if(rcd==502 || rcd==505){
			var cbprovider : Array = [
																	{label: "フルアクセス", data: "s"},
																	{label: "宿泊", data: "a"}, 
																	{label: "宿泊メール", data: "b"}, 
																	{label: "新婦", data: "c"}, 
																	{label: "新婦メール", data: "d"}, 
																	{label: "CSのみ", data: "z"} 
																];
		}
		//	--アルツ
		if(rcd==503){
			var cbprovider : Array = [
																	{label: "フルアクセス", data: "s"},
																	{label: "BOH", data: "a"}, 
																	{label: "BOHメール", data: "b"}, 
																	{label: "RIB", data: "c"}, 
																	{label: "RIBメール", data: "d"}, 
																	{label: "CSのみ", data: "z"} 
																];
		}
		//	--アルツアカデミー
		if(rcd==506){
			var cbprovider : Array = [
																	{label: "フルアクセス", data: "s"},
																	{label: "ゲレンデ", data: "a"}, 
																	{label: "ゲレンデメール", data: "b"}, 
																	{label: "アカデミー", data: "c"}, 
																	{label: "アカデミーメール", data: "d"}, 
																	{label: "CSのみ", data: "z"} 
																];
		}
		return cbprovider

	}
}
