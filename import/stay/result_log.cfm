<!---
*************************************************************************************************
* Name			: error_log
* Description	: ログ出力用カスタムタグ
* Attributes 	: label (string,optional)		:  ラベル
*				  errorType (string,optional)	:  エラーの種類
*				  message (string,optional)		: メッセージデータ
*				  detail (string,optional)		: メッセージ詳細データ
* Note				: none
* History			: 1) Coded by	
*					: 
*************************************************************************************************
--->
<cfsilent>

<!--- パラメータ初期化 --->
<cfparam name="attributes.label" default="">
<cfparam name="attributes.errorType" default="">
<cfparam name="attributes.message" default="">
<cfparam name="attributes.detail" default="">
<cfparam name="attributes.path" default="">
<cfparam name="attributes.filename" default="">

<!--- 書き出し先のPATH確定 --->
<cfset wklogroot="#Application.root#import/#attributes.path#/">
<cfset wk_filePath = ExpandPath(#wklogroot#)>

<!--- 改行コード変換 --->
<cfscript>
	message = replace(attributes.message,"#chr(13)#"," ","all");
	message = replace(message,"#chr(10)#"," ","all");
	message = replace(message,"""","""""","all");
	
	detail = replace(attributes.detail,"#chr(13)#"," ","all") ;
	detail = replace(detail,"#chr(10)#"," ","all") ;
	detail = replace(detail,"""","""""","all") ;
</cfscript>

<!--- ログ出力 --->
<cfif len(trim(wk_filePath))>
	<cfscript>
		filePath = wk_filePath & attributes.filename;
	
		CRLF = chr(13) & chr(10);
		QUOTE = chr(34);
		
		if (fileExists(trim(filePath))) {
			action = "append";
		} else {
			action = "write";
		}
		
		log_string =  quote & dateFormat(now(),"yyyy/mm/dd") & QUOTE & ",";
		log_string = log_string & QUOTE & TimeFormat(Now(),"HH:mm:ss") & QUOTE & ",";
		log_string = log_string & QUOTE & GetTemplatePath() & QUOTE & ",";
		log_string = log_string & QUOTE & attributes.label & QUOTE & ",";
		log_string = log_string & QUOTE & attributes.errorType & QUOTE & ",";
		log_string = log_string & QUOTE & message & QUOTE & "," ;
		log_string = log_string & QUOTE & detail & QUOTE ;
	</cfscript>
	
	<cflock type="EXCLUSIVE" name="LogFile" timeout="30">
		<cffile action="#action#" file="#filePath#" output="#log_string#" mode="777">
	</cflock>
</cfif>

</cfsilent>