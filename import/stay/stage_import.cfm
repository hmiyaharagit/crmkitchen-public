<cftry>
	<cfsetting enableCFoutputOnly = "Yes" showDebugOutput="Yes">
	<cfscript>
		SetEncoding("form", "shift_jis");
		theEncoding = GetEncoding("form");
	</cfscript>	
	<!--- NULL⇔-1変換ファンクション --->
	<cffunction name="nullvar">
		<cfargument name="avar" type="variablename" required="yes">
		<cfargument name="cvar" type="any" required="no" default="null">

		<cfscript>var cfv_var = "";</cfscript>
		<cfif evaluate("#avar#") eq -1>			<cfset cfv_var = #cvar#>
		<cfelseif len(evaluate("#avar#")) eq 0>	<cfset cfv_var = -1>
		<cfelse>								<cfset cfv_var = evaluate("#avar#")>
		</cfif>
		<cfreturn cfv_var>
	</cffunction>	

	<!--- 設定ファイル読み込み --->
	<cfinclude template="inc_setting.cfm">


	<!--- 顧客テーブル --->
	<cfparam name="form.customernumber" default="">			<!--- 顧客番号 --->
	<cfparam name="form.customercategory" default="">		<!--- 顧客区分 --->
	<cfparam name="form.customerrank" default="">			<!--- 客種 --->
	<cfparam name="form.kananame" default="">				<!--- カナ名 --->
	<cfparam name="form.kanjiname" default="">				<!--- 漢字名 --->
	<cfparam name="form.birthdate" default="">				<!--- 生年月日 --->
	<cfparam name="form.postalcode" default="">				<!--- 郵便番号 --->
	<cfparam name="form.address1" default="">				<!--- 住所1 --->
	<cfparam name="form.address2" default="">				<!--- 住所2 --->
	<cfparam name="form.address3" default="">				<!--- 住所3 --->
	<cfparam name="form.homephonenumber" default="">		<!--- 自宅電話番号 --->
	<cfparam name="form.portablephonenumber" default="">	<!--- 携帯電話番号 --->
	<cfparam name="form.e_mail" default="">					<!--- Eメールアドレス --->
	<cfparam name="form.married" default="">				<!--- 結婚フラグ --->
	<cfparam name="form.sex" default="">					<!--- 性別 --->
	<cfparam name="form.note" default="">					<!--- 備考 --->
	<cfparam name="form.marketingarea" default="">			<!--- エリアコード --->

	<!--- 予約テーブル --->
	<cfparam name="form.roomnumber" default="">				<!--- ルーム --->
	<cfparam name="form.reservationnumber" default="">		<!--- 予約番号 --->
	<cfparam name="form.arrivaldate" default="">			<!--- 到着日 --->
	<cfparam name="form.nights" default="">					<!--- 泊数 --->
	<cfparam name="form.departuredate" default="">			<!--- 出発日 --->
	<cfparam name="form.lateout" default="">				<!--- レイトアウト --->
	<cfparam name="form.manpersoncount" default="">			<!--- 男 --->
	<cfparam name="form.womenpersoncount" default="">		<!--- 女 --->
	<cfparam name="form.childpersoncount_a" default="">		<!--- 子A --->
	<cfparam name="form.childpersoncount_b" default="">		<!--- 子B --->
	<cfparam name="form.childpersoncount_c" default="">		<!--- 子C --->
	<cfparam name="form.childpersoncount_d" default="">		<!--- 子D --->

	<cfparam name="form.roomtype" default="">				<!--- ルームタイプ --->
	<cfparam name="form.roomcontrol" default="">			<!--- ルームコントロール --->
	<cfparam name="form.reservationroute" default="">		<!--- 予約経路 --->
	<cfparam name="form.totalsalesamount" default="">		<!--- 客室総売上額 --->
	<cfparam name="form.totalsalesamountdividedbynights" default="">			<!--- 客室単価 --->
	<cfparam name="form.personunitprice" default="">		<!--- 客単価（一泊） --->
	<cfparam name="form.roomcharge" default="">				<!--- ルームチャージ（一泊） --->
	<cfparam name="form.optionamount" default="">			<!--- オプション料金（一泊） --->
	<cfparam name="form.restaurantamount" default="">		<!--- レストラン料金（一泊） --->

	<cfparam name="form.marketing_1" default="">			<!--- マーケティング/認知経路 --->
	<cfparam name="form.marketing_2" default="">			<!--- マーケティング/旅行目的1 --->
	<cfparam name="form.marketing_3" default="">			<!--- マーケティング/客層区分 --->
	<cfparam name="form.marketing_4" default="">			<!--- マーケティング --->
	<cfparam name="form.marketing_5" default="">			<!--- マーケティング --->
	<cfparam name="form.marketing_6" default="">			<!--- マーケティング --->
	<cfparam name="form.marketing_7" default="">			<!--- マーケティング --->
	<cfparam name="form.marketing_8" default="">			<!--- マーケティング --->
	<cfparam name="form.marketing_9" default="">			<!--- マーケティング --->
	<cfparam name="form.marketing_10" default="">			<!--- マーケティング --->
		
	<cfparam name="form.referencenumber" default="">		<!--- 照会用番号 --->
	<cfparam name="form.surveymethod" default="">			<!--- 調査手段 --->
	<cfparam name="form.shopresult_01" default="">			<!--- 店舗1 --->
	<cfparam name="form.shopresult_02" default="">			<!--- 店舗2 --->
	<cfparam name="form.shopresult_03" default="">			<!--- 店舗3 --->
	<cfparam name="form.shopresult_04" default="">			<!--- 店舗4 --->
	<cfparam name="form.shopresult_05" default="">			<!--- 店舗5 --->
	<cfparam name="form.shopresult_06" default="">			<!--- 店舗6 --->
	<cfparam name="form.shopresult_07" default="">			<!--- 店舗7 --->
	<cfparam name="form.shopresult_08" default="">			<!--- 店舗8 --->
	<cfparam name="form.shopresult_09" default="">			<!--- 店舗9 --->
	<cfparam name="form.shopresult_10" default="">			<!--- 店舗10 --->
	<cfparam name="form.shopresult_11" default="">			<!--- 店舗11 --->
	<cfparam name="form.shopresult_12" default="">			<!--- 店舗12 --->
	<cfparam name="form.shopresult_13" default="">			<!--- 店舗13 --->
	<cfparam name="form.shopresult_14" default="">			<!--- 店舗14 --->
	<cfparam name="form.shopresult_15" default="">			<!--- 店舗15 --->
	<cfparam name="form.shopresult_16" default="">			<!--- 店舗16 --->
	<cfparam name="form.shopresult_17" default="">			<!--- 店舗17 --->
	<cfparam name="form.shopresult_18" default="">			<!--- 店舗18 --->
	<cfparam name="form.shopresult_19" default="">			<!--- 店舗19 --->
	<cfparam name="form.shopresult_20" default="">			<!--- 店舗20 --->
		
	<cfparam name="form.facilityid" default="">				<!--- 施設コード --->
	<cfparam name="form.roomsalesitemid" default="">		<!--- パッケージ --->
	<cfparam name="form.createuseraccountid" default="">	<!--- 予約担当者 --->
	<cfparam name="form.createtimestamp" default="">		<!--- 予約日 --->
	<cfparam name="form.pos_menu" default="">				<!--- POSメニュー --->
	<cfparam name="form.pricingtype" default="">			<!--- 料金区分 --->
	<cfparam name="form.reservationtype" default="">		<!--- 団体・個人フラグ --->

	<!--- NULL→-1変換 --->
	<cfset form.roomnumber = nullvar("form.roomnumber")>
	<cfset form.nights = nullvar("form.nights")>
	<cfset form.lateout = nullvar("form.lateout")>
	<cfset form.manpersoncount = nullvar("form.manpersoncount")>
	<cfset form.womenpersoncount = nullvar("form.womenpersoncount")>
	<cfset form.childpersoncount_a = nullvar("form.childpersoncount_a")>
	<cfset form.childpersoncount_b = nullvar("form.childpersoncount_b")>
	<cfset form.childpersoncount_c = nullvar("form.childpersoncount_c")>
	<cfset form.childpersoncount_d = nullvar("form.childpersoncount_d")>
	<cfset form.totalsalesamount = nullvar("form.totalsalesamount")>
	<cfset form.totalsalesamountdividedbynights = nullvar("form.totalsalesamountdividedbynights")>
	<cfset form.personunitprice = nullvar("form.personunitprice")>
	<cfset form.roomcharge = nullvar("form.roomcharge")>
	<cfset form.optionamount = nullvar("form.optionamount")>
	<cfset form.restaurantamount = nullvar("form.restaurantamount")>
	<cfset form.sex = nullvar("form.sex")>
	<cfset form.married = nullvar("form.married")>
	<cfset form.surveymethod = nullvar("form.surveymethod")>

	<!--- 性別コードをマスタ値変換 --->
	<cfif form.sex EQ "M">
		<cfset form.sex = 100000000001>
	<cfelseif form.sex EQ "W">	
		<cfset form.sex = 100000000002>
	<cfelse>
		<cfset form.sex = 100000000000>
	</cfif>

	<!--- デバッグ用ログ --->
	<cfset StructDelete(form,'FIELDNAMES')>
	<cfset wkkey=StructKeyList(form)>
	<cfset validstr = "">
	<cfloop list="#wkkey#" index="str">
		<cfset validstr = validstr & "&#str#="& Evaluate("form.#str#")>
	</cfloop>
	<cf_result_log label = "Error" errorType = "debug" message = "debug" detail = "#validstr#" path="#arg_log_path#" filename="imp_error.log">

	<!--- Validation:数値チェック --->
	<cfif isnumeric(LTrim(form.roomnumber)) eq "NO">
			<cfset error_msg = "ルームナンバーが数値ではありません。"><cfthrow message = "#error_msg#" detail = "#error_msg#">
	</cfif>
	<cfif isnumeric(LTrim(form.nights)) eq "NO">
		<cfset error_msg = "泊数が数値ではありません。"><cfthrow message = "#error_msg#" detail = "#error_msg#">
	</cfif>
	<cfif isnumeric(LTrim(form.lateout)) eq "NO">
		<cfset error_msg = "レイトアウトが数値ではありません。"><cfthrow message = "#error_msg#" detail = "#error_msg#">
	</cfif>
	<cfif isnumeric(form.manpersoncount) eq "NO">
		<cfset error_msg = "人数：男が数値ではありません。"><cfthrow message = "#error_msg#" detail = "#error_msg#">
	</cfif>
	<cfif isnumeric(form.womenpersoncount) eq "NO">
		<cfset error_msg = "人数：女が数値ではありません。"><cfthrow message = "#error_msg#" detail = "#error_msg#">
	</cfif>
	<cfif isnumeric(form.childpersoncount_a) eq "NO">
		<cfset error_msg = "人数：子Aが数値ではありません。"><cfthrow message = "#error_msg#" detail = "#error_msg#">
	</cfif>
	<cfif isnumeric(form.childpersoncount_b) eq "NO">
		<cfset error_msg = "人数：子Bが数値ではありません。"><cfthrow message = "#error_msg#" detail = "#error_msg#">
	</cfif>
	<cfif isnumeric(form.childpersoncount_c) eq "NO">
		<cfset error_msg = "人数：子Cが数値ではありません。"><cfthrow message = "#error_msg#" detail = "#error_msg#">
	</cfif>
	<cfif isnumeric(form.childpersoncount_d) eq "NO">
		<cfset error_msg = "人数：子Dが数値ではありません。"><cfthrow message = "#error_msg#" detail = "#error_msg#">
	</cfif>
	<cfif isnumeric(form.totalsalesamount) eq "NO">
		<cfset error_msg = "客室総売上額が数値ではありません。"><cfthrow message = "#error_msg#" detail = "#error_msg#">
	</cfif>
	<cfif isnumeric(form.totalsalesamountdividedbynights) eq "NO">
		<cfset error_msg = "客室単価が数値ではありません。"><cfthrow message = "#error_msg#" detail = "#error_msg#">
	</cfif>
	<cfif isnumeric(form.personunitprice) eq "NO">
		<cfset error_msg = "客単価（一泊）が数値ではありません。"><cfthrow message = "#error_msg#" detail = "#error_msg#">
	</cfif>
	<cfif isnumeric(form.roomcharge) eq "NO">
		<cfset error_msg = "ルームチャージ（一泊）が数値ではありません。"><cfthrow message = "#error_msg#" detail = "#error_msg#">
	</cfif>
	<cfif isnumeric(form.optionamount) eq "NO">
		<cfset error_msg = "オプション料金（一泊）が数値ではありません。"><cfthrow message = "#error_msg#" detail = "#error_msg#">
	</cfif>
	<cfif isnumeric(form.restaurantamount) eq "NO">
		<cfset error_msg = "レストラン料金（一泊）が数値ではありません。"><cfthrow message = "#error_msg#" detail = "#error_msg#">
	</cfif>
	<cfif isnumeric(form.married) eq "NO">
		<cfset error_msg = "結婚フラグが数値ではありません。"><cfthrow message = "#error_msg#" detail = "#error_msg#">
	</cfif>

	<!--- Validation:マスタデータ形式チェック --->
	<cfset marketingname = arraynew(1)>
	<cfset marketingname[1] = "マーケティング/認知経路">
	<cfset marketingname[2] = "マーケティング/旅行目的1">
	<cfset marketingname[3] = "マーケティング/旅行目的2">
	<cfset marketingname[4] = "マーケティング/旅行目的3">
	<cfset marketingname[5] = "マーケティング/同行者">
	<cfset marketingname[6] = "マーケティング/見た目年齢">
	<cfset marketingname[7] = "マーケティング/項目7">
	<cfset marketingname[8] = "マーケティング/項目8">
	<cfset marketingname[9] = "マーケティング/項目9">
	<cfset marketingname[10] = "マーケティング/項目10">
	<cfloop from="1" to="10" index="idx">
		<cfset chackcode = idx>
		<cfset chackparam = evaluate("form.marketing_" & chackcode)>
		<cfif len(chackparam) neq 0>
			<cfif FindNoCase(':',chackparam) eq 0>
				<cfset error_msg = "#marketingname[chackcode]#の形式が不正です。「コード：名称」の形式ではありません。">
				<cfthrow message = "#error_msg#" detail = "#error_msg#">
			</cfif>
			<cfparam name="arg_marketing_#chackcode#" default="#left(chackparam,FindNoCase(':',chackparam)-1)#" type="string">	
			<cfparam name="arg_marketing_#chackcode#_name" default="#mid(chackparam,FindNoCase(':',chackparam)+1,len(chackparam))#" type="string">
		<cfelse>
			<cfparam name="arg_marketing_#chackcode#" default="" type="string">	
			<cfparam name="arg_marketing_#chackcode#_name" default="" type="string">
		</cfif>
		<!--- 
		#chackcode#====#evaluate("arg_marketing_" & chackcode)#------------#evaluate("arg_marketing_" & chackcode & "_name")#<br>
		 --->
	</cfloop>
	<!--- 2009/02/23 roomtype&roomcontrol追加 --->
	<cfset chackparam = evaluate("form.roomtype")>
	<cfif len(chackparam) neq 0>
		<cfif FindNoCase(':',chackparam) eq 0>
			<cfset error_msg = "部屋タイプの形式が不正です。「コード：名称」の形式ではありません。">
			<cfthrow message = "#error_msg#" detail = "#error_msg#">
		</cfif>
		<cfparam name="arg_roomtype" default="#left(form.roomtype,FindNoCase(':',form.roomtype)-1)#" type="string">							<!--- ルームタイプコード --->
		<cfparam name="arg_roomtype_name" default="#mid(form.roomtype,FindNoCase(':',form.roomtype)+1,len(form.roomtype))#" type="string">	<!--- ルームタイプ名称 --->
	<cfelse>
		<cfparam name="arg_roomtype" default="" type="string">		<!--- ルームタイプコード --->
		<cfparam name="arg_roomtype_name" default="" type="string">	<!--- ルームタイプ名称 --->
	</cfif>
	<cfset chackparam = evaluate("form.roomcontrol")>
	<cfif len(chackparam) neq 0>
		<cfif FindNoCase(':',chackparam) eq 0>
			<cfset error_msg = "ブロックの形式が不正です。「コード：名称」の形式ではありません。">
			<cfthrow message = "#error_msg#" detail = "#error_msg#">
		</cfif>
		<cfparam name="arg_roomcontrol" default="#left(form.roomcontrol,FindNoCase(':',form.roomcontrol)-1)#" type="string">							<!--- ルームコントロールコード --->
		<cfparam name="arg_roomcontrol_name" default="#mid(form.roomcontrol,FindNoCase(':',form.roomcontrol)+1,len(form.roomcontrol))#" type="string">	<!--- ルームコントロール名称 --->
	<cfelse>
		<cfparam name="arg_roomcontrol" default="" type="string">		<!--- ルームコントロールコード --->
		<cfparam name="arg_roomcontrol_name" default="" type="string">	<!--- ルームコントロール名称 --->
	</cfif>

	<cfif FindNoCase(':',form.marketingarea) eq 0>
		<cfset error_msg = "エリアコードの形式が不正です。「コード：名称」の形式ではありません。">
		<cfthrow message = "#error_msg#" detail = "#error_msg#">
	</cfif>
	<cfif len(form.marketingarea) lte 1>
		<cfset error_msg = "エリアコードの形式が不正です。参照できない値です。">
		<cfthrow message = "#error_msg#" detail = "#error_msg#">
	</cfif>

	<!--- Validation:ショップデータ形式チェック --->
	<cfloop from="1" to="20" index="idx">
		<cfset chackcode = numberformat(idx,'00')>
		<cfset chackparam = evaluate("form.shopresult_" & chackcode)>
		<cfif len(chackparam) neq 0>
			<cfif listlen(chackparam,'-') neq 3>
				<cfset error_msg = "店舗#chackcode#コードの形式が不正です。「コード-名称-利用金額」の形式ではありません。">
				<cfthrow message = "#error_msg#" detail = "#error_msg#">
			</cfif>
			<cfset shopresult_cd=#listgetat(chackparam,1,'-')#>
			<cfset shopresult_nm=#listgetat(chackparam,2,'-')#>
			<cfset shopresult_am=#listgetat(chackparam,3,'-')#>
			<cfif isnumeric(shopresult_cd) eq "NO"><cfset error_msg = "店舗#chackcode#コードが数値ではありません。"><cfthrow message = "#error_msg#" detail = "#error_msg#"></cfif>
			<cfif len(shopresult_nm) eq 0><cfset error_msg = "店舗#chackcode#名称がありません。"><cfthrow message = "#error_msg#" detail = "#error_msg#"></cfif>
			<cfif isnumeric(shopresult_am) eq "NO"><cfset error_msg = "店舗#chackcode#利用金額が数値ではありません。"><cfthrow message = "#error_msg#" detail = "#error_msg#"></cfif>
			<cfparam name="arg_shopresult_#chackcode#" default="#shopresult_cd#" type="numeric">
			<cfparam name="arg_shopresult_#chackcode#_name" default="#shopresult_nm#" type="string">
			<cfparam name="arg_shopresult_#chackcode#_amnt" default="#shopresult_am#" type="numeric">
		<cfelse>
			<cfparam name="arg_shopresult_#chackcode#" default="0" type="numeric">
			<cfparam name="arg_shopresult_#chackcode#_name" default="" type="string">
			<cfparam name="arg_shopresult_#chackcode#_amnt" default="0" type="numeric">
		</cfif>
	</cfloop>

	<!--- Validation:デフォルトチェック --->
	<!--- 顧客テーブル --->
	<cfparam name="arg_customernumber" default="#form.customernumber#" type="string">						<!--- 顧客番号 --->
	<cfparam name="arg_customercategory" default="#form.customercategory#" type="string">					<!--- 顧客区分 --->
	<cfparam name="arg_customerrank" default="#form.customerrank#" type="string">							<!--- 客種 --->
	<cfparam name="arg_kananame" default="#form.kananame#" type="string">									<!--- カナ名 --->
	<cfparam name="arg_kanjiname" default="#form.kanjiname#" type="string">									<!--- 漢字名 --->
	<cfparam name="arg_birthdate" default="#form.birthdate#" type="string">									<!--- 生年月日 --->
	<cfparam name="arg_postalcode" default="#form.postalcode#" type="string">								<!--- 郵便番号 --->
	<cfparam name="arg_address1" default="#form.address1#" type="string">									<!--- 住所1 --->
	<cfparam name="arg_address2" default="#form.address2#" type="string">									<!--- 住所2 --->
	<cfparam name="arg_address3" default="#form.address3#" type="string">									<!--- 住所3 --->
	<cfparam name="arg_homephonenumber" default="#form.homephonenumber#" type="string">						<!--- 自宅電話番号 --->
	<cfparam name="arg_portablephonenumber" default="#form.portablephonenumber#" type="string">				<!--- 携帯電話番号 --->
	<cfif len(form.e_mail) eq 0>
		<cfparam name="arg_e_mail1" default="#form.e_mail#" type="string">									<!--- Eメールアドレス --->
		<cfparam name="arg_e_mail2" default="#form.e_mail#" type="string">									<!--- Eメールアドレス --->
	<cfelse>
		<cfparam name="arg_e_mail1" default="#form.e_mail#" type="string">									<!--- Eメールアドレス --->
		<cfparam name="arg_e_mail2" default="#form.e_mail#" type="string">									<!--- Eメールアドレス --->
		<!--- メールアドレス分割 --->
		<cfif ListLen(form.e_mail,';') GT 1>
			<cfset arg_e_mail1 = #ListGetAt(form.e_mail,1,';')#>
			<cfset arg_e_mail2 = #ListGetAt(form.e_mail,2,';')#>
			<cfif not refind("^[-[:alnum:]_\.]+@[[:alnum:]][-[:alnum:]\.]*[[:alnum:]]\.[[:alpha:]]{2,3}$",arg_e_mail1)>
				<cfset error_msg = "メールアドレス１の形式が不正です。">
				<cfset arg_log_message = arg_log_message & "メールアドレス１の形式が不正です。">
				<!--- 
				<cfthrow message = "#error_msg#" detail = "#error_msg#">
				 --->
			</cfif>
			<cfif not refind("^[-[:alnum:]_\.]+@[[:alnum:]][-[:alnum:]\.]*[[:alnum:]]\.[[:alpha:]]{2,3}$",arg_e_mail2)>
				<cfset error_msg = "メールアドレス２の形式が不正です。">
				<cfset arg_log_message = arg_log_message & "メールアドレス２の形式が不正です。">
				<!--- 
				<cfthrow message = "#error_msg#" detail = "#error_msg#">
				 --->
			</cfif>
		<cfelse>
			<cfset arg_e_mail1 = #form.e_mail#>
			<cfset arg_e_mail2 = ''>
			<cfif not refind("^[-[:alnum:]_\.]+@[[:alnum:]][-[:alnum:]\.]*[[:alnum:]]\.[[:alpha:]]{2,3}$",arg_e_mail1)>
				<cfset error_msg = "メールアドレス１の形式が不正です。">
				<cfset arg_log_message = arg_log_message & "メールアドレス１の形式が不正です。">
				<!--- 
				<cfthrow message = "#error_msg#" detail = "#error_msg#">
				 --->
			</cfif>
		</cfif>
	</cfif>

	<cfparam name="arg_married" default="#form.married#" type="numeric">									<!--- 結婚フラグ --->
	<cfparam name="arg_sex" default="#form.sex#" type="string">												<!--- 性別 --->
	<cfparam name="arg_note" default="#form.note#" type="string">											<!--- 備考 --->

	<!--- 予約テーブル --->
	<cfparam name="arg_room_code" default="#form.roomnumber#" type="numeric">								<!--- ルーム --->
	<cfparam name="arg_reserve_code" default="#form.reservationnumber#" type="string">						<!--- 予約番号 --->
	<cfparam name="arg_arrive_date" default="#dateformat(form.arrivaldate,'yyyy/mm/dd')#" type="date">		<!--- 到着日 --->
	<cfparam name="arg_nights" default="#form.nights#" type="numeric">										<!--- 泊数 --->
	<cfparam name="arg_depart_date" default="#dateformat(form.departuredate,'yyyy/mm/dd')#" type="date">	<!--- 出発日 --->
	<cfparam name="arg_lateout_flg" default="#form.lateout#" type="string">									<!--- レイトアウト --->
	<cfparam name="arg_manpersoncount" default="#form.manpersoncount#" type="numeric">						<!--- 男 --->
	<cfparam name="arg_womenpersoncount" default="#form.womenpersoncount#" type="numeric">					<!--- 女 --->
	<cfparam name="arg_childpersoncount_a" default="#form.childpersoncount_a#" type="numeric">				<!--- 子A --->
	<cfparam name="arg_childpersoncount_b" default="#form.childpersoncount_b#" type="numeric">				<!--- 子B --->
	<cfparam name="arg_childpersoncount_c" default="#form.childpersoncount_c#" type="numeric">				<!--- 子C --->
	<cfparam name="arg_childpersoncount_d" default="#form.childpersoncount_d#" type="numeric">				<!--- 子D --->

	<cfparam name="arg_reservationroute" default="#left(form.reservationroute,FindNoCase(':',form.reservationroute)-1)#" type="string">									<!--- 予約経路コード --->
	<cfparam name="arg_reservationroute_name" default="#mid(form.reservationroute,FindNoCase(':',form.reservationroute)+1,len(form.reservationroute))#" type="string">	<!--- 予約経路名称 --->

	<cfparam name="arg_totalsalesamount" default="#form.totalsalesamount#" type="numeric">					<!--- 客室総売上額 --->
	<cfparam name="arg_totalsalesamountdividedbynights" default="#form.totalsalesamountdividedbynights#" type="numeric">			<!--- 客室単価 --->
	<cfparam name="arg_personunitprice" default="#form.personunitprice#" type="numeric">					<!--- 客単価（一泊） --->
	<cfparam name="arg_roomcharge" default="#form.roomcharge#" type="numeric">								<!--- ルームチャージ（一泊） --->
	<cfparam name="arg_optionamount" default="#form.optionamount#" type="numeric">							<!--- オプション料金（一泊） --->
	<cfparam name="arg_restaurantamount" default="#form.restaurantamount#" type="numeric">					<!--- レストラン料金（一泊） --->

	<cfparam name="arg_marketingarea" default="#left(form.marketingarea,FindNoCase(':',form.marketingarea)-1)#" type="string">								<!--- マーケティング/エリアコード --->
	<cfparam name="arg_marketingarea_name" default="#mid(form.marketingarea,FindNoCase(':',form.marketingarea)+1,len(form.marketingarea))#" type="string">	<!--- マーケティング/エリア名称 --->

	<cfparam name="arg_referencenumber" default="#form.referencenumber#" type="string">						<!--- 照会用番号 --->
	<cfparam name="arg_surveymethod" default="#form.surveymethod#" type="string">							<!--- 調査手段 --->
	<cfparam name="arg_facilityid" default="#form.facilityid#" type="string">								<!--- 施設コード --->
	<!--- 施設コードマッチング --->	
	<cfinvoke component="#Application.root#import/common/import" method="setfacilityid" returnVariable="wk_facilityid">
		<cfinvokeargument name="resort_code" value="#arg_facilityid#">
		<cfinvokeargument name="dbu" value="#arg_baseschema#">
		<cfinvokeargument name="dsn" value="#arg_basedsn#">
	</cfinvoke>
	<cfset arg_facilityid = wk_facilityid>
		
	<cfparam name="arg_roomsalesitemid" default="#form.roomsalesitemid#" type="string">						<!--- パッケージ --->
	<cfparam name="arg_createuseraccountid" default="#form.createuseraccountid#" type="string">				<!--- 予約担当者 --->
	<cfparam name="arg_createtimestamp" default="#form.createtimestamp#" type="string">						<!--- 予約日 --->
	<cfparam name="arg_pos_menu" default="#form.pos_menu#" type="string">									<!--- POSメニュー --->
	<cfparam name="arg_pos_category_01" default="0" type="numeric">											<!--- POS:特別料理フラグ --->
	<cfparam name="arg_pos_category_02" default="0" type="numeric">											<!--- POS:和食フラグ --->
	<cfparam name="arg_pos_category_03" default="0" type="numeric">											<!--- POS:洋食フラグ --->
	<cfparam name="arg_pos_category_04" default="0" type="numeric">											<!--- POS:和食廉価版フラグ --->
	<cfparam name="arg_pos_category_05" default="0" type="numeric">											<!--- POS:アラカルトフラグ --->
	<cfparam name="arg_pos_category_06" default="0" type="numeric">											<!--- POS:朝食和食フラグ --->
	<cfparam name="arg_pos_category_07" default="0" type="numeric">											<!--- POS:朝食洋食フラグ --->
		
	<cfparam name="arg_pricingtype" default="#form.pricingtype#" type="string">								<!--- 料金区分 --->
	<cfparam name="arg_reservationtype" default="#form.reservationtype#" type="string">						<!--- 団体・個人フラグ --->

	<!--- -1→NULL変換 --->
	<cfset arg_room_code = nullvar("arg_room_code")>
	<cfset arg_nights = nullvar("arg_nights")>
	<cfset arg_lateout_flg = nullvar("arg_lateout_flg",0)>
	<cfset arg_manpersoncount = nullvar("arg_manpersoncount")>
	<cfset arg_womenpersoncount = nullvar("arg_womenpersoncount")>
	<cfset arg_childpersoncount_a = nullvar("arg_childpersoncount_a")>
	<cfset arg_childpersoncount_b = nullvar("arg_childpersoncount_b")>
	<cfset arg_childpersoncount_c = nullvar("arg_childpersoncount_c")>
	<cfset arg_childpersoncount_d = nullvar("arg_childpersoncount_d")>
	<cfset arg_totalsalesamount = nullvar("arg_totalsalesamount")>
	<cfset arg_totalsalesamountdividedbynights = nullvar("arg_totalsalesamountdividedbynights")>
	<cfset arg_personunitprice = nullvar("arg_personunitprice")>
	<cfset arg_roomcharge = nullvar("arg_roomcharge")>
	<cfset arg_optionamount = nullvar("arg_optionamount")>
	<cfset arg_restaurantamount = nullvar("arg_restaurantamount")>
	<cfset arg_sex = nullvar("arg_sex")>
	<cfset arg_married = nullvar("arg_married")>
	<cfset arg_surveymethod = nullvar("arg_surveymethod")>

	<!--- マスタデータ --->
	<cfinvoke component="#Application.root#import/common/import" method="setcontrolitems" returnVariable="r_reservation">
		<cfinvokeargument name="controldiv_id" value="#application.app00.c_reservation#">
		<cfinvokeargument name="control_id" value="#arg_reservationroute#">
		<cfinvokeargument name="control_nm" value="#arg_reservationroute_name#">
		<cfinvokeargument name="resort_code" value="#arg_facilityid#">
		<cfinvokeargument name="dsn" value="#arg_dsn#">
		<cfinvokeargument name="dbu" value="#arg_schema#">
		<cfinvokeargument name="tbl" value="#arg_mastertbl#">
	</cfinvoke>
	<cfinvoke component="#Application.root#import/common/import" method="setcontrolitems" returnVariable="r_cognition">
		<cfinvokeargument name="controldiv_id" value="#application.app00.c_cognition#">
		<cfinvokeargument name="control_id" value="#arg_marketing_1#">
		<cfinvokeargument name="control_nm" value="#arg_marketing_1_name#">
		<cfinvokeargument name="resort_code" value="#arg_facilityid#">
		<cfinvokeargument name="dsn" value="#arg_dsn#">
		<cfinvokeargument name="dbu" value="#arg_schema#">
		<cfinvokeargument name="tbl" value="#arg_mastertbl#">
	</cfinvoke>
	<cfinvoke component="#Application.root#import/common/import" method="setcontrolitems" returnVariable="r_purpose">
		<cfinvokeargument name="controldiv_id" value="#application.app00.c_purpose#">
		<cfinvokeargument name="control_id" value="#arg_marketing_2#">
		<cfinvokeargument name="control_nm" value="#arg_marketing_2_name#">
		<cfinvokeargument name="resort_code" value="#arg_facilityid#">
		<cfinvokeargument name="dsn" value="#arg_dsn#">
		<cfinvokeargument name="dbu" value="#arg_schema#">
		<cfinvokeargument name="tbl" value="#arg_mastertbl#">
	</cfinvoke>
	<cfinvoke component="#Application.root#import/common/import" method="setcontrolitems" returnVariable="r_together">
		<cfinvokeargument name="controldiv_id" value="#application.app00.c_together#">
		<cfinvokeargument name="control_id" value="#arg_marketing_5#">
		<cfinvokeargument name="control_nm" value="#arg_marketing_5_name#">
		<cfinvokeargument name="resort_code" value="#arg_facilityid#">
		<cfinvokeargument name="dsn" value="#arg_dsn#">
		<cfinvokeargument name="dbu" value="#arg_schema#">
		<cfinvokeargument name="tbl" value="#arg_mastertbl#">
	</cfinvoke>

	<!--- 2009/02/23 roomtype&roomcontrol追加 --->
	<cfinvoke component="#Application.root#import/common/import" method="setcontrolitems" returnVariable="r_roomtype">
		<cfinvokeargument name="controldiv_id" value="#application.app00.c_roomtype#">
		<cfinvokeargument name="control_id" value="#arg_roomtype#">
		<cfinvokeargument name="control_nm" value="#arg_roomtype_name#">
		<cfinvokeargument name="resort_code" value="#arg_facilityid#">
		<cfinvokeargument name="dsn" value="#arg_dsn#">
		<cfinvokeargument name="dbu" value="#arg_schema#">
		<cfinvokeargument name="tbl" value="#arg_mastertbl#">
	</cfinvoke>
	<cfinvoke component="#Application.root#import/common/import" method="setcontrolitems" returnVariable="r_roomcontrol">
		<cfinvokeargument name="controldiv_id" value="2">
		<cfinvokeargument name="control_id" value="#arg_roomcontrol#">
		<cfinvokeargument name="control_nm" value="#arg_roomcontrol_name#">
		<cfinvokeargument name="resort_code" value="#arg_facilityid#">
		<cfinvokeargument name="dsn" value="#arg_dsn#">
		<cfinvokeargument name="dbu" value="#arg_schema#">
		<cfinvokeargument name="tbl" value="#arg_mastertbl#">
	</cfinvoke>

	<!--- 顧客レコードチェック --->
	<cfquery name="getCustomer" datasource = "#arg_dsn#">
		select	 customernumber,isnull(repeatcount,0) as repeatcount
		from	 #arg_schema#.#arg_customertbl#
		where	 customernumber = '#arg_customernumber#'
		and		 resort_code = #arg_facilityid#
	</cfquery>
	<!--- リピートカウント（全体リピート） --->
	<cfquery name="getcustomerrpt" datasource = "#arg_dsn#">
		select	 count(reserve_code) as repeatcount
				,isnull(datediff(day,max(depart_date), '#arg_arrive_date#' ),0) as useinterval
		from	 #arg_schema#.#arg_reservetbl#
		where	 customernumber = '#arg_customernumber#'
	</cfquery>
	<!--- 同一データの再更新はリピート扱いしない --->
	<cfquery name="getdelcount" datasource = "#arg_dsn#">
		select	 count(reserve_code) as repeatcount
		from	 #arg_schema#.#arg_reservetbl#
		where	 customernumber = '#arg_customernumber#'
		and		 resort_code = #arg_facilityid#
		and		 reserve_code = #arg_reserve_code#
	</cfquery>
	
	<cfset arg_repeatcount = getcustomerrpt.repeatcount-getdelcount.repeatcount>
	<cfif arg_repeatcount eq 0>
		<cfset arg_useinterval = 0>
		<cfset arg_repeater = 0>
	<cfelse>
		<cfset arg_useinterval = getcustomerrpt.useinterval>
		<cfset arg_repeater = 1>
	</cfif>

	<cfif getCustomer.recordcount eq 0>
		<cfquery name="upd_customer" datasource="#arg_dsn#">
			insert into #arg_schema#.#arg_customertbl#(
				 resort_code
				,customer_code
				,customernumber
				,customercategory
				,customerrank
				,name
				,name_kana
				,gender_code
				,birth_date
				,address_zipcode
				,address_segment_1
				,address_segment_2
				,address_segment_3
				,phone_number_1
				,phone_number_2
				,mail_address_1
				,mail_address_2
				,post_permission_code
				,mail_permission_code
				,information
				,active_flg
				,married_flg
				,repeater
				,marketingarea
				,useinterval
				,repeatcount
				,impstamp
			)values(
				  #arg_facilityid#
				,null
				,'#arg_customernumber#'
				,'#arg_customercategory#'
				,'#arg_customerrank#'
				,'#arg_kanjiname#'
				,'#arg_kananame#'
				, #arg_sex# 
				,'#arg_birthdate#'
				,'#arg_postalcode#'
				,'#arg_address1#'
				,'#arg_address2#'
				,'#arg_address3#'
				,'#arg_homephonenumber#'
				,'#arg_portablephonenumber#'
				,'#arg_e_mail1#'
				,'#arg_e_mail2#'
				,null
				,null
				,'#arg_note#'
				,1
				, #arg_married#
				, #arg_repeater#
				,'#arg_marketingarea#'
				, #arg_useinterval#
				, #arg_repeatcount#
				, convert(varchar,getdate(),111)
			)
		</cfquery>
		<cfset arg_log_errorType="登録完了">
		<cfset arg_log_detail="【顧客番号：#arg_customernumber#】登録完了">
	<cfelse>

		<cfquery name="upd_customer" datasource="#arg_dsn#">
			update #arg_schema#.#arg_customertbl# set
				 customercategory			 = '#arg_customercategory#'
				,customerrank				 = '#arg_customerrank#'
				,name						 = '#arg_kanjiname#'
				,name_kana 					 = '#arg_kananame#'
				,gender_code				 =  #arg_sex# 
				,birth_date					 = '#arg_birthdate#'
				,address_zipcode			 = '#arg_postalcode#'
				,address_segment_1			 = '#arg_address1#'
				,address_segment_2			 = '#arg_address2#'
				,address_segment_3			 = '#arg_address3#'
				,phone_number_1				 = '#arg_homephonenumber#'
				,phone_number_2				 = '#arg_portablephonenumber#'
				,mail_address_1				 = '#arg_e_mail1#'
				,mail_address_2				 = '#arg_e_mail2#'
				,information				 = '#arg_note#'
				,married_flg				 = #arg_married#
				,repeater					 = #arg_repeater#
				,marketingarea				 = '#arg_marketingarea#'
				,useinterval				 = #arg_useinterval#
				,repeatcount				 = #arg_repeatcount#
				,impstamp					 = convert(varchar,getdate(),111)
			where	 customernumber = '#arg_customernumber#'
		</cfquery>
		<cfset arg_log_errorType="登録完了">
		<cfset arg_log_detail="【顧客番号：#arg_customernumber#】更新完了">
	</cfif>
	<!--- 予約レコードチェック --->
	<cfquery name="getReserve" datasource = "#arg_dsn#">
		select	 reserve_code,isnull(repeatcount,0) as repeatcount
		from	 #arg_schema#.#arg_reservetbl#
		where	 reserve_code = #arg_reserve_code#
		and		 resort_code = #arg_facilityid#
		and		 room_code = #arg_room_code#
	</cfquery>
		
	<!--- POSメニューフラグチェック --->
	<cfif len(arg_pos_menu) neq 0>
		<cfinvoke component="#Application.root#import/common/import" method="setpos_menu" returnVariable="menuresults">
			<cfinvokeargument name="pos_menu" value="#arg_pos_menu#">
			<cfinvokeargument name="dsn" value="#arg_dsn#">
			<cfinvokeargument name="dbu" value="#arg_schema#">
			<cfinvokeargument name="resort_code" value="#arg_facilityid#">
		</cfinvoke>
		<cfset arg_pos_category_01 = menuresults.category_01>
		<cfset arg_pos_category_02 = menuresults.category_02>
		<cfset arg_pos_category_03 = menuresults.category_03>
		<cfset arg_pos_category_04 = menuresults.category_04>
		<cfset arg_pos_category_05 = menuresults.category_05>
		<cfset arg_pos_category_06 = menuresults.category_06>
		<cfset arg_pos_category_07 = menuresults.category_07>
	</cfif>
	<!--- リピートカウント（施設リピート） --->
	<cfquery name="getreserverpt" datasource = "#arg_dsn#">
		select	 count(reserve_code) as repeatcount
				,isnull(datediff(day,max(depart_date), '#arg_arrive_date#' ),0) as useinterval
		from	 #arg_schema#.#arg_reservetbl#
		where	 customernumber = '#arg_customernumber#'
		and		 resort_code = #arg_facilityid#
	</cfquery>
	<!--- 同一データの再更新はリピート扱いしない --->
	<cfquery name="getdelcount" datasource = "#arg_dsn#">
		select	 count(reserve_code) as repeatcount
		from	 #arg_schema#.#arg_reservetbl#
		where	 customernumber = '#arg_customernumber#'
		and		 resort_code = #arg_facilityid#
		and		 reserve_code = #arg_reserve_code#
	</cfquery>
	<cfset arg_repeatcount = getreserverpt.repeatcount-getdelcount.repeatcount>
	<cfif getreserverpt.useinterval lt 0>
		<cfset arg_useinterval = 0>
	<cfelse>
		<cfset arg_useinterval = getreserverpt.useinterval>
	</cfif>
	<cfif getReserve.recordcount eq 0>
		<cfquery name="upd_reserve" datasource="#arg_dsn#">
			insert into #arg_schema#.#arg_reservetbl#(
				 resort_code
				,reserve_code
				,room_code
				,customernumber
				,arrive_date
				,nights
				,depart_date
				,lateout_flg
				,manpersoncount
				,womenpersoncount
				,childpersoncount_a
				,childpersoncount_b
				,childpersoncount_c
				,childpersoncount_d
				,roomtype
				,roomcontrol
				,reservationroute
				,totalsalesamount
				,totalsalesamountdividedbynights
				,personunitprice
				,roomcharge
				,optionamount
				,restaurantamount
				,referencenumber
				,surveymethod
				,marketing_1
				,marketing_2
				,marketing_3
				,marketing_4
				,marketing_5
				,marketing_6
				,marketing_7
				,marketing_8
				,marketing_9
				,marketing_10
				,roomsalesitemid
				,createuseraccountid
				,createtimestamp
				,pos_category_01
				,pos_category_02
				,pos_category_03
				,pos_category_04
				,pos_category_05
				,pos_category_06
				,pos_category_07
				,pos_menu
				,pricingtype
				,reservationtype
				,useinterval
				,repeatcount
			)values(
				  #arg_facilityid#
				, #arg_reserve_code# 
				, #arg_room_code#
				,'#arg_customernumber#'
				,'#arg_arrive_date#'
				,#arg_nights#
				,'#arg_depart_date#'
				, #arg_lateout_flg#
				, #arg_manpersoncount#
				, #arg_womenpersoncount#
				, #arg_childpersoncount_a#
				, #arg_childpersoncount_b#
				, #arg_childpersoncount_c#
				, #arg_childpersoncount_d#
				,'#arg_roomtype#'
				,'#arg_roomcontrol#'
				,'#arg_reservationroute#'
				, #arg_totalsalesamount#
				, #arg_totalsalesamountdividedbynights#
				, #arg_personunitprice#
				, #arg_roomcharge#
				, #arg_optionamount#
				, #arg_restaurantamount#
				,'#arg_referencenumber#'
				, #arg_surveymethod#
				,'#arg_marketing_1#'
				,'#arg_marketing_2#'
				,'#arg_marketing_3#'
				,'#arg_marketing_4#'
				,'#arg_marketing_5#'
				,'#arg_marketing_6#'
				,'#arg_marketing_7#'
				,'#arg_marketing_8#'
				,'#arg_marketing_9#'
				,'#arg_marketing_10#'
				,'#arg_roomsalesitemid#'
				,'#arg_createuseraccountid#'
				,'#arg_createtimestamp#'
				, #arg_pos_category_01#
				, #arg_pos_category_02#
				, #arg_pos_category_03#
				, #arg_pos_category_04#
				, #arg_pos_category_05#
				, #arg_pos_category_06#
				, #arg_pos_category_07#
				,'#arg_pos_menu#'
				,'#arg_pricingtype#'
				,'#arg_reservationtype#'
				, #arg_useinterval#
				, #arg_repeatcount#
			)
		</cfquery>
		<cfset arg_log_errorType="登録完了">
		<cfset arg_log_detail= arg_log_detail & "【予約番号：#arg_reserve_code#】登録完了">
	<cfelse>
		<cfquery name="upd_reserve" datasource="#arg_dsn#">
			update #arg_schema#.#arg_reservetbl# set
				 resort_code				 = #arg_facilityid#
				,reserve_code				 = #arg_reserve_code# 
				,room_code					 = #arg_room_code#
				,customernumber				 ='#arg_customernumber#'
				,arrive_date				 ='#arg_arrive_date#'
				,nights						 = #arg_nights#
				,depart_date				 ='#arg_depart_date#'
				,lateout_flg				 = #arg_lateout_flg#
				,manpersoncount				 = #arg_manpersoncount#
				,womenpersoncount			 = #arg_womenpersoncount#
				,childpersoncount_a			 = #arg_childpersoncount_a#
				,childpersoncount_b			 = #arg_childpersoncount_b#
				,childpersoncount_c			 = #arg_childpersoncount_c#
				,childpersoncount_d			 = #arg_childpersoncount_d#
				,roomtype					 ='#arg_roomtype#'
				,roomcontrol				 ='#arg_roomcontrol#'
				,reservationroute			 ='#arg_reservationroute#'
				,totalsalesamount			 = #arg_totalsalesamount#
				,totalsalesamountdividedbynights= #arg_totalsalesamountdividedbynights#
				,personunitprice			 = #arg_personunitprice#
				,roomcharge					 = #arg_roomcharge#
				,optionamount				 = #arg_optionamount#
				,restaurantamount			 = #arg_restaurantamount#
				,referencenumber			 ='#arg_referencenumber#'
				,surveymethod				 = #arg_surveymethod#
				,marketing_1				 ='#arg_marketing_1#'
				,marketing_2				 ='#arg_marketing_2#'
				,marketing_3				 ='#arg_marketing_3#'
				,marketing_4				 ='#arg_marketing_4#'
				,marketing_5				 ='#arg_marketing_5#'
				,marketing_6				 ='#arg_marketing_6#'
				,marketing_7				 ='#arg_marketing_7#'
				,marketing_8				 ='#arg_marketing_8#'
				,marketing_9				 ='#arg_marketing_9#'
				,marketing_10				 ='#arg_marketing_10#'
				,roomsalesitemid			 ='#arg_roomsalesitemid#'
				,createuseraccountid		 ='#arg_createuseraccountid#'
				,createtimestamp			 ='#arg_createtimestamp#'
				,pos_category_01			 = #arg_pos_category_01#
				,pos_category_02			 = #arg_pos_category_02#
				,pos_category_03			 = #arg_pos_category_03#
				,pos_category_04			 = #arg_pos_category_04#
				,pos_category_05			 = #arg_pos_category_05#
				,pos_category_06			 = #arg_pos_category_06#
				,pos_category_07			 = #arg_pos_category_07#
				,pos_menu					 ='#arg_pos_menu#'
				,pricingtype				 ='#arg_pricingtype#'
				,reservationtype			 ='#arg_reservationtype#'
				,useinterval				 = #arg_useinterval#
				,repeatcount				 = #arg_repeatcount#
			where	 reserve_code =  #arg_reserve_code#
			and		 room_code = #arg_room_code#
			and		 resort_code = #arg_facilityid#
		</cfquery>
		<cfset arg_log_errorType="登録完了">
		<cfset arg_log_detail= arg_log_detail & "【予約番号：#arg_reserve_code#】更新完了">
	</cfif>
	
	<!--- 施設毎リピーターフラグ設定 --->
	<cfif arg_repeatcount gt 0>
		<cfquery name="getrepflg" datasource = "#arg_dsn#">
			select	 repeater
			from	 #arg_schema#.#arg_repeatertbl#
			where	 customernumber = '#arg_customernumber#'
			and		 resort_code = #arg_facilityid#
		</cfquery>
		<cfif getrepflg.recordcount eq 0>
			<cfquery name="upd_reserve" datasource="#arg_dsn#">
				insert into #arg_schema#.#arg_repeatertbl#(
					 resort_code
					,customernumber
					,repeater
				)values(
					  #arg_facilityid#
					,'#arg_customernumber#'
					, 1
				)
			</cfquery>
		</cfif>
	<cfelse>
		<cfquery name="getrepflg" datasource = "#arg_dsn#">
			delete
			from	 #arg_schema#.#arg_repeatertbl#
			where	 customernumber = '#arg_customernumber#'
			and		 resort_code = #arg_facilityid#
		</cfquery>
	</cfif>

	<!--- マーケティングテーブル --->
	<!--- --->
	<cfset lst_marketing_id = "#application.app00.c_cognition#,#application.app00.c_purpose#,#application.app00.c_together#,#application.app00.c_looks#">
	<cfset lst_marketing = "1,2,5,6">
	<cfset cnt = 1>
	<cfloop list="#lst_marketing_id#" index="idx">
		<cfset tmp_control_id = #Evaluate("arg_marketing_" & ListGetAt(lst_marketing,cnt))#>
		<cfquery name="q_rtt_marketing" datasource = "#arg_dsn#">
			select	 reserve_code
					,room_code
			from	#arg_schema#.#arg_marketingtbl#
			where	 reserve_code	 = #arg_reserve_code#
			and		 room_code		 = #arg_room_code#
			and		 resort_code	 = #arg_facilityid#
			and		 controldiv_id	 = #idx#
		</cfquery>
		<cfif q_rtt_marketing.recordcount EQ 0 and len(tmp_control_id) neq 0>
			<cfquery name="q_ist_rtt_marketing" datasource = "#arg_dsn#">
		 		insert into #arg_schema#.#arg_marketingtbl#(
		 			 resort_code
		 			,reserve_code
		 			,room_code
		 			,controldiv_id
		 			,control_id
		 		)values(
		 			  #arg_facilityid#
		 			, #arg_reserve_code#
		 			, #arg_room_code#
		 			, #idx#
		 			,'#tmp_control_id#'
		 		)
			</cfquery>
		</cfif>
		<cfset cnt = incrementvalue(cnt)>
	</cfloop>
	<!--- ログ出力 --->
	<cf_result_log label = "#arg_log_label#" errorType = "#arg_log_errorType#" message = "#arg_log_message#" detail = "#arg_log_detail#" path="#arg_log_path#" filename="#arg_log_filename#">


	<!--- 画面出力 --->
	<cfcontent type="text/html; charset=shift_jis">
	<cfoutput><html><body>SUCCESS</body></html></cfoutput>
	<cfcatch type="any">
		<cfset returnmark = #chr(10)# & #chr(13)#>
		<cfset mail_body = "データインポートエラーが発生しました"&returnmark>
		<cfset mail_body = mail_body&"【顧客番号】#form.customernumber#"&returnmark>
		<cfset mail_body = mail_body&"【予約番号】#form.reservationnumber#"&returnmark>
		<cfset mail_body = mail_body&"【出発日】#form.departuredate#"&returnmark>
		<cfset mail_body = mail_body&"【氏名】#form.kanjiname#"&returnmark>
		<cfset mail_body = mail_body&"【エラー内容】#cfcatch.detail#"&returnmark>

		<!--- DBError --->
		<cfset sqlstr = "">
		<cfif isdefined("cfcatch.sql")>
			<cfset sqlstr = ":#cfcatch.sql#">
		</cfif>
		<cfmail to="#arg_to_mail#" from="#arg_from_mail#"
				bcc="#arg_bcc_mail#"
				subject="-TAPデータ- インポートエラー" type="text/plain"
				server="#arg_smtpserver#" port="#arg_smtpport#" charset="iso-2022-jp"
				username="#arg_username#" password ="#arg_password#"
		>#mail_body#</cfmail>

		<!--- ログ出力 --->
		<cf_result_log label = "Error" errorType = "#cfcatch.type#" message = "#cfcatch.message#" detail = "#mail_body##sqlstr#" path="#arg_log_path#" filename="imp_error.log">

		<!--- 画面出力 --->
		<cfcontent type="text/html; charset=shift_jis">
		<cfoutput><html><body>#cfcatch.message##cfcatch.detail#</body></html></cfoutput>
	</cfcatch>
</cftry>
		