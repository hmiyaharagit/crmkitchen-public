<cfsetting enableCFoutputOnly = "Yes" showDebugOutput="Yes">


<!--- パラメータ初期設定 --->
<cfparam name="arg_log_label" default="インポート結果">		<!--- ログ出力：ラベル --->
<cfparam name="arg_log_errorType" default="登録完了">		<!--- ログ出力：種別 --->
<cfparam name="arg_log_message" default="">					<!--- ログ出力：メッセージ --->
<cfparam name="arg_log_detail" default="">					<!--- ログ出力：詳細 --->
<cfparam name="arg_log_path" default="stay/log">			<!--- ログ出力：ディレクトリ名称 --->
<cfparam name="arg_log_filename" default="imp_result.log">	<!--- ログ出力：ファイル名称 --->
<cfparam name="arg_log_post_filename" default="fortap.log">	<!--- ログ出力：ファイル名称 --->

<!---
<cfparam name="arg_to_mail" default="sales@mic-systems.com">				<!--- エラーメール：送信先アドレス --->
 --->
<cfparam name="arg_to_mail" default="it-help@hoshinoresort.com">				<!--- エラーメール：送信先アドレス --->

<cfparam name="arg_bcc_mail" default="sales@mic-systems.com">				<!--- エラーメール：送信先アドレス --->
<cfparam name="arg_from_mail" default="tap_imp@hoshinoresort.com">			<!--- エラーメール：送信元アドレス --->
<cfparam name="arg_smtpserver" default="#Application.SMTPServer#">			<!--- エラーメール：SMTPサーバー --->
<cfparam name="arg_smtpport" default="#Application.port#">					<!--- エラーメール：SMTPポート --->
<cfparam name="arg_username" default="#Application.username#">				<!--- エラーメール：認証ユーザ --->
<cfparam name="arg_password" default="#Application.password#">				<!--- エラーメール：認証パスワード --->

<cfparam name="arg_basedsn" default="#Application.datasource#">				<!--- 登録先：基準データソース（crmpublic） --->
<cfparam name="arg_baseschema" default="dbo">								<!--- 登録先：基準スキーマ（dbo） --->
<cfquery name="getapdata" datasource = "#arg_basedsn#" result="res">
	select	 dsn
			,dschema
	from	 #arg_baseschema#.mst_resort
	where	 tap_facilityid = '#form.facilityid#'
</cfquery>
<cfif getapdata.recordcount eq 1>
	<cfparam name="arg_dsn" default="#getapdata.dsn#">			<!--- 登録先：データソース --->
	<cfparam name="arg_schema" default="#getapdata.dschema#">	<!--- 登録先：スキーマ --->
<cfelse>
	<cfset error_msg = "アプリケーション情報の取得に失敗しました"><cfthrow message = "#form.facilityid#:#res.sql#:#error_msg#" detail = "#error_msg#">
</cfif>
<cfparam name="arg_customertbl" default="rtm_customer">			<!--- 登録先：顧客 --->
<cfparam name="arg_reservetbl" default="rtt_reserve">			<!--- 登録先：予約 --->
<cfparam name="arg_marketingtbl" default="rtt_marketing">		<!--- 登録先：マーケティング --->
<cfparam name="arg_mastertbl" default="mst_control">			<!--- 登録先：マスタ --->
<cfparam name="arg_repeatertbl" default="imp_repeater">			<!--- 登録先：施設毎リピーターフラグテーブル --->
