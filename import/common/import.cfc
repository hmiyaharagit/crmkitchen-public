﻿<cfcomponent hint="" name="">
	
	<cffunction name="setcontrolitems" access="remote" returntype="any">
		<cfargument name="controldiv_id" type="numeric" default="">
		<cfargument name="control_id" type="String" default="">
		<cfargument name="control_nm" type="String" default="">
		<cfargument name="resort_code" type="String" default="">
		<cfargument name="dsn" type="String" default="">
		<cfargument name="dbu" type="String" default="">
		<cfargument name="tbl" type="String" default="">

		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<!--- レコードチェック --->
			<cfquery name="retQuery" datasource = "#arguments.dsn#">
				select	 count(control_id) cnt
				from	 #arguments.dbu#.#arguments.tbl# 
				where	 controldiv_id = #arguments.controldiv_id#
				and		 control_id = '#arguments.control_id#'
				and		 resort_code = #arguments.resort_code#
			</cfquery>
			<!--- seq --->
			<cfquery name="cntQuery" datasource = "#arguments.dsn#">
				select	 isnull(max(seq)+1,1) nextseq
						,controldiv_nm
				from	 #arguments.dbu#.#arguments.tbl# 
				where	 controldiv_id = #arguments.controldiv_id#
				and		 resort_code = #arguments.resort_code#
				group by controldiv_nm
			</cfquery>
			<cfif cntQuery.recordcount neq 0>
				<cfset wk_nextseq = cntQuery.nextseq>
				<cfset wk_controldiv_nm = cntQuery.controldiv_nm>
			<cfelse>
				<cfset wk_nextseq = 1>
				<cfset wk_controldiv_nm = ''>
			</cfif>
			<cfif retQuery.cnt eq 0>
				<cfquery name="setQuery" datasource = "#arguments.dsn#">
					insert into #arguments.dbu#.#arguments.tbl# (
						 controldiv_id
						,control_id
						,resort_code
						,controldiv_nm
						,control_nm
						,seq
						,active_flg
						,control_opt1
						,control_opt2
					)values(
						  #arguments.controldiv_id#
						,'#arguments.control_id#'
						, #arguments.resort_code#
						,'#wk_controldiv_nm#'
						,'#arguments.control_nm#'
						, #wk_nextseq#
						,1
						,''
						,''
					)
				</cfquery>
				<!---2015/05/01 19:19 復活 --->
			<cfelse>
<!---  --->
				<cfquery name="setQuery" datasource = "#arguments.dsn#">
					update #arguments.dbu#.#arguments.tbl# set
					control_nm = '#arguments.control_nm#'
					where	 controldiv_id = #arguments.controldiv_id#
					and		 control_id = '#arguments.control_id#'
					and		 resort_code = #arguments.resort_code#
				</cfquery>
				 
			</cfif>
			<cfscript>
				execinfo = StructNew();
				StructInsert(execinfo,	"errorFlg",	0);
				StructInsert(execinfo,	"errorMsg",	'');
			</cfscript>
			<cfreturn execinfo>
			<cfcatch type="any">
				<cfrethrow>
				<cfscript>
					execinfo = StructNew();
					StructInsert(execinfo,	"errorFlg",	1);
					StructInsert(execinfo,	"errorMsg",	'マスタメンテナンス更新時にエラーが発生しました。&#cfcatch.detail#');
				</cfscript>
				<cfreturn execinfo>
			</cfcatch>
		</cftry>
	</cffunction>
					
	
	<cffunction name="setfacilityid" access="remote" returntype="any">
		<cfargument name="resort_code" type="String" default="">
		<cfargument name="dbu" type="String" default="">
		<cfargument name="dsn" type="String" default="">
		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<cfquery name="retQuery" datasource = "#arguments.dsn#">
				select	 resort_code
				from	 #arguments.dbu#.crm_facilityid2resort_code 
				where	 tap_facilityid = '#arguments.resort_code#'
			</cfquery>
			<cfreturn retQuery.resort_code>
			<cfcatch type="any">
				<cfrethrow>
				<cfscript>
					execinfo = StructNew();
					StructInsert(execinfo,	"errorFlg",	1);
					StructInsert(execinfo,	"errorMsg",	'対象施設の登録がありません。&#cfcatch.detail#');
				</cfscript>
				<cfreturn execinfo>
			</cfcatch>
		</cftry>
	</cffunction>					
	
	<cffunction name="setpos_menu" access="remote" returntype="any">
		<cfargument name="pos_menu" type="String" default="">
		<cfargument name="dbu" type="String" default="">
		<cfargument name="dsn" type="String" default="">
		<cfargument name="resort_code" type="String" default="">

		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<cfset retmenu = structnew()>
			<cfset retmenu.category_01 = 0>
			<cfset retmenu.category_02 = 0>
			<cfset retmenu.category_03 = 0>
			<cfset retmenu.category_04 = 0>
			<cfset retmenu.category_05 = 0>
			<cfset retmenu.category_06 = 0>
			<cfset retmenu.category_07 = 0>
			<cfset wk_menu = listtoarray(arguments.pos_menu,"/")>
			<cfloop from="1" to="#arraylen(wk_menu)#" index="i">
				<!--- ここに処理挿入 --->
				<cfquery name="getmenuflg" datasource = "#arguments.dsn#">
					select	 menutype_flg
					from	 #arguments.dbu#.posmenugroup 
					where	 resort_code = '#arguments.resort_code#'
					and		 posmenu_code = '#left(wk_menu[i],FindNoCase(':',wk_menu[i])-1)#'
				</cfquery>
				<cfif getmenuflg.recordcount eq 1>
					<cfswitch expression="#getmenuflg.menutype_flg#">
						<cfcase value="0">
							<cfset retmenu.category_01 = 1>
						</cfcase>
						<cfcase value="1">
							<cfset retmenu.category_02 = 1>
						</cfcase>
						<cfcase value="2">
							<cfset retmenu.category_03 = 1>
						</cfcase>
						<cfcase value="3">
							<cfset retmenu.category_04 = 1>
						</cfcase>
						<cfcase value="4">
							<cfset retmenu.category_05 = 1>
						</cfcase>
						<cfcase value="5">
							<cfset retmenu.category_06 = 1>
						</cfcase>
						<cfcase value="6">
							<cfset retmenu.category_07 = 1>
						</cfcase>
					</cfswitch>
				</cfif>
					<!--- ここに処理挿入 --->
			</cfloop>
			<cfreturn retmenu>
			<cfcatch type="any">
				<cfrethrow>
				<cfscript>
					execinfo = StructNew();
					StructInsert(execinfo,	"errorFlg",	1);
					StructInsert(execinfo,	"errorMsg",	'メニューフラグの設定ができませんでした。&#cfcatch.detail#');
				</cfscript>
				<cfreturn execinfo>
			</cfcatch>
		</cftry>
	</cffunction>					

	<cffunction name="getexcept" access="remote" returntype="any">
		<cfargument name="controldiv_id" type="numeric" default="">
		<cfargument name="control_id" type="String" default="">
		<cfargument name="resort_code" type="String" default="">
		<cfargument name="dsn" type="String" default="">
		<cfargument name="dbu" type="String" default="">
		<cfargument name="tbl" type="String" default="">

		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<!--- レコードチェック --->
			<cfquery name="retQuery" datasource = "#arguments.dsn#">
				select	 count(control_id) cnt
				from	 #arguments.dbu#.#arguments.tbl#
				where	 controldiv_id = #arguments.controldiv_id#
				and		 control_id = '#arguments.control_id#'
				and		 resort_code = #arguments.resort_code#
				and		 active_flg = 1
				and		 except_flg = 1
			</cfquery>
			<cfreturn retQuery.cnt>
			<cfcatch type="any">
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>	

					
</cfcomponent>
