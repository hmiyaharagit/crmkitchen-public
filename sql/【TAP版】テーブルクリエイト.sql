if exists (select * from dbo.sysobjects where id = object_id(N'[tap].[h_logintrack]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [tap].[h_logintrack]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[tap].[hst_answers]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [tap].[hst_answers]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[tap].[hst_answersdetails]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [tap].[hst_answersdetails]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[tap].[hst_deliverinformation]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [tap].[hst_deliverinformation]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[tap].[hst_valueinformation]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [tap].[hst_valueinformation]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[tap].[m_loginuser]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [tap].[m_loginuser]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[tap].[mst_answeroptions]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [tap].[mst_answeroptions]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[tap].[mst_confirm]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [tap].[mst_confirm]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[tap].[mst_control]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [tap].[mst_control]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[tap].[mst_mailtemplate]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [tap].[mst_mailtemplate]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[tap].[mst_questionnaire]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [tap].[mst_questionnaire]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[tap].[mst_questionnairecategory]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [tap].[mst_questionnairecategory]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[tap].[mst_questionnairesheet]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [tap].[mst_questionnairesheet]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[tap].[mst_room]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [tap].[mst_room]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[tap].[mst_scenario]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [tap].[mst_scenario]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[tap].[mst_summary]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [tap].[mst_summary]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[tap].[mst_user]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [tap].[mst_user]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[tap].[oth_scenarioconfiguration]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [tap].[oth_scenarioconfiguration]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[tap].[oth_sheetconfiguration]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [tap].[oth_sheetconfiguration]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[tap].[rtm_area]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [tap].[rtm_area]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[tap].[rtm_customer]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [tap].[rtm_customer]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[tap].[rtm_resort]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [tap].[rtm_resort]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[tap].[rtt_marketing]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [tap].[rtt_marketing]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[tap].[rtt_reserve]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [tap].[rtt_reserve]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[tap].[rtt_shopresult]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [tap].[rtt_shopresult]
GO

CREATE TABLE [tap].[h_logintrack] (
	[login_id] [varchar] (12) COLLATE Japanese_CI_AS NOT NULL ,
	[login_dt] [datetime] NOT NULL ,
	[login_cnt] [int] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [tap].[hst_answers] (
	[referencenumber] [varchar] (128) COLLATE Japanese_CI_AS NOT NULL ,
	[questionnairesheet_id] [smallint] NOT NULL ,
	[reserve_code] [bigint] NULL ,
	[registrated] [varchar] (256) COLLATE Japanese_CI_AS NULL ,
	[arrive_date] [varchar] (256) COLLATE Japanese_CI_AS NULL ,
	[depart_date] [varchar] (256) COLLATE Japanese_CI_AS NULL ,
	[area_code] [bigint] NULL ,
	[statistics_value_code] [bigint] NULL ,
	[roomnumber] [bigint] NULL ,
	[answermethod] [tinyint] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [tap].[hst_answersdetails] (
	[referencenumber] [varchar] (128) COLLATE Japanese_CI_AS NOT NULL ,
	[questionnairesheet_id] [smallint] NOT NULL ,
	[questionnaire_id] [smallint] NOT NULL ,
	[answer] [varchar] (50) COLLATE Japanese_CI_AS NOT NULL ,
	[reserve_code] [bigint] NULL ,
	[answertext] [text] COLLATE Japanese_CI_AS NULL ,
	[othertext] [text] COLLATE Japanese_CI_AS NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [tap].[hst_deliverinformation] (
	[referencenumber] [varchar] (128) COLLATE Japanese_CI_AS NULL ,
	[deliverd_date] [datetime] NULL ,
	[cope_flg] [tinyint] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [tap].[hst_valueinformation] (
	[reserve_code] [bigint] NOT NULL ,
	[room_code] [bigint] NOT NULL ,
	[valueinfo] [text] COLLATE Japanese_CI_AS NULL ,
	[lastdata] [text] COLLATE Japanese_CI_AS NULL ,
	[checkout] [tinyint] NOT NULL ,
	[active_flg] [tinyint] NOT NULL ,
	[updatedby] [varchar] (20) COLLATE Japanese_CI_AS NULL ,
	[updated] [datetime] NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [tap].[m_loginuser] (
	[login_id] [varchar] (48) COLLATE Japanese_CI_AS NOT NULL ,
	[login_pw] [varchar] (48) COLLATE Japanese_CI_AS NULL ,
	[login_nm] [varchar] (48) COLLATE Japanese_CI_AS NULL ,
	[login_dv] [varchar] (2) COLLATE Japanese_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [tap].[mst_answeroptions] (
	[questionnaire_id] [smallint] NOT NULL ,
	[options_no] [tinyint] NOT NULL ,
	[options_label] [varchar] (100) COLLATE Japanese_CI_AS NULL ,
	[options_type] [tinyint] NULL ,
	[addtext] [varchar] (100) COLLATE Japanese_CI_AS NULL ,
	[scenario] [tinyint] NULL ,
	[resort_code] [bigint] NULL ,
	[viewsequence] [tinyint] NULL ,
	[active_flg] [tinyint] NOT NULL ,
	[updatedby] [char] (20) COLLATE Japanese_CI_AS NULL ,
	[updated] [datetime] NULL ,
	[cs_alabel] [varchar] (16) COLLATE Japanese_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [tap].[mst_confirm] (
	[questionnaire_id] [smallint] NOT NULL ,
	[confirm_q] [smallint] NOT NULL ,
	[options_no] [tinyint] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [tap].[mst_control] (
	[controldiv_id] [bigint] NOT NULL ,
	[control_id] [varchar] (128) COLLATE Japanese_CI_AS NOT NULL ,
	[controldiv_nm] [varchar] (128) COLLATE Japanese_CI_AS NULL ,
	[control_nm] [varchar] (128) COLLATE Japanese_CI_AS NULL ,
	[seq] [int] NULL ,
	[active_flg] [tinyint] NOT NULL ,
	[control_opt1] [varchar] (128) COLLATE Japanese_CI_AS NULL ,
	[control_opt2] [varchar] (128) COLLATE Japanese_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [tap].[mst_mailtemplate] (
	[mailtemplate_id] [smallint] IDENTITY (1, 1) NOT NULL ,
	[mail_div] [tinyint] NULL ,
	[mail_subject] [varchar] (100) COLLATE Japanese_CI_AS NULL ,
	[mail_body] [text] COLLATE Japanese_CI_AS NULL ,
	[mail_from] [varchar] (50) COLLATE Japanese_CI_AS NOT NULL ,
	[fromaddress] [varchar] (50) COLLATE Japanese_CI_AS NOT NULL ,
	[bccaddress] [varchar] (50) COLLATE Japanese_CI_AS NULL ,
	[testaddress] [varchar] (50) COLLATE Japanese_CI_AS NULL ,
	[resort_code] [bigint] NULL ,
	[active_flg] [tinyint] NOT NULL ,
	[updatedby] [varchar] (20) COLLATE Japanese_CI_AS NULL ,
	[updated] [datetime] NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [tap].[mst_questionnaire] (
	[questionnaire_id] [smallint] IDENTITY (1, 1) NOT NULL ,
	[questionnaire_nm] [varchar] (400) COLLATE Japanese_CI_AS NULL ,
	[questionnaire_type] [tinyint] NOT NULL ,
	[requisite_flg] [tinyint] NOT NULL ,
	[questionnairecategory_id] [smallint] NULL ,
	[resort_code] [bigint] NULL ,
	[active_flg] [tinyint] NOT NULL ,
	[oatype] [smallint] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [tap].[mst_questionnairecategory] (
	[questionnairecategory_id] [smallint] IDENTITY (1, 1) NOT NULL ,
	[questionnairecategory_nm] [varchar] (100) COLLATE Japanese_CI_AS NULL ,
	[resort_code] [bigint] NOT NULL ,
	[active_flg] [tinyint] NOT NULL ,
	[seq] [bigint] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [tap].[mst_questionnairesheet] (
	[questionnairesheet_id] [smallint] NOT NULL ,
	[questionnairesheet_nm] [varchar] (40) COLLATE Japanese_CI_AS NULL ,
	[resort_code] [bigint] NOT NULL ,
	[active_flg] [tinyint] NOT NULL ,
	[start_date] [datetime] NULL ,
	[end_date] [datetime] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [tap].[mst_room] (
	[room_id] [bigint] NOT NULL ,
	[roomtype_id] [varchar] (128) COLLATE Japanese_CI_AS NOT NULL ,
	[roomnumber] [bigint] NOT NULL ,
	[seq] [int] NULL ,
	[active_flg] [tinyint] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [tap].[mst_scenario] (
	[scenario_id] [tinyint] IDENTITY (1, 1) NOT NULL ,
	[scenario_nm] [varchar] (100) COLLATE Japanese_CI_AS NULL ,
	[headertext] [varchar] (256) COLLATE Japanese_CI_AS NULL ,
	[default_flg] [tinyint] NULL ,
	[step] [tinyint] NULL ,
	[viewsequence] [tinyint] NULL ,
	[active_flg] [tinyint] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [tap].[mst_summary] (
	[summary_id] [smallint] IDENTITY (1, 1) NOT NULL ,
	[summary_nm] [varchar] (40) COLLATE Japanese_CI_AS NULL ,
	[summary_ip] [varchar] (40) COLLATE Japanese_CI_AS NULL ,
	[basicinfo] [varchar] (100) COLLATE Japanese_CI_AS NULL ,
	[importantinfo] [varchar] (40) COLLATE Japanese_CI_AS NULL ,
	[satisfiedinfo] [varchar] (40) COLLATE Japanese_CI_AS NULL ,
	[stayinfo] [varchar] (128) COLLATE Japanese_CI_AS NULL ,
	[csinfo] [varchar] (40) COLLATE Japanese_CI_AS NULL ,
	[owner] [varchar] (35) COLLATE Japanese_CI_AS NULL ,
	[resort_code] [bigint] NOT NULL ,
	[active_flg] [tinyint] NOT NULL ,
	[updatedby] [varchar] (20) COLLATE Japanese_CI_AS NULL ,
	[updated] [datetime] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [tap].[mst_user] (
	[user_id] [smallint] IDENTITY (1, 1) NOT NULL ,
	[user_nm] [varchar] (40) COLLATE Japanese_CI_AS NULL ,
	[user_ip] [varchar] (20) COLLATE Japanese_CI_AS NOT NULL ,
	[role_flg] [tinyint] NULL ,
	[resort_code] [bigint] NULL ,
	[comment] [varchar] (4000) COLLATE Japanese_CI_AS NULL ,
	[active_flg] [tinyint] NOT NULL ,
	[updatedby] [varchar] (20) COLLATE Japanese_CI_AS NULL ,
	[updated] [datetime] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [tap].[oth_scenarioconfiguration] (
	[questionnairesheet_id] [smallint] NOT NULL ,
	[scenario_id] [tinyint] NOT NULL ,
	[default_flg] [tinyint] NULL ,
	[step] [tinyint] NULL ,
	[viewsequence] [tinyint] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [tap].[oth_sheetconfiguration] (
	[questionnairesheet_id] [smallint] NOT NULL ,
	[questionnaire_id] [smallint] NOT NULL ,
	[owner] [varchar] (35) COLLATE Japanese_CI_AS NOT NULL ,
	[viewsequence] [tinyint] NULL ,
	[viewtype] [tinyint] NULL ,
	[scenario] [tinyint] NOT NULL ,
	[resort_code] [bigint] NOT NULL ,
	[questionnaire_text] [varchar] (400) COLLATE Japanese_CI_AS NULL ,
	[leadmessage] [varchar] (256) COLLATE Japanese_CI_AS NULL ,
	[settype] [tinyint] NULL ,
	[answertype] [tinyint] NULL ,
	[memo] [text] COLLATE Japanese_CI_AS NULL ,
	[labeltext] [varchar] (400) COLLATE Japanese_CI_AS NULL ,
	[labelmemo] [varchar] (50) COLLATE Japanese_CI_AS NULL ,
	[parent] [smallint] NULL ,
	[active_flg] [tinyint] NOT NULL ,
	[updatedby] [varchar] (20) COLLATE Japanese_CI_AS NULL ,
	[updated] [datetime] NULL ,
	[cs_qlabel] [varchar] (16) COLLATE Japanese_CI_AS NULL 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO

CREATE TABLE [tap].[rtm_area] (
	[area_code] [bigint] NOT NULL ,
	[name] [varchar] (256) COLLATE Japanese_CI_AS NOT NULL ,
	[wk_area] [varchar] (50) COLLATE Japanese_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [tap].[rtm_customer] (
	[customer_code] [bigint] NULL ,
	[customernumber] [varchar] (128) COLLATE Japanese_CI_AS NOT NULL ,
	[customercategory] [varchar] (32) COLLATE Japanese_CI_AS NULL ,
	[customerrank] [varchar] (32) COLLATE Japanese_CI_AS NULL ,
	[name] [varchar] (256) COLLATE Japanese_CI_AS NULL ,
	[name_kana] [varchar] (256) COLLATE Japanese_CI_AS NULL ,
	[gender_code] [bigint] NOT NULL ,
	[birth_date] [varchar] (256) COLLATE Japanese_CI_AS NULL ,
	[area_code] [bigint] NOT NULL ,
	[address_zipcode] [varchar] (256) COLLATE Japanese_CI_AS NULL ,
	[address_segment_1] [varchar] (256) COLLATE Japanese_CI_AS NULL ,
	[address_segment_2] [varchar] (256) COLLATE Japanese_CI_AS NULL ,
	[address_segment_3] [varchar] (256) COLLATE Japanese_CI_AS NULL ,
	[phone_number_1] [varchar] (256) COLLATE Japanese_CI_AS NULL ,
	[phone_number_2] [varchar] (256) COLLATE Japanese_CI_AS NULL ,
	[mail_address_1] [varchar] (256) COLLATE Japanese_CI_AS NULL ,
	[mail_address_2] [varchar] (256) COLLATE Japanese_CI_AS NULL ,
	[post_permission_code] [bigint] NULL ,
	[mail_permission_code] [bigint] NULL ,
	[information] [varchar] (4096) COLLATE Japanese_CI_AS NULL ,
	[active_flg] [tinyint] NOT NULL ,
	[married_flg] [tinyint] NOT NULL ,
	[repeater] [tinyint] NULL ,
	[marketingarea] [varchar] (32) COLLATE Japanese_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [tap].[rtm_resort] (
	[resort_code] [bigint] NOT NULL ,
	[name] [varchar] (256) COLLATE Japanese_CI_AS NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [tap].[rtt_marketing] (
	[reserve_code] [bigint] NOT NULL ,
	[room_code] [bigint] NOT NULL ,
	[controldiv_id] [bigint] NOT NULL ,
	[control_id] [varchar] (128) COLLATE Japanese_CI_AS NULL 
) ON [PRIMARY]
GO

CREATE TABLE [tap].[rtt_reserve] (
	[reserve_code] [bigint] NOT NULL ,
	[room_code] [bigint] NOT NULL ,
	[customernumber] [varchar] (32) COLLATE Japanese_CI_AS NOT NULL ,
	[arrive_date] [varchar] (64) COLLATE Japanese_CI_AS NULL ,
	[nights] [bigint] NOT NULL ,
	[depart_date] [varchar] (64) COLLATE Japanese_CI_AS NULL ,
	[cancel_code] [bigint] NULL ,
	[information] [varchar] (4096) COLLATE Japanese_CI_AS NULL ,
	[lateout_flg] [tinyint] NOT NULL ,
	[manpersoncount] [smallint] NOT NULL ,
	[womenpersoncount] [smallint] NOT NULL ,
	[childpersoncount_a] [smallint] NOT NULL ,
	[childpersoncount_b] [smallint] NOT NULL ,
	[childpersoncount_c] [smallint] NOT NULL ,
	[childpersoncount_d] [smallint] NOT NULL ,
	[roomtype] [varchar] (32) COLLATE Japanese_CI_AS NULL ,
	[roomcontrol] [varchar] (32) COLLATE Japanese_CI_AS NULL ,
	[reservationroute] [varchar] (128) COLLATE Japanese_CI_AS NULL ,
	[totalsalesamount] [bigint] NOT NULL ,
	[totalsalesamountdividedbynights] [bigint] NOT NULL ,
	[personunitprice] [bigint] NOT NULL ,
	[roomcharge] [bigint] NOT NULL ,
	[optionamount] [bigint] NOT NULL ,
	[restaurantamount] [bigint] NOT NULL ,
	[referencenumber] [varchar] (128) COLLATE Japanese_CI_AS NULL ,
	[surveymethod] [tinyint] NOT NULL 
) ON [PRIMARY]
GO

CREATE TABLE [tap].[rtt_shopresult] (
	[reserve_code] [bigint] NOT NULL ,
	[room_code] [bigint] NOT NULL ,
	[shop_id] [bigint] NOT NULL ,
	[shop_nm] [varchar] (128) COLLATE Japanese_CI_AS NOT NULL ,
	[amount] [bigint] NOT NULL 
) ON [PRIMARY]
GO

ALTER TABLE [tap].[h_logintrack] WITH NOCHECK ADD 
	CONSTRAINT [PK_h_logintrack] PRIMARY KEY  CLUSTERED 
	(
		[login_id],
		[login_dt]
	)  ON [PRIMARY] 
GO

ALTER TABLE [tap].[hst_answers] WITH NOCHECK ADD 
	CONSTRAINT [pk_hst_answers] PRIMARY KEY  CLUSTERED 
	(
		[referencenumber],
		[questionnairesheet_id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [tap].[hst_answersdetails] WITH NOCHECK ADD 
	CONSTRAINT [pk_hst_answersdetails] PRIMARY KEY  CLUSTERED 
	(
		[referencenumber],
		[questionnairesheet_id],
		[questionnaire_id],
		[answer]
	)  ON [PRIMARY] 
GO

ALTER TABLE [tap].[hst_valueinformation] WITH NOCHECK ADD 
	CONSTRAINT [pk_hst_valueinformation] PRIMARY KEY  CLUSTERED 
	(
		[reserve_code],
		[room_code]
	)  ON [PRIMARY] 
GO

ALTER TABLE [tap].[m_loginuser] WITH NOCHECK ADD 
	CONSTRAINT [PK_m_loginuser] PRIMARY KEY  CLUSTERED 
	(
		[login_id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [tap].[mst_answeroptions] WITH NOCHECK ADD 
	CONSTRAINT [pk_mst_answeroptions] PRIMARY KEY  CLUSTERED 
	(
		[questionnaire_id],
		[options_no]
	)  ON [PRIMARY] 
GO

ALTER TABLE [tap].[mst_confirm] WITH NOCHECK ADD 
	CONSTRAINT [pk_mst_confirm] PRIMARY KEY  CLUSTERED 
	(
		[questionnaire_id],
		[confirm_q],
		[options_no]
	)  ON [PRIMARY] 
GO

ALTER TABLE [tap].[mst_control] WITH NOCHECK ADD 
	CONSTRAINT [pk_mst_control] PRIMARY KEY  CLUSTERED 
	(
		[controldiv_id],
		[control_id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [tap].[mst_mailtemplate] WITH NOCHECK ADD 
	CONSTRAINT [pk_mst_mailtemplate] PRIMARY KEY  CLUSTERED 
	(
		[mailtemplate_id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [tap].[mst_questionnaire] WITH NOCHECK ADD 
	CONSTRAINT [pk_mst_questionnaire] PRIMARY KEY  CLUSTERED 
	(
		[questionnaire_id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [tap].[mst_questionnairecategory] WITH NOCHECK ADD 
	CONSTRAINT [pk_mst_questionnairecategory] PRIMARY KEY  CLUSTERED 
	(
		[questionnairecategory_id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [tap].[mst_questionnairesheet] WITH NOCHECK ADD 
	CONSTRAINT [pk_mst_questionnairesheet] PRIMARY KEY  CLUSTERED 
	(
		[questionnairesheet_id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [tap].[mst_room] WITH NOCHECK ADD 
	CONSTRAINT [pk_mst_room] PRIMARY KEY  CLUSTERED 
	(
		[room_id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [tap].[mst_scenario] WITH NOCHECK ADD 
	CONSTRAINT [pk_mst_scenario] PRIMARY KEY  CLUSTERED 
	(
		[scenario_id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [tap].[mst_summary] WITH NOCHECK ADD 
	CONSTRAINT [pk_mst_summary] PRIMARY KEY  CLUSTERED 
	(
		[summary_id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [tap].[mst_user] WITH NOCHECK ADD 
	CONSTRAINT [pk_mst_user] PRIMARY KEY  CLUSTERED 
	(
		[user_id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [tap].[oth_scenarioconfiguration] WITH NOCHECK ADD 
	CONSTRAINT [pk_oth_scenarioconfiguration] PRIMARY KEY  CLUSTERED 
	(
		[questionnairesheet_id],
		[scenario_id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [tap].[oth_sheetconfiguration] WITH NOCHECK ADD 
	CONSTRAINT [pk_oth_sheetconfiguration] PRIMARY KEY  CLUSTERED 
	(
		[questionnairesheet_id],
		[questionnaire_id],
		[owner]
	)  ON [PRIMARY] 
GO

ALTER TABLE [tap].[rtm_area] WITH NOCHECK ADD 
	CONSTRAINT [pk_rtm_area_code] PRIMARY KEY  CLUSTERED 
	(
		[area_code]
	)  ON [PRIMARY] 
GO

ALTER TABLE [tap].[rtm_customer] WITH NOCHECK ADD 
	CONSTRAINT [pk_rtm_customer] PRIMARY KEY  CLUSTERED 
	(
		[customernumber]
	)  ON [PRIMARY] 
GO

ALTER TABLE [tap].[rtm_resort] WITH NOCHECK ADD 
	CONSTRAINT [pk_rtm_resort] PRIMARY KEY  CLUSTERED 
	(
		[resort_code]
	)  ON [PRIMARY] 
GO

ALTER TABLE [tap].[rtt_marketing] WITH NOCHECK ADD 
	CONSTRAINT [pk_rtt_marketing] PRIMARY KEY  CLUSTERED 
	(
		[reserve_code],
		[room_code],
		[controldiv_id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [tap].[rtt_reserve] WITH NOCHECK ADD 
	CONSTRAINT [pk_rtt_reserve] PRIMARY KEY  CLUSTERED 
	(
		[reserve_code]
	)  ON [PRIMARY] 
GO

ALTER TABLE [tap].[rtt_shopresult] WITH NOCHECK ADD 
	CONSTRAINT [pk_rtt_shopresult] PRIMARY KEY  CLUSTERED 
	(
		[reserve_code],
		[room_code],
		[shop_id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [tap].[h_logintrack] ADD 
	CONSTRAINT [DF_h_logintrack_login_cnt] DEFAULT (1) FOR [login_cnt]
GO

ALTER TABLE [tap].[hst_valueinformation] ADD 
	CONSTRAINT [df_hst_valueinformation_checkout] DEFAULT (0) FOR [checkout],
	CONSTRAINT [df_hst_valueinformation_active_flg] DEFAULT (1) FOR [active_flg]
GO

ALTER TABLE [tap].[mst_answeroptions] ADD 
	CONSTRAINT [df_mst_answeroptions_active_flg] DEFAULT (1) FOR [active_flg]
GO

ALTER TABLE [tap].[mst_control] ADD 
	CONSTRAINT [df_mst_control_active_flg] DEFAULT (1) FOR [active_flg]
GO

ALTER TABLE [tap].[mst_mailtemplate] ADD 
	CONSTRAINT [df_mst_mailtemplate_active_flg] DEFAULT (1) FOR [active_flg]
GO

ALTER TABLE [tap].[mst_questionnaire] ADD 
	CONSTRAINT [df_mst_questionnaire_active_flg] DEFAULT (1) FOR [active_flg]
GO

ALTER TABLE [tap].[mst_questionnairesheet] ADD 
	CONSTRAINT [df_mst_questionnairesheet_active_flg] DEFAULT (1) FOR [active_flg]
GO

ALTER TABLE [tap].[mst_room] ADD 
	CONSTRAINT [df_mst_room_active_flg] DEFAULT (1) FOR [active_flg]
GO

ALTER TABLE [tap].[mst_scenario] ADD 
	CONSTRAINT [df_mst_scenario_active_flg] DEFAULT (1) FOR [active_flg]
GO

ALTER TABLE [tap].[mst_summary] ADD 
	CONSTRAINT [df_mst_summary_active_flg] DEFAULT (1) FOR [active_flg]
GO

ALTER TABLE [tap].[mst_user] ADD 
	CONSTRAINT [df_mst_user_active_flg] DEFAULT (1) FOR [active_flg]
GO

ALTER TABLE [tap].[oth_sheetconfiguration] ADD 
	CONSTRAINT [df_oth_sheetconfiguration_active_flg] DEFAULT (1) FOR [active_flg]
GO

ALTER TABLE [tap].[rtm_customer] ADD 
	CONSTRAINT [df_rtm_customer_gender_code] DEFAULT (100000000000) FOR [gender_code],
	CONSTRAINT [df_rtm_customer_area_code] DEFAULT (100000000000) FOR [area_code],
	CONSTRAINT [df_rtm_customer_active_flg] DEFAULT (1) FOR [active_flg],
	CONSTRAINT [df_rtm_customer_married_flg] DEFAULT (0) FOR [married_flg]
GO

ALTER TABLE [tap].[rtt_reserve] ADD 
	CONSTRAINT [df_rtt_reserve_nights] DEFAULT (0) FOR [nights],
	CONSTRAINT [df_rtt_reserve_cancel_code] DEFAULT (100000000000) FOR [cancel_code],
	CONSTRAINT [df_rtt_reserve_lateout_flg] DEFAULT (0) FOR [lateout_flg],
	CONSTRAINT [df_rtt_reserve_manpersoncount] DEFAULT (0) FOR [manpersoncount],
	CONSTRAINT [df_rtt_reserve_womenpersoncount] DEFAULT (0) FOR [womenpersoncount],
	CONSTRAINT [df_rtt_reserve_childpersoncount_a] DEFAULT (0) FOR [childpersoncount_a],
	CONSTRAINT [df_rtt_reserve_childpersoncount_b] DEFAULT (0) FOR [childpersoncount_b],
	CONSTRAINT [df_rtt_reserve_childpersoncount_c] DEFAULT (0) FOR [childpersoncount_c],
	CONSTRAINT [df_rtt_reserve_childpersoncount_d] DEFAULT (0) FOR [childpersoncount_d],
	CONSTRAINT [df_rtt_reserve_totalsalesamount] DEFAULT (0) FOR [totalsalesamount],
	CONSTRAINT [df_rtt_reserve_totalsalesamountdividedbynights] DEFAULT (0) FOR [totalsalesamountdividedbynights],
	CONSTRAINT [df_rtt_reserve_personunitprice] DEFAULT (0) FOR [personunitprice],
	CONSTRAINT [df_rtt_reserve_roomcharge] DEFAULT (0) FOR [roomcharge],
	CONSTRAINT [df_rtt_reserve_optionamount] DEFAULT (0) FOR [optionamount],
	CONSTRAINT [df_rtt_reserve_restaurantamount] DEFAULT (0) FOR [restaurantamount],
	CONSTRAINT [df_rtt_reserve_surveymethod] DEFAULT (0) FOR [surveymethod]
GO

