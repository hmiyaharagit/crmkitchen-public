select	 oth_sheetconfiguration.questionnairesheet_id as expr1
		,mst_questionnaire.questionnaire_id
		,mst_questionnaire.questionnaire_nm as expr2
		,oth_sheetconfiguration.labeltext
		,oth_sheetconfiguration.questionnaire_text
from	 oth_sheetconfiguration inner join
		 mst_questionnaire on 
		 oth_sheetconfiguration.questionnaire_id = mst_questionnaire.questionnaire_id
where	 oth_sheetconfiguration.questionnairesheet_id = 1
and		 oth_sheetconfiguration.owner = oth_sheetconfiguration.owner
order by     oth_sheetconfiguration.viewsequence