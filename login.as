﻿/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	Name			: login
	Description		: 
	Usage			: 
	Attributes		: 
	ExtendsParam	: 
	Method			: 
	Note			: 
	History			: 1) Coded by	
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
import mx.remoting.Service;
import mx.services.Log;
import mx.rpc.RelayResponder;
import mx.remoting.PendingCall;
import mx.remoting.RecordSet;
import mx.remoting.debug.NetDebug;
import mx.remoting.debug.NetDebugConfig;
import mx.rpc.ResultEvent;
import mx.rpc.FaultEvent;
import mx.controls.Alert;
import mx.transitions.easing.*;
import mx.transitions.Tween;
import mx.utils.Delegate;

class login extends MovieClip{

	var gpath:MovieClip			//基準ムービークリップパス
	var gdepth:Number			//Depth
	var sobject:Object			//CFCService
	var applicode:String		//宿泊（00）：新婦（01）：参列（02）
	var menucategory:String		//ＣＳ（cs）：メール（ml）：顧客（cm）
	var menuid:String

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: login
		Description		: Constructor
		Usage			: login(  myPath:MovieClip  );
		Attributes		: myPath:MovieClip	（基準ＭＣパス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function login( myPath:MovieClip ){
		gpath = myPath;
		gdepth = gpath.getNextHighestDepth();

		//初期設定
		initializedData();
	}
	
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: initializedData
		Description		: 設定ファイル読込～システム共通情報設定
		Usage			: initializedData();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/	
	private function initializedData(){
		var LoadObj = new LoadVars();
		var tmp_txt = ""
		LoadObj.owner = this
		LoadObj.onLoad = function(bSuccess) {
			if (bSuccess) {
				//共通情報設定
				_global.Depth = 1;							//	Depth
				_global.hostAddress = this.hostaddress ;	//	ホストアドレス
				_global.servicePath = this.servicepath;		//	CFCClassPath
				_global.uniqueCode = "";					//	テスト用キャッシュ防止用コード
			} else {
				var HeaderMsg = "読込失敗                "
				var ErrorMsg = "設定ファイルの読込に失敗しました。\r管理者へ確認してください。"
				var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, this.owner.gpath, null, null, null)
			}
		};
		LoadObj.load( "setting.txt" );

		//読込内容セット完了監視
		gpath.owner = this
		gpath.onEnterFrame = function(){
			if(_global.hostAddress != undefined && _global.servicePath != undefined ){
				//ログインサービス開始
				this.owner.LoginService();
				delete this.onEnterFrame
			}
		}
		gpath.loginpanel.id_txt.onSetFocus = function() {
			System.IME.setEnabled(false);
		};
		gpath.loginpanel.id_txt.onKillFocus = function() {
			System.IME.setEnabled(true);
		};
		gpath.loginpanel.password_txt.onSetFocus = function() {
			System.IME.setEnabled(false);
		};
		gpath.loginpanel.password_txt.onKillFocus = function() {
			System.IME.setEnabled(true);
		};
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: LoginService
		Description		: Remoting接続～初期設定
		Usage			: LoginService();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function LoginService() {

		var myDebug = mx.remoting.debug.NetDebug.initialize();
		sobject = new Service( "http://" + _global.hostAddress + "/flashservices/gateway/", null , _global.servicePath + ".cfc.login" , null , null ); 
		
		//NetDebbuger設定
		var config:NetDebugConfig = sobject.connection.getDebugConfig();
		config.app_server.trace = true;
		config.client.trace = true;
		
		//CFCメソッド呼出
		var LoginPC:PendingCall = sobject.getUser();
		LoginPC.responder = new RelayResponder( this, "LoginService_Result", "LoginService_Fault" );

	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: LoginService_Result
		Description		: ユーザー情報取得成功時処理
		Usage			: LoginService_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent		（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function LoginService_Result(ReEvt:ResultEvent):Void{
		//ユーザー情報をオブジェクトへ格納
		_global.UserInfo = new Object();
		_global.UserInfo.ipAddress = ReEvt.result.items[0].user_ip;
		_global.UserInfo.role = ReEvt.result.items[0].role_flg;
		_global.UserInfo.summary = ReEvt.result.items[0].summary_id;
		
		if( ReEvt.result.mRecordsAvailable != 0 ){
			setContents();
		} else {
			var HeaderMsg = "認証不可                "
			var ErrorMsg = "認証が許可されませんでした。\rこのシステムを利用することが出来ません。\r管理者へ確認してください。"
			var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, gpath, null, null, null)
		}

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setContents
		Description		: 設定ファイル読込～システム共通情報設定
		Usage			: initializedData();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/	
	function setContents(){
		var owner = this
		var loginpanel = gpath.loginpanel
		loginpanel.id_txt.setFocus();
		loginpanel.loginBtn.onRelease = function(){
			var conditionParam = new Object();
			conditionParam.login_id = this._parent.id_txt.text
			conditionParam.login_pw = this._parent.password_txt.text
			if(conditionParam.login_id.length != 0 || conditionParam.login_pw.length != 0){
				var LoginPC:PendingCall = owner.sobject.getloginUser(conditionParam);
				LoginPC.responder = new RelayResponder(  owner, "Login_Result", "Service_Fault" );
			}else{
				var HeaderMsg = "未入力項目があります                "
				var ErrorMsg = "ＩＤ・パスワードを入力して下さい"
				var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, owner.gpath, null, null, null)
			}
		}

		loginpanel.pBtn.onRelease = function(){
			var conditionParam = new Object();
			conditionParam.login_id = this._parent.id_txt.text
			conditionParam.login_pw = this._parent.password_txt.text

			var LoginPC:PendingCall = owner.sobject.getloginUser(conditionParam);
			LoginPC.responder = new RelayResponder(  owner, "Password_Result", "LoginService_Fault" );

			/*
			if(conditionParam.login_id.length != 0 || conditionParam.login_pw.length != 0){
				var LoginPC:PendingCall = owner.sobject.getloginUser(conditionParam);
				LoginPC.responder = new RelayResponder(  owner, "Password_Result", "LoginService_Fault" );
			}else{
				
				var HeaderMsg = "未入力項目があります                "
				var ErrorMsg = "ＩＤ・パスワードを入力して下さい"
				var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, owner.gpath, null, null, null)
			}
			*/
		}

		loginpanel.aBtn.onRelease = function(){
			var conditionParam = new Object();
			conditionParam.login_id = this._parent.id_txt.text
			conditionParam.login_pw = this._parent.password_txt.text

			var LoginPC:PendingCall = owner.sobject.getloginUser(conditionParam);
			LoginPC.responder = new RelayResponder(  owner, "ask_Result", "LoginService_Fault" );
			/*
			if(conditionParam.login_id.length != 0 || conditionParam.login_pw.length != 0){
				var LoginPC:PendingCall = owner.sobject.getloginUser(conditionParam);
				LoginPC.responder = new RelayResponder(  owner, "ask_Result", "LoginService_Fault" );
			}else{
				var HeaderMsg = "未入力項目があります                "
				var ErrorMsg = "ＩＤ・パスワードを入力して下さい"
				var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, owner.gpath, null, null, null)
			}
			*/
		}

		//	--キーイベント設定
		var keyListener = new Object();
		keyListener.onKeyDown = function() {
			if(Key.getCode()==13){
				var conditionParam = new Object();
				conditionParam.login_id = loginpanel.id_txt.text
				conditionParam.login_pw = loginpanel.password_txt.text
				if(conditionParam.login_id.length != 0 || conditionParam.login_pw.length != 0){
					var LoginPC:PendingCall = owner.sobject.getloginUser(conditionParam);
					LoginPC.responder = new RelayResponder(  owner, "Login_Result", "Service_Fault" );
				}else{
					var HeaderMsg = "未入力項目があります                "
					var ErrorMsg = "ＩＤ・パスワードを入力して下さい"
					var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, owner.gpath, null, null, null)
				}
			}
		};
		Key.addListener(keyListener);

		loginpanel.id_txt.restrict = "A-Za-z0-9\\-\\@\\_\\/\\#\\$\\%\\&\\.";
		loginpanel.password_txt.restrict = "A-Za-z0-9\\-\\@\\_\\/\\#\\$\\%\\&\\.";
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: tubclear
		Description		: FLAファイルからコール
		Usage			: 
		Attributes		: 
		Note			: 
		History			: 1) Coded by
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function tabclear(){
		gpath.applicationtab._x = -1000
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setmenuprop
		Description		: FLAファイルからコール
		Usage			: 
		Attributes		: 
		Note			: 
		History			: 1) Coded by
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function setmenuprop(){
		setbtnevent(gpath.area_00.menu_cs,true)
		setbtnevent(gpath.area_01.menu_cs,true)
		setbtnevent(gpath.area_02.menu_cs,true)

		//	--メインユーザー
		if(_global.UserInfo.login_dv == 's' || _global.UserInfo.login_dv == 'a'){
			setbtnevent(gpath.area_00.menu_cm,true)
		} else {
			setbtnevent(gpath.area_00.menu_cm,false)
		}

		//	--メールのみユーザー
		if(_global.UserInfo.login_dv == 's' || _global.UserInfo.login_dv == 'a' || _global.UserInfo.login_dv == 'b'){
			setbtnevent(gpath.area_00.menu_ml,true)
		} else {
			setbtnevent(gpath.area_00.menu_ml,false)
		}

		//	--表示ファイル設定
		gpath.applicationtab.menu_cs["01btn"].loadfile = "app00/oa.swf";
		gpath.applicationtab.menu_cs["02btn"].loadfile = "app00/cs.swf";
		gpath.applicationtab.menu_cs["03btn"].loadfile = "app00/cs.swf";
		gpath.applicationtab.menu_cs["04btn"].loadfile = "app00/mkt.swf";
		gpath.applicationtab.menu_cs["05btn"].loadfile = "app00/rpt.swf";
		gpath.applicationtab.menu_cs["06btn"].loadfile = "";
		gpath.applicationtab.menu_cs["06btn"].webcs 	= "app00/webcs/index.cfm";
		gpath.applicationtab.menu_cs["07btn"].loadfile	= "app00/paper.swf";
//		gpath.applicationtab.menu_cs["08btn"].loadfile	= "app00/return.swf";
		gpath.applicationtab.menu_ml["01btn"].loadfile = "app00/mailPackage.swf";
		gpath.applicationtab.menu_ml["02btn"].loadfile = "app00/mailTemplate.swf";
		gpath.applicationtab.menu_cm["01btn"].loadfile = "app00/defaultSearch.swf";


		//初期表示設定
		gpath.cs01screen._visible = gpath.cs01screen.enabled = false
		gpath.cs02screen._visible = gpath.cs02screen.enabled = false
		gpath.cs03screen._visible = gpath.cs03screen.enabled = false
		gpath.cs04screen._visible = gpath.cs04screen.enabled = false
		gpath.cs05screen._visible = gpath.cs05screen.enabled = false
		gpath.cs07screen._visible = gpath.cs07screen.enabled = false
//		gpath.cs08screen._visible = gpath.cs08screen.enabled = false
		gpath.cm01screen._visible = gpath.cm01screen.enabled = false
		gpath.ml01screen._visible = gpath.ml01screen.enabled = false
		gpath.ml02screen._visible = gpath.ml02screen.enabled = false

		gpath.loadingcover.onRelease = function(){}
		gpath.loadingcover.enabled = false

		gpath.dconditionbox._visible = false

		//	--プログレスバー
		gpath.loadingcover.progress._xscale = 1
		gpath.onEnterFrame = function(){
			var wk_scale = this._currentframe/(this._totalframes)*100
			this.loadingcover.progress._xscale = wk_scale
			if(wk_scale==100){
				this.dconditionbox._visible = true
				delete this.onEnterFrame
			}
		}


		//	--各画面読み込み開始
		gpath.gotoAndPlay("screen");
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setbtnevent
		Description		: 
		Usage			: 
		Attributes		: 
		Note			: 
		History			: 1) Coded by
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function setbtnevent(tgt,bl){
		var owner = this
		var sourcemc = tgt
		tgt.enabled = tgt._visible = bl
		var _atween:Object = new Tween(tgt, "_alpha", Back.easeOut, 0, 100, 1.25, true);
		for (var myName in sourcemc) {
			var btnnm = sourcemc[myName]._name.substr(2,3)
			if (btnnm == "btn") {
				sourcemc[myName].onRelease = function(){
					var app = this._parent._parent._name.substr(5,2)
					var menu = this._parent._name.substr(5,2)
					var cd = this._name.substr(0,2)
					owner.tubcontrol(app,menu,cd)
				}
			}
		}
	}
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getloadfiles
		Description		: 
		Usage			: 
		Attributes		: 
		Note			: 
		History			: 1) Coded by
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function getloadfiles(app,menu,cd){
/*
trace("applicode:" + app + " menucategory:" + menu + " menuid:" + cd)
trace("loadfile1: " + gpath.applicationtab["menu_" + menu][cd + "btn"].loadfile)
trace("_name: " + gpath.applicationtab["menu_" + menu][cd + "btn"]._name)
*/
		return gpath.applicationtab["menu_" + menu][cd + "btn"].loadfile
	}
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getwebcsurl
		Description		: 
		Usage			: 
		Attributes		: 
		Note			: 
		History			: 1) Coded by
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function getwebcsurl(app,menu,cd){
		return gpath.applicationtab["menu_" + menu][cd + "btn"].webcs
	}
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: Login_Result
		Description		: ユーザー情報取得成功時処理
		Usage			: Login_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent		（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function tubcontrol(app,menu,cd){
		applicode = app
		menucategory = menu
		menuid = cd
//		trace("applicode:" + applicode + " menucategory:" + menucategory + " menuid:" + menuid)

		//対象ファイル読込
		var loadfile = getloadfiles(app,menu,cd)
		//var toppage:LoadImage = new LoadImage( loadfile , gpath.screen );

		if(loadfile.length==0){
			var webcsurl = getwebcsurl(app,menu,cd)
			gpath.getURL(webcsurl,"_blank" )
		}else{
			gpath.applicationtab.applicode = applicode
			gpath.applicationtab.menucategory = menucategory
			gpath.applicationtab.menuid = menuid
			gpath[ menucategory + menuid + "screen" ].menuid = menuid


			var toppage:LoadImage = new LoadImage( loadfile , gpath[ menucategory + menuid + "screen" ].screenobj );
			gpath.applicationtab._x = 0
			gpath.applicationtab.gotoAndStop("scene" + app)
		}
	}
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: Login_Result
		Description		: ユーザー情報取得成功時処理
		Usage			: Login_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent		（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function Login_Result(ReEvt:ResultEvent):Void{
		var owner = this
		if( ReEvt.result.mRecordsAvailable == 1){
			var alpha_tween:Object = new Tween(gpath.loginpanel, "_alpha", Strong.easeOut, 100, 0, 1, true);

			gpath.loginpanel.id_txt.text = ""
			gpath.loginpanel.password_txt.text = ""
			gpath.loginpanel.loginBtn.label = ""

//			var loadfile = getloadfiles(applicode,menucategory,menuid)
			alpha_tween.delmc = gpath.loginpanel
			alpha_tween.onMotionFinished = function() {
				//owner.gpath.gotoAndPlay("screen");
				owner.gpath.gotoAndPlay("setmenu");
				this.delmc._y = -1000
				_global.UserInfo.login_dv = ReEvt.result.items[0].login_dv;
				_global.resort_code = ReEvt.result.items[0].resort_code;
			};
		} else {//ログイン認証不可
			var HeaderMsg = "認証不可                "
			var ErrorMsg = "ＩＤ・パスワードをご確認下さい。ユーザーが特定できませんでした。"
			var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, gpath, null, null, null)
		}
		
	}
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: LoginService_Fault
		Description		:  ユーザー情報取得失敗時処理
		Usage			: LoginService_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent	（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function LoginService_Fault(FltEvt:FaultEvent):Void{
		var HeaderMsg = "システムメンテナンス中            "
		var ErrorMsg = "只今このシステムを利用することが出来ません。\r管理者へ確認してください。"
		var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, gpath, null, null, null)

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: Password_Result
		Description		: ユーザー情報取得成功時処理　パスワード変更
		Usage			: Password_Result(ReEvt:ResultEvent); 
		Attributes		: ReEvt:ResultEvent		（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function Password_Result(ReEvt:ResultEvent):Void{
		var owner = this
		if( ReEvt.result.mRecordsAvailable == 1){
			//var alpha_tween:Object = new Tween(gpath.loginpanel, "_alpha", Strong.easeOut, 100, 0, 1, true);
			gpath.loginpanel.id_txt.text = ""
			gpath.loginpanel.password_txt.text = ""

			//パスワード変更イベント
			var myClickHandler = new Object();
			myClickHandler = function(evt){

				if(evt.detail == Alert.YES){
					var conditionParam:Object = new Object();

					conditionParam.login_id = _global.UserInfo.login_id;
					conditionParam.login_ow = _global.UserInfo.login_pw;
					conditionParam.login_nw= owner.gpath.loginpanel.pass1_txt.text;

					var LoginPC:PendingCall = owner.sobject.passwordConvert(conditionParam);
					LoginPC.responder = new RelayResponder(  owner, "passwordConvert_Result", "LoginService_Fault" );
				}
			}
			//gpath.loginpanel.removeMovieClip();
			//gpath.attachMovie("passPanel","loginpanel",gdepth++,{_x:gpath.loginpanel._x,_y:gpath.loginpanel._y,_alpha:0});
			gpath.loginpanel.gotoAndStop(2)
			var alpha_tween:Object = new Tween(gpath.loginpanel, "_alpha", Strong.easeOut, 0, 100, 1, true);
			alpha_tween.onMotionFinished = function() {
				_global.UserInfo.login_id = ReEvt.result.items[0].login_id;
				_global.UserInfo.login_pw = ReEvt.result.items[0].login_pw;
				_global.UserInfo.login_dv = ReEvt.result.items[0].login_dv;

				//キャンセルボタン
				owner.gpath.loginpanel.uBtn.onRelease = function() {
					if (owner.gpath.loginpanel.pass1_txt.text.length == 0) {
						var AlertObj = mx.controls.Alert.show("「パスワード」が未入力です。", "パスワード変更                 ", Alert.YES);
						Selection.setFocus(owner.gpath.loginpanel.pass1_txt); 
					} 
					else if(owner.gpath.loginpanel.pass2_txt.text.length == 0) {
						var AlertObj = mx.controls.Alert.show("「入力確認」が未入力です。", "パスワード変更                 ", Alert.YES);
						Selection.setFocus(owner.gpath.loginpanel.pass2_txt); 
					}
					else if (owner.gpath.loginpanel.pass1_txt.text != owner.gpath.loginpanel.pass2_txt.text) {
						var AlertObj = mx.controls.Alert.show("「新パスワード」と「入力確認」が一致しません。\r入力しなおしてください。", "パスワード変更                 ", Alert.YES);
						Selection.setFocus(owner.gpath.loginpanel.pass1_txt); 
					}
					else {
						var AlertObj = mx.controls.Alert.show("パスワードを変更します。よろしいですか？", "パスワード変更                 ", Alert.YES | Alert.NO, owner.gpath, myClickHandler, null , Alert.OK);
					}
				}
				//キャンセルボタン
				owner.gpath.loginpanel.cBtn.onRelease = function() {
					owner.endPassConvert();
				}
			}

		} else {//ログイン認証不可
			gpath.loginpanel.alert_txt.text = "ＩＤ・パスワードをご確認下さい。ユーザーが特定できませんでした。";
		}
		
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: ask_Result
		Description		: ユーザー情報取得成功時処理　パスワード変更
		Usage			: ask_Result(ReEvt:ResultEvent); 
		Attributes		: ReEvt:ResultEvent		（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function ask_Result():Void{
		var owner = this
		//var alpha_tween:Object = new Tween(gpath.loginpanel, "_alpha", Strong.easeOut, 100, 0, 1, true);
		gpath.loginpanel.id_txt.text = ""
		gpath.loginpanel.password_txt.text = ""

		//パスワード確認イベント
		var myClickHandler = new Object();
		myClickHandler = function(evt){

			if(evt.detail == Alert.YES){
				var conditionParam:Object = new Object();
				conditionParam.mailaddress = owner.gpath.loginpanel.pass1_txt.text;
				conditionParam.ask = owner.gpath.loginpanel.pass2_txt.text;
				var LoginPC:PendingCall = owner.sobject.passwordCheck(conditionParam);
				LoginPC.responder = new RelayResponder(  owner, "passwordCheck_Result", "LoginService_Fault" );
			}
		}

		gpath.loginpanel.gotoAndStop(3)
		gpath.loginpanel.pass1_txt.onSetFocus = function() {
			System.IME.setEnabled(false);
		};
		gpath.loginpanel.pass1_txt.onKillFocus = function() {
			System.IME.setEnabled(true);
		};
		gpath.loginpanel.passwordCheck.onSetFocus = function() {
			System.IME.setEnabled(false);
		};
		gpath.loginpanel.passwordCheck.onKillFocus = function() {
			System.IME.setEnabled(true);
		};
		var alpha_tween:Object = new Tween(gpath.loginpanel, "_alpha", Strong.easeOut, 0, 100, 1, true);
		alpha_tween.onMotionFinished = function() {
			//キャンセルボタン
			owner.gpath.loginpanel.uBtn.onRelease = function() {
				if (owner.gpath.loginpanel.pass1_txt.text.length == 0) {
					var AlertObj = mx.controls.Alert.show("「メールアドレス」が未入力です。", "ID/パスワード問い合わせ         ", Alert.YES);
					Selection.setFocus(owner.gpath.loginpanel.pass1_txt); 
				} 
				else if(owner.gpath.loginpanel.pass2_txt.text.length == 0) {
					var AlertObj = mx.controls.Alert.show("「問い合わせ内容」が未入力です。", "ID/パスワード問い合わせ         ", Alert.YES);
					Selection.setFocus(owner.gpath.loginpanel.pass2_txt); 
				}
				else if(owner.gpath.loginpanel.pass1_txt.text.indexOf("@") == -1) {
					var AlertObj = mx.controls.Alert.show("「メールアドレス」の形式が正しくありません。", "ID/パスワード問い合わせ         ", Alert.YES);
					Selection.setFocus(owner.gpath.loginpanel.pass2_txt); 
				}
				else {
					var AlertObj = mx.controls.Alert.show("問い合わせメールを送信します。よろしいですか？", "ID/パスワード問い合わせ         ", Alert.YES | Alert.NO, owner.gpath, myClickHandler, null , Alert.OK);
				}
			}
			//キャンセルボタン
			owner.gpath.loginpanel.cBtn.onRelease = function() {
				owner.endPassConvert();
			}
		}
	}
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: passwordConvert_Result
		Description		: パスワード問い合わせ終了
		Usage			: passwordConvert_Result(void); 
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function passwordCheck_Result():Void{
		var owner = this;
		var myClickHandler = new Object();
		myClickHandler = function (evt) {
			owner.gpath.loginpanel.pass1_txt.text = "";
			owner.gpath.loginpanel.pass2_txt.text = "";
			owner.gpath.loginpanel.uBtn.label = "";
			owner.gpath.loginpanel.cBtn.label = "";
			var alpha_tween:Object = new Tween(owner.gpath.loginpanel, "_alpha", Strong.easeOut, 100, 0, 1, true);

			alpha_tween.delmc = owner.gpath.loginpanel;
			alpha_tween.onMotionFinished = function() {
				owner.endPassConvert();
			};
		};
		var AlertObj = mx.controls.Alert.show("問い合わせメールを送信しました。", "ID/パスワード問い合わせ         ", Alert.YES, gpath, myClickHandler, null , Alert.OK);
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: endPassConvert
		Description		: パワード変更処理終了
		Usage			: endPassConvert(void); 
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function endPassConvert():Void{
		gpath.loginpanel.gotoAndStop(1)
		LoginService();
		var alpha_tween:Object = new Tween(gpath.loginpanel, "_alpha", Strong.easeOut, 0, 100, 1, true);
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: passwordConvert_Result
		Description		: パワード変更処理終了
		Usage			: passwordConvert_Result(void); 
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function passwordConvert_Result():Void{
		var owner = this;
		var myClickHandler = new Object();
		myClickHandler = function (evt) {
			owner.gpath.loginpanel.pass1_txt.text = "";
			owner.gpath.loginpanel.pass2_txt.text = "";
			owner.gpath.loginpanel.uBtn.label = "";
			owner.gpath.loginpanel.cBtn.label = "";
			var alpha_tween:Object = new Tween(owner.gpath.loginpanel, "_alpha", Strong.easeOut, 100, 0, 1, true);
	
			alpha_tween.delmc = owner.gpath.loginpanel;
			alpha_tween.onMotionFinished = function() {
				owner.endPassConvert();
			};
		};
		var AlertObj = mx.controls.Alert.show("パスワードを変更しました。", "パスワード変更", Alert.YES, gpath, myClickHandler, null , Alert.OK);
	}

}
