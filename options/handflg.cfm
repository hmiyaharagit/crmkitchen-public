<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=shift_jis" />
<title>手渡し管理</title>

</head>

<body>
<cfsavecontent variable="showAlert">
    var myClickHandler = function (evt){
        if (evt.detail == mx.controls.Alert.OK){
            this._parent.submitForm();
        }
    }
    alert("手渡し情報を更新しますか ?", "手渡情報更新確認", mx.controls.Alert.OK | mx.controls.Alert.CANCEL, myClickHandler);
</cfsavecontent>

<cftry>
<cfif IsDefined("form.sdate")>
<cfif IsDefined("form.gridCancele") eq false>

    <cftransaction action="begin">

    <cfloop index="i" from="1" to="#ArrayLen(form.g_handover.rowstatus.action)#">

        <cfswitch expression="#form.g_handover.rowstatus.action[i]#">

        <cfcase value="U">

            <cfquery datasource="#Application.datasource#" name="handover_delete">
                delete 
                from 
                    hst_handinformation
                where
                    (referencenumber = '#form.g_handover.照会用番号[i]#')
            </cfquery>

            <cfif #form.g_handover.手渡し[i]# eq 0>

                <cfquery datasource="#Application.datasource#" name="handover_insert">
                    insert into hst_handinformation 
                        (
                        referencenumber, 
                        comment 
                        )
                    values 
                        (
                        '#form.g_handover.照会用番号[i]#',
                        '#form.g_handover.理由[i]#'
                        )
                </cfquery>

            </cfif>

        </cfcase>

        </cfswitch>

    </cfloop>

    </cftransaction>

</cfif>
</cfif>

<cfquery datasource="#Application.datasource#" name="q_handover">
    select 
        case isnull(hst_handinformation.referencenumber,'1') 
            when '1' then 1
            else        0
        end as 手渡対象,
        rtt_reserve.referencenumber as 照会用番号, 
        rtt_reserve.depart_date as 出発日,
        case isnull(len(rtm_customer.name),0)
            when 0 then rtm_customer.name_kana
            else        rtm_customer.name
        end as 氏名,
        hst_handinformation.comment as 理由
    from 
        (rtt_reserve inner join rtm_customer on 
         rtt_reserve.customernumber = rtm_customer.customernumber) 
                     left  join hst_handinformation on 
         rtt_reserve.referencenumber = hst_handinformation.referencenumber 
    where
        <cfif IsDefined("form.dstartdate")>
            (rtt_reserve.depart_date >= '#form.dstartdate#') and 
            (rtt_reserve.depart_date <= '#form.denddate#') and 
        <cfelse>
            (rtt_reserve.depart_date >= '#form.sdate#') and 
            (rtt_reserve.depart_date <= '#form.edate#') and 
        </cfif>
            (rtt_reserve.surveymethod = 1)
    order by 
        rtt_reserve.depart_date, 
        rtt_reserve.referencenumber
</cfquery>

<!--- cfdump var=#q_handover# --->
手渡対象リストより手渡ししない対象データのチェックを外して、【手渡し更新】ボタンをクリックして下さい。
<br>
<cfform name="f_handover" action="handflg.cfm" method="post" format="Flash" skin="haloGreen" onLoad="initApp()">

    <cfgrid name="g_handover" format="Flash" height="420" autoWidth="no" query="q_handover"
            selectMode="Edit" font="Verdana" fontsize="12"
            rowHeight="56">

        <cfgridcolumn name="手渡対象" select="yes" type="boolean" width="70">
        <cfgridcolumn name="照会用番号" select="no" width="120">
        <cfgridcolumn name="出発日" select="no" width="100">
        <cfgridcolumn name="氏名" select="no" width="180">
        <cfgridcolumn name="理由" select="yes" type="string_noCase " width="300">
    </cfgrid>

	<cfformitem type="script">
	function initApp()
	{
		g_handover.variableRowHeight=true; 
		g_handover.getColumnAt(5).wordWrap=true;
	}
	</cfformitem>

    <cfformgroup type="horizontal">

        <cfinput type="button" name="gridEntered" onClick="#showAlert#" value="手渡し更新" />
        <cfinput type="submit" name="gridCancele" value="キャンセル">
<!---
        <cfinput type="button" name="gridClose" onClick="getURL('javascript:window.close()')" value="閉じる" />
--->
        <cfinput type="button" name="gridClose" onClick="getURL('javascript:self.close()')" value="閉じる" />

        <cfif IsDefined("form.dstartdate")>
            <cfinput type="hidden" name="sdate" value="#form.dstartdate#">
            <cfinput type="hidden" name="edate" value="#form.denddate#">
        <cfelse>
            <cfinput type="hidden" name="sdate" value="#form.sdate#">
            <cfinput type="hidden" name="edate" value="#form.edate#">
        </cfif>

    </cfformgroup>

</cfform>

<cfcatch>
    <cftransaction action="rollback"/>
    <cfoutput>手渡し情報の更新処理ができませんでした。</cfoutput>
    <cfabort>
</cfcatch>

</cftry>
</body>
</html>
