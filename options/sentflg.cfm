<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=shift_jis" />
<title>送信履歴削除</title>

</head>

<body>
<cfsavecontent variable="showAlert">
    var myClickHandler = function (evt){
        if (evt.detail == mx.controls.Alert.OK){
            this._parent.submitForm();
        }
    }
    alert("送信履歴情報を削除しますか ?", "送信履歴削除確認", mx.controls.Alert.OK | mx.controls.Alert.CANCEL, myClickHandler);
</cfsavecontent>

<cftry>
<cfif IsDefined("form.sdate")>
<cfif IsDefined("form.gridCancele") eq false>

    <cftransaction action="begin">

    <cfloop index="i" from="1" to="#ArrayLen(form.g_deliver.rowstatus.action)#">

        <cfswitch expression="#form.g_deliver.rowstatus.action[i]#">

        <cfcase value="U">
            <cfif #form.g_deliver.削除対象[i]#>
                <cfquery datasource="#Application.datasource#" name="del_deliver">
                    delete 
                    from 
                        hst_deliverinformation
                    where
                        (referencenumber = '#form.g_deliver.照会用番号[i]#')
                </cfquery>
            </cfif>
        </cfcase>

        <cfcase value="D">
            <cfquery datasource="#Application.datasource#" name="del_deliver">
                delete 
                from 
                    hst_deliverinformation
                where
                    (referencenumber = '#form.g_deliver.照会用番号[i]#')
            </cfquery>
        </cfcase>

        </cfswitch>

    </cfloop>

    </cftransaction>

</cfif>
</cfif>

<cfquery datasource="#Application.datasource#" name="q_deliver">
    select distinct
        convert(bit, 0) as 削除対象, 
        hst_deliverinformation.referencenumber as 照会用番号, 
        hst_deliverinformation.deliverd_date as 送信日付,
        rtm_customer.name as 氏名,
        case isnull(len(rtm_customer.mail_address_1),0) 
            when 0 then rtm_customer.mail_address_2
            else        rtm_customer.mail_address_1
        end as メールアドレス,
        rtt_reserve.depart_date
    from 
        (hst_deliverinformation inner join rtt_reserve on 
         hst_deliverinformation.referencenumber = rtt_reserve.referencenumber)
                                inner join rtm_customer on
         rtt_reserve.customernumber = rtm_customer.customernumber
    where
        <cfif IsDefined("form.dstartdate")>
            (rtt_reserve.depart_date >= '#form.dstartdate#') and 
            (rtt_reserve.depart_date <= '#form.denddate#') 
        <cfelse>
            (rtt_reserve.depart_date >= '#form.sdate#') and 
            (rtt_reserve.depart_date <= '#form.edate#') 
        </cfif>
    order by 
        rtt_reserve.depart_date, 
        hst_deliverinformation.referencenumber
</cfquery>

<!--- cfdump var=#q_deliver# --->
送信履歴から削除したい対象データにチェックをつけて、【送信履歴削除】ボタンをクリックして下さい。
<br>
<cfform name="f_deliver" action="sentflg.cfm" method="post" format="Flash" skin="haloGreen">

    <cfgrid name="g_deliver" format="Flash" height="420" width="800" query="q_deliver"
            selectMode="Edit" font="Verdana" fontsize="12"
            rowHeight="28">

        <cfgridcolumn name="削除対象" select="yes" type="boolean" width="70">
        <cfgridcolumn name="照会用番号" select="no" width="120">
        <cfgridcolumn name="送信日付" select="no" mask="yyyy/mm/dd" width="100">
        <cfgridcolumn name="氏名" select="no" width="180">
        <cfgridcolumn name="メールアドレス" select="no">
        <cfgridcolumn name="depart_date" display="no">
    </cfgrid>

    <cfformgroup type="horizontal">

        <cfinput type="button" name="gridEntered" onClick="#showAlert#" value="送信履歴削除" />
        <cfinput type="submit" name="gridCancele" value="キャンセル">
<!---
        <cfinput type="button" name="gridClose" onClick="getURL('javascript:window.close()')" value="閉じる" />
--->
        <cfinput type="button" name="gridClose" onClick="getURL('javascript:self.close()')" value="閉じる" />

        <cfif IsDefined("form.dstartdate")>
            <cfinput type="hidden" name="sdate" value="#form.dstartdate#">
            <cfinput type="hidden" name="edate" value="#form.denddate#">
        <cfelse>
            <cfinput type="hidden" name="sdate" value="#form.sdate#">
            <cfinput type="hidden" name="edate" value="#form.edate#">
        </cfif>

    </cfformgroup>

</cfform>

<cfcatch>
    <cftransaction action="rollback"/>
    <cfoutput>送信履歴情報の削除処理ができませんでした。</cfoutput>
    <cfabort>
</cfcatch>

</cftry>
</body>
</html>
