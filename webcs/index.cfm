<cfparam name="form.referencenumber" default="">
<cfquery datasource="#dsn#" name="q_rsv_cst">
	select	 rsv.reserve_code
			,rsv.room_code
			,rsv.customernumber
			,rsv.arrive_date
			,rsv.nights
			,rsv.manpersoncount + rsv.womenpersoncount +  rsv.childpersoncount_a + rsv.childpersoncount_b + rsv.childpersoncount_c + rsv.childpersoncount_d as personcount
			,rsv.depart_date
			,rsv.roomtype
			,ct4.control_nm as reservationroute
			,ct8.control_nm as surveymethod
			,cst.name
			,cst.name_kana
			,cst.married_flg
			,cst.birth_date
			,cst.repeater
	from	 (select control_id,control_nm from #dbu#.mst_control where controldiv_id=4) ct4
			,(select control_id,control_nm from #dbu#.mst_control where controldiv_id=8) ct8
			,#dbu#.rtm_customer cst
			,#dbu#.rtt_reserve rsv
	where	 rsv.referencenumber='#form.referencenumber#'
	and		 rsv.reservationroute*=ct4.control_id
	and		 rsv.surveymethod*=ct8.control_id
	and		 rsv.customernumber=cst.customernumber
</cfquery>
<cfif q_rsv_cst.recordcount neq 0>
	<cfquery datasource="#dsn#" name="q_mkt">
		select	 ctl.controldiv_id
	 			,ctl.control_nm
		from	 #dbu#.rtt_marketing mkt
				,#dbu#.mst_control ctl
		where	 mkt.reserve_code=#q_rsv_cst.reserve_code#
		and		 mkt.controldiv_id=ctl.controldiv_id
		and		 mkt.control_id=ctl.control_id
		and		 mkt.controldiv_id in (5,6)
		order by mkt.controldiv_id
	</cfquery>
	<cfset mkt5="">
	<cfset mkt6="">
	<cfloop query="q_mkt">
		<cfset variables["mkt" & q_mkt.controldiv_id]=q_mkt.control_nm>
	</cfloop>
	<cfquery datasource="#dsn#" name="q_answer">
		select	 registrated
		from	 #dbu#.hst_answers
		where	 referencenumber='#form.referencenumber#'
	</cfquery>
</cfif>
<cfoutput>
<html><head><meta http-equiv="content-type" content="text/html"/><link rel="stylesheet" href="configcss.css"><title>#title#</title></head>
<body onload="<cfif isdefined("form.search") and q_rsv_cst.recordcount eq 0>alert('照会用番号#form.referencenumber#の顧客はありません');</cfif>">
<form method="post" action="index.cfm" onsubmit="if(document.forms[0].referencenumber.value=='')return false">
<table width ="400" border="1" cellspacing="5" cellpadding="5">
	<tr>
		<td class="nouse">照会用番号</td>
		<td class="nouse"><input type="text" name="referencenumber" maxlength="128" value="#form.referencenumber#">&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="search" value="検   索" style="background-color:##FFCC00;height: 22px;width: 60px;border: none;"></td>
	</tr>
</table>
</form>

<cfif isdefined("form.search") and q_rsv_cst.recordcount neq 0>
<!--- 対象シート --->
<cfquery datasource="#dsn#" name="q_sheet">
	select top 1 questionnairesheet_id from #dbu#.mst_questionnairesheet where start_date <= '#q_rsv_cst.depart_date#' and questionnairesheet_id != 0 order by start_date desc
</cfquery>
<cfabort>
<cflock timeout=20 scope="Session" type="Exclusive">
	<cfif q_rsv_cst.repeater eq 1>
		<cfset session.sheet_id = 0>
	<cfelse>
		<cfset session.sheet_id = q_sheet.questionnairesheet_id>
	</cfif>
</cflock>
<form method="post" action="entry.cfm">
<table width ="600" border="1" cellspacing="5" cellpadding="5">
	<tr>
		<td class="nouse">照会用番号</td>
		<td class="str">#form.referencenumber#</td>
	</tr>
	<tr>
		<td class="nouse">調査手段</td>
		<td class="str">#q_rsv_cst.surveymethod#</td>
	</tr>
	<tr>
		<td class="nouse">顧客番号</td>
		<td class="str">#q_rsv_cst.customernumber#</td>
	</tr>
	<tr>
		<td class="nouse">漢字名</td>
		<td class="str">#q_rsv_cst.name#</td>
	</tr>
	<tr>
		<td class="nouse">カナ名</td>
		<td class="str">#q_rsv_cst.name_kana#</td>
	</tr>
	<tr>
		<td class="nouse">到着日</td>
		<td class="str">#q_rsv_cst.arrive_date#</td>
	</tr>
	<tr>
		<td class="nouse">出発日</td>
		<td class="str">#q_rsv_cst.depart_date#</td>
	</tr>
	<tr>
		<td class="nouse">予約番号</td>
		<td class="str">#q_rsv_cst.reserve_code#</td>
	</tr>
	<tr>
		<td class="nouse">部屋番号</td>
		<td class="str">#q_rsv_cst.room_code#</td>
	</tr>
	<tr>
		<td class="nouse">ルームタイプ</td>
		<td class="str">#q_rsv_cst.roomtype#</td>
	</tr>
	<tr>
		<td class="nouse">泊数</td>
		<td class="str">#q_rsv_cst.nights#</td>
	</tr>
	<tr>
		<td class="nouse">認知経路</td>
		<td class="str">#q_rsv_cst.reservationroute#</td>
	</tr>
	<tr>
		<td class="nouse">旅行目的</td>
		<td class="str">#mkt5#</td>
	</tr>
	<tr>
		<td class="nouse">客種区分</td>
		<td class="str">#mkt6#</td>
	</tr>
	<tr>
		<td class="nouse">人数</td>
		<td class="str">#q_rsv_cst.personcount#</td>
	</tr>
	<tr>
		<td class="nouse">結婚</td>
		<td class="str"><cfif q_rsv_cst.married_flg eq 1>既婚<cfelse>未婚</cfif></td>
	</tr>
	<tr>
		<td class="nouse">生年月日</td>
		<td class="str">#q_rsv_cst.birth_date#</td>
	</tr>
	<tr>
		<td colspan="2" align="right">
			<cfif q_answer.recordcount neq 0>
			#q_answer.registrated#に回答データが入力されています。<input type="submit" value="回答編集" style="background-color:##FFCC00;height: 30px;width: 200px;border: none;">
			<cfelse>
			<input type="submit" value="回答開始" style="background-color:##FFCC00;height: 30px;width: 200px;border: none;">
			</cfif>
		</td>
	</tr>
</table>
<input type="hidden" name="reserve_code" value="#q_rsv_cst.reserve_code#">
<input type="hidden" name="referencenumber" value="#form.referencenumber#">
</form>
</cfif>

</body>
</html>
</cfoutput>