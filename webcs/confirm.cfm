<cfparam name="form.reserve_code" default="">
<cfparam name="form.referencenumber" default="">
<!--- tmpDB生成 --->	
<cfinvoke component="csrespond" method="createduplicate" returnVariable="wk_duplicate"></cfinvoke>
<cfquery datasource="#dsn#" name="q_question">
	select	 b.questionnaire_id
			,b.questionnairesheet_id
			,b.viewtype
			,b.questionnaire_text
			,b.leadmessage
			,b.settype
			,b.parent
			,b.answertype
			,b.memo as message
			,b.scenario
			,a.questionnaire_nm
			,a.questionnaire_type
			,c.questionnairecategory_nm
			,b.labeltext
			,b.labelmemo as memo
			,b.viewsequence
	from	 #dbu#.mst_questionnaire as a
			,#dbu#.oth_sheetconfiguration as b
			,#dbu#.mst_questionnairecategory as c
	where	 a.questionnaire_id = b.questionnaire_id
	and		 a.questionnairecategory_id = c.questionnairecategory_id
	and		 b.questionnairesheet_id = #session.sheet_id#
	and		 b.owner = 'master'
	and		 b.active_flg = 1
	order by b.viewsequence
</cfquery>
<!--- 登録処理 --->
<cfif isdefined("form.confirm")>

	
	
	<cftransaction action="begin">
		<cfquery datasource="#dsn#" name="q_customer">
			select	 cst.area_code
					,rsv.room_code
					,rsv.arrive_date
					,rsv.depart_date
			from	 #dbu#.rtm_customer cst,
					 #dbu#.rtt_reserve rsv
			where	 rsv.referencenumber='#form.referencenumber#'
			and		 rsv.customernumber=cst.customernumber
		</cfquery>
		<cfquery datasource="#dsn#" name="del_answer">
			delete from #dbu#.hst_answers where referencenumber='#form.referencenumber#'
		</cfquery>
		<cfquery datasource="#dsn#" name="del_answerdetail">
			delete from #dbu#.hst_answersdetails where referencenumber='#form.referencenumber#'
		</cfquery>
		<cfquery datasource="#dsn#" name="ins_answer">
			insert into #dbu#.hst_answers(
				referencenumber,
				questionnairesheet_id,
				reserve_code,
				registrated,
				arrive_date,
				depart_date,
				area_code,
				statistics_value_code,
				roomnumber,
				answermethod
			)
			values(
				'#form.referencenumber#',
				#session.sheet_id#,
				#form.reserve_code#,
				'#dateformat(now(),"yyyy/mm/dd")#',
				'#q_customer.arrive_date#',
				'#q_customer.depart_date#',
				#q_customer.area_code#,
				null,
				#q_customer.room_code#,
				1
			)
		</cfquery>
			
		<!--- 複製分追加 2007/03/11 --->
		<cfquery datasource="#dsn#" name="wk_question">
			select	 b.questionnaire_id
					,b.questionnairesheet_id
					,b.viewtype
					,b.questionnaire_text
					,b.leadmessage
					,b.settype
					,b.parent
					,b.answertype
					,b.memo as message
					,b.scenario
					,a.questionnaire_nm
					,a.questionnaire_type
					,c.questionnairecategory_nm
					,b.labeltext
					,b.labelmemo as memo
					,b.viewsequence
			from	 #dbu#.mst_questionnaire as a
					,#dbu#.oth_sheetconfiguration as b
					,#dbu#.mst_questionnairecategory as c
			where	 a.questionnaire_id = b.questionnaire_id
			and		 a.questionnairecategory_id = c.questionnairecategory_id
			and		 b.questionnairesheet_id = #session.sheet_id#
			and		 b.owner = 'master'
			and		 b.active_flg = 0
			and		 b.scenario in (
				select	 b.scenario
				from	 #dbu#.mst_questionnaire as a
						,#dbu#.oth_sheetconfiguration as b
						,#dbu#.mst_questionnairecategory as c
				where	 a.questionnaire_id = b.questionnaire_id
				and		 a.questionnairecategory_id = c.questionnairecategory_id
				and		 b.questionnairesheet_id = #session.sheet_id#
				and		 b.owner = 'master'
				and		 b.active_flg = 1
			)
			order by b.viewsequence
		</cfquery>	
		<cfquery name="qoq_question" dbtype="query">
			select * from q_question union all select * from wk_question order by viewsequence
		</cfquery>
		<cfset wk_array = arraynew(1)>
		<cfset q_question = qoq_question>
<!--- 
<cfdump var=#q_question#>
<cfdump var=#form#>
<cfabort>
 --->
		<cfloop query="q_question">
			<cfset q_question_currentrow=q_question.currentrow>
			<cfif form["scenario" & q_question.scenario[q_question_currentrow]] eq 0>
				<cfparam name="form.opt#q_question.questionnaire_id#" default="0">
				<cfparam name="form.opt#q_question.questionnaire_id#_text" default="">
				<cfif form["opt" & q_question.questionnaire_id] eq 0 and form["opt" & q_question.questionnaire_id & "_text"] eq "">
					<cfset form["opt" & q_question.questionnaire_id & "_text"]="未回答">
				</cfif>
				<cfif len(q_question.parent) eq 0>
					<cfset wk_q_id = q_question.questionnaire_id>
				<cfelse>
					<cfset wk_q_id = q_question.parent>
				</cfif>
				<cfif form["opt" & q_question.questionnaire_id] neq 0>
					<cfquery name="q_option" datasource = "#dsn#">
						select	 options_no,
								 options_label
						from	 #dbu#.mst_answeroptions
						where	 questionnaire_id = #wk_q_id#
						and		 options_no in (#form["opt" & q_question.questionnaire_id]#)
						and		 active_flg = 1
						and		 resort_code = #resort_code#
					</cfquery>


					<cfloop query="q_option">
						<cfquery datasource="#dsn#" name="ins_answerdetail">
							insert into #dbu#.hst_answersdetails(
								referencenumber,
								questionnairesheet_id,
								questionnaire_id,
								answer,
								answertext,
								othertext,
								reserve_code
							)
							values(
								'#form.referencenumber#',
								#session.sheet_id#,
								#q_question.questionnaire_id[q_question_currentrow]#,
								#q_option.options_no#,
								'#q_option.options_label#',
								'',
								#form.reserve_code#
							)
						</cfquery>
						<cfif q_option.options_no eq "99" and form["opt" & q_question.questionnaire_id & "_text"] neq "">
							<cfquery datasource="#dsn#" name="ins_answerdetail">
								insert into #dbu#.hst_answersdetails(
									questionnairesheet_id,
									questionnaire_id,
									answer,
									answertext,
									othertext,
									referencenumber,
									reserve_code
								)
								values(
									#session.sheet_id#,
									#q_question.questionnaire_id[q_question_currentrow]#,
									0,
									'#form["opt" & q_question.questionnaire_id[q_question_currentrow] & "_text"]#',
									'',
									'#form.referencenumber#',
									#form.reserve_code#
								)	
							</cfquery>
						</cfif>
					</cfloop>
				<cfelseif (q_question.answertype[q_question_currentrow] neq 1 and q_question.answertype[q_question_currentrow] neq 3) or form["opt" & q_question.questionnaire_id & "_text"] neq "未回答">
					<cfquery datasource="#dsn#" name="ins_answerdetail">
						insert into #dbu#.hst_answersdetails(
							referencenumber,
							questionnairesheet_id,
							questionnaire_id,
							answer,
							answertext,
							othertext,
							reserve_code
						)
						values(
							'#form.referencenumber#',
							#session.sheet_id#,
							#q_question.questionnaire_id#,
							0,
							'#form["opt" & q_question.questionnaire_id[q_question_currentrow] & "_text"]#',
							'',
							#form.reserve_code#
						)	
					</cfquery>
				</cfif>
			</cfif>
		</cfloop>
	</cftransaction>
	<cflocation url="regist.cfm">
</cfif>
<cfquery dbtype="query" name="q_scenario">
	select distinct viewtype, scenario from q_question order by	scenario
</cfquery>

<cfset scenario_ary=arraynew(2)>

<cfloop query="q_question">
	<cfset arrayappend(scenario_ary[q_question.scenario],q_question.questionnaire_id)>
</cfloop>

	
<cfoutput>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html" />
	<link rel="stylesheet" href="configcss.css">
<title>#title#</title>
<script language="javascript" type="text/javascript">


</script>

</head>
<body>
<h3>以下の内容で登録します。よろしいですか？</h3>
<form method="post" action="confirm.cfm">
<table border="0" cellpadding="0" cellspacing="1" width="1024">
</cfoutput>
<cfloop query="q_question">
	<cfset q_question_currentrow=q_question.currentrow>
	<cfif len(q_question.parent) eq 0>
		<cfset wk_q_id = q_question.questionnaire_id>
	<cfelse>
		<cfset wk_q_id = q_question.parent>
	</cfif>	
	<cfquery name="q_option" datasource = "#dsn#">
		select	 A.questionnaire_id
				,A.options_no
				,A.options_label
				,A.options_type
				,A.addtext
				,A.scenario
				,A.viewsequence
				,B.answertype
		from	 #dbu#.mst_answeroptions as A
				,#dbu#.oth_sheetconfiguration as B
		where	 A.questionnaire_id = #wk_q_id#
		and		 A.questionnaire_id = B.questionnaire_id
		and		 B.questionnairesheet_id = #session.sheet_id#
		and		 A.active_flg = 1
		and		 A.resort_code = #resort_code#
		and		 B.Owner = 'Master'
		order by A.viewsequence
	</cfquery>
	<cfset groupscenario_ary=listtoarray(valuelist(q_option.scenario))>
	
	<cfparam name="form.opt#q_question.questionnaire_id#" default="0">
	<cfparam name="form.opt#q_question.questionnaire_id#_text" default="">
	<!---
	<cfif form["opt" & q_question.questionnaire_id] eq 0 and form["opt" & q_question.questionnaire_id & "_text"] eq "">
		<cfset form["opt" & q_question.questionnaire_id & "_text"]="未回答">
	</cfif>
	--->
	<cfset q_answersdetail=querynew("questionnaire_id,answer,answertext","integer,integer,varchar")>
	<cfset queryaddrow(q_answersdetail,listlen(form["opt" & q_question.questionnaire_id]))>
	<cfset i=1>
	<cfloop list="#form["opt" & q_question.questionnaire_id]#" index="idx">
		<cfset querysetcell(q_answersdetail,"questionnaire_id",q_question.questionnaire_id,i)>
		<cfset querysetcell(q_answersdetail,"answer",idx,i)>
		<cfif (idx eq "0" and (q_question.answertype eq 1 or q_question.answertype eq 3)) or idx eq "99">
			<cfif form["opt" & q_question.questionnaire_id & "_text"] eq "">
				<cfset querysetcell(q_answersdetail,"answertext","未回答",i)>
			<cfelse>
				<cfset querysetcell(q_answersdetail,"answertext",form["opt" & q_question.questionnaire_id & "_text"],i)>
			</cfif>
		</cfif>
		<cfset i=i+1>
	</cfloop>
	<cfset othertext="">
	<cfquery dbtype="query" name="q_othertext">
		select answertext from q_answersdetail where answer = 99
	</cfquery>
	<cfif q_othertext.recordcount neq 0>
		<cfset othertext=q_othertext.answertext>
	</cfif>
<cfoutput>
<cfif q_question.labeltext neq "">
<cfif q_question.currentrow neq 1>
	<tr>
		<td>&nbsp;</td>
	</tr>
</cfif>
	<tr>
		<th align="left" class="question">#q_question.labeltext#</th>
	</tr>
</cfif>
	<tr>
		<td><div style="margin-left:5px padding:5;background:##EEEEE9">#q_question.questionnaire_text#</div></td>
	</tr>
	<tr>
		<td>
			<div style="margin-left:20px">
			<cfif Len(q_question.parent) NEQ 0>
				<cfset QueryAddRow(q_option,1)>
				<cfset QuerySetCell(q_option,"options_no", 0)>
				<cfset QuerySetCell(q_option,"viewsequence", 0)>
				<cfset QuerySetCell(q_option,"options_label", "未回答")>
				<cfquery dbtype="query" name="qoq_options_label">
					select	options_label,	viewsequence from q_option where options_no = #q_answersdetail.answer#
				</cfquery>
				#qoq_options_label.viewsequence#&nbsp;&nbsp;#qoq_options_label.options_label#
			<cfelse>
				<cfloop query="q_option">
					#RepeatString("	",4)#
					<cfswitch expression="#q_option.options_type#">
						<cfcase value="0"><!--- ラジオボタンのその他 --->
							<cfif q_answersdetail.answer eq q_option.options_no>●<cfelse>○</cfif>#viewsequence#&nbsp;&nbsp;#q_option.options_label# [ #othertext# ]
						</cfcase>
						<cfcase value="1"><!--- ラジオボタン --->
							<cfif q_answersdetail.answer eq q_option.options_no>●<cfelse>○</cfif>#viewsequence#&nbsp;&nbsp;#q_option.options_label#
						</cfcase>
						<cfcase value="2"><!--- チェックボックスのその他 --->
							<cfif listfind(valuelist(q_answersdetail.answer),q_option.options_no) neq 0>■<cfelse>□</cfif>#viewsequence#&nbsp;&nbsp;#q_option.options_label# [ #othertext# ]
							<br>
						</cfcase>
						<cfcase value="3"><!--- チェックボックス --->
							<cfif listfind(valuelist(q_answersdetail.answer),q_option.options_no) neq 0>■<cfelse>□</cfif>#viewsequence#&nbsp;&nbsp;#q_option.options_label#
							<br>
						</cfcase>
						<cfcase value="4"><!--- テキスト --->
							#htmleditformat(q_answersdetail.answertext)#
						</cfcase>
						<cfcase value="5"><!--- テキストエリア --->
							#replace(htmleditformat(q_answersdetail.answertext),chr(13) & chr(10),"<br>","all")#
						</cfcase>
					</cfswitch>
					<!--- 
					<cfif q_option.currentrow mod 10 eq 0 and q_option.currentrow neq q_option.recordcount><br><cfelseif q_option.currentrow neq q_option.recordcount>&nbsp;&nbsp;</cfif></cfloop>
					 --->
					</cfloop>
				</cfif>
			</div>
		</td>
	</tr>
</cfoutput>
</cfloop>
<cfoutput>
	<tr>
		<td align="center">
			<input type="button" name="back" value=" 修 正 " onClick="history.back();" style="background-color:##FFCC00;height: 30px;width: 200px;border: none;">&nbsp;&nbsp;
			<input type="submit" name="confirm" value=" 登 録 " style="background-color:##FFCC00;height: 30px;width: 200px;border: none;">
		</td>
	</tr>
</table>
<cfset dellist = "">	
<cfloop list="#fieldnames#" index="idx">
	<cfif left(idx,3) eq "opt" or left(idx,8) eq "scenario">
		<!--- 2006/11/22　複数回答時は回答複製する --->
		<cfset wk_qid = mid(idx,4,len(idx))>
		<cfif isnumeric(wk_qid)>
			<cfset wk_ans = htmleditformat(form["opt"&wk_qid])>
			<cfquery name="qoqduplicate" dbtype="query">
				select dup_id,bse_id,key_qid from wk_duplicate where key_qid = #wk_qid# and key_ans in(#wk_ans#)
			</cfquery>
			<cfloop query="qoqduplicate">
				<cfloop list="#dup_id#" index="cnt" delimiters="/">
					<cfset wk_ans = listgetat(bse_id,listfind(dup_id,cnt,"/"),"/")>
					<cfset wk_str =structnew()>
					<cfset wk_str.opt = "opt"&cnt>
					<cfset wk_str.wk_ans = "opt"&wk_ans>
					<cfset wk_str.ans = #htmleditformat(form[wk_str.wk_ans])#>
					<input type="hidden" name="#wk_str.opt#" value="#wk_str.ans#">
					<!--- #wk_str.opt#-#wk_str.ans#<br> --->
				</cfloop>
			</cfloop>
			
			<!--- 削除対象リスト生成 --->
			<cfif qoqduplicate.recordcount neq 0 and len(wk_ans) neq 0>
				<cfloop list="#qoqduplicate.bse_id#" index="cnt" delimiters="/">
					<cfset dellist = listappend(dellist,cnt)>
				</cfloop>
			</cfif>
			<cfif not listfind(dellist,wk_qid)>
				<input type="hidden" name="#idx#" value="#htmleditformat(form[idx])#">
			</cfif>
		<cfelse>
			<input type="hidden" name="#idx#" value="#htmleditformat(form[idx])#">
		</cfif>
		<!--- 2006/11/22　複数回答時は回答複製する --->
	</cfif>
</cfloop>				
<input type="hidden" name="reserve_code" value="#form.RESERVE_CODE#">
<input type="hidden" name="referencenumber" value="#form.referencenumber#">
</form>
</body>
</html>
</cfoutput>