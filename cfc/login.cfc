<cfcomponent name="login" hint="ＩＰによる認証チェック">
	<!---
	*************************************************************************************************
	* Class				:login
	* Method			:getUser
	* Description		: 
	* Custom Attributes	:datasource
	* Return Paramters	:
	* History			:1) Coded by	2004/09/13	h.miyahara(NetFusion)
	*					:2) Updated by	2005/11/29	
	*************************************************************************************************
	--->
	<cffunction name="getloginUser" access="remote" returntype="query">
		<cfargument name="login_id" type="String" default="">
		<cfargument name="login_pw" type="String" default="">

		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<cfquery name="getloginUser" datasource = "#Application.datasource#">
				select	 login_id
						,login_pw
						,'' as login_dv
						,login_nm
						,'' as resort_code 
				from	m_loginuser
				where	login_id = '#arguments.login_id#'
				and		login_pw = '#arguments.login_pw#'
				group by login_id,login_pw,login_nm
			</cfquery>
			<cfif getloginUser.recordcount eq 1>
				<cfquery name="getResortcode" datasource = "#Application.datasource#">
					select	resort_code , login_dv
					from	m_loginuser
					where	login_id = '#arguments.login_id#'
					and		login_pw = '#arguments.login_pw#'
				</cfquery>
				<cfset QuerySetCell(getloginUser,"resort_code",ValueList(getResortcode.resort_code))>

				<cfquery name="getDiv" dbtype="query">
					select login_dv from getResortcode group by login_dv
				</cfquery>
				<cfif getDiv.recordcount eq 1>
					<cfset QuerySetCell(getloginUser,"login_dv",getDiv.login_dv)>
					<cflock name="session.user_info" timeout="60">
						<cfset session.user_info.resort_code = ValueList(getResortcode.resort_code)>
					</cflock>
					<cfset QuerySetCell(getloginUser,"login_dv",'#getDiv.login_dv#')>
				<cfelse>
					<!--- 2009/12/26 同一アカウント対応 --->
					<cfquery name="getDiv" dbtype="query">
						select resort_code from getResortcode where login_dv != 'z'
					</cfquery>
					<!--- アカウント情報格納 --->
					<cflock name="session.user_info" timeout="60">
						<cfset session.user_info.resort_code = '#valuelist(getDiv.resort_code)#'>
					</cflock>
					<cfset QuerySetCell(getloginUser,"login_dv",'s')>
				</cfif>


				<cfset QuerySetCell(getloginUser,"resort_code",ValueList(getResortcode.resort_code))>

				<!--- 2011/09/15 子施設（インバウンド用擬似施設）概念導入対応⇒子施設も利用可能施設に含める --->
				<cfif getResortcode.resort_code neq 0>
					<cfquery name="getAddResortcode" datasource = "#Application.datasource#">
						select	resort_code
						from	mst_resort
						where	parent_code in (#ValueList(getResortcode.resort_code)#)
					</cfquery>
					<cfset QuerySetCell(getloginUser,"resort_code",ValueList(getAddResortcode.resort_code))>
				</cfif>

				<!--- トラッキング --->
				<cfquery name="gettrack" datasource = "#Application.datasource#">
					select	isnull(max(login_cnt),0) cnt from h_logintrack
					where	login_id = '#getloginUser.login_id#'
					and		login_dt = convert(varchar,getdate(),111)
				</cfquery>
				<cfif gettrack.cnt eq 0>
					<cfquery name="settrack" datasource = "#Application.datasource#">
						insert into h_logintrack (login_id, login_dt) values (
						 '#getloginUser.login_id#'
						,convert(varchar,getdate(),111)
						)
					</cfquery>
				<cfelse>
					<cfquery name="settrack" datasource = "#Application.datasource#">
						update h_logintrack set 
						login_cnt = #gettrack.cnt#+1
						where	login_id = '#getloginUser.login_id#'
						and		login_dt = convert(varchar,getdate(),111)
					</cfquery>
				</cfif>
				<!--- アカウント情報格納 --->
				<cflock name="session.user_info" timeout="60">
					<cfset session.user_info.login_id = '#getloginUser.login_id#'>
					<cfif len(cgi.remote_addr) eq 0>
						<cfset session.user_info.user_ip = '#getloginUser.login_id#'>
					</cfif>
				</cflock>

			</cfif>
			<cfreturn getloginUser>
			<cfsetting enablecfoutputonly="No">
			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>	
	</cffunction>	
	<!---
	*************************************************************************************************
	* Class				:login
	* Method			:getUser
	* Description		: 
	* Custom Attributes	:datasource
	* Return Paramters	:
	* History			:1) Coded by	2004/09/13	h.miyahara(NetFusion)
	*					:2) Updated by	2005/11/29	
	*************************************************************************************************
	--->
	<cffunction name="getUser" access="remote" returntype="query">
		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<!--- 
			<cfquery name="getUser" datasource = "#Application.datasource#">
				select	resort_code
						,role_flg
						,user_ip
						,user_nm
				from	dbo.mst_user
				where	user_ip = '#cgi.remote_addr#'
				and		active_flg = 1
			</cfquery>
			 --->
			<!--- IP制御外す --->
			<cfset getUser=querynew("resort_code,role_flg,user_ip,user_nm")>
			<cfset queryaddrow(getUser,1)>
			<cfset QuerySetCell(getUser,"resort_code",10)>
			<cfset QuerySetCell(getUser,"role_flg",1)>
			<cfset QuerySetCell(getUser,"user_ip",'#cgi.remote_addr#')>
			<cfset QuerySetCell(getUser,"user_nm",'#cgi.remote_addr#')>
		
			<!--- 現在のアンケートマスタをセット --->
			<cfquery name="getmasterEnquete" datasource = "#Application.datasource#">
				select	questionnairesheet_id from dbo.mst_questionnairesheet	where start_date <= getdate() and end_date is null
			</cfquery>

			<!--- ユーザー情報格納 --->
			<cflock name="session.user_info" timeout="60">
				<cfset session.user_info = StructNew()>
				<cfset session.user_info.resort_id = #getUser.resort_code#>
				<cfif len(cgi.remote_addr) eq 0>
					<cfset session.user_info.user_ip = '127.0.0.1'>
				<cfelse>
					<cfset session.user_info.user_ip = #getUser.user_ip#>
				</cfif>
				<cfset session.user_info.user_nm = #getUser.user_nm#>
				<cfset session.user_info.masterEnquete = #getmasterEnquete.questionnairesheet_id#>
				<cfreturn getUser>
			</cflock>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>	
	</cffunction>
	<!---
	*************************************************************************************************
	* Class				:login
	* Method			:passwordConvert
	* Description		: 
	* Custom Attributes	:datasource
	* Return Paramters	:
	* History			:1) Coded by	2004/09/13	h.miyahara(NetFusion)
	*					:2) Updated by	2005/11/29	
	*************************************************************************************************
	--->
	<cffunction name="passwordConvert" access="remote" returntype="any">
		<cfargument name="login_id" type="String" default="">
		<cfargument name="login_ow" type="String" default="">
		<cfargument name="login_nw" type="String" default="">
		<cftry>

		<cftransaction action="BEGIN">
		<cfquery name="passwordConvert" datasource = "#Application.datasource#">
			update 	m_loginuser
			set 	login_pw = '#arguments.login_nw#'
			where	login_id = '#arguments.login_id#' 
			and		login_pw = '#arguments.login_ow#'
		</cfquery>
		</cftransaction>

		<!--- 戻値設定 --->		
		<cfscript>
			executeinformation = StructNew();
			StructInsert(executeinformation, "executeDate",	dateformat(now(),'YYYY/MM/DD'));
			StructInsert(executeinformation, "executeTime",	timeformat(now(),'hh,mm,ss'));
			StructInsert(executeinformation, "errorMsg",	"パスワードが変更されました。");
		</cfscript>
		<cfreturn executeinformation>
		<cfcatch type="any">
		<cftransaction action="rollback"/>
 		<cfrethrow>

	</cfcatch>
	</cftry>

	</cffunction>	

	<!---
	*************************************************************************************************
	* Class				:login
	* Method			:passwordConvert
	* Description		: 
	* Custom Attributes	:datasource
	* Return Paramters	:
	* History			:1) Coded by	2004/09/13	h.miyahara(NetFusion)
	*					:2) Updated by	2005/11/29	
	*************************************************************************************************
	--->
	<cffunction name="passwordCheck" access="remote" returntype="any">
		<cfargument name="mailaddress" type="String" default="">
		<cfargument name="ask" type="String" default="">
		<cftry>
			<!--- 管理者メールアドレス --->
			<cfset arg_to_mail = Application.crmmaster>
			<cfset mail_body= "メールアドレス：#arguments.mailaddress#のユーザーから、下記の問い合わせです。">
			<cfset mail_body= mail_body & arguments.ask>
			<cfmail to="#arg_to_mail#" from="CRMKitchen"
					subject="CRMKitchenアカウント問い合わせ" type="text/plain"
					server="#Application.SMTPServer#" port="#Application.port#" charset="iso-2022-jp"
					username="#Application.username#" password ="#Application.password#"
			>#mail_body#</cfmail>
			<!--- 戻値設定 --->		
			<cfscript>
				executeinformation = StructNew();
				StructInsert(executeinformation, "executeDate",	dateformat(now(),'YYYY/MM/DD'));
				StructInsert(executeinformation, "executeTime",	timeformat(now(),'hh,mm,ss'));
				StructInsert(executeinformation, "errorMsg",	"問い合わせ送信しました。");
			</cfscript>
			<cfreturn executeinformation>
		<cfcatch type="any">
 			<cfrethrow>
		</cfcatch>
	</cftry>

	</cffunction>

</cfcomponent>