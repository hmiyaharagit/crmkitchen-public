﻿<cfcomponent>
		<!---受け渡し方法の項目1--->
	<cffunction name="getExportData1"  output="false" returntype="string">
		<cfargument name="folder1" type="string" required="yes">
		<cfargument name="folder2" type="string" required="yes">
		<cfargument name="formatid" type="string" required="yes">
		<cfargument name="userid" type="string" required="yes">
		<cfargument name="userpw" type="string" required="yes">
		<cfset ansquery = StructNew()>
		
		<!---cfhttpで必要データ取得--->
		<cfset urlStr = "#Application.getExportDataURL#folder1=#arguments.folder1#&folder2=#arguments.folder2#&cutOffMilliSec=259200000&id=#arguments.formatid#">
		<cfhttp url="#urlStr#" method="get" username="#arguments.userid#" password="#arguments.userpw#" result="rValue">
		</cfhttp>
		<cfif rValue.Responseheader.Status_Code eq 200>
			<!---取得したデータをxmlへ変換し戻り値で戻す--->
			<cfset ID_Query = QueryNew("ID,")>
			<cfset myXMLDocument=XmlParse(rValue.Filecontent)>
			<cfreturn myXMLDocument>
		<cfelse>
			<cfset erMsg="FAILED">
			<cfreturn erMsg>
		</cfif>
	</cffunction>
	
	<!---受け渡し方法の項目2--->
	<cffunction name="getExportData2"  output="false" returntype="any">
		<cfargument name="data2Xml" type="string" required="yes">
		<cfargument name="userid" type="string" required="yes">
		<cfargument name="userpw" type="string" required="yes">
		<cfset ansquery = StructNew()>
		
		<cfset myXMLDocument=XmlParse(data2Xml)>
		<cfset MyNewArray1 = ArrayNew(1)>
		<cfset cnt1 = 1>
		
		<cfset test = XmlSearch(myXMLDocument,"/DataExport")>
		
		<cfif ArrayLen(myXMLDocument.DataExport.v1.XmlChildren) eq 1>
			<cfset erMsg="NODATA">
			<cfreturn erMsg>
		</cfif>
		
		<cfset ID_Query = QueryNew("ID")>
		<!---お客様ID?読み込み用のループ処理--->
		<cfloop index="i" from="1" to="#ArrayLen(myXMLDocument.DataExport.v1.exportInput)#">
		
			<cfset MyNewArray1[#cnt1#] = "#myXMLDocument.DataExport.v1.exportInput[i].id.XmlText#">

			<!---お客様IDを大元のクエリへ挿入--->
			<cfset newRow = QueryAddRow(ID_Query, 1)>
			<cfset ap_id = "#myXMLDocument.DataExport.v1.exportInput[i].id.XmlText#">
			
	
			<cfset temp = QuerySetCell(ID_Query, "ID", ap_id, #i#)>
			<cfset cnt1 = cnt1 + 1>
		</cfloop>

		
		<!---1で取得したデータをPUTで配置する--->
		<cfhttp url="#Application.getExportDataURL2#" method="put" username="#arguments.userid#" password="#arguments.userpw#" result="rValue">
			<cfhttpparam type="xml" value="#data2Xml#">
		</cfhttp>
		
		<cfif rValue.Responseheader.Status_Code eq 200>
			<cfreturn rValue.filecontent>
		<cfelse>
			<cfset erMsg="FAILED">
			<cfreturn erMsg>
		</cfif>
	</cffunction>
	
	<!---受け渡し方法の項目3--->
	<cffunction name="getExportData3"  output="false" returntype="any">
		<cfargument name="batchid" type="string" required="yes">
		<cfargument name="userid" type="string" required="yes">
		<cfargument name="userpw" type="string" required="yes">
		<cfset ansquery = StructNew()>
		

	<!---設問内容が生成できたか確認--->
		<cfhttp url="#Application.getExportDataURL3##batchid#" method="get" username="#arguments.userid#" password="#arguments.userpw#" result="rValue">
		</cfhttp>
		<cfif rValue.Responseheader.Status_Code eq 200>
			<cfreturn rValue.filecontent>
		<cfelse>
			<cfset erMsg="FAILED">
			<cfreturn erMsg>
		</cfif>
	</cffunction>
	<!---受け渡し方法の項目4--->
	<cffunction name="getExportData4"  output="false" returntype="any">
		<cfargument name="batchid" type="string" required="yes">
		<cfargument name="formatid" type="string" required="yes">
		<cfargument name="userid" type="string" required="yes">
		<cfargument name="userpw" type="string" required="yes">
		<cfargument name="tasknm" type="string" required="yes">
		<cfset ansquery = StructNew()>
		

		<!---解答値の入ったXML取得--->
		<cfhttp url="#Application.getExportDataURL4##batchid#" method="get" username="#arguments.userid#" password="#arguments.userpw#" result="rValue">
		</cfhttp>
		<cfif rValue.Responseheader.Status_Code neq 200>
			<cfset erMsg="FAILED">
			<cfreturn erMsg>
		</cfif>
		
		<!---取得したXMLを--->
		<cfset myXMLDocument=XmlParse(rValue.filecontent)>
		
		<cfset date1 = now()>
		<!------>
		<cfinvoke method="unZipFile">
			<cfinvokeargument name="BinaryObj" value="#myXMLDocument.DataExport.v1.binariesZip.XmlText#">
			<cfinvokeargument name="jpgnm" value="#arguments.tasknm#">
			<cfinvokeargument name="date1" value="#date1#">
		</cfinvoke>
		
		<cfset MyNewArray1 = ArrayNew(1)>
		<cfset cnt1 = 1>
		
		<cfset ID_Query = QueryNew("ID, Answer")>
		<!---お客様ID?読み込み用のループ処理--->
		<cfloop index="i" from="1" to="#ArrayLen(myXMLDocument.DataExport.v1.exportInput)#">
		
			<cfset MyNewArray1[#cnt1#] = "#myXMLDocument.DataExport.v1.exportInput[i].id.XmlText#">

			<!---お客様IDを大元のクエリへ挿入--->
			<cfset newRow = QueryAddRow(ID_Query, 1)>
			<cfset ap_id = "#myXMLDocument.DataExport.v1.exportInput[i].id.XmlText#">
			<cfset session.errorid = "#myXMLDocument.DataExport.v1.exportInput[i].id.XmlText#">
			<cfset temp = QuerySetCell(ID_Query, "ID", "#myXMLDocument.DataExport.v1.exportInput[i].id.XmlText#", #i#)>
			<!---子クエリへ回答状況を挿入--->
			<cfset Answer = QueryNew("Q_ID,Other,b_code, Q_str, answer")>
			<cfset count = 1>
			
			<!---設問分ループしQUERY[Answer]へ挿入--->
			<cfloop index="j" from="1" to="#ArrayLen(myXMLDocument.DataExport.v1.exportElement)#">
				<cfset arLen = ArrayLen(myXMLDocument.DataExport.v1.exportElement[j].data[i].XmlChildren)>
				
				<cfif arLen eq 0>
					<cfset newRow = QueryAddRow(Answer, 1)>
					<!---設問Noの挿入--->
					<cfset temp = QuerySetCell(Answer, "Q_ID", "#myXMLDocument.DataExport.v1.exportElement[j].questionnaireItem[1].id.xmltext#", #count#)>
					<!---設問--->
					<cfset temp = QuerySetCell(Answer, "Q_str", "#myXMLDocument.DataExport.v1.exportElement[j].questionnaireItem[1].label.xmltext#", #count#)>
					<cfset temp = QuerySetCell(Answer, "b_code", "1", #count#)>
					<cfset temp = QuerySetCell(Answer, "answer", "未回答", #count#)>
					<cfset count = count + 1>
				<cfelse>
				<cfloop index="k" from="1" to="#arLen#">
					<cfif myXMLDocument.DataExport.v1.exportElement[j].data[i].XmlChildren[k].xmlname neq "state">
					
						<cfset newRow = QueryAddRow(Answer, 1)>
						<!---設問Noの挿入--->
						<cfset temp = QuerySetCell(Answer, "Q_ID", "#myXMLDocument.DataExport.v1.exportElement[j].questionnaireItem[1].id.xmltext#", #count#)>
						<!---設問--->
						<cfset temp = QuerySetCell(Answer, "Q_str", "#myXMLDocument.DataExport.v1.exportElement[j].questionnaireItem[1].label.xmltext#", #count#)>
						<cfset temp = QuerySetCell(Answer, "b_code", "#k#", #count#)>
						
						<cfif myXMLDocument.DataExport.v1.exportElement[j].data[i].XmlChildren[k].xmlname neq "imageNameInZip">
							<cfif ArrayLen(myXMLDocument.DataExport.v1.exportElement[j].data[i].XmlChildren) eq 1 and len(myXMLDocument.DataExport.v1.exportElement[j].data[i].XmlChildren[k].xmltext) eq 0>
								<cfset temp = QuerySetCell(Answer, "answer", "未回答", #count#)>
							<cfelse>
								<cfset temp = QuerySetCell(Answer, "answer", "#myXMLDocument.DataExport.v1.exportElement[j].data[i].XmlChildren[k].xmltext#", #count#)>
							</cfif>
						<cfelse>
								<cfset temp = QuerySetCell(Answer, "answer", "#myXMLDocument.DataExport.v1.exportElement[j].data[i].XmlChildren[k].xmltext#", #count#)>
								<cfset temp = QuerySetCell(Answer, "Other", "#myXMLDocument.DataExport.v1.exportElement[j].data[i].XmlChildren[k].xmltext#", #count#)>
						</cfif>
						<cfset count = count + 1>
					<cfelse>
						
						<cfset newRow = QueryAddRow(Answer, 1)>
						<!---設問Noの挿入--->
						<cfset temp = QuerySetCell(Answer, "Q_ID", "#myXMLDocument.DataExport.v1.exportElement[j].questionnaireItem[1].id.xmltext#", #count#)>
						<!---設問--->
						<cfset temp = QuerySetCell(Answer, "Q_str", "#myXMLDocument.DataExport.v1.exportElement[j].questionnaireItem[1].label.xmltext#", #count#)>
						<cfset temp = QuerySetCell(Answer, "b_code", "1", #count#)>
						<cfset temp = QuerySetCell(Answer, "answer", "未回答", #count#)>
						<cfset count = count + 1>
						<cfbreak>
					</cfif>
					
				</cfloop>
				</cfif>
			</cfloop>
		<!---取得したXMLデータを登録--->
		<cfinvoke method="insertGetData" returnVariable="rValue4">
			<cfinvokeargument name="formatid" value="#arguments.formatid#">
			<cfinvokeargument name="Answer" value="#Answer#">
			<cfinvokeargument name="jpgnm" value="#arguments.tasknm#">
			<cfinvokeargument name="date1" value="#date1#">
			<cfinvokeargument name="ap_id" value="#ap_id#">
		</cfinvoke>
		
		<cfset temp = QuerySetCell(ID_Query, "Answer", Answer, #i#)>
		<cfset cnt1 = cnt1 + 1>
		</cfloop>
		<!------>
		<!---青森用--->
		<cfset dirmake1 = ArrayNew(1)>
		<!--- 配列が ArraySet によって初期化されていないと、ArrayToList は正常に動作しません。 --->
		
		<cfset date1 = now()>
		<!--- 要素を設定 --->
		<cfset dirmake1[1] = "#Application.dir1#">
		<cfset dirmake1[2] = "#Application.dir2_1#">
		
		<cfset d1 = "0">
		
		<cfif month(date1) lt 10>
			<cfset d1 = "0#month(date1)#">
		<cfelse>
			<cfset d1 = "#month(date1)#">
		</cfif>
		
		<cfset d2 = "0">
		
		<cfif day(date1) lt 10>
			<cfset d2 = "0#day(date1)#">
		<cfelse>
			<cfset d2 = "#day(date1)#">
		</cfif>

		
		<cfset dirmake1[3] = "#year(date1)##d1##d2#">
		
		<cfset rv1 = false>
		<cfset rv2 = false>
		<cfset rv3 = false>
		<cfset rv4 = false>
		
		<cfinvoke webservice="#Application.webname#" method="ensureMakeDir" username="#Application.getExportDatauser#" password="#Application.getExportDataPass#" returnvariable="rv3">
			<cfinvokeargument name="pathElementsFromRoot" value="#dirmake1#"/>
		</cfinvoke>
		<!---沖縄用--->
		<cfset dirmake2 = ArrayNew(1)>
		<!--- 配列が ArraySet によって初期化されていないと、ArrayToList は正常に動作しません。 --->
		
		<!--- 要素を設定 --->
		<cfset dirmake2[1] = "#Application.dir1#">
		<cfset dirmake2[2] = "#Application.dir2_2#">
		<cfset dirmake2[3] = "#year(date1)##d1##d2#">
		
		<cfinvoke webservice="#Application.webname#" method="ensureMakeDir"  username="#Application.getExportDatauser#" password="#Application.getExportDataPass#" returnvariable="rv4">
			<cfinvokeargument name="pathElementsFromRoot" value="#dirmake2#"/>
		</cfinvoke>
		<!------>
		<cfif rv3 neq true or not IsDefined("rv3")>
			<cfset session.mailmsg = session.mailmsg & "登録済みフォルダ内へフォルダを作成できませんでした。その為、移動が出来ません。">
			<cfset erMsg="SUCCESS">
			<cfreturn erMsg>
		</cfif>
		<cfif rv4 neq true or not IsDefined("rv4")>
			<cfset session.mailmsg = session.mailmsg & "登録済みフォルダ内へフォルダを作成できませんでした。その為、移動が出来ません。">
			<cfset erMsg="SUCCESS">
			<cfreturn erMsg>
		</cfif>
		
		<cfset MyNewArray2_2 = ArrayNew(1)>
		<cfset MyNewArray2_2[1] = "#Application.folder1#">
		<cfset jtest2 = structNew()>
		<cfset jtest2.item = MyNewArray2_2>
		
		<cfset MyNewArray2 = ArrayNew(1)>
		<cfset MyNewArray2[1] = jtest2>
		
		<cfset bl1 = true>
		<!---青森用--->
		<cfinvoke webservice="#Application.webname#" method="moveRecOverwrite" username="#Application.getExportDatauser#" password="#Application.getExportDataPass#" returnvariable="rv1">
			<cfinvokeargument name="destFolder" value="#dirmake1#"/>
			<cfinvokeargument name="moveFromRootFolders" value="#MyNewArray2#"/>
			<cfinvokeargument name="moveContentsIDs" value="#MyNewArray1#"/>
			<cfinvokeargument name="purgeNullChildDirOnNot" value="#bl1#" />
		</cfinvoke>
		<!---沖縄用--->
		<cfset MyNewArray3_2 = ArrayNew(1)>
		<cfset MyNewArray3_2[1] = "#Application.folder2#">
		<cfset jtest3 = structNew()>
		<cfset jtest3.item = MyNewArray3_2>
		
		<cfset MyNewArray3 = ArrayNew(1)>
		<cfset MyNewArray3[1] = jtest3>
				
		<cfinvoke webservice="#Application.webname#" method="moveRecOverwrite" username="#Application.getExportDatauser#" password="#Application.getExportDataPass#" returnvariable="rv2">
			<cfinvokeargument name="destFolder" value="#dirmake2#"/>
			<cfinvokeargument name="moveFromRootFolders" value="#MyNewArray3#"/>
			<cfinvokeargument name="moveContentsIDs" value="#MyNewArray1#"/>
			<cfinvokeargument name="purgeNullChildDirOnNot" value="#bl1#" />
		</cfinvoke>
		<!------>
		<cfif rv1 neq true>
			<cfset session.mailmsg = session.mailmsg & "青森フォルダへ移動できませんでした。">
		</cfif>
		<cfif rv2 neq true>
			<cfset session.mailmsg = session.mailmsg & "沖縄フォルダへ移動できませんでした。">
		</cfif>
		
		<cfset erMsg="SUCCESS">
		<cfreturn erMsg>
	</cffunction>
	<!---BinaryDataをzipへ変換し、解凍する。解凍場所は日付（秒まで）とする--->
	<cffunction name="unZipFile"  output="false" returntype="any">
		<cfargument name="BinaryObj" type="string" required="yes">
		<cfargument name="jpgnm" type="string" required="yes">
		<cfargument name="date1" type="date" required="yes">
		
		<cffile action="write" file = "#ExpandPath('../apimg')#\#arguments.jpgnm#.zip" 
			output = "#toBinary(arguments.BinaryObj)#">
		<cfdirectory action = "create" directory = "#ExpandPath('../apimg')#\#year(date1)##month(date1)##day(date1)##hour(date1)##minute(date1)##second(date1)##arguments.jpgnm#" >
		<cfzip file="#ExpandPath('../apimg')#\#arguments.jpgnm#.zip" action="unzip" destination="#ExpandPath('../apimg')#\#year(date1)##month(date1)##day(date1)##hour(date1)##minute(date1)##second(date1)##arguments.jpgnm#\" overwrite="yes">
		<!---出来上がったらZIPファイルは消去--->
		<cffile action="Delete" 
            file="#ExpandPath('../apimg')#\#arguments.jpgnm#.zip"> 
			
		<cfdirectory action="list" name = "list1"
 directory="#ExpandPath('../apimg')#\#year(date1)##month(date1)##day(date1)##hour(date1)##minute(date1)##second(date1)##arguments.jpgnm#">
 <!------>
	<cfftp connection = "myConnection"
	   username = "#Application.ftp_user#"
	   password = "#Application.ftp_pass#"
	   server = "#Application.ftp_server#"
	   action = "open"
	   stopOnError = "Yes"> 
 
	<cfftp connection = "myConnection"
		action = "createDir"
		directory = "#Application.ftp_root##year(date1)##month(date1)##day(date1)##hour(date1)##minute(date1)##second(date1)##arguments.jpgnm#">

	<cfloop query="list1">
		 <cfset jpgrep = replace(list1.name,".JPEG",".jpg")>

		 <cffile
			action = "rename"
			destination = "#ExpandPath('../apimg')#\#year(date1)##month(date1)##day(date1)##hour(date1)##minute(date1)##second(date1)##arguments.jpgnm#\#jpgrep#" 
			source = "#ExpandPath('../apimg')#\#year(date1)##month(date1)##day(date1)##hour(date1)##minute(date1)##second(date1)##arguments.jpgnm#\#list1.name#">

	
 <!------>
	<cfftp 
	action="putfile" 
	connection="myConnection"
	localfile="#ExpandPath('../apimg')#\#year(date1)##month(date1)##day(date1)##hour(date1)##minute(date1)##second(date1)##arguments.jpgnm#\#jpgrep#"
	remotefile="#Application.ftp_root##year(date1)##month(date1)##day(date1)##hour(date1)##minute(date1)##second(date1)##arguments.jpgnm#/#jpgrep#"
	transfermode="auto"
	/>
	
	
		<cffile action="Delete" 
            file="#ExpandPath('../apimg')#\#year(date1)##month(date1)##day(date1)##hour(date1)##minute(date1)##second(date1)##arguments.jpgnm#\#jpgrep#"> 
	</cfloop>
 <!------>
	<cfftp connection = "myConnection"
	action = "close"
	stopOnError = "Yes">

	
	<cfdirectory  
		directory = "#ExpandPath('../apimg')#\#year(date1)##month(date1)##day(date1)##hour(date1)##minute(date1)##second(date1)##arguments.jpgnm#\" 
		action = "delete" >	
	
		
	</cffunction>
	<!---データを登録--->	
	<cffunction name="insertGetData"  output="false" returntype="any">
		<cfargument name="formatid" type="string" required="yes">
		<cfargument name="Answer" type="query" required="yes">
		<cfargument name="jpgnm" type="string" required="yes">
		<cfargument name="date1" type="date" required="yes">
		<cfargument name="ap_id" type="string" required="yes">
		<cfset ansquery = StructNew()>
		
		<cfset list1 = "１,２,３,４,５,６,７,８,９,０,Ａ,Ｂ,Ｃ,Ｄ,Ｅ,Ｆ,Ｇ,Ｈ,Ｉ,Ｊ,Ｋ,Ｌ,Ｍ,Ｎ,Ｏ,Ｐ,Ｑ,Ｒ,Ｓ,Ｔ,Ｕ,Ｖ,Ｗ,Ｘ,Ｙ,Ｚ,ａ,ｂ,ｃ,ｄ,ｅ,ｆ,ｇ,ｈ,ｉ,ｊ,ｋ,ｌ,ｍ,ｎ,ｏ,ｐ,ｑ,ｒ,ｓ,ｔ,ｕ,ｖ,ｗ,ｘ,ｙ,ｚ">
		<cfset list2 = "1,2,3,4,5,6,7,8,9,0,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z">
		<!---referencenumberの取得--->
		<cfquery dbtype="query" name="query1">
			select
				Answer.Answer as a1
			from
				Answer
			where
				q_str = 'パスワード' and b_code = 1
		</cfquery>
		<cfset defrValue = ReplaceList(query1.a1, list1, list2)>
		
		
		<!---取得したreferencenumberの登録の有無などをチェック--->
		<cfinvoke method="checkingRefnum" returnVariable="rValue">
			<cfinvokeargument name="r_num" value="#defrValue#">
			<cfinvokeargument name="formatid" value="#arguments.formatid#">
			<cfinvokeargument name="ap_id" value="#ap_id#">
		</cfinvoke>
		<cfif rValue eq -1>
			<cfreturn>
		</cfif>
		
		<!---登録時に必要なデータを取得する--->		
		<cfquery datasource="#Application.datasource#" name="query2">
			select
				rtt_reserve.arrive_date,
				rtt_reserve.depart_date
			from
				rtt_reserve
			where
				rtt_reserve.referencenumber = '#rValue#'
		</cfquery>
		<cfquery datasource="#Application.datasource#" name="query3">
			select
				mst_questionnairesheet.questionnairesheet_id,
				mst_questionnairesheet.resort_code
			from
				mst_questionnairesheet
			where
				('#query2.depart_date#' between mst_questionnairesheet.start_date and mst_questionnairesheet.end_date) and
				mst_questionnairesheet.ap_formatid = '#arguments.formatid#'
		</cfquery>
		<!---登録データをQueryOfQueryで抽出する--->		
		<cfquery dbtype="query" name="query4">
			select 
				*
			from
				Answer
			where
				q_str != 'パスワード' and answer is not null and answer != ''
		</cfquery>

		<!---hst_Answers用の答えの格納場所--->
		<cfset ex1 = 0>
		<cfset ex2 = 0>
		<cfset ex3 = 0>
		<cfset ex4 = 0>
		<cfset ex5 = 0>
		<cfset ex6 = 0>
		<cfset ex7 = "">
		<cfloop query="query4">
			<cfset q_id1 = ListGetAt(query4.q_str,1)>
			<!---q_id1（questionnaire_id）が無い場合は登録しない--->
			<cfif IsNumeric(q_id1)  eq true>
				<!---カンマ区切りでリストを取得し、その長さにより取得IDを変える--->
				<cfif ListLen(query4.Answer) gte 1>
					<cfset answer_id = ListGetAt(query4.Answer,1)>
				<cfelse>
					<cfset answer_id = 0>
				</cfif>
				<!---カンマ区切りでリストを取得し、その長さにより取得txtを変える--->
				<cfif ListLen(query4.Answer) gt 1>
					<cfset answer_txt = ListGetAt(query4.Answer,2)>
				<cfelse>
					<cfset answer_txt = query4.Answer>
				</cfif>
				<!---answeridが数字へ変換できない場合はanswer_id = 0, answer_txt = '未回答'とする。--->
				<cfif IsNumeric(answer_id) eq false>
					<cfset answer_id = 0>
					<cfset answer_txt = ListGetAt(query4.Answer,1)>
					<cfif len(answer_txt) eq 0>
						<cfset answer_txt = "未回答">
					</cfif>
				</cfif>
				
				<cfif answer_id eq 999>
					<cfset answer_id = 99>
				</cfif>
				
				<cfquery  datasource="#Application.datasource#" name="cntcheck">
					select count(*) as cnt from wk_answersdetails
					where referencenumber = '#rValue#' and questionnairesheet_id = #query3.questionnairesheet_id#
					and resort_code = #query3.resort_code# and questionnaire_id = #q_id1# and answer = #answer_id#
				</cfquery>
					<cfif cntcheck.cnt eq 0>
					
						<!---Query4のOtherがある場合はJpegファイルなので登録値にパスを入れる--->
						<cfif len(query4.Other) eq 0>
							<!---通常登録--->
							<cfquery  datasource="#Application.datasource#" name="query5">
								insert into wk_answersdetails 
									(referencenumber,questionnairesheet_id,resort_code,
									questionnaire_id,answer,answertext,othertext)
								values
									('#rValue#',#query3.questionnairesheet_id#,#query3.resort_code#,
									#q_id1#,#answer_id#,'#answer_txt#','')
							</cfquery>
						<cfelse>
							<cfset jpgrep = replace(query4.Other,".JPEG",".jpg")>
							<!---jpegのパスを入れる--->
							<cfquery  datasource="#Application.datasource#" name="query5">
								insert into wk_answersdetails 
									(referencenumber,questionnairesheet_id,resort_code,
									questionnaire_id,answer,answertext,othertext)
								values
									('#rValue#',#query3.questionnairesheet_id#,#query3.resort_code#,
									#q_id1#,#answer_id#,'','#Application.imgpath_db#/#year(date1)##month(date1)##day(date1)##hour(date1)##minute(date1)##second(date1)##arguments.jpgnm#/#jpgrep#')
							</cfquery>
						</cfif>
					</cfif>
				</cfif><!------>
				<!---Application.cfm内のidと比べて該当の者がある場合は各々、値を格納--->
				<cfif q_id1 eq Application.gender_id>
					<cfset ex1 = answer_id>
				<cfelseif q_id1 eq Application.married_id>
					<cfset ex2 = answer_id>
				<cfelseif q_id1 eq Application.age_id>
					<cfset ex3 = answer_id>
				<cfelseif q_id1 eq Application.child_id>
					<cfset ex4 = answer_id>
				<cfelseif q_id1 eq Application.job_id>
					<cfset ex5 = answer_id>
				<cfelseif q_id1 eq Application.trip_id>
					<cfset ex6 = answer_id>
				<cfelseif q_id1 eq Application.old_ref>
					<cfset ex7 = answer_id>
				</cfif>
		</cfloop>
		<cfif defrValue eq "未回答">
			<cfset defrValue = "">
		</cfif>
		<!---hst_answersへの登録--->
		<cfquery  datasource="#Application.datasource#" name="query6">
			insert into hst_answers 
				(referencenumber,questionnairesheet_id,resort_code,
				 depart_date,arrive_date,registrated,answermethod,
				gender_id,married_id,age_id,child_id,job_id,trip_id,old_ref,ap_id)
			values
				('#rValue#',#query3.questionnairesheet_id#,#query3.resort_code#,
				'#query2.depart_date#','#query2.arrive_date#',convert(varchar,getdate(),111),9,
				#ex1#,#ex2#,#ex3#,#ex4#,#ex5#,#ex6#,'#defrValue#','#arguments.ap_id#')
		</cfquery>
		<!---未回答→利用しないへ変更--->
		<cfquery datasource="#Application.datasource#" name="get_answerdetail">
			update	 wk_answersdetails
			set		 answer = 8
			where	 referencenumber = '#rValue#'
			and		 questionnairesheet_id = #query3.questionnairesheet_id#
			and 	 answer = 0
			and		 questionnaire_id in (
						select	 a.questionnaire_id
						from	 oth_answeroptions as a
								,mst_questionnaire as b
						where	 a.questionnaire_id = b.questionnaire_id
						and		 b.questionnaire_type = 1
						and		 a.questionnairesheet_id = #query3.questionnairesheet_id#
						and		 a.resort_code = #query3.resort_code#
						group by a.questionnaire_id,a.resort_code,a.questionnairesheet_id
						having count(*) > 7
						)
		</cfquery>
		<!---利用しないにもかかわらず回答しているものを未回答へ変更--->
		<cfquery datasource="#Application.datasource#" name="get_answerdetail">
			update	 wk_answersdetails
			set		 answer = 8
			where	 referencenumber = '#rValue#'
			and		 questionnairesheet_id = #query3.questionnairesheet_id#
			and		 questionnaire_id in (
						select	 unused_id
						from	 oth_unused
						where	 resort_code = #query3.resort_code#
						and		 questionnairesheet_id = #query3.questionnairesheet_id#
						and		 questionnaire_id in (
									select	questionnaire_id
									from	wk_answersdetails
									where	referencenumber = '#rValue#'
									and		questionnairesheet_id = #query3.questionnairesheet_id#
									and		answer = 8
									and		questionnaire_id in (
										select	 questionnaire_id
										from	 oth_unused
										where	 resort_code = #query3.resort_code#
										and		 questionnairesheet_id = #query3.questionnairesheet_id#
										group by questionnaire_id
									)
						)
			)
		</cfquery>
		<cfif defrValue neq "未回答">
			<cfif  len(defrValue) neq 0>
				<cfif not isdefined("session.mailflg") or session.mailflg eq 0>
					<cfinvoke method="SendmailAndElog">
						<cfinvokeargument name="r_num" value="#defrValue#">
						<cfinvokeargument name="formatid" value="#arguments.formatid#">
						<cfinvokeargument name="ap_id" value="#ap_id#">
						<cfinvokeargument name="r_num2" value="#rValue#">
						<cfinvokeargument name="checking" value="0">
					</cfinvoke>
					<cfset session.mailflg = 0>
				<cfelse>
					<cfset session.mailflg = 0>
				</cfif>
			</cfif>
		</cfif>
		<cfreturn>
	</cffunction>
	<!---データの登録の有無のチェックetc--->
	<cffunction name="checkingRefnum"  output="false" returntype="any">
		<cfargument name="r_num" type="string" required="yes">
		<cfargument name="formatid" type="string" required="yes">
		<cfargument name="ap_id" type="string" required="yes">
		
		<cfset list1 = "１,２,３,４,５,６,７,８,９,０,Ａ,Ｂ,Ｃ,Ｄ,Ｅ,Ｆ,Ｇ,Ｈ,Ｉ,Ｊ,Ｋ,Ｌ,Ｍ,Ｎ,Ｏ,Ｐ,Ｑ,Ｒ,Ｓ,Ｔ,Ｕ,Ｖ,Ｗ,Ｘ,Ｙ,Ｚ,ａ,ｂ,ｃ,ｄ,ｅ,ｆ,ｇ,ｈ,ｉ,ｊ,ｋ,ｌ,ｍ,ｎ,ｏ,ｐ,ｑ,ｒ,ｓ,ｔ,ｕ,ｖ,ｗ,ｘ,ｙ,ｚ">
		<cfset list2 = "1,2,3,4,5,6,7,8,9,0,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z,a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z">
		<cfset arguments.r_num = ReplaceList(arguments.r_num, list1, list2)>
		
		<!---まず該当のIDがあるかどうかチェック--->
		<cfquery  datasource="#Application.datasource#" name="query1">
			select
				referencenumber
			from
				rtt_reserve
			where
				rtt_reserve.referencenumber = '#arguments.r_num#'
		</cfquery>
		<!---無ければ不明登録--->
		<cfif query1.recordcount eq 0>
			
				<cfquery  datasource="#Application.datasource#" name="query4">
					select
						referencenumber
					from
						hst_answers
					where
						hst_answers.ap_id = '#arguments.ap_id#'
				</cfquery>
				<cfif query4.recordcount eq 0>
					<cfif mid(arguments.r_num,1,3) eq 'zzz'>
					
						<cfset inp_reserve_code=mid(arguments.r_num,4,len(arguments.r_num)-3)>
						
						<cfquery datasource="#Application.datasource#" name="checkreg">
							select
								*
							from
								rtt_reserve inner join mst_questionnairesheet on
								rtt_reserve.resort_code = mst_questionnairesheet.resort_code
							where
								reserve_code = '#inp_reserve_code#' and
								ap_formatid = #arguments.formatid#
						</cfquery>
						<cfif checkreg.recordcount neq 0>
							<cfquery datasource="#Application.datasource#" name="q_ref">
								select	 isnull(max(right(wk_a.referencenumber,3)+1),1) as referencenumber 
								from	(select referencenumber,resort_code from dbo.rtt_reserve where left(referencenumber,3) = 'zzz')	 wk_a
										inner join mst_questionnairesheet on
										wk_a.resort_code = mst_questionnairesheet.resort_code
								where	 right(left(referencenumber,len(referencenumber)-3),len(left(referencenumber,len(referencenumber)-3))-3) = '#inp_reserve_code#'
								and		 left(referencenumber,3) = 'zzz'
								and		 ap_formatid = #arguments.formatid#
							</cfquery>
						
							<cfset arguments.r_num = arguments.r_num & numberformat(q_ref.referencenumber,'000')>
						
							<!--- 照会用番号書き換え --->
							<cfquery datasource="#Application.datasource#" name="q_grouptarget">
								select	 top 1 rsv.reserve_code
										,rsv.resort_code
										,rsv.room_code
										,rsv.referencenumber
								from	 dbo.rtm_customer cst,
										 dbo.rtt_reserve rsv,
										 dbo.mst_questionnairesheet qsheet
								where	 rsv.reserve_code=#inp_reserve_code#
								and		 rsv.customernumber=cst.customernumber
								and		 qsheet.resort_code=rsv.resort_code
								and		 qsheet.ap_formatid = #arguments.formatid#
								and		 substring(rsv.referencenumber ,1 ,3) != 'zzz'
								order by rsv.depart_date,rsv.room_code
							</cfquery>
							<cfquery datasource="#Application.datasource#" name="u_grouptarget">
								update dbo.rtt_reserve set referencenumber = '#arguments.r_num#'
								from   dbo.rtt_reserve
								where	 reserve_code = #q_grouptarget.reserve_code#
								and		 room_code = #q_grouptarget.room_code#
								and		 substring(referencenumber ,1 ,3) != 'zzz'
								and		 resort_code = #q_grouptarget.resort_code#
							</cfquery>
								<cfreturn arguments.r_num>
							</cfif>
					</cfif>
					<cfinvoke method="InsertunknownData" returnVariable="rValue">
						<cfinvokeargument name="r_num" value="#arguments.r_num#">
						<cfinvokeargument name="formatid" value="#arguments.formatid#">
					</cfinvoke>
					<!---メール送信--->
					<!---cflog--->
					<cfinvoke method="SendmailAndElog">
						<cfinvokeargument name="r_num" value="#arguments.r_num#">
						<cfinvokeargument name="formatid" value="#arguments.formatid#">
						<cfinvokeargument name="ap_id" value="#ap_id#">
						<cfinvokeargument name="r_num2" value="#rValue#">
						<cfinvokeargument name="checking" value="1">
					</cfinvoke>
					<cfreturn rValue>
				<cfelse>
					<cfinvoke method="SendmailAndElog">
						<cfinvokeargument name="r_num" value="">
						<cfinvokeargument name="formatid" value="#arguments.formatid#">
						<cfinvokeargument name="ap_id" value="#ap_id#">
						<cfinvokeargument name="r_num2" value="">
						<cfinvokeargument name="checking" value="99">
					</cfinvoke>
					<cfset rValue = -1>
					<cfreturn rValue>
				</cfif>
		<cfelse>
			<!---ある場合はさらに解答済みかチェック--->
			<cfquery  datasource="#Application.datasource#" name="query2">
				select
					referencenumber
				from
					hst_answers
				where
					hst_answers.referencenumber = '#arguments.r_num#'
			</cfquery>
			
			<!---解答していなければそのままのr_numを返す--->
			<cfif query2.recordcount eq 0>
				<cfif mid(arguments.r_num,1,3) eq 'zzz'>
				
					<cfset inp_reserve_code=mid(arguments.r_num,4,len(arguments.r_num)-3)>
					
					<cfquery datasource="#Application.datasource#" name="checkreg">
						select
							*
						from
							rtt_reserve inner join mst_questionnairesheet on
							rtt_reserve.resort_code = mst_questionnairesheet.resort_code
						where
							reserve_code = '#inp_reserve_code#' and
							ap_formatid = #arguments.formatid#
					</cfquery>
					<cfif checkreg.recordcount neq 0>
						<cfquery datasource="#Application.datasource#" name="q_ref">
							select	 isnull(max(right(wk_a.referencenumber,3)+1),1) as referencenumber 
							from	(select referencenumber,resort_code from dbo.rtt_reserve where left(referencenumber,3) = 'zzz')	 wk_a
									inner join mst_questionnairesheet on
									wk_a.resort_code = mst_questionnairesheet.resort_code
							where	 right(left(referencenumber,len(referencenumber)-3),len(left(referencenumber,len(referencenumber)-3))-3) = '#inp_reserve_code#'
							and		 left(referencenumber,3) = 'zzz'
							and		 ap_formatid = #arguments.formatid#
						</cfquery>
					
						<cfset arguments.r_num = arguments.r_num & numberformat(q_ref.referencenumber,'000')>
					
						<!--- 照会用番号書き換え --->
						<cfquery datasource="#Application.datasource#" name="q_grouptarget">
							select	 top 1 rsv.reserve_code
									,rsv.resort_code
									,rsv.room_code
									,rsv.referencenumber
							from	 dbo.rtm_customer cst,
									 dbo.rtt_reserve rsv,
									 dbo.mst_questionnairesheet qsheet
							where	 rsv.reserve_code=#inp_reserve_code#
							and		 rsv.customernumber=cst.customernumber
							and		 qsheet.resort_code=rsv.resort_code
							and		 qsheet.ap_formatid = #arguments.formatid#
							and		 substring(rsv.referencenumber ,1 ,3) != 'zzz'
							order by rsv.depart_date,rsv.room_code
						</cfquery>
						<cfquery datasource="#Application.datasource#" name="u_grouptarget">
							update dbo.rtt_reserve set referencenumber = '#arguments.r_num#'
							from   dbo.rtt_reserve
							where	 reserve_code = #q_grouptarget.reserve_code#
							and		 room_code = #q_grouptarget.room_code#
							and		 substring(referencenumber ,1 ,3) != 'zzz'
							and		 resort_code = #q_grouptarget.resort_code#
						</cfquery>
						<cfreturn arguments.r_num>
					</cfif>
				</cfif>
				<cfreturn arguments.r_num>
			<cfelse>
				<cfquery  datasource="#Application.datasource#" name="query3">
					select
						referencenumber
					from
						hst_answers
					where
						hst_answers.ap_id = '#arguments.ap_id#'
				</cfquery>
				<cfif query3.recordcount eq 0>
				<!---解答している場合は不明登録にする--->
					<cfinvoke method="InsertunknownData" returnVariable="rValue">
						<cfinvokeargument name="r_num" value="#arguments.r_num#">
						<cfinvokeargument name="formatid" value="#arguments.formatid#">
					</cfinvoke>
					<cfinvoke method="SendmailAndElog">
						<cfinvokeargument name="r_num" value="#arguments.r_num#">
						<cfinvokeargument name="formatid" value="#arguments.formatid#">
						<cfinvokeargument name="ap_id" value="#ap_id#">
						<cfinvokeargument name="r_num2" value="#rValue#">
						<cfinvokeargument name="checking" value="2">
					</cfinvoke>
					<cfreturn rValue>
				<cfelse>
					<cfinvoke method="SendmailAndElog">
						<cfinvokeargument name="r_num" value="#arguments.r_num#">
						<cfinvokeargument name="formatid" value="#arguments.formatid#">
						<cfinvokeargument name="ap_id" value="#ap_id#">
						<cfinvokeargument name="r_num2" value="">
						<cfinvokeargument name="checking" value="99">
					</cfinvoke>
					<cfset rValue = -1>
					<cfreturn rValue>
				</cfif>
			</cfif>
		</cfif>
		
	</cffunction>
	<!---不明登録データの作成--->
	<cffunction name="InsertunknownData"  output="false" returntype="any">
		<cfargument name="r_num" type="string" required="yes">
		<cfargument name="formatid" type="string" required="yes">
		<!---リゾートコードの取得--->
		<cfquery  datasource="#Application.datasource#" name="query1">
			select
				resort_code
			from
				mst_questionnairesheet
			where
				mst_questionnairesheet.ap_formatid like '%#arguments.formatid#%'
		</cfquery>
		<cfset resortcode = query1.resort_code>
		<!---リサーブコードの取得--->
		<cfquery  datasource="#Application.datasource#" name="query2">
			select	 isnull(max(reserve_code),900000000)+1 as cnt1
			from	 rtt_reserve
			where	 reserve_code >= 900000000
			and		 resort_code = #resortcode#
		</cfquery>
		<cfset reservecode = query2.cnt1>
		<!---取得後に不明コードを登録する--->
		<cfquery  datasource="#Application.datasource#" name="query1">
			insert into rtt_reserve (resort_code,reserve_code,room_code,customernumber,arrive_date,depart_date,referencenumber)
			values (#resortcode#,#reservecode#,0,'CRM#numberformat(resortcode,'00')#9999UDU',convert(varchar,getdate(),111),
					convert(varchar,getdate(),111),'CRM#resortcode##reservecode#' )
		</cfquery>
		
		<cfset refnum = "CRM" & resortcode & reservecode>
		<cfreturn refnum>
	</cffunction>
	<cffunction name="SendmailAndElog"  output="false" returntype="any">
		<cfargument name="r_num" type="string" required="yes">
		<cfargument name="formatid" type="string" required="yes">
		<cfargument name="ap_id" type="string" required="yes">
		<cfargument name="r_num2" type="string" required="yes">
		<cfargument name="checking" type="string" required="yes">
		
		<cfset msg = "">
		<cfset msg2 = "">
		
		<cfset session.mailflg = 0>
		
		<cfif checking eq "0">
<!--- 
			<cfset msg = chr(13) & chr(10) &  arguments.r_num & "登録成功" & chr(13) & chr(10) &  "フォーマットID:"& arguments.formatid & " ALT-P ID:"& arguments.ap_id>
 --->
			<cfset msg = chr(13) & chr(10) &  "【ALT-P ID: "& arguments.ap_id & "】⇒【正常登録（ " & arguments.r_num & "）】">
			<cfset session.mailflg = 0>
		<cfelseif checking eq "99">
<!--- 
			<cfset msg = chr(13) & chr(10) & chr(13) & chr(10) &  arguments.r_num & "すでに登録済みです。"& chr(13) & chr(10) &  "フォーマットID:"& arguments.formatid & " ALT-P ID:"& arguments.ap_id>
 --->
			<cfset msg = chr(13) & chr(10) &  "【ALT-P ID: "& arguments.ap_id & "】⇒【登録済み（ " & arguments.r_num & "）】">
			<cfset msg2 = "rtt_reserveへ存在していません。 referencenumber:" & arguments.r_num & " フォーマットID:"& 
			arguments.formatid & " ALT-P ID:"& arguments.ap_id & " CRMKitchen 不明登録ID:"& arguments.r_num2>
			<cfset session.mailflg = 1>
		<cfelseif checking eq "100">
			<cfset msg = chr(13) & chr(10) & chr(13) & chr(10) &  "本日は登録対象がありませんでした。">
			<cfset msg2 = "本日の登録は0件です。">

		<cfelseif checking eq "1">
<!--- 
			<cfset msg = chr(13) & chr(10) & chr(13) & chr(10) &  "不明登録（照会用番号無し）"& chr(13) & chr(10) & "フォーマットID:"& arguments.formatid & " ALT-P ID:"& arguments.ap_id & " CRMKitchen 不明登録ID:"& arguments.r_num2>
 --->
			<cfset msg = chr(13) & chr(10) &  "【ALT-P ID: "& arguments.ap_id & "】⇒【不明登録 -照会用番号無し-（ " & arguments.r_num & " ⇒ " & arguments.r_num2 & "）】">

			<cfset msg2 = "rtt_reserveへ存在していません。 referencenumber:" & arguments.r_num & " フォーマットID:"& 
			arguments.formatid & " ALT-P ID:"& arguments.ap_id & " CRMKitchen 不明登録ID:"& arguments.r_num2>
			<cfset session.mailflg = 1>
		<cfelseif  checking eq "2">
<!--- 
			<cfset msg = chr(13) & chr(10) & chr(13) & chr(10) &  "不明登録（該当番号回答済み）" & chr(13) & chr(10) & "フォーマットID:"& arguments.formatid & " ALT-P ID:"& arguments.ap_id & " CRMKitchen 不明登録ID:"& arguments.r_num2>
 --->
			<cfset msg = chr(13) & chr(10) &  "【ALT-P ID: "& arguments.ap_id & "】⇒【不明登録 -該当番号回答済-（ " & arguments.r_num & " ⇒ " & arguments.r_num2 & "）】">

			<cfset msg2 = "rtt_reserveへ重複登録。 referencenumber:" & arguments.r_num & " フォーマットID:"& 
			arguments.formatid & " ALT-P ID:"& arguments.ap_id & " CRMKitchen 不明登録ID:"& arguments.r_num2>

			<cfset session.mailflg = 1>
		<cfelseif  checking eq "11">
			<cfset msg = chr(13) & chr(10) & chr(13) & chr(10) & "ALT-Pシステムへ情報を取得しに行きましたが失敗しました。作業工程 = 1 フォーマットID:"& arguments.formatid>
			<cfset msg2 = "ALT-Pシステムへ情報を取得しに行きましたが失敗しました。作業工程 = 1 フォーマットID:"& arguments.formatid>
			<cfset session.mailflg = 1>
		<cfelseif  checking eq "12">
			<cfset msg = chr(13) & chr(10) & chr(13) & chr(10) &  "ALT-Pシステムへ情報を取得しに行きましたが失敗しました。作業工程 = 2 フォーマットID:"& arguments.formatid>
			<cfset msg2 = "ALT-Pシステムへ情報を取得しに行きましたが失敗しました。作業工程 = 2 フォーマットID:"& arguments.formatid>
			<cfset session.mailflg = 1>
		<cfelseif  checking eq "13">
			<cfset msg = chr(13) & chr(10) &  "ALT-Pシステムへ情報を取得しに行きましたが失敗しました。作業工程 = 3 フォーマットID:"& arguments.formatid>
			<cfset msg2 = "ALT-Pシステムへ情報を取得しに行きましたが失敗しました。作業工程 = 3 フォーマットID:"& arguments.formatid>
			<cfset session.mailflg = 1>
		<cfelseif  checking eq "14">
			<cfset msg = chr(13) & chr(10) & "ALT-Pシステムへ情報を取得しに行きましたが失敗しました。作業工程 = 4 フォーマットID:"& arguments.formatid>
			<cfset msg2 = "ALT-Pシステムへ情報を取得しに行きましたが失敗しました。作業工程 = 4 フォーマットID:"& arguments.formatid>
			<cfset session.mailflg = 1>
		</cfif>
		
		<cfset session.mailmsg = session.mailmsg & msg>
		
		<cflog file="alertlog" text="#msg2#">
		<cfreturn msg2>
	</cffunction>
	
		<cffunction name="Sendmail"  output="false" returntype="any">
		<cfargument name="Msg" type="string" required="yes">
		<cfargument name="formatid" type="string" required="yes">
		
		<cfquery  datasource="#Application.datasource#" name="query1">
			select
				mst_resort.resort_nm
			from
				mst_questionnairesheet inner join mst_resort on
				mst_questionnairesheet.resort_code = mst_resort.resort_code
			where
				mst_questionnairesheet.ap_formatid like '%#arguments.formatid#%'
		</cfquery>
		<cfset msgheader = "---------------------------------------------------------------------------------
AltPaper⇒CRMKitchenインポート結果　
#query1.resort_nm#【フォーマットID：#arguments.formatid#】【取込日：#dateformat(now(),'yyyy/mm/dd')#】
---------------------------------------------------------------------------------">
		<cfset msg = #msgheader# & chr(13) & #Msg#>
		<cfmail to = "#Application.mail_to#" from = "#Application.mail_from#" cc="#Application.mail_cc#" subject = "#arguments.formatid#:#query1.resort_nm#:#Application.mail_subject1#" port="#Application.mail_port#"
		 server="#Application.mail_smtp#" password="#Application.mail_pw#" username="#Application.mail_usernm#">
			#msg#
        </cfmail>
		<cfset session.mailmsg = "">
		<cfreturn msg>
	</cffunction>

</cfcomponent>