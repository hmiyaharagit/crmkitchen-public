﻿<cfprocessingdirective suppresswhitespace=true pageEncoding="UTF-8">
<cfsetting enableCFoutputOnly = "Yes" showdebugoutput="yes">
<cfoutput>
<!---
<cfset url.batchid = 36447>
<cfset url.formatid = 3419>
<cfset url.tasknm = "ryokan_task1">
--->

<cfset erMsg = "">
<cfset session.mailmsg = "">
<cfset flg = 0>

<cftry>
	<!------>
	<cfinvoke component="getalt_p" method="getExportData3" returnVariable="rValue3">
		<cfinvokeargument name="batchid" value="#url.batchid#">
		<cfinvokeargument name="userid" value="#Application.getExportDatauser#">
		<cfinvokeargument name="userpw" value="#Application.getExportDataPass#">
	</cfinvoke>
	<!------>
	<cfif rValue3 eq "COMPLETE">
		<cfinvoke component="getalt_p" method="getExportData4" returnVariable="rValue4">
			<cfinvokeargument name="batchid" value="#url.batchid#">
			<cfinvokeargument name="formatid" value="#url.formatid#">
			<cfinvokeargument name="userid" value="#Application.getExportDatauser#">
			<cfinvokeargument name="userpw" value="#Application.getExportDataPass#">
			<cfinvokeargument name="tasknm" value="#url.tasknm#">
		</cfinvoke>
		<cfif  rvalue4 neq "FAILED">
	
		<cfelse>
			<cfinvoke component="getalt_p" method="SendmailAndElog" returnVariable="rValue">
				<cfinvokeargument name="r_num" value="">
				<cfinvokeargument name="formatid" value="#url.formatid#">
				<cfinvokeargument name="ap_id" value="">
				<cfinvokeargument name="r_num2" value="">
				<cfinvokeargument name="checking" value="14">
			</cfinvoke>
		</cfif>
	<cfelseif rvalue3 eq "ERROR">
		<cflog file="alertlog" text="#rValue3.errorlog#">
		<cfinvoke component="getalt_p" method="SendmailAndElog" returnVariable="rValue">
			<cfinvokeargument name="r_num" value="">
			<cfinvokeargument name="formatid" value="#url.formatid#">
			<cfinvokeargument name="ap_id" value="">
			<cfinvokeargument name="r_num2" value="">
			<cfinvokeargument name="checking" value="13">
		</cfinvoke>
	<cfelseif rvalue3 eq "FAILED">
		<cfinvoke component="getalt_p" method="SendmailAndElog" returnVariable="rValue">
			<cfinvokeargument name="r_num" value="">
			<cfinvokeargument name="formatid" value="#url.formatid#">
			<cfinvokeargument name="ap_id" value="">
			<cfinvokeargument name="r_num2" value="">
			<cfinvokeargument name="checking" value="13">
		</cfinvoke>
	</cfif>
	
		<cfinvoke component="getalt_p" method="Sendmail" returnVariable="rValue">
			<cfinvokeargument name="Msg" value="#session.mailmsg#">
			<cfinvokeargument name="formatid" value="#url.formatid#">
		</cfinvoke>
	<!---スケジュールはどんな場合でも削除--->
	<cfschedule action = "delete" task = "#url.tasknm#">
<cfcatch>
	<cfif isdefined("cfcatch.sql") eq true>
		<cfset flg = 1>
		<cfset erMsg = "#session.mailmsg#" & chr(13) & chr(10) &  "#cfcatch.sql#" & chr(13) & chr(10) &  "【ALT-P ID: "& session.errorid & "】をエラーフォルダへ移動しタスクを再実行します。">
	<cfinvoke component="getalt_p" method="Sendmail" returnVariable="rValue">
		<cfinvokeargument name="Msg" value="#erMsg#">
		<cfinvokeargument name="formatid" value="#url.formatid#">
	</cfinvoke>
	<cfelse>
		<cfschedule action = "delete" task = "#url.tasknm#">
		<cfinvoke component="getalt_p" method="Sendmail" returnVariable="rValue">
			<cfinvokeargument name="Msg" value="#cfcatch.message#">
			<cfinvokeargument name="formatid" value="#url.formatid#">
		</cfinvoke>
		<cflog file ="mic" text="#cfcatch.detail#">
	<!--- DBエラー時詳細取得 --->
		<cfif isdefined("cfcatch.sql")>
			<cflog file ="mic" text="#cfcatch.sql#">
			<cflog file ="mic" text="#cfcatch.detail#">
			<cfabort>
		</cfif>
		
	</cfif>
</cfcatch>
</cftry>

<cfif flg eq 1>
<cftry>
	
	<cfset MyNewArray1 = ArrayNew(1)>
	<cfset MyNewArray1[1] = "#session.errorid#">
	
	<!------>
	<!---青森用--->
	<cfset dirmake1 = ArrayNew(1)>
	<!--- 配列が ArraySet によって初期化されていないと、ArrayToList は正常に動作しません。 --->
	
	<cfset date1 = now()>
	<!--- 要素を設定 --->
	<cfset dirmake1[1] = "#Application.er_dir1#">
	<cfset dirmake1[2] = "#Application.dir2_1#">
	
	<cfset d1 = "0">
	
	<cfif month(date1) lt 10>
		<cfset d1 = "0#month(date1)#">
	<cfelse>
		<cfset d1 = "#month(date1)#">
	</cfif>
	
	<cfset d2 = "0">
	
	<cfif day(date1) lt 10>
		<cfset d2 = "0#day(date1)#">
	<cfelse>
		<cfset d2 = "#day(date1)#">
	</cfif>

	
	<cfset dirmake1[3] = "#year(date1)##d1##d2#">
	
	<cfset rv1 = false>
	<cfset rv2 = false>
	<cfset rv3 = false>
	<cfset rv4 = false>
	
	<cfinvoke webservice="#Application.webname#" method="ensureMakeDir" username="#Application.getExportDatauser#" password="#Application.getExportDataPass#" returnvariable="rv3">
		<cfinvokeargument name="pathElementsFromRoot" value="#dirmake1#"/>
	</cfinvoke>
	<!---沖縄用--->
	<cfset dirmake2 = ArrayNew(1)>
	<!--- 配列が ArraySet によって初期化されていないと、ArrayToList は正常に動作しません。 --->
	
	<!--- 要素を設定 --->
	<cfset dirmake2[1] = "#Application.er_dir1#">
	<cfset dirmake2[2] = "#Application.dir2_2#">
	<cfset dirmake2[3] = "#year(date1)##d1##d2#">
	
	<cfinvoke webservice="#Application.webname#" method="ensureMakeDir"  username="#Application.getExportDatauser#" password="#Application.getExportDataPass#" returnvariable="rv4">
		<cfinvokeargument name="pathElementsFromRoot" value="#dirmake2#"/>
	</cfinvoke>
	<!------>
	<cfif rv3 neq true or not IsDefined("rv3")>
		<cfset session.mailmsg = session.mailmsg & "登録済みフォルダ内へフォルダを作成できませんでした。その為、移動が出来ません。">
		<cfset erMsg="SUCCESS">
		<cfreturn erMsg>
	</cfif>
	<cfif rv4 neq true or not IsDefined("rv4")>
		<cfset session.mailmsg = session.mailmsg & "登録済みフォルダ内へフォルダを作成できませんでした。その為、移動が出来ません。">
		<cfset erMsg="SUCCESS">
		<cfreturn erMsg>
	</cfif>
	
	<cfset MyNewArray2_2 = ArrayNew(1)>
	<cfset MyNewArray2_2[1] = "#Application.folder1#">
	<cfset jtest2 = structNew()>
	<cfset jtest2.item = MyNewArray2_2>
	
	<cfset MyNewArray2 = ArrayNew(1)>
	<cfset MyNewArray2[1] = jtest2>
	
	<cfset bl1 = true>
	<!---青森用--->
	<cfinvoke webservice="#Application.webname#" method="moveRecOverwrite" username="#Application.getExportDatauser#" password="#Application.getExportDataPass#" returnvariable="rv1">
		<cfinvokeargument name="destFolder" value="#dirmake1#"/>
		<cfinvokeargument name="moveFromRootFolders" value="#MyNewArray2#"/>
		<cfinvokeargument name="moveContentsIDs" value="#MyNewArray1#"/>
		<cfinvokeargument name="purgeNullChildDirOnNot" value="#bl1#" />
	</cfinvoke>
	<!---沖縄用--->
	<cfset MyNewArray3_2 = ArrayNew(1)>
	<cfset MyNewArray3_2[1] = "#Application.folder2#">
	<cfset jtest3 = structNew()>
	<cfset jtest3.item = MyNewArray3_2>
	
	<cfset MyNewArray3 = ArrayNew(1)>
	<cfset MyNewArray3[1] = jtest3>
			
	<cfinvoke webservice="#Application.webname#" method="moveRecOverwrite" username="#Application.getExportDatauser#" password="#Application.getExportDataPass#" returnvariable="rv2">
		<cfinvokeargument name="destFolder" value="#dirmake2#"/>
		<cfinvokeargument name="moveFromRootFolders" value="#MyNewArray3#"/>
		<cfinvokeargument name="moveContentsIDs" value="#MyNewArray1#"/>
		<cfinvokeargument name="purgeNullChildDirOnNot" value="#bl1#" />
	</cfinvoke>
	<!------>
	<cfif rv1 neq true>
		<cfset session.mailmsg = session.mailmsg & "青森フォルダへ移動できませんでした。">
	</cfif>
	<cfif rv2 neq true>
		<cfset session.mailmsg = session.mailmsg & "沖縄フォルダへ移動できませんでした。">
	</cfif>
	<cfset date1 =DateAdd("n",10,now())>
	<cfschedule action = "delete" task = "#url.tasknm#">
	<cfschedule action = "update"
	   task = "ex_#url.tasknm#" 
	   operation = "HTTPRequest"
	   url = "#Application.scheduleURL2#?formatid=#url.formatid#&tasknm=ex_#url.tasknm#"
	   startDate = "#year(date1)#/#month(date1)#/#day(date1)#"
	   startTime = "#hour(date1)#:#minute(date1)#"
	   interval = "once"
	   resolveURL = "Yes"
	   publish = "Yes"
	   file = "result.html"
	   path = "#expandpath('result')#"
	   requestTimeOut = "3600">
	   
	   
<cfcatch>
	<cfschedule action = "delete" task = "#url.tasknm#">
	<cfinvoke component="getalt_p" method="Sendmail" returnVariable="rValue">
		<cfinvokeargument name="Msg" value="#cfcatch.message#">
		<cfinvokeargument name="formatid" value="#url.formatid#">
	</cfinvoke>
</cfcatch>
</cftry>
</cfif>
</cfoutput>
</cfprocessingdirective>
