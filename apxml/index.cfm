﻿<cfprocessingdirective suppresswhitespace=true pageEncoding="UTF-8">
<cfsetting enableCFoutputOnly = "Yes" showdebugoutput="yes">
<cfoutput>

<cfif isdefined("url.formatid") eq true>
	<cfschedule action = "delete" task = "#url.tasknm#">
	<cfquery datasource="#Application.datasource#" name="query1">
		select
			'#url.formatid#' as resortid
	</cfquery>
<cfelse>
	<cfquery datasource="#Application.datasource#" name="query1">
		select
			ap_formatid as resortid
		from
			mst_questionnairesheet
		where
			ap_formatid is not null
	</cfquery>
</cfif>
<cfset session.mailmsg = "">

<cfset getString = "">
<cfset cnt = 1>
<cfloop query="query1">
	<cfloop list="#query1.resortid#" index="f_id">
	
		<cfset f1 = URLEncodedFormat(Application.folder1,"utf-8")>
		<cfset f2 = URLEncodedFormat(Application.folder2,"utf-8")>
		
	
		<cfinvoke component="getalt_p" method="getExportData1" returnVariable="rValue1">
			<cfinvokeargument name="folder1" value="#f1#">
			<cfinvokeargument name="folder2" value="#f2#">
			<cfinvokeargument name="formatid" value="#f_id#">
			<cfinvokeargument name="userid" value="#Application.getExportDatauser#">
			<cfinvokeargument name="userpw" value="#Application.getExportDataPass#">
		</cfinvoke>
		<cfif rValue1 neq "FAILED">
			<cfinvoke component="getalt_p" method="getExportData2" returnVariable="rValue2">
				<cfinvokeargument name="data2Xml" value="#rValue1#">
				<cfinvokeargument name="userid" value="#Application.getExportDatauser#">
				<cfinvokeargument name="userpw" value="#Application.getExportDataPass#">
			</cfinvoke>
			
			<cfif rValue2 eq "FAILED">
				<cfinvoke component="getalt_p" method="SendmailAndElog" returnVariable="rValue">
					<cfinvokeargument name="r_num" value="">
					<cfinvokeargument name="formatid" value="#f_id#">
					<cfinvokeargument name="ap_id" value="">
					<cfinvokeargument name="r_num2" value="">
					<cfinvokeargument name="checking" value="12">
				</cfinvoke>
				<cfinvoke component="getalt_p" method="Sendmail" returnVariable="rValue">
					<cfinvokeargument name="Msg" value="#session.mailmsg#">
					<cfinvokeargument name="formatid" value="#f_id#">
				</cfinvoke>
			<cfelseif rValue2 eq "NODATA">
				<cfinvoke component="getalt_p" method="SendmailAndElog" returnVariable="rValue">
					<cfinvokeargument name="r_num" value="">
					<cfinvokeargument name="formatid" value="#f_id#">
					<cfinvokeargument name="ap_id" value="">
					<cfinvokeargument name="r_num2" value="">
					<cfinvokeargument name="checking" value="100">
				</cfinvoke>
				<cfinvoke component="getalt_p" method="Sendmail" returnVariable="rValue">
					<cfinvokeargument name="Msg" value="#session.mailmsg#">
					<cfinvokeargument name="formatid" value="#f_id#">
				</cfinvoke>
			<cfelse>
				<cfset getString = rValue2>
				
				<cfset date1 =DateAdd("n",10*#cnt#,now())>
				<cfschedule action = "update"
				   task = "ryokan_task#cnt#" 
				   operation = "HTTPRequest"
				   url = "#Application.scheduleURL#?batchid=#getString#&formatid=#f_id#&tasknm=ryokan_task#cnt#"
				   startDate = "#year(date1)#/#month(date1)#/#day(date1)#"
				   startTime = "#hour(date1)#:#minute(date1)#"
				   interval = "600"
				   resolveURL = "Yes"
				   publish = "Yes"
				   file = "result.html"
				   path = "#expandpath('result')#"
				   requestTimeOut = "3600">
				<cfset cnt = cnt + 1>
			</cfif>
		<cfelse>
			<cfinvoke component="getalt_p" method="SendmailAndElog" returnVariable="rValue">
					<cfinvokeargument name="r_num" value="">
					<cfinvokeargument name="formatid" value="#f_id#">
					<cfinvokeargument name="ap_id" value="">
					<cfinvokeargument name="r_num2" value="">
					<cfinvokeargument name="checking" value="11">
			</cfinvoke>
			<cfinvoke component="getalt_p" method="Sendmail" returnVariable="rValue">
					<cfinvokeargument name="Msg" value="#session.mailmsg#">
					<cfinvokeargument name="formatid" value="#f_id#">
			</cfinvoke>
		</cfif>
	</cfloop>
</cfloop>


</cfoutput>
</cfprocessingdirective>
