﻿/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	Name			: dayselect
	Description		: 
	Usage			: 
	Attributes		: path:MovieClip			（基準パス）
	Extends			: 
	Method			: 
	Note			: none
	History			: 1) Coded by	17:20 2006/09/19 H.Miyahara（micSystems）
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
import mx.styles.CSSStyleDeclaration;
import mx.controls.Label

class dayselect extends MovieClip{
	var GroundPath:MovieClip	//基準ムービークリップパス
	var GPDepth:Number			//Depth
	var t_date:Date				//指定日付
	var date_text:Object		//日付表示テキストエリア
	var after_btn:MovieClip		//次ボタン
	var before_btn:MovieClip	//前ボタン
	var c_btn:MovieClip			//カレンダー表示ボタン
	var a_day:Array				//曜日配列

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: dayselect
		Description		: Constructor
		Usage			: dayselect( myPath:MovieClip );
		Attributes		: myPath:MovieClip	（基準ＭＣパス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2005/03/16
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function dayselect() {
		GroundPath = _root
		GPDepth = GroundPath.getNextHighestDepth();

		t_date = new Date(); 
		a_day = new Array("日","月","火","水","木","金","土"); 

		//ＭＣ初期化
		initializedData();
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: initializedData
		Description		: 初期設定
		Usage			: initializedData();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function initializedData(){
		//初期日付取得
		//日付設定
		setdate(t_date)
		
		//翌日ボタン処理
		after_btn.onRelease = function(){
			var t_date = new Date(this._parent.t_date.getFullYear(),this._parent.t_date.getMonth(),(this._parent.t_date.getDate()+1))
			this._parent.setdate(t_date)
		}

		//前日ボタン処理
		before_btn.onRelease = function(){
			var t_date = new Date(this._parent.t_date.getFullYear(),this._parent.t_date.getMonth(),(this._parent.t_date.getDate()-1))
			this._parent.setdate(t_date)
		}
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setdate
		Description		: 
		Usage			: setdate();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function setdate(td){
		//日付入れ替え
		t_date = td
		//表示設定
		var wk_datetxt = t_date.getFullYear() + "年" + (t_date.getMonth()+1) + "月" + t_date.getDate() + "日（" + a_day[td.getDay()] +"）"; 
		date_text.text = wk_datetxt
	}
}
