﻿import mx.core.UIComponent

class MultiLineCell extends UIComponent
{

	var mcObj : MovieClip;
	var listOwner : MovieClip;
	var getCellIndex : Function;
	var	getDataLabel : Function;
	var owner;

	function MultiLineCell()
	{
	}

	function createChildren(Void) : Void
	{
		mcObj = createObject("TextArea", "TextArea", 1, {styleName:this, owner:this});
		mcObj.borderStyle = "none"
		mcObj.wordWrap = true
		//mcObj.addEventListener("focusIn", this);
		size();
	}

	function size(Void) : Void
	{
		var index = getCellIndex();
		var mcObj_w = listOwner.getColumnAt(index.columnIndex).width-20;
		mcObj.setSize(mcObj_w, owner.height);
		mcObj._x = 0;
		mcObj._y = 0;
	}

	function setValue(str:String, item:Object, sel:Boolean) : Void
	{	
		//プレビュー用は非表示
		if(listOwner._name == "tmp_dg"){
			mcObj.vScrollPolicy = "off";
			mcObj.hScrollPolicy = "off";
		}

		mcObj._visible = (item!=undefined);
		mcObj.text = str;
		if( getCellIndex().itemIndex%2 == 0 ){
			mcObj.backgroundColor  = 0xFFFFFF
		}else{
			mcObj.backgroundColor  = 0xECEDE4
		}
	}

	function getPreferredHeight(Void) : Number
	{
		return owner.height;
	}

	function getPreferredWidth(Void) : Number
	{
		return owner.width
	}

}
