﻿/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	Name			: SpecsheetEditService
	Description		: スペックシート詳細情報取得
	Usage			: var XXXXXX:SpecsheetEditService = new SpecsheetEditService( Path );
	Attributes		: Path:MovieClip	（基準パス）
	ExtendsParam	: none
	Method			: 
	Note			: none
	History			: 1) Coded by	nf)h.miyahara	2004/07/09
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
import mx.remoting.Service;
import mx.services.Log;
import mx.rpc.RelayResponder;
import mx.remoting.PendingCall;
import mx.remoting.debug.NetDebug;
import mx.remoting.debug.NetDebugConfig;
import mx.rpc.ResultEvent;
import mx.rpc.FaultEvent;
import mx.controls.CheckBox
import mx.transitions.easing.*;
import mx.transitions.Tween;
import commonfunc;

class boh.SpecsheetEditService extends MovieClip {

	var GroundPath:MovieClip	//基準ムービークリップパス
	var GPDepth:Number			//Depth
	var targetIndex:Number		//対象ＩＮＤＥＸ
	var targetData:Object		//対象データプロバイダ
	var ServiceObject:Object	//CFCServiceObject
	var SummaryID:Number		//サマリID
	var QSheetID:Number			//アンケートシートID
	var SheetOwner:String		//アンケートシートオーナーINDEX
	var PaneSource:MovieClip	//ScrollPane内MovieClip
	var cfunc:commonfunc

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: SpecsheetEditService
		Description		: Constructor
		Usage			: SpecsheetEditService( myPath:MovieClip );
		Attributes		: myPath:MovieClip	（基準ＭＣパス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function SpecsheetEditService( myPath:MovieClip ) {
		GroundPath = myPath;
		targetIndex = _global.targetIndex;			//MailTemplateServiceクラスで設定
		targetData = _global.targetData;			//MailTemplateServiceクラスで設定
		GPDepth = GroundPath.getNextHighestDepth();
		SummaryID = targetData.getItemAt(targetIndex).summary_id	//ＩＮＤＥＸよりＩＤを取得
		cfunc = new commonfunc()

		//新規作成時
		if( targetIndex < 0 ){
			SummaryID = -1
			SheetOwner = ""
		}
		var myDebug = mx.remoting.debug.NetDebug.initialize();
		ServiceObject = new Service( "http://" + _global.hostAddress + "/flashservices/gateway/", null , _global.servicePath + ".boh.cfc.specsheet" , null , null ); 
		
		//NetDebbuger設定
		var config:NetDebugConfig = ServiceObject.connection.getDebugConfig();
		config.app_server.trace = true;
		config.client.trace = true;

		//ＭＣ初期化
		initializedData();
	}

	
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getSummaryData
		Description		: サマリ詳細情報取得
		Usage			: getSummaryData( SummaryID );
		Attributes		: SummaryID:Number	（サマリＩＤ）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/12
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function getSummaryData(){

		//ＣＦＣメソッド呼出
		var SmrydtlPC:PendingCall = ServiceObject.getSummaryDetail( SummaryID );
		SmrydtlPC.responder = new RelayResponder( this, "getSummaryDetail_Result", "getSummaryDetail_Fault" );

	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getSummaryDetail_Result
		Description		: サマリ詳細取得成功時処理
		Usage			: getSummaryDetail_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent		（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/12
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getSummaryDetail_Result(ReEvt:ResultEvent):Void{
		var Owner:Object = this;

		if( ReEvt.result.mRecordsAvailable > 0 ){		//編集時

		//	trace( ReEvt.result.mRecordsAvailable + "件取得しました。");
		//取得データセット
			//QSheetID = ReEvt.result.items[0].questionnairesheet_id
			SheetOwner = ReEvt.result.items[0].owner
			PaneSource.EditPanel.summary_nm_txt.text = ReEvt.result.items[0].summary_nm
			PaneSource.EditPanel.summary_ip_txt.text = ReEvt.result.items[0].summary_ip

		//顧客基本情報表示設定
			var objCnt = 1;
			while( PaneSource.EditPanel[ "CB_Item_" + objCnt ] != undefined ){
				PaneSource.EditPanel[ "CB_Item_" + objCnt ].selected = false;
				objCnt++;
			}
			PaneSource.EditPanel.CB_Items.selected = false

			var temp_basicinfo:Array = ReEvt.result.items[0].basicinfo.split(",")
			var temp_basicinfoCnt:Number = 0
			for(var i=0; i< ReEvt.result.items[0].basicinfo.length; i++){
				PaneSource.EditPanel[ "CB_Item_" + temp_basicinfo[i] ].selected = true
			}
			//全選択チェック
			if( objCnt-1 == temp_basicinfo.length){
				PaneSource.EditPanel.CB_Items.selected = true
			}

		//顧客重要情報表示設定
			if( ReEvt.result.items[0].importantinfo.length != 0 ){
				PaneSource.EditPanel.CI_Item.selected = true
			} else {
				PaneSource.EditPanel.CI_Item.selected = false
			}


		//表示件数設定
/*
			PaneSource.EditPanel.viewrecords_cb.selectedIndex = ReEvt.result.items[0].viewrecords-1;
			var tmpList = PaneSource.EditPanel.viewrecords_cb.dropdown;

		//表示順設定

			PaneSource.EditPanel.roominfo_seq_cb.selectedIndex = ReEvt.result.items[0].roominfo_seq-1;
			PaneSource.EditPanel.detailinfo_seq_cb.selectedIndex = ReEvt.result.items[0].detailinfo_seq-1;
			PaneSource.EditPanel.chargeinfo_seq_cb.selectedIndex = ReEvt.result.items[0].chargeinfo_seq-1;
			PaneSource.EditPanel.statisticalinfo_seq_cb.selectedIndex = ReEvt.result.items[0].statisticalinfo_seq-1;
			PaneSource.EditPanel.csinfo_seq_cb.selectedIndex = ReEvt.result.items[0].csinfo_seq-1;

			var tmpList = PaneSource.EditPanel.roominfo_seq_cb.dropdown;
			var tmpList = PaneSource.EditPanel.detailinfo_seq_cb.dropdown;
			var tmpList = PaneSource.EditPanel.chargeinfo_seq_cb.dropdown;
			var tmpList = PaneSource.EditPanel.statisticalinfo_seq_cb.dropdown;
			var tmpList = PaneSource.EditPanel.csinfo_seq_cb.dropdown;
*/

		//満足度推移表示設定
			if( ReEvt.result.items[0].satisfiedinfo.length != 0 ){
				PaneSource.EditPanel.CS_Item.selected = true
			} else {
				PaneSource.EditPanel.CS_Item.selected = false
			}
		//予約基本情報表示設定
			var objCnt = 1;
			while( PaneSource.EditPanel[ "RB_Item_" + objCnt ] != undefined ){
				PaneSource.EditPanel[ "RB_Item_" + objCnt ].selected = false;
				objCnt++;
			}
			PaneSource.EditPanel.RB_Items.selected = false

			var temp_bohinfo:Array = ReEvt.result.items[0].bohinfo.split(",")
			for(var i=0; i< ReEvt.result.items[0].bohinfo.length; i++){
				PaneSource.EditPanel[ "RB_Item_" + temp_bohinfo[i] ].selected = true
			}

			//全選択チェック
			if( objCnt-1 == temp_bohinfo.length){
				PaneSource.EditPanel.RB_Items.selected = true
			}
/*			


		//部屋情報表示設定
			var objCnt = 1;
			while( PaneSource.EditPanel[ "R_Item_" + objCnt ] != undefined ){
				PaneSource.EditPanel[ "R_Item_" + objCnt ].selected = false;
				objCnt++;
			}
			PaneSource.EditPanel.R_Items.selected = false

			var temp_roominfo:Array = ReEvt.result.items[0].roominfo.split(",")
			for(var i=0; i< ReEvt.result.items[0].roominfo.length; i++){
				PaneSource.EditPanel[ "R_Item_" + temp_roominfo[i] ].selected = true
			}

			//全選択チェック
			if( objCnt-1 == temp_roominfo.length){
				PaneSource.EditPanel.R_Items.selected = true
			}

		//明細情報表示設定
			var objCnt = 1;
			while( PaneSource.EditPanel[ "D_Item_" + objCnt ] != undefined ){
				PaneSource.EditPanel[ "D_Item_" + objCnt ].selected = false;
				objCnt++;
			}
			PaneSource.EditPanel.D_Items.selected = false

			var temp_detailinfo:Array = ReEvt.result.items[0].detailinfo.split(",")
			for(var i=0; i< ReEvt.result.items[0].detailinfo.length; i++){
				PaneSource.EditPanel[ "D_Item_" + temp_detailinfo[i] ].selected = true
			}

			//全選択チェック
			if( objCnt-1 == temp_detailinfo.length){
				PaneSource.EditPanel.D_Items.selected = true
			}

		//担当情報表示設定
			var objCnt = 1;
			while( PaneSource.EditPanel[ "C_Item_" + objCnt ] != undefined ){
				PaneSource.EditPanel[ "C_Item_" + objCnt ].selected = false;
				objCnt++;
			}
			PaneSource.EditPanel.C_Items.selected = false

			var temp_chargeinfo:Array = ReEvt.result.items[0].chargeinfo.split(",")
			for(var i=0; i< ReEvt.result.items[0].chargeinfo.length; i++){
				PaneSource.EditPanel[ "C_Item_" + temp_chargeinfo[i] ].selected = true
			}

			//全選択チェック
			if( objCnt-1 == temp_chargeinfo.length){
				PaneSource.EditPanel.C_Items.selected = true
			}

		//統計情報表示設定
			var objCnt = 1;
			while( PaneSource.EditPanel[ "S_Item_" + objCnt ] != undefined ){
				PaneSource.EditPanel[ "S_Item_" + objCnt ].selected = false;
				objCnt++;
			}
			PaneSource.EditPanel.S_Items.selected = false
*/
			var temp_statisticalinfo:Array = ReEvt.result.items[0].statisticalinfo.split(",")
			for(var i=0; i< ReEvt.result.items[0].statisticalinfo.length; i++){
				PaneSource.EditPanel[ "S_Item_" + temp_statisticalinfo[i] ].selected = true
			}

			//全選択チェック
			if( objCnt-1 == temp_statisticalinfo.length){
				PaneSource.EditPanel.S_Items.selected = true
			}

		//ＣＳ情報表示設定
			var objCnt = 1;
			while( PaneSource.EditPanel[ "E_Item_" + objCnt ] != undefined ){
				PaneSource.EditPanel[ "E_Item_" + objCnt ].selected = false;
				objCnt++;
			}

			var temp_csinfo:Array = ReEvt.result.items[0].csinfo.split(",")
			for(var i=0; i< temp_csinfo.length; i++){
				PaneSource.EditPanel[ "E_Item_" + temp_csinfo[i] ].selected = true
			}

		} else {										//新規作成時
			PaneSource.EditPanel.summary_nm_txt.text = ""
			PaneSource.EditPanel.summary_ip_txt.text = ""
			PaneSource.EditPanel.roominfo_seq_cb.selectedIndex = 0;
			PaneSource.EditPanel.detailinfo_seq_cb.selectedIndex = 1;
			PaneSource.EditPanel.chargeinfo_seq_cb.selectedIndex = 2;
			PaneSource.EditPanel.statisticalinfo_seq_cb.selectedIndex = 3;
			PaneSource.EditPanel.csinfo_seq_cb.selectedIndex = 4;

		}
		//必須設定
		var cb_item_reqlist = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16"
		var target_mc = PaneSource.EditPanel
		for (var i = 0; i<cb_item_reqlist.length; i++) {
			target_mc[ "CB_Item_" + i ].selected = true;
			target_mc[ "CB_Item_" + i ].enabled = false;
		}
		//必須設定
		PaneSource.EditPanel.CI_Item.selected = true;
		PaneSource.EditPanel.CI_Item.enabled = false;

		//必須設定
		var rb_item_reqlist = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31"
		var target_mc = PaneSource.EditPanel
		for (var i = 0; i<rb_item_reqlist.length; i++) {
			target_mc[ "RB_Item_" + i ].selected = true;
			target_mc[ "RB_Item_" + i ].enabled = false;
		}

		//必須設定
		PaneSource.EditPanel.R_Item_1.selected = true;
		PaneSource.EditPanel.R_Item_1.enabled = false;
		PaneSource.EditPanel.R_Item_2.selected = true;
		PaneSource.EditPanel.R_Item_2.enabled = false;
		PaneSource.EditPanel.R_Item_3.selected = true;
		PaneSource.EditPanel.R_Item_3.enabled = false;
		//必須設定
		PaneSource.EditPanel.S_Item_2.selected = true;
		PaneSource.EditPanel.S_Item_2.enabled = false;
		PaneSource.EditPanel.S_Item_3.selected = true;
		PaneSource.EditPanel.S_Item_3.enabled = false;
		PaneSource.EditPanel.S_Item_4.selected = true;
		PaneSource.EditPanel.S_Item_4.enabled = false;

		//全選択イベント
		var basicinfoListener:Object = new Object()
		basicinfoListener.click = function(eventObj){
			var objCnt = 1;
			var inactiveList = cb_item_reqlist
			while( Owner.PaneSource.EditPanel[ "CB_Item_" + objCnt ] != undefined ){
				if( inactiveList.indexOf(objCnt) == -1){
					Owner.PaneSource.EditPanel[ "CB_Item_" + objCnt ].selected = eventObj.target.selected;
				}
				objCnt++;
			}
		}
		PaneSource.EditPanel.CB_Items.addEventListener("click", basicinfoListener);

		var bohinfoListener:Object = new Object()
		bohinfoListener.click = function(eventObj){
			var objCnt = 1;
			var inactiveList = rb_item_reqlist
			while(Owner.PaneSource.EditPanel[ "RB_Item_" + objCnt ] != undefined ){
				if( inactiveList.indexOf(objCnt) == -1){
					Owner.PaneSource.EditPanel[ "RB_Item_" + objCnt ].selected = eventObj.target.selected;
				}
				objCnt++;
			}
		}
		PaneSource.EditPanel.RB_Items.addEventListener("click", bohinfoListener);

		var roominfoListener:Object = new Object()
		roominfoListener.click = function(eventObj){
			var objCnt = 1;
			var inactiveList = "1,2,3"
			while(Owner.PaneSource.EditPanel[ "R_Item_" + objCnt ] != undefined ){
				if( inactiveList.indexOf(objCnt) == -1){
					Owner.PaneSource.EditPanel[ "R_Item_" + objCnt ].selected = eventObj.target.selected;
				}
				objCnt++;
			}
		}
		PaneSource.EditPanel.R_Items.addEventListener("click", roominfoListener);
		
		var detailinfoListener:Object = new Object()
		detailinfoListener.click = function(eventObj){
			var objCnt = 1;
			while(Owner.PaneSource.EditPanel[ "D_Item_" + objCnt ] != undefined ){
				Owner.PaneSource.EditPanel[ "D_Item_" + objCnt ].selected = eventObj.target.selected;
				objCnt++;
			}
		}
		PaneSource.EditPanel.D_Items.addEventListener("click", detailinfoListener);

		var chargeinfoListener:Object = new Object()
		chargeinfoListener.click = function(eventObj){
			var objCnt = 1;
			while(Owner.PaneSource.EditPanel[ "C_Item_" + objCnt ] != undefined ){
				Owner.PaneSource.EditPanel[ "C_Item_" + objCnt ].selected = eventObj.target.selected;
				objCnt++;
			}
		}
		PaneSource.EditPanel.C_Items.addEventListener("click", chargeinfoListener);

		var statisticalinfoListener:Object = new Object()
		statisticalinfoListener.click = function(eventObj){
			var objCnt = 1;
			var inactiveList = "2,3,4"
			while(Owner.PaneSource.EditPanel[ "S_Item_" + objCnt ] != undefined ){
				if( inactiveList.indexOf(objCnt) == -1){
					Owner.PaneSource.EditPanel[ "S_Item_" + objCnt ].selected = eventObj.target.selected;
				}
				objCnt++;
			}
		}
		PaneSource.EditPanel.S_Items.addEventListener("click", statisticalinfoListener);

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getSummaryDetail_Fault
		Description		: サマリ取得失敗時処理
		Usage			: getSummaryDetail_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent	（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/12
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getSummaryDetail_Fault(FltEvt:FaultEvent):Void{	//trace("Categories Fault: " + FltEvt.fault.description);
		cfunc.ShowConfirm( GroundPath , "サマリ情報を取得できませんでした。\rエラーメッセージ："+ FltEvt.fault.description, this , null );
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: RegistTargetData
		Description		: サマリ登録
		Usage			: RegistTargetData( Obj );
		Attributes		: Obj : （コンファームを呼出した呼出元クラスインスタンス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function RegistTargetData( Obj ){
		//インスタンス入替
		this = Obj

		//ＣＦＣ引数オブジェクト作成
		var arguments1:Number =  SummaryID;
		var arguments2:String =  PaneSource.EditPanel.summary_nm_txt.text;
		var arguments3:String =  PaneSource.EditPanel.summary_ip_txt.text;

		//顧客基本情報
		var arguments4:String =  new String("");
		var CB_Cnt = 1;
		while(PaneSource.EditPanel[ "CB_Item_" + CB_Cnt ] != undefined ){
			if( PaneSource.EditPanel[ "CB_Item_" + CB_Cnt ].selected ){
				arguments4 = arguments4 + CB_Cnt + ","
			}
			CB_Cnt++;
		}
		arguments4 = arguments4.substring(0,arguments4.length-1)

		//顧客重要情報
		var arguments5 =  "";
		if( PaneSource.EditPanel.CI_Item.selected == true ){
			arguments5 = 1
		}

		//満足度推移
		var arguments6 = "";
		if( PaneSource.EditPanel.CS_Item.selected == true ){
			arguments6 = 1
		}

		//予約基本情報
		var arguments7:String = new String("");
		var RB_Cnt = 1;
		while(PaneSource.EditPanel[ "RB_Item_" + RB_Cnt ] != undefined ){
			if( PaneSource.EditPanel[ "RB_Item_" + RB_Cnt ].selected ){
				arguments7 = arguments7 + RB_Cnt + ","
			}
			RB_Cnt++;
		}
		arguments7 = arguments7.substring(0,arguments7.length-1)

		//CS情報
		var arguments8:String = new String("");
		var E_Cnt = 1;
		while(PaneSource.EditPanel[ "E_Item_" + E_Cnt ] != undefined ){
			if( PaneSource.EditPanel[ "E_Item_" + E_Cnt ].selected ){
				arguments8 = arguments8 + E_Cnt + ","
			}
			E_Cnt++;
		}
		arguments8 = arguments8.substring(0,arguments8.length-1)

		//CS情報
		var arguments9:Array = new Array();
		var Q_Cnt = 0;
		while(PaneSource[ "Question_" + Q_Cnt ] != undefined ){
			arguments9.push({id:PaneSource[ "Question_" + Q_Cnt ].questionnaire_id,seq:PaneSource[ "Question_" + Q_Cnt ].viewsequence,flg:0,scenario:PaneSource[ "Question_" + Q_Cnt ].scenario})
			if( PaneSource[ "Question_" + Q_Cnt ].Q_Item.selected ){
				arguments9[Q_Cnt].flg = 1;
			}
			Q_Cnt++;
		}
		var arguments10:String = SheetOwner;

		var conditionParam = new Object({
				summary_id:arguments1,
				summary_nm:arguments2,
				summary_ip:arguments3,
				basicinfo:arguments4,
				importantinfo:arguments5,
				satisfiedinfo:arguments6,
				bohinfo:arguments7,
				csinfo:arguments8,
				questionnaireData:arguments9,
				owner:arguments10
		});
/**/

		//CFCメソッド呼出
		var SmrydtlPC:PendingCall = ServiceObject.setSummary(conditionParam);
		SmrydtlPC.responder = new RelayResponder( this, "setSummary_Result", "setSummary_Fault" );

	}
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setSummary_Result
		Description		: サマリ登録成功時イベント
		Usage			: setSummary_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent		（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function setSummary_Result(ReEvt:ResultEvent):Void{
		//サマリ一覧画面読込み
		//var LoadClip:LoadImage = new LoadImage( "mailTemplate.swf", GroundPath )
		if(ReEvt.result.errorFlg == 1){
			cfunc.ShowConfirm( GroundPath ,ReEvt.result.errorMsg , this , null );
		} else {
			GroundPath.gotoAndStop("list")
		}
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setSummary_Fault
		Description		: サマリ登録失敗時イベント
		Usage			: setSummary_Fault(ReEvt:ResultEvent);
		Attributes		: FltEvt:FaultEvent	（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function setSummary_Fault(FltEvt:FaultEvent):Void{	//trace("setSummary_Fault: " + FltEvt.fault.description);
		cfunc.ShowConfirm( GroundPath , "サマリ登録が正常に行われませんでした。\rエラーメッセージ："+ FltEvt.fault.description, this , null );
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: initializedData
		Description		: 初期設定
		Usage			: initializedData();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function initializedData(){
		var Owner = this;
		//test
		//var myConfirm:ConfirmWindow = new ConfirmWindow( GroundPath , "Your IP：\r"+ _global.UserInfo.ipAddress, this , null );

		//ローディング用ＭＣ
		GroundPath.createEmptyMovieClip("CoveredStage_mc", GPDepth);
		GPDepth++;
		GroundPath.CoveredStage_mc.beginFill(0xCCCCCC);
		GroundPath.CoveredStage_mc.moveTo(0,0);
		GroundPath.CoveredStage_mc.lineTo(Stage.width, 0);
		GroundPath.CoveredStage_mc.lineTo(Stage.width, Stage.height);
		GroundPath.CoveredStage_mc.lineTo(0, Stage.height);
		GroundPath.CoveredStage_mc.lineTo(0, 0);
		GroundPath.CoveredStage_mc.endFill();
		GroundPath.CoveredStage_mc._alpha = 40;
		GroundPath.CoveredStage_mc.enabled = false
		GroundPath.CoveredStage_mc.onRollOver = false
		GroundPath.CoveredStage_mc.attachMovie("Loading","Loading",GPDepth++,{_x:Stage.width*0.5,_y:Stage.height*0.5})

		//ScrollPane設定
		GroundPath.specsheet_sp.borderStyle = "none"
		GroundPath.specsheet_sp.vPageScrollSize = GroundPath.specsheet_sp.height;
		GroundPath.specsheet_sp.vLineScrollSize = GroundPath.specsheet_sp.height;

		//ScrollPane内Content設定
		GroundPath.specsheet_sp.contentPath = "SpecsheetContent"
		PaneSource = GroundPath.specsheet_sp.content

		//ペイン内初期化
		GroundPath.specsheet_sp.refreshPane();

		PaneSource.attachMovie("EditSSPanel","EditPanel",GPDepth);
		GPDepth++;

		//メニュー切替
		GroundPath.changeBtn.onRelease = function(){
			Owner.SetMenuList(Owner.GroundPath)
		}
		
		//登録ボタン設定
		GroundPath.registBtn.onRelease = function(){
			//var myConfirm:ConfirmWindow = new ConfirmWindow( this._parent , "現在の内容でテンプレートを登録します。よろしいですか？", this.Owner , this.Owner.RegistTargetData,0 );
			Owner.cfunc.ShowConfirm( this._parent , "現在の内容でサマリを登録します。よろしいですか？", Owner , Owner.RegistTargetData );
		}

		//詳細情報取得
		getSummaryData();

		//アンケートマスタデータ取得
		//var conditionParam = new Object({summary_id:SummaryID})
		var enquetePC:PendingCall = ServiceObject.getEnquete();
		enquetePC.responder = new RelayResponder( this, "getEnqueteMaster_Result", "getEnqueteMaster_Fault" );

	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getEnqueteMaster_Result
		Description		: アンケートマスタ情報取得成功時処理
		Usage			: getEnqueteMaster_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent		（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/08
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getEnqueteMaster_Result(ReEvt:ResultEvent):Void{	trace( ReEvt.result.mRecordsAvailable + "件取得しました。");
		var Owner:Object = this;
		var Q_startX:Number = PaneSource.EditPanel.CSStartItem._x
		var Ymargin:Number = 20
		var startY:Number = PaneSource.EditPanel.CSStartItem._y + PaneSource.EditPanel.CSStartItem._height
		var Categorygroup:Number = 0
		var QCnt:Number = 0
		var RecCnt:Number = ReEvt.result.mRecordsAvailable

		//全選択イベント
		var CategoryListner:Object = new Object()
		CategoryListner.click = function(eventObj){
			for( var i=0; i<RecCnt; i++ ){
				if( Owner.PaneSource[ "Question_" + i ].Categorygroup == eventObj.target.Categorygroup && Owner.PaneSource[ "Question_" + i ].viewtype == 1){
					Owner.PaneSource[ "Question_" + i ].Q_Item.selected = eventObj.target.selected;
				}
			}
		}


		//ＣＳ設問ＭＣ設定
		for( var i=0; i<ReEvt.result.mRecordsAvailable; i++ ){

			//カテゴリパネル
			if(ReEvt.result.items[i-1].questionnairecategory_id != ReEvt.result.items[i].questionnairecategory_id){
				Categorygroup++;
				PaneSource.attachMovie("Q_Item_1", "Category_" + Categorygroup , GPDepth, {_y:startY, _x:Q_startX })
				GPDepth++;
				PaneSource[ "Category_" + Categorygroup ].data_txt.text =  ReEvt.result.items[i].questionnairecategory_nm;
				startY = startY + PaneSource[ "Category_" + Categorygroup ]._height

				PaneSource[ "Category_" + Categorygroup ].createClassObject(CheckBox, "CG_Item", GPDepth, {_x:+5,_y:+5 , selected:true })
				PaneSource[ "Category_" + Categorygroup ].CG_Item.Categorygroup = Categorygroup;
				GPDepth++;

				PaneSource[ "Category_" + Categorygroup ].CG_Item.addEventListener("click", CategoryListner);
			}

			//設問パネル
			PaneSource.attachMovie("Q_Item_0", "Question_" + i , GPDepth, {_y:startY, _x:Q_startX })
			GPDepth++;
			PaneSource[ "Question_" + i ].data_txt.text =  ReEvt.result.items[i].questionnaire_nm;
			PaneSource[ "Question_" + i ].questionnaire_id =  ReEvt.result.items[i].questionnaire_id;
			PaneSource[ "Question_" + i ].viewsequence =  ReEvt.result.items[i].viewsequence;
			PaneSource[ "Question_" + i ].viewtype =  ReEvt.result.items[i].viewtype;
			PaneSource[ "Question_" + i ].scenario =  ReEvt.result.items[i].scenario;
			PaneSource[ "Question_" + i ].Categorygroup =  Categorygroup;
			startY = startY + PaneSource[ "Question_" + i ]._height
			//trace("ID: " + PaneSource[ "Question_" + i ].questionnaire_id + " Name: " + PaneSource[ "Question_" + i ].data_txt.text )
			PaneSource[ "Question_" + i ].createClassObject(CheckBox, "Q_Item", GPDepth, {_x:+5,_y:+5 , selected:true })
			GPDepth++;
			//必須設定
			if( ReEvt.result.items[i].viewtype == 0){
				PaneSource[ "Question_" + i ].Q_Item.enabled = false;
				PaneSource[ "Question_" + i ].Q_Item.selected = true;
			}
	

		}
		GroundPath.specsheet_sp.setSize(GroundPath.specsheet_sp.width,GroundPath.specsheet_sp.height)

		//アンケート情報取得ＣＦＣメソッド呼出
		var enquetePC:PendingCall = ServiceObject.getEnqueteCheck(SummaryID);
		//var enquetePC:PendingCall = ServiceObject.getEnqueteCheck(QSheetID);
		enquetePC.responder = new RelayResponder( this, "getEnquete_Result", "getEnquete_Fault" );


	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getEnqueteMaster_Fault
		Description		: アンケートマスタ情報取得失敗時処理
		Usage			: getEnqueteMaster_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent	（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/06
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getEnqueteMaster_Fault(FltEvt:FaultEvent):Void{	//trace("getEnquete_Fault: " + FltEvt.fault.description);
		cfunc.ShowConfirm( GroundPath , "アンケートマスタ情報を取得できませんでした。\rエラーメッセージ："+ FltEvt.fault.description, this , null );
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getEnquete_Result
		Description		: アンケートマスタ情報取得成功時処理
		Usage			: getEnquete_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent		（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/08
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getEnquete_Result(ReEvt:ResultEvent):Void{	//trace( ReEvt.result.mRecordsAvailable + "件取得しました。");
		for( var i=0; i<ReEvt.result.mRecordsAvailable; i++){
			if( ReEvt.result.items[i].active_flg == 1 ){
				PaneSource[ "Question_" + i ].Q_Item.selected = true;
			} else {
				PaneSource[ "Question_" + i ].Q_Item.selected = false;
			}
			if(PaneSource[ "Question_" + i ].viewtype != 1){
				PaneSource[ "Question_" + i ].Q_Item.selected = true;
			}

		}
		//ローディング用ＭＣ削除処理
		var delmc = GroundPath.CoveredStage_mc
		delmc.Loading.gotoAndPlay(2)
		var Deltween:Object = new Tween(delmc, "_alpha", Strong.easeIn, delmc._alpha, 0, 2, true);
		Deltween.delmc = delmc
		Deltween.onMotionFinished = function() {
			this.delmc.removeMovieClip();
		};
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getEnquete_Fault
		Description		: アンケートマスタ情報取得失敗時処理
		Usage			: getEnquete_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent	（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/06
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getEnquete_Fault(FltEvt:FaultEvent):Void{	//trace("getEnquete_Fault: " + FltEvt.fault.description);
		cfunc.ShowConfirm( GroundPath , "アンケート情報を取得できませんでした。\rエラーメッセージ："+ FltEvt.fault.description, this , null );

	}

}