﻿/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	Name			: SpecsheetListService
	Description		: スペックシート一覧情報取得
	Usage			: var XXXXXX:SpecsheetListService = new SpecsheetListService( datagridname, path );
	Attributes		: datagridname:DataGrid		（結果格納用データグリッドコンポーネント名）
					: path:MovieClip			（基準パス）
	ExtendsParam	: none
	Method			: initializedData 			初期設定
					: getSummaryTemplate 			メールテンプレートリスト取得
					: getSummaryTemplate_Result 	メールテンプレートリスト取得正常終了時イベント
					: getSummaryTemplate_Fault 	メールテンプレートリスト取得不正終了時イベント
					: SetTargetData 			編集対象取得～編集画面表示
					: DeleteTargetData 			テンプレート削除
					: deleteMailTemplate_Result	テンプレート削除正常終了時イベント
					: deleteMailTemplate_Fault	テンプレート削除不正終了時イベント
	Note			: none
	History			: 1) Coded by	nf)h.miyahara	2004/07/09
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
import mx.remoting.Service;
import mx.services.Log;
import mx.rpc.RelayResponder;
import mx.remoting.PendingCall;
import mx.remoting.debug.NetDebug;
import mx.remoting.debug.NetDebugConfig;
import mx.remoting.RecordSet;
import mx.rpc.ResultEvent;
import mx.rpc.FaultEvent;
import mx.controls.gridclasses.DataGridColumn
import mx.controls.DataGrid
import mx.transitions.easing.*;
import mx.transitions.Tween;
import commonfunc;

class boh.SpecsheetListService extends MovieClip {

	var targetGrid:DataGrid				//結果表示ViewComponent
	var GroundPath:MovieClip			//基準ムービークリップパス
	var GPDepth:Number					//Depth
	var ServiceObject:Object			//CFCServiceObject
	var outputPath:String				//CSVOutputPath
	var DataSourceArr:Array				//データソース事前指定用配列
	var cfunc:commonfunc

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: SpecsheetListService
		Description		: Constructor
		Usage			: SpecsheetListService( myGrid:DataGrid, myPath:MovieClip );
		Attributes		: myGrid:DataGrid	（結果格納用データグリッドコンポーネント名）
						: myPath:MovieClip	（基準ＭＣパス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function SpecsheetListService( myGrid:DataGrid, myPath:MovieClip ) {
		targetGrid = myGrid;
		GroundPath = myPath;
		GPDepth = GroundPath.getNextHighestDepth();
		cfunc = new commonfunc()

		var myDebug = mx.remoting.debug.NetDebug.initialize();
		ServiceObject = new Service( "http://" + _global.hostAddress + "/flashservices/gateway/", null , _global.servicePath + ".boh.cfc.specsheet" , null , null ); 
		
		//NetDebbuger設定
		var config:NetDebugConfig = ServiceObject.connection.getDebugConfig();
		config.app_server.trace = true;
		config.client.trace = true;
		
		//ＭＣ初期化
		initializedData();

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getSummaryTemplate
		Description		: メールテンプレートリスト取得
		Usage			: getSummaryTemplate();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function getSummaryTemplate(){

		//CFCメソッド呼出
		var MailtmpPC:PendingCall = ServiceObject.getSummaryList();
		MailtmpPC.responder = new RelayResponder( this, "getSummaryList_Result", "getSummaryList_Fault" );

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getSummaryList_Result
		Description		: サマリリスト取得成功時処理
		Usage			: getSummaryList_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent		（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getSummaryList_Result(ReEvt:ResultEvent):Void{
		var Owner = this;

		var RecSet:RecordSet = RecordSet(ReEvt.result);
		targetGrid.dataProvider = RecSet

		//ローディング用ＭＣ削除処理
		GroundPath._x = 0
		cfunc.loadingdelete(GroundPath._parent)
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getSummaryList_Fault
		Description		: サマリリスト取得失敗時処理
		Usage			: getSummaryList_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent	（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getSummaryList_Fault(FltEvt:FaultEvent):Void{	//trace("getSummaryList_Fault: " + FltEvt.fault.description);
		//処理を記述
		cfunc.ShowConfirm( GroundPath , "サマリリストを取得できませんでした。\rエラーメッセージ："+ FltEvt.fault.description, this , null );
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: initializedData
		Description		: 初期設定
		Usage			: initializedData();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
						: 1) Updated by	nf)h.miyahara	2005/01/11	ソート項目追加
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function initializedData(){
		var Owner = this

		//カラム設定
		var col:DataGridColumn = new DataGridColumn("Selected");
 		col.headerText = "";
 		col.width = 30;
		col.cellRenderer = "CheckCellRenderer";
		col.sortOnHeaderRelease = false;
		targetGrid.addColumn(col);

		col = new DataGridColumn("summary_nm");
 		col.headerText = "サマリ名";
 		col.width = 200;
		col.sortOnHeaderRelease = true;
		targetGrid.addColumn(col);

		col = new DataGridColumn("summary_ip");
 		col.headerText = "管理対象ＩＰ";
 		col.width = 120;
		col.sortOnHeaderRelease = true;
		targetGrid.addColumn(col)

		col = new DataGridColumn("summary_id");
 		col.headerText = "詳細";
		col.cellRenderer = "SDetailCellRenderer";
		col.sortOnHeaderRelease = false;
		targetGrid.addColumn(col)
		
		//スタイル設定
		cfunc.bohGridStyle(targetGrid);
		targetGrid.setVScrollPolicy("auto");

		//サマリリスト取得
		getSummaryTemplate();

		//メニュー切替
		GroundPath.changeBtn.onRelease = function(){
			Owner.SetMenuList(Owner.GroundPath)
		}

		//新規作成ボタン設定
		GroundPath.createBtn.onRelease = function(){
			Owner.SetTargetData(-1);
			//var LoadClip:LoadImage = new LoadImage( "defaultSearch.swf",this._parent )
		}
		

		//削除ボタン設定
		GroundPath.deleteBtn.onRelease = function(){
			var LenCheck:Boolean = false;
			for(var i=0; i< Owner.targetGrid.length; i++){
				if(Owner.targetGrid.getItemAt(i).Selected){
					LenCheck = true
				}
			}

			//チェックされている時のみ
			if( LenCheck ){
				Owner.cfunc.ShowConfirm( this._parent , "選択したサマリを削除します。よろしいですか？", Owner , Owner.DeleteTargetData );
			} else {
				Owner.cfunc.ShowConfirm( this._parent , "削除したいテンプレートをチェックして下さい。", Owner , null );
			}

		}

	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: SetTargetData
		Description		: 編集対象取得～編集画面表示
		Usage			: SetTargetData(index);
		Attributes		: index：	（編集対象ＩＮＤＥＸ）
		Note			: 編集用CellRendererよりコールされる。
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function SetTargetData(index){
		
		//グローバル変数に編集対象ＩＮＤＥＸ・データプロバイダを退避
		_global.targetIndex = index;
		_global.targetData = targetGrid.dataProvider;

		//trace("_global.targetIndex: " + _global.targetIndex)
		//編集画面読込み
		//var LoadClip:LoadImage = new LoadImage( "mailEdit.swf", GroundPath )
		GroundPath.gotoAndStop("detail")
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: DeleteTargetData
		Description		: 削除対象取得表示
		Usage			: DeleteTargetData( Obj );
		Attributes		: Obj : （コンファームを呼出した呼出元クラスインスタンス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function DeleteTargetData( Obj ){
		//インスタンス入替
		this = Obj
		var retArray:Array = new Array();
		for(var i=0; i< targetGrid.length; i++){
			if(targetGrid.getItemAt(i).Selected){
				retArray.push(targetGrid.getItemAt(i).summary_id)
				trace(targetGrid.getItemAt(i).summary_id)
			}
		}

		//CFCメソッド呼出
		var MailtmpPC:PendingCall = Obj.ServiceObject.deleteSummary( retArray );
		MailtmpPC.responder = new RelayResponder( this, "deleteSummary_Result", "deleteSummary__Fault" );
	}
	

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: deleteSummary_Result
		Description		: 削除正常終了時
		Usage			: deleteSummary_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function deleteSummary_Result(ReEvt:ResultEvent):Void{
		targetGrid.removeAllColumns();
		initializedData();
		trace("削除完了")
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: deleteSummary__Fault
		Description		: 削除不正終了時
		Usage			: deleteSummary__Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function deleteSummary__Fault(FltEvt:FaultEvent):Void{	//trace("Categories Fault: " + FltEvt.fault.description);
		//処理を記述
		cfunc.ShowConfirm( GroundPath , "削除が正常に行われませんでした。\rエラーメッセージ："+ FltEvt.fault.description, this , null );
	}

}
