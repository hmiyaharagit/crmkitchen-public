﻿/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	Name			: customerEdit
	Description		: スペックシート表示画面
	Usage			: var XXXXXX:customerEdit = new customerEdit( path );
	Attributes		: path:MovieClip			（基準パス）
	Extends			: CommonFunction(Class)
	Method			: initializedData		初期設定
					: getCRMGuestList_Result	検索実行正常終了時イベント
					: getCRMGuestList_Fault	検索実行不正終了時イベント
	Note			: none
	History			: 1) Coded by	nf)h.miyahara	2004/07/09
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
import mx.remoting.Service;
import mx.services.Log;
import mx.rpc.RelayResponder;
import mx.remoting.PendingCall;
import mx.remoting.debug.NetDebug;
import mx.remoting.debug.NetDebugConfig;
import mx.remoting.RecordSet;
import mx.rpc.ResultEvent;
import mx.rpc.FaultEvent;
import mx.controls.ComboBox
import mx.controls.TextArea
import mx.data.binding.*;
import mx.data.components.DataSet
import mx.controls.Label

import ValueInformation;

class customerEdit extends CommonFunction {

	var GroundPath:MovieClip	//基準ムービークリップパス
	var GPDepth:Number			//Depth
	var ServiceObject:Object	//CFCServiceObject
	var PaneSource:MovieClip	//ScrollPane内MovieClip
	var	targetIndex:Number		//初期表示顧客情報ＩＮＤＥＸ
	var	SummaryIndex:Number		//表示サマリＩＮＤＥＸ
	var	targetData:Object		//表示対象データ
	var customer_code:Number	//customer_code
	var SuumaryData:DataSet		//サマリデータ退避用Dataset
	var EnqueteData:DataSet		//アンケートデータ退避用Dataset
	var DetailAreaHeight:Number	//詳細情報表示時の表示エリア高
	var CallSequence:Array		//表示順（ファンクション呼出順）
	var OpenRecord:Number		//エキスパンド開始ＭＣ
	var SearchResult:DataSet	//検索結果退避用オブジェクト
	var DataSourceArr:Array		//データソース事前指定用配列
	var tmp_startY:Number		
	var tmp_tgtID:Number		
	var tmp_mode:Number		
	var tmp_seq:Number		
	
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: customerEdit
		Description		: Constructor
		Usage			: customerEdit( myPath:MovieClip );
		Attributes		: myGrid:DataGrid	（結果格納用データグリッドコンポーネント名）
						: myPath:MovieClip	（基準ＭＣパス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function customerEdit( myPath:MovieClip  ) {
		GroundPath = myPath;
		GPDepth = GroundPath.getNextHighestDepth();
		targetIndex = _global.targetIndex	//前画面にて格納
		targetData = _global.targetData		//前画面にて格納
		customer_code = targetData.getItemAt(targetIndex).customer_code

		//本画面ボタン制御用カバーＭＣ
		GroundPath._parent.createEmptyMovieClip("EnabledScreen", GPDepth);
		GPDepth++;
		GroundPath._parent.EnabledScreen.beginFill(0xFFFFFF);
		GroundPath._parent.EnabledScreen.moveTo(0,0);
		GroundPath._parent.EnabledScreen.lineTo(Stage.width, 0);
		GroundPath._parent.EnabledScreen.lineTo(Stage.width, Stage.height);
		GroundPath._parent.EnabledScreen.lineTo(0, Stage.height);
		GroundPath._parent.EnabledScreen.lineTo(0, 0);
		GroundPath._parent.EnabledScreen._alpha = 0;
		GroundPath._parent.EnabledScreen.enabled = false
		GroundPath._parent.EnabledScreen.onRollOver = function(){}

		DetailAreaHeight = 0;

		var myDebug = mx.remoting.debug.NetDebug.initialize();
		ServiceObject = new Service( "http://" + _global.hostAddress + "/flashservices/gateway/", null , _global.servicePath + ".customeredit" , null , null ); 
		
		//NetDebbuger設定
		var config:NetDebugConfig = ServiceObject.connection.getDebugConfig();
		config.app_server.trace = true;
		config.client.trace = true;

		initializedData();
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: initializedData
		Description		: 初期設定
		Usage			: initializedData();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function initializedData(){
		var Owner = this

		//予約パネル非表示
		GroundPath.RB_Panel._visible = false;


		//予約パネル表示制御
		var CBObj = new Object();
		CBObj.click = function(eventObj){
			Owner.GroundPath.RB_Panel._visible = eventObj.target.selected;
			Owner.GroundPath.RB_Panel.enabled = eventObj.target.selected;
		}
		GroundPath.RBPanel_cb.addEventListener("click", CBObj);

		//日付入力初期設定
		var today_date:Date = new Date();
		var month = today_date.getMonth()+1;
		var day = today_date.getDate();
		var nextday = today_date.getDate()+1;
		if( month < 10 ){ month = "0" + month }
		if( day < 10 ){ day = "0" + day }
		var today:String = (today_date.getFullYear() + "/" + month + "/" + day);
		if( nextday < 10 ){ nextday = "0" + nextday }
		var nexttoday:String = (today_date.getFullYear() + "/" + month + "/" + nextday);
		GroundPath.RB_Panel.RB_Item_2.data_txt.text = today
		GroundPath.RB_Panel.RB_Item_3.data_txt.text = nexttoday

		//日付入力補助設定
		GroundPath.RB_Panel.RB_Item_2.aDate_mc.onRelease = function(){
			var targetTxt = new Array(Owner.GroundPath.RB_Panel.RB_Item_2.data_txt)
			var aDateChoose = new DateChoose( Owner.GroundPath.RB_Panel.RB_Item_1, targetTxt);
		}
		GroundPath.RB_Panel.RB_Item_3.dDate_mc.onRelease = function(){
			var targetTxt = new Array(Owner.GroundPath.RB_Panel.RB_Item_3.data_txt)
			var dDateChoose = new DateChoose( Owner.GroundPath.RB_Panel.RB_Item_1, targetTxt,Owner.GroundPath.RB_Panel.RB_Item_2.data_txt.text);
		}

		var Owner = this

		//閉じるボタン
		GroundPath.ss_closeBtn.onRelease = function(){
			Owner.GroundPath._parent.EnabledScreen.removeMovieClip();
			Owner.GroundPath.removeMovieClip();
		}

		//登録ボタン
		GroundPath.editBtn.onRelease = function() {
			var OKFunction = Owner.setCustomerData
			Owner.ShowConfirm( Owner.GroundPath , "顧客情報を登録します。よろしいですか？", Owner , OKFunction ,0 );
		}

		//削除ボタン
		GroundPath.deleteBtn.onRelease = function() {
			var OKFunction = Owner.delCustomerData
			Owner.ShowConfirm( Owner.GroundPath , "顧客情報を削除します。よろしいですか？", Owner , OKFunction ,0 );
		}

		//タブ設定
		TabInitialize();


		//ローディング用ＭＣ削除処理
		GroundPath.onEnterFrame = function(){
			var addSpeed = 0.7
			this._parent.Loading.removeMovieClip();
			this._parent.CoveredStage_mc._height = this._parent.CoveredStage_mc._height*addSpeed; 
			if(this._parent.CoveredStage_mc._height<0){
				this._parent.CoveredStage_mc.removeMovieClip();
				delete this.onEnterFrame
			}
		}
		//コンボボックススタイル設定
		GroundPath.area_cb.setStyle("fontSize", 11);
		GroundPath.area_cb.setStyle("color", 0x666666);
		GroundPath.area_cb.dropdown;
		GroundPath.gender_cb.setStyle("fontSize", 11);
		GroundPath.gender_cb.setStyle("color", 0x666666);
		GroundPath.gender_cb.dropdown;

		var CustmrdtlPC:PendingCall = ServiceObject.getMaster();
		CustmrdtlPC.responder = new RelayResponder( this, "getMaster_Result", "Service_Fault" );
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setCustomerData
		Description		: 顧客情報の登録
		Usage			: setCustomerData(Obj);
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/12/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function setCustomerData(Obj){
		//インスタンス入替
		this = Obj
/**/
		var arguments1 =  customer_code;
		var arguments2 =  GroundPath.data2_txt.text;
		var arguments3 =  GroundPath.data3_txt.text;
		var arguments4 =  GroundPath.gender_cb.selectedItem.data;
		var arguments5 =  GroundPath.data5_txt.text;
		var arguments6 =  GroundPath.area_cb.selectedItem.data;
		var arguments7 =  GroundPath.data8_txt.text;
		var arguments8 =  GroundPath.data9_txt.text;
		var arguments9 =  GroundPath.data10_txt.text;
		var arguments10 =  GroundPath.data15_txt.text;
		var arguments11 =  GroundPath.data16_txt.text;
		var arguments12 =  GroundPath.data17_txt.text;
		var arguments13 =  GroundPath.data18_txt.text;
		var arguments14 =  GroundPath.commentArea.text;
		var arguments15 =  GroundPath.RB_Panel.RB_Item_2.data_txt.text;
		var arguments16 =  GroundPath.RB_Panel.RB_Item_3.data_txt.text;
		var arguments17 =  GroundPath.RB_Panel.cancel_rb.selected;
		var arguments18 =  GroundPath.RB_Panel.edit_rb.selected;
		//var arguments19 =  GroundPath.RB_Panel.RB_Item_1.reserve_code;
		var arguments19 =  GroundPath.RB_Panel.RB_Item_1.data_txt.text;
		var arguments20 =  GroundPath.RBPanel_cb.selected;

		var conditionParam = new Object({
			customer_code:arguments1,
			customer_nm:arguments2,
			customer_kana:arguments3,
			gender:arguments4,
			birthday:arguments5,
			area:arguments6,
			zipcode:arguments7,
			address1:arguments8,
			address2:arguments9,
			tel1:arguments10,
			email1:arguments11,
			tel2:arguments12,
			email2:arguments13,
			information:arguments14,
			arrive_date:arguments15,
			depart_date:arguments16,
			cancel_flg:arguments17,
			update_flg:arguments18,
			reserve_code:arguments19,
			reg_flg:arguments20
		});

		//検索条件確認
		var checkVal = CheckconditionParam(conditionParam)
		if(checkVal.Compelete){
			SearchWait( GroundPath, true );
			//CFCメソッド呼出
			var VIdtlPC:PendingCall = ServiceObject.setCustomerData(conditionParam);
			VIdtlPC.responder = new RelayResponder( this, "setCustomerData_Result", "Service_Fault" );
		} else {
			//アラート表示
			ShowConfirm( GroundPath , checkVal.message, this, null );
		}

	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: delCustomerData
		Description		: 顧客情報の登録
		Usage			: delCustomerData(Obj);
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/12/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function delCustomerData(Obj){
		//インスタンス入替
		this = Obj
/**/
		var arguments0 =  customer_code;
		var conditionParam = new Object({customer_code:arguments0});

		//CFCメソッド呼出
		var VIdtlPC:PendingCall = ServiceObject.delCustomerData(conditionParam);
		VIdtlPC.responder = new RelayResponder( this, "delCustomerData_Result", "Service_Fault" );

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: TabInitialize
		Description		: タブ設定
		Usage			: TabInitialize();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/30
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function TabInitialize():Void{
		var tabs = 0
		GroundPath.tabChildren = true
		GroundPath.data2_txt.tabIndex = 1;
		GroundPath.data3_txt.tabIndex = 2;
		GroundPath.gender_cb.tabIndex = 3;
		GroundPath.data5_txt.tabIndex = 4;
		GroundPath.area_cb.tabIndex = 5;
		GroundPath.data8_txt.tabIndex = 6;
		GroundPath.data9_txt.tabIndex = 7;
		GroundPath.data10_txt.tabIndex = 8;
		GroundPath.data15_txt.tabIndex = 9;
		GroundPath.data16_txt.tabIndex = 10;
		GroundPath.data17_txt.tabIndex = 11;
		GroundPath.data18_txt.tabIndex = 12;
		GroundPath.commentArea.tabIndex = 13;
		GroundPath.RBPanel_cb.tabIndex = 14;
		GroundPath.deleteBtn.tabIndex = 15;
		GroundPath.editBtn.tabIndex = 16;
		GroundPath.data3_txt.data5_txt.restrict = "0-9¥¥/";
		GroundPath.data8_txt.restrict = "0-9¥¥-";
		GroundPath.data15_txt.restrict = "0-9¥¥-";
		GroundPath.data17_txt.restrict = "0-9¥¥-";
		GroundPath.data16_txt.restrict = "A-Za-z0-9\\-\\@\\_\\/\\#\\$\\%\\&\\.";
		GroundPath.data18_txt.restrict = "A-Za-z0-9\\-\\@\\_\\/\\#\\$\\%\\&\\.";
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: textRefresh
		Description		: テキストクリア
		Usage			: textRefresh();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/30
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function textRefresh():Void{
		for(var i=1; i<21; i++){
			GroundPath[ "data" + i + "_txt"].text = ""
		}
		GroundPath.CI_Item.commentArea.text = ""
		for(var i=2; i<4; i++){
			GroundPath.RB_Panel[ "RB_Item_" + i ].data_txt.text = ""
		}
		//日付入力初期設定
		var today_date:Date = new Date();
		var month = today_date.getMonth()+1;
		var day = today_date.getDate();
		var nextday = today_date.getDate()+1;
		if( month < 10 ){ month = "0" + month }
		if( day < 10 ){ day = "0" + day }
		var today:String = (today_date.getFullYear() + "/" + month + "/" + day);
		if( nextday < 10 ){ nextday = "0" + nextday }
		var nexttoday:String = (today_date.getFullYear() + "/" + month + "/" + nextday);
		GroundPath.RB_Panel.RB_Item_1.data_txt.text = ""
		GroundPath.RB_Panel.RB_Item_2.data_txt.text = today
		GroundPath.RB_Panel.RB_Item_3.data_txt.text = nexttoday

		GroundPath.RB_Panel.RB_Item_1.reserve_code = -1
		GroundPath.RB_Panel.cancel_rb._visible = false;
		GroundPath.RB_Panel.cancel_rb.enabled = false;
		GroundPath.RB_Panel.edit_rb._visible = false;
		GroundPath.RB_Panel.edit_rb.enabled = false;


	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getCustomerDetail_Result
		Description		: 顧客詳細取得成功時処理
		Usage			: getCustomerDetail_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent		（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/06
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getCustomerDetail_Result(ReEvt:ResultEvent):Void{
		GroundPath.RB_Panel.cancel_rb._visible = false;
		GroundPath.RB_Panel.cancel_rb.enabled = false;
		GroundPath.RB_Panel.edit_rb._visible = false;
		GroundPath.RB_Panel.edit_rb.enabled = false;

		//ShowConfirm( GroundPath , "顧客："+ ReEvt.result.items[0].customer_nm, this , null );
		GroundPath.data1_txt.text = ReEvt.result.items[0].customer_id;
		GroundPath.data2_txt.text = ReEvt.result.items[0].customer_nm;
		GroundPath.data3_txt.text = ReEvt.result.items[0].customer_kana;
		GroundPath.data4_txt.text = ReEvt.result.items[0].gender;
		GroundPath.data5_txt.text = ReEvt.result.items[0].birthday;
		GroundPath.data7_txt.text = ReEvt.result.items[0].area;
		GroundPath.data8_txt.text = ReEvt.result.items[0].zipcode;
		GroundPath.data9_txt.text = ReEvt.result.items[0].address1;
		GroundPath.data10_txt.text = ReEvt.result.items[0].address2;
		GroundPath.data15_txt.text = ReEvt.result.items[0].tel1;
		GroundPath.data16_txt.text = ReEvt.result.items[0].email1;
		GroundPath.data17_txt.text = ReEvt.result.items[0].tel2;
		GroundPath.data18_txt.text = ReEvt.result.items[0].email2;
		GroundPath.commentArea.text = ReEvt.result.items[0].information;
		for(var i=1; i<21; i++){
			if( GroundPath[ "CB_Item_" + i ].data_txt.text == "undefined" || GroundPath[ "CB_Item_" + i ].data_txt.text == "null"){
				GroundPath[ "CB_Item_" + i ].data_txt.text = ""
			}
		}
		GroundPath.area_cb.selectedIndex = ReEvt.result.items[0].areaindex-1;
		GroundPath.gender_cb.selectedIndex = ReEvt.result.items[0].genderindex-1;
		GroundPath.RB_Panel.RB_Item_1.reserve_code = -1
		//予約情報取得
		if(ReEvt.result.items[0].reserve_code.length != 0){
			GroundPath.RB_Panel.cancel_rb._visible = true;
			GroundPath.RB_Panel.cancel_rb.enabled = true;
			GroundPath.RB_Panel.edit_rb._visible = true;
			GroundPath.RB_Panel.edit_rb.enabled = true;
			GroundPath.RB_Panel.edit_rb.enabled = true;
			GroundPath.RB_Panel.RB_Item_1.data_txt.selectable = false;
			
			var reserve_code:String = ReEvt.result.items[0].reserve_code;
			var CustmrrsvPC:PendingCall = ServiceObject.getCustomerReserve( reserve_code, customer_code );
			CustmrrsvPC.responder = new RelayResponder( this, "getCustomerReserve_Result", "getCustomerReserve_Fault" );
		}
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: delCustomerData_Result
		Description		: 顧客詳細削除成功時処理
		Usage			: delCustomerData_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent		（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/06
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function delCustomerData_Result(ReEvt:ResultEvent):Void{
		GroundPath._parent.EnabledScreen.removeMovieClip();
		GroundPath.removeMovieClip();
		ShowConfirm( GroundPath ,ReEvt.result.errorMsg , this , null );
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getCustomerDetail_Fault
		Description		: 顧客詳細取得失敗時処理
		Usage			: getCustomerDetail_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent	（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getCustomerDetail_Fault(FltEvt:FaultEvent):Void{	//trace("getCustomerDetail_Fault: " + FltEvt.fault.description);
		//処理を記述
		ShowConfirm( GroundPath , "顧客詳細情報を取得できませんでした。\rエラーメッセージ："+ FltEvt.fault.description, this , null );
		
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getCustomerReserve_Result
		Description		: 顧客予約情報取得成功時処理
		Usage			: getCustomerReserve_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent		（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/06
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getCustomerReserve_Result(ReEvt:ResultEvent):Void{
		var Owner = this;
		GroundPath.RB_Panel.RB_Item_1.reserve_code = ReEvt.result.items[0].reserve_code;

		GroundPath.RB_Panel.RB_Item_1.data_txt.text = ReEvt.result.items[0].reserve_code;
		GroundPath.RB_Panel.RB_Item_2.data_txt.text = ReEvt.result.items[0].arrive_date;
		GroundPath.RB_Panel.RB_Item_3.data_txt.text = ReEvt.result.items[0].depart_date;
		for(var i=2; i<4; i++){
			if( GroundPath.RB_Panel[ "RB_Item_" + i ].data_txt.text == "undefined" || GroundPath.RB_Panel[ "RB_Item_" + i ].data_txt.text == "null"){
				GroundPath.RB_Panel[ "RB_Item_" + i ].data_txt.text = ""
			}
		}
		GroundPath.RB_Panel.enabled = false;
		GroundPath.RB_Panel.csurl_txt.html = true;
		GroundPath.RB_Panel.csurl_txt.htmlText = "<u><a href='" + ReEvt.result.items[0].csurl + "' target='_blank'>" + ReEvt.result.items[0].csurl + "</a></u>"

		if(ReEvt.result.items[0].repeatflg == 1){
			GroundPath.RB_Panel.csurl_txt.htmlText = ""
			GroundPath.RB_Panel.attachMovie("RCSRegBtn","RCSRegBtn",GPDepth++,{_y:60.7})
			GroundPath.RB_Panel.RCSRegBtn.reserve_code = ReEvt.result.items[0].reserve_code
			GroundPath.RB_Panel.RCSRegBtn.onRelease = function(){
				var	RCS = new RepeatCS( Owner.GroundPath,Owner.GroundPath, this.reserve_code);
			}
		}

		GroundPath.RB_Panel.cancel_rb._visible = true;
		GroundPath.RB_Panel.cancel_rb.enabled = true;
		GroundPath.RB_Panel.edit_rb._visible = true;
		GroundPath.RB_Panel.edit_rb.enabled = true;
		GroundPath.RB_Panel.edit_rb.enabled = true;
		GroundPath.RB_Panel.RB_Item_1.data_txt.selectable = false;

}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getMaster_Result
		Description		: エリアマスタ取得成功時処理
		Usage			: getMaster_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent		（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getMaster_Result(ReEvt:ResultEvent):Void{
		var Owner = this
		var aComboData = new Array();
		var areaData = ReEvt.result.items[0].rtm_area;
		for(var i=0; i<areaData.length; i++){
			aComboData.push({data:areaData.items[i].area_code,label:areaData.items[i].name})
		}
		GroundPath.area_cb.dataProvider = aComboData;

		var gComboData = new Array();
		var genderData = ReEvt.result.items[0].rtm_gender;
		for(var i=0; i<genderData.length; i++){
			gComboData.push({data:genderData.items[i].gender_code,label:genderData.items[i].name})
		}
		GroundPath.gender_cb.dataProvider = gComboData;

		//顧客詳細情報取得ＣＦＣメソッド呼出
		if(customer_code != undefined){
			var CustmrdtlPC:PendingCall = ServiceObject.getCustomerDetail( customer_code );
			CustmrdtlPC.responder = new RelayResponder( this, "getCustomerDetail_Result", "getCustomerDetail_Fault" );
		} else {
			customer_code = -1
			textRefresh();
		}

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setCustomerData_Result
		Description		: 顧客情報の登録成功時イベント
		Usage			: setCustomerData_Result(ReEvt);
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/12/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function setCustomerData_Result(ReEvt:ResultEvent):Void{
		SearchWait( GroundPath, false );
		ShowConfirm( GroundPath ,ReEvt.result.errorMsg , this , null );

		customer_code = ReEvt.result.customer_code
		var reserve_code:String = ReEvt.result.reserve_code;
		var CustmrrsvPC:PendingCall = ServiceObject.getCustomerReserve( reserve_code, customer_code);
		CustmrrsvPC.responder = new RelayResponder( this, "getCustomerReserve_Result", "getCustomerReserve_Fault" );
/*

		GroundPath.RB_Panel.RB_Item_1.data_txt.text = ReEvt.result.reserve_code;
		GroundPath.RB_Panel.csurl_txt.html = true;
		GroundPath.RB_Panel.csurl_txt.htmlText = "<u><a href='" + ReEvt.result.csurl + "' target='_blank'>" + ReEvt.result.csurl + "</a></u>"
		customer_code = ReEvt.result.customer_code
		GroundPath.RB_Panel.cancel_rb._visible = true;
		GroundPath.RB_Panel.cancel_rb.enabled = true;
		GroundPath.RB_Panel.edit_rb._visible = true;
		GroundPath.RB_Panel.edit_rb.enabled = true;
		GroundPath.RB_Panel.edit_rb.enabled = true;
		GroundPath.RB_Panel.RB_Item_1.data_txt.selectable = false;
*/
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getCustomerReserve_Fault
		Description		: 顧客詳細取得失敗時処理
		Usage			: getCustomerReserve_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent	（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getCustomerReserve_Fault(FltEvt:FaultEvent):Void{	//trace("getCustomerDetail_Fault: " + FltEvt.fault.description);
		//処理を記述
		ShowConfirm( GroundPath , "顧客予約情報を取得できませんでした。\rエラーメッセージ："+ FltEvt.fault.description, this , null );

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: Service_Fault
		Description		: 失敗時処理
		Usage			: Service_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent	（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function Service_Fault(FltEvt:FaultEvent):Void{
		//処理を記述
		ShowConfirm( GroundPath , "システムが利用できません。\rエラーメッセージ："+ FltEvt.fault.description, this , null );
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: CheckconditionParam
		Description		: 登録項目のチェック
		Usage			: CheckconditionParam(Obj);
		Attributes		: Obj : （検索条件オブジェクト）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/05
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function CheckconditionParam( Obj ){
		var ResultObj = new Object();
		ResultObj.Compelete = false
		if( Obj.reserve_code.length != 0){
			ResultObj.Compelete = true
		} else {
			ResultObj.message = "予約番号が未入力です"
		}
		
		return ResultObj
	}
}
