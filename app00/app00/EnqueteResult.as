﻿/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	Name			: EnqueteResult
	Description		: ＣＳ回答結果取得
	Usage			: var XXXXXX:EnqueteResult = new EnqueteResult( path );
	Attributes		: datagridname:DataGrid		（結果格納用データグリッドコンポーネント名）
					: path:MovieClip			（基準パス）
	ExtendsParam	: ServiceObject:Service
	Method			: initializedData		初期設定
					: getGuestList_Result	検索実行正常終了時イベント
					: getGuestList_Fault	検索実行不正終了時イベント
	Note			: none
	History			: 1) Coded by	nf)h.miyahara	2004/07/09
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
import mx.remoting.Service;
import mx.services.Log;
import mx.rpc.RelayResponder;
import mx.remoting.PendingCall;
import mx.remoting.debug.NetDebug;
import mx.remoting.debug.NetDebugConfig;
import mx.rpc.ResultEvent;
import mx.rpc.FaultEvent;
import mx.controls.gridclasses.DataGridColumn
import mx.controls.DataGrid

class EnqueteResult {

	var GroundPath:MovieClip			//基準ムービークリップパス
	var GPDepth:Number					//Depth
	var ServiceObject:Object			//CFCServiceObject

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: EnqueteResult
		Description		: Constructor
		Usage			: EnqueteResult( myPath:MovieClip );
		Attributes		: myPath:MovieClip	（基準ＭＣパス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/17
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function EnqueteResult( myPath:MovieClip  ) {
		GroundPath = myPath;
		GPDepth = GroundPath.getNextHighestDepth();

		var myDebug = mx.remoting.debug.NetDebug.initialize();
		ServiceObject = new Service( "http://" + _global.hostAddress + "/flashservices/gateway/", null , _global.servicePath + ".Enquete" , null , null ); 
		
		//NetDebbuger設定
		var config:NetDebugConfig = ServiceObject.connection.getDebugConfig();
		config.app_server.trace = true;
		config.client.trace = true;
		
		//ＭＣ初期化
		initializedData();

	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: initializedData
		Description		: 初期設定
		Usage			: initializedData();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function initializedData(){

		//CFCメソッド呼出
		var QuestionPC:PendingCall = ServiceObject.getEnquete(); 
		QuestionPC.responder = new RelayResponder( this, "getQ_Result", "getQ_Fault" );
		/*
		var AnswerPC:PendingCall = ServiceObject.doQry2(); 
		AnswerPC.responder = new RelayResponder( this, "getA_Result", "getA_Fault" );
		*/
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		ログイン処理成功時処理
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function getQ_Result(ReEvt:ResultEvent):Void{
		trace( ReEvt.result.mRecordsAvailable + "件取得しました。"); 
		
		//第一カテゴリ
		GroundPath.attachMovie("QuestionBlock","QuestionBlock_0",100)
		GroundPath.QuestionBlock_0.data_txt.text = ReEvt.result.items[0].questionnairecategory_nm

		var SetCategory = ReEvt.result.items[0].questionnairecategory_nm
		var y_margin = GroundPath.QuestionBlock_0._height
		for(var i=0; i<ReEvt.result.mRecordsAvailable; i++){
			if( SetCategory != ReEvt.result.items[i].questionnairecategory_nm){
				GroundPath.attachMovie("QuestionBlock","QuestionBlock_"+i ,100+i,{_y:y_margin})
				GroundPath["QuestionBlock_" + i].data_txt.text = ReEvt.result.items[i].questionnairecategory_nm
				y_margin = y_margin+_root["QuestionBlock_" + i]._height
				SetCategory = ReEvt.result.items[i].questionnairecategory_nm
			}
			
			GroundPath.attachMovie("Question","Question_"+i ,300+i,{_y:y_margin})
			GroundPath["Question_" + i].data_txt.text = ReEvt.result.items[i].questionnaire_nm
			GroundPath["Question_" + i].id_txt.text = ReEvt.result.items[i].questionnaire_id
			y_margin = y_margin+_root["Question_" + i]._height
		}
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		検索実行失敗時処理
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function getQ_Fault(FltEvt:FaultEvent):Void{
		//処理を記述
		trace("Categories Fault: " + FltEvt.fault.description);
	}
	
	
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		ログイン処理成功時処理
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function getA_Result(ReEvt:ResultEvent):Void{
		trace( ReEvt.result.mRecordsAvailable + "件取得しました。"); 
		/*
		for(var i=0; i<ReEvt.result.mRecordsAvailable; i++){
			for(var C=0; C<ReEvt.result.mRecordsAvailable; C++){
				if(_root["Question_" + C].id_txt.text == ReEvt.result.items[i].questionnaire_id){
					_root["Question_" + C].answer_txt.text = ReEvt.result.items[i].answer
				}
			}
			
		}
		*/
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		検索実行失敗時処理
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function getA_Fault(FltEvt:FaultEvent):Void{
		//処理を記述
		trace("Categories Fault: " + FltEvt.fault.description);
	}

}

