﻿/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	Name			: SpecsheetService
	Description		: スペックシート表示画面
	Usage			: var XXXXXX:SpecsheetService = new SpecsheetService( path );
	Attributes		: path:MovieClip			（基準パス）
	Extends			: CommonFunction(Class)
	Method			: initializedData		初期設定
					: getCRMGuestList_Result	検索実行正常終了時イベント
					: getCRMGuestList_Fault	検索実行不正終了時イベント
	Note			: none
	History			: 1) Coded by	nf)h.miyahara	2004/07/09
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
import mx.remoting.Service;
import mx.services.Log;
import mx.rpc.RelayResponder;
import mx.remoting.PendingCall;
import mx.remoting.debug.NetDebug;
import mx.remoting.debug.NetDebugConfig;
import mx.remoting.RecordSet;
import mx.rpc.ResultEvent;
import mx.rpc.FaultEvent;
import mx.controls.ComboBox;
import mx.controls.TextArea;
import mx.controls.Button;
import mx.controls.Label
import mx.data.binding.*;
import mx.data.components.DataSet
import app00.ValueInformation;
import mx.transitions.easing.*;
import mx.transitions.Tween;
import commonfunc;
import mx.managers.PopUpManager;
import mx.containers.Window;

class app00.SpecsheetService extends MovieClip {

	var GroundPath:MovieClip	//基準ムービークリップパス
	var GPDepth:Number			//Depth
	var ServiceObject:Object	//CFCServiceObject
	var PaneSource:MovieClip	//ScrollPane内MovieClip
	var	targetIndex:Number		//初期表示顧客情報ＩＮＤＥＸ
	var	SummaryIndex:Number		//表示サマリＩＮＤＥＸ
	var	targetData:Object		//表示対象データ
	var customernumber:Number	//customernumber
	var resort_code:Number		//リゾートコード（0:共通）
	var SuumaryData:DataSet		//サマリデータ退避用Dataset
	var EnqueteData:DataSet		//アンケートデータ退避用Dataset
	var DetailAreaHeight:Number	//詳細情報表示時の表示エリア高
	var CallSequence:Array		//表示順（ファンクション呼出順）
	var OpenRecord:Number		//エキスパンド開始ＭＣ
	var SearchResult:DataSet	//検索結果退避用オブジェクト
	var DataSourceArr:Array		//データソース事前指定用配列
	var tmp_startY:Number		
	var tmp_tgtID:Number		
	var tmp_mode:Number		
	var tmp_seq:Number		
	var cfunc:commonfunc
	
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: SpecsheetService
		Description		: Constructor
		Usage			: SpecsheetService( myPath:MovieClip );
		Attributes		: myGrid:DataGrid	（結果格納用データグリッドコンポーネント名）
						: myPath:MovieClip	（基準ＭＣパス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function SpecsheetService( myPath:MovieClip  ) {
		GroundPath = myPath;
		GPDepth = GroundPath.getNextHighestDepth();
		targetIndex = _global.targetIndex	//前画面にて格納
		targetData = _global.targetData		//前画面にて格納

		customernumber = targetData.getItemAt(targetIndex).customernumber//100000083511
		resort_code = targetData.getItemAt(targetIndex).resort_code

		if(customernumber == undefined || resort_code == undefined){
			customernumber = null
			resort_code = 1
		}
		
		tmp_startY = new Number();
		tmp_tgtID = new Number();
		tmp_mode = new Number();
		tmp_seq = new Number();
		cfunc = new commonfunc()
/*
		//本画面ボタン制御用カバーＭＣ
		GroundPath._parent.createEmptyMovieClip("EnabledScreen", GPDepth);
		GPDepth++;
		GroundPath._parent.EnabledScreen.beginFill(0xFF0000);
		GroundPath._parent.EnabledScreen.moveTo(0,0);
		GroundPath._parent.EnabledScreen.lineTo(Stage.width, 0);
		GroundPath._parent.EnabledScreen.lineTo(Stage.width, Stage.height);
		GroundPath._parent.EnabledScreen.lineTo(0, Stage.height);
		GroundPath._parent.EnabledScreen.lineTo(0, 0);
		GroundPath._parent.EnabledScreen._alpha = 40;
		GroundPath._parent.EnabledScreen.enabled = false
		GroundPath._parent.EnabledScreen.onRollOver = function(){}
*/
		DetailAreaHeight = 0;
		CallSequence = new Array()

		var myDebug = mx.remoting.debug.NetDebug.initialize();
		ServiceObject = new Service( "http://" + _global.hostAddress + "/flashservices/gateway/", null , _global.servicePath + ".app00.cfc.specsheet" , null , null ); 
		
		//NetDebbuger設定
		var config:NetDebugConfig = ServiceObject.connection.getDebugConfig();
		config.app_server.trace = true;
		config.client.trace = true;

		//サマリ情報取得
		getSummary();
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: initializedData
		Description		: 初期設定
		Usage			: initializedData();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function initializedData(){

		var Owner = this

		//ScrollPane設定
		GroundPath.specsheet_sp.borderStyle = "none"
		GroundPath.specsheet_sp.vPageScrollSize = GroundPath.specsheet_sp.height;
		GroundPath.specsheet_sp.vLineScrollSize = GroundPath.specsheet_sp.height;
		//GroundPath.specsheet_sp.stop();

		//コンボボックス先行アクセス
		var tmpList = GroundPath.summary_cb.dropdown;

		//メニュー切替
		GroundPath.changeBtn.onRelease = function(){
			Owner.SetMenuList(Owner.GroundPath);
		}

		//閉じるボタン
		GroundPath.ss_closeBtn.onRelease = function(){
			Owner.GroundPath.removeMovieClip();
			Owner.GroundPath._parent.EnabledScreen.removeMovieClip();
		}

		//タブ設定
		TabInitialize();

		//ページング処理呼出
		setPagingInfo()
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setPagingInfo
		Description		: ページング処理
		Usage			: setPagingInfo();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/21
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function setPagingInfo(){
		var nowRecord = targetIndex;  
		var totalRecords = targetData.length;
		var Owner = this;

		//ボタン制御
		GroundPath.paging.BackBtn.enabled = true;
		GroundPath.paging.NextBtn.enabled = true;
		GroundPath.paging.LastBtn.enabled = true;
		GroundPath.paging.FirstBtn.enabled = true;

		//ページ数表示
		GroundPath.paging.page_txt.text = (nowRecord+1) + "/" + totalRecords;
		
		//ボタンクリックイベント
		GroundPath.paging.BackBtn.onRelease = function(){
			Owner.targetIndex = nowRecord - 1;
			Owner.customernumber = Owner.targetData.getItemAt(Owner.targetIndex).customernumber
			//Owner.getCustomerDetail();
			Owner.setSuumaryContents()
		}
		GroundPath.paging.NextBtn.onRelease = function(){
			Owner.targetIndex = nowRecord + 1;
			Owner.customernumber = Owner.targetData.getItemAt(Owner.targetIndex).customernumber
			//Owner.getCustomerDetail();
			Owner.setSuumaryContents()
		}

		GroundPath.paging.LastBtn.onRelease = function(){
			Owner.targetIndex = totalRecords - 1;
			Owner.customernumber = Owner.targetData.getItemAt(Owner.targetIndex).customernumber
			//Owner.getCustomerDetail();
			Owner.setSuumaryContents()
		}

		GroundPath.paging.FirstBtn.onRelease = function(){
			Owner.targetIndex = 0;
			Owner.customernumber = Owner.targetData.getItemAt(Owner.targetIndex).customernumber
			//Owner.getCustomerDetail();
			Owner.setSuumaryContents()
		}

		//ボタン制御
		GroundPath.paging.onEnterFrame = function(){
			if( nowRecord == 0 ){
				this.BackBtn.enabled = false;
				this.FirstBtn.enabled = false;
				this.BackBtn.gotoAndStop(1)
				this.FirstBtn.gotoAndStop(1)
			} else {
				this.BackBtn.enabled = true;
				this.FirstBtn.enabled = true;
			}

			if( nowRecord + 1 == totalRecords ){
				this.NextBtn.enabled = false;
				this.LastBtn.enabled = false;
				this.NextBtn.gotoAndStop(1)
				this.LastBtn.gotoAndStop(1)
			} else {
				this.NextBtn.enabled = true;
				this.LastBtn.enabled = true;
			}
		}
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: TabInitialize
		Description		: タブ設定
		Usage			: TabInitialize();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/30
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function TabInitialize():Void{

		GroundPath.tabChildren = true
		GroundPath.CConditionBox.tabChildren = true
		GroundPath.summary_cb.tabIndex = 2;
		GroundPath.changesummaryBtn.tabIndex = 4;
		GroundPath.previewBtn.tabIndex = 6;
		GroundPath.printBtn.tabIndex = 8;
		GroundPath.changeBtn.tabIndex = 10;

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getSummary
		Description		: サマリ情報取得
		Usage			: getSummary();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/06
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function getSummary(){

		//ＣＦＣメソッド呼出
		var spcsheetPC:PendingCall = ServiceObject.getSummaryData();
		spcsheetPC.responder = new RelayResponder( this, "getSummaryData_Result", "getSummaryData_Fault" );

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getSummaryData_Result
		Description		: サマリ情報取得成功時処理
		Usage			: getSummaryData_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent		（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/06
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getSummaryData_Result(ReEvt:ResultEvent):Void{
		var Owner = this;
		var ComboData = new Array();
		var selctedIdx = 0;
		for(var i=0; i<ReEvt.result.mRecordsAvailable; i++){
			ComboData.push({data:ReEvt.result.items[i].summary_id,label:ReEvt.result.items[i].summary_nm})
			if(_global.UserInfo.ipAddress == ReEvt.result.items[i].summary_ip){
				selctedIdx = i
			}
		}
		GroundPath.summary_cb.dataProvider = ComboData;
		GroundPath.summary_cb.selectedIndex = selctedIdx;

		//Changeイベント
		var ComboListner = new Object();
		ComboListner.change = function(eventObj){
			Owner.SummaryIndex = eventObj.target.selectedIndex
		}
		GroundPath.summary_cb.addEventListener( "change", ComboListner );

		//サマリ情報をデータセットに退避
		SuumaryData = new DataSet();
		SuumaryData.dataProvider = ReEvt.result;

		//表示対象インデックス
		SummaryIndex = selctedIdx;

		//表示切替ボタンに割当
		GroundPath.changesummaryBtn.onRelease = function(){
			Owner.setSuumaryContents()
		}

		//ScrollPane内Content設定
		GroundPath.specsheet_sp.contentPath = "SpecsheetContent"
		PaneSource = GroundPath.specsheet_sp.content

		//ペイン内初期化
		GroundPath.specsheet_sp.refreshPane();

		setSuumaryContents()
		initializedData();

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getSummaryData_Fault
		Description		: サマリ情報取得失敗時処理
		Usage			: getCustomerDetail_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent	（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/06
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getSummaryData_Fault(FltEvt:FaultEvent):Void{	//trace("getSummaryData_Fault: " + FltEvt.fault.description);
		//処理を記述
		cfunc.ShowConfirm( GroundPath , "サマリ情報を取得できませんでした。\rエラーメッセージ："+ FltEvt.fault.description, this , null );
		
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getEnqueteData
		Description		: アンケート情報取得
		Usage			: getEnqueteData();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/06
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function getEnqueteData(startY, tgtID, mode, seq){
		tmp_startY = startY
		tmp_tgtID = tgtID
		tmp_mode = mode
		tmp_seq = seq
		//ＣＦＣメソッド呼出
		var enquetePC:PendingCall = ServiceObject.getUserEnquete(GroundPath.summary_cb.selectedItem.data,PaneSource[ "ReserveRecord_" + tgtID ].expansionBtn.referencenumber,resort_code);
		enquetePC.responder = new RelayResponder( this, "getEnquete_Result", "getEnquete_Fault" );
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getEnquete_Result
		Description		: アンケート情報取得成功時処理
		Usage			: getEnquete_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent		（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/08
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getEnquete_Result(ReEvt:ResultEvent):Void{	//trace( ReEvt.result.mRecordsAvailable + "件取得しました。");
		//サマリ情報をデータセットに退避
		EnqueteData = new DataSet();
		EnqueteData.dataProvider = ReEvt.result;

		//ＭＣ初期化
		//setSuumaryContents()
		//initializedData();
		setCSContents( tmp_startY, tmp_tgtID, tmp_mode, tmp_seq )
/**/
		if( tmp_mode ){
			var tgt_reserve_code = PaneSource[ "ReserveRecord_" + tmp_tgtID ].expansionBtn.reserve_code;
			var tgt_room_code = PaneSource[ "ReserveRecord_" + tmp_tgtID ].expansionBtn.room_code;
			var tgt_referencenumber = PaneSource[ "ReserveRecord_" + tmp_tgtID ].expansionBtn.referencenumber;
			var tgt_MCID = PaneSource[ "ReserveRecord_" + tmp_tgtID ].expansionBtn.MCID;
			var conditionParam = new Object({reserve_code:tgt_reserve_code,room_code:tgt_room_code,referencenumber:tgt_referencenumber,MCID:tgt_MCID});
			var RsrvdtlPC:PendingCall = ServiceObject.getReserveDetail( conditionParam );
			RsrvdtlPC.responder = new RelayResponder( this, "getReserveDetail_Result", "getReserveDetail_Fault" );
		}

	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getEnquete_Fault
		Description		: アンケート情報取得失敗時処理
		Usage			: getEnquete_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent	（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/06
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getEnquete_Fault(FltEvt:FaultEvent):Void{	//trace("getEnquete_Fault: " + FltEvt.fault.description);
		//処理を記述
		cfunc.ShowConfirm( GroundPath , "アンケート情報を取得できませんでした。\rエラーメッセージ："+ FltEvt.fault.description, this , null );

	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setSuumaryContents
		Description		: サマリ情報よりオブジェクトをセット
		Usage			: setSuumaryContents();
		Attributes		: idx:Number	（データセット対象インデックス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/06
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function setSuumaryContents(){

		//ScrollPane内Content設定
		GroundPath.specsheet_sp.contentPath = "SpecsheetContent"
		PaneSource = GroundPath.specsheet_sp.content

		//ペイン内初期化
		GroundPath.specsheet_sp.refreshPane();

		//顧客基本情報ＭＣ設置
		setCustomerBasicContents(0)

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setCustomerBasicContents
		Description		: 顧客基本情報ＭＣ設置
		Usage			: setCustomerBasicContents(idx, startY);
		Attributes		: startY:Number	（配置開始Ｙ座標）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/06
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function setCustomerBasicContents( startY ){

		//サマリデータより表示情報取得
		var viewList = SuumaryData.items[SummaryIndex].basicinfo.split(",")

		//顧客基本情報ヘッダＭＣ設定
		if( SuumaryData.items[SummaryIndex].basicinfo.length != 0 ){
			viewList.unshift(0);
		} else {
			viewList.pop();
		}

		//顧客基本情報ＭＣ設定
		for( var i=0; i<viewList.length; i++ ){
			PaneSource.attachMovie("CB_Item_" + viewList[i] , "CB_Item_" +  viewList[i]  , GPDepth, {_y:startY})
			GPDepth++;
			PaneSource[ "CB_Item_" + viewList[i] ].data_txt.text = "Loading..."
			startY = startY + PaneSource["CB_Item_" + viewList[i]]._height
		}

		//顧客重要情報ＭＣ設置
		setCustomerImportantContents( startY )

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setCustomerImportantContents
		Description		: 顧客重要情報ＭＣ設置
		Usage			: setCustomerImportantContents(idx, startY);
		Attributes		: txtHight:Number	（テキストエリアの高さ）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/06
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function setCustomerImportantContents( txtHight ){

		//サマリデータより表示情報取得
		var viewList = SuumaryData.items[SummaryIndex].importantinfo.split(",")

		var startX = 470
		var startY = 0

		//顧客重要情報ＭＣ設定
		for( var i=0; i<SuumaryData.items[SummaryIndex].importantinfo.length; i++ ){
			PaneSource.attachMovie("CI_Item", "CI_Item", GPDepth, {_y:startY,_x:startX})
			GPDepth++;
			startY = startY + PaneSource.CI_Item._height
		}

		//テキストエリアのリサイズ
		var minimam = 30;
		if( txtHight< minimam ){ txtHight = minimam }
		PaneSource.CI_Item.onEnterFrame = function(){
			if(this.commentArea != undefined){
				this.commentArea.setSize(this.commentArea.width,txtHight)
				delete this.onEnterFrame;
			}
		}
		
		//顧客満足度ＭＣ設置
		setCustomerSatisfiedContents( txtHight )
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setCustomerSatisfiedContents
		Description		: 顧客満足度ＭＣ設置
		Usage			: setCustomerSatisfiedContents(idx, startY);
		Attributes		: startY:Number	（配置開始Ｙ座標）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/06
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function setCustomerSatisfiedContents( startY ){

		//サマリデータより表示情報取得
		var viewList = SuumaryData.items[SummaryIndex].satisfiedinfo.split(",")

		//顧客満足度ＭＣ設定
		var startX = 0
		var Ymargin = 20
		var minimam = 60;
		if( startY< minimam ){ startY = minimam }
		for( var i=0; i<SuumaryData.items[SummaryIndex].satisfiedinfo.length; i++ ){
			PaneSource.attachMovie("CS_Item", "CS_Item", GPDepth, {_y:startY + Ymargin, _x:startX})
			GPDepth++;
			startY = PaneSource.CS_Item._y + PaneSource.CS_Item._height
		}

		//予約基本情報ＭＣ設置
		setReserveBasicContents(startY)

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setReserveBasicContents
		Description		: 予約基本情報ＭＣ設置
		Usage			: setReserveBasicContents(startY);
		Attributes		: startY:Number	（配置開始Ｙ座標）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/06
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function setReserveBasicContents( startY ){
		var Owner = this;

		//サマリデータより表示情報取得
		var viewList = SuumaryData.items[SummaryIndex].stayinfo.split(",");
		var maxRecord = 5//SuumaryData.items[SummaryIndex].viewrecords;

		//予約基本情報ＭＣ設定
		var startX = 0
		var Ymargin = 10
		var Recmargin = 10
		var temp_x  = 0
		var temp_y  = startY
		var temp_Btn_y  = 0

		//予約基本情報ヘッダＭＣ設定
		if( SuumaryData.items[SummaryIndex].stayinfo.length != 0 ){

			PaneSource.attachMovie("RB_Item", "RB_Item", GPDepth, {_y:startY + Ymargin, _x:startX })
			GPDepth++;
			startY = PaneSource.RB_Item._height + startY
			viewList.unshift(0);
		}
		
		//指定履歴回数分表示

		for( var c=0; c<maxRecord; c++ ){

			for( var i=0; i<viewList.length; i++ ){



				PaneSource.createEmptyMovieClip("ReserveRecord_" + c, GPDepth )
				GPDepth++;
				PaneSource[ "ReserveRecord_" + c ]._visible = false;
				PaneSource[ "ReserveRecord_" + c ].attachMovie("RB_Item_" + viewList[i], "RB_Item_" + viewList[i] , GPDepth, {_y:startY + Ymargin, _x:startX })
				GPDepth++;
				startX = startX + PaneSource[ "ReserveRecord_" + c ][ "RB_Item_" + viewList[i] ]._width

				//備考欄座標決定用temp
				if( i==1 ){
					temp_x = startX
				}

				//Ｙ座標シフト
				if( i==13 or i==17 or i==23 or i==29){
					startX = PaneSource[ "ReserveRecord_" + c ].RB_Item_0._width;
					startY = startY + PaneSource[ "ReserveRecord_" + c ].RB_Item_1._height;
				}

				//エキスパンドボタン座標決定用temp
				/*	*/
				if( temp_Btn_y < PaneSource[ "ReserveRecord_" + c ][ "RB_Item_" + viewList[i] ]._y + PaneSource[ "ReserveRecord_" + c ][ "RB_Item_" + viewList[i] ]._height ){
					temp_Btn_y = PaneSource[ "ReserveRecord_" + c ][ "RB_Item_" + viewList[i] ]._y + PaneSource[ "ReserveRecord_" + c ][ "RB_Item_" + viewList[i] ]._height
				}
			
			}

			//valueinfo移動

			if( PaneSource[ "ReserveRecord_" + c ].RB_Item_31 != undefined ){
				//PaneSource[ "ReserveRecord_" + c ].RB_Item_31._x = PaneSource[ "ReserveRecord_" + c ].RB_Item_0._width + PaneSource[ "ReserveRecord_" + c ].RB_Item_0._x
				//PaneSource[ "ReserveRecord_" + c ].RB_Item_31._y = temp_Btn_y
				temp_Btn_y = temp_Btn_y + PaneSource[ "ReserveRecord_" + c ].RB_Item_31._height
				PaneSource[ "ReserveRecord_" + c ].RB_Item_31.data_txt.vScrollPolicy = "auto"
				PaneSource[ "ReserveRecord_" + c ].RB_Item_31.data_txt.hScrollPolicy = "auto"
				PaneSource[ "ReserveRecord_" + c ].RB_Item_31.data_txt.borderStyle = "solid"

				PaneSource[ "ReserveRecord_" + c ].RB_Item_31.valueRegBtn.MCID = c;
				PaneSource[ "ReserveRecord_" + c ].RB_Item_31.onEnterFrame = function(){
					if(Owner.GroundPath.valueInformation != undefined){
						this.valueRegBtn.enabled = false;
						this.valueRegBtn.gotoAndStop(1);
					} else {
						this.valueRegBtn.enabled = true;
					}
				}
				PaneSource[ "ReserveRecord_" + c ].RB_Item_31.valueRegBtn.onRelease = function(){
					//ValueInformationクラスインスタンス生成
					var	VI = new ValueInformation( Owner.PaneSource,Owner.GroundPath, this.reserve_code, this.room_code, this.MCID,Owner.resort_code);

				}
			}
/**/
			//エキスパンドボタンアタッチ
			PaneSource[ "ReserveRecord_" + c ].attachMovie("expansionBtn", "expansionBtn" , GPDepth, {_y:temp_Btn_y, _x:PaneSource[ "ReserveRecord_" + c ].RB_Item_0._width })
			GPDepth++;
			//PaneSource[ "ReserveRecord_" + c ].expansionBtn._y = startY +  PaneSource[ "ReserveRecord_" + c ].RB_Item_31._height + PaneSource[ "ReserveRecord_" + c ].expansionBtn._height;
			PaneSource[ "ReserveRecord_" + c ].expansionBtn._y = PaneSource[ "ReserveRecord_" + c ].RB_Item_30._y + PaneSource[ "ReserveRecord_" + c ].RB_Item_30._height;
			PaneSource[ "ReserveRecord_" + c ].expansionBtn.MCID = c;
			PaneSource[ "ReserveRecord_" + c ].expansionBtn.openMode = false;
			PaneSource[ "ReserveRecord_" + c ].expansionBtn.onRelease = function(){

				//詳細情報読込開始
				this.openMode = !this.openMode
				this.arrow_mc._rotation += 180;
				this._parent.add_h = 0
				
				//詳細情報取得
				//Owner.OpenRecord = this.MCID;

				if(this.openMode){
					var tgt_reserve_code = this.reserve_code;
					var tgt_room_code = this.room_code;
					var tgt_referencenumber = this.referencenumber;
					var tgt_MCID = this.MCID;
					var conditionParam = new Object({reserve_code:tgt_reserve_code,room_code:tgt_room_code,referencenumber:tgt_referencenumber,MCID:tgt_MCID});
					var RsrvdtlPC:PendingCall = Owner.ServiceObject.getReserveDetail( conditionParam );
					RsrvdtlPC.responder = new RelayResponder( Owner, "getReserveDetail_Result", "getReserveDetail_Fault" );
					this.data_txt.text = "this.MCID:" + this.MCID

				}

				//表示順１番目の項目設定ファンクションをコール
				Owner.setSequence( this._y, this.MCID, this.openMode, 0 )
				//Owner.setDetailsContents( this._y, this.MCID, this.openMode )

			}

			//備考欄移動
			//PaneSource[ "ReserveRecord_" + c ].RB_Item_30._x = temp_x
			//PaneSource[ "ReserveRecord_" + c ].RB_Item_30._y = startY + 160
			PaneSource[ "ReserveRecord_" + c ].RB_Item_30.data_txt.borderStyle = "solid"

			//ラベルテキスト
			PaneSource[ "ReserveRecord_" + c ].RB_Item_0.label_txt.text = maxRecord-c

			startY = PaneSource[ "ReserveRecord_" + c ].expansionBtn._y + PaneSource[ "ReserveRecord" + "_" + c ].expansionBtn._height
			startX = 0
		}

		//ＳＰサイズ調整用ＭＣ
/*
		PaneSource.createEmptyMovieClip("EndMC", GPDepth);
		GPDepth++;
		PaneSource.EndMC.beginFill(0xFF0000);
		PaneSource.EndMC.moveTo(0, startY);
		PaneSource.EndMC.lineTo(0, startY + 100 );
		PaneSource.EndMC.lineTo(10, startY + 100 );
		PaneSource.EndMC.lineTo(10, startY);
		PaneSource.EndMC.lineTo(0, startY);
		PaneSource.EndMC.endFill();
*/		
		//詳細情報取得
		getCustomerDetail();
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setSequence
		Description		: 表示順によるファンクション呼出制御
		Usage			: setRoomContents(startY, tgtID, mode);
		Attributes		: startY:Number		（配置開始Ｙ座標）
						: tgtID:Number		（アタッチ対象ＭＣ）
						: mode:Boolian		（表示モード）
						: seq:Number		（表示順）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/08
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function setSequence( startY, tgtID, mode, seq ){

		//表示順格納
		CallSequence[0] = "setSRContents"
		CallSequence[1] = "setCSContents"
/**/
		if( CallSequence[seq]  == "setSRContents" ){
			//setCSContents( startY, tgtID, mode, seq )
			setSRContents( startY, tgtID, mode, seq )
		}

		if( CallSequence[seq]  == "setCSContents" ){
			//setCSContents( startY, tgtID, mode, seq )
			getEnqueteData( startY, tgtID, mode, seq )
		}

		//getEnqueteData( startY, tgtID, mode, seq )
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setSRContents
		Description		: 明細情報ＭＣ設置
		Usage			: setSRContents(startY, tgtID, mode);
		Attributes		: startY:Number		（配置開始Ｙ座標）
						: tgtID:Number		（アタッチ対象ＭＣ）
						: openMode:Boolian	（表示モード）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/08
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function setSRContents( startY, tgtID, mode, seq ){
		//表示順設定
		seq++;
/*
		//サマリデータより表示情報取得
		var viewList = SuumaryData.items[SummaryIndex].shopresultinfo.split(",");
		var startX = 40
		var Ymargin = 20
		var Owner = this;
		//trace("DetailAreaHeight[setSRContents] : " + DetailAreaHeight + " mode: " + mode)
		DetailAreaHeight = PaneSource[ "ReserveRecord_" + tgtID ]._height
		if(mode){
			if( SuumaryData.items[SummaryIndex].detailinfo.length != 0 ){
				//trace("明細情報開始Y：" + startY)
				//明細情報ＭＣ設定
				PaneSource[ "ReserveRecord_" + tgtID ].attachMovie("D_Item", "D_Item" , GPDepth, {_y:startY + Ymargin, _x:startX })
				GPDepth++;
				//startX = startX + PaneSource[ "ReserveRecord" + "_" + tgtID ].D_Item._width
				startY = startY + Ymargin + PaneSource[ "ReserveRecord" + "_" + tgtID ].D_Item._height

				//データソース事前指定
				var gridFieldsArr = new Array("amount","shop_nm","shop_id");
				var NewgridFieldsArr = new Array();
				for(var i=0; i<gridFieldsArr.length; i++){
					if( SuumaryData.items[SummaryIndex].detailinfo.indexOf(i+1) != -1 ){
						NewgridFieldsArr.push(gridFieldsArr[i])
					}
				}
				var ColumnObj = new Object();
				for(var i=0; i<gridFieldsArr.length; i++){
					ColumnObj[gridFieldsArr[i]] = ""
				}
				var SourceArr = new Array(ColumnObj);

				//Header設定
				var temp_ColumnHeaderStr = new Array("店舗コード", "店舗名", "金額")
				var ColumnHeaderStr = new Array();

				for(var i=0; i<temp_ColumnHeaderStr.length; i++){
					if( SuumaryData.items[SummaryIndex].detailinfo.indexOf(i+1) != -1 ){
						ColumnHeaderStr.push(temp_ColumnHeaderStr[i])
					}
				}

				//データグリッド設定
				PaneSource[ "ReserveRecord_" + tgtID ].D_Item.detail_grd.onLoad = function(){
					this.dataProvider = SourceArr
					//for( var i=0; i<ColumnHeaderStr.length;i++ ){
					for( var i=0; i<temp_ColumnHeaderStr.length;i++ ){
						this.getColumnAt(i).headerText = temp_ColumnHeaderStr[i];
					}
					this.setStyle("fontSize", 10);
					this.setStyle("headerStyle", 10);
				}

			}

		} else {
			//明細情報ＭＣ消去
			PaneSource[ "ReserveRecord_" + tgtID ].D_Item.removeMovieClip();
		}
*/
		//エキスパンド処理
		RecExpansion(tgtID)

		//担当情報取得
		//setChargeContents( startY, tgtID, mode );
		setSequence( startY, tgtID, mode, seq );

	}
	
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setCSContents
		Description		: ＣＳ情報ＭＣ設置
		Usage			: setCSContents(startY, tgtID, mode);
		Attributes		: startY:Number		（配置開始Ｙ座標）
						: tgtID:Number		（アタッチ対象ＭＣ）
						: openMode:Boolian	（表示モード）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/08
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function setCSContents( startY, tgtID, mode, seq ){
		//表示順設定
		seq++;

		//サマリデータより表示情報取得
		var viewList = SuumaryData.items[SummaryIndex].csinfo.split(",");
		var startX = 40
		var Q_startX = 40
		var Ymargin = 20
		DetailAreaHeight = PaneSource[ "ReserveRecord_" + tgtID ]._height
		//trace("DetailAreaHeight[setCSContents] : " + DetailAreaHeight + " mode: " + mode)

		if(mode){

			//ＣＳ情報ヘッダＭＣ設定
			if( SuumaryData.items[SummaryIndex].csinfo.length != 0 ){
				PaneSource[ "ReserveRecord_" + tgtID ].attachMovie("E_Item_0", "E_Item_0", GPDepth, {_y:startY + Ymargin, _x:startX })
				GPDepth++;
				startY = startY + Ymargin + PaneSource[ "ReserveRecord_" + tgtID ].E_Item_0._height

				//ＣＳ情報ＭＣ設定
				for( var i=0; i<viewList.length; i++ ){
					PaneSource[ "ReserveRecord_" + tgtID ].attachMovie("E_Item_" + viewList[i], "E_Item_" + viewList[i] , GPDepth, {_y:startY, _x:startX })
					GPDepth++;
					startX = startX + PaneSource[ "ReserveRecord_" + tgtID ]["E_Item_" + viewList[i]]._width

				}
				startY = startY + PaneSource[ "ReserveRecord_" + tgtID ]["E_Item_" + viewList[viewList.length-1]]._height
				//trace("CS情報開始Y：" + startY)
			} else {

				if( EnqueteData.dataProvider.length > 0 ){
					PaneSource[ "ReserveRecord_" + tgtID ].attachMovie("E_Item_0", "E_Item_0", GPDepth, {_y:startY + Ymargin, _x:startX })
					GPDepth++;
					startY = startY + Ymargin + PaneSource[ "ReserveRecord_" + tgtID ].E_Item_0._height
				}

			}

			//ＣＳ設問ＭＣ設定
			for( var i=0; i<EnqueteData.dataProvider.length; i++ ){

				//カテゴリパネル
				if(EnqueteData.items[i-1].questionnairecategory_id != EnqueteData.items[i].questionnairecategory_id){
					PaneSource[ "ReserveRecord_" + tgtID ].attachMovie("Q_Item_1", "QC_Item_" + i , GPDepth, {_y:startY, _x:Q_startX })
					GPDepth++;
					PaneSource[ "ReserveRecord_" + tgtID ]["QC_Item_" + i].data_txt.text =  EnqueteData.items[i].questionnairecategory_nm;
					//PaneSource[ "ReserveRecord_" + tgtID ]["QC_Item_" + i].data_txt.text =  PaneSource[ "ReserveRecord_" + tgtID ]["QC_Item_" + i]._name;
					startY = startY + PaneSource[ "ReserveRecord_" + tgtID ]["QC_Item_" + i]._height
				
				} 
				//設問パネル
				var q_id = EnqueteData.items[i].questionnaire_id
//trace("q_id : "  + q_id)
				PaneSource[ "ReserveRecord_" + tgtID ].attachMovie("Q_Item_0", "Q_Item_" + q_id , GPDepth, {_y:startY, _x:Q_startX })
				GPDepth++;
				PaneSource[ "ReserveRecord_" + tgtID ]["Q_Item_" + q_id].data_txt.text =  EnqueteData.items[i].questionnaire_nm;
				//PaneSource[ "ReserveRecord_" + tgtID ]["Q_Item_" + q_id].data_txt.text = PaneSource[ "ReserveRecord_" + tgtID ]["Q_Item_" + q_id]._name;
				var sidePush = PaneSource[ "ReserveRecord_" + tgtID ]["Q_Item_" + q_id]._width
				//回答値パネル
				if( EnqueteData.items[i].questionnaire_type == 1 ){
					PaneSource[ "ReserveRecord_" + tgtID ]["Q_Item_" + q_id].attachMovie("Q_Item_3", "A_Item" , GPDepth, {_y:0, _x:PaneSource[ "ReserveRecord" + "_" + tgtID ]["Q_Item_" + q_id]._width })
					GPDepth++;
//					trace("1 q_id : "  + q_id + "questionnaire_type : "  + EnqueteData.items[i].questionnaire_type)
				} else if( EnqueteData.items[i].questionnaire_type == 0 ) {
					PaneSource[ "ReserveRecord_" + tgtID ]["Q_Item_" + q_id].attachMovie("Q_Item_4", "A_Item" , GPDepth, {_y:0, _x:PaneSource[ "ReserveRecord" + "_" + tgtID ]["Q_Item_" + q_id]._width })
					GPDepth++;
//					trace("2 q_id : "  + q_id + "questionnaire_type : "  + EnqueteData.items[i].questionnaire_type)
				} else {
					PaneSource[ "ReserveRecord_" + tgtID ]["Q_Item_" + q_id].attachMovie("Q_Item_5", "A_Item" , GPDepth, {_y:0, _x:PaneSource[ "ReserveRecord" + "_" + tgtID ]["Q_Item_" + q_id]._width })
					GPDepth++;
//					trace("3 q_id : "  + q_id + "questionnaire_type : "  + EnqueteData.items[i].questionnaire_type)
					PaneSource[ "ReserveRecord_" + tgtID ]["Q_Item_" + q_id].A_Item.createClassObject(TextArea, "data_txt" , GPDepth)
					PaneSource[ "ReserveRecord_" + tgtID ]["Q_Item_" + q_id].A_Item.data_txt.vScrollPolicy = "auto";
					PaneSource[ "ReserveRecord_" + tgtID ]["Q_Item_" + q_id].A_Item.data_txt.setSize(315,40)
					PaneSource[ "ReserveRecord_" + tgtID ]["Q_Item_" + q_id].A_Item.data_txt.wordWrap = true;
				}
				GPDepth++;
				
				startY = startY + PaneSource[ "ReserveRecord" + "_" + tgtID ][ "Q_Item_" + q_id ]._height
				PaneSource[ "ReserveRecord_" + tgtID ]["Q_Item_" + q_id].A_Item.gotoAndStop(8)
			}

		} else {
			//ＣＳ情報ヘッダＭＣ消去
			PaneSource[ "ReserveRecord_" + tgtID ].E_Item_0.removeMovieClip();

			//ＣＳ情報ＭＣ消去
			for( var i=0; i<viewList.length; i++ ){
				PaneSource[ "ReserveRecord_" + tgtID ]["E_Item_" + viewList[i]].removeMovieClip();
			}
			
			//設問＆回答値ＭＣ消去
			for( var i=0; i<EnqueteData.dataProvider.length; i++ ){
				var q_id = EnqueteData.items[i].questionnaire_id
				PaneSource[ "ReserveRecord_" + tgtID ]["QC_Item_" + i].removeMovieClip();
				PaneSource[ "ReserveRecord_" + tgtID ]["Q_Item_" + q_id].removeMovieClip();
				//PaneSource[ "ReserveRecord" + "_" + tgtID ]["A_Item_" + q_id].removeMovieClip();
			}
		}

		//エキスパンド処理
		RecExpansion(tgtID)
		
		setSequence( startY, tgtID, mode, seq );

		//詳細情報取得
		//getCustomerDetail();

	}
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: RecExpansion
		Description		: 予約情報詳細エキスパンド処理
		Usage			: RecExpansion( tgtID );
		Attributes		: tgtID:Number		（アタッチ対象ＭＣ）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/08
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function RecExpansion( tgtID ){
		//trace("_height : " +  PaneSource[ "ReserveRecord" + "_" + tgtID ]._height + " DetailAreaHeight : " + DetailAreaHeight)
		//補正値（？？原因不明）
		var adjustment_h = 3

		var resize = PaneSource[ "ReserveRecord" + "_" + tgtID ]._height - DetailAreaHeight + adjustment_h
		var RecobjCnt = tgtID + 1
		while(PaneSource[ "ReserveRecord" + "_" + RecobjCnt ] != undefined ){
			PaneSource[ "ReserveRecord" + "_" + RecobjCnt ]._y = PaneSource[ "ReserveRecord" + "_" + RecobjCnt ]._y + resize;
			RecobjCnt++;
		}
		DetailAreaHeight = 0
		//ＳＰリサイズ
		GroundPath.specsheet_sp.setSize(GroundPath.specsheet_sp.width,GroundPath.specsheet_sp.height)
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getCustomerDetail
		Description		: 顧客詳細情報取得
		Usage			: getCustomerDetail( customerID );
		Attributes		: customer_id:Number	（顧客番号）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/06
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function getCustomerDetail(){	//trace(customerID)
		GroundPath.specsheet_sp.setSize(GroundPath.specsheet_sp.width,GroundPath.specsheet_sp.height)

		
		//顧客詳細情報取得ＣＦＣメソッド呼出
		var CustmrdtlPC:PendingCall = ServiceObject.getCustomerDetail( customernumber,resort_code );
		CustmrdtlPC.responder = new RelayResponder( this, "getCustomerDetail_Result", "getCustomerDetail_Fault" );

		//詳細情報リフレッシュ
		var OpenRecord:Number = 0;
		PaneSource[ "ReserveRecord_" + OpenRecord ].R_Item.detail_grd.dataProvider = new Array();
		PaneSource[ "ReserveRecord_" + OpenRecord ].C_Item.C_Item_1.data_txt.text = "Loading...";
		PaneSource[ "ReserveRecord_" + OpenRecord ].C_Item.C_Item_2.data_txt.text = "Loading...";
		PaneSource[ "ReserveRecord_" + OpenRecord ].C_Item.C_Item_3.data_txt.text = "Loading...";
		PaneSource[ "ReserveRecord_" + OpenRecord ].C_Item.C_Item_4.data_txt.text = "Loading...";
		PaneSource[ "ReserveRecord_" + OpenRecord ].S_Item.S_Item_1.data_txt.text = "Loading...";
		PaneSource[ "ReserveRecord_" + OpenRecord ].S_Item.S_Item_2.data_txt.text = "Loading...";
		PaneSource[ "ReserveRecord_" + OpenRecord ].S_Item.S_Item_3.data_txt.textsweeper = "Loading...";
		PaneSource[ "ReserveRecord_" + OpenRecord ].S_Item.S_Item_4.data_txt.text = "Loading...";
		//PaneSource[ "ReserveRecord_" + OpenRecord ].D_Item.detail_grd.dataProvider = new Array();

		PaneSource[ "ReserveRecord_" + OpenRecord ].E_Item_1.data_txt.text = "Loading...";
		PaneSource[ "ReserveRecord_" + OpenRecord ].E_Item_2.data_txt.text = "Loading...";
		
/*
*/		
		for( var i=0; i<EnqueteData.dataProvider.length; i++ ){
			PaneSource[ "ReserveRecord_" + OpenRecord ][ "Q_Item_" + EnqueteData.items[i].questionnaire_id ].A_Item.gotoAndStop(8)
			PaneSource[ "ReserveRecord_" + OpenRecord ][ "Q_Item_" + EnqueteData.items[i].questionnaire_id ].A_Item.data_txt.text = "";
		}

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getCustomerDetail_Result
		Description		: 顧客詳細取得成功時処理
		Usage			: getCustomerDetail_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent		（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	2005/12/20	 h.miyahara(NetFusion)
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getCustomerDetail_Result(ReEvt:ResultEvent):Void{
		//cfunc.ShowConfirm( GroundPath , "顧客："+ ReEvt.result.items[0].customer_nm, this , null );
		PaneSource.CB_Item_1.data_txt.text = ReEvt.result.items[0].customernumber;
		PaneSource.CB_Item_2.data_txt.text = ReEvt.result.items[0].customercategory;
		PaneSource.CB_Item_3.data_txt.text = ReEvt.result.items[0].customerrank;
		PaneSource.CB_Item_4.data_txt.text = ReEvt.result.items[0].customer_nm;
		PaneSource.CB_Item_5.data_txt.text = ReEvt.result.items[0].customer_kana;
		PaneSource.CB_Item_6.data_txt.text = ReEvt.result.items[0].birthday;
		PaneSource.CB_Item_7.data_txt.text = ReEvt.result.items[0].area;
		PaneSource.CB_Item_8.data_txt.text = ReEvt.result.items[0].zipcode;
		PaneSource.CB_Item_9.data_txt.text = ReEvt.result.items[0].address1;
		PaneSource.CB_Item_10.data_txt.text = ReEvt.result.items[0].address2;
		PaneSource.CB_Item_11.data_txt.text = ReEvt.result.items[0].address3;
		PaneSource.CB_Item_12.data_txt.text = ReEvt.result.items[0].tel1;
		PaneSource.CB_Item_13.data_txt.text = ReEvt.result.items[0].tel2;
		PaneSource.CB_Item_14.data_txt.text = ReEvt.result.items[0].email1;
		PaneSource.CB_Item_15.data_txt.text = ReEvt.result.items[0].email2;
		PaneSource.CB_Item_16.data_txt.text = ReEvt.result.items[0].married;
		var nullchk:String = ReEvt.result.items[0].information;
		if(nullchk.length != undefined){
			PaneSource.CI_Item.commentArea.text = ReEvt.result.items[0].information;
		} else {
			PaneSource.CI_Item.commentArea.text = "";
		}
		for(var i=1; i<16; i++){
			if( PaneSource[ "CB_Item_" + i ].data_txt.text == "undefined" || PaneSource[ "CB_Item_" + i ].data_txt.text == "null"){
				PaneSource[ "CB_Item_" + i ].data_txt.text = ""
			}
		}

		//満足度情報設定
		var start_x = 64.0;
		var start_y = 125.5;
		var temp_satisfied = ReEvt.result.items[0].satisfied.split(",");
		var temp_answerd = ReEvt.result.items[0].answerd.split(",");
		var ObjCnt:Number = 0;
		while( PaneSource.CS_Item["GraphData_" + ObjCnt] != undefined ){
			PaneSource.CS_Item["GraphData_" + ObjCnt].removeMovieClip();
			ObjCnt++;
		}

		if( ReEvt.result.items[0].satisfied.length != 0 ){
			PaneSource.CS_Item.gotoAndStop(2)
			for(var i=0; i<temp_satisfied.length; i++){
				
				PaneSource.CS_Item.attachMovie("GraphData" , "GraphData_" + i ,GPDepth,{_x:start_x, _y:start_y})
				GPDepth++;
				PaneSource.CS_Item["GraphData_" + i].Data._height = temp_satisfied[i];
				PaneSource.CS_Item["GraphData_" + i].point_txt.text = temp_satisfied[i];
				PaneSource.CS_Item["GraphData_" + i].data_txt.text = temp_answerd[i];
				start_x = start_x + PaneSource.CS_Item["GraphData_" + i]._width;
			}
		}else{
			PaneSource.CS_Item.gotoAndStop(1)
		}
		
		//対象顧客の予約情報取得
		var customernumber:String = ReEvt.result.items[0].customernumber;
		var CustmrrsvPC:PendingCall = ServiceObject.getCustomerReserve( customernumber,resort_code );
		CustmrrsvPC.responder = new RelayResponder( this, "getCustomerReserve_Result", "getCustomerReserve_Fault" );

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getCustomerDetail_Fault
		Description		: 顧客詳細取得失敗時処理
		Usage			: getCustomerDetail_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent	（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getCustomerDetail_Fault(FltEvt:FaultEvent):Void{	//trace("getCustomerDetail_Fault: " + FltEvt.fault.description);
		//処理を記述
		cfunc.ShowConfirm( GroundPath , "顧客詳細情報を取得できませんでした。\rエラーメッセージ："+ FltEvt.fault.description, this , null );
		
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getCustomerReserve_Result
		Description		: 顧客予約情報取得成功時処理
		Usage			: getCustomerReserve_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent		（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/06
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getCustomerReserve_Result(ReEvt:ResultEvent):Void{
		var Owner = this;
		//予約情報初期化
		var ObjCnt:Number = 0;
		while( PaneSource[ "ReserveRecord_" + ObjCnt ] != undefined ){
			//PaneSource[ "ReserveRecord_" + ObjCnt ]._visible = false;
			PaneSource[ "ReserveRecord_" + ObjCnt ].enabled = false;
			ObjCnt++;
		}
		PaneSource.RB_Item.gotoAndStop(2)
		//存在予約分表示
		var resist_count = 0
		for( var i=0; i<ReEvt.result.mRecordsAvailable; i++ ){
			PaneSource[ "ReserveRecord_" + i ]._visible = true;
			//初期化
			PaneSource[ "ReserveRecord_" + i ].RB_Item_1.cancelDataBtn.removeMovieClip();
			PaneSource[ "ReserveRecord_" + i ].RB_Item_1.gotoAndStop(1);
			//PaneSource[ "ReserveRecord_" + i ].RB_Item_7.vip_icon.gotoAndStop(1);
			PaneSource.RB_Item.gotoAndStop(1)

			PaneSource[ "ReserveRecord_" + i ]._visible = true;
			PaneSource[ "ReserveRecord_" + i ].enabled = true;
			PaneSource[ "ReserveRecord_" + i ].RB_Item_0.label_txt.text = ReEvt.result.mRecordsAvailable-i
			PaneSource[ "ReserveRecord_" + i ].RB_Item_1.data_txt.text = ReEvt.result.items[i].reserve_code;
			PaneSource[ "ReserveRecord_" + i ].RB_Item_2.data_txt.text = ReEvt.result.items[i].room_code;
			PaneSource[ "ReserveRecord_" + i ].RB_Item_3.data_txt.text = ReEvt.result.items[i].arrive_date;
			PaneSource[ "ReserveRecord_" + i ].RB_Item_4.data_txt.text = ReEvt.result.items[i].depart_date;
			PaneSource[ "ReserveRecord_" + i ].RB_Item_5.data_txt.text = ReEvt.result.items[i].nights + "泊";
			PaneSource[ "ReserveRecord_" + i ].RB_Item_6.data_txt.text = ReEvt.result.items[i].lateout_flg;
			PaneSource[ "ReserveRecord_" + i ].RB_Item_7.data_txt.text = ReEvt.result.items[i].manpersoncount + ReEvt.result.items[i].womenpersoncount + ReEvt.result.items[i].childpersoncount_a + ReEvt.result.items[i].childpersoncount_b + ReEvt.result.items[i].childpersoncount_c + ReEvt.result.items[i].childpersoncount_d + "名";
			PaneSource[ "ReserveRecord_" + i ].RB_Item_8.data_txt.text = ReEvt.result.items[i].manpersoncount + "名";
			PaneSource[ "ReserveRecord_" + i ].RB_Item_9.data_txt.text = ReEvt.result.items[i].womenpersoncount + "名";
			PaneSource[ "ReserveRecord_" + i ].RB_Item_10.data_txt.text = ReEvt.result.items[i].childpersoncount_a + "名";
			PaneSource[ "ReserveRecord_" + i ].RB_Item_11.data_txt.text = ReEvt.result.items[i].childpersoncount_b + "名";
			PaneSource[ "ReserveRecord_" + i ].RB_Item_12.data_txt.text = ReEvt.result.items[i].childpersoncount_c + "名";
			PaneSource[ "ReserveRecord_" + i ].RB_Item_13.data_txt.text = ReEvt.result.items[i].childpersoncount_d + "名";
			PaneSource[ "ReserveRecord_" + i ].RB_Item_14.data_txt.text = ReEvt.result.items[i].surveymethod_nm;
			PaneSource[ "ReserveRecord_" + i ].RB_Item_15.data_txt.text = ReEvt.result.items[i].registrated.length;
			PaneSource[ "ReserveRecord_" + i ].RB_Item_16.data_txt.text = ReEvt.result.items[i].referencenumber;
			PaneSource[ "ReserveRecord_" + i ].RB_Item_17.data_txt.text = ReEvt.result.items[i].referencenumber;
			PaneSource[ "ReserveRecord_" + i ].RB_Item_18.data_txt.text = ReEvt.result.items[i].marketing_1;
			PaneSource[ "ReserveRecord_" + i ].RB_Item_19.data_txt.text = ReEvt.result.items[i].marketing_2;
			PaneSource[ "ReserveRecord_" + i ].RB_Item_20.data_txt.text = ReEvt.result.items[i].marketing_3;
			PaneSource[ "ReserveRecord_" + i ].RB_Item_21.data_txt.text = ReEvt.result.items[i].roomtype;
			PaneSource[ "ReserveRecord_" + i ].RB_Item_22.data_txt.text = ReEvt.result.items[i].roomcontrol;
			PaneSource[ "ReserveRecord_" + i ].RB_Item_23.data_txt.text = ReEvt.result.items[i].reservationroute;
			PaneSource[ "ReserveRecord_" + i ].RB_Item_24.data_txt.text = "￥" + ReEvt.result.items[i].totalsalesamount;
			PaneSource[ "ReserveRecord_" + i ].RB_Item_25.data_txt.text = "￥" + ReEvt.result.items[i].totalsalesamountdividedbynights;
			PaneSource[ "ReserveRecord_" + i ].RB_Item_26.data_txt.text = "￥" + ReEvt.result.items[i].personunitprice;
			PaneSource[ "ReserveRecord_" + i ].RB_Item_27.data_txt.text = "￥" + ReEvt.result.items[i].roomcharge;
			PaneSource[ "ReserveRecord_" + i ].RB_Item_28.data_txt.text = "￥" + ReEvt.result.items[i].optionamount;
			PaneSource[ "ReserveRecord_" + i ].RB_Item_29.data_txt.text = "￥" + ReEvt.result.items[i].restaurantamount;
			var nullchk:String = ReEvt.result.items[i].information;
			if(nullchk.length != undefined){
				PaneSource[ "ReserveRecord_" + i ].RB_Item_30.data_txt.text = ReEvt.result.items[i].information;
			} else {
				PaneSource[ "ReserveRecord_" + i ].RB_Item_30.data_txt.text = "";
			}
			PaneSource[ "ReserveRecord_" + i ].RB_Item_31.data_txt.text = ReEvt.result.items[i].valueinformation;
			PaneSource[ "ReserveRecord_" + i ].RB_Item_31.data_txt.setSize(375,40);

			//CancelData
			if( ReEvt.result.items[i].cancel_code == 100000000001 ){
				PaneSource[ "ReserveRecord_" + i ].RB_Item_1.attachMovie("cancelDataBtn","cancelDataBtn",GPDepth,{_x:0.5,_y:0.5})
				GPDepth++;
			}

			//ExpandBtn
			PaneSource[ "ReserveRecord_" + i ].expansionBtn.reserve_code = ReEvt.result.items[i].reserve_code;
			PaneSource[ "ReserveRecord_" + i ].expansionBtn.room_code = ReEvt.result.items[i].room_code;
			PaneSource[ "ReserveRecord_" + i ].expansionBtn.referencenumber = ReEvt.result.items[i].referencenumber;
			if( PaneSource[ "ReserveRecord_" + i ].expansionBtn.openMode == true ){
				var tgt_reserve_code = PaneSource[ "ReserveRecord_" + i ].expansionBtn.reserve_code;
				var tgt_room_code = PaneSource[ "ReserveRecord_" + i ].expansionBtn.room_code;
				var tgt_referencenumber = PaneSource[ "ReserveRecord_" + i ].expansionBtn.referencenumber;
				var tgt_MCID = PaneSource[ "ReserveRecord_" + i ].expansionBtn.MCID;
				var conditionParam = new Object({reserve_code:tgt_reserve_code,room_code:tgt_room_code,referencenumber:tgt_referencenumber,MCID:tgt_MCID});
				var RsrvdtlPC:PendingCall = ServiceObject.getReserveDetail( conditionParam );
				RsrvdtlPC.responder = new RelayResponder( this, "getReserveDetail_Result", "getReserveDetail_Fault" );
			}

			//CS回答アリ
			var cstype = ReEvt.result.items[i].cstype
			if(ReEvt.result.items[i].registrated != '0' &&  cstype == 0){
				PaneSource[ "ReserveRecord_" + i ].RB_Item_15.gotoAndStop(2);
				resist_count=  i
			}else{
				PaneSource[ "ReserveRecord_" + i ].RB_Item_15.csviewBtn.referencenumber = ReEvt.result.items[i].referencenumber
				PaneSource[ "ReserveRecord_" + i ].RB_Item_0.test.text = i


				PaneSource[ "ReserveRecord_" + i ].RB_Item_15.csviewBtn.cstype = cstype
				PaneSource[ "ReserveRecord_" + i ].RB_Item_15.csviewBtn.onRelease = function(){
					if(this.cstype == 0){
						var csurlPC:PendingCall = Owner.ServiceObject.getcsurl( this.referencenumber );
						csurlPC.responder = new RelayResponder( Owner, "getcsurl_Result", "getcsurl_Fault" );
					} else {
						/*var	RCS = new RepeatCS( Owner.PaneSource,Owner.GroundPath, this.referencenumber);
						
						var csurlPC:PendingCall = Owner.ServiceObject.getcsurl( this.referencenumber );
						csurlPC.responder = new RelayResponder( Owner, "getcsurl_Result", "getcsurl_Fault" );
						*/
					}
				}
			}

			//リピーターＣＳ登録
			/*
			if( ReEvt.result.items[i].cancel_code != 100000000001 ){
				PaneSource[ "ReserveRecord_" + i ].RB_Item_15.attachMovie("RCSRegBtn","RCSRegBtn",GPDepth)
				GPDepth++;
				PaneSource[ "ReserveRecord_" + i ].RB_Item_15.RCSRegBtn.reserve_code = ReEvt.result.items[i].referencenumber;
				PaneSource[ "ReserveRecord_" + i ].RB_Item_15.RCSRegBtn.enabled = false;
				PaneSource[ "ReserveRecord_" + i ].RB_Item_15.RCSRegBtn._visible = false;
				PaneSource[ "ReserveRecord_" + i ].RB_Item_15.RCSRegBtn.onRelease = function(){
					var	RCS = new RepeatCS( Owner.PaneSource,Owner.GroundPath, this.reserve_code);
				}
			}
			*/

			//ValueInformation登録
			PaneSource[ "ReserveRecord_" + i ].RB_Item_31.valueRegBtn.reserve_code = ReEvt.result.items[i].reserve_code;
			PaneSource[ "ReserveRecord_" + i ].RB_Item_31.valueRegBtn.room_code = ReEvt.result.items[i].room_code;

			//ショップリザルト
			var temp = ReEvt.result.items[i].shopresult.split(';')
			var trgt = PaneSource[ "ReserveRecord_" + i ].RB_Item_17
			for (var t = 0; t<temp.length; t++) {
				var mark = temp[t]
				if(mark.length == 1){
					mark = "0" + mark
				}
				trgt[ "shopresult_" + mark ].gotoAndStop(2);
			}
		}
		
		//表示件数より履歴が少ない場合削除
		for(var c=0; c<ObjCnt; c++){
			if( PaneSource[ "ReserveRecord_" + c ].enabled == false ){
				PaneSource[ "ReserveRecord_" + c ].removeMovieClip();
			}
		}

		//リピーターＣＳ登録ボタン表示
		for(var r=0; r<resist_count; r++){
			PaneSource[ "ReserveRecord_" + r ].RB_Item_1.RCSRegBtn.enabled = true;
			PaneSource[ "ReserveRecord_" + r ].RB_Item_1.RCSRegBtn._visible = true;
		}

		//ＳＰリサイズ
		GroundPath.specsheet_sp.setSize(GroundPath.specsheet_sp.width,GroundPath.specsheet_sp.height)

		//ページング処理呼出
		setPagingInfo()

		//ローディング用ＭＣ削除処理
		var delmc = GroundPath._parent.CoveredStage_mc
		delmc.Loading.gotoAndPlay(2)
		var Deltween:Object = new Tween(delmc, "_alpha", Strong.easeIn, delmc._alpha, 0, 2, true);
		Deltween.delmc = delmc
		Deltween.onMotionFinished = function() {
			this.delmc.removeMovieClip();
		};

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getCustomerReserve_Fault
		Description		: 顧客詳細取得失敗時処理
		Usage			: getCustomerReserve_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent	（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getCustomerReserve_Fault(FltEvt:FaultEvent):Void{	//trace("getCustomerDetail_Fault: " + FltEvt.fault.description);
		//処理を記述
		cfunc.ShowConfirm( GroundPath , "顧客予約情報を取得できませんでした。\rエラーメッセージ："+ FltEvt.fault.description, this , null );

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getReserveDetail_Result
		Description		: 予約詳細取得成功時処理
		Usage			: getReserveDetail_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent		（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/06
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getReserveDetail_Result(ReEvt:ResultEvent):Void{
		var owner = this;
		var OpenRecord = ReEvt.result.RecNum;
		PaneSource[ "ReserveRecord_" + OpenRecord ].D_Item.detail_grd.dataProvider = ReEvt.result.useQuery;
		/*		*/
		var dgLength = PaneSource[ "ReserveRecord_" + OpenRecord ].D_Item.detail_grd.columnCount
		var removeList = new Array()
		for(var i=0; i<dgLength; i++){
			if( SuumaryData.items[SummaryIndex].detailinfo.indexOf(i+1) == -1 ){
				removeList.push(i)
			}
		}
		var delCnt = 0
		for(var i=0; i<removeList.length; i++){
			PaneSource[ "ReserveRecord_" + OpenRecord ].D_Item.detail_grd.removeColumnAt(removeList[i] - delCnt)
			delCnt++;
		}
		PaneSource[ "ReserveRecord_" + OpenRecord ].D_Item.detail_grd.spaceColumnsEqually()


		//trace("add_h:" + PaneSource[ "ReserveRecord_" + OpenRecord ].add_h)
		if( PaneSource[ "ReserveRecord_" + OpenRecord ].add_h != undefined ){
			//座標調整
			add_h = -PaneSource[ "ReserveRecord_" + OpenRecord ].add_h

			var start_y:Number = PaneSource[ "ReserveRecord" + "_" + OpenRecord ].QC_Item_0._y;
			for( var i=0; i<EnqueteData.dataProvider.length; i++ ){			
				if(EnqueteData.items[i-1].questionnairecategory_id != EnqueteData.items[i].questionnairecategory_id){
					PaneSource[ "ReserveRecord_" + OpenRecord ][ "QC_Item_" + i ]._y = start_y;
					start_y = start_y + PaneSource[ "ReserveRecord_" + OpenRecord ]["QC_Item_" + i]._height
				}
				
				PaneSource[ "ReserveRecord_" + OpenRecord ][ "Q_Item_" + EnqueteData.items[i].questionnaire_id ]._y = start_y;
				start_y = start_y + PaneSource[ "ReserveRecord_" + OpenRecord ][ "Q_Item_" + EnqueteData.items[i].questionnaire_id ]._height
			}

			var RecobjCnt = OpenRecord + 1
			while(PaneSource[ "ReserveRecord_" + RecobjCnt ] != undefined ){
				PaneSource[ "ReserveRecord_" + RecobjCnt ]._y += add_h;
				RecobjCnt++;
			}
			GroundPath.specsheet_sp.setSize(GroundPath.specsheet_sp.width,GroundPath.specsheet_sp.height)
		}


		//CS回答値
		var CSAnswers = new DataSet();
		CSAnswers = ReEvt.result.answersQuery;
		if( CSAnswers.items[0].depart_date != undefined || CSAnswers.items[0].depart_date != null ){
			PaneSource[ "ReserveRecord_" + OpenRecord ].E_Item_1.data_txt.text = CSAnswers.items[0].depart_date;
		} else {
			PaneSource[ "ReserveRecord_" + OpenRecord ].E_Item_1.data_txt.text = "";
		}
		if( CSAnswers.items[0].registrated != undefined || CSAnswers.items[0].registrated != null ){
			PaneSource[ "ReserveRecord_" + OpenRecord ].E_Item_2.data_txt.text = CSAnswers.items[0].registrated.split("-").join("/");
		} else {
			PaneSource[ "ReserveRecord_" + OpenRecord ].E_Item_2.data_txt.text = "";
		}
		

		var tmp_qid = CSAnswers.items[0].questionnaire_id
		var tmp_area_h = PaneSource[ "ReserveRecord" + "_" + OpenRecord ]._height
		var Q_startX = 40

		//ＣＳ設問ＭＣ設定
		for( var i=0; i<EnqueteData.dataProvider.length; i++ ){

			//全削除
			var targetObj = PaneSource[ "ReserveRecord_" + OpenRecord ][ "Q_Item_" + EnqueteData.items[i].questionnaire_id ].A_Item
			targetObj.removeMovieClip();

			//回答値パネル再配置
			if( EnqueteData.items[i].questionnaire_type == 1 ){
				PaneSource[ "ReserveRecord_" + OpenRecord ]["Q_Item_" + EnqueteData.items[i].questionnaire_id ].attachMovie("Q_Item_3", "A_Item" , GPDepth++, {_y:0, _x:PaneSource[ "ReserveRecord_" + OpenRecord ]["Q_Item_" + EnqueteData.items[i].questionnaire_id]._width })
			} else if( EnqueteData.items[i].questionnaire_type == 0 ) {
				PaneSource[ "ReserveRecord_" + OpenRecord ]["Q_Item_" + EnqueteData.items[i].questionnaire_id ].attachMovie("Q_Item_4", "A_Item" , GPDepth++, {_y:0, _x:PaneSource[ "ReserveRecord_" + OpenRecord ]["Q_Item_" + EnqueteData.items[i].questionnaire_id]._width })
			} else {
				PaneSource[ "ReserveRecord_" + OpenRecord ]["Q_Item_" + EnqueteData.items[i].questionnaire_id ].attachMovie("Q_Item_5", "A_Item" , GPDepth++, {_y:0, _x:PaneSource[ "ReserveRecord_" + OpenRecord ]["Q_Item_" + EnqueteData.items[i].questionnaire_id]._width })
				GPDepth++;

				//	2010/08/07 画像対応用ボタン
				PaneSource[ "ReserveRecord_" + OpenRecord ]["Q_Item_" + EnqueteData.items[i].questionnaire_id].A_Item.createClassObject(Button, "oabutton" , GPDepth);
				GPDepth++;
				PaneSource[ "ReserveRecord_" + OpenRecord ]["Q_Item_" + EnqueteData.items[i].questionnaire_id].A_Item.oabutton.label = "OA画像";
				var clickListener = new Object();
				clickListener.click = function(event) {
					var wk_content =event.target._parent.str
					owner.createWindow(wk_content);
				}
				PaneSource[ "ReserveRecord_" + OpenRecord ]["Q_Item_" + EnqueteData.items[i].questionnaire_id].A_Item.oabutton.addEventListener("click", clickListener);
				//	2010/08/07 画像対応用ボタン

				PaneSource[ "ReserveRecord_" + OpenRecord ]["Q_Item_" + EnqueteData.items[i].questionnaire_id].A_Item.createClassObject(TextArea, "data_txt" , GPDepth,{vScrollPolicy:"on",hScrollPolicy:"on",editable:false})
				GPDepth++;
				PaneSource[ "ReserveRecord_" + OpenRecord ]["Q_Item_" + EnqueteData.items[i].questionnaire_id].A_Item.data_txt.vScrollPolicy = "auto";
				PaneSource[ "ReserveRecord_" + OpenRecord ]["Q_Item_" + EnqueteData.items[i].questionnaire_id].A_Item.data_txt.setSize(315,40)
				PaneSource[ "ReserveRecord_" + OpenRecord ]["Q_Item_" + EnqueteData.items[i].questionnaire_id].A_Item.data_txt.wordWrap = true;
				PaneSource[ "ReserveRecord_" + OpenRecord ]["Q_Item_" + EnqueteData.items[i].questionnaire_id].A_Item.data_txt.text = ""
					
					
				
			}
			GPDepth++;
			PaneSource[ "ReserveRecord_" + OpenRecord ]["Q_Item_" + EnqueteData.items[i].questionnaire_id].A_Item.gotoAndStop(8)
		}

		var add_h:Number = 0;
		for(var i=0; i<CSAnswers.length; i++){
			//trace("id:" + CSAnswers.items[i].questionnaire_id + " a:" + CSAnswers.items[i].answer + " t: " + CSAnswers.items[i].answertext)
			PaneSource[ "ReserveRecord_" + OpenRecord ][ "Q_Item_" + CSAnswers.items[i].questionnaire_id ].A_Item.gotoAndStop(8)
			PaneSource[ "ReserveRecord_" + OpenRecord ][ "Q_Item_" + CSAnswers.items[i].questionnaire_id ].A_Item.data_txt.text = ""
			PaneSource[ "ReserveRecord_" + OpenRecord ][ "Q_Item_" + CSAnswers.items[i].questionnaire_id ].A_Item["A_" + i].data_txt.text = ""

			PaneSource[ "ReserveRecord_" + OpenRecord ][ "Q_Item_" + CSAnswers.items[i].questionnaire_id ].A_Item.gotoAndStop(CSAnswers.items[i].answer)
			PaneSource[ "ReserveRecord_" + OpenRecord ][ "Q_Item_" + CSAnswers.items[i].questionnaire_id ].A_Item.data_txt.text = CSAnswers.items[i].answertext
		

			//複数回答時回答表示欄追加
			if( tmp_qid == CSAnswers.items[i+1].questionnaire_id){
				var targetObj = PaneSource[ "ReserveRecord_" + OpenRecord ][ "Q_Item_" + CSAnswers.items[i].questionnaire_id ].A_Item
				targetObj.attachMovie("Q_Item_4", "A_" + i , GPDepth, {_y:targetObj._height, _x:0 })
				GPDepth++;
				targetObj["A_" + i].data_txt.text =  CSAnswers.items[i].answertext
				
				//表示されている設問追加回答分の高さ取得
				if( targetObj["A_" + i] != undefined ){
					add_h = add_h + targetObj["A_" + i]._height
				}
			}
			tmp_qid = CSAnswers.items[i+1].questionnaire_id
			//trace("回答：" + CSAnswers.items[i].answertext)
			
			//	2010/08/07 OA画像表示対応
			var str = PaneSource[ "ReserveRecord_" + OpenRecord ][ "Q_Item_" + CSAnswers.items[i].questionnaire_id ].A_Item.data_txt.text;
				PaneSource[ "ReserveRecord_" + OpenRecord ][ "Q_Item_" + CSAnswers.items[i].questionnaire_id ].A_Item.str = str;
			var checkstr = str.substr( (str.length-3), 3);

			//	画像orテキスト判別
			if( checkstr == 'jpg'){
				PaneSource[ "ReserveRecord_" + OpenRecord ][ "Q_Item_" + CSAnswers.items[i].questionnaire_id ].A_Item.data_txt.removeMovieClip();
				var clickListener = new Object();
				clickListener.click = function(event) {
					var wk_content =this._parent.str
//					trace(wk_content)
					owner.createWindow(wk_content);
				}
				PaneSource[ "ReserveRecord_" + OpenRecord ][ "Q_Item_" + CSAnswers.items[i].questionnaire_id ].A_Item.oabutton.addEventListener("click", clickListener);
			}
			//	2010/08/07 OA画像表示対応			
			
			
		}

		//座標調整
		PaneSource[ "ReserveRecord_" + OpenRecord ].add_h = add_h

		var start_y:Number = PaneSource[ "ReserveRecord_" + OpenRecord ].QC_Item_0._y;
		for( var i=0; i<EnqueteData.dataProvider.length; i++ ){			
			if(EnqueteData.items[i-1].questionnairecategory_id != EnqueteData.items[i].questionnairecategory_id){
				PaneSource[ "ReserveRecord_" + OpenRecord ][ "QC_Item_" + i ]._y = start_y;
				start_y = start_y + PaneSource[ "ReserveRecord_" + OpenRecord ]["QC_Item_" + i]._height
			}
			
			PaneSource[ "ReserveRecord_" + OpenRecord ][ "Q_Item_" + EnqueteData.items[i].questionnaire_id ]._y = start_y;
			start_y = start_y + PaneSource[ "ReserveRecord_" + OpenRecord ][ "Q_Item_" + EnqueteData.items[i].questionnaire_id ]._height
		}

		var RecobjCnt = OpenRecord + 1
		while(PaneSource[ "ReserveRecord_" + RecobjCnt ] != undefined ){
			PaneSource[ "ReserveRecord_" + RecobjCnt ]._y += add_h;
			RecobjCnt++;
		}
		GroundPath.specsheet_sp.setSize(GroundPath.specsheet_sp.width,GroundPath.specsheet_sp.height)

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getReserveDetail_Fault
		Description		: 予約詳細取得失敗時処理
		Usage			: getCustomerDetail_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent	（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getReserveDetail_Fault(FltEvt:FaultEvent):Void{	//trace("getCustomerDetail_Fault: " + FltEvt.fault.description);
		//処理を記述
		cfunc.ShowConfirm( GroundPath , "予約詳細情報を取得できませんでした。\rエラーメッセージ："+ FltEvt.fault.description, this , null );
		
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: MCprintPreview
		Description		: スクロールペイン印刷プレビュー設定
		Usage			: MCprintPreview();
		Attributes		: none
		Note			: 共通関数
		History			: 1) Coded by	nf)h.miyahara	2004/10/04
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function MCprintPreview(mode){

		//変数設定
		var Owner = this;
		var printtargetMC = GroundPath.specsheet_sp;
		var headerArea_h = 30;
		var headerArea_x = 10;
		var temp:mx.data.types.Str;
		var temp:mx.data.types.Num;
		var temp_MC_y = printtargetMC._y;					//対象ＭＣＹ座標退避
		var temp_MC_h = printtargetMC.height;	//対象ＭＣ高さ退避
		var temp_MC_depth = printtargetMC.getDepth();		//対象ＭＣ深度退避
		var tmp_sp_w = 900
		var tmp_sp_h = 420
		printtargetMC.vPosition = 0;

		//プレビュー用ＭＣ
		GroundPath.attachMovie("PreviewHeader","PPreview", GPDepth,{_x:GroundPath._x, _y:GroundPath._y});
		GPDepth++;
		GroundPath.PPreview.beginFill(0xFFFFF9);
		GroundPath.PPreview.moveTo(GroundPath.PPreview._x,GroundPath.PPreview._height);
		GroundPath.PPreview.lineTo(Stage.width, GroundPath.PPreview._height);
		GroundPath.PPreview.lineTo(Stage.width, Stage.height);
		GroundPath.PPreview.lineTo(GroundPath.PPreview._x, Stage.height);
		GroundPath.PPreview.lineTo(GroundPath.PPreview._x, GroundPath.PPreview._height);
		GroundPath.PPreview.endFill();

		//ページング用ＣＢ作成
		GroundPath.PPreview.createClassObject(ComboBox,"pages_cb",GPDepth,{_x:250,_y:5,_width:100, _height:22});
		GPDepth++;

		//プレビュー用ＳＰ位置設定
		printtargetMC.swapDepths( GroundPath.PPreview.getDepth()+10 ); 
		var pageWidth = 559
		var pageHeight = 806

		var s_w = pageWidth/tmp_sp_w
		var s_h = pageHeight/tmp_sp_h
		var r_h = tmp_sp_w/pageWidth
		printtargetMC.scaleX = s_w*100
		printtargetMC.scaleY = s_w*100
		var printSize_h = Math.floor(pageHeight*r_h)
		var resize_h = pageHeight+50
		//var resize_h = Math.floor(pageHeight*r_h)
		//GroundPath.PPreview.PrintTarget.temp_sp.setSize(tmp_sp_w,resize_h);
		printtargetMC.setSize(tmp_sp_w,resize_h);

		//printtargetMC.setSize( printtargetMC._width, 545 )
		if( mode == "preview" ){
			printtargetMC._y = headerArea_h + 5
		}
		var tempRec = SearchResult
		var PaneSource = printtargetMC.content
		var start_y = 0;
		//var rowsPerPage = 30
		var rowsPerPage =20
		//var dummyCnt = rowsPerPage - (tempRec.dataProvider.length%rowsPerPage)
		
		//ページング用変数設定
		var Pagesize = Math.floor(printtargetMC._height*r_h); //Math.floor((targetPath.PPreview.tmp_dg.height - targetPath.PPreview.tmp_dg.headerHeight)/targetPath.PPreview.tmp_dg.rowHeight);
		//trace("s_w : " + s_w)
		var pages = Math.ceil(Pagesize/printSize_h);
/*
		trace("r_h : " + r_h)
		trace("Pagesize : " + Pagesize)
		trace("printtargetMC.height : " + printtargetMC.height)
		trace("printtargetMC._height : " + printtargetMC._height)
		trace("pages : " + pages)
*/
		var pages_arr = new Array({data:"",label:"全ページ"});
		for(var i=0;i<pages;i++){
			pages_arr.push({data:i,label:i+1+"ページ"})
		}

		printtargetMC.Pagesize = Pagesize
		printtargetMC.pages = pages
		printtargetMC.rowsPerPage = rowsPerPage
		printtargetMC.r_h = r_h

		//コンボボックスイベント設定
		var ComboListner = new Object();
		ComboListner.rowsPerPage = rowsPerPage;

		//Loadイベント
		GroundPath.PPreview.pages_cb.onEnterFrame = function(){
			Owner.GroundPath.PPreview.pages_cb.dataProvider = pages_arr
			if(Owner.GroundPath.PPreview.pages_cb.dataProvider.length > 0){
				delete this.onEnterFrame;
			}
		}

		//Changeイベント
		ComboListner.change = function(eventObj){

			//var startRec = rowsPerPage*eventObj.target.value*60;
			var startRec = pageHeight*eventObj.target.value*r_h;
			trace("startRec : " + startRec)
			if(eventObj.target.value == "" ){
				startRec = 0
				printtargetMC.vScrollPolicy = "auto";
			} else {
				printtargetMC.vScrollPolicy = "off";
			}
				printtargetMC.vPosition = startRec
		}
		GroundPath.PPreview.pages_cb.addEventListener( "change", ComboListner );

		//閉じるボタン
		GroundPath.PPreview.pre_closeBtn.onRelease = function (){
			Owner.RecoverResultData(temp_MC_y,temp_MC_h,temp_MC_depth);
			this._parent.removeMovieClip();
		}

		//印刷ボタン
		GroundPath.PPreview.pre_printBtn.onRelease = function (){
			printtargetMC.setSize(tmp_sp_w,printSize_h);
			//Owner.doDGPrint(Owner.targetPath.PPreview.tmp_dg)
			Owner.doMCPrint(Owner.GroundPath.ResultsPanel,mode);
		}

		//印刷ボタンより直接コール
		if( mode =="print" ){
			GroundPath.PPreview._y = 1000;
			GroundPath.PPreview.onEnterFrame = function(){
				if( printtargetMC != undefined ){
					var tmp = Owner.doMCPrint("print" );
					if(tmp == undefined){
						printtargetMC.swapDepths( GroundPath.PPreview.getDepth()-10 );
						Owner.GroundPath.PPreview.removeMovieClip();
					}
				}
				printtargetMC.scaleX = 100;
				printtargetMC.scaleY = 100;
				printtargetMC.setSize( 900, 420 )
			}

		}
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: doMCPrint
		Description		: 印刷実行
		Usage			: doMCPrint(tgtMC);
		Attributes		: tgtMC			印刷対象ＭＣ
		Note			: 
		History			: 1) Coded by	nf)h.miyahara	2004/09/27
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function doMCPrint(mode){
		trace("doMCPrint:mode  " + mode)
		var printtargetMC = GroundPath.specsheet_sp;
		var PrintQueue : PrintJob = new PrintJob();
		var prev_vPosition:Number = printtargetMC.vPosition;
		var prev_width:Number = printtargetMC.width;
		var prev_height:Number = printtargetMC.height;
		var pageWidth = 559
		var pageHeight = 806

		//var RecordCount:Number = printtargetMC.dataProvider.length 
/*印刷用
印刷用*/

		if( PrintQueue.start() != true){
			printtargetMC.setSize(prev_width,856);
		//GroundPath.PPreview.removeMovieClip();
		return;
		}
		//printtargetMC.setSize(PrintQueue.pageWidth,PrintQueue.pageHeight);
		printtargetMC.vScrollPolicy = "off";

		//var rowsPerPage = 9
		//var Pagesize = printtargetMC.height + 20; //Math.floor((targetPath.PPreview.tmp_dg.height - targetPath.PPreview.tmp_dg.headerHeight)/targetPath.PPreview.tmp_dg.rowHeight);
		//var pages = Math.ceil(printtargetMC.height/Pagesize);
		//var headerHeight = 20;

		var rowsPerPage = printtargetMC.rowsPerPage
		var Pagesize = printtargetMC.Pagesize
		var pages = printtargetMC.pages
		var headerHeight = 0;

		//対象ページ確認
		//trace("：" + GroundPath.PPreview.pages_cb.selectedItem.data.length)
		var startCnt = 0
		if( GroundPath.PPreview.pages_cb.selectedItem.data != "" ){
			startCnt = GroundPath.PPreview.pages_cb.selectedItem.data
			pages = GroundPath.PPreview.pages_cb.selectedItem.data+1
		}

		var yStart = 0;
		for ( var i=startCnt; i<pages; i++ ) {

			//var startRec = rowsPerPage*i*60;
			var startRec = pageHeight*i*printtargetMC.r_h;

			printtargetMC.vScrollPolicy = "off";
			printtargetMC.vPosition = startRec;

			//var b= {xMin:0,xMax:printtargetMC._width,yMin:yStart,yMax:printtargetMC._height};
			var b= {xMin:0,xMax:printtargetMC.width,yMin:yStart,yMax:printtargetMC.height};
			yStart = headerHeight;
			PrintQueue.addPage(printtargetMC,b);

		}
		PrintQueue.send();
		
		delete PrintQueue;

		printtargetMC.setSize(prev_width,856);
		printtargetMC.vPosition = 0;
		printtargetMC.vScrollPolicy = "auto";

		//印刷ボタンより直接コール
		if( mode =="print" ){
			trace("印刷ボタンより直接コール")
			GroundPath.PPreview.removeMovieClip();
			for( var i=0; i<SearchResult.dataProvider.length; i++ ){
				PaneSource["resultBar_" + i].information_txt.setSize(760,40);
				PaneSource["resultBar_" + i].specSheetBtn._visible = true;
			}
			printtargetMC.specsheetBtn._visible = true
		}

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: RecoverResultData
		Description		: プレビュー用に変更した対象ＭＣのデータ、サイズ等を元に戻す
		Usage			: RecoverSearchData();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/05
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function RecoverResultData( y_pos, h_size, dpt ){
		trace("RecoverResultData" + y_pos)
		var printtargetMC = GroundPath.specsheet_sp;
		var temp_MC_y = y_pos;
		var temp_MC_h = h_size;
		var temp_MC_depth = dpt;
		var start_y = 0;

		//スクロールペイン位置戻し
		printtargetMC._y = temp_MC_y;
		printtargetMC.scaleX = 100;
		printtargetMC.scaleY = 100;
		printtargetMC.setSize( printtargetMC._width, temp_MC_h )
		printtargetMC.swapDepths( temp_MC_depth ); 

		printtargetMC.vScrollPolicy = "auto";

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getcsurl_Result
		Description		: ＷＥＢＣＳＵＲＬ取得成功時処理
		Usage			: getcsurl_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent		（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/06
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getcsurl_Result(ReEvt:ResultEvent):Void{
		trace(ReEvt.result.csurl)
		var Owner = this;
		GroundPath.getURL(ReEvt.result.csurl,'_blank')
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: createWindow
		Description		: 
		Usage			: 
		Attributes		: 
		Note			: none
		History			: 1) Coded by	2010/08/06 5:23
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function createWindow(wk_content){
		var owner = this
		var my_win:MovieClip = mx.managers.PopUpManager.createPopUp(_root, Window, false , {closeButton:true, contentPath:wk_content})
		my_win._visible = false

		var winListener:Object = new Object();
		winListener.click = function() {
			 my_win.deletePopUp();
		};
		my_win.addEventListener("click", winListener);
		
		winListener.complete = function(evt_obj:Object) {

			var wk_w = my_win._width
			var wk_h = my_win._height
			my_win.setSize(Stage.width/2 , wk_h + 25);

			my_win.scaleX = 80;
			my_win.scaleY = 80;

			my_win.setSize(wk_w , wk_h + 25);

			my_win.content.x_scale = 50;
			my_win.content.y_scale = 50;

			my_win._x = (Stage.width/2) - (my_win._width/2);
			my_win._y = (Stage.height/2) - (my_win._height/2);
			my_win._visible = true
		}

		my_win.addEventListener("complete", winListener);
		
		
	}

}
