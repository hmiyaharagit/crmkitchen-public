﻿/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	Name			: MailSingleService
	Description		: メールテンプレート情報取得
	Usage			: var XXXXXX:MailSingleService = new MailSingleService( Path );
	Attributes		: Path:MovieClip	（基準パス）
	ExtendsParam	: none
	Method			: 
	Note			: none
	History			: 1) Coded by	nf)h.miyahara	2004/07/09
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
import mx.remoting.Service;
import mx.services.Log;
import mx.rpc.RelayResponder;
import mx.remoting.PendingCall;
import mx.remoting.debug.NetDebug;
import mx.remoting.debug.NetDebugConfig;
import mx.rpc.ResultEvent;
import mx.rpc.FaultEvent;
import mx.transitions.easing.*;
import mx.transitions.Tween;
import commonfunc;

class boh.MailSingleService extends MovieClip {

	var GroundPath:MovieClip			//基準ムービークリップパス
	var GPDepth:Number					//Depth
	var targetIndex:Number				//対象ＩＮＤＥＸ
	var targetData:Object				//対象データプロバイダ
	var ServiceObject:Object			//CFCServiceObject
	var MailID:Number					//mailtemlate_id
	var rMailID:Number					//mailtemlate_id
	var rmdp:Array					
	var rmidx:Number					
	var mdp:Array					
	var midx:Number					
	var cfunc:commonfunc

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: MailSingleService
		Description		: Constructor
		Usage			: MailSingleService( myPath:MovieClip );
		Attributes		: myPath:MovieClip	（基準ＭＣパス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function MailSingleService( myPath:MovieClip ) {
		var dataLength = _global.targetData.length
		
		GroundPath = myPath;
		targetIndex = _global.targetIndex;			//MailTemplateServiceクラスで設定
		targetData = _global.targetData;			//MailTemplateServiceクラスで設定
		MailID = _global.mailTemplateIndex;			//
		rMailID = _global.mailrTemplateIndex;		//
		rmdp = GroundPath.rtemplate_cb.dataProvider
		rmidx=GroundPath.rtemplate_cb.selectedIndex
		mdp=GroundPath.template_cb.dataProvider
		midx=GroundPath.template_cb.selectedIndex
		cfunc = new commonfunc()

		GPDepth = GroundPath.getNextHighestDepth();

		var myDebug = mx.remoting.debug.NetDebug.initialize();
		ServiceObject = new Service( "http://" + _global.hostAddress + "/flashservices/gateway/", null , _global.servicePath + ".boh.cfc.mailTemplate" , null , null ); 
		
		//NetDebbuger設定
		var config:NetDebugConfig = ServiceObject.connection.getDebugConfig();
		config.app_server.trace = true;
		config.client.trace = true;

		//Loading
		GroundPath.createEmptyMovieClip("CoveredStage_mc", GPDepth);
		GPDepth++;
		GroundPath.CoveredStage_mc.beginFill(0xCCCCCC);
		GroundPath.CoveredStage_mc.moveTo(0,0);
		GroundPath.CoveredStage_mc.lineTo(Stage.width, 0);
		GroundPath.CoveredStage_mc.lineTo(Stage.width, Stage.height);
		GroundPath.CoveredStage_mc.lineTo(0, Stage.height);
		GroundPath.CoveredStage_mc.lineTo(0, 0);
		GroundPath.CoveredStage_mc.endFill();
		GroundPath.CoveredStage_mc._alpha = 40;
		GroundPath.CoveredStage_mc.enabled = false
		GroundPath.CoveredStage_mc.onRollOver = false
		GroundPath.CoveredStage_mc.attachMovie("Loading","Loading",GPDepth,{_x:Stage.width*0.5,_y:Stage.height*0.5})
		GPDepth++;
		
		//ＭＣ初期化
		initializedData();
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getMailDetail
		Description		: メールテンプレート詳細情報取得
		Usage			: getMailDetail( mail_id );
		Attributes		: mail_id:Number	（メールテンプレートＩＤ）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function getMailDetail(){
		var conditionParam = new Object()
		if(targetData.getItemAt(targetIndex).repeater==''){
			conditionParam.mailtemplate_id = MailID
			GroundPath.template2_cb.dataProvider = mdp
			GroundPath.template2_cb.selectedIndex = midx
		}else{
			conditionParam.mailtemplate_id = rMailID
			GroundPath.template2_cb.dataProvider = rmdp
			GroundPath.template2_cb.selectedIndex = rmidx
		}
		conditionParam.customer_nm = targetData.getItemAt(targetIndex).customer_nm.substring(0,targetData.getItemAt(targetIndex).customer_nm.indexOf("("))
		conditionParam.referencenumber = targetData.getItemAt(targetIndex).referencenumber
		//ＣＦＣメソッド呼出
		var MaildtlPC:PendingCall = ServiceObject.getMailDetail( conditionParam );
		MaildtlPC.responder = new RelayResponder( this, "getMailDetail_Result", "getMailDetail_Fault" );
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getMailDetail_Result
		Description		: メールテンプレート詳細取得成功時処理
		Usage			: getMailDetail_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent		（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getMailDetail_Result(ReEvt:ResultEvent):Void{
		if( ReEvt.result.mRecordsAvailable > 0 ){
			//取得データセット
			GroundPath.data0_txt.text = targetData.getItemAt(targetIndex).customer_nm.substring(0,targetData.getItemAt(targetIndex).customer_nm.indexOf("(")) + "様";
			GroundPath.data1_txt.text = targetData.getItemAt(targetIndex).email;
			GroundPath.data2_txt.text = ReEvt.result.items[0].mail_from;
			GroundPath.data3_txt.text = ReEvt.result.items[0].fromaddress;
			GroundPath.data4_txt.text = ""//ReEvt.result.items[0].bccaddress;
			GroundPath.data5_txt.text = ReEvt.result.items[0].mail_subject;
			GroundPath.data6_txt.text = ReEvt.result.items[0].mail_body;
			//trace( ReEvt.result.mRecordsAvailable + "件取得しました。");
		} else {
			for(var i=0; i<6; i++ ){
				GroundPath["data" + i + "_txt"].text = "";
			}
		}

		//ページング処理呼出
		setPagingInfo()

		//ローディング用ＭＣ削除処理
		var delmc = GroundPath.CoveredStage_mc
		delmc.Loading.gotoAndPlay(2)
		var Deltween:Object = new Tween(delmc, "_alpha", Strong.easeIn, delmc._alpha, 0, 2, true);
		Deltween.delmc = delmc
		Deltween.onMotionFinished = function() {
			this.delmc.removeMovieClip();
		};

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getMailDetail_Fault
		Description		: メールテンプレート取得失敗時処理
		Usage			: getMailDetail_Result(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent	（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getMailDetail_Fault(FltEvt:FaultEvent):Void{
		GroundPath.CoveredStage_mc.removeMovieClip();
		cfunc.ShowConfirm( GroundPath , "メールテンプレートの取得に失敗しました。\rエラーメッセージ："+ FltEvt.fault.description, this , null );
		//処理を記述
		trace("getMailDetail_Fault: " + FltEvt.fault.description);
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setPagingInfo
		Description		: ページング処理
		Usage			: setPagingInfo();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/21
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function setPagingInfo(){
		var nowRecord = targetIndex;  
		var totalRecords = targetData.length;
		var Owner = this;

		//ボタン制御
		GroundPath.paging.BackBtn.enabled = true;
		GroundPath.paging.NextBtn.enabled = true;
		GroundPath.paging.LastBtn.enabled = true;
		GroundPath.paging.FirstBtn.enabled = true;

		//ページ数表示
		GroundPath.paging.page_txt.text = (nowRecord+1) + "/" + totalRecords;
		
		//ボタンクリックイベント
		GroundPath.paging.BackBtn.onRelease = function(){
			Owner.targetIndex = nowRecord - 1;
			Owner.getMailDetail();
		}
		GroundPath.paging.NextBtn.onRelease = function(){
			Owner.targetIndex = nowRecord + 1;
			Owner.getMailDetail();
		}

		GroundPath.paging.LastBtn.onRelease = function(){
			Owner.targetIndex = totalRecords - 1;
			Owner.getMailDetail();
		}

		GroundPath.paging.FirstBtn.onRelease = function(){
			Owner.targetIndex = 0;
			Owner.getMailDetail();
		}

		//ボタン制御
		GroundPath.paging.onEnterFrame = function(){
			if( nowRecord == 0 ){
				this.BackBtn.enabled = false;
				this.FirstBtn.enabled = false;
				this.BackBtn.gotoAndStop(1)
				this.FirstBtn.gotoAndStop(1)
			} else {
				this.BackBtn.enabled = true;
				this.FirstBtn.enabled = true;
			}

			if( nowRecord + 1 == totalRecords ){
				this.NextBtn.enabled = false;
				this.LastBtn.enabled = false;
				this.NextBtn.gotoAndStop(1)
				this.LastBtn.gotoAndStop(1)
			} else {
				this.NextBtn.enabled = true;
				this.LastBtn.enabled = true;
			}
		}
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: SendMail
		Description		: メール送信
		Usage			: SendTestMail( Obj );
		Attributes		: Obj : （コンファームを呼出した呼出元クラスインスタンス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function SendMail( Obj ){
		//インスタンス入替
		this = Obj

		//HTML表示で送られてしまう為送信前はHTML解除
		GroundPath.data6_txt.html = false

		//メール送信ＣＦＣ接続
		//var	MailSend = new MailSendService( GroundPath );
		var arguments1 = GroundPath.data0_txt.text
		var arguments2 = GroundPath.data1_txt.text
		var arguments3 = GroundPath.data2_txt.text
		var arguments4 = GroundPath.data3_txt.text
		var arguments5 = GroundPath.data4_txt.text
		var arguments6 = GroundPath.data5_txt.text
		var arguments7 = GroundPath.data6_txt.text
		var arguments8 = targetData.getItemAt(targetIndex).referencenumber
		var conditionParam = new Object({recieveName:arguments1,recieveAddress:arguments2,sendName:arguments3,sendAddress:arguments4,bccAddress:arguments5,subject:arguments6,body:arguments7,referencenumber:arguments8});

		//送信ファンクション呼出
		var	MailSendService = new Service( "http://" + _global.hostAddress + "/flashservices/gateway/", null , _global.servicePath + ".sendMail" , null , null ); 
		var MaildtlPC:PendingCall = MailSendService.mailSingle( conditionParam );
		MaildtlPC.responder = new RelayResponder( this, "SendMail_Result", "SendMail_Fault" );
		
		//元に戻す
		GroundPath.data6_txt.html = true
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: SendMail_Result
		Description		: メール送信ファンクション完了
		Usage			: SendMail_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent		（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function SendMail_Result(ReEvt:ResultEvent):Void{
		targetData.removeItemAt(targetIndex)
		if(targetData.length <= targetIndex){
			targetIndex--
		}
		setPagingInfo();
		if(targetData.length < 1){
			cfunc.ShowConfirm( GroundPath , "全ての送信対象へメール送信しました。\r検索画面へ戻ります。", this , null );
			GroundPath.gotoAndStop("list")
		} else {
			cfunc.ShowConfirm( GroundPath , "メールを送信しました。", this , null );
			getMailDetail();
		}
}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: SendMail_Fault
		Description		: メール送信ファンクション完了
		Usage			: SendMail_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent	（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function SendMail_Fault(FltEvt:FaultEvent):Void{
		cfunc.ShowConfirm( GroundPath , "メール送信を実行できませんでした。\r" + FltEvt.fault.description, this , null );
	}



	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: initializedData
		Description		: 初期設定
		Usage			: initializedData();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function initializedData(){
		var Owner = this;
		//GroundPath._parent.CoveredStage_mc.removeMovieClip();
		//test
		//var myConfirm:ConfirmWindow = new ConfirmWindow( GroundPath , "Your IP：\r"+ _global.UserInfo.ipAddress, this , null );

		//テンプレートコンボ設定
		if(targetData.getItemAt(targetIndex).repeater==''){
			GroundPath.template2_cb.dataProvider = mdp
			GroundPath.template2_cb.selectedIndex = midx
		}else{
			GroundPath.template2_cb.dataProvider = rmdp
			GroundPath.template2_cb.selectedIndex = rmidx
		}

		//Changeイベント
		var ComboListner = new Object();
		ComboListner.change = function(eventObj){
			Owner.MailID = eventObj.target.selectedItem.data
			var arguments1 =  Owner.MailID
			var arguments2 =  Owner.targetData.getItemAt(Owner.targetIndex).customer_nm.substring(0,Owner.targetData.getItemAt(Owner.targetIndex).customer_nm.indexOf("("))
			var arguments3 =  Owner.targetData.getItemAt(Owner.targetIndex).referencenumber
			var conditionParam = new Object({mailtemplate_id:arguments1,customer_nm:arguments2,referencenumber:arguments3});

			//ＣＦＣメソッド呼出
			var MaildtlPC:PendingCall = Owner.ServiceObject.getMailDetail( conditionParam );
			MaildtlPC.responder = new RelayResponder( Owner, "getMailDetail_Result", "getMailDetail_Fault" );
		}
		GroundPath.template2_cb.addEventListener( "change", ComboListner );
		var CategoryList = GroundPath.template2_cb.dropdown;

		//メニュー切替
		GroundPath.changeBtn.onRelease = function(){
			Owner.SetMenuList(Owner.GroundPath)
		}

		//メニュー切替
		GroundPath.backBtn.onRelease = function(){
			Owner.GroundPath.gotoAndPlay(1)
		}



		//送信ボタン設定
		GroundPath.sendBtn.onRelease = function(){
			var OKFunction = Owner.SendMail
			Owner.cfunc.ShowConfirm( this._parent , "現在の内容でメールを送信します。\rよろしいですか？", Owner , OKFunction ,0 );

		}
		
		//検索条件スタイル設定
		for(var i=0; i<5; i++){
			GroundPath[ "data" + i + "_txt" ].borderStyle = "none"
			GroundPath[ "data" + i + "_txt" ].maxChars = 255
			
		}
		GroundPath.data1_txt.restrict = "A-Za-z0-9\\-\\@\\_\\/\\#\\$\\%\\&\\.";
		GroundPath.data3_txt.restrict = "A-Za-z0-9\\-\\@\\_\\/\\#\\$\\%\\&\\.";
		GroundPath.data4_txt.restrict = "A-Za-z0-9\\-\\@\\_\\/\\#\\$\\%\\&\\.";
		var myTextFormat = new TextFormat(); 
		GroundPath.data0_txt.myTextFormat.font = "_ゴシック"; 
		GroundPath.data0_txt.setNewTextFormat(myTextFormat);
		GroundPath.data1_txt.myTextFormat.font = "_ゴシック"; 
		GroundPath.data1_txt.setNewTextFormat(myTextFormat);
		GroundPath.data2_txt.myTextFormat.font = "_ゴシック"; 
		GroundPath.data2_txt.setNewTextFormat(myTextFormat);
		GroundPath.data3_txt.myTextFormat.font = "_ゴシック"; 
		GroundPath.data3_txt.setNewTextFormat(myTextFormat);
		GroundPath.data4_txt.myTextFormat.font = "_ゴシック"; 
		GroundPath.data4_txt.setNewTextFormat(myTextFormat);
		GroundPath.data5_txt.myTextFormat.font = "_ゴシック"; 
		GroundPath.data5_txt.setNewTextFormat(myTextFormat);
		GroundPath.data6_txt.myTextFormat.font = "_ゴシック"; 
		GroundPath.data6_txt.setNewTextFormat(myTextFormat);
		GroundPath.data6_txt.borderStyle = "solid";
		GroundPath.data6_txt.borderColor = 0x68A800;

		TabInitialize();

		//詳細情報取得
		getMailDetail();
		

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: TabInitialize
		Description		: タブ設定
		Usage			: TabInitialize();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/30
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function TabInitialize():Void{
		GroundPath.tabChildren = true
		GroundPath.data0_txt.tabIndex = 2;
		GroundPath.data1_txt.tabIndex = 4;
		GroundPath.data2_txt.tabIndex = 6;
		GroundPath.data3_txt.tabIndex = 8;
		GroundPath.data4_txt.tabIndex = 10;
		GroundPath.data5_txt.tabIndex = 12;
		GroundPath.data6_txt.tabIndex = 14;
		GroundPath.sendBtn.tabIndex = 16;
		GroundPath.changeBtn.tabIndex = 18;
	}
}