﻿/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	Name			: ConfirmWindow
	Description		: コンファーム画面の表示
	Usage			: var INSTANCENAME:ConfirmWindow = new ConfirmWindow( targetPath, confirmStr, OKFunction, viewType );
	Attributes		: targetPath:MovieClip		（呼出元ターゲットパス）
					: confirmStr:String			（コンファームウインドウ内表示文字列）
					: Owner:Object				（呼出元インスタンス）
					: OKFunction:Function		（ＯＫ時処理）
					: viewType:Number			（表示タイプ	1:Alert、0:Confirm ）
	Method			: ShowConfirm
					: SetViewStyle
	Note			: none
	History			: 1) Coded by	nf)h.miyahara	2004/08/27
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
class ConfirmWindow extends MovieClip{
	var targetPath:MovieClip;		//パス
	var confirmStr:String;			//表示文字列
	var Owner:Object				//呼出元インスタンス
	var OKFunction:Function			//ＯＫ時処理
	var viewType:Number				//表示タイプ（1:Alert、0:Confirm）
	var CWDepth:Number				//Depth

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: ConfirmWindow
		Description		: Constructor
		Usage			: var INSTANCENAME:ConfirmWindow = new ConfirmWindow( targetPath, confirmStr, OKFunction, viewType );
		Attributes		: targetPath:MovieClip		（呼出元ターゲットパス）
						: confirmStr:String			（コンファームウインドウ内表示文字列）
						: Owner:Object				（呼出元インスタンス）
						: OKFunction:Function		（ＯＫ時処理）
						: viewType:Number			（表示タイプ	1:Alert、0:Confirm ）
		Method			: ShowConfirm
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/08/27
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function ConfirmWindow( myPath:MovieClip, myStr:String, myOwnr:Object, myFunc:Function, myType:Number ){
		targetPath = myPath;
		confirmStr = myStr;
		Owner = myOwnr;
		OKFunction = myFunc;
		viewType = myType;
		CWDepth = 100;
		ShowConfirm();
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: ShowConfirm
		Description		: 画面制御用ＭＣ作成＆ウインドウ表示
		Usage			: ShowConfirm();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/08/27
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function ShowConfirm():Void{

		//ボタン無効用カバーＭＣ
		targetPath.createEmptyMovieClip("Alldisable_mc", CWDepth);
		CWDepth++;
		targetPath.Alldisable_mc.beginFill(0xCCCCCC);
		targetPath.Alldisable_mc.moveTo(0,0);
		targetPath.Alldisable_mc.lineTo(Stage.width, 0);
		targetPath.Alldisable_mc.lineTo(Stage.width, Stage.height);
		targetPath.Alldisable_mc.lineTo(0, Stage.height);
		targetPath.Alldisable_mc.lineTo(0, 0);
		targetPath.Alldisable_mc.endFill();
		targetPath.Alldisable_mc._alpha = 40;
		targetPath.Alldisable_mc.enabled = false
		targetPath.Alldisable_mc.onRollOver = function(){}
		targetPath.Alldisable_mc.onEnterFrame = function(){
			if( this._parent.confirmwindow == undefined ){
				this.removeMovieClip();
			}
		}
		
		//Confirm表示
		targetPath.attachMovie("confirm","confirmwindow",CWDepth,{_x:Stage.width/2,_y:Stage.height/2});
		CWDepth++
		targetPath.confirmwindow._x = targetPath.confirmwindow._x-targetPath.confirmwindow._width/2;
		targetPath.confirmwindow._y = targetPath.confirmwindow._y-targetPath.confirmwindow._height;
		targetPath.confirmwindow.confirm_txt.text = confirmStr;
		
		SetViewStyle();

	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: SetViewStyle
		Description		: 表示タイプ切替（viewType	1:Alert、0:Confirm）
		Usage			: SetViewStyle(type);
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/08/27
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function SetViewStyle(){
		if( OKFunction == null ){ //Alert

			//実行時処理
			targetPath.confirmwindow.OKBtn.owner = this;
			targetPath.confirmwindow.OKBtn.onRelease = function(){
				this.owner.sweep();
			}
			
			targetPath.confirmwindow.OKBtn._x = 109;
			targetPath.confirmwindow.CancelBtn.enabled = false;
			targetPath.confirmwindow.CancelBtn._visible = false;

		} else { //Confirm

			//実行時処理
			targetPath.confirmwindow.OKBtn.owner = this;
			targetPath.confirmwindow.OKBtn.onRelease = function(){
				this.owner.OKFunction( this.owner.Owner );
				this.owner.sweep();
			}
			
			//キャンセル時処理（共通）
			targetPath.confirmwindow.CancelBtn.owner = this;
			targetPath.confirmwindow.CancelBtn.onRelease = function(){
				this.owner.sweep();
				//this._parent.removeMovieClip();
			}

		}
		
	}
	
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		コンファーム状態解除
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function sweep(){
		targetPath.confirmwindow.removeMovieClip();
	}

}
