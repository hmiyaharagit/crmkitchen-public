﻿/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	Name			: mailsinglepanel
	Description		: メールテンプレート情報取得
	Usage			: var XXXXXX:mailsinglepanel = new mailsinglepanel( Path );
	Attributes		: Path:MovieClip	（基準パス）
	ExtendsParam	: none
	Method			: 
	Note			: none
	History			: 1) Coded by	nf)h.miyahara	2004/07/09
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
import mx.remoting.Service;
import mx.services.Log;
import mx.rpc.RelayResponder;
import mx.remoting.PendingCall;
import mx.remoting.debug.NetDebug;
import mx.remoting.debug.NetDebugConfig;
import mx.rpc.ResultEvent;
import mx.rpc.FaultEvent;
import mx.transitions.easing.*;
import mx.transitions.Tween;
import mx.controls.Alert;
import mx.controls.ComboBox
import commonfunc

class app00.mailsinglepanel extends MovieClip {

	var gpath:MovieClip			//基準ムービークリップパス
	var gdepth:Number					//Depth
	var targetIndex:Number				//対象ＩＮＤＥＸ
	var targetData:Object				//対象データプロバイダ
	var sobject:Object			//CFCsobject
	var MailID:Number					//mailtemlate_id
	var rMailID:Number					//mailtemlate_id
	var rmdp:Array					
	var rmidx:Number					
	var mdp:Array					
	var midx:Number					
	var cfunc:commonfunc
	var parentobj:app00.MailPackageService

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: mailsinglepanel
		Description		: Constructor
		Usage			: mailsinglepanel( myPath:MovieClip );
		Attributes		: myPath:MovieClip	（基準ＭＣパス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function mailsinglepanel() {
		var dataLength = _global.targetData.length
		gpath = this;
		targetIndex = _global.targetIndex;			//MailTemplateServiceクラスで設定
		targetData = _global.targetData;			//MailTemplateServiceクラスで設定
		MailID = _global.mailTemplateIndex;			//
		rMailID = _global.mailrTemplateIndex;		//

		mdp=parentobj.gpath.template_cb.dataProvider
		midx=parentobj.gpath.template_cb.selectedIndex

		rmdp = parentobj.gpath.rtemplate_cb.dataProvider
		rmidx=parentobj.gpath.rtemplate_cb.selectedIndex

		gdepth = gpath.getNextHighestDepth();
		cfunc = new commonfunc()

		var myDebug = mx.remoting.debug.NetDebug.initialize();
		sobject = new Service( "http://" + _global.hostAddress + "/flashservices/gateway/", null , _global.servicePath + ".app00.cfc.mailTemplate" , null , null ); 
		
		//NetDebbuger設定
		var config:NetDebugConfig = sobject.connection.getDebugConfig();
		config.app_server.trace = true;
		config.client.trace = true;

		//ＭＣ初期化
		initializedData();
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getMailDetail
		Description		: メールテンプレート詳細情報取得
		Usage			: getMailDetail( mail_id );
		Attributes		: mail_id:Number	（メールテンプレートＩＤ）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function getMailDetail(){
		var conditionParam = new Object()
		conditionParam.mailtemplate_id = MailID
		var conditionParam = new Object()
		if(targetData.getItemAt(targetIndex).repeater==''){
			conditionParam.mailtemplate_id = MailID
			gpath.template2_cb.dataProvider = mdp
			gpath.template2_cb.selectedIndex = midx

			trace("通常：" + targetData.getItemAt(targetIndex).repeater)
			trace("通常：" + mdp.length)
			trace("通常：" + midx)

		}else{
			conditionParam.mailtemplate_id = rMailID
			gpath.template2_cb.dataProvider = rmdp
			gpath.template2_cb.selectedIndex = rmidx
			trace("りぷ：" + targetData.getItemAt(targetIndex).repeater)
			trace("りぷ：" + rmdp.length)
			trace("りぷ：" + rmidx)
		}

		conditionParam.customer_nm = targetData.getItemAt(targetIndex).customer_nm
		conditionParam.referencenumber = targetData.getItemAt(targetIndex).referencenumber
		//ＣＦＣメソッド呼出
		var MaildtlPC:PendingCall = sobject.getMailDetail( conditionParam );
		MaildtlPC.responder = new RelayResponder( this, "getMailDetail_Result", "getMailDetail_Fault" );
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getMailDetail_Result
		Description		: メールテンプレート詳細取得成功時処理
		Usage			: getMailDetail_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent		（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getMailDetail_Result(ReEvt:ResultEvent):Void{
		if( ReEvt.result.mRecordsAvailable > 0 ){
			//取得データセット
			//gpath.data0_txt.text = targetData.getItemAt(targetIndex).customer_nm.substring(0,targetData.getItemAt(targetIndex).customer_nm.indexOf("(")) + "様";
			gpath.data0_txt.text = targetData.getItemAt(targetIndex).customer_nm + "様";
			gpath.data1_txt.text = targetData.getItemAt(targetIndex).email;
			gpath.data2_txt.text = ReEvt.result.items[0].mail_from;
			gpath.data3_txt.text = ReEvt.result.items[0].fromaddress;
			gpath.data4_txt.text = ""//ReEvt.result.items[0].bccaddress;
			gpath.data5_txt.text = ReEvt.result.items[0].mail_subject;
			gpath.data6_txt.text = ReEvt.result.items[0].mail_body;
			//trace( ReEvt.result.mRecordsAvailable + "件取得しました。");
		} else {
			for(var i=0; i<6; i++ ){
				gpath["data" + i + "_txt"].text = "";
			}
		}

		//ページング処理呼出
		setPagingInfo()


		gpath._x = 0
		cfunc.loadingdelete(gpath._parent)
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getMailDetail_Fault
		Description		: メールテンプレート取得失敗時処理
		Usage			: getMailDetail_Result(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent	（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getMailDetail_Fault(FltEvt:FaultEvent):Void{
		gpath.CoveredStage_mc.removeMovieClip();
		mx.controls.Alert.show("メールテンプレートの取得に失敗しました。\r：" + FltEvt.fault.description , "                                   ");
		//処理を記述
		trace("getMailDetail_Fault: " + FltEvt.fault.description);
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setPagingInfo
		Description		: ページング処理
		Usage			: setPagingInfo();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/21
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function setPagingInfo(){
		var nowRecord = targetIndex;  
		var totalRecords = targetData.length;
		var Owner = this;

		//ボタン制御
		gpath.paging.BackBtn.enabled = true;
		gpath.paging.NextBtn.enabled = true;
		gpath.paging.LastBtn.enabled = true;
		gpath.paging.FirstBtn.enabled = true;

		//ページ数表示
		gpath.paging.page_txt.text = (nowRecord+1) + "/" + totalRecords;
		
		//ボタンクリックイベント
		gpath.paging.BackBtn.onRelease = function(){
			Owner.targetIndex = nowRecord - 1;
			Owner.getMailDetail();
		}
		gpath.paging.NextBtn.onRelease = function(){
			Owner.targetIndex = nowRecord + 1;
			Owner.getMailDetail();
		}

		gpath.paging.LastBtn.onRelease = function(){
			Owner.targetIndex = totalRecords - 1;
			Owner.getMailDetail();
		}

		gpath.paging.FirstBtn.onRelease = function(){
			Owner.targetIndex = 0;
			Owner.getMailDetail();
		}

		//ボタン制御
		gpath.paging.onEnterFrame = function(){
			if( nowRecord == 0 ){
				this.BackBtn.enabled = false;
				this.FirstBtn.enabled = false;
				this.BackBtn.gotoAndStop(1)
				this.FirstBtn.gotoAndStop(1)
			} else {
				this.BackBtn.enabled = true;
				this.FirstBtn.enabled = true;
			}

			if( nowRecord + 1 == totalRecords ){
				this.NextBtn.enabled = false;
				this.LastBtn.enabled = false;
				this.NextBtn.gotoAndStop(1)
				this.LastBtn.gotoAndStop(1)
			} else {
				this.NextBtn.enabled = true;
				this.LastBtn.enabled = true;
			}
		}
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: SendMail
		Description		: メール送信
		Usage			: SendTestMail( Obj );
		Attributes		: Obj : （コンファームを呼出した呼出元クラスインスタンス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function SendMail( Obj ){
		//インスタンス入替
		this = Obj

		//HTML表示で送られてしまう為送信前はHTML解除
		gpath.data6_txt.html = false

		//メール送信ＣＦＣ接続
		//var	MailSend = new MailSendService( gpath );
		var conditionParam = new Object();
		conditionParam.recieveName = gpath.data0_txt.text
		conditionParam.recieveAddress = gpath.data1_txt.text
		conditionParam.sendName = gpath.data2_txt.text
		conditionParam.sendAddress = gpath.data3_txt.text
		conditionParam.bccAddress = gpath.data4_txt.text
		conditionParam.subject = gpath.data5_txt.text
		conditionParam.body = gpath.data6_txt.text
		conditionParam.referencenumber = targetData.getItemAt(targetIndex).referencenumber
		conditionParam.mail_id = MailID

		//送信ファンクション呼出
		var	MailSendService = new Service( "http://" + _global.hostAddress + "/flashservices/gateway/", null , _global.servicePath + ".app00.cfc.sendMail" , null , null ); 
		var MaildtlPC:PendingCall = MailSendService.mailSingle( conditionParam );
		MaildtlPC.responder = new RelayResponder( this, "SendMail_Result", "SendMail_Fault" );
		
		//元に戻す
		gpath.data6_txt.html = true
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: SendMail_Result
		Description		: メール送信ファンクション完了
		Usage			: SendMail_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent		（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function SendMail_Result(ReEvt:ResultEvent):Void{
		targetData.removeItemAt(targetIndex)
		if(targetData.length <= targetIndex){
			targetIndex--
		}
		setPagingInfo();
		if(targetData.length < 1){
			var AlertObj = mx.controls.Alert.show("全ての送信対象へメール送信しました。\r検索画面へ戻ります。", "送信完了            ", Alert.OK, gpath, null, null , Alert.OK);
			this.removeMovieClip();
			//gpath.gotoAndStop("list")
		} else {
			var AlertObj = mx.controls.Alert.show("メールを送信しました。", "送信完了            ", Alert.OK, gpath, null, null , Alert.OK);
			getMailDetail();
		}
}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: SendMail_Fault
		Description		: メール送信ファンクション完了
		Usage			: SendMail_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent	（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function SendMail_Fault(FltEvt:FaultEvent):Void{
		mx.controls.Alert.show("メール送信を実行できませんでした。\r：" + FltEvt.fault.description , "                                   ");
	}



	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: initializedData
		Description		: 初期設定
		Usage			: initializedData();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function initializedData(){
		var Owner = this;

		//	--閉じる
		gpath.closebtn.onRelease = function(){
			this._parent.removeMovieClip();
		}

		gpath.createObject("ComboBox","template2_cb",gdepth++,{_x:271.6,_y:88.5});
		gpath.template2_cb.setSize(220,22)

		//Changeイベント
		var ComboListner = new Object();
		ComboListner.change = function(eventObj){
			Owner.MailID = eventObj.target.selectedItem.data
			var arguments1 =  Owner.MailID
			//var arguments2 =  Owner.targetData.getItemAt(Owner.targetIndex).customer_nm.substring(0,Owner.targetData.getItemAt(Owner.targetIndex).customer_nm.indexOf("("))
			var arguments2 =  Owner.targetData.getItemAt(Owner.targetIndex).customer_nm
			var arguments3 =  Owner.targetData.getItemAt(Owner.targetIndex).referencenumber
			var conditionParam = new Object({mailtemplate_id:arguments1,customer_nm:arguments2,referencenumber:arguments3});

			//ＣＦＣメソッド呼出
			var MaildtlPC:PendingCall = Owner.sobject.getMailDetail( conditionParam );
			MaildtlPC.responder = new RelayResponder( Owner, "getMailDetail_Result", "getMailDetail_Fault" );
		}
		gpath.template2_cb.addEventListener( "change", ComboListner );
		gpath.template2_cb.dropdown;

		//テンプレートコンボ設定
		if(targetData.getItemAt(targetIndex).repeater==''){
			gpath.template2_cb.dataProvider = mdp
			gpath.template2_cb.selectedIndex = midx
		}else{
			gpath.template2_cb.dataProvider = rmdp
			gpath.template2_cb.selectedIndex = rmidx
		}

		gpath.changeBtn.onRelease = function(){
			Owner.SetMenuList(Owner.gpath)
		}
		gpath.backBtn.onRelease = function(){
			Owner.gpath.gotoAndPlay(1)
		}

		//送信イベント
		var sendhandler = new Object();
		sendhandler = function(evt){
			if(evt.detail == Alert.YES){
				//HTML表示で送られてしまう為送信前はHTML解除
				Owner.gpath.data6_txt.html = false

				//メール送信ＣＦＣ接続
				//var	MailSend = new MailSendService( gpath );
				var conditionParam = new Object();
				conditionParam.recieveName = Owner.gpath.data0_txt.text
				conditionParam.recieveAddress =Owner. gpath.data1_txt.text
				conditionParam.sendName = Owner.gpath.data2_txt.text
				conditionParam.sendAddress = Owner.gpath.data3_txt.text
				conditionParam.bccAddress = Owner.gpath.data4_txt.text
				conditionParam.subject = Owner.gpath.data5_txt.text
				conditionParam.body = Owner.gpath.data6_txt.text
				conditionParam.referencenumber = Owner.targetData.getItemAt(Owner.targetIndex).referencenumber
				conditionParam.mail_id = Owner.MailID

				//送信ファンクション呼出
				var	MailSendService = new Service( "http://" + _global.hostAddress + "/flashservices/gateway/", null , _global.servicePath + ".app00.cfc.sendMail" , null , null ); 
				var MaildtlPC:PendingCall = MailSendService.mailSingle( conditionParam );
				MaildtlPC.responder = new RelayResponder( Owner, "SendMail_Result", "SendMail_Fault" );
				
				//元に戻す
				Owner.gpath.data6_txt.html = true
			}
		}

		//登録イベント
		var sethandler = new Object();
		sethandler = function(evt){
			if(evt.detail == Alert.YES){
				//メール送信ＣＦＣ接続
				var conditionParam = new Object();
				conditionParam.mail_body = Owner.gpath.data6_txt.text
				conditionParam.referencenumber = Owner.targetData.getItemAt(Owner.targetIndex).referencenumber
				conditionParam.mailtemplate_id = Owner.MailID

				var MaildtlPC:PendingCall = Owner.sobject.setcustomizedmail( conditionParam );
				MaildtlPC.responder = new RelayResponder( Owner, "setcustomizedmail_Result", "getMailDetail_Fault" );
			}
		}

		//送信
		gpath.sendBtn.onRelease = function(){
			var AlertObj = mx.controls.Alert.show("現在の内容でメールを送信します。\rよろしいですか？", "メール送信", Alert.YES | Alert.NO, Owner.gpath, sendhandler, null , Alert.OK);
		}

		//カスタマイズ登録
		gpath.registBtn.onRelease = function(){
			var AlertObj = mx.controls.Alert.show("現在の内容でメールを登録します。\rよろしいですか？", "メール登録", Alert.YES | Alert.NO, Owner.gpath, sethandler, null , Alert.OK);
		}

		//検索条件スタイル設定
		for(var i=0; i<6; i++){
			gpath[ "data" + i + "_txt" ].borderStyle = "none"
			gpath[ "data" + i + "_txt" ].maxChars = 255
			
		}
		gpath.data1_txt.restrict = "A-Za-z0-9\\-\\@\\_\\/\\#\\$\\%\\&\\.";
		gpath.data3_txt.restrict = "A-Za-z0-9\\-\\@\\_\\/\\#\\$\\%\\&\\.";
		gpath.data4_txt.restrict = "A-Za-z0-9\\-\\@\\_\\/\\#\\$\\%\\&\\.";
		var myTextFormat = new TextFormat(); 
		gpath.data0_txt.myTextFormat.font = "_ゴシック"; 
		gpath.data0_txt.setNewTextFormat(myTextFormat);
		gpath.data1_txt.myTextFormat.font = "_ゴシック"; 
		gpath.data1_txt.setNewTextFormat(myTextFormat);
		gpath.data2_txt.myTextFormat.font = "_ゴシック"; 
		gpath.data2_txt.setNewTextFormat(myTextFormat);
		gpath.data3_txt.myTextFormat.font = "_ゴシック"; 
		gpath.data3_txt.setNewTextFormat(myTextFormat);
		gpath.data4_txt.myTextFormat.font = "_ゴシック"; 
		gpath.data4_txt.setNewTextFormat(myTextFormat);
		gpath.data5_txt.myTextFormat.font = "_ゴシック"; 
		gpath.data5_txt.setNewTextFormat(myTextFormat);
		gpath.data6_txt.myTextFormat.font = "_ゴシック"; 
		gpath.data6_txt.setNewTextFormat(myTextFormat);
		gpath.data6_txt.borderColor = 0x68A800;

		TabInitialize();

		//詳細情報取得
		getMailDetail();
		

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: TabInitialize
		Description		: タブ設定
		Usage			: TabInitialize();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/30
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function TabInitialize():Void{
		gpath.tabChildren = true
		gpath.data0_txt.tabIndex = 2;
		gpath.data1_txt.tabIndex = 4;
		gpath.data2_txt.tabIndex = 6;
		gpath.data3_txt.tabIndex = 8;
		gpath.data4_txt.tabIndex = 10;
		gpath.data5_txt.tabIndex = 12;
		gpath.data6_txt.tabIndex = 14;
		gpath.sendBtn.tabIndex = 16;
		gpath.changeBtn.tabIndex = 18;
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setcustomizedmail
		Description		: メール登録
		Usage			: setcustomizedmail( Obj );
		Attributes		: Obj : （コンファームを呼出した呼出元クラスインスタンス）
		Note			: none
		History			: 1) Coded by 2007/06/25 15:53
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function setcustomizedmail( Obj ){
		//インスタンス入替
		this = Obj

		//メール送信ＣＦＣ接続
		//var	MailSend = new MailSendService( gpath );
		var conditionParam = new Object();
		conditionParam.mail_body = gpath.data6_txt.text
		conditionParam.referencenumber = targetData.getItemAt(targetIndex).referencenumber
		conditionParam.mailtemplate_id = MailID

		var MaildtlPC:PendingCall = sobject.setcustomizedmail( conditionParam );
		MaildtlPC.responder = new RelayResponder( this, "setcustomizedmail_Result", "getMailDetail_Fault" );

	}
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setcustomizedmail_Result
		Description		: ファンクション完了
		Usage			: setcustomizedmail_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent		（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function setcustomizedmail_Result(ReEvt:ResultEvent):Void{
		mx.controls.Alert.show("メールを保存しました" , "メール登録完了                                  ");
	}
}