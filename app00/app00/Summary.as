﻿/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	Name			: Summary
	Extends			: FRConnection
	Description		: サマリ情報取得～表示
	Usage			: var INSTANCENAME:Summary = new Summary( hostaddress, cfcclasspath, cfcomponentname, targetPath );
	Attributes		: hostaddress:String		（ホストアドレス名）
					: cfcclasspath:String		（ＣＦＣクラスパス）
					: cfcomponentname:String	（ＣＦコンポーネント名）
					: targetPath:MovieClip		（ＭＣアタッチ先パス）
	Extends			: hostaddress:String
					: classPath:String
					: className:String
	Method			: none
	Note			: none
	History			: 1) Coded by	nf)h.miyahara	2004/07/09
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
/**/
import mx.remoting.Service;
import mx.services.Log;
import mx.rpc.RelayResponder;
import mx.remoting.PendingCall;
import mx.remoting.RecordSet;
import mx.remoting.debug.NetDebug;
import mx.remoting.debug.NetDebugConfig;
import mx.rpc.ResultEvent;
import mx.rpc.FaultEvent;

class Summary extends FRConnections {
	var targetPath:MovieClip	//パス
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Constructor
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function Summary(  myHost:String, myPath:String, myName:String, myTarget:MovieClip ){
		_global.userInfo = new Object();
		hostAddress = myHost;
		classPath = myPath;
		className = myName;
		targetPath = myTarget
		
		/*
		
		var myDebug = mx.remoting.debug.NetDebug.initialize();
		var myService:Service = new Service( "http://" + hostAddress + "/flashservices/gateway", null , classPath + "." + className , null , null ); 
		ServiceObject = myService
		
		//NetDebbuger設定
		var config:NetDebugConfig = myService.connection.getDebugConfig();
		config.app_server.trace = true;
		config.client.trace = true;

		//CFCメソッド呼出
		var QuestionPC:PendingCall = myService.doQry3(); 
		QuestionPC.responder = new RelayResponder( this, "getQ_Result", "getQ_Fault" );
		*/
		//テストデータセット
		TestDataSet();
		
	}

	function TestDataSet(){
		var y_margin = 80
		for(var i=0; i<13; i++){
			targetPath.attachMovie( "CB_Item_" + i, "CB_Item_" + i + "_mc" ,1000+i,{_y:y_margin} )
			//targetPath["CB_Item_" + i + "_mc"].data_txt.text = "CB_Item_" + i + "_mc"
			y_margin = y_margin + targetPath["CB_Item_" + i + "_mc"]._height
		}
		var groundPath = targetPath._parent._parent;
		var dp_array = new Array();
		dp_array.push({data:0,label:"全表示"})
		dp_array.push({data:1,label:"レストラン"})
		dp_array.push({data:2,label:"予約"})
		groundPath.summary_cb.dataProvider = dp_array;
		
		//表示切替処理実行
		groundPath.changesummaryBtn.summaryType = 0
		groundPath.changesummaryBtn.targetPath = targetPath
		groundPath.changesummaryBtn.onRelease = function(){
			this.targetPath.data_txt.text = this._parent.summary_cb.selectedItem.label
			var index = this.summaryType-1
		
			var temp_items = new Array();
			temp_items.push({summary_id:1,basicinfo:"1,2"})
			temp_items.push({summary_id:2,basicinfo:"1,2,3,4,6"})
			var myArray = temp_items[index].basicinfo.split(",")
			
			if( this.summaryType == 0 ){
				var y_margin = 80
				for(var i=0; i<13; i++){
					this.targetPath["CB_Item_" + i + "_mc"]._visible = true;
					this.targetPath["CB_Item_" + i + "_mc"]._y = y_margin;
					y_margin = y_margin+this.targetPath["CB_Item_" + i + "_mc"]._height
				}
			} else {
				for(var i=0; i<13; i++){
					this.targetPath["CB_Item_" + i + "_mc"]._visible = false;
				}
				var y_margin = 80
				for(var i=0; i<myArray.length; i++){
					this.targetPath["CB_Item_" + myArray[i] + "_mc"]._visible = true;
					this.targetPath["CB_Item_" + myArray[i] + "_mc"]._y = y_margin;
					y_margin = y_margin+this.targetPath["CB_Item_" + myArray[i] + "_mc"]._height
				}

			}
		}

		var summaryCBListener = new Object(); 
		summaryCBListener.groundPath = groundPath
		summaryCBListener.change = function(eventObj){
			groundPath.changesummaryBtn.summaryType = eventObj.target.value;
		}
		groundPath.summary_cb.addEventListener("change", summaryCBListener);
	}


	
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		ログイン処理成功時処理
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function getQ_Result(ReEvt:ResultEvent):Void{
		setSummaryCombo(ReEvt);
		trace( ReEvt.result.mRecordsAvailable + "件取得しました。"); 

		var y_margin = 80
		for(var i=0; i<13; i++){
			targetPath.attachMovie( "CB_Item_" + i, "CB_Item_" + i + "_mc" ,1000+i,{_y:y_margin} )
			targetPath["CB_Item_" + i + "_mc"].data_txt.text = "CB_Item_" + i + "_mc"
			y_margin = y_margin + targetPath["CB_Item_" + i + "_mc"]._height

		}

/*

		targetPath.attachMovie("QuestionBlock","QuestionBlock_0",100)
		targetPath.QuestionBlock_0.name_txt.text = ReEvt.result.items[0].questionnairecategory_nm

		var SetCategory = ReEvt.result.items[0].questionnairecategory_nm
		var y_margin = targetPath.QuestionBlock_0._height
		for(var i=0; i<ReEvt.result.mRecordsAvailable; i++){
			if( SetCategory != ReEvt.result.items[i].questionnairecategory_nm){
				targetPath.attachMovie("QuestionBlock","QuestionBlock_"+i ,100+i,{_y:y_margin})
				targetPath["QuestionBlock_" + i].name_txt.text = ReEvt.result.items[i].questionnairecategory_nm
				y_margin = y_margin+targetPath["QuestionBlock_" + i]._height
				SetCategory = ReEvt.result.items[i].questionnairecategory_nm
			}
			
			targetPath.attachMovie("Question","Question_"+i ,300+i,{_y:y_margin})
			targetPath["Question_" + i].name_txt.text = ReEvt.result.items[i].questionnaire_nm
			targetPath["Question_" + i].id_txt.text = ReEvt.result.items[i].questionnaire_id
			y_margin = y_margin+targetPath["Question_" + i]._height
		}
*/
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		検索実行失敗時処理
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function getQ_Fault(FltEvt:FaultEvent):Void{
		//処理を記述
		trace("Categories Fault: " + FltEvt.fault.description);
		setMessage(0,"ログイン失敗");
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		表示切替コンボボックス設定
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function setSummaryCombo(ReEvt:ResultEvent){

		var groundPath = targetPath._parent._parent;
		var dp_array = new Array();
		dp_array.push({data:0,label:"全表示"})
		for(var i=0; i<ReEvt.result.mRecordsAvailable; i++){
			dp_array.push({data:ReEvt.result.items[i].summary_id,label:ReEvt.result.items[i].summary_nm})
		}
		
		groundPath.summary_cb.dataProvider = dp_array;

		//表示切替処理実行
		groundPath.changesummaryBtn.summaryType = 0
		groundPath.changesummaryBtn.targetPath = targetPath
		groundPath.changesummaryBtn.onRelease = function(){
			this.targetPath.data_txt.text = this._parent.summary_cb.selectedItem.label
			var index = this.summaryType-1
			var myArray = ReEvt.result.items[index].basicinfo.split(",")
			
			if( this.summaryType == 0 ){
				var y_margin = 80
				for(var i=0; i<13; i++){
					this.targetPath["CB_Item_" + i + "_mc"]._visible = true;
					this.targetPath["CB_Item_" + i + "_mc"]._y = y_margin;
					y_margin = y_margin+this.targetPath["CB_Item_" + i + "_mc"]._height

				}
			} else {
				for(var i=0; i<13; i++){
					this.targetPath["CB_Item_" + i + "_mc"]._visible = false;
				}
				var y_margin = 80
				for(var i=0; i<myArray.length; i++){
					this.targetPath["CB_Item_" + myArray[i] + "_mc"]._visible = true;
					this.targetPath["CB_Item_" + myArray[i] + "_mc"]._y = y_margin;
					y_margin = y_margin+this.targetPath["CB_Item_" + myArray[i] + "_mc"]._height
				}

			}
		}
		var summaryCBListener = new Object(); 
		summaryCBListener.groundPath = groundPath
		summaryCBListener.change = function(eventObj){
			groundPath.changesummaryBtn.summaryType = eventObj.target.value;
		}
		groundPath.summary_cb.addEventListener("change", summaryCBListener);
	}


}
