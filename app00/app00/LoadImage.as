﻿/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	Name			: LoadImage
	Description		: 指定ＭＣへの画像読込
	Usage			: var XXXXXX:LoadImage = new LoadImage( imagefile, targetmc );
	Attributes		: imagefile:String					（読込み画像ファイル名）
					: targetmc:MovieClip				（読込みベースMovieClip）
	Method			: instanceName.deleteClip()			（読込みベースMovieClip）
	Note			: none
	History			: 1) Coded by	nf)h.miyahara	2004/07/09
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
class LoadImage {
	var imgFile:String;
	var tgtMC:MovieClip;
	var mcLoader:MovieClipLoader;
	
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		UnLoad
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function deleteClip(){
		mcLoader.unloadClip(tgtMC)
	}
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Constructor
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function LoadImage( loadimg:String, targetMC:MovieClip ){
		imgFile = loadimg;
	    tgtMC = targetMC;
		var GroundPath = targetMC._parent
		var evtListener = new Object();
		var tmp_mcLoader = new MovieClipLoader();
		mcLoader = tmp_mcLoader;
		mcLoader.loadClip( imgFile, tgtMC );
		mcLoader.addListener( evtListener );
		evtListener.GroundPath = GroundPath
		evtListener.onLoadStart = function (target_mc){
			var LIDepth = 100;
			this.GroundPath.createEmptyMovieClip("CoveredStage_mc", LIDepth);
			LIDepth++;
			this.GroundPath.CoveredStage_mc.beginFill(0x999999);
			this.GroundPath.CoveredStage_mc.moveTo(0,0);
			this.GroundPath.CoveredStage_mc.lineTo(Stage.width, 0);
			this.GroundPath.CoveredStage_mc.lineTo(Stage.width, Stage.height);
			this.GroundPath.CoveredStage_mc.lineTo(0, Stage.height);
			this.GroundPath.CoveredStage_mc.lineTo(0, 0);
			this.GroundPath.CoveredStage_mc.endFill();
			this.GroundPath.CoveredStage_mc._alpha = 40;
			this.GroundPath.CoveredStage_mc.enabled = false
			this.GroundPath.CoveredStage_mc.onRollOver = false
			this.GroundPath.CoveredStage_mc.attachMovie("Loading","Loading",LIDepth,{_x:Stage.width*0.5,_y:Stage.height*0.5})
			LIDepth++;
			//trace(target_mc + "にロードが開始されました。")
		}
		
		evtListener.onLoadProgress = function (target_mc, loadedBytes, totalBytes){
			//trace("総数" + totalBytes + "を"+ target_mc + "に" + loadedBytes + "ロード中･･･")
		}
		
		evtListener.onLoadComplete = function (target_mc){
			//trace("ロード完了")
		}
		
		evtListener.onLoadInit = function (target_mc)
		{
			mcLoader.unloadClip(target_mc);
			//trace("ロード初期化");
		} 

		evtListener.onLoadError = function (target_mc, errorCode) 
		{
			//trace("ロードが不完全で完了しました。")
			//trace ("ERROR CODE = " + errorCode);
			//trace ("Your load failed on movie clip = " + target_mc + "n");
		} 
	}


}
