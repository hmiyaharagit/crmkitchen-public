﻿/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	Name			: flpaper
	Description		: 
	Usage			: 
	Attapp00utes		: 
	Extends			: 
	Method			: 
	Note			: none
	History			: 1) Coded by	2007/09/03
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
import mx.remoting.Service;
import mx.services.Log;
import mx.rpc.RelayResponder;
import mx.remoting.PendingCall;
import mx.remoting.debug.NetDebug;
import mx.remoting.debug.NetDebugConfig;
import mx.remoting.RecordSet;
import mx.rpc.ResultEvent;
import mx.rpc.FaultEvent;
import mx.controls.ComboBox
import mx.data.binding.*;
import mx.data.components.DataSet
import mx.controls.Alert;
import commonfunc

class app00.flpaper extends MovieClip {
	var gpath:MovieClip			//基準ムービークリップパス
	var gdepth:Number			//depth
	var sobject:Object			//CFCsobject
	var resort_code:Number		//リゾートコード（0:共通）
	var cfunc:commonfunc
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: flpaper
		Description		: Constructor
		Usage			: flpaper( yPath:MovieClip );
		Attapp00utes		:
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function flpaper(myPath:MovieClip,r_id:Number) {	
		gpath = myPath;
		gdepth = 1000;
		resort_code = r_id;
		var myDebug = mx.remoting.debug.NetDebug.initialize();
		sobject = new Service( "http://" + _global.hostAddress + "/flashservices/gateway/", null , _global.servicePath + ".app00.cfc.commonfunction" , null , null ); 
		//NetDebbuger設定
		var config:NetDebugConfig = sobject.connection.getDebugConfig();
		config.app_server.trace = true;
		config.client.trace = true;
		cfunc = new commonfunc()
		//ＭＣ初期化
		initializedData();
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: initializedData
		Description		: 
		Usage			:
		Attapp00utes		:
		Note			: none
		History			: 1) Coded by 
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function initializedData(){
		gpath._x = 0

		var conditionParam = new Object();
		var QestinPC:PendingCall = sobject.getqsheet(resort_code);
		QestinPC.responder = new RelayResponder( this, "getqsheet_Result", "Service_Fault" );
		var loadNotifier:Object = new Object;
		loadNotifier.onLoaded = function(fp:Object):Void
		{
			fp.showUIElement("Sidebar", false);
			fp.addListener(this);
		};
	}
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getqsheet_Result
		Description		: 
		Usage			: getqsheet_Result(ReEvt:ResultEvent);
		Attapp00utes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getqsheet_Result(ReEvt:ResultEvent):Void{
		var owner =this;
		var ComboData = new Array();
		for(var i=0; i<ReEvt.result.mRecordsAvailable; i++){
			ComboData.push({	 data:ReEvt.result.items[i].questionnairesheet_id
								,label:ReEvt.result.items[i].questionnairesheet_nm
								,name:ReEvt.result.items[i].questionnaire_nm
								,resort_code:ReEvt.result.items[i].resort_code
							})
		}
		gpath.sheet_cb.dataProvider = ComboData;
		gpath.sheet_cb.dropdown;
		gpath.viewbtn.onRelease = function(){
			owner.gpath.removeMovieClip("fileloader_mc");
			owner.gpath.attachMovie("fileloader","fileloader_mc",gdepth++,{_x:0,_y:40.0})
			//var filenm = "app00/file/csreport" + owner.gpath.sheet_cb.selectedItem.resort_code + "_" + owner.gpath.sheet_cb.selectedItem.data + ".swf"
			//owner.loadFlashPaper(filenm,owner.gpath.fileloader_mc,owner.loadNotifier);
			var filenm = "app00/file/csreport" + owner.gpath.sheet_cb.selectedItem.resort_code + "_" + owner.gpath.sheet_cb.selectedItem.data + ".pdf"
			_root.getURL(filenm,"_blank")
		}
		gpath._x = 0
		cfunc.loadingdelete(gpath._parent)
	}
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: initializedData
		Description		: 
		Usage			:
		Attapp00utes		:
		Note			: none
		History			: 1) Coded by 
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function loadFlashPaper(
			path_s		// 読み込む SWF ファイルのパス
		   ,dest_mc	// その SWF と入れ替えるムービークリップ（MC)
//		   ,width_i	// dest MC の新しいサイズ
//		   ,height_i	// dest MC の新しいサイズ
		   ,loaded_o	// オプション： 読み込み完了の通達先オブジェクト){
		)
	{
		var intervalID = 0;
		var width_i = 1000;
		var height_i = 520;
		var loadFunc = function()
		{

			dest_mc._visible = false;
			var fp = dest_mc.getIFlashPaper();
			if (!fp)
				return;
			if (fp.setSize(width_i, height_i) == false)
				return;

			dest_mc._visible = true;
			clearInterval(intervalID);
			fp.showUIElement("Sidebar", false);
			loaded_o.onLoaded(fp);
		}
		intervalID = setInterval(loadFunc, 120);
		dest_mc.loadMovie(path_s);
	}
}
