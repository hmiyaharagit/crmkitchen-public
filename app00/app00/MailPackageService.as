﻿/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	Name			: MailPackageService
	Description		: メール一括送信クラス
	Usage			: var XXXXXX:MailPackageService = new MailPackageService( datagridname, path );
	Attributes		: datagridname:DataGrid		（結果格納用データグリッドコンポーネント名）
					: path:MovieClip			（基準パス）
	ExtendsParam	: sobject:Service
	Method			: initializedData		初期設定
					: getGuestList_Result	検索実行正常終了時イベント
					: getGuestList_Fault	検索実行不正終了時イベント
	Note			: none
	History			: 1) Coded by	nf)h.miyahara	2004/07/09
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
import mx.remoting.Service;
import mx.services.Log;
import mx.rpc.RelayResponder;
import mx.remoting.PendingCall;
import mx.remoting.debug.NetDebug;
import mx.remoting.debug.NetDebugConfig;
import mx.remoting.RecordSet;
import mx.rpc.ResultEvent;
import mx.rpc.FaultEvent;
import mx.controls.gridclasses.DataGridColumn
import mx.controls.DataGrid
import mx.controls.ComboBox
import mx.data.binding.*;
import mx.data.components.*
import mx.transitions.easing.*;
import mx.transitions.Tween;
import app00.downloader;
import mx.styles.CSSStyleDeclaration;
import commonfunc;

class app00.MailPackageService extends MovieClip {

	var tgrid:DataGrid				//結果表示ViewComponent
	var gpath:MovieClip			//基準ムービークリップパス
	var rpath:MovieClip		//基準ムービークリップパス
	var resort_code:String	//リゾートコード（0:共通）
	var gdepth:Number					//Depth
	var sobject:Object			//CFCsobject
	var opath:String				//CSVopath
	var dsarr:Array				//データソース事前指定用配列
	var tempRecord:RecordSet			//取得データ退避用レコードセット
	var cfunc:commonfunc

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: MailPackageService
		Description		: Constructor
		Usage			: MailPackageService( myGrid:DataGrid, myPath:MovieClip );
		Attributes		: myGrid:DataGrid	（結果格納用データグリッドコンポーネント名）
						: myPath:MovieClip	（基準ＭＣパス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function MailPackageService( myPath:MovieClip,r_id:String  ) {
		gpath = myPath;
		rpath = _root;
		resort_code = r_id;
		tgrid = myPath.results_grd;
		gdepth = gpath.getNextHighestDepth();
		cfunc = new commonfunc()

		var myDebug = mx.remoting.debug.NetDebug.initialize();
		sobject = new Service( "http://" + _global.hostAddress + "/flashservices/gateway/", null , _global.servicePath + ".app00.cfc.mailPackage" , null , null ); 
		
		//NetDebbuger設定
		var config:NetDebugConfig = sobject.connection.getDebugConfig();
		config.app_server.trace = true;
		config.client.trace = true;
		
		//ＭＣ初期化
		initializedData();
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: initializedData
		Description		: 初期設定
		Usage			: initializedData();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function initializedData(){
		var Owner = this

		//日付入力初期設定
		var today_date:Date = new Date();
		var start_date:Date = new Date(today_date.getFullYear(),today_date.getMonth(),today_date.getDate()-3)
		var end_date:Date = new Date(today_date.getFullYear(),today_date.getMonth(),today_date.getDate()-2)

		var syear = start_date.getFullYear();
		var eyear = end_date.getFullYear();
		var smonth = start_date.getMonth()+1;
		var emonth = end_date.getMonth()+1;
		var sday = start_date.getDate();
		var eday = end_date.getDate();

		if( smonth < 10 ){ smonth = "0" + smonth }
		if( emonth < 10 ){ emonth = "0" + emonth }
		if( sday < 10 ){ sday = "0" + sday }
		if( eday < 10 ){ eday = "0" + eday }
		var str_sday:String = (syear + "/" + smonth + "/" + sday);
		var str_eday:String = (eyear + "/" + emonth + "/" + eday);
		gpath.MConditionBox.dStartDate.text = str_sday
		gpath.MConditionBox.dEndDate.text = str_eday

		//日付入力補助設定
		gpath.MConditionBox.dStartDate_mc.onRelease = function(){
			var targetTxt = new Array(Owner.gpath.MConditionBox.dStartDate)
			var dStartDateChoose = new DateChoose( Owner.gpath.MConditionBox, targetTxt);
		}
		gpath.MConditionBox.dEndDate_mc.onRelease = function(){
			var targetTxt = new Array(Owner.gpath.MConditionBox.dEndDate)
			var dEndDateChoose = new DateChoose( Owner.gpath.MConditionBox, targetTxt, Owner.gpath.MConditionBox.dStartDate.text);
		}

		//調査手段設定
		var svyPC:PendingCall = sobject.getSurvey();
		svyPC.responder = new RelayResponder( this, "getSurvey_Result", "Service_Fault" );
		gpath.MConditionBox.send_cb.dropdown;
		gpath.MConditionBox.used_cb.dropdown;
		gpath.MConditionBox.survey_cb.dropdown;
		
		//施設設定
		var rstPC:PendingCall = sobject.getresort(resort_code);
		rstPC.responder = new RelayResponder( this, "getresort_Result", "Service_Fault" );		
		
		//カラム設定
		var col = new DataGridColumn("depature_date");
 		col.headerText = "出発日";
 		col.width = 100;
		col.sortOnHeaderRelease = true;
		tgrid.addColumn(col);

		col = new DataGridColumn("customernumber");
 		col.headerText = "顧客番号";
 		col.width = 100;
		col.sortOnHeaderRelease = true;
		tgrid.addColumn(col);

		col = new DataGridColumn("customer_nm");
 		col.headerText = "氏名";
 		col.width = 100;
		col.sortOnHeaderRelease = true;
		tgrid.addColumn(col);

		col = new DataGridColumn("customer_kana");
 		col.headerText = "カナ";
 		col.width = 120;
		col.sortOnHeaderRelease = true;
		tgrid.addColumn(col);

		col = new DataGridColumn("email");
 		col.headerText = "E-mail";
 		col.width = 320;
		col.sortOnHeaderRelease = true;
		tgrid.addColumn(col);

		col = new DataGridColumn("repeater");
 		col.headerText = "";
 		col.width = 20;
		col.sortOnHeaderRelease = true;
		tgrid.addColumn(col);

		col = new DataGridColumn("sendflg");
 		col.headerText = "送信状態";
 		col.width = 80;
		col.sortOnHeaderRelease = true;
		tgrid.addColumn(col);

		col = new DataGridColumn("sendchk");
 		col.headerText = "";
 		col.width = 30;
		col.cellRenderer = "SCheckCellRenderer";
		col.sortOnHeaderRelease = true;
		tgrid.addColumn(col);

		col = new DataGridColumn("detail");
 		col.headerText = "";
		col.cellRenderer = "MEditCellRenderer";
		col.sortOnHeaderRelease = true;
		tgrid.addColumn(col);

		//初期状態：非表示
		tgrid._visible = false;

		//メニュー切替
		gpath.changeBtn.onRelease = function(){
			Owner.gpath.MConditionBox.Calender.removeMovieClip();
			Owner.SetMenuList(Owner.gpath)
		}

		//検索ボタン設定
		gpath.searchBtn.onRelease = function(){
			Owner.gpath.MConditionBox.Calender.removeMovieClip();
			//検索条件オブジェクト作成
			var conditionParam = new Object();
			conditionParam.dStartDate = Owner.gpath.MConditionBox.dStartDate.text
			conditionParam.dEndDate = Owner.gpath.MConditionBox.dEndDate.text
			conditionParam.customer_nm = Owner.gpath.MConditionBox.findResults0_txt.text
			conditionParam.EMail = Owner.gpath.MConditionBox.findResults1_txt.text
			conditionParam.customernumber = Owner.gpath.MConditionBox.findResults2_txt.text
			conditionParam.sendflg = Owner.gpath.MConditionBox.send_cb.selectedItem.data
			conditionParam.surveymethod = Owner.gpath.MConditionBox.survey_cb.selectedItem.data
			conditionParam.usedflg = Owner.gpath.MConditionBox.used_cb.selectedItem.data
//			conditionParam.resort_code = Owner.resort_code
			conditionParam.resort_code = Owner.gpath.MConditionBox.resort_cb.selectedItem.data

			//検索条件確認
			if(Owner.CheckconditionParam(conditionParam)){
				Owner.cfunc.SearchWait( Owner.gpath, true );
				//CFCメソッド呼出
				var MaildtlPC:PendingCall = Owner.sobject.getMailSendList(conditionParam);
				MaildtlPC.responder = new RelayResponder( Owner, "getMailSendList_Result", "getMailSendList_Fault" );
			} else {
				//アラート表示
				Owner.cfunc.ShowConfirm( Owner.gpath , "検索条件の期間指定が矛盾しています。\r期間設定を正しく指定して再度検索してください。", Owner, null );
				Owner.cfunc.SearchWait( Owner.gpath, false );
			}
		}

		//クリアボタン設定
		gpath.clearBtn.onRelease = function(){
			Owner.gpath.MConditionBox.Calender.removeMovieClip();
			var ConditionMC = Owner.gpath.MConditionBox
			ConditionMC.dStartDate.text = str_sday
			ConditionMC.dEndDate.text = str_eday
			ConditionMC.findResults0_txt.text = "";
			ConditionMC.findResults1_txt.text = "";
			ConditionMC.findResults2_txt.text = "";
			ConditionMC.findResults3_txt.text = "";
			Owner.setDefaultvalue();
		}

		//CSVボタン設定
		gpath.csvBtn.onRelease = function(){
			Owner.gpath.MConditionBox.Calender.removeMovieClip();
			var OKFunction = Owner.OutputCSV
			Owner.cfunc.ShowConfirm( this._parent , "一覧をＣＳＶファイルに出力します。よろしいですか？", Owner , OKFunction ,0 );
		}

		//一括送信ボタン設定
		gpath.AllsendBtn.onRelease = function(){
			Owner.gpath.MConditionBox.Calender.removeMovieClip();
			var CheckNum:Number = 0;
			for(var i=0; i< Owner.tgrid.length; i++){
				if( Owner.tgrid.getItemAt(i).sendchk ){
					CheckNum++;
				}
			}
			var OKFunction = Owner.AllSendMail
			if(CheckNum == 0){
				Owner.cfunc.ShowConfirm( this._parent , "送信対象がありません。\n送信対象者の送信のチェックボックスをオンにして下さい。", Owner , null );
			} else {
				Owner.cfunc.ShowConfirm( this._parent , (CheckNum) + "件にメール送信します。よろしいですか？", Owner , OKFunction ,0 );
			}
		}


		//フラグ更新ボタン設定
		gpath.MConditionBox.sentupdate.onRelease = function(){
			var conditionParam = new Object();
			conditionParam.dStartDate = Owner.gpath.MConditionBox.dStartDate.text
			conditionParam.dEndDate = Owner.gpath.MConditionBox.dEndDate.text
			conditionParam.customer_nm = Owner.gpath.MConditionBox.findResults0_txt.text
			conditionParam.EMail = Owner.gpath.MConditionBox.findResults1_txt.text
			conditionParam.customernumber = Owner.gpath.MConditionBox.findResults2_txt.text
			conditionParam.sendflg = Owner.gpath.MConditionBox.send_cb.selectedItem.data
			conditionParam.surveymethod = Owner.gpath.MConditionBox.survey_cb.selectedItem.data
			conditionParam.usedflg = Owner.gpath.MConditionBox.used_cb.selectedItem.data
			this.dStartDate = conditionParam.dStartDate
			this.dEndDate = conditionParam.dEndDate
			this.getURL("options/sentflg.cfm",this,"post")
		}

		//手渡し更新ボタン設定
		gpath.MConditionBox.handupdate.onRelease = function(){
			var conditionParam = new Object();
			conditionParam.dStartDate = Owner.gpath.MConditionBox.dStartDate.text
			conditionParam.dEndDate = Owner.gpath.MConditionBox.dEndDate.text
			conditionParam.customer_nm = Owner.gpath.MConditionBox.findResults0_txt.text
			conditionParam.EMail = Owner.gpath.MConditionBox.findResults1_txt.text
			conditionParam.customernumber = Owner.gpath.MConditionBox.findResults2_txt.text
			conditionParam.sendflg = Owner.gpath.MConditionBox.send_cb.selectedItem.data
			conditionParam.surveymethod = Owner.gpath.MConditionBox.survey_cb.selectedItem.data
			conditionParam.usedflg = Owner.gpath.MConditionBox.used_cb.selectedItem.data
			this.dStartDate = conditionParam.dStartDate
			this.dEndDate = conditionParam.dEndDate
			this.getURL("options/handflg.cfm",this,"post")
		}

		//各ボタン有効無効切替
		cfunc.ChangeEnabled( gpath, "" , false )

		//検索条件スタイル設定
		for(var i=0; i<4; i++){
			gpath.MConditionBox[ "findResults" + i + "_txt" ].borderStyle = "none"
			gpath.MConditionBox[ "findResults" + i + "_txt" ].maxChars = 255
		}
		gpath.MConditionBox.findResults1_txt.restrict = "A-Za-z0-9\\-\\@\\_\\/\\#\\$\\%\\&\\.";
		gpath.MConditionBox.findResults2_txt.restrict = "A-Za-z0-9";
		gpath.MConditionBox.addList_txt.borderStyle = "none"
		gpath.MConditionBox.addList_txt.restrict = "0-9\\,"
		var myTextFormat = new TextFormat(); 
		gpath.MConditionBox.findResults0_txt.myTextFormat.font = "_ゴシック"; 
		gpath.MConditionBox.findResults0_txt.setNewTextFormat(myTextFormat);
		gpath.MConditionBox.findResults1_txt.myTextFormat.font = "_ゴシック"; 
		gpath.MConditionBox.findResults1_txt.setNewTextFormat(myTextFormat);
		gpath.MConditionBox.findResults2_txt.myTextFormat.font = "_ゴシック"; 
		gpath.MConditionBox.findResults2_txt.setNewTextFormat(myTextFormat);
		gpath.MConditionBox.addList_txt.myTextFormat.font = "_ゴシック"; 
		gpath.MConditionBox.addList_txt.setNewTextFormat(myTextFormat);


		// GridAllcheck
		var allchckListener:Object = new Object();
		allchckListener.click = function(eventObject:Object) {
			for(var i=0; i<Owner.tgrid.length; i++){
				Owner.tgrid.editField(i, "sendchk", eventObject.target.selected);
			}
		};
		gpath.allchecker.addEventListener("click", allchckListener);
		gpath.allchecker._visible = false;
		gpath.allchecker.enabled = false;

		//タブ設定
		TabInitialize();
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getMailDetail_Result
		Description		: メールテンプレート詳細取得成功時処理
		Usage			: getMailDetail_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent		（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getresort_Result(ReEvt:ResultEvent):Void{
		var owner = this;
		gpath.MConditionBox.resort_cb.dataProvider = ReEvt.result.items
		for(var i=0;i<gpath.MConditionBox.resort_cb.dataProvider.length;i++){
			if(gpath.MConditionBox.resort_cb.dataProvider[i].data == resort_code){
				gpath.MConditionBox.resort_cb.selectedIndex = i
			}
		}
		
		var rcblistner:Object = new Object();
		rcblistner.change = function(eventObject:Object) {
			
			var MailtmplPC:PendingCall = owner.sobject.gettemplatelist(eventObject.target.selectedItem.data);
			MailtmplPC.responder = new RelayResponder( owner, "getTemplateList_Result", "getTemplateList_Fault" );

		};
		
		gpath.MConditionBox.resort_cb.addEventListener("change", rcblistner)
			
		//メールテンプレート取得CFCメソッド呼出
		var MailtmplPC:PendingCall = owner.sobject.gettemplatelist(gpath.MConditionBox.resort_cb.dataProvider[0].data);
		MailtmplPC.responder = new RelayResponder( owner, "getTemplateList_Result", "getTemplateList_Fault" );
		
	}
	
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: TabInitialize
		Description		: タブ設定
		Usage			: TabInitialize();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/30
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function TabInitialize():Void{
		gpath.tabChildren = true
		gpath.MConditionBox.tabChildren = true
		gpath.MConditionBox.findResults0_txt.tabIndex = 2;
		gpath.MConditionBox.findResults1_txt.tabIndex = 4;
		gpath.MConditionBox.findResults2_txt.tabIndex = 6;
		gpath.MConditionBox.findResults3_txt.tabIndex = 8;
		gpath.MConditionBox.send_cb.tabIndex = 10;
		gpath.searchBtn.tabIndex = 12;
		gpath.clearBtn.tabIndex = 14;
		gpath.template_cb.tabIndex = 16;
		gpath.AllsendBtn.tabIndex = 18;
		gpath.csvBtn.tabIndex = 20;
		gpath.previewBtn.tabIndex = 22;
		gpath.printBtn.tabIndex = 24;
		gpath.changeBtn.tabIndex = 26;
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getTemplateList_Result
		Description		: メールテンプレート取得正常終了時イベント
		Usage			: getTemplateList_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getTemplateList_Result(ReEvt:ResultEvent):Void{
		gpath.template_cb.dataProvider = ReEvt.result.items[0].template.items;
		gpath.rtemplate_cb.dataProvider = ReEvt.result.items[0].rtemplate.items;

		//ローディング用ＭＣ削除処理
		gpath._x = 0
		cfunc.loadingdelete(gpath._parent)
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getTemplateList_Fault
		Description		: メールテンプレート取得不正終了時イベント
		Usage			: getTemplateList_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getTemplateList_Fault(FltEvt:FaultEvent):Void{	//trace("Categories Fault: " + FltEvt.fault.description);
		cfunc.ShowConfirm( gpath , "メールテンプレートが取得できませんでした。\rエラーメッセージ："+ FltEvt.fault.description, this , null );
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getMailSendList_Result
		Description		: 検索実行正常終了時イベント
		Usage			: getGuestList_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getMailSendList_Result(ReEvt:ResultEvent):Void{
		if( ReEvt.result.mRecordsAvailable == 0){
			cfunc.SearchWait( gpath, false );
			cfunc.ShowConfirm( gpath , "該当レコードはありませんでした。\r検索条件を変更して再度検索を行ってみてください", this , null );
			tgrid.dataProvider = RecSet;
			gpath.allchecker._visible = false;
			gpath.allchecker.enabled = false;
			
			
		} else {

			//初期設定
			var Owner = this;
			tgrid._visible = true;
			cfunc.SearchWait( gpath, false );

			//レコードセットオブジェクトに結果代入
			var RecSet:RecordSet = RecordSet(ReEvt.result);

			//データ退避
			tempRecord = RecordSet(ReEvt.result);

			tgrid.dataProvider = RecSet
			
			//スタイル設定
			initGridStyle(tgrid);
			tgrid.setVScrollPolicy("auto");

			//各ボタン有効無効切替
			cfunc.ChangeEnabled( gpath, "AllsendBtn,csvBtn,previewBtn,printBtn" , true )

			//カスタムソート設定
			var GridListener = new Object();
			GridListener.NMsortFlg = false;
			GridListener.CIDsortFlg = false;
			GridListener.RIDsortFlg = false;
			GridListener.headerRelease = function(event) {
				if (event.columnIndex == 3){
					this.NMsortFlg = !this.NMsortFlg
					if(this.NMsortFlg){
						Owner.dataProvider = RecSet.sortItemsBy(["customer_kana"], null, Array.DESCENDING );
					} else {
						Owner.dataProvider = RecSet.sortItemsBy(["customer_kana"], null, null );
					}
				} else if( event.columnIndex == 0 ) {
					this.RIDsortFlg = !this.RIDsortFlg
					if(this.RIDsortFlg){
						Owner.dataProvider = RecSet.sortItemsBy(["referencenumber"], null, Array.NUMERIC | Array.DESCENDING );
					} else {
						Owner.dataProvider = RecSet.sortItemsBy(["referencenumber"], null, Array.NUMERIC );
					}
				} else if( event.columnIndex == 2 ) {
					this.CIDsortFlg = !this.CIDsortFlg
					if(this.CIDsortFlg){
						Owner.dataProvider = RecSet.sortItemsBy(["customernumber"], null, Array.NUMERIC | Array.DESCENDING );
					} else {
						Owner.dataProvider = RecSet.sortItemsBy(["customernumber"], null, Array.NUMERIC );
					}
				}
			}
			tgrid.addEventListener("headerRelease", GridListener);
			gpath.allchecker._visible = true;
			gpath.allchecker.enabled = true;

		}
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getMailSendList_Fault
		Description		: 検索実行不正終了時イベント
		Usage			: getMailSendList_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getMailSendList_Fault(FltEvt:FaultEvent):Void{
		cfunc.ShowConfirm( gpath , "検索実行できませんでした。\rエラーメッセージ："+ FltEvt.fault.description, this , null );
		trace("Categories Fault: " + FltEvt.fault.description);
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: OutputCSV
		Description		: ＣＳＶファイル出力
		Usage			: OutputCSV( Obj );
		Attributes		: Obj : （コンファームを呼出した呼出元クラスインスタンス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function OutputCSV( Obj ){
		//インスタンス入替
		this = Obj
		dsarr = new Array();
		for(var i=0; i<tgrid.length; i++){
			trace(";" + tgrid.getItemAt(i).copeflg)
			dsarr.push({
				depature_date:tgrid.getItemAt(i).depature_date,
				customernumber:tgrid.getItemAt(i).customernumber,
				customer_nm:tgrid.getItemAt(i).customer_nm,
				email:tgrid.getItemAt(i).email,
				sendflg:tgrid.getItemAt(i).sendflg
			})
		}
		//CFCメソッド呼出
		var MailtmpPC:PendingCall = sobject.SendListResultOutputCSV( dsarr );
		MailtmpPC.responder = new RelayResponder( this, "OutputCSV_Result", "OutputCSV_Fault" );
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: OutputCSV_Result
		Description		: ＣＳＶ出力正常終了時
		Usage			: OutputCSV_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function OutputCSV_Result(ReEvt:ResultEvent):Void{
		//opath = ReEvt.result.opath
		//cfunc.ShowConfirm( gpath , ReEvt.result.outputMsg + "に出力しました。ファイルを表示しますか？", this, this.getCSVFile );
		var FileObj:downloader = new downloader(gpath,ReEvt.result.filename,ReEvt.result.newname)
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: OutputCSV_Fault
		Description		: ＣＳＶ出力不正終了時
		Usage			: OutputCSV_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function OutputCSV_Fault(FltEvt:FaultEvent):Void{
		//処理を記述
		//var myConfirm:ConfirmWindow = new ConfirmWindow( gpath , "ＣＳＶファイルが出力できませんでした。\rエラーメッセージ："+ FltEvt.fault.description, this , null );
		cfunc.ShowConfirm( gpath , "ＣＳＶファイルが出力できませんでした。\rエラーメッセージ："+ FltEvt.fault.description, this , null );
		trace("Categories Fault: " + FltEvt.fault.description);
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getCSVFile
		Description		: ＣＳＶファイルダウンロード
		Usage			: getCSVFile(Obj);
		Attributes		: Obj : （コンファームを呼出した呼出元クラスインスタンス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getCSVFile( Obj ){
		//インスタンス入替
		this = Obj
		//該当ファイルを別ウインドウ表示
		var targetFilePath = "Javascript:void(location.href='http://" + _global.hostAddress + opath + "')"
		gpath.getURL( targetFilePath , "_blank" )
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: AllSendMail
		Description		: メール一括送信
		Usage			: AllSendMail( Obj );
		Attributes		: Obj : （コンファームを呼出した呼出元クラスインスタンス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function AllSendMail( Obj ){
		//インスタンス入替
		this = Obj
		dsarr = new Array();
		//for(var i=0; i<tgrid.length; i++){
		for(var i=0; i<tempRecord.length; i++){
//			var tmp_nm = tempRecord.getItemAt(i).customer_nm.substring(0,tempRecord.getItemAt(i).customer_nm.indexOf("("))
			var tmp_nm = tempRecord.getItemAt(i).customer_nm
			if( tempRecord.getItemAt(i).sendchk){
				//	mailid振分
				if(tempRecord.getItemAt(i).repeater==''){
					var wk_mailid = gpath.template_cb.selectedItem.data
				}else{
					var wk_mailid = gpath.rtemplate_cb.selectedItem.data
				}
				dsarr.push({
					referencenumber:tempRecord.getItemAt(i).referencenumber,
					customernumber:tempRecord.getItemAt(i).customernumber,
					mailtemplate_id:wk_mailid,
					customer_nm:tmp_nm,
					email:tempRecord.getItemAt(i).email
				})
			}
		}
		//CFCメソッド呼出
		var SendService = new Service( "http://" + _global.hostAddress + "/flashservices/gateway/", null , _global.servicePath + ".app00.cfc.sendMail" , null , null ); 
		//var MailtmpPC:PendingCall = SendService.AllSendMail( dsarr, gpath.template_cb.selectedItem.data );
		var MailtmpPC:PendingCall = SendService.AllSendMail( dsarr );
		MailtmpPC.responder = new RelayResponder( this, "AllSendMail_Result", "AllSendMail_Fault" );
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: AllSendMail_Result
		Description		: ＣＳＶ出力正常終了時
		Usage			: AllSendMail_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function AllSendMail_Result(ReEvt:ResultEvent):Void{
		cfunc.ShowConfirm( gpath , "メール送信が完了しました。", this , null );
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: AllSendMail_Fault
		Description		: ＣＳＶ出力不正終了時
		Usage			: AllSendMail_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function AllSendMail_Fault(FltEvt:FaultEvent):Void{	//処理を記述		trace("Categories Fault: " + FltEvt.fault.description);
		cfunc.ShowConfirm( gpath , "メールが送信できませんでした。\rエラーメッセージ："+ FltEvt.fault.description, this , null );
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: SetTargetData
		Description		: 編集対象取得～編集画面表示
		Usage			: SetTargetData(index);
		Attributes		: index：	（編集対象ＩＮＤＥＸ）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/06
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function SetTargetData(index){
//	trace("copechk: " + tgrid.getItemAt(index).copechk + " sendchk: " + tgrid.getItemAt(index).sendchk)
		var tempRec:RecordSet = new RecordSet();
		var removeNum:Number = 0;
		if( tempRecord.getItemAt(index).sendchk ){
			
			for(var i=0; i<tgrid.length; i++){
				
				if(tgrid.getItemAt(i).sendchk == undefined ){
					tgrid.editField(i, "sendchk", false);
				}
				if(tgrid.getItemAt(i).sendchk){
					var tItems:Object = {
							customer_nm:tempRecord.getItemAt(i).customer_nm, 
							repeater:tempRecord.getItemAt(i).repeater, 
							referencenumber:tempRecord.getItemAt(i).referencenumber,
							email:tempRecord.getItemAt(i).email
						};
					tempRec.addItem(tItems)
				}else{
					if(i<index){removeNum++;}
				}
			}
			
			/*
			trace(index-removeNum)
			trace(tempRec.getItemAt(0).customer_nm.substring(0,tempRec.getItemAt(0).customer_nm.indexOf("(")));			
			*/
			//グローバル変数に編集対象ＩＮＤＥＸ・データプロバイダを退避
			_global.mailTemplateIndex = gpath.template_cb.selectedItem.data;
			_global.mailrTemplateIndex = gpath.rtemplate_cb.selectedItem.data;
			_global.targetIndex = index-removeNum//index;
			_global.targetData = tempRec //tgrid.dataProvider;
			
			//編集画面読込み
			//gpath.gotoAndStop("edit")
			gpath.mailsingle.removeMovieClip();
			gpath.attachMovie("mailsingle","mailsingle",gdepth++,{parentobj:this})
			gpath.mailsingle._x = 4;
			gpath.mailsingle._y = 4;
			
		} else {
			cfunc.ShowConfirm( gpath , "この対象者にはメール送信できません。\r送信のチェックボックスをオンにして下さい。", this , null );
		}
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: SetTargetData
		Description		: 編集対象取得～編集画面表示
		Usage			: SetTargetData(index);
		Attributes		: index：	（編集対象ＩＮＤＥＸ）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/06
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function SetReaction(index,selected){
/*
		trace(": " + tempRecord.getItemAt(index).reserve_code)
		trace(": " + selected)
*/
		var MailtmplPC:PendingCall = sobject.setReaction(tempRecord.getItemAt(index).referencenumber,selected);
		MailtmplPC.responder = new RelayResponder( this, null, "SetReaction_Fault" );

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: SetReaction_Result
		Description		: 対応情報セット正常終了時
		Usage			: SetReaction_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	private function SetReaction_Result(ReEvt:ResultEvent):Void{

	}
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: SetReaction_Fault
		Description		: 対応情報セット不正終了時
		Usage			: SetReaction_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function SetReaction_Fault(FltEvt:FaultEvent):Void{	//処理を記述		trace("Categories Fault: " + FltEvt.fault.description);
		cfunc.ShowConfirm( gpath , "対応情報の登録ができませんでした。\rエラーメッセージ："+ FltEvt.fault.description, this , null );
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: CheckconditionParam
		Description		: 検索条件のチェック
		Usage			: CheckconditionParam(Obj);
		Attributes		: Obj : （検索条件オブジェクト）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/05
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function CheckconditionParam( Obj ){
		var Compelete = false
		if( Obj.dStartDate <= Obj.dEndDate){
			Compelete = true
		}
		return Compelete
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getSurvey_Result
		Description		: 調査手段マスタ取得正常終了時イベント
		Usage			: getSurvey_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2005/04/26
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getSurvey_Result(ReEvt:ResultEvent):Void{
		var Owner = this
		var EditorPanel = gpath.MConditionBox;
		var ComboData = new Array();
		ComboData.push({data:-1,label:"指定無し"})
		for(var i=0; i<ReEvt.result.mRecordsAvailable; i++){
			ComboData.push({data:ReEvt.result.items[i].control_id,label:ReEvt.result.items[i].control_nm})
		}
		EditorPanel.survey_cb.dataProvider = ComboData;
		setDefaultvalue();
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setDefaultvalue
		Description		: プルダウン初期値設定
		Usage			: setDefaultvalue();
		Attributes		: 
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2005/04/26
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function setDefaultvalue(){
		var ConditionMC = gpath.MConditionBox
		//プルダウン初期値設定
		ConditionMC.send_cb.selectedIndex = 2
		ConditionMC.used_cb.selectedIndex = 0
		ConditionMC.survey_cb.selectedIndex = 3
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: initGridStyle
		Description		: 
		Usage			: initGridStyle(FltEvt:FaultEvent);
		Attributes		: 
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function initGridStyle(tg):Void {
		tg.headerHeight = 22;
		tg.setRowHeight(22);

		tg.setStyle("alternatingRowColors", Array(0xFFFFFF, 0xE7E4DC));
		tg.setStyle("hGridLines", true);
		tg.setStyle("hGridLineColor", 0x95946A);
		tg.setStyle("vGridLines", true);
		tg.setStyle("vGridLineColor", 0x95946A);
		tg.setStyle("borderStyle", "solid");
		tg.setVScrollPolicy("auto");
		// themeColor
		tg.setStyle("themeColor", 0xDEDEBE); 
		tg.setStyle("textSelectedColor", 0x333333);
		tg.setStyle("shadowCapColor", 0xFFFFFF);
		tg.setStyle("shadowColor", 0x336699);
		tg.setStyle("borderColor", 0xE9E1D6);
		tg.setStyle("themeColor", 0xE9E1D6);
		tg.setStyle("useRollOver", false);
		tg.setStyle("textSelectedColor", 0x333333);
		tg.setStyle("rollOverColor", 0xDEDEBE);

		tg.resizableColumns = false;
		tg.setStyle("headerColor", 0xE7E4DC);
		tg.setStyle("fontSize", 11);
		tg.setStyle("fontFamily", "Verdana");
		tg.setVScrollPolicy("auto");

		var headerStyles = new CSSStyleDeclaration();
		headerStyles.setStyle("fontSize", 11);
		headerStyles.setStyle("fontWeight", "normal");
		headerStyles.setStyle("fontFamily", "Verdana");
		tg.setStyle("headerStyle", headerStyles);
		tgrid.hScrollPolicy = "auto";

		tgrid.setVScrollPolicy("auto");

		//	--データグリッドアクセス完了後処理
		tgrid.doLater(this, "loadcomplete");
	}	
	
}
