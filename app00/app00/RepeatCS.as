﻿/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	Name			: RepeatCS
	Description		: リピーターＣＳ登録画面
	Usage			: var XXXXXX:RepeatCS = new RepeatCS( path );
	Attributes		: mySource:mySource		（）
	Attributes		: myPath:myPath			（基準パス）
	Attributes		: rcd:rcd				（予約コード）
	Attributes		: tid:tid				（ムービークリップ識別コード）
	Extends			: CommonFunction(Class)
	Method			: 
	Note			: none
	History			: 1) Coded by	nf)h.miyahara	2005/07/05
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
import mx.remoting.Service;
import mx.services.Log;
import mx.rpc.RelayResponder;
import mx.remoting.PendingCall;
import mx.remoting.debug.NetDebug;
import mx.remoting.debug.NetDebugConfig;
import mx.rpc.ResultEvent;
import mx.rpc.FaultEvent;
import mx.controls.TextArea;
import mx.controls.Label;

class RepeatCS extends CommonFunction {

	var GroundPath:MovieClip	//基準ムービークリップパス
	var GPDepth:Number			//Depth
	var ServiceObject:Object	//CFCServiceObject
	var PaneSource:MovieClip	//ScrollPane内MovieClip
	var	referencenumber:String	//照会用番号
	
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: RepeatCS
		Description		: Constructor
		Usage			: RepeatCS( myPath:MovieClip );
		Attributes		: myGrid:DataGrid	（結果格納用データグリッドコンポーネント名）
						: myPath:MovieClip	（基準ＭＣパス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function RepeatCS( mySource:MovieClip, myPath:MovieClip, rcd:String  ) {
		PaneSource = mySource;
		GroundPath = myPath;
		GPDepth = GroundPath.getNextHighestDepth();
		referencenumber = rcd;

		var myDebug = mx.remoting.debug.NetDebug.initialize();
		ServiceObject = new Service( "http://" + _global.hostAddress + "/flashservices/gateway/", null , _global.servicePath + ".specsheet" , null , null ); 
		
		//NetDebbuger設定
		var config:NetDebugConfig = ServiceObject.connection.getDebugConfig();
		config.app_server.trace = true;
		config.client.trace = true;

		initializedData();

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: initializedData
		Description		: 初期設定
		Usage			: initializedData();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function initializedData(){
		if(GroundPath.RepeatCS != undefined){
			//trace(GroundPath.RepeatCS.referencenumber)
			GroundPath.RepeatCS.removeMovieClip();
		}

		GroundPath.attachMovie("repeaterCS", "RepeatCS", GPDepth, {_y:100, _x:100 })
		GPDepth++
		GroundPath.RepeatCS.referencenumber = referencenumber

		//CFCメソッド呼出
		var VIdtlPC:PendingCall = ServiceObject.getRepeatCS(referencenumber);
		VIdtlPC.responder = new RelayResponder( this, "getRepeatCS_Result", "Service_Fault" );

	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: Service_Fault
		Description		: 取得失敗時イベント
		Usage			: Service_Fault(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/12/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function Service_Fault(FltEvt:FaultEvent):Void{
		ChangeEnabled( GroundPath, "ss_closeBtn" , true )
		ShowConfirm( GroundPath , "エラーメッセージ："+ FltEvt.fault.description, this , null );
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setRepeatCS
		Description		: リピーターＣＳの登録
		Usage			: setRepeatCS(Obj);
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/12/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function setRepeatCS(Obj){
		//インスタンス入替
		this = Obj
		var arguments1 =  GroundPath.RepeatCS.referencenumber
		var arguments2 =  GroundPath.RepeatCS.area_code
		var arguments3 =  GroundPath.RepeatCS.arrive_date
		var arguments4 =  GroundPath.RepeatCS.depart_date
		var arguments5 =  GroundPath.RepeatCS.q1_txt.value
		var arguments6 =  GroundPath.RepeatCS.q2_txt.text
		var arguments7 =  GroundPath.RepeatCS.q3_txt.text
		var conditionParam = new Object({referencenumber:arguments1,area_code:arguments2,arrive_date:arguments3,depart_date:arguments4,a1:arguments5,a2:arguments6,a3:arguments7});
			
		//CFCメソッド呼出
		var VIdtlPC:PendingCall = ServiceObject.setRepeatCS(conditionParam);
		VIdtlPC.responder = new RelayResponder( this, "setRepeatCS_Result", "setRepeatCS_Fault" );
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getRepeatCS_Result
		Description		: リピーターＣＳ取得成功時イベント
		Usage			: getRepeatCS_Result(ReEvt);
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2005/07/06
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function getRepeatCS_Result(ReEvt:ResultEvent):Void{
		var Owner = this;
		trace(ReEvt.result.area_code)
		GroundPath.RepeatCS.referencenumber = referencenumber
		GroundPath.RepeatCS.arrive_date = ReEvt.result.items[0].arrive_date
		GroundPath.RepeatCS.depart_date = ReEvt.result.items[0].depart_date
		GroundPath.RepeatCS.area_code = ReEvt.result.items[0].area_code
		GroundPath.RepeatCS.q1_txt.value = ReEvt.result.items[0].a_1
		GroundPath.RepeatCS.q2_txt.text = ReEvt.result.items[0].a_2
		GroundPath.RepeatCS.q3_txt.text = ReEvt.result.items[0].a_3
		//null値対応
		if(GroundPath.RepeatCS.q2_txt.text == "null" || GroundPath.RepeatCS.q3_txt.text == "null"){
			GroundPath.RepeatCS.q2_txt.text = ""
			GroundPath.RepeatCS.q3_txt.text = ""
		}

		//ドラッガブル処理
		GroundPath.RepeatCS.repeaterCSHeader.onPress = function() {
    		this._parent.startDrag();
		}
		GroundPath.RepeatCS.repeaterCSHeader.onRelease = function() {
		    this._parent.stopDrag();
		}
		GroundPath.RepeatCS.repeaterCSHeader.onReleaseOutside = function() {
		    this._parent.stopDrag();
		}
		GroundPath.RepeatCS.vi_regBtn.onRelease = function() {
			var OKFunction = Owner.setRepeatCS
			Owner.ShowConfirm( Owner.GroundPath , "リピーター用ＣＳ回答を登録します。よろしいですか？", Owner , OKFunction ,0 );
		}
		GroundPath.RepeatCS.vi_closeBtn.onRelease = function() {
			Owner.GroundPath.RepeatCS.removeMovieClip();
		}

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setRepeatCS_Result
		Description		: リピーターＣＳの登録成功時イベント
		Usage			: setRepeatCS_Result(ReEvt);
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/12/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function setRepeatCS_Result(ReEvt:ResultEvent):Void{
		ChangeEnabled( GroundPath.RepeatCS, "vi_rollbackBtn" , true )
		ShowConfirm( GroundPath ,ReEvt.result.errorMsg , this , null );

	}
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setRepeatCS_Fault
		Description		: リピーターＣＳの登録失敗時イベント
		Usage			: setRepeatCS_Fault(FltEvt);
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/12/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function setRepeatCS_Fault(FltEvt:FaultEvent):Void{
		ShowConfirm( GroundPath , "リピーター用ＣＳ回答の登録が正常に行われませんでした。\rエラーメッセージ："+ FltEvt.fault.description, this , null );
	}

}
