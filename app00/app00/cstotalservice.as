﻿/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	Name			: cstotalservice
	Description		: 
	Usage			: 
	Attributes		: 
	Extends			: 
	Method			: 
	Note			: none
	History			: 1) Coded by	nf)h.miyahara	2004/07/09
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
import mx.remoting.Service;
import mx.services.Log;
import mx.rpc.RelayResponder;
import mx.remoting.PendingCall;
import mx.remoting.debug.NetDebug;
import mx.remoting.debug.NetDebugConfig;
import mx.remoting.RecordSet;
import mx.rpc.ResultEvent;
import mx.rpc.FaultEvent;
import mx.controls.ComboBox
import mx.containers.ScrollPane
import mx.data.binding.*;
import mx.data.components.DataSet
import mx.transitions.easing.*;
import mx.transitions.Tween;
import mx.controls.Alert;
import mx.styles.CSSStyleDeclaration;
import app00.conditionsetting;
//import app00.commonfunc
import commonfunc
import app00.downloader;

class app00.cstotalservice extends MovieClip {
	var gpath:MovieClip			//基準ムービークリップパス
	var rpath:MovieClip			//基準ムービークリップパス
	var cscategory:Number		//ＣＳ表示判別カテゴリＩＤ
	var resort_code:String		//リゾートコード（0:共通）
	var gdepth:Number			//Depth
	var sobject:Object			//CFCsobject
	var opath:String			//CSVOutputPath
	var sresult:DataSet			//検索結果退避用オブジェクト
	var psource:MovieClip		//ScrollPane内MovieClip
	var dsarr:Array				//データソース事前指定用配列
	var slctddate:Array			//選択日
	var outputtype:Number		//ＣＳＶ出力指示
	var cfunc:commonfunc
	var myCond:conditionsetting
	var detailstxt:String		//回答者情報用テキスト
	var currentcondition:Object	//現在設定済み集計条件

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: cstotalservice
		Description		: Constructor
		Usage			: cstotalservice( myPath:MovieClip );
		Attributes		: 
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function cstotalservice( myPath:MovieClip ,c_id:Number,r_id:String ) {
		gpath = myPath;
		rpath = _root;
		gdepth = gpath.getNextHighestDepth();
		cscategory = c_id;
		resort_code = r_id;
		dsarr = new Array();
		slctddate = new Array();
		outputtype = 0
		cfunc = new commonfunc()
		detailstxt = "condition_txt"

		//myCond = new conditionsetting(rpath.conditionbox,this);
		myCond = new conditionsetting(rpath.conditionbox,this,gpath);
		var myDebug = mx.remoting.debug.NetDebug.initialize();
		sobject = new Service( "http://" + _global.hostAddress + "/flashservices/gateway/", null , _global.servicePath + ".app00.cfc.cstotal" , null , null ); 

		//NetDebbuger設定
		var config:NetDebugConfig = sobject.connection.getDebugConfig();
		config.app_server.trace = true;
		config.client.trace = true;
		//ＭＣ初期化
		initializedData();
		if(_global.currentscreen==undefined){
			_global.currentscreen=this
			_global.currentscreen.searchbtnevent();
			_global.currentscreen.resetbtnevent();
		}
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: initializedData
		Description		: 初期設定
		Usage			: initializedData();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function initializedData(){
		var owner = this
		var today =new Date();
		//初期状態：結果エリア非表示 & 出力指示MC無効化
		gpath.ResultsPanel._visible = false;
//2008/05/19
//		gpath.dconditionbox.parents = this
		rpath.dconditionbox.parents = _global.currentscreen
		myCond.initializedData();

		//ScrollPane設定
		gpath.ResultsPanel.results_sp.borderStyle = "none"
		gpath.ResultsPanel.results_sp.vPageScrollSize = gpath.ResultsPanel.results_sp.height;
		gpath.ResultsPanel.results_sp.vLineScrollSize = gpath.ResultsPanel.results_sp.height;

		//ScrollPane内Content設定
		gpath.ResultsPanel.results_sp.contentPath = "results"
		psource = gpath.ResultsPanel.results_sp.content

/*

		//クリアボタン設定
		 rpath.conditionbox.menu01.onRelease = function(){
			owner = _global.currentscreen
			owner.tgrid._visible = false;
			owner.myCond.conditionreset();
			var cbox = owner.gpath.qselection
			for (var resetobj in cbox) {
				cbox[resetobj].answertype_cb.selectedIndex = 0
			}
		}
*/


		//設問選択MC表示
		gpath.qselectionview.q_id = ""
		gpath.qselectionview.onRelease = function(){
			owner.cboxtween(owner.gpath.qselection);
			owner.cboxtween(owner.rpath.dconditionbox,false);
			owner.cboxtween(owner.rpath.conditionbox,false);
		}

		//日付条件入力MC表示
		gpath.dconditionview.onRelease = function(){
			owner.cboxtween(owner.rpath.dconditionbox);
			owner.cboxtween(owner.gpath.qselection,false);
			owner.cboxtween(owner.rpath.conditionbox,false);
		}

		//集計条件入力MC表示
		gpath.conditionview.onRelease = function(){
			if(owner.gpath.dselected_txt.text.length==0){
				var ErrorMsg = "先に集計期間を選択してください"
				var HeaderMsg = "日付選択がされていません     "
				var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, owner.gpath, null, null, null)
			}else{
				owner.cboxtween(owner.rpath.conditionbox);
				owner.cboxtween(owner.gpath.qselection,false);
				owner.cboxtween(owner.rpath.dconditionbox,false);
			}
		}

		//出力指示イベント
		gpath.opcondition.output_type0.onRelease = function(){
			this._parent.output_type1.selectedicon.gotoAndStop(1);
			this._parent.output_type2.selectedicon.gotoAndStop(1);
			this.selectedicon.gotoAndStop(2);
			this._parent.outputtype = 0
			var AlertObj = mx.controls.Alert.show("一覧をファイルに出力します。よろしいですか？", "ファイル出力", Alert.YES | Alert.NO, owner.gpath, myClickHandler, null , Alert.OK);
		}
		gpath.opcondition.output_type1.onRelease = function(){
			this._parent.output_type0.selectedicon.gotoAndStop(1);
			this._parent.output_type2.selectedicon.gotoAndStop(1);
			this.selectedicon.gotoAndStop(2);
			this._parent.outputtype = 1
			var AlertObj = mx.controls.Alert.show("一覧をファイルに出力します。よろしいですか？", "ファイル出力", Alert.YES | Alert.NO, owner.gpath, myClickHandler, null , Alert.OK);
		}
		gpath.opcondition.output_type2.onRelease = function(){
			this._parent.output_type1.selectedicon.gotoAndStop(1);
			this._parent.output_type0.selectedicon.gotoAndStop(1);
			this.selectedicon.gotoAndStop(2);
			this._parent.outputtype = 2
			var AlertObj = mx.controls.Alert.show("一覧をファイルに出力します。よろしいですか？", "ファイル出力", Alert.YES | Alert.NO, owner.gpath, myClickHandler, null , Alert.OK);
		}




		//CSV出力ボタンイベント
		var myClickHandler = new Object();
		myClickHandler = function(evt){
			if(evt.detail == Alert.YES){
				owner.outputtype = owner.gpath.opcondition.outputtype;
				owner.gpath.opcondition._visible = false;
				owner.gpath.opcondition.enabled = false;
				owner.OutputCSV();
			}
		}

		//ファイル出力ボタン設定
		gpath.menu02._visible = gpath.menu02.enabled = false;
		gpath.opcondition._visible = gpath.opcondition.enabled = false;
		gpath.menu02.onRelease = function(){
			if(!owner.gpath.ResultsPanel._visible){
				var ErrorMsg = "出力データがありません。先に集計を行ってください。"
				var HeaderMsg = "ファイル出力不可                                   "
				var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, gpath, null, null, null)
			} else {
				var aptween:Object = new Tween(owner.gpath.opcondition, "_alpha", Strong.easeIn, 0, 100, 0.5, true);
				owner.gpath.opcondition._visible = true;
				owner.gpath.opcondition.enabled =  true;
			}
		}

		//日付条件指定オプションイベント
		var cbListener:Object = new Object();
		cbListener.click = function(evt_obj:Object) {
			owner.slctddate = new Array();
			owner.rpath.dconditionbox.initializedData();
		};
		gpath.dselection.addEventListener("click", cbListener);
		
		//日付オーバーフロー
		gpath.dmorebtn.enabled = false
		gpath.dmorebtn.onRelease = function(){
			owner.myCond.viewmorechips(owner.gpath.morechips,this)
		}
		//gpath.morechips.labeltxt.autoSize = "left"

		//集計条件オーバーフロー
		gpath.cmorebtn.enabled = false
		gpath.cmorebtn.onRelease = function(){
			owner.myCond.viewmorechips(owner.gpath.morechips,this)
		}

		//タブ設定
		TabInitialize();
	}
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: searchbtnevent
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function searchbtnevent(){
		var owner = this
		//検索ボタン設定
		gpath.menu00.onRelease = rpath.conditionbox.menu00.onRelease = function(){
			owner = _global.currentscreen
			var wk_val = owner.verifycondition()
			if(wk_val.length==0){

				var conditionParam = new Object();
				if(owner.resort_code == 0){
					conditionParam.arg_resort_code = owner.myCond.paramsetting(owner.rpath.conditionbox.cond_items.conditem12.checkbox_sp.content);
					if(conditionParam.arg_resort_code.length == 0){
						conditionParam.arg_resort_code = new Array();
						conditionParam.arg_resort_code.push(0);
					}
				} else {
					conditionParam.arg_resort_code = owner.myCond.resortcodesetting(owner.rpath.conditionbox.cond_items.conditem12.checkbox_sp.content,owner.resort_code);
					//conditionParam.arg_resort_code = new Array();
					//conditionParam.arg_resort_code.push(owner.resort_code);
				}
				//conditionParam.arg_resort_code = owner.resort_code;
				conditionParam.arg_category_id = owner.cscategory;
				conditionParam.arg_cross_id = owner.rpath.conditionbox.cross_cb.selectedItem.data;

				var QestinPC:PendingCall = owner.sobject.getEnquete(conditionParam);
				QestinPC.responder = new RelayResponder( owner, "setQuestionnaire_Result", "Service_Fault" );


				var cbox = owner.rpath.conditionbox
				//検索条件オブジェクト作成
				var conditionParam = new Object();
				conditionParam.dcondtion = owner.gpath.dselection.selected
				conditionParam.selecteddays = owner.slctddate
				conditionParam.arg_generation = owner.myCond.paramsetting(owner.rpath.conditionbox.cond_items.conditem00.checkbox_sp.content)
				conditionParam.arg_gender = owner.myCond.paramsetting(owner.rpath.conditionbox.cond_items.conditem01);
				conditionParam.arg_nights = owner.myCond.paramsetting(owner.rpath.conditionbox.cond_items.conditem02.checkbox_sp.content);
				conditionParam.arg_job = owner.myCond.paramsetting(owner.rpath.conditionbox.cond_items.conditem03);
//				conditionParam.arg_together = owner.myCond.paramsetting(owner.rpath.conditionbox.cond_items.conditem04);
				conditionParam.arg_together = owner.myCond.paramsetting(owner.rpath.conditionbox.cond_items.conditem04.checkbox_sp.content);
				conditionParam.arg_area = owner.myCond.paramsetting(owner.rpath.conditionbox.cond_items.conditem05);
				conditionParam.arg_residence = owner.myCond.paramsetting(owner.rpath.conditionbox.cond_items.conditem13.checkbox_sp.content);

				conditionParam.arg_roomtype = owner.myCond.paramsetting(owner.rpath.conditionbox.cond_items.conditem06.checkbox_sp.content);
				conditionParam.arg_purpose = owner.myCond.paramsetting(owner.rpath.conditionbox.cond_items.conditem07.checkbox_sp.content);
				conditionParam.arg_times = owner.myCond.paramsetting(owner.rpath.conditionbox.cond_items.conditem08);
				conditionParam.arg_reservation = owner.myCond.paramsetting(owner.rpath.conditionbox.cond_items.conditem09.checkbox_sp.content);
				conditionParam.arg_cognition = owner.myCond.paramsetting(owner.rpath.conditionbox.cond_items.conditem10.checkbox_sp.content);
				conditionParam.arg_roomnumber = owner.myCond.paramsetting(owner.rpath.conditionbox.cond_items.conditem11.checkbox_sp.content);

				conditionParam.arg_answermethod_id =  owner.rpath.conditionbox.answermethod_cb.selectedItem.data;
				conditionParam.arg_group_id =  owner.rpath.conditionbox.group_cb.selectedItem.data;
				conditionParam.arg_resort_code = owner.myCond.paramsetting(owner.rpath.conditionbox.cond_items.conditem12.checkbox_sp.content);
				if(owner.resort_code == 0){
					conditionParam.arg_resort_code = owner.myCond.paramsetting(owner.rpath.conditionbox.cond_items.conditem12.checkbox_sp.content);
					if(conditionParam.arg_resort_code.length == 0){
						conditionParam.arg_resort_code = new Array();
						conditionParam.arg_resort_code.push(0);
					}
				} else {
					conditionParam.arg_resort_code = owner.myCond.resortcodesetting(owner.rpath.conditionbox.cond_items.conditem12.checkbox_sp.content,owner.resort_code);
//					conditionParam.arg_resort_code = new Array();
//					conditionParam.arg_resort_code.push(owner.resort_code);
				}
//				conditionParam.arg_resort_code = owner.resort_code;
				conditionParam.arg_category_id = owner.cscategory;
				conditionParam.arg_cross_id = owner.rpath.conditionbox.cross_cb.selectedItem.data;
				var CSTotlPC:PendingCall = owner.sobject.getAnswers(conditionParam);
				CSTotlPC.responder = new RelayResponder( owner, "getAnswers_Result", "Service_Fault" );
				owner.cboxtween(owner.rpath.dconditionbox,false);
				owner.cboxtween(owner.rpath.conditionbox,false);
				owner.cboxtween(owner.gpath.qselection,false);
				owner.myCond.waiting(owner.gpath,true)
				owner.gpath.ResultsPanel._visible = false;

				owner.currentcondition = new Object();
				owner.currentcondition = conditionParam


			}else{
				var ErrorMsg = wk_val
				var HeaderMsg = "集計できません                 "
				var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, gpath, null, null, null)
			}
		}
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: resetbtnevent
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function resetbtnevent(){
		var owner = this
		//クリアボタン設定
		rpath.conditionbox.menu01.owner = this
		gpath.menu01.onRelease = rpath.conditionbox.menu01.onRelease = function(){
			owner = _global.currentscreen
			trace(owner.gpath)
			owner.tgrid._visible = false;
			owner.myCond.conditionreset();
		}
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: TabInitialize
		Description		: タブ設定
		Usage			: TabInitialize();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/30
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function TabInitialize():Void{
		gpath.tabChildren = true
		gpath.menu00.tabIndex = 1;
		gpath.menu01.tabIndex = 2;
		gpath.menu02.tabIndex = 3;
		gpath.dconditionview.tabIndex = 4;
		gpath.conditionview.tabIndex = 5;

		rpath.conditionbox.tabChildren = true
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setQuestionnaire_Result
		Description		: 設問情報取得正常終了時イベント
		Usage			: setQuestionnaire_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function setQuestionnaire_Result(ReEvt:ResultEvent):Void{
		var owner = this
		//ペイン内初期化
		gpath.ResultsPanel.results_sp.refreshPane();

		//検索結果をデータセットに退避
		sresult = new DataSet();
		sresult.dataProvider = ReEvt.result;

		dsarr = new Array();
		var startY = 0
		var startX = 0
		var CategoryCnt = 1

		for( var i=0; i<ReEvt.result.mRecordsAvailable; i++ ){
			//	--設問紐付け用キー
			var q_id = ReEvt.result.items[i].viewcell_id

			//カテゴリパネル
			if(ReEvt.result.items[i-1].group_id != ReEvt.result.items[i].group_id){
				psource.attachMovie("CategoryBar", "Category_" + CategoryCnt , gdepth, {_y:startY, _x:startX })
				gdepth++;
				psource["Category_" + CategoryCnt].data_txt.text =  ReEvt.result.items[i].category_nm;
				startY = startY + psource["Category_" + CategoryCnt]._height;
				CategoryCnt++;
/*
				//結果退避格納
				dsarr.push({
					order_id:ReEvt.result.items[i].viewcell_id,
					questionnaire_id:ReEvt.result.items[i].questionnaire_id,
					questionnaire_nm:ReEvt.result.items[i].questionnaire_nm,
					questionnairecategory_nm:ReEvt.result.items[i].category_nm,
					samples:"0",
					average:"0",
					rank:"-",
					result1_samples:"0",
					result1_rate:"0",
					result2_samples:"0",
					result2_rate:"0",
					result3_samples:"0",
					result3_rate:"0",
					result4_samples:"0",
					result4_rate:"0",
					result5_samples:"0",
					result5_rate:"0",
					result6_samples:"0",
					result6_rate:"0",
					result7_samples:"0",
					result7_rate:"0",
					result8_samples:"0",
					result8_rate:"0",
					result0_samples:"0",
					result0_rate:"0",
					effective_n:"0",
					sum_score:"0",
					top1:"0.0",
					top2and3:"0",
					nagetive:"0",
					usecount:"0"
				})
*/
			} 

			//設問パネル
			psource.attachMovie("QuestionBar", "QuestionBar" + q_id , gdepth, {_y:startY, _x:startX })
			gdepth++;
			psource["QuestionBar" + q_id].data_txt.text =  ReEvt.result.items[i].viewq_nm;//psource["QuestionBar" + q_id]._name;
			startY = startY + psource["QuestionBar" + q_id]._height

			//	--ドリルダウン用
			for( var c=0; c<9; c++ ){
				psource["QuestionBar" + q_id]["answer" + c].answer = c
				psource["QuestionBar" + q_id]["answer" + c].q_id = ReEvt.result.items[i].questionnaire_id
				psource["QuestionBar" + q_id]["answer" + c].onRelease = function(){
					owner.detailstxt=this._parent.data_txt.text + " : 回答値 " + this.answer
					owner.getdetail(this.answer,this.q_id)
				}
			}

			//dsarrとのマッチング用ID
			psource["QuestionBar" + q_id].mcid =  i
/*
			//結果退避格納
			dsarr.push({
				order_id:ReEvt.result.items[i].viewcell_id,
				questionnaire_id:ReEvt.result.items[i].questionnaire_id,
				questionnaire_nm:ReEvt.result.items[i].questionnaire_nm,
				questionnairecategory_nm:ReEvt.result.items[i].category_nm,
				samples:"0",
				average:"0",
				rank:"-",
				result1_samples:"0",
				result1_rate:"0",
				result2_samples:"0",
				result2_rate:"0",
				result3_samples:"0",
				result3_rate:"0",
				result4_samples:"0",
				result4_rate:"0",
				result5_samples:"0",
				result5_rate:"0",
				result6_samples:"0",
				result6_rate:"0",
				result7_samples:"0",
				result7_rate:"0",
				result8_samples:"0",
				result8_rate:"0",
				result0_samples:"0",
				result0_rate:"0",
				effective_n:"0",
				sum_score:"0",
				top1:"0.0",
				top2and3:"0",
				nagetive:"0",
				usecount:"0"
})
*/
		}
		psource.attachMovie("bottombar", "bottombar" , gdepth++, {_y:startY, _x:startX })
		psource.bottombar._visible = false
		//ＳＰリサイズ
		gpath.ResultsPanel.results_sp.setSize(gpath.ResultsPanel.results_sp.width,gpath.ResultsPanel.results_sp.height)
		/*
		gpath.loadmask._y = -1000
		gpath.loadmask.gotoAndStop(1)
		gpath._parent.gotoAndPlay(2)
		*/
		gpath._x = 0
		cfunc.loadingdelete(gpath._parent)
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getAnswers_Result
		Description		: 集計実行正常終了時イベント
		Usage			: getAnswers_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by		nf)h.miyahara	2004/09/14
						: 2) Updateded by	nf)h.miyahara	2005/10/17 表示項目追加
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getAnswers_Result(ReEvt:ResultEvent):Void{
		myCond.waiting(gpath,false)
		if( ReEvt.result.mRecordsAvailable == 0){
			var ErrorMsg = "該当レコードはありませんでした。\r検索条件を変更して再度検索を行ってください。"
			var HeaderMsg = "検索結果                                   "
			//var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, gpath, null, null, null)
			var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, null, null, null, null)
			gpath.ResultsPanel._visible = false;
			gpath.menu02._visible = gpath.menu02.enabled = false;
		} else {
			//初期設定
			var start_y = 0;
			var owner = this;
			var CategoryCnt = 1

			//レコードセットオブジェクトに結果代入
			var RecSet:RecordSet = RecordSet(ReEvt.result);
			//検索結果をデータセットに退避

			//結果エリア表示
			gpath.ResultsPanel._visible = true;
			gpath.menu02._visible = gpath.menu02.enabled = true;

			var dsarrcnt = 0
			dsarr = new Array();

			//レコード用ＭＣ設定
			for( var i=0; i<ReEvt.result.mRecordsAvailable; i++ ){
				//データ割当
				var qid = ReEvt.result.items[i].order_id

				//	--N数/利用率
				if(ReEvt.result.items[i-1].group_id != ReEvt.result.items[i].group_id){
					psource["Category_" + CategoryCnt].usecount.use_samples.text =  ReEvt.result.items[i].samples;
					psource["Category_" + CategoryCnt].usecount.use_rate.text =  ReEvt.result.items[i].usecount_txt;

					//	--滞在全体/ショップは非表示
					if( ReEvt.result.items[i].group_id == 10 || ReEvt.result.items[i].group_id == 21 ){
						psource["Category_" + CategoryCnt].usecount._visible = false
						//結果退避格納
						dsarr.push({
							order_id:"",
							questionnaire_id:"",
							questionnaire_nm:"",
							questionnairecategory_nm:psource["Category_" + CategoryCnt].data_txt.text,
							samples:"",
							average:"",
							rank:"-",
							result1_samples:"",
							result1_rate:"",
							result2_samples:"",
							result2_rate:"",
							result3_samples:"",
							result3_rate:"",
							result4_samples:"",
							result4_rate:"",
							result5_samples:"",
							result5_rate:"",
							result6_samples:"",
							result6_rate:"",
							result7_samples:"",
							result7_rate:"",
							result8_samples:"",
							result8_rate:"",
							result0_samples:"",
							result0_rate:"",
							effective_n:"",
							sum_score:"",
							top1:"",
							top2and3:"",
							nagetive:"",
							usecount:"",
							totalusecount:""
						})
					} else {
						//結果退避格納
						dsarr.push({
							order_id:"",
							questionnaire_id:"",
							questionnaire_nm:"",
							questionnairecategory_nm:psource["Category_" + CategoryCnt].data_txt.text,
							samples:psource["Category_" + CategoryCnt].usecount.use_samples.text,
							average:"",
							rank:"-",
							result1_samples:"",
							result1_rate:"",
							result2_samples:"",
							result2_rate:"",
							result3_samples:"",
							result3_rate:"",
							result4_samples:"",
							result4_rate:"",
							result5_samples:"",
							result5_rate:"",
							result6_samples:"",
							result6_rate:"",
							result7_samples:"",
							result7_rate:"",
							result8_samples:"",
							result8_rate:"",
							result0_samples:"",
							result0_rate:"",
							effective_n:"",
							sum_score:"",
							top1:"",
							top2and3:"",
							nagetive:"",
							usecount:"",
							totalusecount:psource["Category_" + CategoryCnt].usecount.use_rate.text
						})
					}

					CategoryCnt++;



				} else {
					if(psource["Category_" + CategoryCnt].usecount.use_samples.text < ReEvt.result.items[i].samples){
						psource["Category_" + CategoryCnt].usecount.use_samples.text =  ReEvt.result.items[i].samples;
						psource["Category_" + CategoryCnt].usecount.use_rate.text =  ReEvt.result.items[i].usecount_txt;
					}
				}

				psource["QuestionBar" + qid].samples_txt.text = ReEvt.result.items[i].samples
				psource["QuestionBar" + qid].average_txt.text = ReEvt.result.items[i].average
				psource["QuestionBar" + qid].rank_mc.rank = ReEvt.result.items[i].rank
				psource["QuestionBar" + qid].result1_samples_txt.text = ReEvt.result.items[i].result1_samples
				psource["QuestionBar" + qid].result2_samples_txt.text = ReEvt.result.items[i].result2_samples
				psource["QuestionBar" + qid].result3_samples_txt.text = ReEvt.result.items[i].result3_samples
				psource["QuestionBar" + qid].result4_samples_txt.text = ReEvt.result.items[i].result4_samples
				psource["QuestionBar" + qid].result5_samples_txt.text = ReEvt.result.items[i].result5_samples
				psource["QuestionBar" + qid].result6_samples_txt.text = ReEvt.result.items[i].result6_samples
				psource["QuestionBar" + qid].result7_samples_txt.text = ReEvt.result.items[i].result7_samples
				psource["QuestionBar" + qid].result8_samples_txt.text = ReEvt.result.items[i].result8_samples
				if(ReEvt.result.items[i].result8_samples==null){
					psource["QuestionBar" + qid].result8_samples_txt.text = ""
				}

				psource["QuestionBar" + qid].result0_samples_txt.text = ReEvt.result.items[i].result0_samples

				psource["QuestionBar" + qid].result1_average_txt.text = ReEvt.result.items[i].result1_average_txt
				psource["QuestionBar" + qid].result2_average_txt.text = ReEvt.result.items[i].result2_average_txt
				psource["QuestionBar" + qid].result3_average_txt.text = ReEvt.result.items[i].result3_average_txt
				psource["QuestionBar" + qid].result4_average_txt.text = ReEvt.result.items[i].result4_average_txt
				psource["QuestionBar" + qid].result5_average_txt.text = ReEvt.result.items[i].result5_average_txt
				psource["QuestionBar" + qid].result6_average_txt.text = ReEvt.result.items[i].result6_average_txt
				psource["QuestionBar" + qid].result7_average_txt.text = ReEvt.result.items[i].result7_average_txt
				psource["QuestionBar" + qid].result8_average_txt.text = ReEvt.result.items[i].result8_average_txt
				if(ReEvt.result.items[i].result8_average_txt==null){
					psource["QuestionBar" + qid].result8_average_txt.text = ""
				}

				psource["QuestionBar" + qid].result0_average_txt.text = ReEvt.result.items[i].result0_average_txt

				//	--問い１０設問のみ表示
				if(ReEvt.result.items[i].group_id == 21){
					psource["QuestionBar" + qid].usecount_txt.text = ReEvt.result.items[i].usecount_txt
				} else {
					psource["QuestionBar" + qid].usecount_txt.text = ""
				}


				psource["QuestionBar" + qid].effective_n_txt.text = ReEvt.result.items[i].effective_n
				psource["QuestionBar" + qid].sum_score_txt.text = ReEvt.result.items[i].sum_score
				psource["QuestionBar" + qid].top1_txt.text = ReEvt.result.items[i].top1
				psource["QuestionBar" + qid].top2and3_txt.text = ReEvt.result.items[i].top2and3
				psource["QuestionBar" + qid].nagetive_txt.text = ReEvt.result.items[i].nagetive

				if(ReEvt.result.items[i].rank == 'A'){
					psource["QuestionBar" + qid].rank_mc.gotoAndStop(1);
				}
				if(ReEvt.result.items[i].rank == 'B'){
					psource["QuestionBar" + qid].rank_mc.gotoAndStop(2);
				}
				if(ReEvt.result.items[i].rank == 'C'){
					psource["QuestionBar" + qid].rank_mc.gotoAndStop(3);
				}
				if(ReEvt.result.items[i].rank == 'D'){
					psource["QuestionBar" + qid].rank_mc.gotoAndStop(4);
				}
				if(ReEvt.result.items[i].rank == 'E'){
					psource["QuestionBar" + qid].rank_mc.gotoAndStop(5);
				}


				//結果退避格納
				dsarr.push({
					order_id:ReEvt.result.items[i].viewcell_id,
					questionnaire_id:ReEvt.result.items[i].questionnaire_id,
					questionnaire_nm:psource["QuestionBar" + qid].data_txt.text,
					questionnairecategory_nm:"",
					samples:psource["QuestionBar" + qid].samples_txt.text,
					average:psource["QuestionBar" + qid].average_txt.text,
					rank:psource["QuestionBar" + qid].rank_mc.rank,
					result1_samples:psource["QuestionBar" + qid].result1_samples_txt.text,
					result1_rate:psource["QuestionBar" + qid].result1_average_txt.text,
					result2_samples:psource["QuestionBar" + qid].result2_samples_txt.text,
					result2_rate:psource["QuestionBar" + qid].result2_average_txt.text,
					result3_samples:psource["QuestionBar" + qid].result3_samples_txt.text,
					result3_rate:psource["QuestionBar" + qid].result3_average_txt.text,
					result4_samples:psource["QuestionBar" + qid].result4_samples_txt.text,
					result4_rate:psource["QuestionBar" + qid].result4_average_txt.text,
					result5_samples:psource["QuestionBar" + qid].result5_samples_txt.text,
					result5_rate:psource["QuestionBar" + qid].result5_average_txt.text,
					result6_samples:psource["QuestionBar" + qid].result6_samples_txt.text,
					result6_rate:psource["QuestionBar" + qid].result6_average_txt.text,
					result7_samples:psource["QuestionBar" + qid].result7_samples_txt.text,
					result7_rate:psource["QuestionBar" + qid].result7_average_txt.text,
					result8_samples:psource["QuestionBar" + qid].result8_samples_txt.text,
					result8_rate:psource["QuestionBar" + qid].result8_average_txt.text,
					result0_samples:psource["QuestionBar" + qid].result0_samples_txt.text,
					result0_rate:psource["QuestionBar" + qid].result0_average_txt.text,
					effective_n:psource["QuestionBar" + qid].effective_n_txt.text,
					sum_score:psource["QuestionBar" + qid].sum_score_txt.text,
					top1:psource["QuestionBar" + qid].top1_txt.text,
					top2and3:psource["QuestionBar" + qid].top2and3_txt.text,
					nagetive:psource["QuestionBar" + qid].nagetive_txt.text,
					usecount:psource["QuestionBar" + qid].usecount_txt.text,
					totalusecount:""
					})


				//サンプル数３０以下は参考値
				//psource["QuestionBar" + qid].under30.removeMovieClip();
				if( psource["QuestionBar" + qid].samples_txt.text < 30 ){
					psource["QuestionBar" + qid].under30._visible = true
				} else {
					psource["QuestionBar" + qid].under30._visible = false
				}
				dsarrcnt++;

			}

/*

			//データ譲渡用配列セット
			for(var i=0; i<dsarr.length; i++){
				var qid = dsarr[i].order_id
				var wk_id = psource["QuestionBar" + qid].mcid
				dsarr[wk_id].samples =  psource["QuestionBar" + qid].samples_txt.text
				dsarr[wk_id].average =  psource["QuestionBar" + qid].average_txt.text
				dsarr[wk_id].rank =  psource["QuestionBar" + qid].rank_mc.rank
				dsarr[wk_id].result1_samples =  psource["QuestionBar" + qid].result1_samples_txt.text
				dsarr[wk_id].result2_samples =  psource["QuestionBar" + qid].result2_samples_txt.text
				dsarr[wk_id].result3_samples =  psource["QuestionBar" + qid].result3_samples_txt.text
				dsarr[wk_id].result4_samples =  psource["QuestionBar" + qid].result4_samples_txt.text
				dsarr[wk_id].result5_samples =  psource["QuestionBar" + qid].result5_samples_txt.text
				dsarr[wk_id].result6_samples =  psource["QuestionBar" + qid].result6_samples_txt.text
				dsarr[wk_id].result7_samples =  psource["QuestionBar" + qid].result7_samples_txt.text
				dsarr[wk_id].result8_samples =  psource["QuestionBar" + qid].result8_samples_txt.text
				dsarr[wk_id].result0_samples =  psource["QuestionBar" + qid].result0_samples_txt.text
				dsarr[wk_id].result1_rate =  psource["QuestionBar" + qid].result1_average_txt.text
				dsarr[wk_id].result2_rate =  psource["QuestionBar" + qid].result2_average_txt.text
				dsarr[wk_id].result3_rate =  psource["QuestionBar" + qid].result3_average_txt.text
				dsarr[wk_id].result4_rate =  psource["QuestionBar" + qid].result4_average_txt.text
				dsarr[wk_id].result5_rate =  psource["QuestionBar" + qid].result5_average_txt.text
				dsarr[wk_id].result6_rate =  psource["QuestionBar" + qid].result6_average_txt.text
				dsarr[wk_id].result7_rate =  psource["QuestionBar" + qid].result7_average_txt.text
				dsarr[wk_id].result8_rate =  psource["QuestionBar" + qid].result8_average_txt.text
				dsarr[wk_id].result0_rate =  psource["QuestionBar" + qid].result0_average_txt.text
				dsarr[wk_id].effective_n =  psource["QuestionBar" + qid].effective_n_txt.text
				dsarr[wk_id].sum_score =  psource["QuestionBar" + qid].sum_score_txt.text
				dsarr[wk_id].top1 =  psource["QuestionBar" + qid].top1_txt.text
				dsarr[wk_id].top2and3 =  psource["QuestionBar" + qid].top2and3_txt.text
				dsarr[wk_id].nagetive =  psource["QuestionBar" + qid].nagetive_txt.text

				//サンプル数３０以下は参考値
				psource["QuestionBar" + qid].under30.removeMovieClip();
				if( dsarr[wk_id].samples < 30 ){
					psource["QuestionBar" + qid].attachMovie("under30", "under30", gdepth, {_x:220 })
					gdepth++;
				}
				trace("何番目：" + wk_id)

			}
*/


		}
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: OutputCSV
		Description		: ＣＳＶファイル出力
		Usage			: OutputCSV( Obj );
		Attributes		: Obj : （コンファームを呼出した呼出元クラスインスタンス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function OutputCSV(){
		var argumentsParam = new Object();
		argumentsParam.resultObj = dsarr
		argumentsParam.condseleted = gpath.cmorebtn.labeltxt
		argumentsParam.selecteddate = gpath.dselected_txt.text
		argumentsParam.outputtype = outputtype
		argumentsParam.resort_code = resort_code
		var csvPC:PendingCall = sobject.CRMResultOutputCSV( argumentsParam );
		csvPC.responder = new RelayResponder( this, "OutputCSV_Result", "Service_Fault" );
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: OutputCSV_Result
		Description		: ＣＳＶ出力正常終了時
		Usage			: OutputCSV_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function OutputCSV_Result(ReEvt:ResultEvent):Void{
		var FileObj:downloader = new downloader(gpath,ReEvt.result.filename,ReEvt.result.newname)
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: verifycondition
		Description		: 検索条件のチェック
		Usage			: verifycondition();
		Attributes		: Obj : （検索条件オブジェクト）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/05
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function verifycondition(){
		var retmsg = ""
		if(this.slctddate.length < 1){retmsg="日付を選択してください"}
		return retmsg
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: cboxtween
		Description		: 条件入力状態解除
		Usage			: cboxtween();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/30
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	public function cboxtween(obj,bl){
		if(obj._x < 0 && bl==undefined){
			var _xtween:Object = new Tween(obj, "_x", Back.easeOut, -20, 20, 0.5, true);
		}else if(!bl){
			obj._x = -1000;
		}else{
			var _xtween:Object = new Tween(obj, "_x", Back.easeOut, 20, -1000, 1, true);
		}
	}
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: Service_Fault
		Description		: 設問情報取得不正終了時イベント
		Usage			: Service_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function Service_Fault(FltEvt:FaultEvent):Void{
		var ErrorMsg = FltEvt.fault.description
		var HeaderMsg = "サービスエラー"
		var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, gpath, null, null, null)
		//gpath.loadmask._y = -1000
		myCond.waiting(gpath,false)
	}



	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getdetail
		Description		: 
		Usage			: getdetail(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by		h.miyahara	2007/04/26
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getdetail(ans,qid){
		gpath.detail.removeMovieClip();
		gpath.attachMovie("detail", "detail", gdepth++, {_x:200,_y:150})
		gpath.detail.detail_txt.text = "回答者情報読み込み中…"

		currentcondition.answer = ans
		currentcondition.questionnaire_id = qid
		//CFCメソッド呼出
		var dtPC:PendingCall = sobject.getAnswers(currentcondition);
		dtPC.responder = new RelayResponder( this, "getdetail_Result", "getAnswers_Fault" );
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getdetail_Result
		Description		: 
		Usage			: getdetail_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by		h.miyahara	2007/04/26
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getdetail_Result(ReEvt:ResultEvent):Void{
		if(ReEvt.result.mRecordsAvailable==0){
			gpath.detail.removeMovieClip();
		} else {
			var wk_txt = "顧客番号					チェックイン日				チェックアウト日"+"\n"
			for( var i=0; i<ReEvt.result.mRecordsAvailable; i++ ){
				wk_txt = wk_txt + ReEvt.result.items[i].customernumber + "			" + ReEvt.result.items[i].arrive_date + "			" + ReEvt.result.items[i].depart_date + "\n"
			}
			gpath.detail.ref_list = wk_txt
			gpath.detail.condition = detailstxt

			gpath.detail.onEnterFrame = function(){
			  this.detail_txt.text = this.ref_list
			  this.condition_txt.text = this.condition
			  delete this.onEnterFrame;
			};
		}
	}

}
