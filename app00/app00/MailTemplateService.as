﻿/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	Name			: MailTemplateService
	Description		: メールテンプレート情報取得
	Usage			: var XXXXXX:MailTemplateService = new MailTemplateService( datagridname, path );
	Attributes		: datagridname:DataGrid		（結果格納用データグリッドコンポーネント名）
					: path:MovieClip			（基準パス）
	ExtendsParam	: none
	Method			: initializedData 			初期設定
					: getMailTemplate 			メールテンプレートリスト取得
					: getMailTemplate_Result 	メールテンプレートリスト取得正常終了時イベント
					: getMailTemplate_Fault 	メールテンプレートリスト取得不正終了時イベント
					: SetTargetData 			編集対象取得～編集画面表示
					: DeleteTargetData 			テンプレート削除
					: deleteMailTemplate_Result	テンプレート削除正常終了時イベント
					: deleteMailTemplate_Fault	テンプレート削除不正終了時イベント
	Note			: none
	History			: 1) Coded by	nf)h.miyahara	2004/07/09
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
import mx.remoting.Service;
import mx.services.Log;
import mx.rpc.RelayResponder;
import mx.remoting.PendingCall;
import mx.remoting.debug.NetDebug;
import mx.remoting.debug.NetDebugConfig;
import mx.remoting.RecordSet;
import mx.rpc.ResultEvent;
import mx.rpc.FaultEvent;
import mx.controls.gridclasses.DataGridColumn
import mx.controls.DataGrid
import mx.transitions.easing.*;
import mx.transitions.Tween;
import mx.styles.CSSStyleDeclaration;
import commonfunc;

class app00.MailTemplateService extends MovieClip {

	var tgrid:DataGrid		//結果表示ViewComponent
	var gpath:MovieClip		//基準ムービークリップパス
	var rpath:MovieClip		//基準ムービークリップパス
	var resort_code:String	//リゾートコード（0:共通）
	var gdepth:Number		//Depth
	var sobject:Object		//CFCsobject
	var opath:String		//CSVopath
	var dsarr:Array			//データソース事前指定用配列
	var cfunc:commonfunc

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: MailTemplateService
		Description		: Constructor
		Usage			: MailTemplateService( myGrid:DataGrid, myPath:MovieClip );
		Attributes		: myGrid:DataGrid	（結果格納用データグリッドコンポーネント名）
						: myPath:MovieClip	（基準ＭＣパス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function MailTemplateService( myPath:MovieClip,r_id:String ) {
		gpath = myPath;
		rpath = _root;
		gdepth = gpath.getNextHighestDepth();
		resort_code = r_id;
		tgrid = myPath.mailtemplate_grd;
		cfunc = new commonfunc()

		var myDebug = mx.remoting.debug.NetDebug.initialize();
		sobject = new Service( "http://" + _global.hostAddress + "/flashservices/gateway/", null , _global.servicePath + ".app00.cfc.mailTemplate" , null , null ); 
		
		//NetDebbuger設定
		var config:NetDebugConfig = sobject.connection.getDebugConfig();
		config.app_server.trace = true;
		config.client.trace = true;
		
		//ＭＣ初期化
		initializedData();

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getMailTemplate
		Description		: メールテンプレートリスト取得
		Usage			: getMailTemplate();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function getMailTemplate(){

		//CFCメソッド呼出
		var MailtmpPC:PendingCall = sobject.gettemplatelist(resort_code);
		MailtmpPC.responder = new RelayResponder( this, "getMailTemplate_Result", "getMailTemplate_Fault" );

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getMailTemplate_Result
		Description		: メールテンプレートリスト取得成功時処理
		Usage			: getMailTemplate_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent		（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getMailTemplate_Result(ReEvt:ResultEvent):Void{
		var owner = this
		
		var RecSet:RecordSet = RecordSet(ReEvt.result);
		tgrid.dataProvider = RecSet

		//ローディング用ＭＣ削除処理
		gpath._x = 0
		cfunc.loadingdelete(gpath._parent)
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getMailTemplate_Fault
		Description		: メールテンプレートリスト取得失敗時処理
		Usage			: getMailTemplate_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent	（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getMailTemplate_Fault(FltEvt:FaultEvent):Void{
		//処理を記述
		cfunc.ShowConfirm( gpath , "メールテンプレートリストの取得に失敗しました。\rエラーメッセージ："+ FltEvt.fault.description, this , null );
		trace("getMailTemplate_Fault: " + FltEvt.fault.description);
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: initializedData
		Description		: 初期設定
		Usage			: initializedData();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function initializedData(){
		var owner = this

		//カラム設定
		var col:DataGridColumn = new DataGridColumn("Selected");
 		col.headerText = "";
 		col.width = 24;
		col.cellRenderer = "CheckCellRenderer";
		col.sortOnHeaderRelease = false;
		tgrid.addColumn(col);

		col = new DataGridColumn("resort_nm");
 		col.headerText = "施設名";
 		col.width = 120;
		col.sortOnHeaderRelease = false;
		tgrid.addColumn(col);

		col = new DataGridColumn("mail_div");
 		col.headerText = "送信種別";
 		col.width = 60;
		col.sortOnHeaderRelease = false;
		tgrid.addColumn(col);

		col = new DataGridColumn("mail_nm");
 		col.headerText = "テンプレート名";
 		col.width = 120;
		col.sortOnHeaderRelease = false;
		tgrid.addColumn(col);

		col = new DataGridColumn("mail_subject");
 		col.headerText = "件名";
 		col.width = 220;
		col.sortOnHeaderRelease = false;
		tgrid.addColumn(col);

		col = new DataGridColumn("mail_from");
 		col.headerText = "差出人";
 		col.width = 120;
		col.sortOnHeaderRelease = false;
		tgrid.addColumn(col);

		col = new DataGridColumn("fromaddress");
 		col.headerText = "差出人アドレス";
 		col.width = 160;
		col.sortOnHeaderRelease = false;
		tgrid.addColumn(col);

		col = new DataGridColumn("mailtemplate_id");
 		col.headerText = "詳細";
 		col.width = 32; 		
		col.cellRenderer = "DetailCellRenderer";
		col.sortOnHeaderRelease = false;
		tgrid.addColumn(col);

		//スタイル設定
		initGridStyle(tgrid);
		
		//メニュー切替
		gpath.changeBtn.onRelease = function(){
			owner.SetMenuList(owner.gpath)
		}

		//新規作成ボタン設定
		gpath.createBtn.onRelease = function(){
			owner.SetTargetData(-1);
		}
		
		//削除ボタン設定
		gpath.deleteBtn.onRelease = function(){
			var LenCheck:Boolean = false;
			for(var i=0; i< owner.tgrid.length; i++){
				if(owner.tgrid.getItemAt(i).Selected){
					LenCheck = true
				}
			}

			//チェックされている時のみ
			if( LenCheck ){
				owner.cfunc.ShowConfirm( this._parent , "選択したテンプレートを削除します。よろしいですか？", owner , owner.DeleteTargetData );
			} else {
				owner.cfunc.ShowConfirm( this._parent , "削除したいテンプレートをチェックして下さい。", owner , null );
			}

		}

		//CSVボタン設定
		gpath.csvBtn.onRelease = function(){
			owner.cfunc.ShowConfirm( this._parent , "一覧をＣＳＶファイルに出力します。よろしいですか？", owner , owner.OutputCSV );
		}

		//メールテンプレートリスト取得
		getMailTemplate();
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: SetTargetData
		Description		: 編集対象取得～編集画面表示
		Usage			: SetTargetData(index);
		Attributes		: index：	（編集対象ＩＮＤＥＸ）
		Note			: 編集用CellRendererよりコールされる。
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function SetTargetData(index){
		
		//グローバル変数に編集対象ＩＮＤＥＸ・データプロバイダを退避
		_global.targetIndex = index;
		_global.targetData = tgrid.dataProvider;

		//編集画面読込み
		//gpath.gotoAndStop("detail")
		gpath.maileditor.removeMovieClip();
		gpath.attachMovie("mailedit","maileditor",gdepth++,{parentobj:this})
		gpath.maileditor._x = (Stage.width/2)-(gpath.maileditor._width/2)
		gpath.maileditor._y = (Stage.height/2)-(gpath.maileditor._height/2)-20
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: DeleteTargetData
		Description		: 削除対象取得表示
		Usage			: DeleteTargetData( Obj );
		Attributes		: Obj : （コンファームを呼出した呼出元クラスインスタンス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function DeleteTargetData( Obj ){
		//インスタンス入替
		this = Obj
		var retArray:Array = new Array();
		for(var i=0; i< tgrid.length; i++){
			if(tgrid.getItemAt(i).Selected){
				retArray.push(tgrid.getItemAt(i).mailtemplate_id)
				//trace(tgrid.getItemAt(i).mailtemplate_id)
			}
		}

		//CFCメソッド呼出
		var MailtmpPC:PendingCall = Obj.sobject.deletetemplate( retArray );
		MailtmpPC.responder = new RelayResponder( this, "deleteMailTemplate_Result", "deleteMailTemplate_Fault" );
	}
	

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: deleteMailTemplate_Result
		Description		: 削除正常終了時
		Usage			: deleteMailTemplate_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function deleteMailTemplate_Result(ReEvt:ResultEvent):Void{
		tgrid.removeAllColumns();
		initializedData();
		trace("削除完了")
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: deleteMailTemplate_Fault
		Description		: 削除不正終了時
		Usage			: deleteMailTemplate_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function deleteMailTemplate_Fault(FltEvt:FaultEvent):Void{
		//処理を記述	trace("deleteMailTemplate_Fault: " + FltEvt.fault.description);
		cfunc.ShowConfirm( gpath , "削除が正常に行われませんでした。\rエラーメッセージ："+ FltEvt.fault.description, this , null );
		
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: initGridStyle
		Description		: 
		Usage			: initGridStyle(FltEvt:FaultEvent);
		Attributes		: 
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function initGridStyle(tg):Void {
		tg.headerHeight = 22;
		tg.setRowHeight(22);

		tg.setStyle("alternatingRowColors", Array(0xFFFFFF, 0xE7E4DC));
		tg.setStyle("hGridLines", true);
		tg.setStyle("hGridLineColor", 0x95946A);
		tg.setStyle("vGridLines", true);
		tg.setStyle("vGridLineColor", 0x95946A);
		tg.setStyle("borderStyle", "solid");
		tg.setVScrollPolicy("auto");
		// themeColor
		tg.setStyle("themeColor", 0xDEDEBE); 
		tg.setStyle("textSelectedColor", 0x333333);
		tg.setStyle("shadowCapColor", 0xFFFFFF);
		tg.setStyle("shadowColor", 0x336699);
		tg.setStyle("borderColor", 0xE9E1D6);
		tg.setStyle("themeColor", 0xE9E1D6);
		tg.setStyle("useRollOver", false);
		tg.setStyle("textSelectedColor", 0x333333);
		tg.setStyle("rollOverColor", 0xDEDEBE);

		tg.resizableColumns = false;
		tg.setStyle("headerColor", 0xE7E4DC);
		tg.setStyle("fontSize", 11);
		tg.setVScrollPolicy("auto");

		var headerStyles = new CSSStyleDeclaration();
		headerStyles.setStyle("fontSize", 11);
		headerStyles.setStyle("fontWeight", "normal");
		headerStyles.setStyle("fontFamily", "Verdana");
		tg.setStyle("headerStyle", headerStyles);
		tgrid.hScrollPolicy = "auto";

		tgrid.setVScrollPolicy("auto");

		//	--データグリッドアクセス完了後処理
		tgrid.doLater(this, "loadcomplete");
	}
}
