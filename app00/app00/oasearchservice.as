﻿/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	Name			: oasearchservice
	Description		: 
	Usage			: 
	Attributes		: 
	Extends			: 
	Method			: 
	Note			: none
	History			: 1) Coded by	2007/09/03
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
import mx.remoting.Service;
import mx.services.Log;
import mx.rpc.RelayResponder;
import mx.remoting.PendingCall;
import mx.remoting.debug.NetDebug;
import mx.remoting.debug.NetDebugConfig;
import mx.remoting.RecordSet;
import mx.rpc.ResultEvent;
import mx.rpc.FaultEvent;
import mx.controls.gridclasses.DataGridColumn
import mx.controls.DataGrid
import mx.controls.ComboBox
import mx.data.binding.*;
import mx.data.components.DataSet
import mx.transitions.easing.*;
import mx.transitions.Tween;
import app00.conditionsetting;
import app00.downloader;
import mx.controls.Alert;
import mx.styles.CSSStyleDeclaration;
import mx.managers.PopUpManager;
import mx.containers.Window;
import commonfunc

class app00.oasearchservice extends MovieClip {
	var tgrid:DataGrid			//結果表示ViewComponent
	var gpath:MovieClip			//基準ムービークリップパス
	var rpath:MovieClip			//基準ムービークリップパス
	var resort_code:String		//リゾートコード（0:共通）
	var gdepth:Number			//
	var sobject:Object			//CFCsobject
	var opath:String			//CSVopath
	var sresult:DataSet			//検索結果退避用オブジェクト
	var dsarr:Array				//データソース事前指定用配列
	var slctddate:Array			//選択日
	var cfunc:commonfunc
	var myCond:conditionsetting
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: oasearchservice
		Description		: Constructor
		Usage			: oasearchservice( myGrid:DataGrid, myPath:MovieClip );
		Attributes		: myGrid:DataGrid	（結果格納用データグリッドコンポーネント名）
						: myPath:MovieClip	（基準ＭＣパス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function oasearchservice(myPath:MovieClip,r_id:String) {	
		gpath = myPath;
		rpath = _root;
		gdepth = gpath.getNextHighestDepth();
		resort_code = r_id;
		tgrid = myPath.results_grd;
		tgrid.setSize(960,431.1)

		slctddate = new Array();
		cfunc = new commonfunc()
		myCond = new conditionsetting(rpath.conditionbox,this,gpath);
		var myDebug = mx.remoting.debug.NetDebug.initialize();
		sobject = new Service( "http://" + _global.hostAddress + "/flashservices/gateway/", null , _global.servicePath + ".app00.cfc.oascreen" , null , null ); 
		
		//NetDebbuger設定
		var config:NetDebugConfig = sobject.connection.getDebugConfig();
		config.app_server.trace = true;
		config.client.trace = true;
		
		//ＭＣ初期化
		initializedData();
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: initializedData
		Description		: 初期設定
		Usage			: initializedData();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
						: 2) Updated by	nf)h.miyahara	2005/10/11 文言変更
						: 3) Updated by	nf)h.miyahara	2005/10/11 満足度評価設問追加
						: 4) Updated by	nf)h.miyahara	2005/10/13 カテゴリＩＤパラメータ追加
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function initializedData(){
		var owner = this
		//初期状態：結果エリア・オプション用ボード非表示
		tgrid._visible = false;
//		rpath.conditionbox.initializedData(rpath.conditionbox,this,gpath)
		rpath.dconditionbox.parents = this

		myCond.initializedData();

		//設問一覧
		var CtgryPC:PendingCall = sobject.getCategory(resort_code);
		CtgryPC.responder = new RelayResponder( this, "getCategory_Result", "getCategory_Fault" );

		//設問選択MC表示
		gpath.qselectionview.q_id = ""
		gpath.qselectionview.onRelease = function(){
			owner.cboxtween(owner.gpath.qselection);
			owner.cboxtween(owner.rpath.dconditionbox,false);
			owner.cboxtween(owner.rpath.conditionbox,false);
		}

		//日付条件入力MC表示
		gpath.dconditionview.onRelease = function(){
			if(owner.gpath.qselected_txt.text.length==0){
				var ErrorMsg = "先に設問を選択してください"
				var HeaderMsg = "設問選択がされていません "
				var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, owner.gpath, null, null, null)
			}else{
				owner.cboxtween(owner.rpath.dconditionbox);
				owner.cboxtween(owner.gpath.qselection,false);
				owner.cboxtween(owner.rpath.conditionbox,false);
			}
		}

		//集計条件入力MC表示
		gpath.conditionview.onRelease = function(){
			if(owner.gpath.dselected_txt.text.length==0){
				var ErrorMsg = "先に集計期間を選択してください"
				var HeaderMsg = "日付選択がされていません     "
				var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, owner.gpath, null, null, null)
			}else{
				owner.cboxtween(owner.rpath.conditionbox);
				owner.cboxtween(owner.gpath.qselection,false);
				owner.cboxtween(owner.rpath.dconditionbox,false);
			}
		}
		//ファイル出力ボタン設定
		gpath.menu02._visible = gpath.menu02.enabled = false;
		gpath.menu02.onRelease = function(){
			if(!owner.tgrid._visible){
				var ErrorMsg = "出力データがありません。先に集計を行ってください。"
				var HeaderMsg = "出力不可                                   "
				var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, gpath, null, null, null)
			} else {
				var AlertObj = mx.controls.Alert.show("一覧をファイルに出力します。よろしいですか？", "ファイル出力", Alert.YES | Alert.NO, owner.gpath, myClickHandler, null , Alert.OK);
			}
		}
		//CSV出力ボタンイベント
		var myClickHandler = new Object();
		myClickHandler = function(evt){
			if(evt.detail == Alert.YES){
				owner.gpath.opcondition._visible = false;
				owner.gpath.opcondition.enabled = false;
				owner.OutputCSV();
			}
		}
		
		//ファイル出力ボタン設定
//		gpath.menu03._visible = gpath.menu03.enabled = false;
		gpath.menu03.onRelease = function(){
			if(!owner.tgrid._visible){
				var ErrorMsg = "出力データがありません。先に集計を行ってください。"
				var HeaderMsg = "出力不可                                   "
				var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, gpath, null, null, null)
			} else {
				var AlertObj = mx.controls.Alert.show("一覧をプレビューします。よろしいですか？", "ファイル出力", Alert.YES | Alert.NO, owner.gpath, ViewHandler, null , Alert.OK);
			}
		}

		//CSV出力ボタンイベント
		var ViewHandler = new Object();
		ViewHandler = function(evt){
			if(evt.detail == Alert.YES){
				owner.gpath.opcondition._visible = false;
				owner.gpath.opcondition.enabled = false;
				owner.OutputView();
			}
		}
		//日付条件指定オプションイベント
		var cbListener:Object = new Object();
		cbListener.click = function(evt_obj:Object) {
			owner.slctddate = new Array();
			owner.rpath.dconditionbox.initializedData();
		};
		gpath.dselection.addEventListener("click", cbListener);

		//日付オーバーフロー
		gpath.dmorebtn.enabled = false
		gpath.dmorebtn.onRelease = function(){
			owner.myCond.viewmorechips(owner.gpath.morechips,this)
		}
		//gpath.morechips.labeltxt.autoSize = "left"

		//集計条件オーバーフロー
		gpath.cmorebtn.enabled = false
		gpath.cmorebtn.onRelease = function(){
			owner.myCond.viewmorechips(owner.gpath.morechips,this)
		}
		//タブ設定
		TabInitialize();
		initGridStyle(tgrid);
		initGridEvent(tgrid);
		
	}
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: searchbtnevent
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function searchbtnevent(){
		var owner = this
		//検索ボタン設定
		rpath.conditionbox.menu00.owner = this
		gpath.menu00.onRelease = rpath.conditionbox.menu00.onRelease = function(){
			owner = _global.currentscreen
			var wk_val = owner.verifycondition()
			if(wk_val.length==0){
				var arg_resort_code = owner.resort_code
				if(owner.resort_code == 0){
					arg_resort_code = owner.myCond.paramsetting(owner.rpath.conditionbox.cond_items.conditem12.checkbox_sp.content);
				} else {
					arg_resort_code = owner.myCond.resortcodesetting(owner.rpath.conditionbox.cond_items.conditem12.checkbox_sp.content,owner.resort_code);
				}
				
				//グリッド設定
				var CatQPC:PendingCall = owner.sobject.getCategoryQs(owner.gpath.qselectionview.q_cid , arg_resort_code);
				CatQPC.responder = new RelayResponder( owner, "getCategoryQs_Result", "Service_Fault" );
				owner.cboxtween(owner.rpath.dconditionbox,false);
				owner.cboxtween(owner.rpath.conditionbox,false);
				owner.cboxtween(owner.gpath.qselection,false);
				owner.myCond.waiting(owner.gpath,true)
				owner.tgrid._visible = false;
			}else{
				var ErrorMsg = wk_val
				var HeaderMsg = "集計できません                 "
				var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, gpath, null, null, null)
			}
		}
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: resetbtnevent
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function resetbtnevent(){
		var owner = this
		//クリアボタン設定
		rpath.conditionbox.menu01.owner = this
		gpath.menu01.onRelease = rpath.conditionbox.menu01.onRelease = function(){
			owner = _global.currentscreen
			//trace(owner.gpath)
			owner.tgrid._visible = false;
			owner.myCond.conditionreset();
		}
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getCategory_Result
		Description		: カテゴリデータ取得正常終了時イベント
		Usage			: getCategory_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getCategory_Result(ReEvt:ResultEvent):Void{
		var owner = this
		var sy = 40
		var sx = 14
		var answer_typenms = new Array();
		for(var i=0; i<ReEvt.result.mRecordsAvailable; i++){
			if(i%12==0 && i!=0){
				sx = 480
				sy = 40
			}
			var newmc = "q_panel"+ReEvt.result.items[i].questionnairecategory_id
			gpath.qselection.attachMovie("qselection_panel",newmc,gdepth++,{_x:sx,_y:sy})
			sy = sy + gpath.qselection[newmc]._height + 5
			//gpath.qselection[newmc].answertype_cb.dropdown;
			gpath.qselection[newmc].q_cid = ReEvt.result.items[i].questionnairecategory_id
			gpath.qselection[newmc].q_nm.text = ReEvt.result.items[i]. cs_qlabel
			gpath.qselection[newmc].q_txt.text = ReEvt.result.items[i]. questionnairecategory_nm

			//	--設問選択ボタンイベント
			gpath.qselection[newmc].viewbtn.q_cid = ReEvt.result.items[i].questionnairecategory_id
			gpath.qselection[newmc].viewbtn.q_cnm = ReEvt.result.items[i].questionnairecategory_nm
			gpath.qselection[newmc].viewbtn.onRelease = function(){
				owner.selctedhighlite(this._parent)
				owner.gpath.qselectionview.q_cid = this._parent.q_cid
				owner.gpath.qselectionview.q_cnm = this._parent.q_cnm
				owner.gpath.qselectionview.q_id = this._parent.answertype_cb.selectedItem.data
				owner.gpath.qselectionview.q_nm = this._parent.answertype_cb.selectedItem.name

				owner.gpath.qselected_txt.text = this._parent.q_nm.text + " " + this._parent.q_txt.text + ":" + this._parent.answertype_cb.selectedItem.label
			}
			//	--設問
			var CntntPC:PendingCall = sobject.getContents(ReEvt.result.items[i].questionnairecategory_id);
			CntntPC.responder = new RelayResponder( this, "getContents_Result", "getContents_Fault" );
			/*

			var wk_ds = ReEvt.result.items[i].q_contents
			var wk_combo = gpath.qselection["q_panel" + ReEvt.result.items[i].questionnairecategory_id].answertype_cbanswertype_cb.
			gpath.qselection["q_panel" + ReEvt.result.items[i].questionnairecategory_id].answertype_cb.dropdown;
			gpath.qselection["q_panel" + ReEvt.result.items[i].questionnairecategory_id].q_contents = wk_ds;
			answer_typenms.push(gpath.qselection["q_panel" + ReEvt.result.items[i].questionnairecategory_id])
			//gpath.qselection["q_panel" + ReEvt.result.items[i].questionnairecategory_id].answertype_cb.doLater(this, "test")
			//setContentssource(wk_ds,wk_combo)
			*/

		}
		//スタイル設定
	//	initGridStyle(tgrid);
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getCategory_Fault
		Description		: カテゴリデータ取得不正終了時イベント
		Usage			: getCategory_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getCategory_Fault(FltEvt:FaultEvent):Void{
		var ErrorMsg = "カテゴリの取得ができませんでした。\rエラーメッセージ："+ FltEvt.fault.description
		var HeaderMsg = "カテゴリ取得失敗                 "
		var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, gpath, null, null, null)
		myCond.waiting(gpath,false)
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getContents_Result
		Description		: 内容データ取得正常終了時イベント
		Usage			: getContents_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getContents_Result(ReEvt:ResultEvent):Void{
		//trace( ReEvt.result.mRecordsAvailable + "件取得しました。");
		var owner =this;
		var ComboData = new Array();
		var AllID = new Array();
		var AllName = new Array();
		for(var i=0; i<ReEvt.result.mRecordsAvailable; i++){
			AllID.push(ReEvt.result.items[i].questionnaire_id)
			AllName.push(ReEvt.result.items[i].questionnaire_nm)
			var wk_name=new Array(ReEvt.result.items[i].questionnaire_nm)
			ComboData.push({data:ReEvt.result.items[i].questionnaire_id,label:ReEvt.result.items[i].questionnaire_nm,name:wk_name})
		}
		ComboData.push({data:AllID,label:"すべて",name:AllName})
		gpath.qselection["q_panel" + ReEvt.result.items[0].questionnairecategory_id].answertype_cb.dataProvider = ComboData.reverse();

		//tgrid.getColumnAt(6).headerText = ReEvt.result.items[0].questionnaire_nm;

		rpath.conditionbox.c_qcategory.cond_txt.text = ReEvt.result.items[0].questionnairecategory_nm
		//スタイル設定
		//initGridStyle(tgrid);
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setContentssource
		Description		: 内容データ取得正常終了時イベント
		Usage			: setContentssource();
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function setContentssource(ds,cb):Void{
		var owner =this;
		var ComboData = new Array();
		var AllID = new Array();
		var AllName = new Array();
		for(var i=0; i<ds.mRecordsAvailable; i++){
			AllID.push(ds.items[i].questionnaire_id)
			AllName.push(ds.items[i].questionnaire_nm)
			var wk_name=new Array(ds.items[i].questionnaire_nm)
			ComboData.push({data:ds.items[i].questionnaire_id,label:ds.items[i].questionnaire_nm,name:wk_name})
		}
		ComboData.push({data:AllID,label:"すべて",name:AllName})
		cb.dataProvider = ComboData.reverse();

		//tgrid.getColumnAt(6).headerText = ReEvt.result.items[0].questionnaire_nm;

		rpath.conditionbox.c_qcategory.cond_txt.text = ds.items[0].questionnairecategory_nm
		//スタイル設定
		//initGridStyle(tgrid);
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getContents_Fault
		Description		: 内容データ取得不正終了時イベント
		Usage			: getContents_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getContents_Fault(FltEvt:FaultEvent):Void{
		var ErrorMsg = "内容の取得ができませんでした。\r"+ FltEvt.fault.description
		var HeaderMsg = "内容データ取得失敗             "
		var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, gpath, null, null, null)
		myCond.waiting(gpath,false)
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: TabInitialize
		Description		: タブ設定
		Usage			: TabInitialize();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function TabInitialize():Void{
		gpath.tabChildren = true
		rpath.conditionbox.tabChildren = true
		rpath.conditionbox.c_qcategory_cb.tabIndex = 2;
		rpath.conditionbox.answertype_cb.tabIndex = 4;
		gpath.menu00.tabIndex = 6;
		gpath.clearBtn.tabIndex = 8;
		gpath.menu02.tabIndex = 10;
		gpath.previewBtn.tabIndex = 12;
		gpath.printBtn.tabIndex = 14;
		gpath.changeBtn.tabIndex = 16;
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getOAAnswers_Result
		Description		: 検索実行正常終了時イベント
		Usage			: getOAAnswers_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getOAAnswers_Result(ReEvt:ResultEvent):Void{
		//初期設定
		tgrid.removeAllColumns();
		var owner = this;
		if( ReEvt.result.mRecordsAvailable == 0){
			var ErrorMsg = "該当レコードはありませんでした。\r検索条件を変更して再度検索を行ってください。"
			var HeaderMsg = "検索結果                                   "
			var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, gpath, null, null, null)
			var RecSet:RecordSet = RecordSet(ReEvt.result);
			tgrid.dataProvider = RecSet
			gpath.SearchBar.records_txt.text = RecSet.length
			tgrid._visible = false;
			gpath.menu02._visible = gpath.menu02.enabled = true;
		} else {

			//OAのみ
			if(ReEvt.result.mTitles.length<6){
				var col =  new DataGridColumn("reserve_code")
				col.width = 100;
				col.headerText = "予約番号";
				tgrid.addColumn(col);

				var col =  new DataGridColumn("referencenumber")
				col.width = 120;
				col.headerText = "照会用番号";
				tgrid.addColumn(col);

				var col =  new DataGridColumn("depart_date")
				col.width = 100;
				//col.headerText = "出発日";
				//ラベル変更
				col.headerText = gpath.fieldselect_cb.selectedItem.label
				tgrid.addColumn(col);
				
				if(gpath.fieldselect_cb.selectedIndex != 0){
					var col =  new DataGridColumn("orgdepart_date")
					col.width = 80;
					col.headerText = "出発日";
					tgrid.addColumn(col);
				}

				var cbobj = gpath.qselection["q_panel" + gpath.qselectionview.q_cid].answertype_cb.selectedItem.name
				for(var i=0; i<cbobj.length; i++){
					var col =  new DataGridColumn("OA"+i)
					col.width = 420;
					col.headerText =  cbobj[i];
					col.cellRenderer =  "imageCellRender";
					tgrid.addColumn(col);
				}
			} else {
				var col =  new DataGridColumn("reserve_code")
				col.width = 100;
				col.headerText = "予約番号";
				tgrid.addColumn(col);

				var col =  new DataGridColumn("referencenumber")
				col.width = 120;
				col.headerText = "照会用番号";
				tgrid.addColumn(col);

				var col =  new DataGridColumn("depart_date")
				col.width = 80;
				//col.headerText = "出発日";
				//ラベル変更
				col.headerText = gpath.fieldselect_cb.selectedItem.label
				tgrid.addColumn(col);
				
				if(gpath.fieldselect_cb.selectedIndex != 0){
					var col =  new DataGridColumn("orgdepart_date")
					col.width = 80;
					col.headerText = "出発日";
					tgrid.addColumn(col);
				}

				var col =  new DataGridColumn("gender_nm")
				col.width = 60;
				col.headerText = "性別";
				tgrid.addColumn(col);

				var col =  new DataGridColumn("age_nm")
				col.width = 80;
				col.headerText = "年代";
				tgrid.addColumn(col);

				var col =  new DataGridColumn("companion_nm")
				col.width = 100;
				col.headerText = "同行者";
				tgrid.addColumn(col);

				var col =  new DataGridColumn("job_nm")
				col.width = 100;
				col.headerText = "職業";
				tgrid.addColumn(col);

				var col =  new DataGridColumn("trip_nm")
				col.width = 100;
				col.headerText = "国内旅行回数";
				tgrid.addColumn(col);

				var col =  new DataGridColumn("numPerson")
				col.width = 40;
				col.headerText = "人数";
				tgrid.addColumn(col);

				var col =  new DataGridColumn("nights")
				col.width = 40;
				col.headerText = "泊数";
				tgrid.addColumn(col);

				var col =  new DataGridColumn("area")
				col.width = 100;
				col.headerText = "エリア";
				tgrid.addColumn(col);

				var col =  new DataGridColumn("prefecture")
				col.width = 100;
				col.headerText = "都道府県";
				tgrid.addColumn(col);

				var col =  new DataGridColumn("resort_nm")
				col.width = 100;
				col.headerText = "施設";
				tgrid.addColumn(col);

				var col =  new DataGridColumn("totalsalesamount")
				col.width = 100;
				col.headerText = "客室総売上";
				tgrid.addColumn(col);

				var col =  new DataGridColumn("roomsalesitemid")
				col.width = 100;
				col.headerText = "パッケージ";
				tgrid.addColumn(col);

				//	2010/09/27 全項目統一化に変更
				var col =  new DataGridColumn("roomtype")
				col.width = 100;
				col.headerText = "部屋タイプ";
				tgrid.addColumn(col);

				var col =  new DataGridColumn("roomnumber")
				col.width = 74;
				col.headerText = "部屋番号";
				tgrid.addColumn(col);

				var col =  new DataGridColumn("reservationroute")
				col.width = 100;
				col.headerText = "予約経路";
				tgrid.addColumn(col);

				var col =  new DataGridColumn("satisfy")
				col.width = 80;
				col.headerText = "100点満点";
				col.sortedUp =  true;
				col.sortOnHeaderRelease = false;
				tgrid.addColumn(col);
				//	2010/09/27 全項目統一化に変更


				for(var i=0; i<gpath.qfield.length; i++){
//					trace(i + ":" + gpath.qfield[i])
					var col =  new DataGridColumn("Q"+i)
					col.width = 120;
					col.name =  "Q"+i;
					col.sortedUp =  true;
					col.sortOnHeaderRelease = false;
					col.headerText =  gpath.qfield[i];
					tgrid.addColumn(col);
				}

				var cbobj = gpath.qselection["q_panel" + gpath.qselectionview.q_cid].answertype_cb.selectedItem.name
				for(var i=0; i<cbobj.length; i++){
					var col =  new DataGridColumn("OA"+i)
					col.width = 420;
					col.headerText =  cbobj[i];
					col.cellRenderer =  "imageCellRender";
					tgrid.addColumn(col);
				}
				//	--S権限以下は項目非表示
				if(_global.resort_code != 's'){
				}
				
				//	--全施設参照権限以外は項目非表示
				if(_global.resort_code!=0){
					tgrid.removeColumnAt(tgrid.getColumnIndex("resort_nm"));
				}

			}

			//レコードセットオブジェクトに結果代入
			var RecSet:RecordSet = RecordSet(ReEvt.result);
			tgrid.dataProvider = RecSet

			gpath.menu02._visible = gpath.menu02.enabled = true;
			tgrid.setSize(tgrid.width,tgrid.height)

			//取得件数表示
			gpath.SearchBar.records_txt.text = RecSet.length

			//スペックシート表示
			if(_global.UserInfo.login_dv == 's' || _global.UserInfo.login_dv == 'b'){
				var CellListener = new Object();
				CellListener.cellPress = function(event) {
					if( owner.tgrid.getItemAt(event.itemIndex).customernumber != undefined){
						if(owner.tgrid.getItemAt(event.itemIndex).customernumber == 'IMPDATA'){
							var ErrorMsg = "過去データの為、\r顧客データはありません。" 
							var HeaderMsg = "顧客データはありません                                "
							var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, gpath, null, null, null)
						} else {
							if(owner.tgrid.getColumnAt(event.columnIndex).cellRenderer == "imageCellRender"){
								var wk_content = owner.tgrid.getItemAt(event.itemIndex)[owner.tgrid.getColumnAt(event.columnIndex).columnName]
								owner.createWindow(wk_content);
							} else {
								owner.SetTargetData(event.itemIndex)
							}
						}
					} 


					
				}
				tgrid.addEventListener("cellPress", CellListener);
			} else {
				var CellListener = new Object();
				CellListener.cellPress = function(event) {
					if(owner.tgrid.getColumnAt(event.columnIndex).cellRenderer == "imageCellRender"){
						var wk_content = owner.tgrid.getItemAt(event.itemIndex)[owner.tgrid.getColumnAt(event.columnIndex).columnName]
						owner.createWindow(wk_content);
					}
				}
				tgrid.addEventListener("cellPress", CellListener);
			}
		}
		//	--データグリッドアクセス完了後処理
		tgrid.doLater(this, "delay");
	}
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: SetTargetData
		Description		: 編集対象取得～編集画面表示
		Usage			: SetTargetData(index);
		Attributes		: index：	（編集対象ＩＮＤＥＸ）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/06
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function SetTargetData(index){
		
		//グローバル変数に編集対象ＩＮＤＥＸ・データプロバイダを退避
		_global.targetIndex = index;
		_global.targetData = tgrid.dataProvider;

		var wk_dp = 3000
		gpath.createEmptyMovieClip("SpecSheetScreen", wk_dp);
		gdepth++;
		gpath.SpecSheetScreen.beginFill(0xCCCCCC);
		gpath.SpecSheetScreen.moveTo(0,0);
		gpath.SpecSheetScreen.lineTo(Stage.width, 0);
		gpath.SpecSheetScreen.lineTo(Stage.width, Stage.height);
		gpath.SpecSheetScreen.lineTo(0, Stage.height);
		gpath.SpecSheetScreen.lineTo(0, 0);
		gpath.SpecSheetScreen.endFill();
		//gpath.SpecSheetScreen.enabled = false
		gpath.SpecSheetScreen.onRollOver = function(){}

		//スペックシート表示画面読込み
		var LoadClip:Loadswf = new Loadswf( "app00/specSheet.swf", gpath.SpecSheetScreen );
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: delay
		Description		: 
		Usage			: 
		Attributes		: 
		Note			: none
		History			: 1) Coded by	2007/09/06 16:56
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function delay() {
		myCond.waiting(gpath,false)
		if(tgrid.dataProvider.length != 0){
			tgrid._visible = true;
		}
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: delay
		Description		: 
		Usage			: 
		Attributes		: 
		Note			: none
		History			: 1) Coded by	2007/09/06 16:56
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function loadcomplete() {
		//ロード完了後処理
		gpath._x = 0
		cfunc.loadingdelete(gpath._parent)
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getOAAnswers_Fault
		Description		: 検索実行不正終了時イベント
		Usage			: getOAAnswers_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getOAAnswers_Fault(FltEvt:FaultEvent):Void{
		var ErrorMsg = "検索できませんでした。\rエラーメッセージ："+ FltEvt.fault.description
		var HeaderMsg = "検索失敗                                   "
		var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, gpath, null, null, null)
		myCond.waiting(gpath,false)
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: OutputCSV
		Description		: ＣＳＶファイル出力
		Usage			: OutputCSV( Obj );
		Attributes		: Obj : （コンファームを呼出した呼出元クラスインスタンス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function OutputCSV(){
		var tmpFields = tgrid.columnNames;
		//trace("tmpFields: " + tmpFields.length)
		var headerText = new Array();
		for(var n=0; n<tgrid.columnCount; n++){
			headerText.push(tgrid.getColumnAt(n).headerText)
		}
		dsarr = new Array();
		for(var i=0; i<tgrid.length; i++){
			var tmpObj:Object = new Object();
			for(var c=0; c<tmpFields.length; c++){
				var fieldname = tmpFields[c]
				if( tgrid.getItemAt(i)[ fieldname ] == null ){
					tmpObj[ tmpFields[c] ] = "未回答";
				}else{
					tmpObj[ tmpFields[c] ] = tgrid.getItemAt(i)[ fieldname ];
				}
			}
			dsarr.push(tmpObj)
		}
		var argumentsParam = new Object();
		argumentsParam.resultObj = dsarr
		argumentsParam.fieldnames = tmpFields
		argumentsParam.headerText = headerText
		argumentsParam.qstseleted = gpath.qselected_txt.text
		argumentsParam.condseleted = gpath.cmorebtn.labeltxt
		argumentsParam.selecteddate = gpath.dselected_txt.text
		argumentsParam.resort_code = resort_code;

//		var csvPC:PendingCall = sobject.OAResultOutputCSV(argumentsParam);
		var csvPC:PendingCall = sobject.OAResulPreview(argumentsParam);
		csvPC.responder = new RelayResponder( this, "OutputCSV_Result", "Service_Fault" );
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: OutputCSV_Result
		Description		: ＣＳＶ出力正常終了時
		Usage			: OutputCSV_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function OutputCSV_Result(ReEvt:ResultEvent):Void{
		var FileObj:downloader = new downloader(gpath,ReEvt.result.filename,ReEvt.result.newname)
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: OutputCSV
		Description		: ＣＳＶファイル出力
		Usage			: OutputCSV( Obj );
		Attributes		: Obj : （コンファームを呼出した呼出元クラスインスタンス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function OutputView(){
		var tmpFields = tgrid.columnNames;
		var headerText = new Array();
		for(var n=0; n<tgrid.columnCount; n++){
			headerText.push(tgrid.getColumnAt(n).headerText)
		}
		dsarr = new Array();
		var testarr:Array = new Array();
		for(var i=0; i<tgrid.length; i++){
			var tmpObj:Object = new Object();
			for(var c=0; c<tmpFields.length; c++){
				var fieldname = tmpFields[c]
				if( tgrid.getItemAt(i)[ fieldname ] == null ){
					tmpObj[ tmpFields[c] ] = "未回答";
				}else{
					tmpObj[ tmpFields[c] ] = tgrid.getItemAt(i)[ fieldname ];
				}
			}
			dsarr.push(tmpObj)
			testarr.push(i)
		}
		
		var argumentsParam = new Object();
		argumentsParam.resultObj = dsarr
		argumentsParam.fieldnames = tmpFields
		argumentsParam.headerText = headerText
		argumentsParam.qstseleted = gpath.qselected_txt.text
		argumentsParam.condseleted = gpath.cmorebtn.labeltxt
		argumentsParam.selecteddate = gpath.dselected_txt.text
		argumentsParam.resort_code = resort_code;

		var csvPC:PendingCall = sobject.OAResulPreview(argumentsParam);
/*
		gpath.createEmptyMovieClip("loader_mc", gpath.getNextHighestDepth());
		gpath.loader_mc.resultObj = dsarr;
		gpath.loader_mc.fields = tmpFields;
		gpath.loader_mc.headerText = headerText;
		gpath.loader_mc.qstseleted = gpath.qselected_txt.text;
		gpath.loader_mc.condseleted = gpath.cmorebtn.labeltxt;
		gpath.loader_mc.selecteddate = gpath.dselected_txt.text;
		gpath.loader_mc.resort_code = resort_code;
//		gpath.loader_mc.getURL("test.cfm","_blank","POST")
			
		var sdData = new LoadVars();

		sdData.testarr = testarr;
		sdData.resultObj = dsarr;
		sdData.fields = tmpFields;
		sdData.headerText = headerText;
		sdData.qstseleted = gpath.qselected_txt.text;
		sdData.condseleted = gpath.cmorebtn.labeltxt;
		sdData.selecteddate = gpath.dselected_txt.text;
		sdData.resort_code = resort_code;
		sdData.send("test.cfm", "_blank", "POST");
*/			
			
			
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: cboxtween
		Description		: 条件入力状態解除
		Usage			: cboxtween();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/30
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	public function cboxtween(obj,bl){
		if(obj._x < 0 && bl==undefined){
			var _xtween:Object = new Tween(obj, "_x", Back.easeOut, -20, 20, 0.5, true);
		}else if(!bl){
			obj._x = -1000;
		}else{
			var _xtween:Object = new Tween(obj, "_x", Back.easeOut, 20, -1000, 1, true);
		}
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: verifycondition
		Description		: 検索条件のチェック
		Usage			: verifycondition();
		Attributes		: Obj : （検索条件オブジェクト）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/05
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function verifycondition(){
		var retmsg = ""
		if(slctddate.length < 1){retmsg="日付を選択してください"}
		if(gpath.qselectionview.q_id.length < 1){retmsg="集計対象の設問を選択してください"}
		return retmsg
	}
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getCategoryQs_Result
		Description		: 同一カテゴリ７段階評価取得正常終了時イベント
		Usage			: getCategoryQs_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2005/04/26
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getCategoryQs_Result(ReEvt:ResultEvent):Void{
		gpath.qfield = new Array();
		for(var i=0; i<ReEvt.result.mRecordsAvailable; i++){
			gpath.qfield.push(ReEvt.result.items[i].questionnaire_nm)
			trace(i + ":" +ReEvt.result.items[i].questionnaire_nm)				
		}
		//検索条件オブジェクト作成
		var conditionParam = new Object();
		var cbox = rpath.conditionbox
		conditionParam.questionnaire_id = gpath.qselectionview.q_id
		conditionParam.questionnaire_nm = gpath.qselectionview.q_nm
		conditionParam.questionnairecategory_id = gpath.qselectionview.q_cid
		conditionParam.dcondtion = gpath.dselection.selected
		conditionParam.selecteddays = slctddate
		conditionParam.arg_fieldselect = gpath.fieldselect_cb.selectedItem.data
		conditionParam.arg_generation = myCond.paramsetting(rpath.conditionbox.cond_items.conditem00.checkbox_sp.content)
		conditionParam.arg_gender = myCond.paramsetting(rpath.conditionbox.cond_items.conditem01);
		conditionParam.arg_nights = myCond.paramsetting(rpath.conditionbox.cond_items.conditem02.checkbox_sp.content);
		conditionParam.arg_job = myCond.paramsetting(rpath.conditionbox.cond_items.conditem03);
//		conditionParam.arg_together = myCond.paramsetting(rpath.conditionbox.cond_items.conditem04);
		conditionParam.arg_together = myCond.paramsetting(rpath.conditionbox.cond_items.conditem04.checkbox_sp.content);
		conditionParam.arg_area = myCond.paramsetting(rpath.conditionbox.cond_items.conditem05);
		conditionParam.arg_residence = myCond.paramsetting(rpath.conditionbox.cond_items.conditem13.checkbox_sp.content);

		//conditionParam.arg_roomtype = myCond.paramsetting(rpath.conditionbox.cond_items.conditem06);
		conditionParam.arg_roomtype = myCond.paramsetting(rpath.conditionbox.cond_items.conditem06.checkbox_sp.content);
		conditionParam.arg_purpose = myCond.paramsetting(rpath.conditionbox.cond_items.conditem07.checkbox_sp.content);
		conditionParam.arg_times = myCond.paramsetting(rpath.conditionbox.cond_items.conditem08);
		conditionParam.arg_reservation = myCond.paramsetting(rpath.conditionbox.cond_items.conditem09.checkbox_sp.content);
		conditionParam.arg_cognition = myCond.paramsetting(rpath.conditionbox.cond_items.conditem10.checkbox_sp.content);
		conditionParam.arg_roomnumber = myCond.paramsetting(rpath.conditionbox.cond_items.conditem11.checkbox_sp.content);

//		conditionParam.arg_resort_code = resort_code;
		if(resort_code == 0){
			conditionParam.arg_resort_code = myCond.paramsetting(rpath.conditionbox.cond_items.conditem12.checkbox_sp.content);
		} else {
			conditionParam.arg_resort_code = myCond.resortcodesetting(rpath.conditionbox.cond_items.conditem12.checkbox_sp.content,resort_code);
			//conditionParam.arg_resort_code = new Array();
			//conditionParam.arg_resort_code.push(resort_code);
		}

		conditionParam.arg_answermethod_id = rpath.conditionbox.answermethod_cb.selectedItem.data;
		conditionParam.arg_group_id = rpath.conditionbox.group_cb.selectedItem.data;

		conditionParam.viewchange = gpath.viewchange_chk.selected;

		//CFCメソッド呼出
		try{
			var OAanswPC:PendingCall = sobject.getOAAnswers(conditionParam);
			OAanswPC.responder = new RelayResponder( this, "getOAAnswers_Result", "getOAAnswers_Fault" );
		}
		catch (myError:Error) { 
			var ErrorMsg = myError
			var HeaderMsg = "サービスエラー"
			var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, gpath, null, null, null)
			myCond.waiting(gpath,false)
		} 
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: Service_Fault
		Description		: 設問情報取得不正終了時イベント
		Usage			: Service_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function Service_Fault(FltEvt:FaultEvent):Void{
		var ErrorMsg = FltEvt.fault.description
		var HeaderMsg = "サービスエラー"
		var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, gpath, null, null, null)
		myCond.waiting(gpath,false)
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: initGridStyle
		Description		: 
		Usage			: initGridStyle(FltEvt:FaultEvent);
		Attributes		: 
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function initGridStyle(tg):Void {
		tg.headerHeight = 32;
		tg.setRowHeight(60);

		tg.setStyle("alternatingRowColors", Array(0xFFFFFF, 0xE7E4DC));
		tg.setStyle("hGridLines", true);
		tg.setStyle("hGridLineColor", 0x95946A);
		tg.setStyle("vGridLines", true);
		tg.setStyle("vGridLineColor", 0x95946A);
		tg.setStyle("borderStyle", "solid");
		tg.setVScrollPolicy("auto");
		// themeColor
		tg.setStyle("themeColor", 0xDEDEBE); 
		tg.setStyle("textSelectedColor", 0x333333);
		tg.setStyle("shadowCapColor", 0xFFFFFF);
		tg.setStyle("shadowColor", 0x336699);
		tg.setStyle("borderColor", 0xE9E1D6);
		tg.setStyle("themeColor", 0xE9E1D6);
		tg.setStyle("useRollOver", false);
		tg.setStyle("textSelectedColor", 0x333333);
		tg.setStyle("rollOverColor", 0xDEDEBE);

		tg.resizableColumns = false;
		tg.setStyle("headerColor", 0xE7E4DC);
		tg.setStyle("fontSize", 12);
		tg.setVScrollPolicy("auto");

		var headerStyles = new CSSStyleDeclaration();
		headerStyles.setStyle("fontSize", 12);
		headerStyles.setStyle("fontFamily", "Verdana");
		tg.setStyle("headerStyle", headerStyles);
		tgrid.hScrollPolicy = "auto";

		//	--データグリッドアクセス完了後処理
		tgrid.doLater(this, "loadcomplete");
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: initGridEvent
		Description		: 
		Usage			: initGridEvent(FltEvt:FaultEvent);
		Attributes		: 
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function initGridEvent(tg):Void {
		//カスタムソート設定
		var owner = this;
		var GridListener = new Object();
		tgrid.removeEventListener("headerRelease", GridListener);
		GridListener.headerRelease = function(event) {
			var sortflg = owner.tgrid.getColumnAt(event.columnIndex).sortedUp
			if(sortflg != undefined){
				var wk_key = owner.tgrid.columnNames[event.columnIndex]
				wk_key = owner.str_replace(wk_key,"Q","Sort");
				if(sortflg){
					owner.tgrid.dataProvider.sortItemsBy( [wk_key] ,null, Array.NUMERIC | Array.DESCENDING);
				} else {
					owner.tgrid.dataProvider.sortItemsBy( [wk_key] ,null, Array.NUMERIC);
				}
				owner.tgrid.getColumnAt(event.columnIndex).sortedUp = !owner.tgrid.getColumnAt(event.columnIndex).sortedUp;
			}
		}
		tgrid.addEventListener("headerRelease", GridListener);

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: qselected
		Description		: 
		Usage			: 
		Attributes		: 
		Note			: none
		History			: 1) Coded by	2007/09/19 9:51
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function selctedhighlite(mc):Void{
		var targetmc = gpath.qselection
		for (var resetobj in targetmc) {
			targetmc[resetobj].selectedmc._visible = false
			if (typeof (targetmc[resetobj]) == "movieclip" && resetobj == mc._name) {
				targetmc[resetobj].selectedmc._visible = true
			}
		}
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: createWindow
		Description		: 
		Usage			: 
		Attributes		: 
		Note			: none
		History			: 1) Coded by	2010/08/06 5:23
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function createWindow(wk_content){
		var owner = this
		my_win.deletePopUp();
		var my_win:MovieClip = mx.managers.PopUpManager.createPopUp(_root, Window, true, {closeButton:true, contentPath:wk_content})
		my_win._visible = false

		var winListener:Object = new Object();
		winListener.click = function() {
			 my_win.deletePopUp();
		};
		my_win.addEventListener("click", winListener);
		
		winListener.complete = function(evt_obj:Object) {

			var wk_w = my_win._width
			var wk_h = my_win._height
			my_win.setSize(Stage.width/2 , wk_h + 25);

			my_win.scaleX = 80;
			my_win.scaleY = 80;

			my_win.setSize(wk_w , wk_h + 25);

			my_win.content.x_scale = 50;
			my_win.content.y_scale = 50;

			my_win._x = (Stage.width/2) - (my_win._width/2);
			my_win._y = (Stage.height/2) - (my_win._height/2);
			my_win._visible = true
		}

		my_win.addEventListener("complete", winListener);
		
		
	}
	
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: str_replace
		Description		: 
		Usage			:
						 _str			検索・置換の対象となる文字列
						 _search_str	検索する文字列
						 _replace_str	置換する文字列
		Attributes		: 
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function str_replace( _str , _search_str, _replace_str)	{
		var strArray = _str.split(_search_str);
		return strArray.join( _replace_str );
	}		
	
}
