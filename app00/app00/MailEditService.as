﻿/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	Name			: MailEditService
	Description		: メールテンプレート情報取得
	Usage			: var XXXXXX:MailEditService = new MailEditService( Path );
	Attributes		: Path:MovieClip	（基準パス）
	ExtendsParam	: none
	Method			: 
	Note			: none
	History			: 1) Coded by	nf)h.miyahara	2004/07/09
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
import mx.remoting.Service;
import mx.services.Log;
import mx.rpc.RelayResponder;
import mx.remoting.PendingCall;
import mx.remoting.debug.NetDebug;
import mx.remoting.debug.NetDebugConfig;
import mx.rpc.ResultEvent;
import mx.rpc.FaultEvent;
import commonfunc;

class app00.MailEditService extends MovieClip {

	var gpath:MovieClip			//基準ムービークリップパス
	var gdepth:Number					//Depth
	var targetIndex:Number				//対象ＩＮＤＥＸ
	var targetData:Object				//対象データプロバイダ
	var sobject:Object			//CFCsobject
	var MailID:Number					//mailtemlate_id
	var cfunc:commonfunc
	var parentobj:app00.MailTemplateService


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: MailEditService
		Description		: Constructor
		Usage			: MailEditService( myPath:MovieClip );
		Attributes		: myPath:MovieClip	（基準ＭＣパス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function MailEditService() {
		gpath = this;
		targetIndex = _global.targetIndex;			//MailTemplateServiceクラスで設定
		targetData = _global.targetData;			//MailTemplateServiceクラスで設定
		gdepth = gpath.getNextHighestDepth();
		MailID = targetData.getItemAt(targetIndex).mailtemplate_id	//ＩＮＤＥＸよりテンプレートＩＤを取得
		cfunc = new commonfunc()

		if( targetIndex < 0 ){
			MailID = -1
		}
		//trace("parentobj :  " + parentobj)
		parentobj.getMailTemplate();
		var myDebug = mx.remoting.debug.NetDebug.initialize();
		sobject = new Service( "http://" + _global.hostAddress + "/flashservices/gateway/", null , _global.servicePath + ".app00.cfc.mailTemplate" , null , null ); 
		
		//NetDebbuger設定
		var config:NetDebugConfig = sobject.connection.getDebugConfig();
		config.app_server.trace = true;
		config.client.trace = true;
		
		//ＭＣ初期化
		initializedData();

	}

	
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getMailDetail
		Description		: メールテンプレート詳細情報取得
		Usage			: getMailDetail( mail_id );
		Attributes		: mail_id:Number	（メールテンプレートＩＤ）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function getMailDetail(){

		
		var MaildtlPC:PendingCall = sobject.getresort( parentobj.resort_code);
		MaildtlPC.responder = new RelayResponder( this, "getresort_Result", "getMailDetail_Fault" );
	}



	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getMailDetail_Result
		Description		: メールテンプレート詳細取得成功時処理
		Usage			: getMailDetail_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent		（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getresort_Result(ReEvt:ResultEvent):Void{
		gpath.resort_cb.dataProvider = ReEvt.result.items
		for(var i=0;i<gpath.resort_cb.dataProvider.length;i++){
			if(gpath.resort_cb.dataProvider[i].data == parentobj.resort_code){
				gpath.resort_cb.selectedIndex = i
			}
		}

		//ＣＦＣメソッド呼出
		var MaildtlPC:PendingCall = sobject.gettemplate( MailID,parentobj.resort_code);
		MaildtlPC.responder = new RelayResponder( this, "getMailDetail_Result", "getMailDetail_Fault" );		
		
	}
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getMailDetail_Result
		Description		: メールテンプレート詳細取得成功時処理
		Usage			: getMailDetail_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent		（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getMailDetail_Result(ReEvt:ResultEvent):Void{

		if( ReEvt.result.mRecordsAvailable > 0 ){
			//取得データセット
			gpath.mail_div.selectedIndex = ReEvt.result.items[0].mail_div;
			gpath.resort_txt.text = ReEvt.result.items[0].resort_nm;
			gpath.data0_txt.text = ReEvt.result.items[0].mail_from;
			gpath.data1_txt.text = ReEvt.result.items[0].fromaddress;
			gpath.data2_txt.text = ReEvt.result.items[0].testaddress;
			gpath.data3_txt.text = ReEvt.result.items[0].mail_subject;
			gpath.data4_txt.text = ReEvt.result.items[0].mail_body;
			gpath.data5_txt.text = ReEvt.result.items[0].mail_nm;
			//trace( ReEvt.result.mRecordsAvailable + "件取得しました。");
		} else {
			for(var i=0; i<6; i++ ){
				gpath["data" + i + "_txt"].text = "";
			}
		}

		//ページング処理呼出
		setPagingInfo()
		
		for(var i=0;i<gpath.resort_cb.dataProvider.length;i++){
			if(gpath.resort_cb.dataProvider[i].data == ReEvt.result.items[0].resort_code){
				gpath.resort_cb.selectedIndex = i
			}
		}
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getMailDetail_Fault
		Description		: メールテンプレート取得失敗時処理
		Usage			: getMailDetail_Result(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent	（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getMailDetail_Fault(FltEvt:FaultEvent):Void{
		cfunc.ShowConfirm( gpath , "メールテンプレートの取得に失敗しました。\rエラーメッセージ："+ FltEvt.fault.description, this , null );
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setPagingInfo
		Description		: ページング処理
		Usage			: setPagingInfo();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/21
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function setPagingInfo(){
		var nowRecord = targetIndex;  
		var totalRecords = targetData.length;
		var Owner = this;

		//ボタン制御
		gpath.paging.BackBtn.enabled = true;
		gpath.paging.NextBtn.enabled = true;
		gpath.paging.LastBtn.enabled = true;
		gpath.paging.FirstBtn.enabled = true;

		//ページ数表示
		gpath.paging.page_txt.text = (nowRecord+1) + "/" + totalRecords;
		
		//ボタンクリックイベント
		gpath.paging.BackBtn.onRelease = function(){
			Owner.targetIndex = nowRecord - 1;
			Owner.MailID = Owner.targetData.getItemAt(Owner.targetIndex).mailtemplate_id
			Owner.getMailDetail();
		}
		gpath.paging.NextBtn.onRelease = function(){
			Owner.targetIndex = nowRecord + 1;
			Owner.MailID = Owner.targetData.getItemAt(Owner.targetIndex).mailtemplate_id
			Owner.getMailDetail();
		}

		gpath.paging.LastBtn.onRelease = function(){
			Owner.targetIndex = totalRecords - 1;
			Owner.MailID = Owner.targetData.getItemAt(Owner.targetIndex).mailtemplate_id
			Owner.getMailDetail();
		}

		gpath.paging.FirstBtn.onRelease = function(){
			Owner.targetIndex = 0;
			Owner.MailID = Owner.targetData.getItemAt(Owner.targetIndex).mailtemplate_id
			Owner.getMailDetail();
		}

		//ボタン制御
		gpath.paging.onEnterFrame = function(){
			if( nowRecord == 0 ){
				this.BackBtn.enabled = false;
				this.FirstBtn.enabled = false;
				this.BackBtn.gotoAndStop(1)
				this.FirstBtn.gotoAndStop(1)
			} else {
				this.BackBtn.enabled = true;
				this.FirstBtn.enabled = true;
			}

			if( nowRecord + 1 == totalRecords ){
				this.NextBtn.enabled = false;
				this.LastBtn.enabled = false;
				this.NextBtn.gotoAndStop(1)
				this.LastBtn.gotoAndStop(1)
			} else {
				this.NextBtn.enabled = true;
				this.LastBtn.enabled = true;
			}
		}
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: SendTestMail
		Description		: メールテスト送信
		Usage			: SendTestMail( Obj );
		Attributes		: Obj : （コンファームを呼出した呼出元クラスインスタンス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function SendTestMail( Obj ){
		//インスタンス入替
		this = Obj
		var arguments1 = gpath.data0_txt.text
		var arguments2 = gpath.data1_txt.text
		var arguments3 = gpath.data2_txt.text
		var arguments4 = gpath.data3_txt.text
		var arguments5 = gpath.data4_txt.text
		var arguments6 = gpath.resort_cb.selectedItem.data
		var conditionParam = new Object({sendName:arguments1,sendAddress:arguments2,recieveAddress:arguments3,subject:arguments4,body:arguments5,resort_code:arguments6});

		//メール送信ＣＦＣ接続
		var	MailSendService = new Service( "http://" + _global.hostAddress + "/flashservices/gateway/", null , _global.servicePath + ".app00.cfc.sendMail" , null , null ); 
		var MaildtlPC:PendingCall = MailSendService.sendTest( conditionParam );
		MaildtlPC.responder = new RelayResponder( this, "SendMail_Result", "SendMail_Fault" );

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: SendMail_Result
		Description		: メール送信ファンクション完了
		Usage			: SendMail_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent		（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function SendMail_Result(ReEvt:ResultEvent):Void{
		cfunc.ShowConfirm( gpath , "メールを送信しました", this , null );
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: SendMail_Fault
		Description		: メール送信ファンクション完了
		Usage			: SendMail_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent	（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function SendMail_Fault(FltEvt:FaultEvent):Void{
		cfunc.ShowConfirm( gpath , "メール送信を実行できませんでした。\r" + FltEvt.fault.description, this , null );
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: RegistTargetData
		Description		: メールテンプレート登録
		Usage			: RegistTargetData( Obj );
		Attributes		: Obj : （コンファームを呼出した呼出元クラスインスタンス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function RegistTargetData( Obj ){
		//インスタンス入替
		this = Obj
		
		//CFCメソッド呼出
		var MaildtlPC:PendingCall = sobject.settemplate( 
													 MailID
													,gpath.data0_txt.text
													,gpath.data1_txt.text
													,gpath.data2_txt.text
													,gpath.data3_txt.text
													,gpath.data4_txt.text
													,gpath.data5_txt.text
													,gpath.mail_div.selectedItem.data
													,gpath.resort_cb.selectedItem.data
									);
		MaildtlPC.responder = new RelayResponder( this, "setMailDetail_Result", "setMailDetail_Fault" );

	}
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setMailDetail_Result
		Description		: 
		Usage			: setMailDetail_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent		（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function setMailDetail_Result(ReEvt:ResultEvent):Void{
		//テンプレート一覧画面読込み
		//var LoadClip:LoadImage = new LoadImage( "mailTemplate.swf", gpath )
		//gpath.gotoAndStop("list")
		parentobj.getMailTemplate();
		gpath.removeMovieClip();

	}
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setMailDetail_Fault
		Description		: 
		Usage			: setMailDetail_Fault(ReEvt:ResultEvent);
		Attributes		: FltEvt:FaultEvent	（ＣＦＣリターン結果）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function setMailDetail_Fault(FltEvt:FaultEvent):Void{
		cfunc.ShowConfirm( gpath , "テンプレート登録が正常に行われませんでした。\rエラーメッセージ："+ FltEvt.fault.description, this , null );
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: initializedData
		Description		: 初期設定
		Usage			: initializedData();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function initializedData(){
		var Owner = this;
		gpath._parent.CoveredStage_mc.removeMovieClip();
		//test
		//var myConfirm:ConfirmWindow = new ConfirmWindow( gpath , "Your IP：\r"+ _global.UserInfo.ipAddress, this , null );

		//メニュー切替
		gpath.changeBtn.onRelease = function(){
			Owner.SetMenuList(Owner.gpath)
		}

		//テスト送信ボタン設定
		gpath.sendBtn.onRelease = function(){
			//var myConfirm:ConfirmWindow = new ConfirmWindow( this._parent , "現在の内容でテストメールを送信します。よろしいですか？", this.Owner , this.Owner.SendTestMail,0 );
			Owner.cfunc.ShowConfirm( this._parent , "現在の内容でテストメールを送信します。\rよろしいですか？", Owner , Owner.SendTestMail );
		}
		
		//登録ボタン設定
		gpath.registBtn.onRelease = function(){
			//var myConfirm:ConfirmWindow = new ConfirmWindow( this._parent , "現在の内容でテンプレートを登録します。よろしいですか？", this.Owner , this.Owner.RegistTargetData,0 );
			Owner.cfunc.ShowConfirm( this._parent , "現在の内容でテンプレートを登録します。\rよろしいですか？", Owner , Owner.RegistTargetData );
		}

		//キャンセルボタン設定
		gpath.closebtn.onRelease = function(){
			Owner.gpath.removeMovieClip();
		}

		//検索条件スタイル設定
		for(var i=0; i<4; i++){
			gpath[ "data" + i + "_txt" ].borderStyle = "none"
			gpath[ "data" + i + "_txt" ].maxChars = 255
			
		}
		gpath.data4_txt.borderColor = 0xCCC7B9;
		gpath.data1_txt.restrict = "A-Za-z0-9\\-\\@\\_\\/\\#\\$\\%\\&\\.";
		gpath.data2_txt.restrict = "A-Za-z0-9\\-\\@\\_\\/\\#\\$\\%\\&\\.";
		var myTextFormat = new TextFormat(); 
		gpath.data0_txt.myTextFormat.font = "_ゴシック"; 
		gpath.data0_txt.setNewTextFormat(myTextFormat);
		gpath.data1_txt.myTextFormat.font = "_ゴシック"; 
		gpath.data1_txt.setNewTextFormat(myTextFormat);
		gpath.data2_txt.myTextFormat.font = "_ゴシック"; 
		gpath.data2_txt.setNewTextFormat(myTextFormat);
		gpath.data3_txt.myTextFormat.font = "_ゴシック"; 
		gpath.data3_txt.setNewTextFormat(myTextFormat);
		gpath.data4_txt.myTextFormat.font = "_ゴシック"; 
		gpath.data4_txt.setNewTextFormat(myTextFormat);
		gpath.data5_txt.myTextFormat.font = "_ゴシック"; 
		gpath.data5_txt.setNewTextFormat(myTextFormat);


		//ローディング用ＭＣ削除処理
		gpath.onEnterFrame = function(){
			var addSpeed = 0.7
			if( this.resort_cb != undefined ){
				this._parent.Loading.removeMovieClip();
				this._parent.CoveredStage_mc._height = this._parent.CoveredStage_mc._height*addSpeed; 
				if(this._parent.CoveredStage_mc._height<0){
					this._parent.CoveredStage_mc.removeMovieClip();
					delete this.onEnterFrame
				}
			}
		}

		TabInitialize();

		//詳細情報取得
		getMailDetail();
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: TabInitialize
		Description		: タブ設定
		Usage			: TabInitialize();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/30
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function TabInitialize():Void{
		gpath.tabChildren = true
		var tabcnt = 0
		gpath.data5_txt.tabIndex = tabcnt++;
		gpath.mail_div.tabIndex = tabcnt++;
		gpath.data0_txt.tabIndex = tabcnt++;
		gpath.data1_txt.tabIndex = tabcnt++;
		gpath.data2_txt.tabIndex = tabcnt++;
		gpath.data3_txt.tabIndex = tabcnt++;
		gpath.data4_txt.tabIndex = tabcnt++;
		gpath.sendBtn.tabIndex = tabcnt++;
		gpath.registBtn.tabIndex = tabcnt++;
		gpath.closeBtn.tabIndex = tabcnt++;
	}
}