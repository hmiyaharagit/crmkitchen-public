﻿/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	Name			: csmulti
	Description		: CRM用検索画面
	Usage			: var XXXXXX:csmulti = new csmulti( path );
	Attributes		: path:MovieClip			（基準パス）
	Extends			: CommonFunction(Class)
	Method			: initializedData		初期設定
					: getCRMGuestList_Result	検索実行正常終了時イベント
					: getCRMGuestList_Fault	検索実行不正終了時イベント
	Note			: none
	History			: 1) Coded by	nf)h.miyahara	2004/07/09
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
import mx.remoting.Service;
import mx.services.Log;
import mx.rpc.RelayResponder;
import mx.remoting.PendingCall;
import mx.remoting.debug.NetDebug;
import mx.remoting.debug.NetDebugConfig;
import mx.remoting.RecordSet;
import mx.rpc.ResultEvent;
import mx.rpc.FaultEvent;
import mx.controls.gridclasses.DataGridColumn
import mx.controls.ComboBox
import mx.containers.ScrollPane
import mx.data.binding.*;
import mx.data.components.DataSet
import mx.transitions.easing.*;
import mx.transitions.Tween;
import app00.conditionsetting;
import app00.downloader;
import mx.controls.Alert;
import mx.styles.CSSStyleDeclaration;
//import app00.commonfunc
import commonfunc

class app00.csmulti extends MovieClip {
	var gpath:MovieClip			//基準ムービークリップパス
	var rpath:MovieClip			//基準ムービークリップパス
	var gdepth:Number			//Depth
	var resort_code:String		//リゾートコード（0:共通）
	var sobject:Object			//CFCsobject
	var opath:String			//CSVOutputPath
	var sresult:DataSet			//検索結果退避用オブジェクト
	var psource:MovieClip		//ScrollPane内MovieClip
	var dsarr:Array				//データソース事前指定用配列
	var slctddate:Array			//選択日
	var outputtype:Number		//ＣＳＶ出力指示
	var cfunc:commonfunc
	var myCond:conditionsetting

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: csmulti
		Description		: Constructor
		Usage			: csmulti( myPath:MovieClip );
		Attributes		: myGrid:DataGrid	（結果格納用データグリッドコンポーネント名）
						: myPath:MovieClip	（基準ＭＣパス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function csmulti(myPath:MovieClip,r_id:String) {
		gpath = myPath;
		rpath = _root;
		gdepth = gpath.getNextHighestDepth();
		resort_code = r_id;
		dsarr = new Array();
		slctddate = new Array();
		outputtype = 0
		cfunc = new commonfunc()
		//myCond = new conditionsetting(gpath.conditionbox,this);
		myCond = new conditionsetting(rpath.conditionbox,this,gpath);
		var myDebug = mx.remoting.debug.NetDebug.initialize();
		sobject = new Service( "http://" + _global.hostAddress + "/flashservices/gateway/", null , _global.servicePath + ".app00.cfc.csmulti" , null , null ); 
		
		//NetDebbuger設定
		var config:NetDebugConfig = sobject.connection.getDebugConfig();
		config.app_server.trace = true;
		config.client.trace = true;
		
		//ＭＣ初期化
		initializedData();
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: initializedData
		Description		: 初期設定
		Usage			: initializedData();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function initializedData(){
		var owner = this

		//設問一覧
		var CtgryPC:PendingCall = sobject.getCategory(resort_code);
		CtgryPC.responder = new RelayResponder( this, "getCategory_Result", "getCategory_Fault" );

		//初期状態：結果エリア・オプション用ボード非表示
		//gpath.loadmask._y = 0;
		//gpath.loadmask.gotoAndStop(2)
		gpath.ResultsPanel._visible = false;
		rpath.dconditionbox.parents = this
		myCond.initializedData();

		//ScrollPane設定
		gpath.ResultsPanel.results_sp.borderStyle = "none"
		gpath.ResultsPanel.results_sp.vPageScrollSize = gpath.ResultsPanel.results_sp.height;
		gpath.ResultsPanel.results_sp.vLineScrollSize = gpath.ResultsPanel.results_sp.height;

		//ScrollPane内Content設定
		gpath.ResultsPanel.results_sp.contentPath = "results"
		psource = gpath.ResultsPanel.results_sp.content
/*
		//クリアボタン設定
		 rpath.conditionbox.menu01.onRelease = function(){
			owner = _global.currentscreen
			owner.tgrid._visible = false;
			owner.myCond.conditionreset();
		}
*/
		//設問選択MC表示
		gpath.qselectionview.q_id = ""
		gpath.qselectionview.onRelease = function(){
			owner.cboxtween(owner.gpath.qselection);
			owner.cboxtween(owner.rpath.dconditionbox,false);
			owner.cboxtween(owner.rpath.conditionbox,false);
		}

		//日付条件入力MC表示
		gpath.dconditionview.onRelease = function(){
			if(owner.gpath.qselected_txt.text.length==0){
				var ErrorMsg = "先に設問を選択してください"
				var HeaderMsg = "設問選択がされていません "
				var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, owner.gpath, null, null, null)
			}else{
				owner.cboxtween(owner.rpath.dconditionbox);
				owner.cboxtween(owner.gpath.qselection,false);
				owner.cboxtween(owner.rpath.conditionbox,false);
			}
		}

		//集計条件入力MC表示
		gpath.conditionview.onRelease = function(){
			if(owner.gpath.dselected_txt.text.length==0){
				var ErrorMsg = "先に集計期間を選択してください"
				var HeaderMsg = "日付選択がされていません     "
				var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, owner.gpath, null, null, null)
			}else{
				owner.cboxtween(owner.rpath.conditionbox);
				owner.cboxtween(owner.gpath.qselection,false);
				owner.cboxtween(owner.rpath.dconditionbox,false);
			}
		}

		//出力指示イベント
		gpath.opcondition.output_type0.onRelease = function(){
			this._parent.output_type1.selectedicon.gotoAndStop(1);
			this.selectedicon.gotoAndStop(2);
			this._parent.outputtype = 0
			var AlertObj = mx.controls.Alert.show("一覧をファイルに出力します。よろしいですか？", "ファイル出力", Alert.YES | Alert.NO, owner.gpath, myClickHandler, null , Alert.OK);
		}
		gpath.opcondition.output_type1.onRelease = function(){
			this._parent.output_type0.selectedicon.gotoAndStop(1);
			this.selectedicon.gotoAndStop(2);
			this._parent.outputtype = 1
			var AlertObj = mx.controls.Alert.show("一覧をファイルに出力します。よろしいですか？", "ファイル出力", Alert.YES | Alert.NO, owner.gpath, myClickHandler, null , Alert.OK);
		}

		//CSV出力ボタンイベント
		var myClickHandler = new Object();
		myClickHandler = function(evt){
			if(evt.detail == Alert.YES){
				owner.outputtype = owner.gpath.opcondition.outputtype;
				owner.gpath.opcondition._visible = false;
				owner.gpath.opcondition.enabled = false;
				owner.OutputCSV();
			}
		}

		//ファイル出力ボタン設定
		gpath.menu02._visible = gpath.menu02.enabled = false;
		gpath.opcondition._visible = gpath.opcondition.enabled = false;
		gpath.menu02.onRelease = function(){
			if(!owner.gpath.ResultsPanel._visible){
				var ErrorMsg = "出力データがありません。先に集計を行ってください。"
				var HeaderMsg = "ファイル出力不可                                   "
				var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, gpath, null, null, null)
			} else {
				var aptween:Object = new Tween(owner.gpath.opcondition, "_alpha", Strong.easeIn, 0, 100, 0.5, true);
				owner.gpath.opcondition._visible = true;
				owner.gpath.opcondition.enabled =  true;
			}
		}

		//日付条件指定オプションイベント
		var cbListener:Object = new Object();
		cbListener.click = function(evt_obj:Object) {
			owner.slctddate = new Array();
			owner.rpath.dconditionbox.initializedData();
		};
		gpath.dselection.addEventListener("click", cbListener);

		//日付オーバーフロー
		gpath.dmorebtn.enabled = false
		gpath.dmorebtn.onRelease = function(){
			owner.myCond.viewmorechips(owner.gpath.morechips,this)
		}
		//gpath.morechips.labeltxt.autoSize = "left"

		//集計条件オーバーフロー
		gpath.cmorebtn.enabled = false
		gpath.cmorebtn.onRelease = function(){
			owner.myCond.viewmorechips(owner.gpath.morechips,this)
		}

		//タブ設定
		TabInitialize();

	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: searchbtnevent
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function searchbtnevent(){
		var owner = this
		//検索ボタン設定
		rpath.conditionbox.menu00.owner = this
		gpath.menu00.onRelease = rpath.conditionbox.menu00.onRelease = function(){
			owner = _global.currentscreen
			var wk_val = owner.verifycondition()
			if(wk_val.length==0){
				var cbox = owner.rpath.conditionbox
				//検索条件オブジェクト作成
				var conditionParam = new Object();
				conditionParam.questionnaire_id = owner.gpath.qselectionview.q_id
				conditionParam.dcondtion = owner.gpath.dselection.selected
				conditionParam.selecteddays = owner.slctddate
				conditionParam.arg_generation = owner.myCond.paramsetting(owner.rpath.conditionbox.cond_items.conditem00.checkbox_sp.content)
				conditionParam.arg_gender = owner.myCond.paramsetting(owner.rpath.conditionbox.cond_items.conditem01);
				conditionParam.arg_nights = owner.myCond.paramsetting(owner.rpath.conditionbox.cond_items.conditem02.checkbox_sp.content);
				conditionParam.arg_job = owner.myCond.paramsetting(owner.rpath.conditionbox.cond_items.conditem03);
			//	conditionParam.arg_together = owner.myCond.paramsetting(owner.rpath.conditionbox.cond_items.conditem04);
				conditionParam.arg_together = owner.myCond.paramsetting(owner.rpath.conditionbox.cond_items.conditem04.checkbox_sp.content);
				conditionParam.arg_area = owner.myCond.paramsetting(owner.rpath.conditionbox.cond_items.conditem05);
				conditionParam.arg_residence = owner.myCond.paramsetting(owner.rpath.conditionbox.cond_items.conditem13.checkbox_sp.content);

				conditionParam.arg_roomtype = owner.myCond.paramsetting(owner.rpath.conditionbox.cond_items.conditem06.checkbox_sp.content);
				conditionParam.arg_purpose = owner.myCond.paramsetting(owner.rpath.conditionbox.cond_items.conditem07.checkbox_sp.content);
				conditionParam.arg_times = owner.myCond.paramsetting(owner.rpath.conditionbox.cond_items.conditem08);
				conditionParam.arg_reservation = owner.myCond.paramsetting(owner.rpath.conditionbox.cond_items.conditem09.checkbox_sp.content);
				conditionParam.arg_cognition = owner.myCond.paramsetting(owner.rpath.conditionbox.cond_items.conditem10.checkbox_sp.content);
				conditionParam.arg_roomnumber = owner.myCond.paramsetting(owner.rpath.conditionbox.cond_items.conditem11.checkbox_sp.content);


				if(owner.resort_code == 0){
					conditionParam.arg_resort_code = owner.myCond.paramsetting(owner.rpath.conditionbox.cond_items.conditem12.checkbox_sp.content);
						if(conditionParam.arg_resort_code.length == 0){
							conditionParam.arg_resort_code = new Array();
//trace("conditionParam.arg_resort_code:" + conditionParam.arg_resort_code)
							//conditionParam.arg_resort_code.push(0);
						}
				} else {
					conditionParam.arg_resort_code = owner.myCond.resortcodesetting(owner.rpath.conditionbox.cond_items.conditem12.checkbox_sp.content,owner.resort_code);
					//conditionParam.arg_resort_code = new Array();
					//conditionParam.arg_resort_code.push(owner.resort_code);
				}

/*
				if(conditionParam.arg_resort_code.length == 0){
					conditionParam.arg_resort_code = new Array();
					conditionParam.arg_resort_code.push(0);
				}
*/
				//conditionParam.arg_resort_code = owner.resort_code;
				conditionParam.arg_answermethod_id = owner.rpath.conditionbox.answermethod_cb.selectedItem.data;
				conditionParam.arg_group_id = owner.rpath.conditionbox.group_cb.selectedItem.data;
				conditionParam.arg_cross_id = owner.rpath.conditionbox.cross_cb.selectedItem.data;

				//CFCメソッド呼出
				var CSTotlPC:PendingCall = owner.sobject.getAnswers(conditionParam);
				CSTotlPC.responder = new RelayResponder( owner, "getAnswers_Result", "Service_Fault" );
				owner.cboxtween(owner.rpath.dconditionbox,false);
				owner.cboxtween(owner.rpath.conditionbox,false);
				owner.cboxtween(owner.gpath.qselection,false);
				owner.myCond.waiting(owner.gpath,true)
				owner.gpath.ResultsPanel._visible = false;
			} else {
				var ErrorMsg = wk_val
				var HeaderMsg = "集計できません                 "
				var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, gpath, null, null, null)
			}
		}
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: resetbtnevent
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function resetbtnevent(){
		var owner = this
		//クリアボタン設定
		rpath.conditionbox.menu01.owner = this
		gpath.menu01.onRelease = rpath.conditionbox.menu01.onRelease = function(){
			owner = _global.currentscreen
			trace(owner.gpath)
			owner.tgrid._visible = false;
			owner.myCond.conditionreset();
		}
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getCategory_Result
		Description		: カテゴリデータ取得正常終了時イベント
		Usage			: getCategory_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getCategory_Result(ReEvt:ResultEvent):Void{
		var owner = this
		var sy = 40
		var sx = 14
		for(var i=0; i<ReEvt.result.mRecordsAvailable; i++){
			if(i%12==0 && i!=0){
				sx = sx + 320	
				sy = 40
			}
			var newmc = "q_panel"+ReEvt.result.items[i].questionnaire_id
			gpath.qselection.attachMovie("qselection_panel",newmc,gdepth++,{_x:sx,_y:sy})
			sy = sy + gpath.qselection[newmc]._height + 4
			gpath.qselection[newmc].answertype_cb.dropdown;
			gpath.qselection[newmc].q_id = ReEvt.result.items[i].questionnaire_id
			gpath.qselection[newmc].q_nm.text = ReEvt.result.items[i]. cs_qlabel
			gpath.qselection[newmc].q_txt.text = ReEvt.result.items[i]. questionnaire_nm
			//	--設問選択ボタンイベント
			gpath.qselection[newmc].viewbtn.q_id = ReEvt.result.items[i].questionnaire_id
			gpath.qselection[newmc].viewbtn.q_nm = ReEvt.result.items[i].questionnaire_nm
			gpath.qselection[newmc].viewbtn.onRelease = function(){
				owner.selctedhighlite(this._parent)
				owner.gpath.qselectionview.q_id = this._parent.q_id
				owner.gpath.qselectionview.q_nm = this._parent.q_nm
				owner.gpath.qselected_txt.text = this._parent.q_nm.text + " " + this._parent.q_txt.text
			}
		}

		//ロード完了後処理
		rpath.conditionbox.c_qcategory.cond_txt.text = ReEvt.result.items[0].questionnairecategory_nm
		gpath._x = 0
		cfunc.loadingdelete(gpath._parent)

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getCategory_Fault
		Description		: カテゴリデータ取得不正終了時イベント
		Usage			: getCategory_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getCategory_Fault(FltEvt:FaultEvent):Void{
		var ErrorMsg = "カテゴリの取得ができませんでした。\rエラーメッセージ："+ FltEvt.fault.description
		var HeaderMsg = "カテゴリ取得失敗                 "
		var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, gpath, null, null, null)
		myCond.waiting(gpath,false)
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getstyle_Result
		Description		: 
		Usage			: getstyle_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	2007/09/10 16:07
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getstyle_Result(ReEvt:ResultEvent):Void{
		gpath.styledata = new Object();
		gpath.headertext = new Array()
		var wk_darray = new Array();
		var wk_narray = new Array();
		for(var i=0; i<ReEvt.result.mRecordsAvailable; i++){
			wk_darray.push(ReEvt.result.items[i].items_id)
			wk_narray.push(ReEvt.result.items[i].items_nm)
			gpath.headertext.push(ReEvt.result.items[i].items_nm + "実数")
			gpath.headertext.push(ReEvt.result.items[i].items_nm + "％")
		}
		gpath.styledata.items_id = wk_darray 
		gpath.styledata.items_nm = wk_narray 

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: TabInitialize
		Description		: タブ設定
		Usage			: TabInitialize();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/30
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function TabInitialize():Void{
		gpath.tabChildren = true
		gpath.menu00.tabIndex = 6;
		gpath.menu01.tabIndex = 8;
		gpath.csvBtn.tabIndex = 10;
		gpath.previewBtn.tabIndex = 12;
		gpath.printBtn.tabIndex = 14;
		gpath.changeBtn.tabIndex = 16;
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: Service_Fault
		Description		: 設問情報取得不正終了時イベント
		Usage			: Service_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function Service_Fault(FltEvt:FaultEvent):Void{
		var ErrorMsg = FltEvt.fault.description
		var HeaderMsg = "サービスエラー"
		var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, gpath, null, null, null)
		myCond.waiting(gpath,false)
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getAnswers_Result
		Description		: 集計実行正常終了時イベント
		Usage			: getAnswers_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getAnswers_Result(ReEvt:ResultEvent):Void{
		gpath.ResultsPanel.results_sp.refreshPane();
		if( ReEvt.result.mRecordsAvailable == 0){
			var ErrorMsg = "該当レコードはありませんでした。\r検索条件を変更して再度検索を行ってください。"
			var HeaderMsg = "検索結果                                   "
			var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, gpath, null, null, null)
			gpath.ResultsPanel._visible = gpath.menu02._visible = gpath.menu02.enabled = false;
			
		} else {
			//初期設定
			var start_y = 0;
			var owner = this;

			//レコードセットオブジェクトに結果代入
			var RecSet:RecordSet = RecordSet(ReEvt.result);
			//検索結果をデータセットに退避

			//結果エリア表示
			gpath.ResultsPanel._visible = gpath.menu02._visible = gpath.menu02.enabled = true;

			//検索結果をデータセットに退避
			sresult = new DataSet();
			sresult.dataProvider = ReEvt.result;

			var startY = 2
			var startX = 10
			var start_captx = 0
			dsarr = new Array();
			gpath.fieldnames = new Array();
			//psource.attachMovie("totalCaption", "totalCaption", gdepth++, {_y:startY, _x:startX })
			//startY = startY + psource["totalCaption"]._height

			for( var i=0; i<ReEvt.result.mRecordsAvailable; i++ ){



			//	--キャプション
				start_captx = startX
				var dsarrobject = new Object();
				if(rpath.conditionbox.cross_cb.selectedItem.data==0){
					var startc = 0
				} else {
					var startc = 0
				}
				for( var c=startc; c<ReEvt.result.mTitles.length; c++ ){
					dsarrobject[ReEvt.result.mTitles[c]] = ReEvt.result.items[i][ReEvt.result.mTitles[c]]
					var newmc = "result_block" + i + c
					if(i==0){
						psource.attachMovie("result_caption",newmc,gdepth++,{_x:start_captx,_y:startY})
						psource[newmc].caption_txt.text = ReEvt.result.items[i]["label_" + c]
						gpath.fieldnames.push(ReEvt.result.mTitles[c])

					} else {
						psource.attachMovie("result_block",newmc,gdepth++,{_x:start_captx,_y:startY})
						var newtxt = ReEvt.result.items[i]["label_" + c].split("/")
						var count_txt = newtxt[0]
						var ratio_txt = newtxt[1]
						psource[newmc].caption_txt.text = count_txt
						if(ratio_txt!=undefined){
							psource[newmc].ratio_txt.text = ratio_txt + "%"
						} else {
							psource[newmc].ratio_txt.text = ""
						}
						//	--30サンプル以下
						if(count_txt<30){
							psource[newmc].gotoAndStop(2)
						}
					}
					start_captx = start_captx + psource[newmc]._width
				}
				startY = startY + psource[newmc]._height
				dsarr.push(dsarrobject)
/*
				psource.attachMovie("AnswerBar", "AnswerBar" + i , gdepth++, {_y:startY, _x:startX })
				psource["AnswerBar" + i].samples_txt.text = ReEvt.result.items[i].samples
				psource["AnswerBar" + i].average_txt.text = ReEvt.result.items[i].rate
				psource["AnswerBar" + i].data_txt.text = ReEvt.result.items[i].label
				dsarr.push({
					samples:ReEvt.result.items[i].samples,
					average:ReEvt.result.items[i].rate,
					label:ReEvt.result.items[i].label
				})
				startY = startY + psource["AnswerBar" + i]._height
				//サンプル数３０以下は参考値
				if( ReEvt.result.items[i].samples < 30 ){
					psource["AnswerBar" + i].gotoAndStop(2)
					gdepth++;
				}
*/

			}
			psource.attachMovie("result_block","dummy",gdepth++,{_x:start_captx,_y:startY})
			psource["dummy"].enabled = psource["dummy"]._visible = false

			//ＳＰリサイズ
			gpath.ResultsPanel.results_sp.setSize(gpath.ResultsPanel.results_sp.width,gpath.ResultsPanel.results_sp.height)
		}
		myCond.waiting(gpath,false)
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: OutputCSV
		Description		: ＣＳＶファイル出力
		Usage			: OutputCSV( Obj );
		Attributes		: Obj : （コンファームを呼出した呼出元クラスインスタンス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function OutputCSV(){
		var argumentsParam = new Object();
		argumentsParam.resultObj = dsarr
		argumentsParam.headertext = gpath.headertext
		argumentsParam.fieldnames = gpath.fieldnames
		argumentsParam.qstseleted = gpath.qselected_txt.text
		argumentsParam.condseleted = gpath.cmorebtn.labeltxt
		argumentsParam.selecteddate = gpath.dselected_txt.text
		argumentsParam.outputtype = outputtype;
		argumentsParam.resort_code = resort_code;
		var csvPC:PendingCall = sobject.OutputCSV( argumentsParam );
		csvPC.responder = new RelayResponder( this, "OutputCSV_Result", "Service_Fault" );
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: OutputCSV_Result
		Description		: ＣＳＶ出力正常終了時
		Usage			: OutputCSV_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function OutputCSV_Result(ReEvt:ResultEvent):Void{
		var FileObj:downloader = new downloader(gpath,ReEvt.result.filename,ReEvt.result.newname)
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: cboxtween
		Description		: 条件入力状態解除
		Usage			: cboxtween();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/30
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	public function cboxtween(obj,bl){
		if(obj._x < 0 && bl==undefined){
			var _xtween:Object = new Tween(obj, "_x", Back.easeOut, -20, 20, 0.5, true);
		}else if(!bl){
			obj._x = -1000;
		}else{
			var _xtween:Object = new Tween(obj, "_x", Back.easeOut, 20, -1000, 1, true);
		}
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: verifycondition
		Description		: 検索条件のチェック
		Usage			: verifycondition();
		Attributes		: Obj : （検索条件オブジェクト）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/05
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function verifycondition(){
		var retmsg = ""
		if(slctddate.length < 1){retmsg="日付を選択してください"}
		if(gpath.qselectionview.q_id.length < 1){retmsg="集計対象の設問を選択してください"}
		return retmsg
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: qselected
		Description		: 
		Usage			: 
		Attributes		: 
		Note			: none
		History			: 1) Coded by	2007/09/19 9:51
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function selctedhighlite(mc):Void{
		var targetmc = gpath.qselection
		for (var resetobj in targetmc) {
			targetmc[resetobj].selectedmc._visible = false
			if (typeof (targetmc[resetobj]) == "movieclip" && resetobj == mc._name) {
				targetmc[resetobj].selectedmc._visible = true
			}
		}
	}


}