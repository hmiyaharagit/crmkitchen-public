﻿/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	Name			: targetPrint
	Description		: 指定ＭＣの印刷
	Usage			: var XXXXXX:targetPrint = new targetPrint( targetInstance );
	Attributes		: targetInstance:MovieClip		（ 印刷対象ＭＣ名 ）
	Method			: previewGridPrint();			データグリッドコンポーネント印刷時プレビュー
					: setPagesInfo();				印刷ページ情報設定
					: doPrint();					印刷実行（スプール渡し）
	Note			: none
	History			: 1) Coded by	nf)h.miyahara	2004/07/09
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
import mx.controls.gridclasses.DataGridColumn
import mx.controls.DataGrid
import mx.controls.Label;
import mx.controls.ComboBox
import mx.containers.ScrollPane
import mx.controls.TextArea
import mx.styles.CSSStyleDeclaration;

//import
import mx.data.binding.*;
import mx.data.components.DataSet

class targetPrint{
	var printtargetMC:MovieClip;
	var targetPath:MovieClip;
	var headerTxt:String;
	var tempDS:DataSet;
	var GPDepth:Number					//Depth

	private var rowsPerPage:Number
	private var pages:Number
	var temp_MC_y:Number				//対象ＭＣ元Ｙ座標
	var temp_MC_h:Number				//対象ＭＣ元高さ
	var temp_MC_depth:Number			//対象ＭＣ元深度
	
	//var tmp_grid_w:Number;				//プレビュー用グリッド幅
	//var tmp_grid_h:Number;				//プレビュー用グリッド高さ

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Constructor
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function targetPrint( myMC:MovieClip,myDS:DataSet ) {
		trace("targetPrint Constructor!")
		printtargetMC = myMC;						//印刷対象
		tempDS = myDS								//退避データ格納用
		targetPath = printtargetMC._parent; 		//ターゲットＭＣと同階層にパスを指定
		GPDepth = myMC.getNextHighestDepth();
		//tmp_grid_w = 780;				//プレビュー用グリッド幅
		//tmp_grid_h = 550;				//プレビュー用グリッド高さ
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: DGprintPreview
		Description		: データグリッド印刷プレビュー設定
		Usage			: DGprintPreview();
		Attributes		: none
		Note			: 共通関数
		History			: 1) Coded by	nf)h.miyahara	2004/09/27
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function DGprintPreview(){

		//変数設定
		var Owner = this;
		var headerArea_h = 30;
		var headerArea_x = 10;
		var tmp_grid_w = 780;				//プレビュー用グリッド幅
		var tmp_grid_h = 550;				//プレビュー用グリッド高さ
		var temp:mx.data.types.Str;
		var temp:mx.data.types.Num;

		//データ退避用DataSet生成
		var temp_ds = new DataSet();
		
		//DataBindingの作成
		var src= new EndPoint();
		src.component = printtargetMC;
		src.property = "dataProvider";

		var dest = new EndPoint();
		dest.component = temp_ds;
		dest.property = "dataProvider";

		//DataBindingの設定
		new Binding(src, dest);

		//Grid内データを退避用DataSetに格納
		temp_ds.dataProvider = printtargetMC.dataProvider;
		
		//プレビュー用ＭＣ
		targetPath.attachMovie("PreviewHeader","PPreview", GPDepth,{_x:targetPath._x, _y:targetPath._y});
		GPDepth++;
		targetPath.PPreview.beginFill(0xFFFFF9);
		targetPath.PPreview.moveTo(targetPath.PPreview._x,targetPath.PPreview._height);
		targetPath.PPreview.lineTo(Stage.width, targetPath.PPreview._height);
		targetPath.PPreview.lineTo(Stage.width, Stage.height);
		targetPath.PPreview.lineTo(targetPath.PPreview._x, Stage.height);
		targetPath.PPreview.lineTo(targetPath.PPreview._x, targetPath.PPreview._height);
		targetPath.PPreview.endFill();

		//ページング用ＣＢ作成
		targetPath.PPreview.createClassObject(ComboBox,"pages_cb",GPDepth,{_x:150,_y:5,_width:100, _height:22});
		GPDepth++;

		//プレビュー用ＤＧ作成
		targetPath.PPreview.createClassObject(DataGrid,"tmp_dg",GPDepth,{_x:headerArea_x,_y:headerArea_h+10,_width:tmp_grid_w, _height:tmp_grid_h});
		GPDepth++;
		
		//カラム設定
		var col:DataGridColumn = new DataGridColumn("customer_id")
 		col.headerText = "顧客番号"
 		col.width = 50
		col.sortOnHeaderRelease = false;
		col.resizable = false;
		targetPath.PPreview.tmp_dg.addColumn(col)

		col = new DataGridColumn("customer_nm")
 		col.headerText = "氏名（カナ）"
 		col.width = 150
		col.sortOnHeaderRelease = false;
		col.resizable = false;
		targetPath.PPreview.tmp_dg.addColumn(col)

		col = new DataGridColumn("birthday")
 		col.headerText = "生年月日"
 		col.width = 70
		col.sortOnHeaderRelease = false;
		col.resizable = false;
		targetPath.PPreview.tmp_dg.addColumn(col)

		col = new DataGridColumn("tel")
 		col.headerText = "電話番号"
 		col.width = 80
		col.sortOnHeaderRelease = false;
		col.resizable = false;
		targetPath.PPreview.tmp_dg.addColumn(col)

		col = new DataGridColumn("address")
 		col.headerText = "住所"
 		col.width = 280
		col.sortOnHeaderRelease = false;
		col.resizable = false;
		targetPath.PPreview.tmp_dg.addColumn(col)

		//行毎表示色分け
		targetPath.PPreview.tmp_dg.setStyle("alternatingRowColors", Array(0xFFFFFF, 0xEAEAEA));
		targetPath.PPreview.tmp_dg.setVScrollPolicy("auto");

		//ページング用変数設定
		var rowsPerPage = 24; //Math.floor((targetPath.PPreview.tmp_dg.height - targetPath.PPreview.tmp_dg.headerHeight)/targetPath.PPreview.tmp_dg.rowHeight);
		var pages = Math.ceil(temp_ds.dataProvider.length/rowsPerPage);
		var pages_arr = new Array({data:"",label:"全ページ"});
		for(var i=0;i<pages;i++){
			pages_arr.push({data:i,label:i+1+"ページ"})
		}

		//コンボボックスイベント設定
		var ComboListner = new Object();
		ComboListner.rowsPerPage = rowsPerPage;
		//Loadイベント
		targetPath.PPreview.pages_cb.onEnterFrame = function(){
			Owner.targetPath.PPreview.pages_cb.dataProvider = pages_arr
			if(Owner.targetPath.PPreview.pages_cb.dataProvider.length > 0){
				delete this.onEnterFrame;
			}
		}

		//Changeイベント
		ComboListner.change = function(eventObj){
			var startRec = this.rowsPerPage*eventObj.target.value;
			var endRec = this.rowsPerPage*(eventObj.target.value+1);
			if(eventObj.target.value == "" ){
				startRec = 0
				endRec = Owner.printtargetMC.dataProvider.length
				Owner.targetPath.PPreview.tmp_dg.vScrollPolicy = "auto";
			} else {
				Owner.targetPath.PPreview.tmp_dg.vScrollPolicy = "off";
			}
			temp_ds.dataProvider = Owner.printtargetMC.dataProvider.items.slice( startRec, endRec );
			Owner.targetPath.PPreview.tmp_dg.dataProvider = temp_ds.items
		}
		targetPath.PPreview.pages_cb.addEventListener( "change", ComboListner );


		//退避用DataSetからプレビュー用データグリッドへ
		targetPath.PPreview.tmp_dg.dataProvider = temp_ds.items

		//印刷ボタン
		targetPath.PPreview.pre_printBtn.onRelease = function (){
			Owner.doDGPrint(Owner.targetPath.PPreview.tmp_dg)
		}

	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: doDGPrint
		Description		: 印刷実行
		Usage			: doPrint(tgtMC);
		Attributes		: tgtMC			印刷対象ＭＣ
		Note			: 
		History			: 1) Coded by	nf)h.miyahara	2004/09/27
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function doDGPrint(tgtMC){
		trace(tgtMC)
		var myDataGrid = tgtMC;
		var PrintQueue : PrintJob = new PrintJob();
		var prev_vPosition:Number = myDataGrid.vPosition;
		var prev_width:Number = myDataGrid.width;
		var prev_height:Number = myDataGrid.height;
		var RecordCount:Number = myDataGrid.dataProvider.length 

/**/
		if( PrintQueue.start() != true)
		return;
		myDataGrid.setSize(PrintQueue.pageWidth,PrintQueue.pageHeight);
		myDataGrid.vScrollPolicy = "off";

		var rowsPerPage:Number = Math.floor((myDataGrid.height - myDataGrid.rowHeight)/ myDataGrid.rowHeight);
		var pages:Number = Math.ceil(RecordCount/rowsPerPage);
		var yStart = 0;

		for (var i=0;i<pages;i++) {
			myDataGrid.vPosition = i*rowsPerPage;

			if( i*rowsPerPage+rowsPerPage >RecordCount ){
				var repeatRecord = (i*rowsPerPage+rowsPerPage) - RecordCount
				yStart = myDataGrid.headerHeight + myDataGrid.rowHeight*repeatRecord
			}

			var b= {xMin:0,xMax:myDataGrid.width,yMin:yStart,yMax:myDataGrid.height}
			yStart = myDataGrid.headerHeight

			PrintQueue.addPage(myDataGrid,b);

		}
		PrintQueue.send();
		
		delete PrintQueue;

		myDataGrid.setSize(prev_width,prev_height);
		myDataGrid.vPosition = prev_vPosition;
		myDataGrid.vScrollPolicy = "auto";

/*確認用

		var rowsPerPage:Number = Math.floor((myDataGrid.height - myDataGrid.headerHeight)/ myDataGrid.rowHeight);
		var pages:Number = Math.ceil(RecordCount/rowsPerPage);

		//trace("rowsPerPage : " + Math.floor((myDataGrid.height - myDataGrid.rowHeight)/ myDataGrid.rowHeight))
		//trace("RecordCount: " + RecordCount)
		//trace("pages: " + Math.ceil(RecordCount/Math.floor((myDataGrid.height - myDataGrid.rowHeight)/ myDataGrid.rowHeight)))

		var yStart = 0;
		for (var i=0;i<pages;i++) {
			myDataGrid.vPosition = i*rowsPerPage;
			//trace("RecordCount " + RecordCount )
			//trace( i*rowsPerPage + "～" + (i*rowsPerPage+rowsPerPage) + "まで表示" )

			if( i*rowsPerPage+rowsPerPage >RecordCount ){
				var repeatRecord = (i*rowsPerPage+rowsPerPage) - RecordCount
				//trace(repeatRecord + "件重複表示されます。")
				yStart = myDataGrid.headerHeight + myDataGrid.rowHeight*repeatRecord
				trace(yStart + "移動してプリントします。")
			}
			
			trace("yStart " + yStart )
			var b= {xMin:0,xMax:myDataGrid.width,yMin:yStart,yMax:myDataGrid.height}
			yStart = myDataGrid.headerHeight

		}

確認用*/

	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: MCprintPreview
		Description		: スクロールペイン印刷プレビュー設定
		Usage			: MCprintPreview();
		Attributes		: none
		Note			: 共通関数
		History			: 1) Coded by	nf)h.miyahara	2004/10/04
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function MCprintPreview(){

		//変数設定
		var Owner = this;
		var headerArea_h = 30;
		var headerArea_x = 10;
		var temp:mx.data.types.Str;
		var temp:mx.data.types.Num;
		temp_MC_y = printtargetMC._y;					//対象ＭＣ元Ｙ座標
		temp_MC_h = printtargetMC.results_sp.height;	//対象ＭＣ元高さ

		//プレビュー用ＭＣ
		targetPath.attachMovie("PreviewHeader","PPreview", GPDepth,{_x:targetPath._x, _y:targetPath._y});
		GPDepth++;
		targetPath.PPreview.beginFill(0xFFFFF9);
		targetPath.PPreview.moveTo(targetPath.PPreview._x,targetPath.PPreview._height);
		targetPath.PPreview.lineTo(Stage.width, targetPath.PPreview._height);
		targetPath.PPreview.lineTo(Stage.width, Stage.height);
		targetPath.PPreview.lineTo(targetPath.PPreview._x, Stage.height);
		targetPath.PPreview.lineTo(targetPath.PPreview._x, targetPath.PPreview._height);
		targetPath.PPreview.endFill();

		//ページング用ＣＢ作成
		targetPath.PPreview.createClassObject(ComboBox,"pages_cb",GPDepth,{_x:150,_y:5,_width:100, _height:22});
		GPDepth++;

		//プレビュー用ＳＰ位置設定
		printtargetMC.swapDepths( targetPath.PPreview.getDepth()+1 ); 
		printtargetMC.results_sp.setSize( printtargetMC.results_sp._width, 545 )
		printtargetMC._y = headerArea_h + 5

		//ペイン内初期化
		printtargetMC.results_sp.refreshPane();

		var tempRec:DataSet = tempDS;
		var PaneSource = printtargetMC.results_sp.content
		var start_y = 0;
		var rowsPerPage = 9
		var dummyCnt = rowsPerPage - (tempRec.dataProvider.length%rowsPerPage)

		//印刷用非表示
		printtargetMC.specsheetBtn._visible = false

		for( var i=0; i<tempRec.dataProvider.length+dummyCnt; i++ ){
			PaneSource.attachMovie("resultBar", "resultBar_" + i  , GPDepth, {_y:start_y})
			GPDepth++;
			start_y = start_y + PaneSource["resultBar_" + i]._height

			PaneSource["resultBar_" + i].createClassObject(TextArea, "information_txt", GPDepth, {_x:101,_y:20})
			GPDepth++;
			PaneSource["resultBar_" + i].information_txt.setSize(660,40);

			//印刷用非表示
			PaneSource["resultBar_" + i].specSheetBtn._visible = false;

			//データ割当
				PaneSource["resultBar_" + i].customerId_txt.text = tempRec.items[i].customer_id;
				PaneSource["resultBar_" + i].customerNm_txt.text = tempRec.items[i].customer_nm;
				PaneSource["resultBar_" + i].stayId_txt.text = tempRec.items[i].stay_id;
				PaneSource["resultBar_" + i].arrivalDay_txt.text = tempRec.items[i].arrival_date;
				PaneSource["resultBar_" + i].departureDay_txt.text = tempRec.items[i].depature_date;
				PaneSource["resultBar_" + i].recentlyDay_txt.text = tempRec.items[i].recently;
				PaneSource["resultBar_" + i].stayHistory_txt.text = tempRec.items[i].history;
				PaneSource["resultBar_" + i].satisfaction_txt.text = tempRec.items[i].satisfaction;
				PaneSource["resultBar_" + i].information_txt.text = tempRec.items[i].information;

			//VIP設定
			if(tempRec.items[i].vip_flg == 1){
				PaneSource["resultBar_" + i].vip_icon.gotoAndStop(2);
				PaneSource["resultBar_" + i].important_txt.text = "重要";
			}
			
			//既ＣＳ回答設定
			if( tempRec.items[i].satisfaction.length > 0 ){
				PaneSource["resultBar_" + i].header_mc.gotoAndStop(2);
				PaneSource["resultBar_" + i].header_mc.satisfaction_txt.text = tempRec.items[i].satisfaction;
			}
	
			//スクロール移動用ダミー処理
			if( i >= tempRec.dataProvider.length ){
				PaneSource["resultBar_" + i]._visible = false;
			}

		}

		//ページング用変数設定
		var Pagesize = printtargetMC.results_sp.height + 20; //Math.floor((targetPath.PPreview.tmp_dg.height - targetPath.PPreview.tmp_dg.headerHeight)/targetPath.PPreview.tmp_dg.rowHeight);
			//trace("height : " + printtargetMC.results_sp.height)
		var pages = Math.ceil(printtargetMC._height/Pagesize);
		var pages_arr = new Array({data:"",label:"全ページ"});
			//trace("pages : " + pages)
		for(var i=0;i<pages;i++){
			pages_arr.push({data:i,label:i+1+"ページ"})
		}

		//コンボボックスイベント設定
		var ComboListner = new Object();
		ComboListner.rowsPerPage = rowsPerPage;

		//Loadイベント
		targetPath.PPreview.pages_cb.onEnterFrame = function(){
			Owner.targetPath.PPreview.pages_cb.dataProvider = pages_arr
			if(Owner.targetPath.PPreview.pages_cb.dataProvider.length > 0){
				delete this.onEnterFrame;
			}
		}

		//Changeイベント
		ComboListner.change = function(eventObj){

			var startRec = rowsPerPage*eventObj.target.value*60;
			if(eventObj.target.value == "" ){
				startRec = 0
				Owner.printtargetMC.results_sp.vScrollPolicy = "auto";

			} else {
				Owner.printtargetMC.results_sp.vScrollPolicy = "off";

			}
			Owner.printtargetMC.results_sp.vPosition = startRec

		}
		targetPath.PPreview.pages_cb.addEventListener( "change", ComboListner );

		//閉じるボタン
		targetPath.PPreview.pre_closeBtn.onRelease = function (){
			Owner.RecoverSearchData();
			this._parent.removeMovieClip();
		}

		//印刷ボタン
		targetPath.PPreview.pre_printBtn.onRelease = function (){
			//Owner.doDGPrint(Owner.targetPath.PPreview.tmp_dg)
			Owner.doMCPrint();
		}

	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: doMCPrint
		Description		: 印刷実行
		Usage			: doMCPrint(tgtMC);
		Attributes		: tgtMC			印刷対象ＭＣ
		Note			: 
		History			: 1) Coded by	nf)h.miyahara	2004/09/27
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function doMCPrint(){	trace("doMCPrint")
		
		var PrintQueue : PrintJob = new PrintJob();
		var prev_vPosition:Number = printtargetMC.results_sp.vPosition;
		var prev_width:Number = printtargetMC.results_sp.width;
		var prev_height:Number = printtargetMC.results_sp.height;
		//var RecordCount:Number = printtargetMC.dataProvider.length 

/*印刷用
印刷用*/

		if( PrintQueue.start() != true)
		return;
		printtargetMC.results_sp.setSize(PrintQueue.pageWidth,PrintQueue.pageHeight);
		printtargetMC.vScrollPolicy = "off";

		var rowsPerPage = 9
		var Pagesize = printtargetMC.results_sp.height + 20; //Math.floor((targetPath.PPreview.tmp_dg.height - targetPath.PPreview.tmp_dg.headerHeight)/targetPath.PPreview.tmp_dg.rowHeight);
			//trace("height : " + printtargetMC.results_sp.height)
		var pages = Math.ceil(printtargetMC._height/Pagesize);
		var headerHeight = 20;

		//対象ページ確認
		//trace("：" + targetPath.PPreview.pages_cb.selectedItem.data.length)
		var startCnt = 0
		if( targetPath.PPreview.pages_cb.selectedItem.data != "" ){
			startCnt = targetPath.PPreview.pages_cb.selectedItem.data
			pages = targetPath.PPreview.pages_cb.selectedItem.data+1
		}

		var yStart = 0;
		for ( var i=startCnt; i<pages; i++ ) {

			var startRec = rowsPerPage*i*60;
			printtargetMC.results_sp.vScrollPolicy = "off";
			printtargetMC.results_sp.vPosition = startRec;

			var b= {xMin:0,xMax:printtargetMC._width,yMin:yStart,yMax:printtargetMC._height};
			yStart = headerHeight;
			PrintQueue.addPage(printtargetMC,b);

		}
		PrintQueue.send();
		
		delete PrintQueue;

		printtargetMC.results_sp.setSize(prev_width,prev_height);
		printtargetMC.results_sp.vPosition = prev_vPosition;
		printtargetMC.results_sp.vScrollPolicy = "auto";


/*確認用



		//ページング用変数設定
		var rowsPerPage = 9
		var Pagesize = printtargetMC.results_sp.height + 20; //Math.floor((targetPath.PPreview.tmp_dg.height - targetPath.PPreview.tmp_dg.headerHeight)/targetPath.PPreview.tmp_dg.rowHeight);
		var pages = Math.ceil(printtargetMC._height/Pagesize);
		var headerHeight = 20;

		//対象ページ確認
		//trace("：" + targetPath.PPreview.pages_cb.selectedItem.data.length)
		var startCnt = 0
		if( targetPath.PPreview.pages_cb.selectedItem.data != "" ){
			startCnt = targetPath.PPreview.pages_cb.selectedItem.data
			pages = targetPath.PPreview.pages_cb.selectedItem.data+1
		}

		var yStart = 0;
		for ( var i=startCnt; i<pages; i++ ) {
			
			trace( (i+1) + "ページ目を印刷")
			
			var startRec = rowsPerPage*i*60;
			printtargetMC.results_sp.vScrollPolicy = "off";
			printtargetMC.results_sp.vPosition = startRec
			
			//trace("yStart " + yStart )
			var b= {xMin:0,xMax:printtargetMC._width,yMin:yStart,yMax:printtargetMC._height}
			yStart = headerHeight

		}
確認用*/

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: RecoverSearchData
		Description		: プレビュー用に変更した対象ＭＣのデータ、サイズ等を元に戻す
		Usage			: RecoverSearchData();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/05
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function RecoverSearchData(){
		var tempRec:DataSet = tempDS;
		var PaneSource = printtargetMC.results_sp.content
		var start_y = 0;

		//スクロールペイン位置戻し
		printtargetMC._y = temp_MC_y;
		printtargetMC.results_sp.setSize( printtargetMC.results_sp._width, temp_MC_h )

		//ペイン内初期化
		printtargetMC.results_sp.refreshPane();

		//印刷用非表示戻し
		printtargetMC.specsheetBtn._visible = true

		//データ戻し
		for( var i=0; i<tempRec.dataProvider.length; i++ ){
			PaneSource.attachMovie("resultBar", "resultBar_" + i  , GPDepth, {_y:start_y})
			GPDepth++;
			start_y = start_y + PaneSource["resultBar_" + i]._height

			PaneSource["resultBar_" + i].createClassObject(TextArea, "information_txt", GPDepth, {_x:101,_y:20})
			GPDepth++;
			PaneSource["resultBar_" + i].information_txt.setSize(760,40);

			//印刷用非表示戻し
			PaneSource["resultBar_" + i].specSheetBtn._visible = true;

			//データ割当
				PaneSource["resultBar_" + i].customerId_txt.text = tempRec.items[i].customer_id;
				PaneSource["resultBar_" + i].customerNm_txt.text = tempRec.items[i].customer_nm;
				PaneSource["resultBar_" + i].stayId_txt.text = tempRec.items[i].stay_id;
				PaneSource["resultBar_" + i].arrivalDay_txt.text = tempRec.items[i].arrival_date;
				PaneSource["resultBar_" + i].departureDay_txt.text = tempRec.items[i].depature_date;
				PaneSource["resultBar_" + i].recentlyDay_txt.text = tempRec.items[i].recently;
				PaneSource["resultBar_" + i].stayHistory_txt.text = tempRec.items[i].history;
				PaneSource["resultBar_" + i].satisfaction_txt.text = tempRec.items[i].satisfaction;
				PaneSource["resultBar_" + i].information_txt.text = tempRec.items[i].information;

			//VIP設定
			if(tempRec.items[i].vip_flg == 1){
				PaneSource["resultBar_" + i].vip_icon.gotoAndStop(2);
				PaneSource["resultBar_" + i].important_txt.text = "重要";
			}
			
			//既ＣＳ回答設定
			if( tempRec.items[i].satisfaction.length > 0 ){
				PaneSource["resultBar_" + i].header_mc.gotoAndStop(2);
				PaneSource["resultBar_" + i].header_mc.satisfaction_txt.text = tempRec.items[i].satisfaction;
			}
	
			//スペックシートボタン設定
			PaneSource["resultBar_" + i].specSheetBtn.id = tempRec.items[i].customer_id
			PaneSource["resultBar_" + i].specSheetBtn.onRelease = function(){
				trace( "MyID:" + this.id + "のスペックシート表示")
			}
		}
		printtargetMC.results_sp.vScrollPolicy = "auto";

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: SSprintPreview
		Description		: -結果印刷プレビュー設定
		Usage			: CSprintPreview();
		Attributes		: none
		Note			: 共通関数
		History			: 1) Coded by	nf)h.miyahara	2004/10/04
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function SSprintPreview(){
		//変数設定
		var Owner = this;
		var headerArea_h = 30;
		var headerArea_x = 10;
		var temp:mx.data.types.Str;
		var temp:mx.data.types.Num;
		temp_MC_y = printtargetMC._y;					//対象ＭＣ元Ｙ座標
		temp_MC_h = printtargetMC.height;	//対象ＭＣ元高さ
		temp_MC_depth = printtargetMC.getDepth();

		//プレビュー用ＭＣ
		targetPath.attachMovie("PreviewHeader","PPreview", GPDepth,{_x:targetPath._x, _y:targetPath._y});
		GPDepth++;
		targetPath.PPreview.beginFill(0xFFFFF9);
		targetPath.PPreview.moveTo(targetPath.PPreview._x,targetPath.PPreview._height);
		targetPath.PPreview.lineTo(Stage.width, targetPath.PPreview._height);
		targetPath.PPreview.lineTo(Stage.width, Stage.height);
		targetPath.PPreview.lineTo(targetPath.PPreview._x, Stage.height);
		targetPath.PPreview.lineTo(targetPath.PPreview._x, targetPath.PPreview._height);
		targetPath.PPreview.endFill();

		//ページング用ＣＢ作成
		targetPath.PPreview.createClassObject(ComboBox,"pages_cb",GPDepth,{_x:150,_y:5,_width:100, _height:22});
		GPDepth++;

		//プレビュー用ＳＰ位置設定
		printtargetMC.swapDepths( targetPath.PPreview.getDepth()+1 ); 
		printtargetMC.setSize( printtargetMC._width, 545 )
		printtargetMC._y = headerArea_h + 5

		var tempRec:DataSet = tempDS;
		var PaneSource = printtargetMC.content
		var start_y = 0;
		var rowsPerPage = 9
		var dummyCnt = rowsPerPage - (tempRec.dataProvider.length%rowsPerPage)


		//ページング用変数設定
		var Pagesize = printtargetMC.height + 20; //Math.floor((targetPath.PPreview.tmp_dg.height - targetPath.PPreview.tmp_dg.headerHeight)/targetPath.PPreview.tmp_dg.rowHeight);
			//trace("height : " + printtargetMC.height)
		var pages = Math.ceil(printtargetMC._height/Pagesize);
		var pages_arr = new Array({data:"",label:"全ページ"});
			//trace("pages : " + pages)
		for(var i=0;i<pages;i++){
			pages_arr.push({data:i,label:i+1+"ページ"})
		}

		//コンボボックスイベント設定
		var ComboListner = new Object();
		ComboListner.rowsPerPage = rowsPerPage;

		//Loadイベント
		targetPath.PPreview.pages_cb.onEnterFrame = function(){
			Owner.targetPath.PPreview.pages_cb.dataProvider = pages_arr
			if(Owner.targetPath.PPreview.pages_cb.dataProvider.length > 0){
				delete this.onEnterFrame;
			}
		}

		//Changeイベント
		ComboListner.change = function(eventObj){

			var startRec = rowsPerPage*eventObj.target.value*60;
			if(eventObj.target.value == "" ){
				startRec = 0
				Owner.printtargetMC.vScrollPolicy = "auto";

			} else {
				Owner.printtargetMC.vScrollPolicy = "off";

			}
			Owner.printtargetMC.vPosition = startRec

		}
		targetPath.PPreview.pages_cb.addEventListener( "change", ComboListner );

		//閉じるボタン
		targetPath.PPreview.pre_closeBtn.onRelease = function (){
			Owner.RecoverSpecSheetData();
			this._parent.removeMovieClip();
		}

		//印刷ボタン
		targetPath.PPreview.pre_printBtn.onRelease = function (){
			//Owner.doDGPrint(Owner.targetPath.PPreview.tmp_dg)
			Owner.doMCPrint();
		}

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: RecoverSpecSheetData
		Description		: プレビュー用に変更した対象ＭＣのデータ、サイズ等を元に戻す
		Usage			: RecoverSpecSheetData();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/05
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function RecoverSpecSheetData(){
		var tempRec:DataSet = tempDS;
		var PaneSource = printtargetMC.content
		var start_y = 0;

		//スクロールペイン位置戻し
		printtargetMC._y = temp_MC_y;
		printtargetMC.setSize( printtargetMC._width, temp_MC_h )
		printtargetMC.swapDepths( temp_MC_depth ); 

		//印刷用非表示戻し
		printtargetMC.vScrollPolicy = "auto";

	}



	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: OAprintPreview
		Description		: データグリッド印刷プレビュー設定
		Usage			: OAprintPreview();
		Attributes		: none
		Note			: 共通関数
		History			: 1) Coded by	nf)h.miyahara	2004/10/27
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function OAprintPreview(){

		//変数設定
		var Owner = this;
		var headerArea_h = 30;
		var headerArea_x = 10;
		var tmp_grid_w = 780;				//プレビュー用グリッド幅
		var tmp_grid_h = 550;				//プレビュー用グリッド高さ
		var temp:mx.data.types.Str;
		var temp:mx.data.types.Num;

		//データ退避用DataSet生成
		var temp_ds = new DataSet();
		
		//DataBindingの作成
		var src= new EndPoint();
		src.component = printtargetMC;
		src.property = "dataProvider";

		var dest = new EndPoint();
		dest.component = temp_ds;
		dest.property = "dataProvider";

		//DataBindingの設定
		new Binding(src, dest);

		//Grid内データを退避用DataSetに格納
		temp_ds.dataProvider = printtargetMC.dataProvider;
		
		//プレビュー用ＭＣ
		targetPath.attachMovie("PreviewHeader","PPreview", GPDepth,{_x:targetPath._x, _y:targetPath._y});
		GPDepth++;
		targetPath.PPreview.beginFill(0xFFFFF9);
		targetPath.PPreview.moveTo(targetPath.PPreview._x,targetPath.PPreview._height);
		targetPath.PPreview.lineTo(Stage.width, targetPath.PPreview._height);
		targetPath.PPreview.lineTo(Stage.width, Stage.height);
		targetPath.PPreview.lineTo(targetPath.PPreview._x, Stage.height);
		targetPath.PPreview.lineTo(targetPath.PPreview._x, targetPath.PPreview._height);
		targetPath.PPreview.endFill();

		//ページング用ＣＢ作成
		targetPath.PPreview.createClassObject(ComboBox,"pages_cb",GPDepth,{_x:150,_y:5,_width:100, _height:22});
		GPDepth++;

		//プレビュー用ＤＧ作成
		//targetPath.PPreview.createClassObject(DataGrid,"tmp_dg",GPDepth,{_x:headerArea_x,_y:headerArea_h+10,_width:tmp_grid_w, _height:tmp_grid_h});
		targetPath.PPreview.createClassObject(DataGrid,"tmp_dg",GPDepth,{_x:headerArea_x,_y:headerArea_h+10,_width:tmp_grid_w, _height:tmp_grid_h});
		GPDepth++;
		targetPath.PPreview.tmp_dg.rowHeight = printtargetMC.rowHeight

		var oaWidth = 0
		var oaCnt = 0
		for(var i=0; i<printtargetMC.columnCount; i++){
			var col:DataGridColumn = new DataGridColumn( printtargetMC.columnNames[i]);
	 		col.headerText = printtargetMC.getColumnAt(i).headerText;
	 		col.width = printtargetMC.getColumnAt(i).width;
			col.sortOnHeaderRelease = false;
			targetPath.PPreview.tmp_dg.addColumn(col);
			if( col.columnName.indexOf("OA") != -1 ){
				col.cellRenderer = "MultiLineCell";
				oaCnt++;
			}
			oaWidth += col.width;
		}
		var scale = tmp_grid_w/oaWidth
		trace("oaWidth: " + oaWidth)
		trace("scale: " + scale)
		for( var c=0; c<printtargetMC.columnCount; c++){
			targetPath.PPreview.tmp_dg.getColumnAt(c).width = targetPath.PPreview.tmp_dg.getColumnAt(c).width*scale;
		}
		targetPath.PPreview.tmp_dg.setStyle("fontSize", printtargetMC.getStyle("fontSize")-oaCnt);
		var headerStyles = new CSSStyleDeclaration();
		headerStyles.setStyle("fontSize", 9);
		targetPath.PPreview.tmp_dg.setStyle("headerStyle", headerStyles);

		//targetPath.PPreview.tmp_dg.hScrollPolicy = "auto";

		//行毎表示色分け
		targetPath.PPreview.tmp_dg.setStyle("alternatingRowColors", Array(0xFFFFFF, 0xEAEAEA));
		targetPath.PPreview.tmp_dg.setVScrollPolicy("auto");

		//ページング用変数設定
		var rowsPerPage = 9; //Math.floor((targetPath.PPreview.tmp_dg.height - targetPath.PPreview.tmp_dg.headerHeight)/targetPath.PPreview.tmp_dg.rowHeight);
		var pages = Math.ceil(temp_ds.dataProvider.length/rowsPerPage);
		var pages_arr = new Array({data:"",label:"全ページ"});
		for(var i=0;i<pages;i++){
			pages_arr.push({data:i,label:i+1+"ページ"})
		}

		//コンボボックスイベント設定
		var ComboListner = new Object();
		ComboListner.rowsPerPage = rowsPerPage;
		//Loadイベント
		targetPath.PPreview.pages_cb.onEnterFrame = function(){
			Owner.targetPath.PPreview.pages_cb.dataProvider = pages_arr
			if(Owner.targetPath.PPreview.pages_cb.dataProvider.length > 0){
				delete this.onEnterFrame;
			}
		}

		//Changeイベント
		ComboListner.change = function(eventObj){
			var startRec = this.rowsPerPage*eventObj.target.value;
			var endRec = this.rowsPerPage*(eventObj.target.value+1);
			if(eventObj.target.value == "" ){
				startRec = 0
				endRec = Owner.printtargetMC.dataProvider.length
				Owner.targetPath.PPreview.tmp_dg.vScrollPolicy = "auto";
			} else {
				Owner.targetPath.PPreview.tmp_dg.vScrollPolicy = "off";
			}
			temp_ds.dataProvider = Owner.printtargetMC.dataProvider.items.slice( startRec, endRec );
			Owner.targetPath.PPreview.tmp_dg.dataProvider = temp_ds.items
		}
		targetPath.PPreview.pages_cb.addEventListener( "change", ComboListner );


		//退避用DataSetからプレビュー用データグリッドへ
		targetPath.PPreview.tmp_dg.dataProvider = temp_ds.items

		//印刷ボタン
		targetPath.PPreview.pre_printBtn.onRelease = function (){
			Owner.doDGPrint(Owner.targetPath.PPreview.tmp_dg)
		}

	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: CSprintPreview
		Description		: CS集計結果印刷プレビュー設定
		Usage			: CSprintPreview();
		Attributes		: none
		Note			: 共通関数
		History			: 1) Coded by	nf)h.miyahara	2004/10/04
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function CSprintPreview(){
		//変数設定
		var Owner = this;
		var headerArea_h = 30;
		var headerArea_x = 10;
		var temp:mx.data.types.Str;
		var temp:mx.data.types.Num;
		temp_MC_y = printtargetMC._y;					//対象ＭＣ元Ｙ座標
		temp_MC_h = printtargetMC.results_sp.height;	//対象ＭＣ元高さ
		temp_MC_depth = printtargetMC.getDepth();

		//プレビュー用ＭＣ
		targetPath.attachMovie("PreviewHeader","PPreview", GPDepth,{_x:targetPath._x, _y:targetPath._y});
		GPDepth++;
		targetPath.PPreview.beginFill(0xFFFFF9);
		targetPath.PPreview.moveTo(targetPath.PPreview._x,targetPath.PPreview._height);
		targetPath.PPreview.lineTo(Stage.width, targetPath.PPreview._height);
		targetPath.PPreview.lineTo(Stage.width, Stage.height);
		targetPath.PPreview.lineTo(targetPath.PPreview._x, Stage.height);
		targetPath.PPreview.lineTo(targetPath.PPreview._x, targetPath.PPreview._height);
		targetPath.PPreview.endFill();

		//ページング用ＣＢ作成
		targetPath.PPreview.createClassObject(ComboBox,"pages_cb",GPDepth,{_x:150,_y:5,_width:100, _height:22});
		GPDepth++;

		//プレビュー用ＳＰ位置設定
		printtargetMC.swapDepths( targetPath.PPreview.getDepth()+1 ); 
		printtargetMC.results_sp.setSize( printtargetMC.results_sp._width, 545 )
		printtargetMC._y = headerArea_h + 5

		var tempRec:DataSet = tempDS;
		var PaneSource = printtargetMC.results_sp.content
		var start_y = 0;
		var rowsPerPage = 9
		var dummyCnt = rowsPerPage - (tempRec.dataProvider.length%rowsPerPage)


		//ページング用変数設定
		var Pagesize = printtargetMC.results_sp.height + 20; //Math.floor((targetPath.PPreview.tmp_dg.height - targetPath.PPreview.tmp_dg.headerHeight)/targetPath.PPreview.tmp_dg.rowHeight);
			//trace("height : " + printtargetMC.results_sp.height)
		var pages = Math.ceil(printtargetMC._height/Pagesize);
		var pages_arr = new Array({data:"",label:"全ページ"});
			//trace("pages : " + pages)
		for(var i=0;i<pages;i++){
			pages_arr.push({data:i,label:i+1+"ページ"})
		}

		//コンボボックスイベント設定
		var ComboListner = new Object();
		ComboListner.rowsPerPage = rowsPerPage;

		//Loadイベント
		targetPath.PPreview.pages_cb.onEnterFrame = function(){
			Owner.targetPath.PPreview.pages_cb.dataProvider = pages_arr
			if(Owner.targetPath.PPreview.pages_cb.dataProvider.length > 0){
				delete this.onEnterFrame;
			}
		}

		//Changeイベント
		ComboListner.change = function(eventObj){

			var startRec = rowsPerPage*eventObj.target.value*60;
			if(eventObj.target.value == "" ){
				startRec = 0
				Owner.printtargetMC.results_sp.vScrollPolicy = "auto";

			} else {
				Owner.printtargetMC.results_sp.vScrollPolicy = "off";

			}
			Owner.printtargetMC.results_sp.vPosition = startRec

		}
		targetPath.PPreview.pages_cb.addEventListener( "change", ComboListner );

		//閉じるボタン
		targetPath.PPreview.pre_closeBtn.onRelease = function (){
			Owner.RecoverResultData();
			this._parent.removeMovieClip();
		}

		//印刷ボタン
		targetPath.PPreview.pre_printBtn.onRelease = function (){
			//Owner.doDGPrint(Owner.targetPath.PPreview.tmp_dg)
			Owner.doMCPrint();
		}

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: RecoverResultData
		Description		: プレビュー用に変更した対象ＭＣのデータ、サイズ等を元に戻す
		Usage			: RecoverSearchData();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/05
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function RecoverResultData(){
		var tempRec:DataSet = tempDS;
		var PaneSource = printtargetMC.results_sp.content
		var start_y = 0;

		//スクロールペイン位置戻し
		printtargetMC._y = temp_MC_y;
		printtargetMC.results_sp.setSize( printtargetMC.results_sp._width, temp_MC_h )
		printtargetMC.swapDepths( temp_MC_depth ); 

		//印刷用非表示戻し
		printtargetMC.specsheetBtn._visible = true
		printtargetMC.results_sp.vScrollPolicy = "auto";

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: MLprintPreview
		Description		: データグリッド印刷プレビュー設定
		Usage			: MLprintPreview();
		Attributes		: none
		Note			: 共通関数
		History			: 1) Coded by	nf)h.miyahara	2004/10/27
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function MLprintPreview(){

		//変数設定
		var Owner = this;
		var headerArea_h = 30;
		var headerArea_x = 10;
		var tmp_grid_w = 780;				//プレビュー用グリッド幅
		var tmp_grid_h = 550;				//プレビュー用グリッド高さ
		var temp:mx.data.types.Str;
		var temp:mx.data.types.Num;

		//データ退避用DataSet生成
		var temp_ds = new DataSet();
		
		//DataBindingの作成
		var src= new EndPoint();
		src.component = printtargetMC;
		src.property = "dataProvider";

		var dest = new EndPoint();
		dest.component = temp_ds;
		dest.property = "dataProvider";

		//DataBindingの設定
		new Binding(src, dest);

		//Grid内データを退避用DataSetに格納
		temp_ds.dataProvider = printtargetMC.dataProvider;
		
		trace(printtargetMC)
		
		//プレビュー用ＭＣ
		targetPath.attachMovie("PreviewHeader","PPreview", GPDepth,{_x:targetPath._x, _y:targetPath._y});
		GPDepth++;
		targetPath.PPreview.beginFill(0xFFFFF9);
		targetPath.PPreview.moveTo(targetPath.PPreview._x,targetPath.PPreview._height);
		targetPath.PPreview.lineTo(Stage.width, targetPath.PPreview._height);
		targetPath.PPreview.lineTo(Stage.width, Stage.height);
		targetPath.PPreview.lineTo(targetPath.PPreview._x, Stage.height);
		targetPath.PPreview.lineTo(targetPath.PPreview._x, targetPath.PPreview._height);
		targetPath.PPreview.endFill();

		//ページング用ＣＢ作成
		targetPath.PPreview.createClassObject(ComboBox,"pages_cb",GPDepth,{_x:150,_y:5,_width:100, _height:22});
		GPDepth++;

		//プレビュー用ＤＧ作成
		targetPath.PPreview.createClassObject(DataGrid,"tmp_dg",GPDepth,{_x:headerArea_x,_y:headerArea_h+10,_width:tmp_grid_w, _height:tmp_grid_h});
		GPDepth++;
		//targetPath.PPreview.tmp_dg.rowHeight = printtargetMC.rowHeight
		
		//カラム設定
		var col:DataGridColumn = new DataGridColumn("reserve_id");
 		col.headerText = "予約番号";
 		col.width = 60;
		col.sortOnHeaderRelease = false;
		targetPath.PPreview.tmp_dg.addColumn(col);

		col = new DataGridColumn("depature_date");
 		col.headerText = "出発日";
 		col.width = 80;
		col.sortOnHeaderRelease = false;
		targetPath.PPreview.tmp_dg.addColumn(col);

		col = new DataGridColumn("customer_id");
 		col.headerText = "顧客番号";
 		col.width = 70;
		col.sortOnHeaderRelease = false;
		targetPath.PPreview.tmp_dg.addColumn(col);

		col = new DataGridColumn("customer_nm");
 		col.headerText = "氏名（カナ）";
 		col.width = 160;
		col.sortOnHeaderRelease = false;
		targetPath.PPreview.tmp_dg.addColumn(col);
/*
		col = new DataGridColumn("customer_kana");
 		col.headerText = "氏名カナ";
 		col.width = 120;
		col.sortOnHeaderRelease = false;
		targetGrid.addColumn(col);
*/
		col = new DataGridColumn("email");
 		col.headerText = "E-mail";
 		col.width = 240;
		col.sortOnHeaderRelease = false;
		targetPath.PPreview.tmp_dg.addColumn(col);

		col = new DataGridColumn("checkflg");
 		col.headerText = "要確認";
 		col.width = 60;
		col.sortOnHeaderRelease = false;
		targetPath.PPreview.tmp_dg.addColumn(col);

		col = new DataGridColumn("sendflg");
 		col.headerText = "送信状態";
 		col.width = 60;
		col.sortOnHeaderRelease = false;
		targetPath.PPreview.tmp_dg.addColumn(col);

		col = new DataGridColumn("copeflg");
 		col.headerText = "対応";
 		col.width = 60;
		col.sortOnHeaderRelease = false;
		targetPath.PPreview.tmp_dg.addColumn(col);

		//行毎表示色分け
		targetPath.PPreview.tmp_dg.setStyle("alternatingRowColors", Array(0xFFFFFF, 0xEAEAEA));
		targetPath.PPreview.tmp_dg.setVScrollPolicy("auto");

		//ページング用変数設定
		var rowsPerPage = 24; //Math.floor((targetPath.PPreview.tmp_dg.height - targetPath.PPreview.tmp_dg.headerHeight)/targetPath.PPreview.tmp_dg.rowHeight);
		var pages = Math.ceil(temp_ds.dataProvider.length/rowsPerPage);
		var pages_arr = new Array({data:"",label:"全ページ"});
		for(var i=0;i<pages;i++){
			pages_arr.push({data:i,label:i+1+"ページ"})
		}

		//コンボボックスイベント設定
		var ComboListner = new Object();
		ComboListner.rowsPerPage = rowsPerPage;
		//Loadイベント
		targetPath.PPreview.pages_cb.onEnterFrame = function(){
			Owner.targetPath.PPreview.pages_cb.dataProvider = pages_arr
			if(Owner.targetPath.PPreview.pages_cb.dataProvider.length > 0){
				delete this.onEnterFrame;
			}
		}

		//Changeイベント
		ComboListner.change = function(eventObj){
			var startRec = this.rowsPerPage*eventObj.target.value;
			var endRec = this.rowsPerPage*(eventObj.target.value+1);
			if(eventObj.target.value == "" ){
				startRec = 0
				endRec = Owner.printtargetMC.dataProvider.length
				Owner.targetPath.PPreview.tmp_dg.vScrollPolicy = "auto";
			} else {
				Owner.targetPath.PPreview.tmp_dg.vScrollPolicy = "off";
			}
			temp_ds.dataProvider = Owner.printtargetMC.dataProvider.items.slice( startRec, endRec );
			Owner.targetPath.PPreview.tmp_dg.dataProvider = temp_ds.items
		}
		targetPath.PPreview.pages_cb.addEventListener( "change", ComboListner );


		//退避用DataSetからプレビュー用データグリッドへ
		targetPath.PPreview.tmp_dg.dataProvider = temp_ds.items

		//印刷ボタン
		targetPath.PPreview.pre_printBtn.onRelease = function (){
			Owner.doDGPrint(Owner.targetPath.PPreview.tmp_dg)
		}

	}
	

}