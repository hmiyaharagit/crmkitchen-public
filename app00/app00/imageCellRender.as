﻿import mx.core.UIComponent;
class imageCellRender extends UIComponent {
	var img:MovieClip;
	var url:String;
	var mcObj : MovieClip;
	var listOwner:MovieClip;
	var getCellIndex:Function;
	var getDataLabel:Function;
	var owner;
		
	function imageCellRender() {
	}
	function createChildren(Void):Void {

		//	画像orテキスト判別
		mcObj = createObject("TextArea", "TextArea", 1, {styleName:this, owner:this});
		mcObj.borderStyle = "none"
		mcObj.wordWrap = true

		size();
	}
	
	
	function setValue(str:String, item:Object, sel:Boolean):Void {
	//	var wk_checkstr = item[getDataLabel()].toString();
	//	var checkstr = wk_checkstr.substr( (wk_checkstr.length-3), 3);
		var checkstr = str.substr( (str.length-3), 3);
		
		//	画像orテキスト判別
		if( checkstr == 'jpg'){
//			mcObj.removeMovieClip();
			mcObj._visible = mcObj.enabled = false;
			img._visible = img.enabled = (item != undefined);
			if(str != this.url){
				img.loadMovie(str);
				this.onEnterFrame = function(){
					if(img.getBytesLoaded() >= img.getBytesTotal() && img.getBytesTotal() > 4 && img._width > 4){
						this.url = str
						size();
						delete this.onEnterFrame;
					}
				}
			}

		} else {
//			img.removeMovieClip();
			img._visible = img.enabled = false;
			mcObj._visible = mcObj.enabled = (item!=undefined);
			mcObj.text = str;
			if( getCellIndex().itemIndex%2 == 0 ){
				mcObj.backgroundColor  = 0xFFFFFF
			}else{
				mcObj.backgroundColor  = 0xECEDE4
			}
		}
	}
	
	function size(Void) : Void
	{
		var index = getCellIndex();
		var mcObj_w = listOwner.getColumnAt(index.columnIndex).width;
		var mcObj_h = listOwner.rowHeight;

		mcObj.setSize(mcObj_w, owner.height);
		mcObj._x = 0;
		mcObj._y = 0;
/*
		img.setSize(mcObj_w, owner.height);
		img._x = 0;
		img._y = 0;
*/


		if(img._width>0){
			var w_xscale = (mcObj_w/img._width)
			img._xscale = w_xscale*100
		}
		if(img._height>0){
			var w_yscale = (mcObj_h/img._height)
			img._yscale = w_yscale*100
		}
	}

	function getPreferredHeight(Void) : Number
	{
		return owner.height;
	}

	function getPreferredWidth(Void) : Number
	{
		return owner.width
	}
}
