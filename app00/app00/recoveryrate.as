﻿/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	Name			: recoveryrate
	Description		: ＣＳ調査回答率画面
	Usage			: var XXXXXX:recoveryrate = new recoveryrate( path );
	Attributes		: path:MovieClip			（基準パス）
	Extends			: CommonFunction(Class)
	Method			: initializedData				初期設定
					: getCRMGuestList_Result		検索実行正常終了時イベント
					: getCRMGuestList_Fault			検索実行不正終了時イベント
	Note			: none
	History			: 1) Coded by	nf)h.miyahara	2006/04/06
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
import mx.remoting.Service;
import mx.services.Log;
import mx.rpc.RelayResponder;
import mx.remoting.PendingCall;
import mx.remoting.debug.NetDebug;
import mx.remoting.debug.NetDebugConfig;
import mx.remoting.RecordSet;
import mx.rpc.ResultEvent;
import mx.rpc.FaultEvent;
import mx.controls.gridclasses.DataGridColumn
import mx.controls.ComboBox
import mx.containers.ScrollPane
import mx.data.binding.*;
import mx.data.components.DataSet
import mx.transitions.easing.*;
import mx.transitions.Tween;
import app00.conditionsetting;
import app00.downloader;
import mx.controls.Alert;
import mx.styles.CSSStyleDeclaration;
//import app00.commonfunc
import commonfunc

class app00.recoveryrate extends MovieClip{

	var gpath:MovieClip			//基準ムービークリップパス
	var rpath:MovieClip			//基準ムービークリップパス
	var resort_code:Number		//リゾートコード（0:共通）
	var gdepth:Number					//Depth
	var sobject:Object			//CFCsobject
	var opath:String			//CSVopath
	var sresult:DataSet			//検索結果退避用オブジェクト
	var dsarr:Array				//データソース事前指定用配列
	var slctddate:Array			//選択日
	var cfunc:commonfunc
	var myCond:conditionsetting

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: recoveryrate
		Description		: Constructor
		Usage			: recoveryrate( myGrid:DataGrid, myPath:MovieClip );
		Attributes		: myGrid:DataGrid	（結果格納用データグリッドコンポーネント名）
						: myPath:MovieClip	（基準ＭＣパス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function recoveryrate( myPath:MovieClip , r_id:Number) {
		gpath = myPath;
		rpath = _root;
		gdepth = gpath.getNextHighestDepth();
		resort_code = r_id;
		dsarr = new Array();
		slctddate = new Array();
		cfunc = new commonfunc()
		myCond = new conditionsetting(gpath.resortselect,this,gpath);

		var myDebug = mx.remoting.debug.NetDebug.initialize();
		sobject = new Service( "http://" + _global.hostAddress + "/flashservices/gateway/", null , _global.servicePath + ".app00.cfc.recoveryrate" , null , null ); 

		var rstPC:PendingCall = sobject.getResort(_global.resort_code);
		rstPC.responder = new RelayResponder( this, "getResort_Result", "Service_Fault" );


		
		//NetDebbuger設定
		var config:NetDebugConfig = sobject.connection.getDebugConfig();
		config.app_server.trace = true;
		config.client.trace = true;
		//ＭＣ初期化
		initializedData();
		if(_global.currentscreen==undefined){
			_global.currentscreen=this
			_global.currentscreen.menu00event();
			_global.currentscreen.resetbtnevent();
		}
	}
	
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getResort_Result
		Description		: エリアマスタ取得正常終了時イベント
		Usage			: getResort_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2005/04/26
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	public function getResort_Result(ReEvt:ResultEvent):Void{
		var owner = this
		myCond.conditemsetting(ReEvt.result.m_resort,"12")			// 旅館
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: Service_Fault
		Description		: 設問情報取得不正終了時イベント
		Usage			: Service_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	public function Service_Fault(FltEvt:FaultEvent):Void{
		/**/
		var ContentsMsg = "情報の取得ができませんでした。\rエラーメッセージ："+ FltEvt.fault.description
		var HeaderMsg = "情報取得エラー"
		var AlertObj = mx.controls.Alert.show(ContentsMsg, HeaderMsg, null, gpath, null, null, null)
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: initializedData
		Description		: 初期設定
		Usage			: initializedData();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
						: 2) Updated by	nf)h.miyahara	2005/10/11 文言変更
						: 3) Updated by	nf)h.miyahara	2005/10/11 満足度評価設問追加
						: 4) Updated by	nf)h.miyahara	2005/10/13 カテゴリＩＤパラメータ追加
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function initializedData(){
		var owner = this
		_global.currentscreen=this

		//初期状態：結果エリア非表示 & 出力指示MC無効化
		gpath.ResultsPanel._visible = false;
		rpath.dconditionbox.parents = _global.currentscreen
		myCond.initializedData();

		
		//	--旅館選択
		if(resort_code != 0){
			gpath.resortselect._visible = gpath.resortselect.enabled = false
		}
		gpath.resortselect.selectbtn._visible = gpath.resortselect.resort_code.enabled = false
		gpath.resortselect.selectbtn.onRelease = function(){
			owner.cboxtween(owner.rpath.dconditionbox,false);
			
			owner.gpath.resortselect.duplicateMovieClip("owner.rpath.conditionbox.cond_items.conditem12","ss",10);
			owner.gpath.resortselect.resort_code._visible = owner.gpath.resortselect.resort_code.enabled = true;
		}

		//日付条件入力MC表示
		gpath.dconditionview.onRelease = function(){
			owner.cboxtween(owner.rpath.dconditionbox);
			owner.gpath.resortselect.resort_code._visible = owner.gpath.resortselect.resort_code.enabled = false;
		}

		//検索ボタン設定
		gpath.menu00.onRelease = function(){
			owner = _global.currentscreen
			var wk_val = owner.verifycondition()
			if(wk_val.length==0){
				var conditionParam = new Object();

				var cbox = owner.rpath.conditionbox
				//検索条件オブジェクト作成
				var conditionParam = new Object();
				conditionParam.dcondtion = owner.gpath.dselection.selected
				conditionParam.selecteddays = owner.slctddate
				conditionParam.arg_resort_code = owner.myCond.paramsetting(owner.gpath.resortselect.cond_items.conditem12.checkbox_sp.content);
				if(owner.resort_code == 0){
					conditionParam.arg_resort_code = owner.myCond.paramsetting(owner.gpath.resortselect.cond_items.conditem12.checkbox_sp.content);
					if(conditionParam.arg_resort_code.length == 0){
						conditionParam.arg_resort_code = new Array();
						conditionParam.arg_resort_code.push(0);
					}
				} else {
					conditionParam.arg_resort_code = new Array();
					conditionParam.arg_resort_code.push(owner.resort_code);
				}

				var CSTotlPC:PendingCall = owner.sobject.getRequestcount(conditionParam);
				CSTotlPC.responder = new RelayResponder( owner, "getRequestcount_Result", "getRequestcount_Fault" );
				owner.cboxtween(owner.rpath.dconditionbox,false);
				owner.cboxtween(owner.rpath.conditionbox,false);
				owner.cboxtween(owner.gpath.qselection,false);
				owner.myCond.waiting(owner.gpath,true)
				owner.gpath.ResultsPanel._visible = false;

				owner.currentcondition = new Object();
				owner.currentcondition = conditionParam
			}else{
				var ErrorMsg = wk_val
				var HeaderMsg = "集計できません                 "
				var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, gpath, null, null, null)
			}
		}

		//CSVボタン設定
		//ファイル出力ボタン設定
		gpath.menu02._visible = gpath.menu02.enabled = false;
		gpath.menu02.onRelease = function(){
			if(!owner.gpath.ResultsPanel._visible){
				var ErrorMsg = "出力データがありません。先に集計を行ってください。"
				var HeaderMsg = "ファイル出力不可                                   "
				var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, gpath, null, null, null)
			} else {
				var AlertObj = mx.controls.Alert.show("一覧をファイルに出力します。よろしいですか？", "ファイル出力", Alert.YES | Alert.NO, owner.gpath, myClickHandler, null , Alert.OK);
			}
		}

		//CSV出力ボタンイベント
		var myClickHandler = new Object();
		myClickHandler = function(evt){
			if(evt.detail == Alert.YES){
				owner.gpath.opcondition._visible = false;
				owner.gpath.opcondition.enabled = false;
				owner.OutputCSV();
			}
		}

		//日付条件指定オプションイベント
		var cbListener:Object = new Object();
		cbListener.click = function(evt_obj:Object) {
			owner.slctddate = new Array();
			owner.rpath.dconditionbox.initializedData();
		};
		gpath.dselection.addEventListener("click", cbListener);

		//日付オーバーフロー
		gpath.dmorebtn.enabled = false
		gpath.dmorebtn.onRelease = function(){
			owner.myCond.viewmorechips(owner.gpath.morechips,this)
		}
		//gpath.morechips.labeltxt.autoSize = "left"

		//集計条件オーバーフロー
		gpath.cmorebtn.enabled = false
		gpath.cmorebtn.onRelease = function(){
			owner.myCond.viewmorechips(owner.gpath.morechips,this)
		}

		//タブ設定
		TabInitialize();

		//ローディング用ＭＣ削除処理
		gpath._x = 0
		cfunc.loadingdelete(gpath._parent)
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: TabInitialize
		Description		: タブ設定
		Usage			: TabInitialize();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function TabInitialize():Void{
		gpath.tabChildren = true
		gpath.ConditionBox.tabChildren = true
		gpath.ConditionBox.category_cb.tabIndex = 2;
		gpath.ConditionBox.answertype_cb.tabIndex = 4;
		gpath.menu00.tabIndex = 6;
		gpath.clearBtn.tabIndex = 8;
		gpath.menu02.tabIndex = 10;
		gpath.previewBtn.tabIndex = 12;
		gpath.printBtn.tabIndex = 14;
		gpath.changeBtn.tabIndex = 16;

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getOAAnswers_Result
		Description		: 検索実行正常終了時イベント
		Usage			: getOAAnswers_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getRequestcount_Result(ReEvt:ResultEvent):Void{
		myCond.waiting(gpath,false)
		if( ReEvt.result.mRecordsAvailable == 0){
			var ErrorMsg = "該当レコードはありませんでした。\r検索条件を変更して再度検索を行ってください。"
			var HeaderMsg = "検索結果                                   "
			var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, null, null, null, null)
			var RecSet:RecordSet = RecordSet(ReEvt.result);
			gpath.ResultsPanel._visible = false;
		} else {
			//初期設定
			var owner = this;
			gpath.ResultsPanel._visible = true;

			//レコードセットオブジェクトに結果代入
			var RecSet:RecordSet = RecordSet(ReEvt.result);

			//検索結果をデータセットに退避
			sresult = new DataSet();
			sresult.dataProvider = ReEvt.result;
			dsarr = new Array();
			for( var i=0; i<ReEvt.result.mRecordsAvailable; i++ ){
				gpath.ResultsPanel["resultsbar_" + i].samples_txt.text = ReEvt.result.items[i].samples
				gpath.ResultsPanel["resultsbar_" + i].answers_txt.text = ReEvt.result.items[i].answers
				gpath.ResultsPanel["resultsbar_" + i].rate_txt.text = ReEvt.result.items[i].rate
				dsarr.push({
					samples:ReEvt.result.items[i].samples,
					answers:ReEvt.result.items[i].answers,
					rate:ReEvt.result.items[i].rate,
					label:ReEvt.result.items[i].label
				})
			}
			gpath.menu02._visible = gpath.menu02.enabled = true;
		}
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getRequestcount_Fault
		Description		: 検索実行不正終了時イベント
		Usage			: getOAAnswers_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getRequestcount_Fault(FltEvt:FaultEvent):Void{
		var ErrorMsg = "検索できませんでした。\rエラーメッセージ："+ FltEvt.fault.description
		var HeaderMsg = "検索エラー                                 "
		var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, null, null, null, null)
		myCond.waiting(gpath,false)
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: OutputCSV
		Description		: ＣＳＶファイル出力
		Usage			: OutputCSV( Obj );
		Attributes		: Obj : （コンファームを呼出した呼出元クラスインスタンス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function OutputCSV(){
		var argumentsParam = new Object();
		argumentsParam.resultObj = dsarr
		argumentsParam.condseleted = gpath.selectedtext.text
		argumentsParam.selecteddate = gpath.dselected_txt.text
		argumentsParam.resort_code = resort_code
		var RsltOPPC:PendingCall = sobject.OutputCSV(argumentsParam);
		RsltOPPC.responder = new RelayResponder( this, "OutputCSV_Result", "OutputCSV_Fault" );
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: OutputCSV_Result
		Description		: ＣＳＶ出力正常終了時
		Usage			: OutputCSV_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function OutputCSV_Result(ReEvt:ResultEvent):Void{
		var FileObj:downloader = new downloader(gpath,ReEvt.result.filename,ReEvt.result.newname)
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: OutputCSV_Fault
		Description		: ＣＳＶ出力不正終了時
		Usage			: OutputCSV_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function OutputCSV_Fault(FltEvt:FaultEvent):Void{
		var ErrorMsg = "ＣＳＶファイルが出力できませんでした。\rエラーメッセージ：" + FltEvt.fault.description
		var HeaderMsg = "サービスエラー"
		var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, gpath, null, null, null)
		//gpath.loadmask._y = -1000
		myCond.waiting(gpath,false)
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: verifycondition
		Description		: 検索条件のチェック
		Usage			: verifycondition();
		Attributes		: Obj : （検索条件オブジェクト）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/05
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function verifycondition(){
		var retmsg = ""
		if(this.slctddate.length < 1){retmsg="日付を選択してください"}
		return retmsg
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: cboxtween
		Description		: 条件入力状態解除
		Usage			: cboxtween();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/30
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	public function cboxtween(obj,bl){
		if(obj._x < 0 && bl==undefined){
			var _xtween:Object = new Tween(obj, "_x", Back.easeOut, -20, 20, 0.5, true);
		}else if(!bl){
			obj._x = -1000;
		}else{
			var _xtween:Object = new Tween(obj, "_x", Back.easeOut, 20, -1000, 1, true);
		}
	}

}
