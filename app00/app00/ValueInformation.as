﻿/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	Name			: ValueInformation
	Description		: スペックシート表示画面
	Usage			: var XXXXXX:ValueInformation = new ValueInformation( path );
	Attributes		: path:MovieClip			（基準パス）
	Extends			: CommonFunction(Class)
	Method			: initializedData		初期設定
					: getCRMGuestList_Result	検索実行正常終了時イベント
					: getCRMGuestList_Fault	検索実行不正終了時イベント
	Note			: none
	History			: 1) Coded by	nf)h.miyahara	2004/07/09
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
import mx.remoting.Service;
import mx.services.Log;
import mx.rpc.RelayResponder;
import mx.remoting.PendingCall;
import mx.remoting.debug.NetDebug;
import mx.remoting.debug.NetDebugConfig;
import mx.rpc.ResultEvent;
import mx.rpc.FaultEvent;
import mx.controls.TextArea;
import mx.controls.Label;
import mx.controls.Alert;
import commonfunc;

class app00.ValueInformation extends MovieClip{

	var gpath:MovieClip	//基準ムービークリップパス
	var gdepth:Number			//Depth
	var sobject:Object	//CFCsobject
	var psource:MovieClip	//ScrollPane内MovieClip
	var	reserve_code:Number		//
	var	room_code:Number		//
	var	target_id:Number
	var	targetData:Object		//表示対象データ
	var customer_code:Number	//customer_code
	var OpenRecord:Number		//エキスパンド開始ＭＣ
	var dsarr:Array		//データソース事前指定用配列
	var cfunc:commonfunc
	var resort_code:Number

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: ValueInformation
		Description		: Constructor
		Usage			: ValueInformation( myPath:MovieClip );
		Attributes		: myGrid:DataGrid	（結果格納用データグリッドコンポーネント名）
						: myPath:MovieClip	（基準ＭＣパス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function ValueInformation( mySource:MovieClip, myPath:MovieClip, rcd:Number, rmcd:Number, tid:Number ,rescd:Number ) {
		psource = mySource;
		gpath = myPath;
		gdepth = gpath.getNextHighestDepth();
		reserve_code = rcd;
		room_code = rmcd;
		target_id = tid
		resort_code = rescd
		cfunc = new commonfunc()
		var myDebug = mx.remoting.debug.NetDebug.initialize();
		sobject = new Service( "http://" + _global.hostAddress + "/flashservices/gateway/", null , _global.servicePath + ".app00.cfc.specsheet" , null , null ); 
		
		//NetDebbuger設定
		var config:NetDebugConfig = sobject.connection.getDebugConfig();
		config.app_server.trace = true;
		config.client.trace = true;

		var conditionParam = new Object({reserve_code:reserve_code,room_code:room_code,target_id:target_id,resort_code:resort_code});
		var VLdtlPC:PendingCall = sobject.getValueInformation(conditionParam);
		VLdtlPC.responder = new RelayResponder( this, "getValueInformation_Result", "getValueInformation_Fault" );
		
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: initializedData
		Description		: 初期設定
		Usage			: initializedData();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function initializedData(){

		var owner = this

		//ScrollPane設定
		gpath.specsheet_sp.borderStyle = "none"
		gpath.specsheet_sp.vPageScrollSize = gpath.specsheet_sp.height;
		gpath.specsheet_sp.vLineScrollSize = gpath.specsheet_sp.height;
		//gpath.specsheet_sp.stop();

		//コンボボックス先行アクセス
		var tmpList = gpath.summary_cb.dropdown;

		//メニュー切替
		gpath.changeBtn.onRelease = function(){
			owner.SetMenuList(owner.gpath);
		}

		//閉じるボタン
		gpath.ss_closeBtn.onRelease = function(){
			owner.gpath.removeMovieClip();
			owner.gpath._parent.EnabledScreen.removeMovieClip();
		}

		//印刷ボタン設定
		gpath.printBtn.onRelease = function(){
			owner.TxtprintPreview("preview");
			//owner.TxtprintPreview("print");
		}
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getValueInformation_Result
		Description		: ValueInformaiton取得正常終了時イベント
		Usage			: getValueInformation_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/12/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getValueInformation_Result(ReEvt:ResultEvent):Void{
		var owner = this;

		if(gpath.valueInformation != undefined){
			//trace(gpath.valueInformation.reserve_code)
			var VLdtlPC:PendingCall = sobject.checkIn(gpath.valueInformation.reserve_code,gpath.valueInformation.room_code,owner.resort_code);
			VLdtlPC.responder = new RelayResponder( this, "checkIn_Result", "checkIn_Fault" );
			gpath.valueInformation.removeMovieClip();

		}

		if(ReEvt.result.executeFlg != 1){
			gpath.attachMovie("valueInformation", "valueInformation", 100, {_y:100, _x:100 })
			gdepth++
			gpath.valueInformation.reserve_code = ReEvt.result.reserve_code
			gpath.valueInformation.room_code = ReEvt.result.room_code
			gpath.valueInformation.MCID = ReEvt.result.target_id
			//cfunc.ChangeEnabled( gpath.valueInformation, "vi_rollbackBtn" , false )
			//cfunc.ChangeEnabled( gpath, "ss_closeBtn" , false )
			//ドラッガブル処理
			gpath.valueInformation.valueInformationHeader.onPress = function() {
	    		this._parent.startDrag();
			}
			gpath.valueInformation.valueInformationHeader.onRelease = function() {
			    this._parent.stopDrag();
			}
			gpath.valueInformation.valueInformationHeader.onReleaseOutside = function() {
			    this._parent.stopDrag();
			}
			gpath.valueInformation.vi_regBtn.onRelease = function() {
				/*
				var OKFunction = owner.setValueInformation
				owner.cfunc.ShowConfirm( this._parent , "", owner , OKFunction ,0 );
				*/

				var HeaderMsg = "ValueInformation登録"
				var BodyMsg = "ValueInformationを登録します。よろしいですか？"
				var AlertObj = mx.controls.Alert.show(BodyMsg, HeaderMsg, Alert.YES | Alert.NO, owner.gpath, regHandler, null , Alert.OK);
			}
			//登録イベント
			var regHandler = new Object();
			regHandler = function(evt){
				if(evt.detail == Alert.YES){
					owner.setValueInformation();
				}
			}

			gpath.valueInformation.vi_rollbackBtn.onRelease = function() {
				var HeaderMsg = "ValueInformation登録"
				var BodyMsg = "ValueInformationをひとつ前のデータに戻します。\nよろしいですか？"
				var AlertObj = mx.controls.Alert.show(BodyMsg, HeaderMsg, Alert.YES | Alert.NO, owner.gpath, rollbackHandler, null , Alert.OK);
			}

			//rollbackイベント
			var rollbackHandler = new Object();
			rollbackHandler = function(evt){
				if(evt.detail == Alert.YES){
					owner.setRollBack();
				}
			}

			gpath.valueInformation.vi_closeBtn.onRelease = function() {
				var VLdtlPC:PendingCall = owner.sobject.checkIn(ReEvt.result.reserve_code,ReEvt.result.room_code);
				VLdtlPC.responder = new RelayResponder( owner, "checkIn_Result", "checkIn_Fault" );
				owner.gpath.valueInformation.removeMovieClip();

			}

			gpath.valueInformation.vi_printBtn.onRelease = function() {
				//owner.TxtprintPreview("preview");
				owner.TxtprintPreview("print");
			}
/*
			gpath.valueInformation.createClassObject(TextArea, "data_txt", gdepth, {_x:10.3,_y:27.5})
			gdepth++
			gpath.valueInformation.data_txt.setSize(580,300);
			gpath.valueInformation.data_txt.wordWrap = true;
			gpath.valueInformation.data_txt.vScrollPolicy = "auto"
			gpath.valueInformation.data_txt.hScrollPolicy = "auto"
*/

			var myTextFormat = new TextFormat(); 
			gpath.valueInformation.data_txt.myTextFormat.font = "_ゴシック"; 
			gpath.valueInformation.data_txt.setNewTextFormat(myTextFormat);
			gpath.valueInformation.data_txt.borderColor = 0xCCCCCC
			gpath.valueInformation.data_txt.text = ReEvt.result.executeItem
			//trace(this._parent.data_txt.text)
		} else {
			var Message = "現在" + ReEvt.result.checkoutuser + "で編集作業中です。\rしばらくたってから再度お願いします。"
			cfunc.ShowConfirm( gpath ,Message , this , null );
		}
		
	}
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getValueInformation_Fault
		Description		: ValueInformaiton取得失敗時イベント
		Usage			: getValueInformation_Fault(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/12/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getValueInformation_Fault(FltEvt:FaultEvent):Void{
		var ErrorMsg = "ValueInformationが取得できませんでした。\rエラーメッセージ："+ FltEvt.fault.description
		var HeaderMsg = "ValueInformation     "
		var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, gpath, null, null, null)
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: checkIn_Result
		Description		: ValueInformaiton取得正常終了時イベント
		Usage			: checkIn_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/12/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function checkIn_Result(ReEvt:ResultEvent):Void{
		//cfunc.ChangeEnabled( gpath, "ss_closeBtn" , true )
	}
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: checkIn_Fault
		Description		: ValueInformaiton取得失敗時イベント
		Usage			: checkIn_Fault(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/12/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function checkIn_Fault(FltEvt:FaultEvent):Void{
		//cfunc.ChangeEnabled( gpath, "ss_closeBtn" , true )
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setValueInformation
		Description		: 気づき情報の登録
		Usage			: setValueInformation(Obj);
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/12/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function setValueInformation(){
		//インスタンス入替
	//	this = Obj
		var arguments1 =  gpath.valueInformation.reserve_code
		var arguments2 =  gpath.valueInformation.room_code
		var arguments3 =  gpath.valueInformation.data_txt.text
		var arguments4 =  gpath.valueInformation.MCID
		var arguments5 =  resort_code

		var conditionParam = new Object({reserve_code:arguments1,room_code:arguments2,valueInformation:arguments3,target_id:arguments4,resort_code:arguments5});
			
		//CFCメソッド呼出
		var VIdtlPC:PendingCall = sobject.setValueInformation(conditionParam);
		VIdtlPC.responder = new RelayResponder( this, "setValueInformation_Result", "setValueInformation_Fault" );
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setValueInformation_Result
		Description		: 気づき情報の登録成功時イベント
		Usage			: setValueInformation_Result(ReEvt);
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/12/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function setValueInformation_Result(ReEvt:ResultEvent):Void{
		//cfunc.ChangeEnabled( gpath.valueInformation, "vi_rollbackBtn" , true )

//		cfunc.ShowConfirm( gpath ,ReEvt.result.errorMsg , this , null );
		var HeaderMsg = "ValueInformation     "
		var BodyMsg = ReEvt.result.errorMsg
		var AlertObj = mx.controls.Alert.show(BodyMsg, HeaderMsg, null, gpath, null, null, null)

		psource[ "ReserveRecord_" + ReEvt.result.target_id ].RB_Item_31.data_txt.text = ReEvt.result.executeItem
		psource[ "ReserveRecord_" + ReEvt.result.target_id ].RB_Item_31.data_txt.setSize(375,40);

		//2005/04/05 h.miyahara(NetFusion) ADD	ターゲット検索結果からの呼出ケース
		//psource[ "resultBar_" + ReEvt.result.target_id ].viBtn.valueinfo = ReEvt.result.executeItem
	}
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setValueInformation_Fault
		Description		: 気づき情報の登録失敗時イベント
		Usage			: setValueInformation_Fault(FltEvt);
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/12/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function setValueInformation_Fault(FltEvt:FaultEvent):Void{
		var ErrorMsg = "ValueInformationの登録が正常に行われませんでした。\rエラーメッセージ："+ FltEvt.fault.description
		var HeaderMsg = "ValueInformation登録     "
		var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, gpath, null, null, null)
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setRollBack
		Description		: 気づき情報のロールバック
		Usage			: setRollBack(Obj);
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/12/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function setRollBack(){
		var arguments1 =  gpath.valueInformation.reserve_code
		var arguments2 =  gpath.valueInformation.room_code
		var arguments3 =  gpath.valueInformation.MCID
		var arguments4 =  resort_code

		var conditionParam = new Object({reserve_code:arguments1,room_code:arguments2,target_id:arguments3,resort_code:arguments4});
		//CFCメソッド呼出
		var VIdtlPC:PendingCall = sobject.setRollBack(conditionParam);
		VIdtlPC.responder = new RelayResponder( this, "setRollBack_Result", "setRollBack_Fault" );
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setRollBack_Result
		Description		: 気づき情報のロールバック成功時イベント
		Usage			: setRollBack_Result(ReEvt);
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/12/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function setRollBack_Result(ReEvt:ResultEvent):Void{
		//cfunc.ChangeEnabled( gpath.valueInformation, "vi_rollbackBtn" , false )
		var HeaderMsg = "ValueInformation     "
		var BodyMsg = ReEvt.result.errorMsg
		var AlertObj = mx.controls.Alert.show(BodyMsg, HeaderMsg, null, gpath, null, null, null)

		psource[ "ReserveRecord_" + ReEvt.result.target_id ].RB_Item_31.data_txt.text = ReEvt.result.executeItem
		gpath.valueInformation.data_txt.text = ReEvt.result.executeItem
		psource[ "ReserveRecord_" + ReEvt.result.target_id ].RB_Item_31.data_txt.setSize(375,40);
	}
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setRollBack_Fault
		Description		: 気づき情報のロールバック失敗時イベント
		Usage			: setRollBack_Fault(FltEvt);
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/12/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function setRollBack_Fault(FltEvt:FaultEvent):Void{
		var ErrorMsg = "ValueInformationの登録が正常に行われませんでした。\rエラーメッセージ："+ FltEvt.fault.description
		var HeaderMsg = "ValueInformation登録     "
		var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, gpath, null, null, null)
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: sweeper_Result
		Description		: 気づき情報ロック解除成功時イベント
		Usage			: sweeper_Result(ReEvt);
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/12/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function sweeper_Result(ReEvt:ResultEvent):Void{
		trace("Sweep")
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: sweeper_Fault
		Description		: 気づき情報ロック解除失敗時イベント
		Usage			: sweeper_Fault(FltEvt);
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/12/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function sweeper_Fault(FltEvt:FaultEvent):Void{
		var ErrorMsg = "ValueInformationの登録が正常に行われませんでした。\rエラーメッセージ："+ FltEvt.fault.description
		var HeaderMsg = "ValueInformation登録     "
		var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, gpath, null, null, null)
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: TxtprintPreview
		Description		: 
		Usage			: sweeper_Fault(FltEvt);
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/12/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function TxtprintPreview(mode){
		//変数設定
		var owner = this;

		//ターゲット用印刷ＭＣ生成
		gpath.createEmptyMovieClip("PrintSource", gdepth);
		gdepth++;

/**/
		gpath.PrintSource.createClassObject(Label, "label_txt", gdepth);
		gpath.PrintSource.label_txt.autoSize = "left"
		gdepth++;
		gpath.PrintSource.label_txt.text = "ValueInformation登録"

		//gpath.PrintSource.createClassObject(TextArea, "print_txt", gdepth,{_y:25});
		gpath.PrintSource.createClassObject(Label, "print_txt", gdepth,{_y:25});
		gpath.PrintSource.print_txt.autoSize = "left"
		gdepth++;
		gpath.PrintSource.print_txt.text = gpath.valueInformation.data_txt.text

/*
		gpath.PrintSource.print_txt.setSize(900,500);
		gpath.PrintSource.print_txt.vScrollPolicy = "off"
		gpath.PrintSource.print_txt.hScrollPolicy = "off"
		gpath.PrintSource.print_txt.text = gpath.valueInformation.data_txt.text
		gpath.PrintSource.print_txt.borderStyle = "solid"
*/
		var printtargetMC = gpath.PrintSource
		var headerArea_h = 30;
		var headerArea_x = 10;
		var temp:mx.data.types.Str;
		var temp:mx.data.types.Num;
		var temp_MC_y = printtargetMC._y;					//対象ＭＣＹ座標退避
		var temp_MC_h = printtargetMC.height;				//対象ＭＣ高さ退避
		var temp_MC_depth = printtargetMC.getDepth();		//対象ＭＣ深度退避

		//プレビュー用ＭＣ
/*
		gpath.attachMovie("PreviewHeader","PPreview", gdepth,{_x:gpath._x, _y:gpath._y});
		gdepth++;
		gpath.PPreview.beginFill(0xFFFFF9);
		gpath.PPreview.moveTo(gpath.PPreview._x,gpath.PPreview._height);
		gpath.PPreview.lineTo(Stage.width, gpath.PPreview._height);
		gpath.PPreview.lineTo(Stage.width, Stage.height);
		gpath.PPreview.lineTo(gpath.PPreview._x, Stage.height);
		gpath.PPreview.lineTo(gpath.PPreview._x, gpath.PPreview._height);
		gpath.PPreview.endFill();

		//プレビュー用ＳＰ位置設定
		printtargetMC.swapDepths( gpath.PPreview.getDepth()+10 );
*/
		//閉じるボタン
/*
		gpath.PPreview.pre_closeBtn.onRelease = function (){
			this._parent.removeMovieClip();
			owner.gpath.PrintSource.removeMovieClip();
		}
*/
		//印刷ボタンより直接コール
		if( mode =="print" ){
			gpath.PrintSource._y = 1000;
			/**/
			printtargetMC.onEnterFrame = function(){
				if( printtargetMC.print_txt.length != 0 ){
					var tmp = owner.doMCPrint("print");
					if(tmp == undefined){
						owner.gpath.PrintSource.removeMovieClip();
					}
				}
				delete this.onEnterFrame;
			}
		}
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: doMCPrint
		Description		: 印刷実行
		Usage			: doMCPrint(tgtMC);
		Attributes		: tgtMC			印刷対象ＭＣ
		Note			: 
		History			: 1) Coded by	nf)h.miyahara	2004/09/27
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function doMCPrint(mode){
		trace("doMCPrint:mode  " + mode)
		var printtargetMC = gpath.PrintSource;
		var PrintQueue : PrintJob = new PrintJob();
		var prev_vPosition:Number = printtargetMC.print_txt.vPosition;
		var prev_width:Number = printtargetMC.print_txt.width;
		var prev_height:Number = printtargetMC.print_txt.height;
		//var RecordCount:Number = printtargetMC.dataProvider.length
/*
		if( PrintQueue.start() != true)
		return;
		printtargetMC.print_txt.setSize(PrintQueue.pageWidth,printtargetMC.print_txt.height);
		trace(PrintQueue.pageWidth)
		trace(PrintQueue.pageHeight)
		//ページング用変数設定
		var rowsPerPage = 30
		//var Pagesize = printtargetMC.print_txt.height + 20; //Math.floor((targetPath.PPreview.tmp_dg.height - targetPath.PPreview.tmp_dg.headerHeight)/targetPath.PPreview.tmp_dg.rowHeight);
		var Pagesize = PrintQueue.pageHeight; //Math.floor((targetPath.PPreview.tmp_dg.height - targetPath.PPreview.tmp_dg.headerHeight)/targetPath.PPreview.tmp_dg.rowHeight);
		var pages = Math.ceil(printtargetMC._height/Pagesize);
		//var pages = Math.ceil((printtargetMC.print_txt.height+20)/Pagesize);
		var headerHeight = 20;

		//trace("printtargetMC.print_txt.height: " + printtargetMC.print_txt.height)
		//trace("pages: " + pages + " Pagesize: " + Pagesize)
		//trace("pages: " + Math.ceil(printtargetMC._height/Pagesize) + " Pagesize: " + Pagesize)

		//対象ページ確認
		var startCnt = 0
		var yStart = 0;
		var yEnd = PrintQueue.pageHeight;
		for ( var i=startCnt; i<pages; i++ ) {
			
			trace( (i+1) + "ページ目を印刷")
			
			var startRec = rowsPerPage*i*20;
			printtargetMC.print_txt.vScrollPolicy = "off";
			printtargetMC.print_txt.vPosition = startRec
			trace("yStart " + yStart + " yEnd:" + yEnd )
			var b= {xMin:0,xMax:printtargetMC._width,yMin:yStart,yMax:yEnd};
			yStart = yEnd
			yEnd = yEnd + PrintQueue.pageHeight

		}
*/


//印刷用
//印刷用
/**/
		if( PrintQueue.start() != true)
		return;
		if(printtargetMC.print_txt.width > PrintQueue.pageWidth){
			var s_w = PrintQueue.pageWidth/printtargetMC.print_txt.width
			var r_h = printtargetMC.print_txt.width/PrintQueue.pageWidth
			printtargetMC.print_txt.scaleX = s_w*100
			printtargetMC.print_txt.scaleY = s_w*100
			var resize_h = Math.floor(printtargetMC.print_txt.height*r_h)
			printtargetMC.print_txt.setSize(PrintQueue.pageWidth*r_h,resize_h);
			//printtargetMC.print_txt.setSize(PrintQueue.pageWidth,printtargetMC.print_txt.height);
		} else {
			printtargetMC.print_txt.setSize(PrintQueue.pageWidth,printtargetMC.print_txt.height);
		}
		var rowsPerPage = 9
		//var Pagesize = printtargetMC.print_txt.height + 20; //Math.floor((targetPath.PPreview.tmp_dg.height - targetPath.PPreview.tmp_dg.headerHeight)/targetPath.PPreview.tmp_dg.rowHeight);
		//var pages = Math.ceil(printtargetMC._height/Pagesize);
		var Pagesize = PrintQueue.pageHeight; //Math.floor((targetPath.PPreview.tmp_dg.height - targetPath.PPreview.tmp_dg.headerHeight)/targetPath.PPreview.tmp_dg.rowHeight);
		var pages = Math.ceil(printtargetMC._height/Pagesize);


		var headerHeight = 0;

		var startCnt = 0
		var yStart = 0;
		var yEnd = PrintQueue.pageHeight;
		for ( var i=startCnt; i<pages; i++ ) {
			//var b= {xMin:0,xMax:printtargetMC._width,yMin:yStart,yMax:yEnd};
			var b= {xMin:0,xMax:printtargetMC.print_txt.width,yMin:yStart,yMax:yEnd};
			yStart = yEnd
			yEnd = yEnd + PrintQueue.pageHeight*r_h
			PrintQueue.addPage(printtargetMC,b);
		}

		PrintQueue.send();
		
		delete PrintQueue;


		//印刷ボタンより直接コール
		if( mode =="print" ){
			gpath.PrintSource.removeMovieClip();
			printtargetMC.removeMovieClip();
		}
	}


}
