﻿/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	Name			: downloader
	Description		: CRM用検索画面
	Usage			: var XXXXXX:downloader = new downloader( path );
	Attributes		: path:MovieClip			（基準パス）
	Extends			: CommonFunction(Class)
	Method			: initializedData		初期設定
					: getCRMGuestList_Result	検索実行正常終了時イベント
					: getCRMGuestList_Fault	検索実行不正終了時イベント
	Note			: none
	History			: 1) Coded by	nf)h.miyahara	2004/07/09
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
import mx.remoting.Service;
import mx.services.Log;
import mx.rpc.RelayResponder;
import mx.remoting.PendingCall;
import mx.controls.Alert;
import mx.utils.Delegate;
import flash.net.FileReference;

class app00.downloader {

	var GroundPath:MovieClip	//基準ムービークリップパス
	var ServiceObject:Object	//CFCServiceObject
	var outputPath:String		//出力先パス
	var filename:String			//出力先ファイル名称
	var fileRef:FileReference;

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: downloader
		Description		: Constructor
		Usage			: downloader( myPath:MovieClip );
		Attributes		: myGrid:DataGrid	（結果格納用データグリッドコンポーネント名）
						: myPath:MovieClip	（基準ＭＣパス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function downloader( myPath:MovieClip,oPath:String, fname:String ) {
		var Owner = this
		var dllistener:Object = new Object();

		dllistener.onComplete = function(file:FileReference):Void {
			var HeaderMsg ="ダウンロード完了                "
			var ErrorMsg = "ＣＳＶファイルをダウンロードしました"
			var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, Owner.GroundPath, null, null, null)
		}
		dllistener.onCancel = function():Void {
			var HeaderMsg ="キャンセル                "
			var ErrorMsg = "ダウンロードをキャンセルしました"
			var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, Owner.GroundPath, null, null, null)
		}
		dllistener.onHTTPError = function():Void {
			var HeaderMsg ="HTTPError                "
			var ErrorMsg = "HTTPErrorにより、ダウンロードに失敗しました。"
			var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, Owner.GroundPath, null, null, null)
		}
		dllistener.onIOError = function():Void {
			var HeaderMsg ="IOError                "
			var ErrorMsg = "IOErrorにより、ダウンロードに失敗しました。"
			var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, Owner.GroundPath, null, null, null)
		}
		dllistener.onSecurityError = function():Void {
			var HeaderMsg ="SecurityError                "
			var ErrorMsg = "SecurityErrorにより、ダウンロードに失敗しました。"
			var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, Owner.GroundPath, null, null, null)
		}

		GroundPath = myPath;
		outputPath = oPath;
		filename = fname
		fileRef = new FileReference();
		fileRef.addListener(dllistener);

		var cHandler = new Object();
		cHandler = function(evt){
			var success:Boolean;
			success = Owner.fileRef.download( Owner.outputPath, Owner.filename);
			if(success == false)
			{
				var HeaderMsg ="取得失敗                "
				var ErrorMsg = "ファイル取得に失敗しました "
				var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, GroundPath, null, null, null)
			}
		}
		var AlertObj = mx.controls.Alert.show("ファイル出力しました", "出力完了", Alert.YES, GroundPath, cHandler, null , Alert.OK);

	}
}