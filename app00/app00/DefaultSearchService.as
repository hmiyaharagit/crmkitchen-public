﻿/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	Name			: DefaultSearchService
	Description		: 通常検索
	Usage			: var XXXXXX:DefaultSearchService = new DefaultSearchService( datagridname, path );
	Attributes		: datagridname:DataGrid		（結果格納用データグリッドコンポーネント名）
					: path:MovieClip			（基準パス）
	ExtendsParam	: 
	Note			: none
	History			: 1) Coded by	nf)h.miyahara	2004/07/09
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
import mx.remoting.Service;
import mx.services.Log;
import mx.rpc.RelayResponder;
import mx.remoting.PendingCall;
import mx.remoting.debug.NetDebug;
import mx.remoting.debug.NetDebugConfig;
import mx.remoting.RecordSet;
import mx.rpc.ResultEvent;
import mx.rpc.FaultEvent;
import mx.controls.Alert;
import mx.controls.gridclasses.DataGridColumn
import mx.controls.DataGrid
import mx.controls.ComboBox
import mx.controls.RadioButton
import mx.transitions.easing.*;
import mx.transitions.Tween;
import mx.styles.CSSStyleDeclaration;
import app00.downloader;
import commonfunc;
import mx.data.binding.*;
import mx.data.components.*

class app00.DefaultSearchService extends MovieClip {

	var targetGrid:DataGrid				//結果表示ViewComponent
	var gpath:MovieClip			//基準ムービークリップパス
	var gdepth:Number					//Depth
	var resort_code:Number		//リゾートコード（0:共通）
	var sobject:Object			//CFCsobject
	var outputPath:String				//CSVOutputPath
	var sresult:DataSet			//検索結果退避用オブジェクト
	var dsarr:Array				//データソース事前指定用配列
	var cfunc:commonfunc

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: DefaultSearchService
		Description		: Constructor
		Usage			: DefaultSearchService( myGrid:DataGrid, myPath:MovieClip );
		Attributes		: myGrid:DataGrid	（結果格納用データグリッドコンポーネント名）
						: myPath:MovieClip	（基準ＭＣパス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function DefaultSearchService( myGrid:DataGrid, myPath:MovieClip, r_id:Number) {
		targetGrid = myGrid;
		gpath = myPath;
		gdepth = gpath.getNextHighestDepth();
		resort_code = r_id;
		cfunc = new commonfunc()

		var myDebug = mx.remoting.debug.NetDebug.initialize();
		sobject = new Service( "http://" + _global.hostAddress + "/flashservices/gateway/", null , _global.servicePath + ".app00.cfc.guestinfo" , null , null ); 
		
		//NetDebbuger設定
		var config:NetDebugConfig = sobject.connection.getDebugConfig();
		config.app_server.trace = true;
		config.client.trace = true;
		
		//ＭＣ初期化
		initializedData();

	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: initializedData
		Description		: 初期設定
		Usage			: initializedData();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function initializedData(){
		var owner = this

		//スタイル設定
		this.initGridStyle(targetGrid);

		//カラム設定
		var col:DataGridColumn = new DataGridColumn("depart_date");
 		col.headerText = "出発日";
 		col.width = 72;
		col.sortOnHeaderRelease = false;
		targetGrid.addColumn(col);

		col = new DataGridColumn("room_code");
 		col.headerText = "部屋番号";
 		col.width = 72;
		col.sortOnHeaderRelease = false;
		targetGrid.addColumn(col);

		col = new DataGridColumn("reserve_code");
 		col.headerText = "予約番号";
 		col.width = 72;
		col.sortOnHeaderRelease = true;
		targetGrid.addColumn(col);

		col = new DataGridColumn("customernumber");
 		col.headerText = "顧客番号";
 		col.width = 90;
		col.sortOnHeaderRelease = true;
		targetGrid.addColumn(col);

		col = new DataGridColumn("customer_nm");
 		col.headerText = "氏名";
 		col.width = 72;
		col.sortOnHeaderRelease = false;
		targetGrid.addColumn(col);

		col = new DataGridColumn("customer_kn");
 		col.headerText = "カナ";
 		col.width = 104;
		col.sortOnHeaderRelease = true;
		targetGrid.addColumn(col);

		col = new DataGridColumn("referencenumber");
 		col.headerText = "照会用番号";
 		col.width = 124;
		col.sortOnHeaderRelease = true;
		targetGrid.addColumn(col);

		col = new DataGridColumn("resort_nm");
 		col.headerText = "施設";
 		col.width = 68;
		col.sortOnHeaderRelease = true;
		targetGrid.addColumn(col);

		col = new DataGridColumn("birth_date");
 		col.headerText = "生年月日";
 		col.width = 72;
		col.sortOnHeaderRelease = false;
		targetGrid.addColumn(col)

		col = new DataGridColumn("tel");
 		col.headerText = "電話番号";
 		col.width = 90;
		col.sortOnHeaderRelease = false;
		targetGrid.addColumn(col);

		col = new DataGridColumn("email");
 		col.headerText = "E-mail";
 		col.width = 80;
		col.sortOnHeaderRelease = false;
		targetGrid.addColumn(col);
/*
		col = new DataGridColumn("address");
 		col.headerText = "住所";
 		col.width = 240;
		col.sortOnHeaderRelease = false;
		targetGrid.addColumn(col);
*/
		col = new DataGridColumn("detail");
 		col.headerText = "詳細";
 		col.width = 38;
 		col.cellRenderer = "SPCellRenderer";
		col.sortOnHeaderRelease = false;
		targetGrid.addColumn(col);

		/*
		col = new DataGridColumn("edit");
 		col.headerText = "編集";
 		col.cellRenderer = "EDCellRenderer";
		col.sortOnHeaderRelease = false;
		targetGrid.addColumn(col);
		*/
		//初期状態：非表示
		targetGrid._visible = false;
		targetGrid.selectable = true;
		targetGrid.editable = true;

		//調査手段設定
		gpath.NConditionBox.survey_cb.dropdown;

		//メニュー切替
		gpath.changeBtn.onRelease = function(){
			owner.SetMenuList(owner.gpath)
		}

		//検索ボタン設定
		gpath.searchBtn.onRelease = function(){
			//owner.gpath.SearchBar.gotoAndStop(2)

			//検索条件オブジェクト作成
			var conditionParam = new Object();
			conditionParam.custmoer_nm = owner.gpath.NConditionBox.findResults0_txt.text
			conditionParam.tel = owner.gpath.NConditionBox.findResults1_txt.text
			conditionParam.birth_date = owner.gpath.NConditionBox.findResults2_txt.text
			conditionParam.address = owner.gpath.NConditionBox.findResults3_txt.text
			conditionParam.EMail = owner.gpath.NConditionBox.findResults4_txt.text
			conditionParam.surveymethod = owner.gpath.NConditionBox.survey_cb.selectedItem.data

			conditionParam.reserve_code = owner.gpath.NConditionBox.findResults6_txt.text
			conditionParam.referencenumber = owner.gpath.NConditionBox.findResults8_txt.text
			conditionParam.customernumber = owner.gpath.NConditionBox.findResults9_txt.text

			conditionParam.resort_code = owner.resort_code

			//検索条件確認
			if(owner.CheckconditionParam(conditionParam)){
				owner.cfunc.SearchWait( owner.gpath, true );
				//CFCメソッド呼出
				var MaildtlPC:PendingCall = owner.sobject.getGuestList(conditionParam);
				MaildtlPC.responder = new RelayResponder( owner, "getGuestList_Result", "getGuestList_Fault" );

			} else {
				//アラート表示
				var ErrorMsg = "最低ひとつの検索条件を入力して下さい。"
				var HeaderMsg = "条件未設定                 "
				var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, gpath, null, null, null)
			}
			
		}
		//クリアボタン設定
		gpath.clearBtn.onRelease = function(){
			owner.gpath.NConditionBox.findResults0_txt.text = ""
			owner.gpath.NConditionBox.findResults1_txt.text = ""
			owner.gpath.NConditionBox.findResults2_txt.text = ""
			owner.gpath.NConditionBox.findResults3_txt.text = ""
			owner.gpath.NConditionBox.findResults4_txt.text = ""
			owner.gpath.NConditionBox.survey_cb.selectedIndex = 0
			owner.gpath.NConditionBox.findResults6_txt.text = ""
			owner.gpath.NConditionBox.findResults8_txt.text = ""
			owner.gpath.NConditionBox.findResults9_txt.text = ""
		}

		//ファイル出力ボタン設定
		gpath.csvBtn.onRelease = function(){
			if(!owner.targetGrid._visible){
				var ErrorMsg = "出力データがありません。先に検索を行ってください。"
				var HeaderMsg = "出力不可                                   "
				var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, gpath, null, null, null)
			} else {
				var AlertObj = mx.controls.Alert.show("一覧をファイルに出力します。よろしいですか？", "ファイル出力", Alert.YES | Alert.NO, owner.gpath, myClickHandler, null , Alert.OK);
			}
		}
		//CSV出力ボタンイベント
		var myClickHandler = new Object();
		myClickHandler = function(evt){
			if(evt.detail == Alert.YES){
				owner.gpath.opcondition._visible = false;
				owner.gpath.opcondition.enabled = false;
				owner.OutputCSV();
			}
		}

		//日付書式フォーマット（YYYYMMDD→YYYY/MM/DD）
		var DateListner:Object = new Object
		DateListner.change = function(eventObj){
			if( eventObj.target.text.indexOf("/") == -1 && eventObj.target.text.length == 8 ){
				eventObj.target.text = eventObj.target.text.substr(0,4) + "/" + eventObj.target.text.substr(4,2) + "/" + eventObj.target.text.substr(6,2)
			}
		}
		gpath.NConditionBox.findResults1_txt.addEventListener("change", DateListner);

		//各ボタン有効無効切替
		cfunc.ChangeEnabled( gpath, "csvBtn" , false )

		//検索条件スタイル設定
		for(var i=0; i<10; i++){
			gpath.NConditionBox[ "findResults" + i + "_txt" ].borderStyle = "none"
		}
		gpath.NConditionBox.findResults1_txt.restrict = "0-9¥¥-";
		gpath.NConditionBox.findResults2_txt.maxChars = 10
		gpath.NConditionBox.findResults2_txt.restrict = "0-9¥¥/";
		gpath.NConditionBox.findResults4_txt.restrict = "A-Za-z0-9\\-\\@\\_\\/\\#\\$\\%\\&\\.";

		gpath.NConditionBox.findResults6_txt.restrict = "0-9";
		gpath.NConditionBox.findResults8_txt.restrict = "A-Za-z0-9";
		//gpath.NConditionBox.findResults9_txt.restrict = "0-9";

		//タブ設定
		TabInitialize();

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getGuestList_Result
		Description		: 検索実行正常終了時イベント
		Usage			: getGuestList_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getGuestList_Result(ReEvt:ResultEvent):Void{
		if( ReEvt.result.mRecordsAvailable == 0){
			cfunc.SearchWait( gpath, false );
			var ErrorMsg = "該当レコードはありませんでした。\r検索条件を変更して再度検索を行ってみてください"
			var HeaderMsg = "件数無し                 "
			var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, gpath, null, null, null)

			targetGrid.dataProvider = RecSet;
			gpath.SearchBar.records_txt.text = ReEvt.result.mRecordsAvailable

		} else {
			if(ReEvt.result.items[0].cnt != undefined){
				var ErrorMsg = "取得件数が" + ReEvt.result.items[0].cnt + "件ありました。\r表示上の問題より、最初の100件のみ表示しています。\r件数を絞り込むよう、検索条件を変更して\r再度検索を行ってみてください"
				var HeaderMsg = "表示件数オーバー                 "
				var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, gpath, null, null, null)
			}

			gpath.records_txt.text = ReEvt.result.mRecordsAvailable
			//各ボタン有効無効切替
			targetGrid._visible = true;
			cfunc.ChangeEnabled( gpath, "csvBtn" , true )

			//初期設定
			var owner = this;

			//レコードセットオブジェクトに結果代入
			var RecSet:RecordSet = RecordSet(ReEvt.result);
			targetGrid.dataProvider = RecSet

			//取得件数表示
			gpath.SearchBar.gotoAndStop(1)
			gpath.SearchBar.records_txt.text = RecSet.length
			cfunc.SearchWait( gpath, false );

			//行毎表示色分け
			//targetGrid.setStyle("alternatingRowColors", Array(0xFFFFFF, 0xEAEAEA));
			//targetGrid.setVScrollPolicy("auto");

			//データ退避用DataSet生成
			sresult = new DataSet();
			
			//DataBindingの作成
			var src= new EndPoint();
			src.component = ReEvt;
			src.property = "dataProvider";

			var dest = new EndPoint();
			dest.component = sresult;
			dest.property = "dataProvider";

			//DataBindingの設定
			new Binding(src, dest);

			//レコードセットを退避用DataSetに格納
			sresult.dataProvider = targetGrid.dataProvider

			//NumberSort処理
			//カスタムソート設定
			var GridListener = new Object();
			GridListener.NMsortFlg = false;
			GridListener.IDsortFlg = false;
			GridListener.headerRelease = function(event) {
				if (event.columnIndex == 1){
					this.NMsortFlg = !this.NMsortFlg
					if(this.NMsortFlg){
						owner.dataProvider = RecSet.sortItemsBy(["customer_kn"], null, Array.DESCENDING );
					} else {
						owner.dataProvider = RecSet.sortItemsBy(["customer_kn"], null, null );
					}

				} else if(event.columnIndex == 1) {
					this.IDsortFlg = !this.IDsortFlg
					if(this.IDsortFlg){
						owner.dataProvider = RecSet.sortItemsBy(["room_code"], null, Array.NUMERIC | Array.DESCENDING );
					} else {
						owner.dataProvider = RecSet.sortItemsBy(["room_code"], null, Array.NUMERIC );
					}
				} else if(event.columnIndex == 2) {
					this.IDsortFlg = !this.IDsortFlg
					if(this.IDsortFlg){
						owner.dataProvider = RecSet.sortItemsBy(["reserve_code"], null, Array.NUMERIC | Array.DESCENDING );
					} else {
						owner.dataProvider = RecSet.sortItemsBy(["reserve_code"], null, Array.NUMERIC );
					}
				}
			}
			targetGrid.addEventListener("headerRelease", GridListener);

		}
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getGuestList_Fault
		Description		: 検索実行不正終了時イベント
		Usage			: getGuestList_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getGuestList_Fault(FltEvt:FaultEvent):Void{	//	trace("Categories Fault: " + FltEvt.fault.description);
		var ErrorMsg = "検索実行できませんでした。\rエラーメッセージ："+ FltEvt.fault.description
		var HeaderMsg = "検索失敗               "
		var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, gpath, null, null, null)
		cfunc.SearchWait( gpath, false );
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: OutputCSV
		Description		: ＣＳＶファイル出力
		Usage			: OutputCSV( Obj );
		Attributes		: Obj : （コンファームを呼出した呼出元クラスインスタンス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function OutputCSV(){
		dsarr = new Array();
		for(var i=0; i<targetGrid.length; i++){
			dsarr.push({
				 depart_date:targetGrid.getItemAt(i).depart_date
				,room_code:targetGrid.getItemAt(i).room_code
				,reserve_code:targetGrid.getItemAt(i).reserve_code
				,customernumber:targetGrid.getItemAt(i).customernumber
				,customer_nm:targetGrid.getItemAt(i).customer_nm
				,customer_kn:targetGrid.getItemAt(i).customer_kn
				,referencenumber:targetGrid.getItemAt(i).referencenumber
				,birth_date:targetGrid.getItemAt(i).birth_date
				,tel:targetGrid.getItemAt(i).tel
				,email:targetGrid.getItemAt(i).email
			})
		}
		//CFCメソッド呼出
		var MailtmpPC:PendingCall = sobject.DefaultResultOutputCSV( dsarr,resort_code );
		MailtmpPC.responder = new RelayResponder( this, "OutputCSV_Result", "OutputCSV_Fault" );
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: OutputCSV_Result
		Description		: ＣＳＶ出力正常終了時
		Usage			: OutputCSV_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function OutputCSV_Result(ReEvt:ResultEvent):Void{

		//outputPath = ReEvt.result.outputPath
		//cfunc.ShowConfirm( gpath , ReEvt.result.outputMsg + "に出力しました。ファイルを表示しますか？", this, this.getCSVFile );
		var FileObj:downloader = new downloader(gpath,ReEvt.result.filename,ReEvt.result.newname)
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: OutputCSV_Fault
		Description		: ＣＳＶ出力不正終了時
		Usage			: OutputCSV_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function OutputCSV_Fault(FltEvt:FaultEvent):Void{
		//処理を記述
		//var myConfirm:ConfirmWindow = new ConfirmWindow( gpath , "ＣＳＶファイルが出力できませんでした。\rエラーメッセージ："+ FltEvt.fault.description, this , null );
		var ErrorMsg = FltEvt.fault.description
		var HeaderMsg = "出力エラー                 "
		var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, gpath, null, null, null)

		trace("Categories Fault: " + FltEvt.fault.description);
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: CheckconditionParam
		Description		: 検索条件のチェック
		Usage			: CheckconditionParam(Obj);
		Attributes		: Obj : （検索条件オブジェクト）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/05
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function CheckconditionParam( Obj ){

		var checkmc = Obj
		var checkstr = ""
		for (var wkName in checkmc) {
			if( wkName != "resort_code" && wkName != "surveymethod"){
				checkstr = checkstr + checkmc[wkName]
			}
		}
		var Compelete = false
		var Nullchk = checkstr.length
		if( Nullchk > 0){
			Compelete = true
		} else {
			if(Obj.surveymethod != '-1'){
				Compelete = true
			}
		}
		return Compelete
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: SetTargetData
		Description		: 編集対象取得～編集画面表示
		Usage			: SetTargetData(index);
		Attributes		: index：	（編集対象ＩＮＤＥＸ）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/06
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function SetTargetData(index){
		
		//グローバル変数に編集対象ＩＮＤＥＸ・データプロバイダを退避
		_global.targetIndex = index;
		_global.targetData = targetGrid.dataProvider;

		var wk_dp = 3000
		gpath.createEmptyMovieClip("SpecSheetScreen", wk_dp);
		gdepth++;
		gpath.SpecSheetScreen.beginFill(0xCCCCCC);
		gpath.SpecSheetScreen.moveTo(0,0);
		gpath.SpecSheetScreen.lineTo(Stage.width, 0);
		gpath.SpecSheetScreen.lineTo(Stage.width, Stage.height);
		gpath.SpecSheetScreen.lineTo(0, Stage.height);
		gpath.SpecSheetScreen.lineTo(0, 0);
		gpath.SpecSheetScreen.endFill();
		//gpath.SpecSheetScreen.enabled = false
		gpath.SpecSheetScreen.onRollOver = function(){}

		//スペックシート表示画面読込み
		var LoadClip:Loadswf = new Loadswf( "app00/specSheet.swf", gpath.SpecSheetScreen );
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: EditTargetData
		Description		: 編集対象取得～編集画面表示
		Usage			: EditTargetData(index);
		Attributes		: index：	（編集対象ＩＮＤＥＸ）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/06
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function EditTargetData(index){
		
		//グローバル変数に編集対象ＩＮＤＥＸ・データプロバイダを退避
		_global.targetIndex = index;
		_global.targetData = targetGrid.dataProvider;

		gpath.createEmptyMovieClip("EditScreen", gdepth);
		gdepth++;
		gpath.EditScreen.beginFill(0xCCCCCC);
		gpath.EditScreen.moveTo(0,0);
		gpath.EditScreen.lineTo(Stage.width, 0);
		gpath.EditScreen.lineTo(Stage.width, Stage.height);
		gpath.EditScreen.lineTo(0, Stage.height);
		gpath.EditScreen.lineTo(0, 0);
		gpath.EditScreen.endFill();
		//gpath.EditScreen.enabled = false
		gpath.EditScreen.onRollOver = function(){}

		//スペックシート表示画面読込み
		var LoadClip:LoadImage = new LoadImage( "customerEdit.swf", gpath.EditScreen );
	}
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: TabInitialize
		Description		: タブ設定
		Usage			: TabInitialize();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/30
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function TabInitialize():Void{
		gpath.tabChildren = true
		gpath.NConditionBox.tabChildren = true
		gpath.NConditionBox.findResults0_txt.tabIndex = 2;
		gpath.NConditionBox.findResults1_txt.tabIndex = 4;
		gpath.NConditionBox.findResults2_txt.tabIndex = 6;
		gpath.NConditionBox.findResults3_txt.tabIndex = 8;
		gpath.NConditionBox.findResults4_txt.tabIndex = 10;
		gpath.NConditionBox.findResults5_txt.tabIndex = 12;
		gpath.NConditionBox.findResults6_txt.tabIndex = 14;
		gpath.searchBtn.tabIndex = 16;
		gpath.clearBtn.tabIndex = 18;
		gpath.csvBtn.tabIndex = 20;
		gpath.previewBtn.tabIndex = 22;
		gpath.printBtn.tabIndex = 24;
		gpath.changeBtn.tabIndex = 26;
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: Service_Fault
		Description		: マスタ情報取得不正終了時イベント
		Usage			: Service_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function Service_Fault(FltEvt:FaultEvent):Void{
		var ErrorMsg = FltEvt.fault.description
		var HeaderMsg = "システムエラー                 "
		var AlertObj = mx.controls.Alert.show(ErrorMsg, HeaderMsg, null, gpath, null, null, null)
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: initGridStyle
		Description		: 
		Usage			: initGridStyle(FltEvt:FaultEvent);
		Attributes		: 
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function initGridStyle(tg):Void {

		tg.setStyle("alternatingRowColors", Array(0xFFFFFF, 0xE7E4DC));
		tg.setStyle("hGridLineColor", 0x95946A);
		tg.setStyle("vGridLineColor", 0x95946A);
//		tg.setStyle("vGridLines", true);
//		tg.setStyle("hGridLines", true);
		tg.setStyle("borderStyle", "solid");
		tg.setVScrollPolicy("auto");
		tg.setHScrollPolicy("auto");
		// themeColor
		tg.setStyle("themeColor", 0xDEDEBE); 
		tg.setStyle("textSelectedColor", 0x333333);
		tg.setStyle("shadowCapColor", 0xFFFFFF);
		tg.setStyle("shadowColor", 0x336699);
		tg.setStyle("borderColor", 0xE9E1D6);
		tg.setStyle("themeColor", 0xE9E1D6);
		tg.setStyle("useRollOver", false);
		tg.setStyle("textSelectedColor", 0x333333);
		tg.setStyle("rollOverColor", 0xDEDEBE);
//		tg.resizableColumns = false;
		tg.setStyle("headerColor", 0xE7E4DC);
		tg.setStyle("fontSize", 12);

/*
		var headerStyles = new CSSStyleDeclaration();
		headerStyles.setStyle("fontSize", 12);
		headerStyles.setStyle("fontFamily", "Verdana");
		tg.setStyle("headerStyle", headerStyles);
		tg.hScrollPolicy = "auto";
*/
	}
}
