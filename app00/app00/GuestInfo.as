﻿/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	Name			: GuestInfo
	Description		: 宿泊者情報取得
	Usage			: var XXXXXX:GuestInfo = new GuestInfo( hostaddress, cfcclasspath, cfcomponentname, datagridname );
	Attributes		: hostaddress:String		（ホストアドレス名）
					: cfcclasspath:String		（ＣＦＣクラスパス）
					: cfcomponentname:String	（ＣＦコンポーネント名）
					: datagridname:DataGrid		（結果格納用データグリッドコンポーネント名）
	ExtendsParam	: hostaddress:String
					: classPath:String
					: className:String
					: ServiceObject:Service
	Method			: getCustomer_Result(ResultEvent);		Remoting接続実行成功時処理
					: getCustomer_Fault(FaultEvent);		Remoting接続実行失敗時処理
					: getCheckList();						結果Grid内チェックリスト取得
					: doPrint();							結果Grid内リスト印刷
	Note			: none
	History			: 1) Coded by	nf)h.miyahara	2004/07/09
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
import mx.remoting.Service;
import mx.services.Log;
import mx.rpc.RelayResponder;
import mx.remoting.PendingCall;
import mx.remoting.RecordSet;
import mx.remoting.debug.NetDebug;
import mx.remoting.debug.NetDebugConfig;
import mx.rpc.ResultEvent;
import mx.rpc.FaultEvent;
import mx.controls.gridclasses.DataGridColumn
import mx.controls.DataGrid

class GuestInfo extends FRConnections {
	
	var targetGrid:DataGrid			//結果表示ViewComponent
	private var initialized:Boolean;
	var targetRS:RecordSet			//結果表示ViewComponent
	

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Constructor
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function GuestInfo( myHost:String, myPath:String, myName:String, myGrid:DataGrid ) {
		hostAddress = myHost;
		classPath = myPath;
		className = myName;
		targetGrid = myGrid;
		initialized = false;


		//検索条件の設定
		var ConditionNum:Number = 4			//検索条件引数開始位置
		var ConditionStr:String = ""		//検索条件文字列（カンマ区切り）
		if(arguments.length > ConditionNum){
			for(var i=ConditionNum; i<arguments.length; i++ ){
				ConditionStr = ConditionStr + arguments[i] + ","
			}
		}
		ConditionStr = ConditionStr.substr(0,ConditionStr.length-1)

		var myDebug = mx.remoting.debug.NetDebug.initialize();
		//var myService:Service = new Service( "http://" + hostAddress + "/flashservices/gateway", new Log(Log.DEBUG) , classPath + "." + className , null , null ); 
		var myService:Service = new Service( "http://" + hostAddress + "/flashservices/gateway/", null , classPath + "." + className , null , null ); 
		ServiceObject = myService
		
		//NetDebbuger設定
		var config:NetDebugConfig = myService.connection.getDebugConfig();
		config.app_server.trace = true;
		config.client.trace = true;

		//CFCメソッド呼出
		var CstmrPC:PendingCall = myService.getGuestList(ConditionStr); 
		CstmrPC.responder = new RelayResponder( this, "getCustomer_Result", "getCustomer_Fault" );
		CstmrPC.responder = new RelayResponder( this, "ReTest_getCustomer_Result", "getCustomer_Fault" );

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		実験その２--->>	速度向上	成功！
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function ReTest_getCustomer_Result(ReEvt:ResultEvent):Void{
		//データソースの書式設定
		var dataFormatter:mx.utils.StringFormatter;
		dataFormatter = new mx.utils.StringFormatter( "YYYY/MM/DD", "M,D,Y,H,N,S", 
			mx.data.binding.DateBase.extractTokenDate, 
			mx.data.binding.DateBase.infuseTokenDate );
		
		for(var i=0; i<ReEvt.result.length; i++){
			ReEvt.result.items[i].startdate = dataFormatter.formatValue(ReEvt.result.items[i].startdate)
			ReEvt.result.items[i].enddate = dataFormatter.formatValue(ReEvt.result.items[i].enddate)
			ReEvt.result.items[i].customer_id = parseInt(ReEvt.result.items[i].customer_id)
			ReEvt.result.items[i].reserve_id = parseInt(ReEvt.result.items[i].reserve_id)
		}

		//NumberSort処理
		//ヘッダクリック時のリスナー
		var GridListener = new Object();
		GridListener.owner = targetGrid;
		GridListener.sortFlg = true;
		GridListener.headerRelease = function(event) {
			if (event.columnIndex == 1){
				this.owner.dataProvider = this.owner._parent.data_arr.sortOn( "customer_id", Array.DESCENDING | Array.NUMERIC );
			}
		}
		targetGrid.addEventListener("headerRelease", GridListener);

		//DataProvider設定
		targetGrid._parent.data_arr = new Array();
		for(var i=0; i<ReEvt.result.length; i++){
			targetGrid._parent.data_arr.push({
				CUSTOMER_ID:ReEvt.result.items[i].customer_id,
				CUSTOMER_NM:ReEvt.result.items[i].customer_nm,
				ENDDATE:ReEvt.result.items[i].enddate,
				PLAN_TYPE:ReEvt.result.items[i].plan_type,
				RESERVE_ID:ReEvt.result.items[i].reserve_id,
				ROOM_TYPE:ReEvt.result.items[i].room_type,
				STARTDATE:ReEvt.result.items[i].startdate
			})
		}
		targetGrid.dataProvider = targetGrid._parent.data_arr
		//trace( ReEvt.result.mRecordsAvailable + "件取得しました。"); 
		
		//１列目のソートを無効にする
		targetGrid.getColumnAt(1).sortOnHeaderRelease = false;

		//ページング処理コール
		setPageinfo();

	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		検索実行成功時処理
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getCustomer_Result(ReEvt:ResultEvent):Void{


		//DataGrid初期化
		if( !initialized ) {
			targetGrid.removeAllColumns()

			for(var i=0;i<ReEvt.result.mTitles.length;i++){
				//trace( ReEvt.result.mTitles[i] )
				var col:DataGridColumn = new DataGridColumn(ReEvt.result.mTitles[i]);
				targetGrid.addColumn( col );
				
				//customer_idはMCをあてる
				/*
				if(ReEvt.result.mTitles[i] == "customer_id"){
					targetGrid.getColumnAt(i).cellRenderer = "MCCellRenderer";
				}
				*/
			}

			//カスタムカラム追加設定
			var column = new DataGridColumn("Selected");
			column.headerText = "チェック";
			column.width = 40;
			column.cellRenderer = "CheckCellRenderer";
			targetGrid.addColumn(column);

/**/
			var column = new DataGridColumn("Edit");
			column.headerText = "編集";
			column.cellRenderer = "MCCellRenderer";
			targetGrid.addColumn(column);
			initialized = true;


		}


		//データ退避用RecordSet生成
		//var targetRS = ReEvt.result;
		targetRS = RecordSet( ReEvt.result );

		//データソースの書式設定
		var dataFormatter:mx.utils.StringFormatter;
		dataFormatter = new mx.utils.StringFormatter( "YYYY/MM/DD", "M,D,Y,H,N,S", 
			mx.data.binding.DateBase.extractTokenDate, 
			mx.data.binding.DateBase.infuseTokenDate );
		
		for(var i=0; i<ReEvt.result.length; i++){
			ReEvt.result.items[i].startdate = dataFormatter.formatValue(ReEvt.result.items[i].startdate)
			ReEvt.result.items[i].enddate = dataFormatter.formatValue(ReEvt.result.items[i].enddate)
			ReEvt.result.items[i].customer_id = parseInt(ReEvt.result.items[i].customer_id)
			ReEvt.result.items[i].reserve_id = parseInt(ReEvt.result.items[i].reserve_id)
		}

		//NumberSort処理
		//ヘッダクリック時のリスナー
		var GridListener = new Object();
		GridListener.owner = targetGrid;
		GridListener.sortFlg = true;
		GridListener.headerRelease = function(event) {
			if (event.columnIndex == 0){
				if(this.sortFlg){
					this.owner.dataProvider = ReEvt.result.items.sortOn( "reserve_id", Array.NUMERIC );
					this.sortFlg = false;
				} else {
					this.owner.dataProvider = ReEvt.result.items.sortOn( "reserve_id", Array.DESCENDING | Array.NUMERIC );
					this.sortFlg = true;
				}
			}
		}
		targetGrid.addEventListener("headerRelease", GridListener);

		//DataProvider設定
		//targetGrid.dataProvider = ReEvt.result
		targetGrid.dataProvider = targetRS
		
		//trace( ReEvt.result.mRecordsAvailable + "件取得しました。"); 
		
		//１列目のソートを無効にする
		targetGrid.getColumnAt(0).sortOnHeaderRelease = false;
		//targetGrid.getColumnAt(4).sortOnHeaderRelease = false;

		//ページング処理コール
		setPageinfo();

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		検索実行失敗時処理
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getCustomer_Fault(FltEvt:FaultEvent):Void{
		//処理を記述
		trace("Categories Fault: " + FltEvt.fault.description);
	}
	
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		グリッド内編集ボタンクリック処理
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function getCellData( id ){
		trace("getCellData  " + id );
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		グリッド内チェックリスト取得
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function getCheckList(){
		var retArray:Array = new Array();
		for(var i=0; i< targetGrid.length; i++){
			if(targetGrid.getItemAt(i).Selected){
				retArray.push(targetGrid.getItemAt(i).customer_id)
			}
		}
		//trace("retArray: " + retArray.length)
		return retArray
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		グリッド内リスト印刷
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function doPrint(){
		//targetPrintクラスインスタンス生成
		//var myPrint = new targetPrint(targetGrid);
		trace("dd")
		//印刷プレビューメソッドコール
		//myPrint.setPagesInfo("顧客リスト")
		
		//印刷プレビューメソッドコール
		//myPrint.previewGridPrint();
		
		
	}

	private function setPageinfo(){
		var rowsPerPage:Number = Math.floor((targetGrid.height - targetGrid.headerHeight)/ targetGrid.rowHeight);
		var pages:Number = Math.ceil(targetGrid.dataProvider.length/rowsPerPage);
		/*
		trace("総件数: " + targetGrid.dataProvider.length )
		trace("height: " + targetGrid.height )
		trace("headerHeight: " + targetGrid.headerHeight )
		trace("rowHeight: " + targetGrid.rowHeight )
		trace("1ページの表示件数: " + rowsPerPage )
		trace("総ページ数: " + pages )
		*/
	}
}