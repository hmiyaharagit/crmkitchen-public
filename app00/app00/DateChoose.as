﻿/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	Name			: DateChoose
	Description		: カレンダー表示による入力補助日付設定
	Usage			: var INSTANCENAME:DateChoose = new DateChoose( path , txt, disableDate );
	Attributes		: path:MovieClip	（AttachMovieClipTarget）
					: txt:MovieClip		（TextInsertTarget）
					: disableDate:String（disableDate）
	Method			: setCalender( setDate:Date )
					: initialized()
					: daysInMonth()
					: dateFormat()
	Note			: none
	History			: 1) Coded by	nf)h.miyahara	2004/07/09
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
class DateChoose {
	var today_date:Date;		//今日
	var fitst_date:Date;		//今日の存在する月の初日
	var disableDate:String;		//無効指定開始日（String）
	var disable_date:Date;		//無効指定開始日（Date）
	var viewPath:MovieClip;		//パス
	var stargetPath:MovieClip	//パス
	var etargetPath:MovieClip	//パス
	var DateChooseDepth:Number	//Depth
	var test:Array
	
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Constructor
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function DateChoose( path , txt , tgtdate ){

		DateChooseDepth = 100//path.getNextHighestDepth();
		//DateChooseDepth = path._parent._parent.getNextHighestDepth();
		viewPath = path;
		stargetPath = txt[0];
		etargetPath = txt[1];
		disableDate = tgtdate;
		var temp_year = disableDate.substr(0,4)
		var temp_month = disableDate.substr(5,2)
		var temp_day = disableDate.substr(8,2)

		var temp_thisyear = stargetPath.text.substr(0,4)
		var temp_thismonth = stargetPath.text.substr(5,2)
		var temp_thisday = stargetPath.text.substr(8,2)

		if( stargetPath.text.length < 1){
			today_date = new Date()
		} else {
			today_date = new Date( temp_thisyear, temp_thismonth-1, temp_thisday )
		}
		disable_date = new Date( temp_year, temp_month-1, temp_day-1 )
		//today_date = new Date(2004, 2, 12)
		fitst_date = new Date(today_date.getFullYear() , today_date.getMonth() , 1 )

		if(viewPath.Calender != undefined){
			sweep();
		} else {
			setCalender(fitst_date);
		}
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setCalender
		Description		: 初期設定
		Usage			: setCalender(setDate);
		Attributes		: setDate	(カレンダー表示開始日)
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function setCalender( setDate ){
		sweep();

		fitst_date = setDate
		//DaysInMonth
		var daysNum = daysInMonth( fitst_date.getMonth(), fitst_date.getFullYear() );  
		var start_x = stargetPath._x + stargetPath._width + 10;
		var start_y = stargetPath._y + 60;
		var margin_x = 2;
		var margin_y = 2;
		var count = 1;
		var removeDays = 0-fitst_date.getDay();
		
		//viewPath.createEmptyMovieClip("Calender", viewPath.getNextHighestDepth());
		viewPath.createEmptyMovieClip("Calender", DateChooseDepth);
		DateChooseDepth++;
		
		//月設定
		viewPath.Calender.calnderHeader.removeMovieClip();
		viewPath.Calender.attachMovie("calnderHeader","calnderHeader",DateChooseDepth,{_x:start_x+15,_y:start_y-60})
		DateChooseDepth++;
		
		var tmp_month = dateFormat( fitst_date.getFullYear(), fitst_date.getMonth()+1, 1 );
		viewPath.Calender.calnderHeader.month_txt.text = tmp_month.substring( 0, 7 )
		
		viewPath.Calender.calnderHeader.forwardBtn.owner = this
		viewPath.Calender.calnderHeader.forwardBtn.nowSettingDate = setDate
		viewPath.Calender.calnderHeader.forwardBtn.onRelease = function(){
			this.owner.initialized();
			var temp_date = new Date(this.nowSettingDate.getFullYear() , this.nowSettingDate.getMonth()+1 , 1 )
			this.owner.setCalender(temp_date);
		}

		viewPath.Calender.calnderHeader.backBtn.owner = this
		viewPath.Calender.calnderHeader.backBtn.nowSettingDate = setDate
		viewPath.Calender.calnderHeader.backBtn.onRelease = function(){
			this.owner.initialized();
			var temp_date = new Date(this.nowSettingDate.getFullYear() , this.nowSettingDate.getMonth()-1 , 1 )
			this.owner.setCalender(temp_date);
		}

		//曜日設定
		var arr_dayOfWeek = new Array("日","月","火","水","木","金","土")
		var dayOfWeek_x = start_x
		for(var i=0; i<7; i++){
			viewPath.Calender[ "dayOfWeek_" + i ].removeMovieClip();
			viewPath.Calender.attachMovie("days","dayOfWeek_"+i,DateChooseDepth,{_x:dayOfWeek_x,_y:start_y-30})
			DateChooseDepth++;
			dayOfWeek_x = dayOfWeek_x + viewPath.Calender[ "dayOfWeek_" + i ]._width + margin_x;

			viewPath.Calender[ "dayOfWeek_" + i ].date_txt.text = arr_dayOfWeek[i];
			viewPath.Calender[ "dayOfWeek_" + i ].enabled = false;
		}

		//日付設定
		for(var i=0+removeDays; i<daysNum; i++){
			
			viewPath.Calender.attachMovie("days","days_"+i,DateChooseDepth,{_x:start_x,_y:start_y})
			DateChooseDepth++;
			
			start_x = start_x + viewPath.Calender[ "days_" + i ]._width + margin_x;
			viewPath.Calender[ "days_" + i ].owner = this
			viewPath.Calender[ "days_" + i ].stargetPath = stargetPath
			viewPath.Calender[ "days_" + i ].date_txt.text = i+1
			viewPath.Calender[ "days_" + i ].date = dateFormat( fitst_date.getFullYear(), fitst_date.getMonth()+1, i+1 );
			
			viewPath.Calender[ "days_" + i ].onRelease = function(){
				this.stargetPath.text = this.date
				this.owner.etargetPath.text = this.date
				this.owner.disablePassedDays(this.date_txt.text)
			}
			//当日設定
			if( setDate.getFullYear() == today_date.getFullYear() && setDate.getMonth() == today_date.getMonth() && today_date.getDate() == i+1 ){
				viewPath.Calender[ "days_" + i ].gotoAndStop(5);
			}
			
			//不完全日消去
			if(i < 0){
				viewPath.Calender[ "days_" + i ].removeMovieClip();
			}

			//週送り
			if( count%7 == 0 && count != 0){
				//start_x = 10;
				start_x = stargetPath._x + stargetPath._width + 10;
				start_y = start_y + viewPath.Calender[ "days_" + i ]._height + margin_y ;
			}

			//選択不可処理
			if( disableDate != undefined && i < disable_date.getDate() && setDate.getMonth() == disable_date.getMonth() ){
				viewPath.Calender[ "days_" + i ].enabled = false
				viewPath.Calender[ "days_" + i ]._alpha = 20;
				viewPath.Calender[ "days_" + i ].gotoAndStop(viewPath.Calender[ "days_" + i ]._totalframes);
			}
			if( disableDate != undefined && setDate.getFullYear() <= disable_date.getFullYear() && setDate.getMonth() < disable_date.getMonth() ){
				viewPath.Calender[ "days_" + i ].enabled = false
				viewPath.Calender[ "days_" + i ]._alpha = 20;
				viewPath.Calender[ "days_" + i ].gotoAndStop(viewPath.Calender[ "days_" + i ]._totalframes);
			}
			if( disableDate != undefined && setDate.getFullYear() < disable_date.getFullYear() ){
				viewPath.Calender[ "days_" + i ].enabled = false
				viewPath.Calender[ "days_" + i ]._alpha = 20;
				viewPath.Calender[ "days_" + i ].gotoAndStop(viewPath.Calender[ "days_" + i ]._totalframes);
			}

			count++;
		}

		//カレンダー背景色設定
		var beginfill_x = stargetPath._x + stargetPath._width + 10 ;
		var beginfill_y = stargetPath._y;
		var space = 5;
/**/
		viewPath.Calender.beginFill(0xFFFFFF);
		viewPath.Calender.lineStyle(0, 0x336633, 100);
		viewPath.Calender.moveTo( beginfill_x - space, beginfill_y-space );
		viewPath.Calender.lineTo( beginfill_x + viewPath.Calender._width + space, beginfill_y - space );
		viewPath.Calender.lineTo( beginfill_x + viewPath.Calender._width - space, beginfill_y + viewPath.Calender._height );
		viewPath.Calender.lineTo( beginfill_x - space, beginfill_y + viewPath.Calender._height - space);
		viewPath.Calender.lineTo( beginfill_x - space, beginfill_y - space );
		viewPath.Calender.endFill();
	}
	
	
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: disablePassedDays
		Description		: 指定日より過去を無効化
		Usage			: disablePassedDays(date);
		Attributes		: date	(無効日)
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function disablePassedDays(date){
		sweep()
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: initialized
		Description		: カレンダー日付初期化
		Usage			: initialized();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function initialized(){
		for(var i=0; i<32; i++){
			viewPath.Calender[ "days_" + i ].removeMovieClip();
		}

	}
	
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: sweep
		Description		: カレンダー初期化
		Usage			: sweep();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	public function sweep(){
		viewPath.Calender.removeMovieClip();
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: daysInMonth
		Description		: 該当月の日数取得
		Usage			: daysInMonth(month, year);
		Attributes		: month
						: year
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function daysInMonth(month, year) {
		return (new Date(year, month+1, 0).getDate());
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: dateFormat
		Description		: YYYY/MM/DD形式への日付書式フォーマット
		Usage			: dateFormat(year , month , date);
		Attributes		: month
						: year
						: date
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function dateFormat(year , month , date) {
		if( month < 10 ){
			month = "0" + month
		}
		if( date < 10 ){
			date = "0" + date
		}
		var formatedDate = year + "/" + month + "/" + date
		return formatedDate;
	}

}
