﻿/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	Name			: Conditionsetting4Repeater
	Description		: CRM用検索画面
	Usage			: var XXXXXX:Conditionsetting4Repeater = new Conditionsetting4Repeater( path );
	Attributes		: path:MovieClip			（基準パス）
	Extends			: CommonFunction(Class)
	Method			: initializedData		初期設定
					: getCRMGuestList_Result	検索実行正常終了時イベント
					: getCRMGuestList_Fault	検索実行不正終了時イベント
	Note			: none
	History			: 1) Coded by	nf)h.miyahara	2004/07/09
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
import mx.remoting.Service;
import mx.services.Log;
import mx.rpc.RelayResponder;
import mx.remoting.PendingCall;
import mx.remoting.debug.NetDebug;
import mx.remoting.debug.NetDebugConfig;
import mx.remoting.RecordSet;
import mx.rpc.ResultEvent;
import mx.rpc.FaultEvent;
import mx.controls.ComboBox
import mx.data.binding.*;
import mx.data.components.DataSet
import mx.containers.ScrollPane
import mx.transitions.easing.*;
import mx.transitions.Tween;

class Conditionsetting4Repeater extends CommonFunction {

	var GroundPath:MovieClip			//基準ムービークリップパス
	var GPDepth:Number					//Depth
	var ServiceObject:Object			//CFCServiceObject
	var outputPath:String				//CSVOutputPath
	var SearchResult:DataSet			//検索結果退避用オブジェクト
	var PaneSource:MovieClip			//ScrollPane内MovieClip
	var DataSourceArr:Array				//データソース事前指定用配列

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: Conditionsetting4Repeater
		Description		: Constructor
		Usage			: Conditionsetting4Repeater( myPath:MovieClip );
		Attributes		: myGrid:DataGrid	（結果格納用データグリッドコンポーネント名）
						: myPath:MovieClip	（基準ＭＣパス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function Conditionsetting4Repeater( myPath:MovieClip  ) {
		GroundPath = myPath;
		GPDepth = GroundPath.getNextHighestDepth();
		DataSourceArr = new Array();

		var myDebug = mx.remoting.debug.NetDebug.initialize();
		ServiceObject = new Service( "http://" + _global.hostAddress + "/flashservices/gateway/", null , _global.servicePath + ".conditionsetting" , null , null ); 
		
		//NetDebbuger設定
		var config:NetDebugConfig = ServiceObject.connection.getDebugConfig();
		config.app_server.trace = true;
		config.client.trace = true;
		
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: initializedData
		Description		: 初期設定
		Usage			: initializedData();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function initializedData(){
		var Owner = this

		//日付入力初期設定
		var today_date:Date = new Date();
		var month = today_date.getMonth()+1;
		var day = today_date.getDate();
		if( month < 10 ){ month = "0" + month }
		if( day < 10 ){ day = "0" + day }
		var today:String = (today_date.getFullYear() + "/" + month + "/" + day);
		GroundPath.sStartDate.text = today
		GroundPath.sEndDate.text = today

		//日付入力補助設定
		GroundPath.sStartDate_mc.onRelease = function(){
			var targetTxt = new Array(Owner.GroundPath.sStartDate)
			var sStartDateChoose = new DateChoose( Owner.GroundPath, targetTxt);
		}
		GroundPath.sEndDate_mc.onRelease = function(){
			var targetTxt = new Array(Owner.GroundPath.sEndDate)
			var sEndDateChoose = new DateChoose( Owner.GroundPath, targetTxt, Owner.GroundPath.sStartDate.text );
		}

		//初期状態：結果エリア・オプション用ボード非表示
		GroundPath.ResultsPanel._visible = false;
		GroundPath.optionbord._visible = false;

		//設問情報取得CFCメソッド呼出
		//var QestinPC:PendingCall = ServiceObject.getEnquete();
		//QestinPC.responder = new RelayResponder( this, "setQuestionnaire_Result", "Service_Fault" );

		//2005/04/26 h.miyahara(NetFusion) ADD Start
		var AreaPC:PendingCall = ServiceObject.getArea();
		AreaPC.responder = new RelayResponder( this, "getArea_Result", "Service_Fault" );

		var TgtrPC:PendingCall = ServiceObject.getcontrolitems(6);
		TgtrPC.responder = new RelayResponder( this, "getTogether_Result", "Service_Fault" );
/*	
		var GndrPC:PendingCall = ServiceObject.getGender();
		GndrPC.responder = new RelayResponder( this, "getGender_Result", "Service_Fault" );

		var EraPC:PendingCall = ServiceObject.getEra();
		EraPC.responder = new RelayResponder( this, "getEra_Result", "Service_Fault" );
*/
		var PpsPC:PendingCall = ServiceObject.getcontrolitems(5);
		PpsPC.responder = new RelayResponder( this, "getPurpose_Result", "Service_Fault" );
		
		var ExpPC:PendingCall = ServiceObject.getcontrolitems(1);
		ExpPC.responder = new RelayResponder( this, "getRoomtype_Result", "Service_Fault" );

		var CogPC:PendingCall = ServiceObject.getcontrolitems(4);
		CogPC.responder = new RelayResponder( this, "getcognition_Result", "Service_Fault" );

		var RsvPC:PendingCall = ServiceObject.getcontrolitems(3);
		RsvPC.responder = new RelayResponder( this, "getreservation_Result", "Service_Fault" );

		GroundPath.area_cb.dropdown;
		GroundPath.roomtype_cb.dropdown;
//		GroundPath.gender_cb.dropdown;
//		GroundPath.era_cb.dropdown;
		GroundPath.together_cb.dropdown;
		GroundPath.nights_cb.dropdown;
		//オプション項目展開
		GroundPath.OptionBtn.used = false
		GroundPath.OptionBtn.onRelease = function(){
			var starty = 5;
			var endy = 132;
			var optionbord = Owner.GroundPath.optionbord
			var OptionBtn = this
			if(!this.used){
				if(this._parent.optionbord._visible){
					var returntween:Object = new Tween(this._parent.optionbord, "_y", Strong.easeOut, endy, starty, 1, true);
					returntween.onMotionFinished = function() {
						optionbord._visible = !optionbord._visible;
						OptionBtn.used = false;
					};
				} else {
					var returntween:Object = new Tween(this._parent.optionbord, "_y", Strong.easeOut, starty, endy, 1, true);
					Owner.GroundPath.optionbord._visible = !Owner.GroundPath.optionbord.optionbord._visible;
					returntween.onMotionFinished = function() {
						OptionBtn.used = false;
					};
				}
			}
			OptionBtn.used = true;
		}
		GroundPath.optionbord.purpose_cb.dropdown;
		GroundPath.optionbord.cognition_cb.dropdown;
		GroundPath.optionbord.reservation_cb.dropdown;
		//2005/04/26 h.miyahara(NetFusion) ADD END

	}
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: Service_Fault
		Description		: 設問情報取得不正終了時イベント
		Usage			: Service_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function Service_Fault(FltEvt:FaultEvent):Void{
		ShowConfirm( GroundPath , "設問情報の取得ができませんでした。\rエラーメッセージ："+ FltEvt.fault.description, this , null );
		trace("Service_Fault: " + FltEvt.fault.description);
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getArea_Result
		Description		: エリアマスタ取得正常終了時イベント
		Usage			: getArea_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2005/04/26
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getArea_Result(ReEvt:ResultEvent):Void{
		var Owner = this
		var EditorPanel = GroundPath;
		var ComboData = new Array();
		ComboData.push({data:-1,label:"指定無し"})
		for(var i=0; i<ReEvt.result.mRecordsAvailable; i++){
			ComboData.push({data:ReEvt.result.items[i].area_code,label:ReEvt.result.items[i].name})
		}
		EditorPanel.area_cb.dataProvider = ComboData;
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getTogether_Result
		Description		: 同行者マスタ取得正常終了時イベント
		Usage			: getTogether_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2005/04/26
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getTogether_Result(ReEvt:ResultEvent):Void{
		var Owner = this
		var EditorPanel = GroundPath;
		var ComboData = new Array();
		ComboData.push({data:"",label:"指定無し"})
		for(var i=0; i<ReEvt.result.mRecordsAvailable; i++){
			ComboData.push({data:ReEvt.result.items[i].control_id,label:ReEvt.result.items[i].control_nm})
		}
		EditorPanel.together_cb.dataProvider = ComboData;
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getGender_Result
		Description		: 性別マスタ取得正常終了時イベント
		Usage			: getGender_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2005/04/26
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getGender_Result(ReEvt:ResultEvent):Void{
		var Owner = this
		var EditorPanel = GroundPath;
		var ComboData = new Array();
		ComboData.push({data:-1,label:"指定無し"})
		for(var i=0; i<ReEvt.result.mRecordsAvailable; i++){
			ComboData.push({data:ReEvt.result.items[i].options_no,label:ReEvt.result.items[i].options_label})
		}
		EditorPanel.gender_cb.dataProvider = ComboData;
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getEra_Result
		Description		: 年代マスタ取得正常終了時イベント
		Usage			: getEra_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2005/04/26
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getEra_Result(ReEvt:ResultEvent):Void{
		var Owner = this
		var EditorPanel = GroundPath;
		var ComboData = new Array();
		ComboData.push({data:-1,label:"指定無し"})
		for(var i=0; i<ReEvt.result.mRecordsAvailable; i++){
			ComboData.push({data:ReEvt.result.items[i].options_no,label:ReEvt.result.items[i].options_label})
		}
		EditorPanel.era_cb.dataProvider = ComboData;
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getPurpose_Result
		Description		: 旅行目的マスタ取得正常終了時イベント
		Usage			: getPurpose_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2005/04/26
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getPurpose_Result(ReEvt:ResultEvent):Void{
		var Owner = this
		var EditorPanel = GroundPath.optionbord;
		var ComboData = new Array();
		ComboData.push({data:"",label:"指定無し"})
		for(var i=0; i<ReEvt.result.mRecordsAvailable; i++){
			ComboData.push({data:ReEvt.result.items[i].control_id,label:ReEvt.result.items[i].control_nm})
		}
		EditorPanel.purpose_cb.dataProvider = ComboData;
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getRoomtype_Result
		Description		: ルームタイプマスタ取得正常終了時イベント
		Usage			: getRoomtype_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2005/04/26
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getRoomtype_Result(ReEvt:ResultEvent):Void{
		var Owner = this
		var EditorPanel = GroundPath;
		var ComboData = new Array();
		ComboData.push({data:"",label:"指定無し"})
		for(var i=0; i<ReEvt.result.mRecordsAvailable; i++){
			ComboData.push({data:ReEvt.result.items[i].control_id,label:ReEvt.result.items[i].control_nm})
		}
		EditorPanel.roomtype_cb.dataProvider = ComboData;
	}
	
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getcognition_Result
		Description		: 認知経路マスタ取得正常終了時イベント
		Usage			: getcognition_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2005/04/26
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getcognition_Result(ReEvt:ResultEvent):Void{
		var Owner = this
		var EditorPanel = GroundPath.optionbord;
		var ComboData = new Array();
		ComboData.push({data:"",label:"指定無し"})
		for(var i=0; i<ReEvt.result.mRecordsAvailable; i++){
			ComboData.push({data:ReEvt.result.items[i].control_id,label:ReEvt.result.items[i].control_nm})
		}
		EditorPanel.cognition_cb.dataProvider = ComboData;
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getreservation_Result
		Description		: 予約経路マスタ取得正常終了時イベント
		Usage			: getreservation_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2005/04/26
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getreservation_Result(ReEvt:ResultEvent):Void{
		var Owner = this
		var EditorPanel = GroundPath.optionbord;
		var ComboData = new Array();
		ComboData.push({data:"",label:"指定無し"})
		for(var i=0; i<ReEvt.result.mRecordsAvailable; i++){
			ComboData.push({data:ReEvt.result.items[i].control_id,label:ReEvt.result.items[i].control_nm})
		}
		EditorPanel.reservation_cb.dataProvider = ComboData;
	}
}