﻿/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	Name			: conditionsetting
	Description		: CRM用検索画面
	Usage			: var XXXXXX:conditionsetting = new conditionsetting( path );
	Attributes		: path:MovieClip			（基準パス）
	Extends			: CommonFunction(Class)
	Method			: initializedData		初期設定
					: getCRMGuestList_Result	検索実行正常終了時イベント
					: getCRMGuestList_Fault	検索実行不正終了時イベント
	Note			: none
	History			: 1) Coded by	nf)h.miyahara	2004/07/09
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
import mx.remoting.Service;
import mx.services.Log;
import mx.rpc.RelayResponder;
import mx.remoting.PendingCall;
import mx.remoting.debug.NetDebug;
import mx.remoting.debug.NetDebugConfig;
import mx.remoting.RecordSet;
import mx.rpc.ResultEvent;
import mx.rpc.FaultEvent;
import mx.controls.ComboBox
import mx.data.binding.*;
import mx.data.components.DataSet
import mx.containers.ScrollPane
import mx.transitions.easing.*;
import mx.transitions.Tween;
import flash.filters.DropShadowFilter;
import mx.controls.Alert;

class app00.conditionsetting {

	var gpath:MovieClip			//基準ムービークリップパス
	var apath:MovieClip			//参照先ムービーパス
	var gdepth:Number			//Depth
	var sobject:Object			//CFCsobject
	var outputPath:String		//CSVOutputPath
	var sresult:DataSet			//検索結果退避用オブジェクト
	var psource:MovieClip		//ScrollPane内MovieClip
	var condtionitems:Array		//
	var overflowlength:Number
	var parents:Object
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: conditionsetting
		Description		: Constructor
		Usage			: conditionsetting( myPath:MovieClip );
		Attributes		: myGrid:DataGrid	（結果格納用データグリッドコンポーネント名）
						: myPath:MovieClip	（基準ＭＣパス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function conditionsetting( myPath:MovieClip,parntObj:Object,appath:MovieClip  ) {
		trace(myPath)
		gpath = myPath;
		//gpath = _root.conditionbox;
		apath = appath;
		gdepth = 777//gpath.getNextHighestDepth();
		parents = parntObj
		condtionitems = new Array();
		overflowlength = 94
		var myDebug = mx.remoting.debug.NetDebug.initialize();
		sobject = new Service( "http://" + _global.hostAddress + "/flashservices/gateway/", null , _global.servicePath + ".app00.cfc.conditionsetting" , null , null ); 
		
		//NetDebbuger設定
		var config:NetDebugConfig = sobject.connection.getDebugConfig();
		config.app_server.trace = true;
		config.client.trace = true;
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: initializedData
		Description		: 初期設定
		Usage			: initializedData();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	public function initializedData(){
		if(gpath.setting!=true){
		//	--権限による非表示
		gpath.condbtn12.enabled = gpath.condbtn12._visible = false
		if(_global.resort_code==0 || _global.resort_code.length > 1){
			gpath.condbtn12.enabled = gpath.condbtn12._visible = true
			var cblistner = new Object();
			cblistner.change = function(eventObj){
				owner.parents.resort_code = eventObj.target.value
				_global.resort_code = owner.parents.resort_code
				trace(owner.parents.resort_code + "に変更されたよ")
				var AreaPC:PendingCall = owner.sobject.getmaster(owner.parents.resort_code);
				AreaPC.responder = new RelayResponder( owner, "resetmaster_Result", "Service_Fault" );
			}
			gpath.condbtn12.resort_code_cb.addEventListener( "change", cblistner );
		}

		var owner = this
		//項目パネル表示
		gpath.cond_items._visible = gpath.cond_items.enabled = false
		var conditionbox =  gpath
		//	各絞込条件毎設定
		for (var itemobj in conditionbox) {
			gpath[itemobj].viewbtn.condid = itemobj.substr(7,2)
			gpath[itemobj].viewbtn.textobj = gpath[itemobj].selectedtext
			//	各絞込条件 選択ボタンイベント
			gpath[itemobj].viewbtn.onRelease = function(){
				var targetmc =  owner.gpath.cond_items
				//	各絞込条件項目パネル設定
				for (var resetobj in targetmc) {
					if (typeof (targetmc[resetobj]) == "movieclip" && resetobj.substr(0,8) == "conditem") {
						targetmc[resetobj]._visible = false
						targetmc[resetobj].enabled = false
					}
					if(this.condid == resetobj.substr(8,2)){
						targetmc[resetobj]._visible = true
						targetmc[resetobj].enabled = true
					}

					//	各絞込条件項目パネル 閉じるボタンイベント
					targetmc[resetobj].closebtn.onRelease = function(){
						this._parent._visible = this._parent.enabled = false
						owner.setselectedlabel(this._parent._name);
					}
				}
				//	選択項目ラベル表示設定
				if(!targetmc[resetobj]._visible){
					owner.setselectedlabel();
				}
				targetmc._visible = targetmc.enabled = true
			}
		}

		//日付入力初期設定
		var today_date:Date = new Date();

		//初期状態：結果エリア非表示
		//gpath.ResultsPanel._visible = false;

		// 2011/06/07 通常・団体デフォルト値変更
		gpath.group_cb.selectedIndex = 1;

		//2005/04/26 h.miyahara(NetFusion) ADD Start
		var AreaPC:PendingCall = sobject.getmaster(_global.resort_code);
		AreaPC.responder = new RelayResponder( this, "getmaster_Result", "Service_Fault" );
		
		gpath.setting=true
		}
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: slctddateadd
		Description		: 条件入力状態解除
		Usage			: slctddateadd();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/30
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	public function slctddateadd(dobj){
		//内容チェック
		var wk_bl = true
		if(!apath.dselection.selected && parents.slctddate.length>1){
			parents.slctddate = new Array()
		}
		for(var i=0;i<parents.slctddate.length;i++){
			if(parents.slctddate[i].toString() == dobj.toString()){
				wk_bl = false
				parents.slctddate.splice(i,1)
			}
		}
		if(wk_bl){
			parents.slctddate.push(dobj)
		}
		dcondtionlabel(apath.dselection.selected)
		return wk_bl
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: slctddatedel
		Description		: 条件入力状態解除
		Usage			: slctddatedel();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/30
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	public function slctddatedel(){
		parents.slctddate = new Array()
		apath.dselected_txt.text = apath.dmorebtn.labeltxt = gpath.dselected_txt.text = ""
		apath.dselection.selected = false
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: slctddateadd
		Description		: 条件入力状態解除
		Usage			: slctddateadd();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/30
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	public function dcondtionlabel(bl){
		apath.dselected_txt.text = " "
//trace("parents.slctddate.length;" + parents.slctddate.length)
		if(bl){	//自由選択
			var cnt=0
			for(var i=0;i<parents.slctddate.length;i++){
//				var sdtxt = (parents.slctddate[i].getMonth()+1) + "/" + (parents.slctddate[i].getDate())
				var sdtxt = (parents.slctddate[i].getFullYear().toString().substr(2,2)) + "/" + num2str(parents.slctddate[i].getMonth()+1) + "/" + num2str(parents.slctddate[i].getDate())
				var rmark = ""
				apath.dselected_txt.text =  apath.dselected_txt.text + sdtxt + ","
				cnt++;
			}
			apath.dselected_txt.text = apath.dmorebtn.labeltxt = apath.dselected_txt.text.substr(0,apath.dselected_txt.text.length-1)
			//	オーバーフロー時対応
			if(apath.dselected_txt.text.length>120){
				apath.dmorebtn.enabled = true
				apath.dselected_txt.text =  apath.dselected_txt.text.substr(0,120) + "･･･"
				apath.dmorebtn._alpha = 100
			} else {
				apath.dmorebtn._alpha = 60
				apath.dmorebtn.enabled = false
			}
		}else{	//期間選択
			if(parents.slctddate.length==1){
//				var sdtxt0 = (parents.slctddate[0].getMonth()+1) + "/" + (parents.slctddate[0].getDate())
				var sdtxt0 = (parents.slctddate[0].getFullYear().toString().substr(2,2)) + "/" + num2str(parents.slctddate[0].getMonth()+1) + "/" + num2str(parents.slctddate[0].getDate())
				apath.dselected_txt.text =  sdtxt0 + "～" +  sdtxt0
			}else if(parents.slctddate.length==2){
//				var sdtxt0 = (parents.slctddate[0].getMonth()+1) + "/" + (parents.slctddate[0].getDate())
//				var sdtxt1 = (parents.slctddate[1].getMonth()+1) + "/" + (parents.slctddate[1].getDate())
				var sdtxt0 = (parents.slctddate[0].getFullYear().toString().substr(2,2)) + "/" + num2str(parents.slctddate[0].getMonth()+1) + "/" + num2str(parents.slctddate[0].getDate())
				var sdtxt1 = (parents.slctddate[1].getFullYear().toString().substr(2,2)) + "/" + num2str(parents.slctddate[1].getMonth()+1) + "/" + num2str(parents.slctddate[1].getDate())
				apath.dselected_txt.text =  sdtxt0 + "～" +  sdtxt1
			}
		}
		gpath.dselected_txt.text = apath.dselected_txt.text 
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: num2str
		Description		: 日付補正
		Usage			: 
		Attributes		: none
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
    private function num2str(num:Number):String {
        if (num<10) return "0"+num;
        return ""+num;
    }

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setselectedlabel
		Description		: 選択項目設定
		Usage			: 
		Attributes		: none
		Note			: none
		History			: 1) Coded by	2007/09/03 16:34
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	public function setselectedlabel(cl){
		var ctxt = ""
		var selectedanswer = ""
		for (var itemobj in gpath) {
			if (typeof (gpath[itemobj]) == "movieclip" && itemobj.substr(0,7) == "condbtn") {
				var targetid = itemobj.substr(7,2)
				var ansobj = gpath.cond_items["conditem" + targetid]
				var checkbox_sp = gpath.cond_items["conditem" + targetid].checkbox_sp.content
				if(checkbox_sp!=undefined){	//選択数が多いので
					var ansobj = checkbox_sp
				}
				var txtobj = gpath["condbtn" + targetid]
				var anstxt = txtobj.selectedtext.text = ""
				for (var anscb in ansobj) {
					if(ansobj[anscb].selected){
						anstxt = ansobj[anscb].label + "," + anstxt
						txtobj.selectedtext.text = anstxt.substr(0,anstxt.length-1)
						selectedanswer = ansobj[anscb].answer + "," + selectedanswer
					}
				}
				txtobj.selectedmc._visible = false
				if(txtobj.selectedtext.text.length != 0){
					ctxt = txtobj.selectedtext.text + "/" + ctxt
					txtobj.selectedmc._visible = true
				}
			}
		}
		_global.currentscreen.gpath.cselected_txt.text = _global.currentscreen.gpath.cmorebtn.labeltxt = ctxt.substr(0,ctxt.length-1)
//		gpath._parent.cselected_txt.text = gpath._parent.cmorebtn.labeltxt = ctxt.substr(0,ctxt.length-1)
//		var dstxt = gpath._parent.cselected_txt
		var dstxt = _global.currentscreen.gpath.cselected_txt

		//	マスタ読み直し処理
		if(cl=="conditem12"){
			selectedanswer = selectedanswer.substr(0,selectedanswer.length-1)
			if(selectedanswer.length<1){
				selectedanswer = 0
			}
			var AreaPC:PendingCall = sobject.getmaster(selectedanswer);
			AreaPC.responder = new RelayResponder( this, "resetmaster_Result", "Service_Fault" );
		}



		//	オーバーフロー時対応
		if(dstxt.text.length>overflowlength){
			_global.currentscreen.gpath.cmorebtn.enabled = true
			dstxt.text =  dstxt.text.substr(0,overflowlength) + "･･･"
			_global.currentscreen.gpath.cmorebtn._alpha = 100
		} else {
			_global.currentscreen.gpath.cmorebtn._alpha = 60
			_global.currentscreen.gpath.cmorebtn.dmorebtn.enabled = false

		}

		

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: Service_Fault
		Description		: 設問情報取得不正終了時イベント
		Usage			: Service_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	public function Service_Fault(FltEvt:FaultEvent):Void{
		/**/
		var ContentsMsg = "設問情報の取得ができませんでした。\rエラーメッセージ："+ FltEvt.fault.description
		var HeaderMsg = "設問情報取得エラー"
		var AlertObj = mx.controls.Alert.show(ContentsMsg, HeaderMsg, null, gpath, null, null, null)
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getArea_Result
		Description		: エリアマスタ取得正常終了時イベント
		Usage			: getArea_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2005/04/26
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	public function getmaster_Result(ReEvt:ResultEvent):Void{
		var owner = this
		//マスタ設定
		conditemsetting(ReEvt.result.m_generation,"00")		// 年齢
		conditemsetting(ReEvt.result.m_gender,"01")			// 性別
		conditemsetting(ReEvt.result.m_nights,"02")			// 泊数
		conditemsetting(ReEvt.result.m_job,"03")			// 職業
		conditemsetting(ReEvt.result.m_together,"04")		// 同行者
		conditemsetting(ReEvt.result.m_area,"05")			// エリア

		conditemsetting(ReEvt.result.m_roomtype,"06")		// ルームタイプ
		conditemsetting(ReEvt.result.m_purpose,"07")		// 旅行目的
		conditemsetting(ReEvt.result.m_trip,"08")			// 旅行回数
		conditemsetting(ReEvt.result.m_reservation,"09")	// 予約経路
		conditemsetting(ReEvt.result.m_cognition,"10")		// 認知経路
		conditemsetting(ReEvt.result.m_roomnumber,"11")		// 部屋番号
		conditemsetting(ReEvt.result.m_resort,"12")			// 旅館
		conditemsetting(ReEvt.result.m_residence,"13")		// INB居住地


	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getArea_Result
		Description		: エリアマスタ取得正常終了時イベント
		Usage			: getArea_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2005/04/26
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	public function resetmaster_Result(ReEvt:ResultEvent):Void{
		var owner = this
		//マスタ設定
		conditemsetting(ReEvt.result.m_generation,"00")		// 年齢
		conditemsetting(ReEvt.result.m_gender,"01")			// 性別
		conditemsetting(ReEvt.result.m_nights,"02")			// 泊数
		conditemsetting(ReEvt.result.m_job,"03")			// 職業
		conditemsetting(ReEvt.result.m_together,"04")		// 同行者
		conditemsetting(ReEvt.result.m_area,"05")			// エリア

		conditemsetting(ReEvt.result.m_roomtype,"06")		// ルームタイプ
		conditemsetting(ReEvt.result.m_purpose,"07")		// 旅行目的
		conditemsetting(ReEvt.result.m_trip,"08")			// 旅行回数
		conditemsetting(ReEvt.result.m_reservation,"09")	// 予約経路
		conditemsetting(ReEvt.result.m_cognition,"10")		// 認知経路
		conditemsetting(ReEvt.result.m_roomnumber,"11")		// 部屋番号
		conditemsetting(ReEvt.result.m_residence,"13")		// INB居住地
			
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: conditemsetting
		Description		: 
		Usage			: conditemsetting
		Attributes		: 
		Note			: none
		History			: 1) Coded by16:58 2006/12/05
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	public function conditemsetting(ds,obj){
		var owner = this;
		var dsource = ds
		var intxt = obj
		var EditorPanel = gpath
		//	--設問番号
		EditorPanel["condbtn" + intxt].q_nm.text = dsource.items[0].q_nm
		if(dsource.items[0].q_nm==undefined){
			EditorPanel["condbtn" + intxt].q_nm.text = "TAP"
		}

		var chksp = EditorPanel.cond_items["conditem" + intxt].checkbox_sp
		chksp.contentPath = "condsource"
		psource = chksp.content
		var starty = 0
		for(var i=0; i<dsource.mRecordsAvailable; i++){
			var chkmc = EditorPanel.cond_items["conditem" + intxt]["ans_" + dsource.items[i].fla_mcname]
			if(chksp!=undefined){	//チェックボックスが多いので、、スクロールペイン利用
				chkmc = psource[newnm]
				var newnm = "ans_" + dsource.items[i].fla_mcname
				var wk_length = newnm.split("/").length
				if(wk_length==1){wk_length = wk_length +1 }
				var startx = ((wk_length-2) * 20)
				psource.createClassObject(mx.controls.CheckBox, newnm, gdepth++, {label:dsource.items[i].items_nm,_x:startx, _y:starty});
				starty = starty + psource[newnm].height;
				psource[newnm].setSize(chksp.width , psource[newnm].height)
				psource[newnm].answer = dsource.items[i].items_id

				//	--クリック時イベント
				var form_obj:Object = new Object();
				form_obj.click = function(event_obj:Object) {
					var wk_source = event_obj.target._parent
					for (var wkcb in wk_source) {
						var index = wkcb.indexOf(event_obj.target._name);
						if(index != -1 && intxt != "12"){
//							wk_source[wkcb].selected = event_obj.target.selected
							if(wk_source[wkcb]._name!=event_obj.target._name){
								wk_source[wkcb].enabled = !event_obj.target.selected
							}
						}
					}
				};
				psource[newnm].addEventListener("click", form_obj);

			} else {
				chkmc.answer = dsource.items[i].items_id
				//trace(chkmc.answer +  " : " + chkmc._name)
			}
			//chkmc.label = dsource.items[i].items_id + ":" + dsource.items[i].items_nm
		}
		//	--AllCheckイベント
		var ac_obj:Object = new Object();
		ac_obj.click = function(event_obj:Object) {
			var wk_source = event_obj.target._parent["checkbox_sp"].content
			if (wk_source == undefined){
				var checkboxsource = event_obj.target._parent
			} else {
				var checkboxsource = wk_source
			}
			for (var wkcb in checkboxsource) {
				if(wkcb.substr(0,4) == "ans_"){
					checkboxsource[wkcb].selected =  event_obj.target.selected
				}
			}

		};
		EditorPanel.cond_items["conditem" + intxt]["allcheck"].addEventListener("click", ac_obj);

		//	--例外処理（エリア）
		if(intxt=="05"){
			//	--首都圏
			var wk_ansarray = "2,3,4,5"
			EditorPanel.cond_items["conditem" + intxt].ans_1.answer = wk_ansarray
			//	--長野県
			var wk_ansarray = "6,7,8,9,10"
			EditorPanel.cond_items["conditem" + intxt].ans_6.answer = wk_ansarray
		}
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: viewmorechips
		Description		: 初期設定
		Usage			: viewmorechips();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	2007/09/03 18:49
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	public function viewmorechips(mc,pmc){
		var _atween:Object = new Tween(mc, "_alpha", Back.easeOut, 0, 100, 1, true);

		mc.labeltxt.autoSize = "left"
		var txt = pmc.labeltxt
		mc._y = pmc._y
		mc._x = 100

		var cnt=0
		var wk_txt = ""
		for(var i=0;i<txt.length;i++){
			if(cnt>overflowlength){
				wk_txt =  wk_txt + "\r"
				cnt=0;
			}
			wk_txt =  wk_txt + txt.substr(i,1)
			cnt++;
		}
		mc.labeltxt.text = wk_txt
		mc.morebody._width = mc.labeltxt._width + 20
		mc.morebody._height = mc.labeltxt._height + 20
		mc.closebtn.onRelease = function(){
			this._parent._x = 1000
		}
		mc.closebtn._x = mc.morebody._width - 11.2
		
		var distance:Number = 4;
		var angleInDegrees:Number = 45;
		var color:Number = 0x999999;
		var alpha:Number = .8;
		var blurX:Number = 4;
		var blurY:Number = 4;
		var strength:Number = 1;
		var quality:Number = 3;
		var inner:Boolean = false;
		var knockout:Boolean = false;
		var hideObject:Boolean = false;
		var filter:DropShadowFilter = new DropShadowFilter(
			distance, 
			angleInDegrees, 
			color, 
			alpha, 
			blurX, 
			blurY, 
			strength, 
			quality, 
			inner, 
			knockout, 
			hideObject);
		var filterArray:Array = new Array();
		filterArray.push(filter);
		mc.filters = filterArray;

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: qsettinginit
		Description		: 
		Usage			: 
		Attributes		: 
		Note			: none
		History			: 1) Coded by	2007/09/05 15:42
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	public function waiting(path,flg):Void{
		if(flg){
			path.attachMovie("waitingcover","waitingcover",path.getNextHighestDepth())
		} else {
			path.waitingcover.removeMovieClip();
			path.waitingcover._visible = path.waitingcover.enabled = false;
		}
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: qsettinginit
		Description		: 
		Usage			: 
		Attributes		: 
		Note			: none
		History			: 1) Coded by	2007/09/05 15:42
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	public function paramsetting(ansobj):Array{
		var retarray = new Array();
		for (var anscb in ansobj) {
			if(ansobj[anscb].selected){
//trace(ansobj[anscb]._name + ":" + ansobj[anscb].answer)
				var wk_ans = ansobj[anscb].answer.toString().split(",")
				for( var i=0; i<wk_ans.length; i++ ){
					retarray.push(wk_ans[i])
				}
			}
		}
		return retarray
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: qsettinginit
		Description		: 
		Usage			: 
		Attributes		: 
		Note			: none
		History			: 1) Coded by	2007/09/05 15:42
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	public function resortcodesetting(ansobj,rcd):Array{
		var retarray = new Array();
		for (var anscb in ansobj) {
			trace(ansobj[anscb])
			if(ansobj[anscb].selected){
				var wk_ans = ansobj[anscb].answer.toString().split(",")
				trace(wk_ans)
				for( var i=0; i<wk_ans.length; i++ ){
					if( rcd.indexOf(wk_ans[i]) != -1){
						retarray.push(wk_ans[i])
						trace(rcd.indexOf(wk_ans[i]))
					} else {
						trace(rcd.indexOf(wk_ans[i]))
					}
				}
			} 
		}
		if(retarray.length==0){
			var wk_ans = rcd.toString().split(",")
			for( var i=0; i<wk_ans.length; i++ ){
				retarray.push(wk_ans[i])
			}
		}
		
			for( var i=0; i<retarray.length; i++ ){
			}
		return retarray
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: qsettinginit
		Description		: 
		Usage			: 
		Attributes		: 
		Note			: none
		History			: 1) Coded by	2007/09/05 15:42
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	public function conditionreset():Void{
		for (var itemobj in gpath) {
			if (typeof (gpath[itemobj]) == "movieclip" && itemobj.substr(0,7) == "condbtn") {
				var targetid = itemobj.substr(7,2)
				var ansobj = gpath.cond_items["conditem" + targetid]
				var chksp = gpath.cond_items["conditem" + targetid].checkbox_sp.content
				if(chksp!=undefined){	//チェックボックスが多いので、、スクロールペイン利用
					ansobj = gpath.cond_items["conditem" + targetid].checkbox_sp.content
				}
				gpath["condbtn" + targetid].selectedmc._visible = false
				var txtobj = gpath["condbtn" + targetid]
				var anstxt = txtobj.selectedtext.text = ""
				for (var anscb in ansobj) {
					ansobj[anscb].selected = false
					ansobj[anscb].enabled = true
					txtobj.selectedtext.text = ""
				}
			}
		}
		gpath.cross_cb.selectedIndex = 0
		gpath.resortselect.resort_code_cb.selectedIndex = 0
		//gpath._parent.cselected_txt.text = gpath._parent.cmorebtn.labeltxt= gpath._parent.qselected_txt.text = gpath._parent.SearchBar.records_txt.text = ""
		//gpath._parent.qselectionview.q_id = ""
		_global.currentscreen.gpath.cselected_txt.text = _global.currentscreen.gpath.cmorebtn.labeltxt= _global.currentscreen.gpath.SearchBar.records_txt.text = ""
		//_global.currentscreen.gpath.qselectionview.q_id = ""
		_global.currentscreen.gpath.cmorebtn._alpha = 60
		_global.currentscreen.gpath.cmorebtn.dmorebtn.enabled = false
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: qselected
		Description		: 
		Usage			: 
		Attributes		: 
		Note			: none
		History			: 1) Coded by	2007/09/19 9:51
	private function selctedhighlite(mc):Void{
		var targetmc = gpath._parent.qselection
		for (var resetobj in targetmc) {
			targetmc[resetobj].selectedmc._visible = false
			if (typeof (targetmc[resetobj]) == "movieclip" && resetobj == mc._name) {
				targetmc[resetobj].selectedmc._visible = true
			}
		}
	}
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/

}