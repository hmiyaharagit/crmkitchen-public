﻿/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	Name			: RepeaterSearchService
	Description		: CRM用検索画面
	Usage			: var XXXXXX:RepeaterSearchService = new RepeaterSearchService( path );
	Attributes		: path:MovieClip			（基準パス）
	Extends			: CommonFunction(Class)
	Method			: initializedData		初期設定
					: getCRMGuestList_Result	検索実行正常終了時イベント
					: getCRMGuestList_Fault	検索実行不正終了時イベント
	Note			: none
	History			: 1) Coded by	nf)h.miyahara	2004/07/09
					: 2) Updateed by	nf)h.miyahara	2005/04/06	ValueInformation登録機能追加
					: 3) Updateed by	nf)h.miyahara	2005/04/06	印刷表示項目追加変更
					: 4) Updateed by	nf)h.miyahara	2005/04/06	結果表示項目追加変更
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
import mx.remoting.Service;
import mx.services.Log;
import mx.rpc.RelayResponder;
import mx.remoting.PendingCall;
import mx.remoting.debug.NetDebug;
import mx.remoting.debug.NetDebugConfig;
import mx.remoting.RecordSet;
import mx.rpc.ResultEvent;
import mx.rpc.FaultEvent;
import mx.controls.ComboBox
import mx.data.binding.*;
import mx.data.components.DataSet
import mx.controls.TextArea
import mx.containers.ScrollPane
import ValueInformation;

class RepeaterSearchService extends CommonFunction {

	var GroundPath:MovieClip			//基準ムービークリップパス
	var GPDepth:Number					//Depth
	var ServiceObject:Object			//CFCServiceObject
	var outputPath:String				//CSVOutputPath
	var SearchResult:DataSet			//検索結果退避用オブジェクト
	var PaneSource:MovieClip			//ScrollPane内MovieClip

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: RepeaterSearchService
		Description		: Constructor
		Usage			: RepeaterSearchService( myPath:MovieClip );
		Attributes		: myPath:MovieClip	（基準ＭＣパス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function RepeaterSearchService( myPath:MovieClip  ) {
/*
テストデータ

_global.Depth = 1;						//Depth
_global.hostAddress = "192.168.1.101";	//	ホストアドレス
_global.servicePath = "crmkitchen.cfc";	//	CFCClassPath
*/

		GroundPath = myPath;
		GPDepth = GroundPath.getNextHighestDepth();

		var myDebug = mx.remoting.debug.NetDebug.initialize();
		ServiceObject = new Service( "http://" + _global.hostAddress + "/flashservices/gateway/", null , _global.servicePath + ".guestinfo" , null , null ); 
		
		//NetDebbuger設定
		var config:NetDebugConfig = ServiceObject.connection.getDebugConfig();
		config.app_server.trace = true;
		config.client.trace = true;
		
		//ＭＣ初期化
		initializedData();

	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: initializedData
		Description		: 初期設定
		Usage			: initializedData();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
						: 2) Updateed by	nf)h.miyahara	2005/04/06	プレビュー時にVI再取得
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function initializedData(){
		var Owner = this

		//日付入力初期設定
		var today_date:Date = new Date();
		var tomorrow_date:Date = new Date(today_date.getFullYear(),today_date.getMonth(),today_date.getDate()+1);

		var month = tomorrow_date.getMonth()+1;
		var day = tomorrow_date.getDate();


		if( month < 10 ){ month = "0" + month }
		if( day < 10 ){ day = "0" + day }
		var today:String = (tomorrow_date.getFullYear() + "/" + month + "/" + day);
		GroundPath.CConditionBox.aStartDate.text = today
		GroundPath.CConditionBox.aEndDate.text = today
		//GroundPath.CConditionBox.dStartDate.text = today
		//GroundPath.CConditionBox.dEndDate.text = today

		//日付入力補助設定
		GroundPath.CConditionBox.aStartDate_mc.onRelease = function(){
			var targetTxt = new Array(Owner.GroundPath.CConditionBox.aStartDate)
			//trace(targetTxt)
			var aStartDateChoose = new DateChoose( Owner.GroundPath.CConditionBox, targetTxt);
		}
		GroundPath.CConditionBox.aEndDate_mc.onRelease = function(){
			var targetTxt = new Array(Owner.GroundPath.CConditionBox.aEndDate)
			var aEndDateChoose = new DateChoose( Owner.GroundPath.CConditionBox, targetTxt, Owner.GroundPath.CConditionBox.aStartDate.text );
		}

		GroundPath.CConditionBox.dStartDate_mc.onRelease = function(){
			var targetTxt = new Array(Owner.GroundPath.CConditionBox.dStartDate)
			var dStartDateChoose = new DateChoose( Owner.GroundPath.CConditionBox, targetTxt);
		}
		GroundPath.CConditionBox.dEndDate_mc.onRelease = function(){
			var targetTxt = new Array(Owner.GroundPath.CConditionBox.dEndDate)
			var dEndDateChoose = new DateChoose( Owner.GroundPath.CConditionBox, targetTxt, Owner.GroundPath.CConditionBox.dStartDate.text);
		}

		//ScrollPane設定
		GroundPath.ResultsPanel.results_sp.borderStyle = "none"
		GroundPath.ResultsPanel.results_sp.vPageScrollSize = GroundPath.ResultsPanel.results_sp.height;
		GroundPath.ResultsPanel.results_sp.vLineScrollSize = GroundPath.ResultsPanel.results_sp.height;

		//ScrollPane内Content設定
		GroundPath.ResultsPanel.results_sp.contentPath = "results"
		PaneSource = GroundPath.ResultsPanel.results_sp.content

		//コンボボックス先行アクセス
		var CBList = GroundPath.CConditionBox.use_cb.dropdown;
		GroundPath.CConditionBox.use_cb.selectedIndex = 1;

		//初期状態：結果エリア非表示
		GroundPath.ResultsPanel._visible = false;

		//メニュー切替
		GroundPath.changeBtn.onRelease = function(){
			Owner.GroundPath.CConditionBox.Calender.removeMovieClip();
			Owner.SetMenuList(Owner.GroundPath)
		}

		//検索ボタン設定
		GroundPath.searchBtn.onRelease = function(){
			Owner.GroundPath.CConditionBox.Calender.removeMovieClip();

			//検索条件オブジェクト作成
			var arguments1 =  Owner.GroundPath.CConditionBox.aStartDate.text
			var arguments2 =  Owner.GroundPath.CConditionBox.aEndDate.text
			var arguments3 =  Owner.GroundPath.CConditionBox.dStartDate.text
			var arguments4 =  Owner.GroundPath.CConditionBox.dEndDate.text
			var arguments5 =  Owner.GroundPath.CConditionBox.use_cb.selectedItem.data
			var arguments6 =  Owner.GroundPath.CConditionBox.email_cb.selectedItem.data
			var conditionParam = new Object({aStartDate:arguments1,aEndDate:arguments2,dStartDate:arguments3,dEndDate:arguments4,stayHistory:arguments5,email:arguments6});
		
			//検索条件確認
			if(Owner.CheckconditionParam(conditionParam)){
				Owner.SearchWait( Owner.GroundPath, true );
				//CFCメソッド呼出
				var MaildtlPC:PendingCall = Owner.ServiceObject.getCRMGuestList(conditionParam);
				MaildtlPC.responder = new RelayResponder( Owner, "getCRMGuestList_Result", "getCRMGuestList_Fault" );

			} else {
				//アラート表示
				Owner.ShowConfirm( Owner.GroundPath , "検索条件の期間指定が矛盾しています。\r期間設定を正しく指定して再度検索してください。", Owner, null );
				Owner.SearchWait( Owner.GroundPath, false );
			}
		}

		//クリアボタン設定
		GroundPath.clearBtn.onRelease = function(){
			Owner.GroundPath.CConditionBox.Calender.removeMovieClip();

			var ConditionMC = Owner.GroundPath.CConditionBox
			ConditionMC.aStartDate.text = today
			ConditionMC.aEndDate.text = today
			ConditionMC.dStartDate.text = ""
			ConditionMC.dEndDate.text = ""
			ConditionMC.use_cb.selectedIndex = 1;
			ConditionMC.email_cb.selectedIndex = 0;

		}

		//CSVボタン設定
		GroundPath.csvBtn.onRelease = function(){
			Owner.GroundPath.CConditionBox.Calender.removeMovieClip();
			var OKFunction = Owner.OutputCSV
			Owner.ShowConfirm( this._parent , "一覧をＣＳＶファイルに出力します。よろしいですか？", Owner , OKFunction ,0 );
		}
		

		//プレビューボタン設定
		GroundPath.previewBtn.onRelease = function(){
			Owner.GroundPath.CConditionBox.Calender.removeMovieClip();
			//Owner.MCprintPreview("preview");
			//CFCメソッド呼出
			var arguments1 =  new Array();
			for( var i=0; i<Owner.SearchResult.dataProvider.length; i++ ){
				arguments1.push(Owner.SearchResult.items[i].reserve_code)
			}
			var arguments2 =  "preview"
			var conditionParam = new Object({reserve_code:arguments1,print_mode:arguments2});
			var RgtVIPC:PendingCall = Owner.ServiceObject.RegetValueInfo(conditionParam);
			RgtVIPC.responder = new RelayResponder( Owner, "RegetValueInfo_Result", "RegetValueInfo_Fault" );

		}

		//印刷ボタン設定
		GroundPath.printBtn.onRelease = function(){
			Owner.GroundPath.CConditionBox.Calender.removeMovieClip();
			//Owner.MCprintPreview("print");
			//CFCメソッド呼出
			var arguments1 =  new Array();
			trace(Owner.SearchResult.dataProvider.length + "__!")
			for( var i=0; i<Owner.SearchResult.dataProvider.length; i++ ){
				arguments1.push(Owner.SearchResult.items[i].reserve_code)
			}
			var arguments2 =  "print"
			var conditionParam = new Object({reserve_code:arguments1,print_mode:arguments2});
			var RgtVIPC:PendingCall = Owner.ServiceObject.RegetValueInfo(conditionParam);
			RgtVIPC.responder = new RelayResponder( Owner, "RegetValueInfo_Result", "RegetValueInfo_Fault" );

		}

		//ヘッダーボタン（ソート機能）設定
		GroundPath.ResultsPanel.custmer_idBtn.sortFlg = false
		GroundPath.ResultsPanel.custmer_idBtn.onRelease = function(){
			Owner.dataSort( this , "customer_id", 16 );
		}

		GroundPath.ResultsPanel.custmer_nmBtn.sortFlg = false
		GroundPath.ResultsPanel.custmer_nmBtn.onRelease = function(){
			Owner.dataSort( this , "customer_nm", 1 );
		}

		GroundPath.ResultsPanel.stay_idBtn.sortFlg = false
		GroundPath.ResultsPanel.stay_idBtn.onRelease = function(){
			Owner.dataSort( this , "stay_id", 16 );
		}

		GroundPath.ResultsPanel.arrivaldateBtn.sortFlg = false
		GroundPath.ResultsPanel.arrivaldateBtn.onRelease = function(){
			Owner.dataSort( this , "arrival_date", 1 );
		}

		GroundPath.ResultsPanel.depaturedateBtn.sortFlg = false
		GroundPath.ResultsPanel.depaturedateBtn.onRelease = function(){
			Owner.dataSort( this , "depature_date", 1 );
		}

		GroundPath.ResultsPanel.recentlydateBtn.sortFlg = false
		GroundPath.ResultsPanel.recentlydateBtn.onRelease = function(){
			Owner.dataSort( this , "recently", 1 );
		}
/**/
		GroundPath.ResultsPanel.vipBtn.sortFlg = false
		GroundPath.ResultsPanel.vipBtn.onRelease = function(){
			Owner.dataSort( this , "vip_flg", 1 );
		}

		GroundPath.ResultsPanel.stayhistoryBtn.sortFlg = false
		GroundPath.ResultsPanel.stayhistoryBtn.onRelease = function(){
			Owner.dataSort( this , "history", 16 );
		}

		//GroundPath.ResultsPanel.specsheetBtn.enabled = false;
		//タブ設定
		TabInitialize();

		//各ボタン有効無効切替
		ChangeEnabled( GroundPath, "csvBtn,previewBtn,printBtn" , false )

		//ローディング用ＭＣ削除処理
		GroundPath.onEnterFrame = function(){
			var addSpeed = 0.7
			if( this.CConditionBox.use_cb != undefined ){
				this.Loading.removeMovieClip();
				this._parent.CoveredStage_mc._height = this._parent.CoveredStage_mc._height*addSpeed; 
				if(this._parent.CoveredStage_mc._height<0){
					this._parent.CoveredStage_mc.removeMovieClip();
					delete this.onEnterFrame
				}
			}
		}

		var SwpPC:PendingCall = ServiceObject.sweeper();
		SwpPC.responder = new RelayResponder( this, "sweeper_Result", "sweeper_Fault" );
	}

	
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: TabInitialize
		Description		: タブ設定
		Usage			: TabInitialize();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/30
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function TabInitialize():Void{

		GroundPath.tabChildren = true
		GroundPath.CConditionBox.tabChildren = true
		GroundPath.CConditionBox.use_cb.tabIndex = 2;
		GroundPath.CConditionBox.email_cb.tabIndex = 4;
		GroundPath.searchBtn.tabIndex = 6;
		GroundPath.clearBtn.tabIndex = 8;
		GroundPath.AllCheckBtn.tabIndex = 10;
		GroundPath.AllCancelBtn.tabIndex = 12;
		GroundPath.csvBtn.tabIndex = 14;
		GroundPath.previewBtn.tabIndex = 16;
		GroundPath.printBtn.tabIndex = 18;
		GroundPath.changeBtn.tabIndex = 20;
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getCRMGuestList_Result
		Description		: 検索実行正常終了時イベント
		Usage			: getCRMGuestList_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
						: 2) Updated by	nf)h.miyahara	2005/01/13	キャンセル表示追加
						: 3) Updated by	nf)h.miyahara	2005/04/06	結果表示項目追加
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getCRMGuestList_Result(ReEvt:ResultEvent):Void{
		if( ReEvt.result.mRecordsAvailable == 0){
			SearchWait( GroundPath, false );
			ShowConfirm( GroundPath , "該当レコードはありませんでした。\r検索条件を変更して再度検索を行ってみてください", this , null );
		} else {
			//結果エリア表示
			GroundPath.records_txt.text = ReEvt.result.mRecordsAvailable
			GroundPath.ResultsPanel._visible = true;
			GroundPath.ResultsPanel.Arrow._visible = false;
			//ボタン機能有効化
			ChangeEnabled( GroundPath, "csvBtn,previewBtn,printBtn" , true )

			//レコードセットオブジェクトに結果代入
			var RecSet:RecordSet = RecordSet(ReEvt.result);

			//取得件数表示
			GroundPath.SearchBar.gotoAndStop(1)
			GroundPath.SearchBar.records_txt.text = RecSet.length
			SearchWait( GroundPath, false );

			//初期設定
			var start_y = 0;
			var Owner = this;

			//ペイン内初期化
			GroundPath.ResultsPanel.results_sp.refreshPane();

			//検索結果をデータセットに退避
			SearchResult = new DataSet();
			SearchResult.dataProvider = ReEvt.result;

			//レコード用ＭＣ設定
			for( var i=0; i<ReEvt.result.mRecordsAvailable; i++ ){
				PaneSource.attachMovie("resultBar", "resultBar_" + i  , GPDepth, {_y:start_y})
				GPDepth++;
				start_y = start_y + PaneSource["resultBar_" + i]._height

				PaneSource["resultBar_" + i].createClassObject(TextArea, "information_txt", GPDepth, {_x:100,_y:60})
				GPDepth++;
				//PaneSource["resultBar_" + i].information_txt.hScrollPolicy = "auto"
				PaneSource["resultBar_" + i].information_txt.setSize(820,20);
				PaneSource["resultBar_" + i].information_txt.wordWrap = true;
				PaneSource["resultBar_" + i].information_txt.borderStyle = "solid"
				PaneSource["resultBar_" + i].information_txt.editable = false;

				PaneSource["resultBar_" + i].createClassObject(TextArea, "c_information_txt", GPDepth, {_x:100,_y:80})
				GPDepth++;
				//PaneSource["resultBar_" + i].c_information_txt.hScrollPolicy = "auto"
				PaneSource["resultBar_" + i].c_information_txt.setSize(820,40);
				PaneSource["resultBar_" + i].c_information_txt.wordWrap = true;
				PaneSource["resultBar_" + i].c_information_txt.borderStyle = "solid"
				PaneSource["resultBar_" + i].c_information_txt.editable = false;

				//PaneSource["resultBar_" + i].information_txt.setSize(PaneSource["resultBar_" + i].information_txt.width,PaneSource["resultBar_" + i].information_txt.height)
				//PaneSource["resultBar_" + i].c_information_txt.setSize(PaneSource["resultBar_" + i].c_information_txt.width,PaneSource["resultBar_" + i].c_information_txt.height)

				//データ割当
				PaneSource["resultBar_" + i].customerId_txt.text = ReEvt.result.items[i].customer_id;
				PaneSource["resultBar_" + i].customerNm_txt.text = ReEvt.result.items[i].customer_nm;
				PaneSource["resultBar_" + i].stayId_txt.text = ReEvt.result.items[i].stay_id;
				PaneSource["resultBar_" + i].arrivalDay_txt.text = ReEvt.result.items[i].arrival_date;
				PaneSource["resultBar_" + i].departureDay_txt.text = ReEvt.result.items[i].depature_date;
				PaneSource["resultBar_" + i].recentlyDay_txt.text = ReEvt.result.items[i].recently;
				PaneSource["resultBar_" + i].stayHistory_txt.text = ReEvt.result.items[i].history;
				PaneSource["resultBar_" + i].satisfaction_txt.text = ReEvt.result.items[i].satisfaction;
				PaneSource["resultBar_" + i].information_txt.text = ReEvt.result.items[i].information;
				PaneSource["resultBar_" + i].planNm_txt.text = ReEvt.result.items[i].plan_nm;
				PaneSource["resultBar_" + i].valueNm_txt.text = ReEvt.result.items[i].value_nm;
				PaneSource["resultBar_" + i].headcount_txt.text = ReEvt.result.items[i].head_count;
				PaneSource["resultBar_" + i].c_information_txt.text = ReEvt.result.items[i].c_information;
				PaneSource["resultBar_" + i].c_information_txt.text = ReEvt.result.items[i].c_information;
				PaneSource["resultBar_" + i].room_num_txt.text = ReEvt.result.items[i].room_num;
				PaneSource["resultBar_" + i].room_name_txt.text = ReEvt.result.items[i].room_name;

				
				//VIP設定
				if(ReEvt.result.items[i].vip_flg == 1){
					PaneSource["resultBar_" + i].vip_icon.gotoAndStop(2);
					PaneSource["resultBar_" + i].important_txt.text = "重要";
				}
				
				//既ＣＳ回答設定
				if( ReEvt.result.items[i].satisfaction.length > 0 ){
					PaneSource["resultBar_" + i].header_mc.gotoAndStop(2);
					PaneSource["resultBar_" + i].header_mc.satisfaction_txt.text = ReEvt.result.items[i].satisfaction;
				}

				//キャンセルデータ設定
				if( ReEvt.result.items[i].cancel_code == 100000000001 ){
					PaneSource["resultBar_" + i].attachMovie("cancelDataBtn","cancelDataBtn",GPDepth,{_y:20})
					GPDepth++;
				}

				//スペックシートボタン設定
				PaneSource["resultBar_" + i].specSheetBtn.index = i
				PaneSource["resultBar_" + i].specSheetBtn.onRelease = function(){
					//trace( "MyID:" + this.id + "のスペックシート表示")
					Owner.SetTargetData(this.index)
				}
				
				//アウトレットアイコン
				if(ReEvt.result.items[i].item_flag1=="1"){
					PaneSource["resultBar_" + i].outlet_mc.item1_mc.gotoAndStop(2);
				}
				if(ReEvt.result.items[i].item_flag2=="1"){
					PaneSource["resultBar_" + i].outlet_mc.item2_mc.gotoAndStop(2);
				}
				if(ReEvt.result.items[i].item_flag3=="1"){
					PaneSource["resultBar_" + i].outlet_mc.item3_mc.gotoAndStop(2);
				}
				if(ReEvt.result.items[i].item_flag4=="1"){
					PaneSource["resultBar_" + i].outlet_mc.item4_mc.gotoAndStop(2);
				}
				
				PaneSource["resultBar_" + i].viBtn.reserve_code = ReEvt.result.items[i].reserve_code;

				PaneSource["resultBar_" + i].viBtn.MCID = i
				PaneSource["resultBar_" + i].viBtn.valueinfo = ReEvt.result.items[i].valueinfo;
				PaneSource["resultBar_" + i].viBtn.onRelease = function(){
					//ここに処理追加
					var	VI = new ValueInformation( Owner.PaneSource,Owner.GroundPath, this.reserve_code, this.MCID);
				}
			}
		}
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getCRMGuestList_Fault
		Description		: 検索実行不正終了時イベント
		Usage			: getCRMGuestList_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getCRMGuestList_Fault(FltEvt:FaultEvent):Void{	//trace("getCRMGuestList_Fault: " + FltEvt.fault.description);
		SearchWait( GroundPath, false );
		ShowConfirm( GroundPath , "検索実行できませんでした。\rエラーメッセージ："+ FltEvt.fault.description, this , null );
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: dataSort
		Description		: データソースソート機能
		Usage			: dataSort();
		Attributes		: tgtbtn：MovieClip		（ソートボタン）
						: tgtField：String		（ソート対象フィールド）
						: opt：Number			（ソートオプション）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/03
		History			: 2) Updated by	nf)h.miyahara	2005/01/14	キャンセル表示追加
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function dataSort( tgtbtn, tgtField, opt ){

		//ソート昇順降順アイコン表示設定
		GroundPath.ResultsPanel.Arrow._visible = true;
		GroundPath.ResultsPanel.Arrow._x = tgtbtn._x + tgtbtn._width -10;
		GroundPath.ResultsPanel.Arrow._rotation = 0

		//データソースの書式設定
		for(var i=0; i<SearchResult.dataProvider.length; i++){
			SearchResult.items[i].customer_id = parseInt(SearchResult.items[i].customer_id)
			SearchResult.items[i].stay_id = parseInt(SearchResult.items[i].stay_id)
			SearchResult.items[i].history = parseInt(SearchResult.items[i].history)
		}

		//DataSet内ソート
		if(tgtbtn.sortFlg){	//昇順
			SearchResult.dataProvider = SearchResult.items.sortOn( tgtField, opt );

		} else {			//降順
			SearchResult.dataProvider = SearchResult.items.sortOn( tgtField, opt + 2 );
			GroundPath.ResultsPanel.Arrow._rotation += 180

		}
		tgtbtn.sortFlg = !tgtbtn.sortFlg

		//レコード用ＭＣ内データ書換
		for( var i=0; i<SearchResult.dataProvider.length; i++ ){
			//trace(SearchResult.items[i].stay_id)
			PaneSource["resultBar_" + i].customerId_txt.text = SearchResult.items[i].customer_id;
			PaneSource["resultBar_" + i].customerNm_txt.text = SearchResult.items[i].customer_nm;
			PaneSource["resultBar_" + i].stayId_txt.text = SearchResult.items[i].stay_id;
			PaneSource["resultBar_" + i].arrivalDay_txt.text = SearchResult.items[i].arrival_date;
			PaneSource["resultBar_" + i].departureDay_txt.text = SearchResult.items[i].depature_date;
			PaneSource["resultBar_" + i].recentlyDay_txt.text = SearchResult.items[i].recently;
			PaneSource["resultBar_" + i].stayHistory_txt.text = SearchResult.items[i].history;
			PaneSource["resultBar_" + i].satisfaction_txt.text = SearchResult.items[i].satisfaction;
			PaneSource["resultBar_" + i].information_txt.text = SearchResult.items[i].information;
			PaneSource["resultBar_" + i].planNm_txt.text = SearchResult.items[i].plan_nm;
			PaneSource["resultBar_" + i].valueNm_txt.text = SearchResult.items[i].value_nm;
			PaneSource["resultBar_" + i].headcount_txt.text = SearchResult.items[i].head_count;
			PaneSource["resultBar_" + i].c_information_txt.text = SearchResult.items[i].c_information;
			PaneSource["resultBar_" + i].viBtn.reserve_code = SearchResult.items[i].reserve_code;
			PaneSource["resultBar_" + i].viBtn.valueinfo = SearchResult.items[i].valueinfo;
			PaneSource["resultBar_" + i].room_num_txt.text = SearchResult.items[i].room_num;
			PaneSource["resultBar_" + i].room_name_txt.text = SearchResult.items[i].room_name;

			//アウトレットアイコン
			PaneSource["resultBar_" + i].outlet_mc.item1_mc.gotoAndStop(1);
			PaneSource["resultBar_" + i].outlet_mc.item2_mc.gotoAndStop(1);
			PaneSource["resultBar_" + i].outlet_mc.item3_mc.gotoAndStop(1);
			PaneSource["resultBar_" + i].outlet_mc.item4_mc.gotoAndStop(1);

			if(SearchResult.items[i].item_flag1=="1"){
				PaneSource["resultBar_" + i].outlet_mc.item1_mc.gotoAndStop(2);
			}
			if(SearchResult.items[i].item_flag2=="1"){
				PaneSource["resultBar_" + i].outlet_mc.item2_mc.gotoAndStop(2);
			}
			if(SearchResult.items[i].item_flag3=="1"){
				PaneSource["resultBar_" + i].outlet_mc.item3_mc.gotoAndStop(2);
			}
			if(SearchResult.items[i].item_flag4=="1"){
				PaneSource["resultBar_" + i].outlet_mc.item4_mc.gotoAndStop(2);
			}
				

			//VIP設定
			if(SearchResult.items[i].vip_flg == 1){
				PaneSource["resultBar_" + i].vip_icon.gotoAndStop(2);
				PaneSource["resultBar_" + i].important_txt.text = "重要";
			} else {
				PaneSource["resultBar_" + i].vip_icon.gotoAndStop(1);
				PaneSource["resultBar_" + i].important_txt.text = "";
			}
			
			//キャンセルデータ設定
			PaneSource["resultBar_" + i].cancelDataBtn.removeMovieClip();
			if( SearchResult.items[i].cancel_code == 100000000001 ){
				PaneSource["resultBar_" + i].attachMovie("cancelDataBtn","cancelDataBtn",GPDepth,{_y:20})
				GPDepth++;
			}

			//既ＣＳ回答設定
			if( SearchResult.items[i].satisfaction.length > 0 ){
				PaneSource["resultBar_" + i].header_mc.gotoAndStop(2);
				PaneSource["resultBar_" + i].header_mc.satisfaction_txt.text = SearchResult.items[i].satisfaction;
			} else {
				PaneSource["resultBar_" + i].header_mc.gotoAndStop(1);
				PaneSource["resultBar_" + i].header_mc.satisfaction_txt.text = "";
			}

		}

	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: OutputCSV
		Description		: ＣＳＶファイル出力
		Usage			: OutputCSV( Obj );
		Attributes		: Obj : （コンファームを呼出した呼出元クラスインスタンス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function OutputCSV( Obj ){
		//インスタンス入替
		this = Obj
		
		//CFCメソッド呼出
		var MailtmpPC:PendingCall = ServiceObject.CRMResultOutputCSV( SearchResult.items );
		MailtmpPC.responder = new RelayResponder( this, "OutputCSV_Result", "OutputCSV_Fault" );

	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: OutputCSV_Result
		Description		: ＣＳＶ出力正常終了時
		Usage			: OutputCSV_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function OutputCSV_Result(ReEvt:ResultEvent):Void{

		outputPath = ReEvt.result.outputPath
		ShowConfirm( GroundPath , ReEvt.result.outputMsg + "に出力しました。ファイルを表示しますか？", this, this.getCSVFile );
		
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: OutputCSV_Fault
		Description		: ＣＳＶ出力不正終了時
		Usage			: OutputCSV_Fault(FltEvt:FaultEvent);
		Attributes		: FltEvt:FaultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function OutputCSV_Fault(FltEvt:FaultEvent):Void{
		//処理を記述
		//var myConfirm:ConfirmWindow = new ConfirmWindow( GroundPath , "ＣＳＶファイルが出力できませんでした。\rエラーメッセージ："+ FltEvt.fault.description, this , null );
		ShowConfirm( GroundPath , "ＣＳＶファイルが出力できませんでした。\rエラーメッセージ："+ FltEvt.fault.description, this , null );
		trace("Categories Fault: " + FltEvt.fault.description);
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: getCSVFile
		Description		: ＣＳＶファイルダウンロード
		Usage			: getCSVFile(Obj);
		Attributes		: Obj : （コンファームを呼出した呼出元クラスインスタンス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/14
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function getCSVFile( Obj ){
		//インスタンス入替
		this = Obj

		//該当ファイルを別ウインドウ表示
		//var targetFilePath = "Javascript:void(location.href='http://" + _global.hostAddress + outputPath + "')"
		var targetFilePath = "http://" + _global.hostAddress + outputPath
		GroundPath.getURL( targetFilePath , "_blank" )

	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: SetTargetData
		Description		: 編集対象取得～編集画面表示
		Usage			: SetTargetData(index);
		Attributes		: index：	（編集対象ＩＮＤＥＸ）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/06
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function SetTargetData(index){
		
		//グローバル変数に編集対象ＩＮＤＥＸ・データプロバイダを退避
		_global.targetIndex = index;
		_global.targetData = SearchResult.dataProvider;

		GroundPath.createEmptyMovieClip("SpecSheetScreen", GPDepth);
		GPDepth++;
		GroundPath.SpecSheetScreen.beginFill(0xCCCCCC);
		GroundPath.SpecSheetScreen.moveTo(0,0);
		GroundPath.SpecSheetScreen.lineTo(Stage.width, 0);
		GroundPath.SpecSheetScreen.lineTo(Stage.width, Stage.height);
		GroundPath.SpecSheetScreen.lineTo(0, Stage.height);
		GroundPath.SpecSheetScreen.lineTo(0, 0);
		GroundPath.SpecSheetScreen.endFill();
		//GroundPath.SpecSheetScreen.enabled = false
		GroundPath.SpecSheetScreen.onRollOver = function(){}

		//スペックシート表示画面読込み
		var LoadClip:LoadImage = new LoadImage( "specSheet.swf", GroundPath.SpecSheetScreen );
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: MCprintPreview
		Description		: スクロールペイン印刷プレビュー設定
		Usage			: MCprintPreview();
		Attributes		: none
		Note			: 共通関数
		History			: 1) Coded by	nf)h.miyahara	2004/10/04
						: 2) Coded by	nf)h.miyahara	2005/01/26	縦印刷対応
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function MCprintPreview(mode){

		//変数設定
		var Owner = this;
		var printtargetMC = GroundPath.ResultsPanel;
		var OrgSource = GroundPath.ResultsPanel.results_sp.content
		var headerArea_h = 30;
		var headerArea_x = 10;
		var temp:mx.data.types.Str;
		var temp:mx.data.types.Num;
		var temp_MC_y = printtargetMC._y;					//対象ＭＣＹ座標退避
		var temp_MC_h = printtargetMC.results_sp.height;	//対象ＭＣ高さ退避
		var temp_MC_depth = printtargetMC.getDepth();		//対象ＭＣ深度退避
		var tmp_sp_w = 960
		var tmp_sp_h = 360
		var h_margin = 30

		if( GroundPath.PPreview == undefined ){
			//プレビュー用ＭＣ
			GroundPath.attachMovie("PreviewHeader","PPreview", GPDepth,{_x:GroundPath._x, _y:GroundPath._y});
			GPDepth++;
			GroundPath.PPreview.beginFill(0xFFFFF9);
			GroundPath.PPreview.moveTo(GroundPath.PPreview._x,GroundPath.PPreview._height);
			GroundPath.PPreview.lineTo(Stage.width, GroundPath.PPreview._height);
			GroundPath.PPreview.lineTo(Stage.width, Stage.height);
			GroundPath.PPreview.lineTo(GroundPath.PPreview._x, Stage.height);
			GroundPath.PPreview.lineTo(GroundPath.PPreview._x, GroundPath.PPreview._height);
			GroundPath.PPreview.endFill();
	
			//ページング用ＣＢ作成
			GroundPath.PPreview.createClassObject(ComboBox,"pages_cb",GPDepth,{_x:150,_y:5,_width:100, _height:22});
			GPDepth++;

			var printTarget = GroundPath.PPreview.createEmptyMovieClip("PrintTarget", GPDepth);
			GPDepth++;
			

			//プレビュー用ＳＰ作成
			printTarget.attachMovie("printHeader","resultHeader", GPDepth);
			GPDepth++;
			var myPane = GroundPath.PPreview.PrintTarget.createClassObject(ScrollPane,"temp_sp",GPDepth,{_y:GroundPath.PPreview.PrintTarget.resultHeader._height});
			myPane.setSize(960,360)
			GPDepth++;
			myPane.borderStyle = "none"
			myPane.vPageScrollSize = myPane.height;
			myPane.vLineScrollSize = myPane.height;

			printTarget._y = headerArea_h + 5

			var pageWidth = 559
			var pageHeight = 806

			var s_w = pageWidth/tmp_sp_w
			var s_h = pageHeight/tmp_sp_h
			var r_h = tmp_sp_w/pageWidth
			printTarget._xscale = s_w*100
			printTarget._yscale = s_w*100
			var printSize_h = Math.floor(pageHeight*r_h)
			var resize_h = pageHeight//+100
			//var resize_h = Math.floor(pageHeight*r_h)
			//GroundPath.PPreview.PrintTarget.temp_sp.setSize(tmp_sp_w,resize_h);
			myPane.setSize(tmp_sp_w,resize_h);

			//プレビュー用ＳＰ位置設定
			//printtargetMC.swapDepths( GroundPath.PPreview.getDepth()+10 ); 
			//printtargetMC.results_sp.setSize( printtargetMC.results_sp._width, 545 )
			if( mode == "preview" ){
				//printtargetMC._y = headerArea_h + 5
			}
	
			//ペイン内初期化
			var tempRec = SearchResult

			//ScrollPane内Content設定
			myPane.contentPath = "results"
			var PaneSource = myPane.content
			var start_y = 0;

			//レコード用ＭＣ設定
			for( var i=0; i<tempRec.dataProvider.length; i++ ){
				
				if(i!=0){
					PaneSource.attachMovie("printRowHeader", "printRowHeader_" + i  , GPDepth, {_y:start_y});
					GPDepth++;
					start_y+=PaneSource["printRowHeader_" + i]._height;
				}
							
				PaneSource.attachMovie("resultBarPrint", "resultBar_" + i  , GPDepth, {_y:start_y})
				GPDepth++;
				
				PaneSource["resultBar_" + i].createClassObject(TextArea, "information_txt", GPDepth, {_x:100,_y:40})
				GPDepth++;
				//PaneSource["resultBar_" + i].information_txt.hScrollPolicy = "auto"
				PaneSource["resultBar_" + i].information_txt.setSize(660,20);
				PaneSource["resultBar_" + i].information_txt.wordWrap = true;
				PaneSource["resultBar_" + i].information_txt.borderStyle = "solid"
				PaneSource["resultBar_" + i].information_txt.editable = false;


				PaneSource["resultBar_" + i].createClassObject(TextArea, "c_information_txt", GPDepth, {_x:0,_y:60})
				GPDepth++;
				PaneSource["resultBar_" + i].c_information_txt.setSize(760,40);
				PaneSource["resultBar_" + i].c_information_txt.wordWrap = true;
				PaneSource["resultBar_" + i].c_information_txt.borderStyle = "solid"
				PaneSource["resultBar_" + i].c_information_txt.editable = false;
				
				//ishigami 3/25 追加
				PaneSource["resultBar_" + i].vi_txt.autoSize=true;
				PaneSource["resultBar_" + i].vi_txt.wordWrap=true;
				
				//データ割当
				PaneSource["resultBar_" + i].customerId_txt.text = tempRec.items[i].customer_id;
				PaneSource["resultBar_" + i].customerNm_txt.text = tempRec.items[i].customer_nm;
				PaneSource["resultBar_" + i].stayId_txt.text = tempRec.items[i].stay_id;
				PaneSource["resultBar_" + i].arrivalDay_txt.text = tempRec.items[i].arrival_date;
				PaneSource["resultBar_" + i].roomName_txt.text = tempRec.items[i].room_name;
				PaneSource["resultBar_" + i].roomNum_txt.text = tempRec.items[i].room_num;
				PaneSource["resultBar_" + i].departureDay_txt.text = tempRec.items[i].depature_date;
				PaneSource["resultBar_" + i].recentlyDay_txt.text = tempRec.items[i].recently;
				PaneSource["resultBar_" + i].stayHistory_txt.text = tempRec.items[i].history;
				PaneSource["resultBar_" + i].satisfaction_txt.text = tempRec.items[i].satisfaction;
				PaneSource["resultBar_" + i].information_txt.text = tempRec.items[i].information;
				PaneSource["resultBar_" + i].planNm_txt.text = tempRec.items[i].plan_nm;
				PaneSource["resultBar_" + i].valueNm_txt.text = tempRec.items[i].value_nm;
				PaneSource["resultBar_" + i].headcount_txt.text = tempRec.items[i].head_count;
				PaneSource["resultBar_" + i].c_information_txt.text = tempRec.items[i].c_information;
				PaneSource["resultBar_" + i].vi_txt.text =  "Value Information"+newline+tempRec.items[i].valueinfo;
				//var newVITxt = OrgSource["resultBar_" + i].viBtn.valueinfo
				//PaneSource["resultBar_" + i].vi_txt.text =  "Value Information"+newline+newVITxt;

				//VIP設定
				if(tempRec.items[i].vip_flg == 1){
					PaneSource["resultBar_" + i].vip_icon.gotoAndStop(2);
					PaneSource["resultBar_" + i].important_txt.text = "重要";
				}
				
				//既ＣＳ回答設定
				if( tempRec.items[i].satisfaction.length > 0 ){
					PaneSource["resultBar_" + i].header_mc.gotoAndStop(2);
					PaneSource["resultBar_" + i].header_mc.satisfaction_txt.text = tempRec.items[i].satisfaction;
				}

				//キャンセルデータ設定
				if( tempRec.items[i].cancel_code == 100000000001 ){
					PaneSource["resultBar_" + i].attachMovie("cancelDataBtn","cancelDataBtn",GPDepth,{_y:20})
					GPDepth++;
				}
				
				//アウトレットアイコン
				if(tempRec.items[i].item_flag1=="1"){
					PaneSource["resultBar_" + i].outlet_mc.item1_mc.gotoAndStop(2);
				}
				if(tempRec.items[i].item_flag2=="1"){
					PaneSource["resultBar_" + i].outlet_mc.item2_mc.gotoAndStop(2);
				}
				if(tempRec.items[i].item_flag3=="1"){
					PaneSource["resultBar_" + i].outlet_mc.item3_mc.gotoAndStop(2);
				}
				if(tempRec.items[i].item_flag4=="1"){
					PaneSource["resultBar_" + i].outlet_mc.item4_mc.gotoAndStop(2);
				}
				
				PaneSource["resultBar_" + i].specSheetBtn._visible = false
				PaneSource["resultBar_" + i].specSheetBtn.enabled = false
				
				start_y = start_y + PaneSource["resultBar_" + i]._height + h_margin

			}
			var rowHeight:Number = PaneSource.resultBar_0._height;
			
			if(PaneSource.printRowHeader1 != undefined){
				rowHeight+=PaneSource.printRowHeader1._height;
			}
			
			//var tmp_rowsPerPage:Number = Math.floor((resize_h - printTarget.resultHeader._height)/ rowHeight);
			var tmp_rowsPerPage:Number = Math.floor((printSize_h - printTarget.resultHeader._height)/ rowHeight);
			//myPane.setSize(myPane.width, tmp_rowsPerPage*rowHeight);
			var rowsPerPage = tmp_rowsPerPage;
/*
			trace("tmp_rowsPerPage: " + tmp_rowsPerPage)
			trace("rowsPerPage : " + rowsPerPage)
			trace("GroundPath.PPreview.PrintTarget._height : " + printTarget._height)
			trace("GroundPath.PPreview.PrintTarget.temp_sp : " + myPane.height)
			trace("PaneSource.resultBar_0._height : " + PaneSource.resultBar_0._height)
*/
			//var rowsPerPage = 5
			var dummyCnt = rowsPerPage - (tempRec.dataProvider.length%rowsPerPage)
	
			//印刷用非表示
			//printtargetMC.specsheetBtn._visible = false
			for( var i=0; i<tempRec.dataProvider.length+dummyCnt; i++ ){
				//PaneSource["resultBar_" + i].information_txt.setSize(660,40);
				//PaneSource["resultBar_" + i].information_txt.setSize(301,40);
				//PaneSource["resultBar_" + i].c_information_txt.setSize(661,40);
				//印刷用非表示
				//PaneSource["resultBar_" + i].specSheetBtn._visible = false;
				//スクロール移動用ダミー処理
				if( i >= tempRec.dataProvider.length ){
					PaneSource["resultBar_" + i]._visible = false;
					PaneSource["printRowHeader_" + i]._visible = false;
				}
			}
	
			//ページング用変数設定
			//var Pagesize = myPane.height + printTarget.resultHeader._height; //Math.floor((targetPath.PPreview.tmp_dg.height - targetPath.PPreview.tmp_dg.headerHeight)/targetPath.PPreview.tmp_dg.rowHeight);
//			var Pagesize = printSize_h + printTarget.resultHeader._height; //Math.floor((targetPath.PPreview.tmp_dg.height - targetPath.PPreview.tmp_dg.headerHeight)/targetPath.PPreview.tmp_dg.rowHeight);
			
//			trace("Pagesize : " + Pagesize)
			//var pages = Math.ceil(myPane.height/Pagesize);
//			var pages = Math.ceil(tempRec.dataProvider.length/rowsPerPage);
//			trace("pages : " + pages)

var Pagesize = Math.floor(printtargetMC._height*r_h); //Math.floor((targetPath.PPreview.tmp_dg.height - targetPath.PPreview.tmp_dg.headerHeight)/targetPath.PPreview.tmp_dg.rowHeight);
//trace("s_w : " + s_w)
var pages = Math.ceil(Pagesize/printSize_h);



			var pages_arr = new Array({data:"",label:"全ページ"});
			for(var i=0;i<pages;i++){
				pages_arr.push({data:i,label:i+1+"ページ"});
			}
	
			printTarget.Pagesize = Pagesize;
			printTarget.pages = pages;
			printTarget.rowsPerPage = rowsPerPage;
			printTarget.rowHeight = rowHeight;
			printTarget.r_h = r_h
			printTarget.resize_h = resize_h


			var today:Date=new Date();
			printTarget.resultHeader.date_txt.text=today.getFullYear()+"/"+(today.getMonth()+1)+"/"+today.getDate();
			
			printTarget.resultHeader.langeA_txt.text=GroundPath.CConditionBox.aStartDate.text+"～"+GroundPath.CConditionBox.aEndDate.text;
			printTarget.resultHeader.langeD_txt.text=GroundPath.CConditionBox.dStartDate.text+"～"+GroundPath.CConditionBox.dEndDate.text;
			if(printTarget.resultHeader.langeA_txt.text=="～"){
				printTarget.resultHeader.langeA_txt.text="";
			}
			if(printTarget.resultHeader.langeD_txt.text=="～"){
				printTarget.resultHeader.langeD_txt.text="";
			}
		
			printTarget.resultHeader.page_txt.text="1/"+pages;

			//コンボボックスイベント設定
			var ComboListner = new Object();
			ComboListner.rowsPerPage = rowsPerPage;

			//Loadイベント
			GroundPath.PPreview.pages_cb.onEnterFrame = function(){
				Owner.GroundPath.PPreview.pages_cb.dataProvider = pages_arr
				if(Owner.GroundPath.PPreview.pages_cb.dataProvider.length > 0){
					delete this.onEnterFrame;
				}
			}

			var tmp_adjust = 30.3*2
			//Changeイベント
			ComboListner.change = function(eventObj){

				//var startRec = rowsPerPage*eventObj.target.value*60;
				var startRec = (pageHeight*eventObj.target.value*r_h)-(tmp_adjust*eventObj.target.value);

				trace("startRec" + startRec)

				myPane.vScrollPolicy = "auto";
				printTarget.resultHeader.page_txt.text= (eventObj.target.value+1) + "/" + pages;
				if(eventObj.target.value == "" or startRec < 0){
					startRec = 0
					printTarget.resultHeader.page_txt.text="1/"+pages;
				} else if(eventObj.target.value == eventObj.target.dataProvider.length-2){
					myPane.vScrollPolicy = "off";
				}
				myPane.vPosition = startRec
			}
			GroundPath.PPreview.pages_cb.addEventListener( "change", ComboListner );
	
			//閉じるボタン
			GroundPath.PPreview.pre_closeBtn.onRelease = function (){
				//Owner.RecoverResultData(temp_MC_y,temp_MC_h,temp_MC_depth);
				this._parent.removeMovieClip();
			}
	
			//印刷ボタン
			GroundPath.PPreview.pre_printBtn.onRelease = function (){
//				myPane.setSize(myPane.width, tmp_rowsPerPage*rowHeight);
				myPane.setSize(myPane.width, printSize_h);

				//Owner.doDGPrint(Owner.targetPath.PPreview.tmp_dg)
				//Owner.doMCPrint(Owner.GroundPath.ResultsPanel,mode);
				Owner.doMCPrint(Owner.GroundPath.PrintTarget,mode);

			}
		} else {

		}

		//印刷ボタンより直接コール
		if( mode =="print" ){
			GroundPath.PPreview._y = 800;
			myPane.setSize(myPane.width, tmp_rowsPerPage*rowHeight);
			GroundPath.PPreview.onEnterFrame = function(){
				if( myPane != undefined ){
					var tmp = Owner.doMCPrint("print" );
					if(tmp == undefined){
						Owner.GroundPath.PPreview.removeMovieClip();
					}
					
					delete this.onEnterFrame;
				}
			}
		}
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: doMCPrint
		Description		: 印刷実行
		Usage			: doMCPrint(tgtMC);
		Attributes		: tgtMC			印刷対象ＭＣ
		Note			: 
		History			: 1) Coded by	nf)h.miyahara	2004/09/27
						: 2) Coded by	nf)h.miyahara	2005/01/26	縦印刷対応
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function doMCPrint(mode){
		trace("doMCPrint:mode  " + mode)
		var printtargetMC = GroundPath.PPreview.PrintTarget;
		var PrintQueue : PrintJob = new PrintJob();
		var prev_vPosition:Number = printtargetMC.temp_sp.vPosition;
		var prev_width:Number = printtargetMC.temp_sp.width;
		var prev_height:Number = printtargetMC.temp_sp.height;
		var pageHeight = 806
		//var RecordCount:Number = printtargetMC.dataProvider.length

		if( PrintQueue.start() != true){
			printtargetMC.temp_sp.setSize(prev_width,printtargetMC.resize_h);
		//GroundPath.PPreview.removeMovieClip();
		return;
		}
		//printtargetMC.results_sp.setSize(PrintQueue.pageWidth,PrintQueue.pageHeight);
		printtargetMC.temp_sp.vScrollPolicy = "off";

		//var rowsPerPage = 9
		var rowsPerPage = printtargetMC.rowsPerPage;
		var Pagesize = printtargetMC.Pagesize; //Math.floor((targetPath.PPreview.tmp_dg.height - targetPath.PPreview.tmp_dg.headerHeight)/targetPath.PPreview.tmp_dg.rowHeight);
			//trace("height : " + printtargetMC.results_sp.height)
		var pages = printtargetMC.pages;
		var headerHeight = 65;
//		var rowHeight = printtargetMC.rowHeight;
		var rowHeight = printtargetMC.r_h;


		//対象ページ確認
		//trace("：" + GroundPath.PPreview.pages_cb.selectedItem.data.length)
		var startCnt = 0
		if( GroundPath.PPreview.pages_cb.selectedItem.data != "" ){
			startCnt = GroundPath.PPreview.pages_cb.selectedItem.data//0
			pages = GroundPath.PPreview.pages_cb.selectedItem.data+1//1
		}

		var yStart = 0;
		for ( var i=startCnt; i<pages; i++ ) {

			if(startCnt != 0 && startCnt != pages ){
				//printtargetMC.resultHeader.page_txt.text="1/1";
			} else {
				printtargetMC.resultHeader.page_txt.text=(i+1)+"/"+pages;
			}

			var tmp_adjust = 30.3*2
			var startRec = (pageHeight*i* printtargetMC.r_h)-(tmp_adjust*i);
			if(startRec<0){
				startRec = 0
			}

			//var startRec = prev_height*i*printtargetMC.r_h;
			printtargetMC.temp_sp.vScrollPolicy = "off";
			printtargetMC.temp_sp.vPosition = startRec;
//			trace("startRec" + startRec)

			//var b= {xMin:0,xMax:PrintQueue.pageWidth,yMin:yStart,yMax:PrintQueue.pageHeight};
			var b= {xMin:0,xMax:printtargetMC.temp_sp.width,yMin:yStart,yMax:printtargetMC.temp_sp.height};

			//trace(PrintQueue.pageWidth)
			//trace(PrintQueue.pageHeight)
			yStart = 0// headerHeight;
			PrintQueue.addPage(printtargetMC,b);

		}
		PrintQueue.send();
		
		delete PrintQueue;
		
		printtargetMC.temp_sp.setSize(prev_width,printtargetMC.resize_h);
		printtargetMC.temp_sp.vPosition = 0;
		printtargetMC.temp_sp.vScrollPolicy = "auto";

		//印刷ボタンより直接コール
		if( mode =="print" ){
			trace("印刷ボタンより直接コール")
			GroundPath.PPreview.removeMovieClip();
		}


	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: RecoverResultData
		Description		: プレビュー用に変更した対象ＭＣのデータ、サイズ等を元に戻す
		Usage			: RecoverSearchData();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/05
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function RecoverResultData( y_pos, h_size, dpt ){
		var printtargetMC = GroundPath.ResultsPanel;
		var temp_MC_y = y_pos;
		var temp_MC_h = h_size;
		var temp_MC_depth = dpt;
		var start_y = 0;

		//スクロールペイン位置戻し
		printtargetMC._y = temp_MC_y;
		printtargetMC.results_sp.setSize( printtargetMC.results_sp._width, temp_MC_h )
		printtargetMC.swapDepths( temp_MC_depth ); 

		for( var i=0; i<SearchResult.dataProvider.length; i++ ){
			PaneSource["resultBar_" + i].information_txt.setSize(401,40);
			PaneSource["resultBar_" + i].c_information_txt.setSize(761,40);
			//印刷用非表示
			PaneSource["resultBar_" + i].specSheetBtn._visible = true;
		}

		//印刷用非表示戻し
		printtargetMC.specsheetBtn._visible = true
		printtargetMC.results_sp.vScrollPolicy = "auto";

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: CheckconditionParam
		Description		: 検索条件のチェック
		Usage			: CheckconditionParam(Obj);
		Attributes		: Obj : （検索条件オブジェクト）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/10/05
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function CheckconditionParam( Obj ){
		var Compelete = false
		if( Obj.aStartDate <= Obj.aEndDate && Obj.dStartDate <= Obj.dEndDate){
			Compelete = true
		}
		
		return Compelete
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: RegetValueInfo_Result
		Description		: ValueInformation再取得正常終了時
		Usage			: RegetValueInfo_Result(ReEvt:ResultEvent);
		Attributes		: ReEvt:ResultEvent : （ＣＦＣ完了イベント）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2005/04/06
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function RegetValueInfo_Result(ReEvt:ResultEvent):Void{
		for(var i=0; i<ReEvt.result.mRecordsAvailable; i++){
			SearchResult.items[i].valueinfo =  ReEvt.result.items[i].valueinfo;
		}
		//戻値判別でプレビューorプリント
		if(ReEvt.result.items[0].print_mode == "preview"){
			MCprintPreview("preview");
		} else {
			MCprintPreview("print");
		}
	}

}
