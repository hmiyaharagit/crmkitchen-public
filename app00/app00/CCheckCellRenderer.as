﻿//****************************************************************************
//Copyright (C) 2003 Macromedia, Inc. All Rights Reserved.
//The following is Sample Code and is subject to all restrictions on
//such code as contained in the End User License Agreement accompanying
//this product.
//****************************************************************************

import mx.core.UIComponent
import mx.controls.CheckBox

class CCheckCellRenderer extends UIComponent
{

	var check : MovieClip;
	var listOwner : MovieClip; // the reference we receive to the list
	var getCellIndex : Function; // the function we receive from the list
	var	getDataLabel : Function; // the function we receive from the list

	function CCheckCellRenderer()
	{
	}

	function createChildren(Void) : Void
	{
		check = createObject("CheckBox", "copechk", 1, {styleName:this, owner:this});
		check.addEventListener("click", this);
		size();
	}

	// note that setSize is implemented by UIComponent and calls size(), after setting
	// __width and __height
	function size(Void) : Void
	{
		check.setSize(20, __height);
		check._x = (__width-20)/2;
		check._y = (__height-16)/2;
	}

	function setValue(str:String, item:Object, sel:Boolean) : Void
	{
		check._visible = (item!=undefined);
		check.selected = item[getDataLabel()];
		check.enabled = true;

		//確認不要と確認済みはボックス非表示
		var NoCheck:String = listOwner.dataProvider.getItemAt(getCellIndex().itemIndex).checkflg
		var Checked:String = listOwner.dataProvider.getItemAt(getCellIndex().itemIndex).copeflg
		if( NoCheck == "不要" || Checked == "対応済み" ){
			check._visible = check.enabled = false;
			check.selected = true;
			listOwner.dataProvider.editField(getCellIndex().itemIndex, getDataLabel(), check.selected);

		}
//check._visible = true
	}

	function getPreferredHeight(Void) : Number
	{
		return 16;
	}

	function getPreferredWidth(Void) : Number
	{
		return 20;
	}

	function click()
	{
		listOwner.dataProvider.editField(getCellIndex().itemIndex, getDataLabel(), check.selected);
		listOwner._parent.MailPackage.SetReaction(getCellIndex().itemIndex,check.selected);
	}

}
