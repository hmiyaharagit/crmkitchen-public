﻿import mx.core.UIComponent

class SDetailCellRenderer extends UIComponent
{

	var mcObj : MovieClip;
	var listOwner : MovieClip;
	var getCellIndex : Function;
	var	getDataLabel : Function;
	
	function SDetailCellRenderer()
	{
	}

	function createChildren(Void) : Void
	{
		mcObj = createObject("EditBtn", "EditBtn", 1, {styleName:this, owner:this});
		mcObj.addEventListener("onRelease", this);
		mcObj.addEventListener("onRollOver", this);
		mcObj.addEventListener("onRollOut", this);
		size();
	}

	function size(Void) : Void
	{
		//mcObj.setSize(20, 20);
		mcObj._x = -3;
		mcObj._y = -1;
	}

	function setValue(str:String, item:Object, sel:Boolean) : Void
	{	
		mcObj._visible = (item!=undefined);
	}

	function getPreferredHeight(Void) : Number
	{
		return 16;
	}

	function getPreferredWidth(Void) : Number
	{
		return 20;
	}

	function onRelease()
	{
		var retObj = new Object();
		retObj.index = getCellIndex().itemIndex
		//listOwner.dataProvider.editField(getCellIndex().itemIndex, getDataLabel(), mcObj.selected);
		//trace("Called!!!  " + listOwner.dataProvider.getItemAt(retObj.index).mailtemplate_id );

		//ルートに作成済みのインスタンス内のメソッドをコールする
		//listOwner._parent.MailTemplate.SetTargetData(listOwner.dataProvider.getItemAt(retObj.index).mailtemplate_id);
		listOwner._parent.SpecsheetList.SetTargetData(retObj.index);
	}

	function onRollOver(){
		mcObj.onRollOver();
	}
	function onRollOut(){
		mcObj.onRollOut();
	}

}
