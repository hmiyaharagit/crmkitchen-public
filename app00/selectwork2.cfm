<cftry>
	<cfset wrk_id = ArrayNew(1)>

	<cfset wk_resort_code = arraynew(1)>
	<cfset ArrayAppend( wk_resort_code , 11)>

	<cfset date_id = ArrayNew(1)>
	<cfset ArrayAppend( date_id , "2009/05/01")>
	<cfset ArrayAppend( date_id , "2010/05/31")>

	<cfinvoke component = "cfc/cstotal_dataselectwork" method = "getAnswers"
		dcondtion=false
		selecteddays		=#date_id#
		arg_area			=#wrk_id#
		arg_cognition		=#wrk_id#
		arg_gender			=#wrk_id#
		arg_generation		=#wrk_id#
		arg_job				=#wrk_id#
		arg_purpose			=#wrk_id#
		arg_reservation		=#wrk_id#
		arg_roomtype		=#wrk_id#
		arg_spouse			=#wrk_id#
		arg_times			=#wrk_id#
		arg_together		=#wrk_id#
		arg_nights			=#wrk_id#
		arg_roomnumber		=#wrk_id#
		arg_category_id		=1
		arg_resort_code		=#wk_resort_code#
		arg_cross_id=3
		arg_group_id=0
	returnVariable	="retVal">

	<cfquery name="retQ" dbtype="query">
		select	 SAMPLES
				,ORDER_ID
				,EFFECTIVE_N
				,AVERAGE
				,RANK
				,TOP1
				,TOP2AND3
				,NAGETIVE
				,RESULT0_AVERAGE_TXT
				,QUESTIONNAIRE_ID
		from	retVal
		where	(QUESTIONNAIRE_ID =100000001
			or	 QUESTIONNAIRE_ID =100000003
			or	 QUESTIONNAIRE_ID =600000001
			or	 QUESTIONNAIRE_ID =700000001
			or	 QUESTIONNAIRE_ID =700004001
			or	 QUESTIONNAIRE_ID =700204001
			or	 QUESTIONNAIRE_ID =700011001
			or	 QUESTIONNAIRE_ID =700211001
				)

	</cfquery>
	<!--- 
	<cfdump var=#retQ#>
	 --->
	<!---  --->
	<cfinvoke component = "cfc/cstotal_dataselectwork" method = "getAnswers"
		dcondtion=false
		selecteddays		=#date_id#
		arg_area			=#wrk_id#
		arg_cognition		=#wrk_id#
		arg_gender			=#wrk_id#
		arg_generation		=#wrk_id#
		arg_job				=#wrk_id#
		arg_purpose			=#wrk_id#
		arg_reservation		=#wrk_id#
		arg_roomtype		=#wrk_id#
		arg_spouse			=#wrk_id#
		arg_times			=#wrk_id#
		arg_together		=#wrk_id#
		arg_nights			=#wrk_id#
		arg_roomnumber		=#wrk_id#
		arg_category_id		=2
		arg_resort_code		=#wk_resort_code#
		arg_cross_id=3
		arg_group_id=0
	returnVariable	="retVal2">

	<cfquery name="worksql" dbtype="query">
		select	 SAMPLES
				,ORDER_ID
				,EFFECTIVE_N
				,AVERAGE
				,RANK
				,TOP1
				,TOP2AND3
				,NAGETIVE
				,RESULT0_AVERAGE_TXT
				,QUESTIONNAIRE_ID
		from	retVal2
		where	(QUESTIONNAIRE_ID =800200005
			or	 QUESTIONNAIRE_ID =900100005
				)
		and		(ORDER_ID = 61101190502
			or	 ORDER_ID = 61101190503
			or	 ORDER_ID = 61101190504
			or	 ORDER_ID = 61101190505
			or	 ORDER_ID = 61101190506
			or	 ORDER_ID = 61101190507
			or	 ORDER_ID = 70000200502
			or	 ORDER_ID = 70000200503
			or	 ORDER_ID = 70000200504
			or	 ORDER_ID = 70000200505
			or	 ORDER_ID = 70000200506
			or	 ORDER_ID = 70000200507
				)
	</cfquery>
	<!--- 
	<cfdump var=#worksql#>
	 --->


	<cfquery name="d" dbtype="query">
		select * from retQ
		union all
		select * from worksql
		order by QUESTIONNAIRE_ID,ORDER_ID
	</cfquery>
	<cftable query = "d" border="1" colheaders HTMLTable>
		<cfcol header="QUESTIONNAIRE_ID" text  = "#QUESTIONNAIRE_ID#">
		<cfcol header="ORDER_ID" text  = "#ORDER_ID#">
		<cfcol header="SAMPLES" text  = "#SAMPLES#">
		<cfcol header="EFFECTIVE_N" text  = "#EFFECTIVE_N#">
		<cfcol header="AVERAGE" text  = "#AVERAGE#">
		<cfcol header="RANK" text  = "#RANK#">
		<cfcol header="TOP1" text  = "#TOP1#">
		<cfcol header="TOP2AND3" text  = "#TOP2AND3#">
		<cfcol header="NAGETIVE" text  = "#NAGETIVE#">
		<cfcol header="RESULT0_AVERAGE_TXT" text  = "#RESULT0_AVERAGE_TXT#">
	</cftable>

	<cfabort>
	<cfcatch>
		<cfdump var=#cfcatch#>
	</cfcatch>

</cftry>
