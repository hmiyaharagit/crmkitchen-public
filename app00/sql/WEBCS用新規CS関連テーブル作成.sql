/*----------------------------------------------------------
Drop Table
							2005/06/27 h.miyahara(NetFusion)
----------------------------------------------------------*/
if exists (select * from dbo.sysobjects where id = object_id(N'[HST_ANSWERS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [HST_ANSWERS]
if exists (select * from dbo.sysobjects where id = object_id(N'[HST_ANSWERSDETAILS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [HST_ANSWERSDETAILS]
if exists (select * from dbo.sysobjects where id = object_id(N'[MST_ANSWEROPTIONS]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [MST_ANSWEROPTIONS]
if exists (select * from dbo.sysobjects where id = object_id(N'[MST_QUESTIONNAIRE]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [MST_QUESTIONNAIRE]
if exists (select * from dbo.sysobjects where id = object_id(N'[MST_QUESTIONNAIREBLOCK]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [MST_QUESTIONNAIREBLOCK]
if exists (select * from dbo.sysobjects where id = object_id(N'[MST_QUESTIONNAIRECATEGORY]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [MST_QUESTIONNAIRECATEGORY]
if exists (select * from dbo.sysobjects where id = object_id(N'[MST_QUESTIONNAIRESHEET]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [MST_QUESTIONNAIRESHEET]
if exists (select * from dbo.sysobjects where id = object_id(N'[MST_SCENARIO]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [MST_SCENARIO]
if exists (select * from dbo.sysobjects where id = object_id(N'[OTH_SHEETCONFIGURATION]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [OTH_SHEETCONFIGURATION]

/*----------------------------------------------------------
回答基本情報登録テーブル登録
							2005/06/27 h.miyahara(NetFusion)
----------------------------------------------------------*/
CREATE TABLE [HST_ANSWERS] (
	[RESERVE_CODE] [bigint] NOT NULL ,
	[QUESTIONNAIRESHEET_ID] [smallint] NOT NULL ,
	[REGISTRATED] [varchar] (256) COLLATE Japanese_CI_AS NULL ,
	[ARRIVE_DATE] [varchar] (256) COLLATE Japanese_CI_AS NULL ,
	[DEPART_DATE] [varchar] (256) COLLATE Japanese_CI_AS NULL ,
	[AREA_CODE] [bigint] NULL ,
	[STATISTICS_VALUE_CODE] [bigint] NULL ,
	CONSTRAINT [PK_HST_ANSWERS] PRIMARY KEY  CLUSTERED 
	(
		[RESERVE_CODE],
		[QUESTIONNAIRESHEET_ID]
	)  ON [PRIMARY] 
) ON [PRIMARY]

/*----------------------------------------------------------
回答詳細情報登録テーブル登録
							2005/06/27 h.miyahara(NetFusion)
----------------------------------------------------------*/
CREATE TABLE [HST_ANSWERSDETAILS] (
	[RESERVE_CODE] [bigint] NOT NULL ,
	[QUESTIONNAIRESHEET_ID] [smallint] NOT NULL ,
	[QUESTIONNAIRE_ID] [smallint] NOT NULL ,
	[ANSWER] [varchar] (50) COLLATE Japanese_CI_AS NOT NULL ,
	[ANSWERTEXT] [text] COLLATE Japanese_CI_AS NULL ,
	[OTHERTEXT] [text] COLLATE Japanese_CI_AS NULL ,
	CONSTRAINT [PK_HST_ANSWERSDETAILS] PRIMARY KEY  CLUSTERED 
	(
		[RESERVE_CODE],
		[QUESTIONNAIRESHEET_ID],
		[QUESTIONNAIRE_ID],
		[ANSWER]
	)  ON [PRIMARY] 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

/*----------------------------------------------------------
回答候補値マスタ登録
							2005/06/27 h.miyahara(NetFusion)
----------------------------------------------------------*/
CREATE TABLE [MST_ANSWEROPTIONS] (
	[OPTIONS_ID] [int] IDENTITY (1, 1) NOT NULL ,
	[QUESTIONNAIRE_ID] [smallint] NOT NULL ,
	[OPTIONS_NO] [tinyint] NOT NULL ,
	[OPTIONS_LABEL] [varchar] (100) COLLATE Japanese_CI_AS NULL ,
	[OPTIONS_TYPE] [tinyint] NULL ,
	[ADDTEXT] [varchar] (100) COLLATE Japanese_CI_AS NULL ,
	[SCENARIO] [tinyint] NULL ,
	[RESORT_CODE] [bigint] NULL ,
	[ACTIVE_FLG] [tinyint] NOT NULL CONSTRAINT [DF_MST_ANSWEROPTIONS_ACTIVE_FLG] DEFAULT (1),
	[UPDATEDBY] [char] (20) COLLATE Japanese_CI_AS NULL ,
	[UPDATED] [datetime] NULL ,
	CONSTRAINT [PK_MST_ANSWEROPTIONS] PRIMARY KEY  CLUSTERED 
	(
		[OPTIONS_ID]
	)  ON [PRIMARY] ,
	CONSTRAINT [FK_MST_ANSWEROPTIONS_RTM_RESORT] FOREIGN KEY 
	(
		[RESORT_CODE]
	) REFERENCES [RTM_RESORT] (
		[RESORT_CODE]
	)
) ON [PRIMARY]


/*----------------------------------------------------------
設問ブロックマスタ登録
							2005/06/27 h.miyahara(NetFusion)
----------------------------------------------------------*/
CREATE TABLE [MST_QUESTIONNAIREBLOCK] (
	[QUESTIONNAIREBLOCK_ID] [tinyint] IDENTITY (1, 1) NOT NULL ,
	[QUESTIONNAIREBLOCK_NM] [varchar] (40) COLLATE Japanese_CI_AS NULL ,
	[LABELTEXT] [varchar] (400) COLLATE Japanese_CI_AS NULL ,
	[MEMO] [varchar] (50) COLLATE Japanese_CI_AS NULL ,
	[QUESTIONNAIRE_ID] [smallint] NULL ,
	CONSTRAINT [PK_MST_QUESTIONNAIREBLOCK] PRIMARY KEY  CLUSTERED 
	(
		[QUESTIONNAIREBLOCK_ID]
	)  ON [PRIMARY] 
) ON [PRIMARY]
GO

/*----------------------------------------------------------
設問カテゴリマスタ登録
							2005/06/27 h.miyahara(NetFusion)
----------------------------------------------------------*/
CREATE TABLE [MST_QUESTIONNAIRECATEGORY] (
	[QUESTIONNAIRECATEGORY_ID] [smallint] IDENTITY (1, 1) NOT NULL ,
	[QUESTIONNAIRECATEGORY_NM] [varchar] (100) COLLATE Japanese_CI_AS NULL ,
	[RESORT_CODE] [bigint] NOT NULL ,
	[ACTIVE_FLG] [tinyint] NOT NULL ,
	[UPDATEDBY] [varchar] (20) COLLATE Japanese_CI_AS NULL ,
	[UPDATED] [datetime] NULL ,
	CONSTRAINT [PK_MST_QUESTIONNAIRECATEGORY] PRIMARY KEY  CLUSTERED 
	(
		[QUESTIONNAIRECATEGORY_ID]
	)  ON [PRIMARY] ,
	CONSTRAINT [FK_MST_QUESTIONNAIRECATEGORY_RTM_RESORT] FOREIGN KEY 
	(
		[RESORT_CODE]
	) REFERENCES [RTM_RESORT] (
		[RESORT_CODE]
	)
) ON [PRIMARY]
GO

/*----------------------------------------------------------
設問マスタ登録
							2005/06/27 h.miyahara(NetFusion)
----------------------------------------------------------*/
CREATE TABLE [MST_QUESTIONNAIRE] (
	[QUESTIONNAIRE_ID] [smallint] IDENTITY (1, 1) NOT NULL ,
	[QUESTIONNAIRE_NM] [varchar] (400) COLLATE Japanese_CI_AS NULL ,
	[QUESTIONNAIRE_TYPE] [tinyint] NOT NULL ,
	[REQUISITE_FLG] [tinyint] NOT NULL ,
	[QUESTIONNAIRECATEGORY_ID] [smallint] NULL ,
	[RESORT_CODE] [bigint] NULL ,
	[ACTIVE_FLG] [tinyint] NOT NULL CONSTRAINT [DF_MST_QUESTIONNAIRE_ACTIVE_FLG] DEFAULT (1),
	[UPDATEDBY] [varchar] (20) COLLATE Japanese_CI_AS NULL ,
	[UPDATED] [datetime] NULL ,
	CONSTRAINT [PK_MST_QUESTIONNAIRE] PRIMARY KEY  CLUSTERED 
	(
		[QUESTIONNAIRE_ID]
	)  ON [PRIMARY] ,
	CONSTRAINT [FK_MST_QUESTIONNAIRE_MST_QUESTIONNAIRECATEGORY] FOREIGN KEY 
	(
		[QUESTIONNAIRECATEGORY_ID]
	) REFERENCES [MST_QUESTIONNAIRECATEGORY] (
		[QUESTIONNAIRECATEGORY_ID]
	),
	CONSTRAINT [FK_MST_QUESTIONNAIRE_RTM_RESORT] FOREIGN KEY 
	(
		[RESORT_CODE]
	) REFERENCES [RTM_RESORT] (
		[RESORT_CODE]
	)
) ON [PRIMARY]

/*----------------------------------------------------------
設問シートマスタ登録
							2005/06/27 h.miyahara(NetFusion)
----------------------------------------------------------*/
CREATE TABLE [MST_QUESTIONNAIRESHEET] (
	[QUESTIONNAIRESHEET_ID] [smallint] IDENTITY (1, 1) NOT NULL ,
	[QUESTIONNAIRESHEET_NM] [varchar] (40) COLLATE Japanese_CI_AS NULL ,
	[RESORT_CODE] [bigint] NOT NULL ,
	[ACTIVE_FLG] [tinyint] NOT NULL CONSTRAINT [DF_MST_QUESTIONNAIRESHEET_ACTIVE_FLG] DEFAULT (1),
	[UPDATEDBY] [varchar] (20) COLLATE Japanese_CI_AS NULL ,
	[UPDATED] [datetime] NULL ,
	CONSTRAINT [PK_MST_QUESTIONNAIRESHEET] PRIMARY KEY  CLUSTERED 
	(
		[QUESTIONNAIRESHEET_ID]
	)  ON [PRIMARY] ,
	CONSTRAINT [FK_MST_QUESTIONNAIRESHEET_RTM_RESORT] FOREIGN KEY 
	(
		[RESORT_CODE]
	) REFERENCES [RTM_RESORT] (
		[RESORT_CODE]
	)
) ON [PRIMARY]
GO

/*----------------------------------------------------------
シナリオマスタ登録
							2005/06/27 h.miyahara(NetFusion)
----------------------------------------------------------*/
CREATE TABLE [MST_SCENARIO] (
	[SCENARIO_ID] [tinyint] IDENTITY (1, 1) NOT NULL ,
	[SCENARIO_NM] [varchar] (100) COLLATE Japanese_CI_AS NULL ,
	[HEADERTEXT] [varchar] (256) COLLATE Japanese_CI_AS NULL ,
	[DEFAULT_FLG] [tinyint] NULL ,
	[STEP] [tinyint] NULL ,
	[VIEWSEQUENCE] [tinyint] NULL ,
	[ACTIVE_FLG] [tinyint] NOT NULL CONSTRAINT [DF_MST_SCENARIO_ACTIVE_FLG] DEFAULT (1),
	CONSTRAINT [PK_MST_SCENARIO] PRIMARY KEY  CLUSTERED 
	(
		[SCENARIO_ID]
	)  ON [PRIMARY] 
) ON [PRIMARY]
GO

/*----------------------------------------------------------
シート設定テーブル登録
							2005/06/27 h.miyahara(NetFusion)
----------------------------------------------------------*/
CREATE TABLE [OTH_SHEETCONFIGURATION] (
	[QUESTIONNAIRESHEET_ID] [smallint] NOT NULL ,
	[QUESTIONNAIRE_ID] [smallint] NOT NULL ,
	[VIEWSEQUENCE] [tinyint] NULL ,
	[VIEWTYPE] [tinyint] NULL ,
	[SCENARIO] [tinyint] NOT NULL ,
	[RESORT_CODE] [bigint] NOT NULL ,
	[QUESTIONNAIRE_TEXT] [varchar] (400) COLLATE Japanese_CI_AS NULL ,
	[LEADMESSAGE] [varchar] (256) COLLATE Japanese_CI_AS NULL ,
	[ACTIVE_FLG] [tinyint] NOT NULL CONSTRAINT [DF_OTH_SHEETCONFIGURATION_ACTIVE_FLG] DEFAULT (1),
	[UPDATEDBY] [varchar] (20) COLLATE Japanese_CI_AS NULL ,
	[UPDATED] [datetime] NULL ,
	[SETTYPE] [tinyint] NULL ,
	[ANSWERTYPE] [tinyint] NULL ,
	[MEMO] [text] COLLATE Japanese_CI_AS NULL ,
	[OWNER] [varchar] (35) COLLATE Japanese_CI_AS NOT NULL ,
	CONSTRAINT [PK_OTH_SHEETCONFIGURATION] PRIMARY KEY  CLUSTERED 
	(
		[QUESTIONNAIRESHEET_ID],
		[QUESTIONNAIRE_ID],
		[OWNER]
	)  ON [PRIMARY] 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
