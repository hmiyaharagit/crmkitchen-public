if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[h_logintrack]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[h_logintrack]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[m_loginuser]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[m_loginuser]
GO

CREATE TABLE [dbo].[h_logintrack] (
	[login_id] [varchar] (12) COLLATE Japanese_CI_AS NOT NULL ,
	[login_dt] [datetime] NOT NULL ,
	[login_cnt] [int] NULL 
) ON [PRIMARY]
GO

CREATE TABLE [dbo].[m_loginuser] (
	[login_id] [varchar] (12) COLLATE Japanese_CI_AS NOT NULL ,
	[login_pw] [varchar] (12) COLLATE Japanese_CI_AS NULL ,
	[login_nm] [varchar] (48) COLLATE Japanese_CI_AS NULL ,
	[login_dv] [smallint] NULL 
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[h_logintrack] WITH NOCHECK ADD 
	CONSTRAINT [PK_h_logintrack] PRIMARY KEY  CLUSTERED 
	(
		[login_id],
		[login_dt]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[m_loginuser] WITH NOCHECK ADD 
	CONSTRAINT [PK_m_loginuser] PRIMARY KEY  CLUSTERED 
	(
		[login_id]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[h_logintrack] ADD 
	CONSTRAINT [DF_h_logintrack_login_cnt] DEFAULT (1) FOR [login_cnt]
GO

