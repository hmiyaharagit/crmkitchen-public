/*----------------------------------------------------------
Drop Table
							2005/06/27 h.miyahara(NetFusion)
----------------------------------------------------------*/
if exists (select * from dbo.sysobjects where id = object_id(N'[HST_DELIVERINFORMATION]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [HST_DELIVERINFORMATION]
if exists (select * from dbo.sysobjects where id = object_id(N'[MST_USER]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [MST_USER]
if exists (select * from dbo.sysobjects where id = object_id(N'[MST_MAILTEMPLATE]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [MST_MAILTEMPLATE]
if exists (select * from dbo.sysobjects where id = object_id(N'[hst_valueinformation]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [hst_valueinformation]
if exists (select * from dbo.sysobjects where id = object_id(N'[MST_SUMMARY]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [MST_SUMMARY]
if exists (select * from dbo.sysobjects where id = object_id(N'[RTT_RESERVE]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [RTT_RESERVE]
if exists (select * from dbo.sysobjects where id = object_id(N'[RTM_CUSTOMER]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [RTM_CUSTOMER]
if exists (select * from dbo.sysobjects where id = object_id(N'[RTM_CANCEL]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [RTM_CANCEL]
if exists (select * from dbo.sysobjects where id = object_id(N'[RTM_GENDER]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [RTM_GENDER]
if exists (select * from dbo.sysobjects where id = object_id(N'[RTM_RESORT]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [RTM_RESORT]
if exists (select * from dbo.sysobjects where id = object_id(N'[RTM_AREA]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [RTM_AREA]

/*----------------------------------------------------------
エリアマスタ登録
							2005/06/27 h.miyahara(NetFusion)
----------------------------------------------------------*/
CREATE TABLE [RTM_AREA] (
	[AREA_CODE] [bigint] NOT NULL ,
	[NAME] [varchar] (256) COLLATE Japanese_CI_AS NOT NULL ,
	CONSTRAINT [PK_RTM_AREA_CODE] PRIMARY KEY  CLUSTERED 
	(
		[AREA_CODE]
	)  ON [PRIMARY] 
) ON [PRIMARY]

/*----------------------------------------------------------
リゾートマスタ登録
							2005/06/27 h.miyahara(NetFusion)
----------------------------------------------------------*/
CREATE TABLE [RTM_RESORT] (
	[RESORT_CODE] [bigint] NOT NULL ,
	[NAME] [varchar] (256) COLLATE Japanese_CI_AS NOT NULL ,
	CONSTRAINT [PK_RTM_RESORT] PRIMARY KEY  CLUSTERED 
	(
		[RESORT_CODE]
	)  ON [PRIMARY] 
) ON [PRIMARY]

/*----------------------------------------------------------
性別マスタ登録
							2005/06/27 h.miyahara(NetFusion)
----------------------------------------------------------*/
CREATE TABLE [RTM_GENDER] (
	[GENDER_CODE] [bigint] NOT NULL ,
	[NAME] [varchar] (256) COLLATE Japanese_CI_AS NOT NULL ,
	CONSTRAINT [PK_RTM_GENDER] PRIMARY KEY  CLUSTERED 
	(
		[GENDER_CODE]
	)  ON [PRIMARY] 
) ON [PRIMARY]

/*----------------------------------------------------------
キャンセルマスタ登録
							2005/06/27 h.miyahara(NetFusion)
----------------------------------------------------------*/
CREATE TABLE [RTM_CANCEL] (
	[CANCEL_CODE] [bigint] NOT NULL ,
	[NAME] [varchar] (256) COLLATE Japanese_CI_AS NOT NULL ,
	CONSTRAINT [PK_RTM_CANCEL] PRIMARY KEY  CLUSTERED 
	(
		[CANCEL_CODE]
	)  ON [PRIMARY] 
) ON [PRIMARY]

/*----------------------------------------------------------
顧客マスタ登録
							2005/06/27 h.miyahara(NetFusion)
----------------------------------------------------------*/
CREATE TABLE [RTM_CUSTOMER] (
	[CUSTOMER_CODE] [bigint] IDENTITY (10000000, 1) NOT NULL ,
	[NAME] [varchar] (256) COLLATE Japanese_CI_AS NOT NULL ,
	[NAME_KANA] [varchar] (256) COLLATE Japanese_CI_AS NOT NULL ,
	[GENDER_CODE] [bigint] NOT NULL ,
	[BIRTH_DATE] [varchar] (256) COLLATE Japanese_CI_AS NULL ,
	[AREA_CODE] [bigint] NOT NULL ,
	[ADDRESS_ZIPCODE] [varchar] (256) COLLATE Japanese_CI_AS NULL ,
	[ADDRESS_SEGMENT_1] [varchar] (256) COLLATE Japanese_CI_AS NULL ,
	[ADDRESS_SEGMENT_2] [varchar] (256) COLLATE Japanese_CI_AS NULL ,
	[ADDRESS_SEGMENT_3] [varchar] (256) COLLATE Japanese_CI_AS NULL ,
	[BELONG_SEGMENT_1] [varchar] (256) COLLATE Japanese_CI_AS NULL ,
	[BELONG_SEGMENT_2] [varchar] (256) COLLATE Japanese_CI_AS NULL ,
	[BELONG_SEGMENT_3] [varchar] (256) COLLATE Japanese_CI_AS NULL ,
	[PHONE_NUMBER_1] [varchar] (256) COLLATE Japanese_CI_AS NULL ,
	[PHONE_NUMBER_2] [varchar] (256) COLLATE Japanese_CI_AS NULL ,
	[MAIL_ADDRESS_1] [varchar] (256) COLLATE Japanese_CI_AS NULL ,
	[MAIL_ADDRESS_2] [varchar] (256) COLLATE Japanese_CI_AS NULL ,
	[POST_PERMISSION_CODE] [bigint] NULL ,
	[MAIL_PERMISSION_CODE] [bigint] NULL ,
	[INFORMATION] [varchar] (8000) COLLATE Japanese_CI_AS NULL ,
	[ACTIVE_FLG] [tinyint] NOT NULL CONSTRAINT [DF_RTM_CUSTOMER_ACTIVE_FLG] DEFAULT (1),
	CONSTRAINT [PK_RTM_CUSTOMER] PRIMARY KEY  CLUSTERED 
	(
		[CUSTOMER_CODE]
	)  ON [PRIMARY] ,
	CONSTRAINT [FK_RTM_CUSTOMER_RTM_AREA] FOREIGN KEY 
	(
		[AREA_CODE]
	) REFERENCES [RTM_AREA] (
		[AREA_CODE]
	),
	CONSTRAINT [FK_RTM_CUSTOMER_RTM_GENDER] FOREIGN KEY 
	(
		[GENDER_CODE]
	) REFERENCES [RTM_GENDER] (
		[GENDER_CODE]
	)
) ON [PRIMARY]

/*----------------------------------------------------------
予約マスタ登録
							2005/06/27 h.miyahara(NetFusion)
----------------------------------------------------------*/
CREATE TABLE [RTT_RESERVE] (
	[RESERVE_CODE] [bigint] NOT NULL ,
	[CUSTOMER_CODE] [bigint] NOT NULL ,
	[ARRIVE_DATE] [varchar] (256) COLLATE Japanese_CI_AS NOT NULL ,
	[DEPART_DATE] [varchar] (256) COLLATE Japanese_CI_AS NOT NULL ,
	[CANCEL_CODE] [bigint] NOT NULL ,
	[INFORMATION] [varchar] (7000) COLLATE Japanese_CI_AS NOT NULL ,
	CONSTRAINT [PK_RTT_RESERVE] PRIMARY KEY  CLUSTERED 
	(
		[RESERVE_CODE]
	)  ON [PRIMARY] 
) ON [PRIMARY]

/*----------------------------------------------------------
サマリマスタ登録
							2005/06/27 h.miyahara(NetFusion)
----------------------------------------------------------*/
CREATE TABLE [MST_SUMMARY] (
	[SUMMARY_ID] [smallint] IDENTITY (1, 1) NOT NULL ,
	[SUMMARY_NM] [varchar] (40) COLLATE Japanese_CI_AS NULL ,
	[SUMMARY_IP] [varchar] (40) COLLATE Japanese_CI_AS NULL ,
	[BASICINFO] [varchar] (100) COLLATE Japanese_CI_AS NULL ,
	[IMPORTANTINFO] [varchar] (40) COLLATE Japanese_CI_AS NULL ,
	[SATISFIEDINFO] [varchar] (40) COLLATE Japanese_CI_AS NULL ,
	[STAYINFO] [varchar] (40) COLLATE Japanese_CI_AS NULL ,
	[CSINFO] [varchar] (40) COLLATE Japanese_CI_AS NULL ,
	[OWNER] [varchar] (35) COLLATE Japanese_CI_AS NULL ,
	[RESORT_CODE] [bigint] NOT NULL ,
	[ACTIVE_FLG] [tinyint] NOT NULL CONSTRAINT [DF_MST_SUMMARY_ACTIVE_FLG] DEFAULT (1),
	[UPDATEDBY] [varchar] (20) COLLATE Japanese_CI_AS NULL ,
	[UPDATED] [datetime] NULL ,
	CONSTRAINT [PK_MST_SUMMARY] PRIMARY KEY  CLUSTERED 
	(
		[SUMMARY_ID]
	)  ON [PRIMARY] ,
	CONSTRAINT [FK_MST_SUMMARY_RTM_RESORT] FOREIGN KEY 
	(
		[RESORT_CODE]
	) REFERENCES [RTM_RESORT] (
		[RESORT_CODE]
	)
) ON [PRIMARY]

/*----------------------------------------------------------
ValueInformationテーブル登録
							2005/06/27 h.miyahara(NetFusion)
----------------------------------------------------------*/
CREATE TABLE [hst_valueinformation] (
	[reserve_code] [bigint] NOT NULL ,
	[valueinfo] [text] COLLATE Japanese_CI_AS NULL ,
	[lastdata] [text] COLLATE Japanese_CI_AS NULL ,
	[checkout] [tinyint] NOT NULL CONSTRAINT [DF_hst_valueinformation_checkout] DEFAULT (0),
	[active_flg] [tinyint] NOT NULL CONSTRAINT [DF_hst_valueinformation_active_flg] DEFAULT (1),
	[updatedby] [varchar] (20) COLLATE Japanese_CI_AS NULL ,
	[updated] [datetime] NULL ,
	CONSTRAINT [PK_hst_valueinformation] PRIMARY KEY  CLUSTERED 
	(
		[reserve_code]
	)  ON [PRIMARY] 
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

/*----------------------------------------------------------
メールテンプレートマスタ登録
							2005/06/27 h.miyahara(NetFusion)
----------------------------------------------------------*/
CREATE TABLE [MST_MAILTEMPLATE] (
	[MAILTEMPLATE_ID] [smallint] IDENTITY (1, 1) NOT NULL ,
	[MAIL_SUBJECT] [varchar] (100) COLLATE Japanese_CI_AS NULL ,
	[MAIL_BODY] [text] COLLATE Japanese_CI_AS NULL ,
	[MAIL_FROM] [varchar] (50) COLLATE Japanese_CI_AS NOT NULL ,
	[FROMADDRESS] [varchar] (50) COLLATE Japanese_CI_AS NOT NULL ,
	[BCCADDRESS] [varchar] (50) COLLATE Japanese_CI_AS NULL ,
	[TESTADDRESS] [varchar] (50) COLLATE Japanese_CI_AS NULL ,
	[RESORT_CODE] [bigint] NULL ,
	[ACTIVE_FLG] [tinyint] NOT NULL CONSTRAINT [DF_MST_MAILTEMPLATE_ACTIVE_FLG] DEFAULT (1),
	[UPDATEDBY] [varchar] (20) COLLATE Japanese_CI_AS NULL ,
	[UPDATED] [datetime] NULL ,
	CONSTRAINT [PK_MST_MAILTEMPLATE] PRIMARY KEY  CLUSTERED 
	(
		[MAILTEMPLATE_ID]
	)  ON [PRIMARY] ,
	CONSTRAINT [FK_MST_MAILTEMPLATE_RTM_RESORT] FOREIGN KEY 
	(
		[RESORT_CODE]
	) REFERENCES [RTM_RESORT] (
		[RESORT_CODE]
	)
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

/*----------------------------------------------------------
ユーザーマスタ登録
							2005/06/27 h.miyahara(NetFusion)
----------------------------------------------------------*/
CREATE TABLE [MST_USER] (
	[USER_ID] [smallint] IDENTITY (1, 1) NOT NULL ,
	[USER_NM] [varchar] (40) COLLATE Japanese_CI_AS NULL ,
	[USER_IP] [varchar] (20) COLLATE Japanese_CI_AS NOT NULL ,
	[ROLE_FLG] [tinyint] NULL ,
	[RESORT_CODE] [bigint] NULL ,
	[COMMENT] [varchar] (4000) COLLATE Japanese_CI_AS NULL ,
	[ACTIVE_FLG] [tinyint] NOT NULL CONSTRAINT [DF_MST_USER_ACTIVE_FLG] DEFAULT (1),
	[UPDATEDBY] [varchar] (20) COLLATE Japanese_CI_AS NULL ,
	[UPDATED] [datetime] NULL ,
	CONSTRAINT [PK_MST_USER] PRIMARY KEY  CLUSTERED 
	(
		[USER_ID]
	)  ON [PRIMARY] ,
	CONSTRAINT [FK_MST_USER_RTM_RESORT] FOREIGN KEY 
	(
		[RESORT_CODE]
	) REFERENCES [RTM_RESORT] (
		[RESORT_CODE]
	)
) ON [PRIMARY]

/*----------------------------------------------------------
メール送信履歴テーブル登録
							2005/06/27 h.miyahara(NetFusion)
----------------------------------------------------------*/
CREATE TABLE [HST_DELIVERINFORMATION] (
	[RESERVE_CODE] [bigint] NULL ,
	[DELIVERD_DATE] [datetime] NULL ,
	[COPE_FLG] [tinyint] NULL 
) ON [PRIMARY]
