/*------------------------------------------------
指定期間宿泊のリピーターリスト取得ストアドプロシージャ
			2006/04/27 h.miyahara(NetFusion) MOD
------------------------------------------------*/
CREATE PROCEDURE [dbo].[sp_repeatcustomer]
	-- パラメータ設定
	@dStartDate varchar(128),
	@dEndDate varchar(128) 
	AS

	-- 一時テーブル作成
	create table #tmpcustomer (
		customernumber varchar(32),
		referencenumber varchar(128),
		customername  varchar(128),
		repeatcount int
	)

	-- DECLAREステートメントにて変数宣言
	DECLARE @customernumber varchar(32)
	DECLARE @referencenumber varchar(128)
	DECLARE @customername varchar(128)
	DECLARE @repeatcount int

	-- カーソル宣言
	DECLARE customer_cursor SCROLL CURSOR FOR
		SELECT
			r.customernumber,
			r.referencenumber,
			c.name
		FROM
			rtm_customer as c,
			rtt_reserve as r
		WHERE
			r.customernumber = c.customernumber
		AND
			r.depart_date between @dStartDate and @dEndDate

	--OPEN ステートメントを使用して SELECT ステートメントを実行し、カーソルを作成
	OPEN customer_cursor

	--カーソルから行を取得
	FETCH NEXT FROM customer_cursor
	INTO @customernumber,@referencenumber,@customername

	--@@FETCH_STATUSの確認
	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		
		set @repeatcount = (select count(referencenumber) from rtt_reserve where customernumber = @customernumber)
		
		--一時テーブルに格納
		insert into #tmpcustomer(
			customernumber,
			referencenumber,
			customername,
			repeatcount
		)values(
			@customernumber,
			@referencenumber,
			@customername,
			@repeatcount
		)

		FETCH NEXT FROM customer_cursor
		INTO @customernumber,@referencenumber,@customername
	END

	--カーソルの使用を終了
	CLOSE customer_cursor

	--カーソル参照を削除
	DEALLOCATE customer_cursor

	--一時テーブルから社員名一覧表示
	select * from #tmpcustomer
	--return
GO
