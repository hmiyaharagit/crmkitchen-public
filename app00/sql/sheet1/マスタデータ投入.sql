/*----------------------------------------------------------
RTM_RESORT
							2005/10/07 h.miyahara(NetFusion)
----------------------------------------------------------*/
insert into [dbo].[RTM_RESORT](RESORT_CODE,name)values(10,'磐梯山温泉ホテル')

/*----------------------------------------------------------
RTM_AREA
							2005/10/07 h.miyahara(NetFusion)
----------------------------------------------------------*/
insert into [dbo].[RTM_AREA](area_code,name)values(1,'東北地方')
insert into [dbo].[RTM_AREA](area_code,name)values(2,'首都圏（東京・埼玉・神奈川・千葉）')
insert into [dbo].[RTM_AREA](area_code,name)values(3,'関東（栃木・茨城・山梨）')
insert into [dbo].[RTM_AREA](area_code,name)values(4,'中部・東海')
insert into [dbo].[RTM_AREA](area_code,name)values(5,'関西・近畿')
insert into [dbo].[RTM_AREA](area_code,name)values(6,'中国・四国・九州・沖縄')
insert into [dbo].[RTM_AREA](area_code,name)values(99,'その他')

/*----------------------------------------------------------
RTM_CANCEL
							2005/10/07 h.miyahara(NetFusion)
----------------------------------------------------------*/
insert into [dbo].[RTM_CANCEL](cancel_code,name)values(100000000000,'開催')
insert into [dbo].[RTM_CANCEL](cancel_code,name)values(100000000001,'キャンセル')

/*----------------------------------------------------------
RTM_GENDER
							2005/10/07 h.miyahara(NetFusion)
----------------------------------------------------------*/
insert into [dbo].[RTM_GENDER](gender_code,name)values(100000000000,'不明')
insert into [dbo].[RTM_GENDER](gender_code,name)values(100000000001,'男性')
insert into [dbo].[RTM_GENDER](gender_code,name)values(100000000002,'女性')

/*----------------------------------------------------------
MST_MAILTEMPLATE
							2005/10/07 h.miyahara(NetFusion)
----------------------------------------------------------*/
insert into [dbo].[MST_MAILTEMPLATE](MAIL_SUBJECT,MAIL_BODY,MAIL_FROM,FROMADDRESS,RESORT_CODE,ACTIVE_FLG)values('サンプルメール','こちらはサンプルデータです。

アンケートサイトのアドレスを表示させる場合は、
$WEBCS_URL$$WEBCS_CD$

このキーワードを本文内に埋めてください。
また、お客様の名前を表示させる場合は、
$CUSTOMER_NAME$様

このキーワードを本文内に埋めてください。','磐梯山温泉ホテル','test@nftest.jp',10,1)

/*----------------------------------------------------------
MST_SUMMARY
							2005/10/07 h.miyahara(NetFusion)
----------------------------------------------------------*/
insert into [dbo].[MST_SUMMARY](SUMMARY_NM,BASICINFO,IMPORTANTINFO,SATISFIEDINFO,STAYINFO,CSINFO,OWNER,RESORT_CODE,ACTIVE_FLG)values(
'全表示','1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16','1','1','1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31','1,2','F9CC3BE2-3472-9F6F-6CB3239010C14712',10,1)

/*----------------------------------------------------------
MST_USER 開発環境用ユーザー
							2005/10/07 h.miyahara(NetFusion)
----------------------------------------------------------*/
insert into mst_user(USER_IP,RESORT_CODE,ROLE_FLG)values('127.0.0.1',10,1)