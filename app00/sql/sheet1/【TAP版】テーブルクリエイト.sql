/****** オブジェクト :  テーブル [dbo].[hst_answers]    スクリプト日付 : 2007/02/06 15:09:47 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[hst_answers]') and objectproperty(id, N'isusertable') = 1)
drop table [dbo].[hst_answers]
go

/****** オブジェクト :  テーブル [dbo].[hst_answersdetails]    スクリプト日付 : 2007/02/06 15:09:47 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[hst_answersdetails]') and objectproperty(id, N'isusertable') = 1)
drop table [dbo].[hst_answersdetails]
go

/****** オブジェクト :  テーブル [dbo].[hst_deliverinformation]    スクリプト日付 : 2007/02/06 15:09:47 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[hst_deliverinformation]') and objectproperty(id, N'isusertable') = 1)
drop table [dbo].[hst_deliverinformation]
go

/****** オブジェクト :  テーブル [dbo].[hst_valueinformation]    スクリプト日付 : 2007/02/06 15:09:47 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[hst_valueinformation]') and objectproperty(id, N'isusertable') = 1)
drop table [dbo].[hst_valueinformation]
go

/****** オブジェクト :  テーブル [dbo].[mst_answeroptions]    スクリプト日付 : 2007/02/06 15:09:47 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[mst_answeroptions]') and objectproperty(id, N'isusertable') = 1)
drop table [dbo].[mst_answeroptions]
go

/****** オブジェクト :  テーブル [dbo].[mst_confirm]    スクリプト日付 : 2007/02/06 15:09:47 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[mst_confirm]') and objectproperty(id, N'isusertable') = 1)
drop table [dbo].[mst_confirm]
go

/****** オブジェクト :  テーブル [dbo].[mst_control]    スクリプト日付 : 2007/02/06 15:09:47 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[mst_control]') and objectproperty(id, N'isusertable') = 1)
drop table [dbo].[mst_control]
go

/****** オブジェクト :  テーブル [dbo].[mst_mailtemplate]    スクリプト日付 : 2007/02/06 15:09:47 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[mst_mailtemplate]') and objectproperty(id, N'isusertable') = 1)
drop table [dbo].[mst_mailtemplate]
go

/****** オブジェクト :  テーブル [dbo].[mst_questionnaire]    スクリプト日付 : 2007/02/06 15:09:47 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[mst_questionnaire]') and objectproperty(id, N'isusertable') = 1)
drop table [dbo].[mst_questionnaire]
go

/****** オブジェクト :  テーブル [dbo].[mst_questionnaireblock]    スクリプト日付 : 2007/02/06 15:09:47 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[mst_questionnaireblock]') and objectproperty(id, N'isusertable') = 1)
drop table [dbo].[mst_questionnaireblock]
go

/****** オブジェクト :  テーブル [dbo].[mst_questionnairecategory]    スクリプト日付 : 2007/02/06 15:09:47 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[mst_questionnairecategory]') and objectproperty(id, N'isusertable') = 1)
drop table [dbo].[mst_questionnairecategory]
go

/****** オブジェクト :  テーブル [dbo].[mst_questionnairesheet]    スクリプト日付 : 2007/02/06 15:09:47 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[mst_questionnairesheet]') and objectproperty(id, N'isusertable') = 1)
drop table [dbo].[mst_questionnairesheet]
go

/****** オブジェクト :  テーブル [dbo].[mst_room]    スクリプト日付 : 2007/02/06 15:09:47 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[mst_room]') and objectproperty(id, N'isusertable') = 1)
drop table [dbo].[mst_room]
go

/****** オブジェクト :  テーブル [dbo].[mst_scenario]    スクリプト日付 : 2007/02/06 15:09:47 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[mst_scenario]') and objectproperty(id, N'isusertable') = 1)
drop table [dbo].[mst_scenario]
go

/****** オブジェクト :  テーブル [dbo].[mst_summary]    スクリプト日付 : 2007/02/06 15:09:47 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[mst_summary]') and objectproperty(id, N'isusertable') = 1)
drop table [dbo].[mst_summary]
go

/****** オブジェクト :  テーブル [dbo].[mst_user]    スクリプト日付 : 2007/02/06 15:09:47 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[mst_user]') and objectproperty(id, N'isusertable') = 1)
drop table [dbo].[mst_user]
go

/****** オブジェクト :  テーブル [dbo].[oth_scenarioconfiguration]    スクリプト日付 : 2007/02/06 15:09:47 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[oth_scenarioconfiguration]') and objectproperty(id, N'isusertable') = 1)
drop table [dbo].[oth_scenarioconfiguration]
go

/****** オブジェクト :  テーブル [dbo].[oth_sheetconfiguration]    スクリプト日付 : 2007/02/06 15:09:47 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[oth_sheetconfiguration]') and objectproperty(id, N'isusertable') = 1)
drop table [dbo].[oth_sheetconfiguration]
go

/****** オブジェクト :  テーブル [dbo].[rtm_area]    スクリプト日付 : 2007/02/06 15:09:47 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[rtm_area]') and objectproperty(id, N'isusertable') = 1)
drop table [dbo].[rtm_area]
go

/****** オブジェクト :  テーブル [dbo].[rtm_cancel]    スクリプト日付 : 2007/02/06 15:09:47 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[rtm_cancel]') and objectproperty(id, N'isusertable') = 1)
drop table [dbo].[rtm_cancel]
go

/****** オブジェクト :  テーブル [dbo].[rtm_customer]    スクリプト日付 : 2007/02/06 15:09:47 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[rtm_customer]') and objectproperty(id, N'isusertable') = 1)
drop table [dbo].[rtm_customer]
go

/****** オブジェクト :  テーブル [dbo].[rtm_gender]    スクリプト日付 : 2007/02/06 15:09:47 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[rtm_gender]') and objectproperty(id, N'isusertable') = 1)
drop table [dbo].[rtm_gender]
go

/****** オブジェクト :  テーブル [dbo].[rtm_resort]    スクリプト日付 : 2007/02/06 15:09:47 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[rtm_resort]') and objectproperty(id, N'isusertable') = 1)
drop table [dbo].[rtm_resort]
go

/****** オブジェクト :  テーブル [dbo].[rtt_marketing]    スクリプト日付 : 2007/02/06 15:09:47 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[rtt_marketing]') and objectproperty(id, N'isusertable') = 1)
drop table [dbo].[rtt_marketing]
go

/****** オブジェクト :  テーブル [dbo].[rtt_reserve]    スクリプト日付 : 2007/02/06 15:09:47 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[rtt_reserve]') and objectproperty(id, N'isusertable') = 1)
drop table [dbo].[rtt_reserve]
go

/****** オブジェクト :  テーブル [dbo].[rtt_shopresult]    スクリプト日付 : 2007/02/06 15:09:47 ******/
if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[rtt_shopresult]') and objectproperty(id, N'isusertable') = 1)
drop table [dbo].[rtt_shopresult]
go

/****** オブジェクト :  テーブル [dbo].[hst_answers]    スクリプト日付 : 2007/02/06 15:09:52 ******/
create table [dbo].[hst_answers] (
	[referencenumber] [varchar] (128) collate japanese_ci_as not null ,
	[questionnairesheet_id] [smallint] not null ,
	[reserve_code] [bigint] null ,
	[registrated] [varchar] (256) collate japanese_ci_as null ,
	[arrive_date] [varchar] (256) collate japanese_ci_as null ,
	[depart_date] [varchar] (256) collate japanese_ci_as null ,
	[area_code] [bigint] null ,
	[statistics_value_code] [bigint] null ,
	[roomnumber] [bigint] null ,
	[answermethod] [tinyint] null 
) on [primary]
go

/****** オブジェクト :  テーブル [dbo].[hst_answersdetails]    スクリプト日付 : 2007/02/06 15:09:53 ******/
create table [dbo].[hst_answersdetails] (
	[referencenumber] [varchar] (128) collate japanese_ci_as not null ,
	[questionnairesheet_id] [smallint] not null ,
	[questionnaire_id] [smallint] not null ,
	[answer] [varchar] (50) collate japanese_ci_as not null ,
	[reserve_code] [bigint] null ,
	[answertext] [text] collate japanese_ci_as null ,
	[othertext] [text] collate japanese_ci_as null 
) on [primary] textimage_on [primary]
go

/****** オブジェクト :  テーブル [dbo].[hst_deliverinformation]    スクリプト日付 : 2007/02/06 15:09:53 ******/
create table [dbo].[hst_deliverinformation] (
	[referencenumber] [varchar] (128) collate japanese_ci_as null ,
	[deliverd_date] [datetime] null ,
	[cope_flg] [tinyint] null 
) on [primary]
go

/****** オブジェクト :  テーブル [dbo].[hst_valueinformation]    スクリプト日付 : 2007/02/06 15:09:54 ******/
create table [dbo].[hst_valueinformation] (
	[reserve_code] [bigint] not null ,
	[room_code] [bigint] not null ,
	[valueinfo] [text] collate japanese_ci_as null ,
	[lastdata] [text] collate japanese_ci_as null ,
	[checkout] [tinyint] not null ,
	[active_flg] [tinyint] not null ,
	[updatedby] [varchar] (20) collate japanese_ci_as null ,
	[updated] [datetime] null 
) on [primary] textimage_on [primary]
go

/****** オブジェクト :  テーブル [dbo].[mst_answeroptions]    スクリプト日付 : 2007/02/06 15:09:54 ******/
create table [dbo].[mst_answeroptions] (
	[questionnaire_id] [smallint] not null ,
	[options_no] [tinyint] not null ,
	[options_label] [varchar] (100) collate japanese_ci_as null ,
	[options_type] [tinyint] null ,
	[addtext] [varchar] (100) collate japanese_ci_as null ,
	[scenario] [tinyint] null ,
	[resort_code] [bigint] null ,
	[viewsequence] [tinyint] null ,
	[active_flg] [tinyint] not null ,
	[updatedby] [char] (20) collate japanese_ci_as null ,
	[updated] [datetime] null 
) on [primary]
go

/****** オブジェクト :  テーブル [dbo].[mst_confirm]    スクリプト日付 : 2007/02/06 15:09:54 ******/
create table [dbo].[mst_confirm] (
	[questionnaire_id] [smallint] not null ,
	[confirm_q] [smallint] not null ,
	[options_no] [tinyint] not null 
) on [primary]
go

/****** オブジェクト :  テーブル [dbo].[mst_control]    スクリプト日付 : 2007/02/06 15:09:54 ******/
create table [dbo].[mst_control] (
	[controldiv_id] [bigint] not null ,
	[control_id] [varchar] (128) collate japanese_ci_as not null ,
	[controldiv_nm] [varchar] (128) collate japanese_ci_as null ,
	[control_nm] [varchar] (128) collate japanese_ci_as null ,
	[seq] [int] null ,
	[active_flg] [tinyint] not null 
) on [primary]
go

/****** オブジェクト :  テーブル [dbo].[mst_mailtemplate]    スクリプト日付 : 2007/02/06 15:09:54 ******/
create table [dbo].[mst_mailtemplate] (
	[mailtemplate_id] [smallint] identity (1, 1) not null ,
	[mail_subject] [varchar] (100) collate japanese_ci_as null ,
	[mail_body] [text] collate japanese_ci_as null ,
	[mail_from] [varchar] (50) collate japanese_ci_as not null ,
	[fromaddress] [varchar] (50) collate japanese_ci_as not null ,
	[bccaddress] [varchar] (50) collate japanese_ci_as null ,
	[testaddress] [varchar] (50) collate japanese_ci_as null ,
	[resort_code] [bigint] null ,
	[active_flg] [tinyint] not null ,
	[updatedby] [varchar] (20) collate japanese_ci_as null ,
	[updated] [datetime] null 
) on [primary] textimage_on [primary]
go

/****** オブジェクト :  テーブル [dbo].[mst_questionnaire]    スクリプト日付 : 2007/02/06 15:09:54 ******/
create table [dbo].[mst_questionnaire] (
	[questionnaire_id] [smallint] identity (1, 1) not null ,
	[questionnaire_nm] [varchar] (400) collate japanese_ci_as null ,
	[questionnaire_type] [tinyint] not null ,
	[requisite_flg] [tinyint] not null ,
	[questionnairecategory_id] [smallint] null ,
	[resort_code] [bigint] null ,
	[active_flg] [tinyint] not null ,
	[updatedby] [varchar] (20) collate japanese_ci_as null ,
	[updated] [datetime] null 
) on [primary]
go

/****** オブジェクト :  テーブル [dbo].[mst_questionnaireblock]    スクリプト日付 : 2007/02/06 15:09:55 ******/
create table [dbo].[mst_questionnaireblock] (
	[questionnaireblock_id] [tinyint] identity (1, 1) not null ,
	[questionnaireblock_nm] [varchar] (40) collate japanese_ci_as null ,
	[labeltext] [varchar] (400) collate japanese_ci_as null ,
	[memo] [varchar] (50) collate japanese_ci_as null ,
	[questionnaire_id] [smallint] null 
) on [primary]
go

/****** オブジェクト :  テーブル [dbo].[mst_questionnairecategory]    スクリプト日付 : 2007/02/06 15:09:55 ******/
create table [dbo].[mst_questionnairecategory] (
	[questionnairecategory_id] [smallint] identity (1, 1) not null ,
	[questionnairecategory_nm] [varchar] (100) collate japanese_ci_as null ,
	[resort_code] [bigint] not null ,
	[active_flg] [tinyint] not null ,
	[updatedby] [varchar] (20) collate japanese_ci_as null ,
	[updated] [datetime] null 
) on [primary]
go

/****** オブジェクト :  テーブル [dbo].[mst_questionnairesheet]    スクリプト日付 : 2007/02/06 15:09:55 ******/
create table [dbo].[mst_questionnairesheet] (
	[questionnairesheet_id] [smallint] NOT NULL ,
	[questionnairesheet_nm] [varchar] (40) COLLATE Japanese_CI_AS NULL ,
	[resort_code] [bigint] not null ,
	[active_flg] [tinyint] NOT NULL ,
	[start_date] [datetime] NULL ,
	[end_date] [datetime] NULL 
) on [primary]
go

/****** オブジェクト :  テーブル [dbo].[mst_room]    スクリプト日付 : 2007/02/06 15:09:55 ******/
create table [dbo].[mst_room] (
	[room_id] [bigint] not null ,
	[roomtype_id] [varchar] (128) collate japanese_ci_as not null ,
	[roomnumber] [bigint] not null ,
	[seq] [int] null ,
	[active_flg] [tinyint] not null 
) on [primary]
go

/****** オブジェクト :  テーブル [dbo].[mst_scenario]    スクリプト日付 : 2007/02/06 15:09:55 ******/
create table [dbo].[mst_scenario] (
	[scenario_id] [tinyint] identity (1, 1) not null ,
	[scenario_nm] [varchar] (100) collate japanese_ci_as null ,
	[headertext] [varchar] (256) collate japanese_ci_as null ,
	[default_flg] [tinyint] null ,
	[step] [tinyint] null ,
	[viewsequence] [tinyint] null ,
	[active_flg] [tinyint] not null 
) on [primary]
go

/****** オブジェクト :  テーブル [dbo].[mst_summary]    スクリプト日付 : 2007/02/06 15:09:55 ******/
create table [dbo].[mst_summary] (
	[summary_id] [smallint] identity (1, 1) not null ,
	[summary_nm] [varchar] (40) collate japanese_ci_as null ,
	[summary_ip] [varchar] (40) collate japanese_ci_as null ,
	[basicinfo] [varchar] (100) collate japanese_ci_as null ,
	[importantinfo] [varchar] (40) collate japanese_ci_as null ,
	[satisfiedinfo] [varchar] (40) collate japanese_ci_as null ,
	[stayinfo] [varchar] (128) collate japanese_ci_as null ,
	[csinfo] [varchar] (40) collate japanese_ci_as null ,
	[owner] [varchar] (35) collate japanese_ci_as null ,
	[resort_code] [bigint] not null ,
	[active_flg] [tinyint] not null ,
	[updatedby] [varchar] (20) collate japanese_ci_as null ,
	[updated] [datetime] null 
) on [primary]
go

/****** オブジェクト :  テーブル [dbo].[mst_user]    スクリプト日付 : 2007/02/06 15:09:56 ******/
create table [dbo].[mst_user] (
	[user_id] [smallint] identity (1, 1) not null ,
	[user_nm] [varchar] (40) collate japanese_ci_as null ,
	[user_ip] [varchar] (20) collate japanese_ci_as not null ,
	[role_flg] [tinyint] null ,
	[resort_code] [bigint] null ,
	[comment] [varchar] (4000) collate japanese_ci_as null ,
	[active_flg] [tinyint] not null ,
	[updatedby] [varchar] (20) collate japanese_ci_as null ,
	[updated] [datetime] null 
) on [primary]
go

/****** オブジェクト :  テーブル [dbo].[oth_scenarioconfiguration]    スクリプト日付 : 2007/02/06 15:09:56 ******/
create table [dbo].[oth_scenarioconfiguration] (
	[questionnairesheet_id] [smallint] not null ,
	[scenario_id] [tinyint] not null ,
	[default_flg] [tinyint] null ,
	[step] [tinyint] null ,
	[viewsequence] [tinyint] null 
) on [primary]
go

/****** オブジェクト :  テーブル [dbo].[oth_sheetconfiguration]    スクリプト日付 : 2007/02/06 15:09:56 ******/
create table [dbo].[oth_sheetconfiguration] (
	[questionnairesheet_id] [smallint] not null ,
	[questionnaire_id] [smallint] not null ,
	[owner] [varchar] (35) collate japanese_ci_as not null ,
	[viewsequence] [tinyint] null ,
	[viewtype] [tinyint] null ,
	[scenario] [tinyint] not null ,
	[resort_code] [bigint] not null ,
	[questionnaire_text] [varchar] (400) collate japanese_ci_as null ,
	[leadmessage] [varchar] (256) collate japanese_ci_as null ,
	[settype] [tinyint] null ,
	[answertype] [tinyint] null ,
	[memo] [text] collate japanese_ci_as null ,
	[labeltext] [varchar] (400) collate japanese_ci_as null ,
	[labelmemo] [varchar] (50) collate japanese_ci_as null ,
	[parent] [smallint] null ,
	[active_flg] [tinyint] not null ,
	[updatedby] [varchar] (20) collate japanese_ci_as null ,
	[updated] [datetime] null 
) on [primary] textimage_on [primary]
go

/****** オブジェクト :  テーブル [dbo].[rtm_area]    スクリプト日付 : 2007/02/06 15:09:56 ******/
create table [dbo].[rtm_area] (
	[area_code] [bigint] not null ,
	[name] [varchar] (256) collate japanese_ci_as not null 
) on [primary]
go

/****** オブジェクト :  テーブル [dbo].[rtm_cancel]    スクリプト日付 : 2007/02/06 15:09:56 ******/
create table [dbo].[rtm_cancel] (
	[cancel_code] [bigint] not null ,
	[name] [varchar] (256) collate japanese_ci_as not null 
) on [primary]
go

/****** オブジェクト :  テーブル [dbo].[rtm_customer]    スクリプト日付 : 2007/02/06 15:09:56 ******/
create table [dbo].[rtm_customer] (
	[customer_code] [bigint] null ,
	[customernumber] [varchar] (128) collate japanese_ci_as not null ,
	[customercategory] [varchar] (32) collate japanese_ci_as null ,
	[customerrank] [varchar] (32) collate japanese_ci_as null ,
	[name] [varchar] (256) collate japanese_ci_as null ,
	[name_kana] [varchar] (256) collate japanese_ci_as null ,
	[gender_code] [bigint] not null ,
	[birth_date] [varchar] (256) collate japanese_ci_as null ,
	[area_code] [bigint] not null ,
	[address_zipcode] [varchar] (256) collate japanese_ci_as null ,
	[address_segment_1] [varchar] (256) collate japanese_ci_as null ,
	[address_segment_2] [varchar] (256) collate japanese_ci_as null ,
	[address_segment_3] [varchar] (256) collate japanese_ci_as null ,
	[phone_number_1] [varchar] (256) collate japanese_ci_as null ,
	[phone_number_2] [varchar] (256) collate japanese_ci_as null ,
	[mail_address_1] [varchar] (256) collate japanese_ci_as null ,
	[mail_address_2] [varchar] (256) collate japanese_ci_as null ,
	[post_permission_code] [bigint] null ,
	[mail_permission_code] [bigint] null ,
	[information] [varchar] (4096) collate japanese_ci_as null ,
	[active_flg] [tinyint] not null ,
	[married_flg] [tinyint] not null ,
	[repeater] [tinyint] null ,
	[marketingarea] [varchar] (32) collate japanese_ci_as null 
) on [primary]
go

/****** オブジェクト :  テーブル [dbo].[rtm_gender]    スクリプト日付 : 2007/02/06 15:09:57 ******/
create table [dbo].[rtm_gender] (
	[gender_code] [bigint] not null ,
	[name] [varchar] (256) collate japanese_ci_as not null 
) on [primary]
go

/****** オブジェクト :  テーブル [dbo].[rtm_resort]    スクリプト日付 : 2007/02/06 15:09:57 ******/
create table [dbo].[rtm_resort] (
	[resort_code] [bigint] not null ,
	[name] [varchar] (256) collate japanese_ci_as not null 
) on [primary]
go

/****** オブジェクト :  テーブル [dbo].[rtt_marketing]    スクリプト日付 : 2007/02/06 15:09:57 ******/
create table [dbo].[rtt_marketing] (
	[reserve_code] [bigint] not null ,
	[room_code] [bigint] not null ,
	[controldiv_id] [bigint] not null ,
	[control_id] [varchar] (128) collate japanese_ci_as null 
) on [primary]
go

/****** オブジェクト :  テーブル [dbo].[rtt_reserve]    スクリプト日付 : 2007/02/06 15:09:57 ******/
create table [dbo].[rtt_reserve] (
	[reserve_code] [bigint] not null ,
	[room_code] [bigint] not null ,
	[customernumber] [varchar] (32) collate japanese_ci_as not null ,
	[arrive_date] [varchar] (64) collate japanese_ci_as null ,
	[nights] [bigint] not null ,
	[depart_date] [varchar] (64) collate japanese_ci_as null ,
	[cancel_code] [bigint] null ,
	[information] [varchar] (4096) collate japanese_ci_as null ,
	[lateout_flg] [tinyint] not null ,
	[manpersoncount] [smallint] not null ,
	[womenpersoncount] [smallint] not null ,
	[childpersoncount_a] [smallint] not null ,
	[childpersoncount_b] [smallint] not null ,
	[childpersoncount_c] [smallint] not null ,
	[childpersoncount_d] [smallint] not null ,
	[roomtype] [varchar] (32) collate japanese_ci_as null ,
	[roomcontrol] [varchar] (32) collate japanese_ci_as null ,
	[reservationroute] [varchar] (128) collate japanese_ci_as null ,
	[totalsalesamount] [bigint] not null ,
	[totalsalesamountdividedbynights] [bigint] not null ,
	[personunitprice] [bigint] not null ,
	[roomcharge] [bigint] not null ,
	[optionamount] [bigint] not null ,
	[restaurantamount] [bigint] not null ,
	[referencenumber] [varchar] (128) collate japanese_ci_as null ,
	[surveymethod] [tinyint] not null 
) on [primary]
go

/****** オブジェクト :  テーブル [dbo].[rtt_shopresult]    スクリプト日付 : 2007/02/06 15:09:57 ******/
create table [dbo].[rtt_shopresult] (
	[reserve_code] [bigint] not null ,
	[room_code] [bigint] not null ,
	[shop_id] [bigint] not null ,
	[shop_nm] [varchar] (128) collate japanese_ci_as not null ,
	[amount] [bigint] not null 
) on [primary]
go

alter table [dbo].[hst_answers] with nocheck add 
	constraint [pk_hst_answers] primary key  clustered 
	(
		[referencenumber],
		[questionnairesheet_id]
	)  on [primary] 
go

alter table [dbo].[hst_answersdetails] with nocheck add 
	constraint [pk_hst_answersdetails] primary key  clustered 
	(
		[referencenumber],
		[questionnairesheet_id],
		[questionnaire_id],
		[answer]
	)  on [primary] 
go

alter table [dbo].[hst_valueinformation] with nocheck add 
	constraint [pk_hst_valueinformation] primary key  clustered 
	(
		[reserve_code],
		[room_code]
	)  on [primary] 
go

alter table [dbo].[mst_answeroptions] with nocheck add 
	constraint [pk_mst_answeroptions] primary key  clustered 
	(
		[questionnaire_id],
		[options_no]
	)  on [primary] 
go

alter table [dbo].[mst_confirm] with nocheck add 
	constraint [pk_mst_confirm] primary key  clustered 
	(
		[questionnaire_id],
		[confirm_q],
		[options_no]
	)  on [primary] 
go

alter table [dbo].[mst_control] with nocheck add 
	constraint [pk_mst_control] primary key  clustered 
	(
		[controldiv_id],
		[control_id]
	)  on [primary] 
go

alter table [dbo].[mst_mailtemplate] with nocheck add 
	constraint [pk_mst_mailtemplate] primary key  clustered 
	(
		[mailtemplate_id]
	)  on [primary] 
go

alter table [dbo].[mst_questionnaire] with nocheck add 
	constraint [pk_mst_questionnaire] primary key  clustered 
	(
		[questionnaire_id]
	)  on [primary] 
go

alter table [dbo].[mst_questionnaireblock] with nocheck add 
	constraint [pk_mst_questionnaireblock] primary key  clustered 
	(
		[questionnaireblock_id]
	)  on [primary] 
go

alter table [dbo].[mst_questionnairecategory] with nocheck add 
	constraint [pk_mst_questionnairecategory] primary key  clustered 
	(
		[questionnairecategory_id]
	)  on [primary] 
go

alter table [dbo].[mst_questionnairesheet] with nocheck add 
	constraint [pk_mst_questionnairesheet] primary key  clustered 
	(
		[questionnairesheet_id]
	)  on [primary] 
go

alter table [dbo].[mst_room] with nocheck add 
	constraint [pk_mst_room] primary key  clustered 
	(
		[room_id]
	)  on [primary] 
go

alter table [dbo].[mst_scenario] with nocheck add 
	constraint [pk_mst_scenario] primary key  clustered 
	(
		[scenario_id]
	)  on [primary] 
go

alter table [dbo].[mst_summary] with nocheck add 
	constraint [pk_mst_summary] primary key  clustered 
	(
		[summary_id]
	)  on [primary] 
go

alter table [dbo].[mst_user] with nocheck add 
	constraint [pk_mst_user] primary key  clustered 
	(
		[user_id]
	)  on [primary] 
go

alter table [dbo].[oth_scenarioconfiguration] with nocheck add 
	constraint [pk_oth_scenarioconfiguration] primary key  clustered 
	(
		[questionnairesheet_id],
		[scenario_id]
	)  on [primary] 
go

alter table [dbo].[oth_sheetconfiguration] with nocheck add 
	constraint [pk_oth_sheetconfiguration] primary key  clustered 
	(
		[questionnairesheet_id],
		[questionnaire_id],
		[owner]
	)  on [primary] 
go

alter table [dbo].[rtm_area] with nocheck add 
	constraint [pk_rtm_area_code] primary key  clustered 
	(
		[area_code]
	)  on [primary] 
go

alter table [dbo].[rtm_cancel] with nocheck add 
	constraint [pk_rtm_cancel] primary key  clustered 
	(
		[cancel_code]
	)  on [primary] 
go

alter table [dbo].[rtm_customer] with nocheck add 
	constraint [pk_rtm_customer] primary key  clustered 
	(
		[customernumber]
	)  on [primary] 
go

alter table [dbo].[rtm_gender] with nocheck add 
	constraint [pk_rtm_gender] primary key  clustered 
	(
		[gender_code]
	)  on [primary] 
go

alter table [dbo].[rtm_resort] with nocheck add 
	constraint [pk_rtm_resort] primary key  clustered 
	(
		[resort_code]
	)  on [primary] 
go

alter table [dbo].[rtt_marketing] with nocheck add 
	constraint [pk_rtt_marketing] primary key  clustered 
	(
		[reserve_code],
		[room_code],
		[controldiv_id]
	)  on [primary] 
go

alter table [dbo].[rtt_reserve] with nocheck add 
	constraint [pk_rtt_reserve] primary key  clustered 
	(
		[reserve_code],
		[room_code]
	)  on [primary] 
go

alter table [dbo].[rtt_shopresult] with nocheck add 
	constraint [pk_rtt_shopresult] primary key  clustered 
	(
		[reserve_code],
		[room_code],
		[shop_id]
	)  on [primary] 
go

alter table [dbo].[hst_valueinformation] with nocheck add 
	constraint [df_hst_valueinformation_checkout] default (0) for [checkout],
	constraint [df_hst_valueinformation_active_flg] default (1) for [active_flg]
go

alter table [dbo].[mst_answeroptions] with nocheck add 
	constraint [df_mst_answeroptions_active_flg] default (1) for [active_flg]
go

alter table [dbo].[mst_control] with nocheck add 
	constraint [df_mst_control_active_flg] default (1) for [active_flg]
go

alter table [dbo].[mst_mailtemplate] with nocheck add 
	constraint [df_mst_mailtemplate_active_flg] default (1) for [active_flg]
go

alter table [dbo].[mst_questionnaire] with nocheck add 
	constraint [df_mst_questionnaire_active_flg] default (1) for [active_flg]
go

alter table [dbo].[mst_questionnairesheet] with nocheck add 
	constraint [df_mst_questionnairesheet_active_flg] default (1) for [active_flg]
go

alter table [dbo].[mst_room] with nocheck add 
	constraint [df_mst_room_active_flg] default (1) for [active_flg]
go

alter table [dbo].[mst_scenario] with nocheck add 
	constraint [df_mst_scenario_active_flg] default (1) for [active_flg]
go

alter table [dbo].[mst_summary] with nocheck add 
	constraint [df_mst_summary_active_flg] default (1) for [active_flg]
go

alter table [dbo].[mst_user] with nocheck add 
	constraint [df_mst_user_active_flg] default (1) for [active_flg]
go

alter table [dbo].[oth_sheetconfiguration] with nocheck add 
	constraint [df_oth_sheetconfiguration_active_flg] default (1) for [active_flg]
go

alter table [dbo].[rtm_customer] with nocheck add 
	constraint [df_rtm_customer_gender_code] default (100000000000) for [gender_code],
	constraint [df_rtm_customer_area_code] default (100000000000) for [area_code],
	constraint [df_rtm_customer_active_flg] default (1) for [active_flg],
	constraint [df_rtm_customer_married_flg] default (0) for [married_flg]
go

alter table [dbo].[rtt_reserve] with nocheck add 
	constraint [df_rtt_reserve_nights] default (0) for [nights],
	constraint [df_rtt_reserve_cancel_code] default (100000000000) for [cancel_code],
	constraint [df_rtt_reserve_lateout_flg] default (0) for [lateout_flg],
	constraint [df_rtt_reserve_manpersoncount] default (0) for [manpersoncount],
	constraint [df_rtt_reserve_womenpersoncount] default (0) for [womenpersoncount],
	constraint [df_rtt_reserve_childpersoncount_a] default (0) for [childpersoncount_a],
	constraint [df_rtt_reserve_childpersoncount_b] default (0) for [childpersoncount_b],
	constraint [df_rtt_reserve_childpersoncount_c] default (0) for [childpersoncount_c],
	constraint [df_rtt_reserve_childpersoncount_d] default (0) for [childpersoncount_d],
	constraint [df_rtt_reserve_totalsalesamount] default (0) for [totalsalesamount],
	constraint [df_rtt_reserve_totalsalesamountdividedbynights] default (0) for [totalsalesamountdividedbynights],
	constraint [df_rtt_reserve_personunitprice] default (0) for [personunitprice],
	constraint [df_rtt_reserve_roomcharge] default (0) for [roomcharge],
	constraint [df_rtt_reserve_optionamount] default (0) for [optionamount],
	constraint [df_rtt_reserve_restaurantamount] default (0) for [restaurantamount],
	constraint [df_rtt_reserve_surveymethod] default (0) for [surveymethod]
go

