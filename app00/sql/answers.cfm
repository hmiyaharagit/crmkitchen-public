<cfset normal7 = "非常に満足した,満足した,やや満足した,どちらともいえない,やや不満だった,不満だった,非常に不満だった">
<cfset illegu7_1 = "非常に利用したい,利用したい,やや利用したい,どちらともいえない,やや利用したくない,利用したくない,まったく利用したくない">
<cfset illegu7_2 = "非常にすすめたい,すすめたい,ややすすめたい,どちらともいえない,ややすすめたくない,すすめたくない,まったくすすめたくない">
<cfset normal8 = "非常に満足した,満足した,やや満足した,どちらともいえない,やや不満だった,不満だった,非常に不満だった,利用していない">

<cfset q8list = "">

<!--- 全設問 --->
<cfquery name="q_question" datasource="rnrcrm">
	select questionnaire_id,questionnaire_type,questionnaire_nm from dbo.mst_questionnaire order by questionnairecategory_id,questionnaire_id
</cfquery>
<cfloop query="q_question">
	<cfif questionnaire_type eq 2>
		<!--- 回答候補値削除 --->
		<cfquery name="d_question" datasource="rnrcrm">
			delete from  dbo.mst_answeroptions where questionnaire_id = #questionnaire_id#
		</cfquery>
		<cfquery name="i_answeroptions" datasource="rnrcrm">
			insert into mst_answeroptions(questionnaire_id,options_no,options_label,options_type,scenario,resort_code,active_flg,viewsequence)values(#questionnaire_id#,1,'',5,null,10,1,1)
		</cfquery>
	</cfif>

	<cfif questionnaire_type eq 1>
		<!--- 回答候補値削除 --->
		<cfquery name="d_question" datasource="rnrcrm">
			delete from  dbo.mst_answeroptions where questionnaire_id = #questionnaire_id#
		</cfquery>
		<cfset wk_list = normal7>
		<cfif listfind(q8list,questionnaire_id) neq 0>	<cfset wk_list = normal8>		</cfif>
		<cfif questionnaire_id eq 4>					<cfset wk_list = illegu7_1>		</cfif>
		<cfif questionnaire_id eq 5>					<cfset wk_list = illegu7_2>		</cfif>

		<cfset cnt=1>
		<cfloop list="#wk_list#" index="idx">
			<cfquery name="i_answeroptions" datasource="rnrcrm">
				insert into mst_answeroptions(questionnaire_id,options_no,options_label,options_type,scenario,resort_code,active_flg,viewsequence)values(#questionnaire_id#,#cnt#,'#idx#',1,null,10,1,#cnt#)
			</cfquery>
			<cfset cnt = cnt+1>
		</cfloop>
	</cfif>

</cfloop>
<cfdump var=#q_question#>