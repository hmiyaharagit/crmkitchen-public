<cfparam name="form.reserve_code" default="">
<cfparam name="form.referencenumber" default="">
<!--- 
<cfquery datasource="#dsn#" name="q_sheet">
	select top 1 questionnairesheet_id from #dbu#.mst_questionnairesheet where start_date <= '#session.depart_date#' order by start_date desc
</cfquery>
 --->
<cfquery datasource="#dsn#" name="q_question">
	select	 b.questionnaire_id
			,b.questionnairesheet_id
			,b.viewtype
			,b.questionnaire_text
			,b.leadmessage
			,b.settype
			,b.parent
			,b.answertype
			,b.memo as message
			,b.scenario
			,a.questionnaire_nm
			,a.questionnaire_type
			,a.requisite_flg
			,c.questionnairecategory_nm
			,b.labeltext
			,b.labelmemo as memo
	from	 #dbu#.mst_questionnaire as a
			,#dbu#.oth_sheetconfiguration as b
			,#dbu#.mst_questionnairecategory as c
	where	 a.questionnaire_id = b.questionnaire_id
	and		 a.questionnairecategory_id = c.questionnairecategory_id
	and		 b.questionnairesheet_id = #session.sheet_id#
	and		 b.owner = 'master'
	and		 b.active_flg = 1
	order by b.viewsequence
</cfquery>
<cfquery dbtype="query" name="q_scenario">
	select distinct	 viewtype
					,scenario
	from			q_question
	order by	 scenario
</cfquery>
<cfset scenario_ary=Arraynew(2)>
<cfloop query="q_question">
	<cfset Arrayappend(scenario_ary[q_question.scenario],q_question.questionnaire_id)>
</cfloop>
<cfoutput>
<html>
<head>
<meta http-equiv="content-type" content="text/html" />
<title>#title#</title>
<link rel="stylesheet" href="configcss.css">
<script language="javascript" type="text/javascript">
	var scenario_ary=new Array();
<cfloop index="i" from="1" to="#arraylen(scenario_ary)#">
	scenario_ary[#i#]=new Array();
<cfloop index="j" from="1" to="#arraylen(scenario_ary[i])#">
	scenario_ary[#i#][#j#]=#scenario_ary[i][j]#;</cfloop>
</cfloop>

function changeScenario(scenario,disable_flg){
	for(var j=1;j<scenario_ary[scenario].length;j++){
		try{
			for(var i=1;i<100;i++){
				document.getElementById('opt'+scenario_ary[scenario][j]+'_'+i).disabled=disable_flg;
				if(disable_flg){
					document.getElementById('opt'+scenario_ary[scenario][j]+'_'+i).checked=false;
				}
			}
		}
		catch(e){}
		try{
			document.getElementById('opt'+scenario_ary[scenario][j]+'_text').disabled=disable_flg;
			if(disable_flg){
				document.getElementById('opt'+scenario_ary[scenario][j]+'_text').text='';
			}
			document.getElementById('opt'+scenario_ary[scenario][j]+'_99').disabled=disable_flg;
			if(disable_flg){
				document.getElementById('opt'+scenario_ary[scenario][j]+'_99').checked=false;
			}
		}
		catch(e){}
	}
	if(disable_flg){
		document.forms[0]['scenario'+scenario].value='1';
	}
	else{
		document.forms[0]['scenario'+scenario].value='0';
	}
}

function setScenario(scenario,field,type,ary){
	var disable_flg=true;
	if(field.checked){
		disable_flg=false;
	}
	changeScenario(scenario,disable_flg);
	if(disable_flg==false && type=='radio'){
		for(var i=1;i<ary.length;i++){
			if(ary[i] != scenario){
				changeScenario(ary[i],true);
			}
		}
	}
}
</script>


</head>
<body onload="init();">
<form method="post" action="confirm.cfm">
<table border="0" cellpadding="0" cellspacing="1" width="1200">
</cfoutput>
<cfset initjs="">
<cfloop query="q_question">
	<cfset q_question_currentrow=q_question.currentrow>
	<cfif Len(q_question.parent) EQ 0>
		<cfset wk_q_id = q_question.questionnaire_id>
	<cfelse>
		<cfset wk_q_id = q_question.parent>
	</cfif>

	<cfquery name="q_option" datasource = "#dsn#">
		select	 A.questionnaire_id
				,A.options_no
				,A.options_label
				,A.options_type
				,A.addtext
				,A.scenario
				,A.viewsequence
				,B.answertype
		from	 #dbu#.mst_answeroptions as A
				,#dbu#.oth_sheetconfiguration as B
		where	 A.questionnaire_id = #wk_q_id#
		and		 A.questionnaire_id = B.questionnaire_id
		and		 B.questionnairesheet_id = #session.sheet_id#
		and		 A.active_flg = 1
		and		 A.resort_code = #resort_code#
		and		 B.Owner = 'Master'
		order by A.viewsequence
	</cfquery>
	<cfset groupscenario_ary=listtoArray(valuelist(q_option.scenario))>
	<cfquery datasource="#dsn#" name="q_answersdetail">
		select	 questionnaire_id
				,answer
				,answertext
		from	 #dbu#.hst_answersdetails
		where	 questionnairesheet_id = #session.sheet_id#
		and		 questionnaire_id = #q_question.questionnaire_id#
		and		 referencenumber = '#form.referencenumber#'
		order by questionnaire_id
	</cfquery>
	<cfif q_answersdetail.recordcount eq 0>
		<cfset queryaddrow(q_answersdetail,1)>
		<cfset querysetcell(q_answersdetail,"questionnaire_id",q_question.questionnaire_id,1)>
		<cfset querysetcell(q_answersdetail,"answer",0,1)>
		<cfset querysetcell(q_answersdetail,"answertext","",1)>
	</cfif>
	
	<cfset othertext="">
	<cfquery dbtype="query" name="q_othertext">
		select answertext from q_answersdetail where answer = '0'
	</cfquery>
	<cfif q_othertext.recordcount neq 0>
		<cfset othertext=q_othertext.answertext>
	</cfif>

<cfoutput>
<cfif Arrayisempty(groupscenario_ary) eq false>
	<script language="javascript" type="text/javascript">
		var groupscenario_ary#q_question.questionnaire_id#=new Array();
		<cfloop index="i" from="1" to="#Arraylen(groupscenario_ary)#">
		groupscenario_ary#q_question.questionnaire_id#[#i#]=#groupscenario_ary[i]#;</cfloop>
	</script>
</cfif>
<cfif q_question.labeltext neq "">
<cfif q_question.currentrow neq 1>
	<tr>
		<td>&nbsp;</td>
	</tr>
</cfif>
	<tr>
		<th align="left" class="question">#q_question.labeltext#</th>
	</tr>
</cfif>
	<tr>
		<td><!--- [#questionnaire_id#] ---><div style="margin-left:5px; padding:5;background:##EEEEE9">#q_question.questionnaire_text#</div></td>
	</tr>
	<tr>
		<td>
		<div style="margin-left:20px; padding:0 0 20 0;">
		<cfif Len(q_question.parent) NEQ 0>
				<cfset QueryAddRow(q_option,1)>
				<cfset QuerySetCell(q_option,"options_no", 0)>
				<cfset QuerySetCell(q_option,"viewsequence", 0)>
				<cfset QuerySetCell(q_option,"options_label", "未回答")>

				<select name="opt#q_question.questionnaire_id#">
				<cfloop query="q_option">
					<option value="#q_option.options_no#"<cfif listfind(valuelist(q_answersdetail.answer),q_option.options_no)> selected</cfif> >#viewsequence#&nbsp;&nbsp;#q_option.options_label#</option>
				</cfloop>
				</select>
			<cfelse>
				<cfloop query="q_option">#repeatstring("	",3)#
					<cfswitch expression="#q_option.options_type#">
						<cfcase value="0"><!--- ラジオボタンのその他 --->
							<input type="radio" name="opt#q_option.questionnaire_id#" value="#q_option.options_no#"<cfif listfind(valuelist(q_answersdetail.answer),q_option.options_no)> checked</cfif> id="opt#q_option.questionnaire_id#_#q_option.options_no#"<cfif q_question.settype[q_question_currentrow] eq 1 and q_option.scenario neq ""> onchange="setscenario(#q_option.scenario#,this,'radio',groupscenario_ary#q_question.questionnaire_id[q_question_currentrow]#);"
							<cfset initjs=initjs & "setscenario(#q_option.scenario#,document.getelementbyid('opt#q_option.questionnaire_id#_#q_option.options_no#'),'radio',groupscenario_ary#q_question.questionnaire_id[q_question_currentrow]#);"></cfif>><label for="opt#q_option.questionnaire_id#_#q_option.options_no#">#viewsequence#&nbsp;&nbsp;#q_option.options_label#</label> <input type="text" name="opt#q_option.questionnaire_id#_text" value="<cfif listfind(valuelist(q_answersdetail.answer),99)>#htmleditformat(othertext)#</cfif>">
						</cfcase>
						<cfcase value="1"><!--- ラジオボタン --->
							<input type="radio" name="opt#q_option.questionnaire_id#" value="#q_option.options_no#"<cfif listfind(valuelist(q_answersdetail.answer),q_option.options_no)> checked</cfif> id="opt#q_option.questionnaire_id#_#q_option.options_no#"<cfif q_question.settype[q_question_currentrow] eq 1 and q_option.scenario neq ""> onchange="setscenario(#q_option.scenario#,this,'radio',groupscenario_ary#q_question.questionnaire_id[q_question_currentrow]#);"
							<cfset initjs=initjs & "setscenario(#q_option.scenario#,document.getelementbyid('opt#q_option.questionnaire_id#_#q_option.options_no#'),'radio',groupscenario_ary#q_question.questionnaire_id[q_question_currentrow]#);"></cfif>><label for="opt#q_option.questionnaire_id#_#q_option.options_no#">#viewsequence#&nbsp;&nbsp;#q_option.options_label#</label>
						</cfcase>
						<cfcase value="2"><!--- チェックボックスのその他 --->
							<input type="checkbox" name="opt#q_option.questionnaire_id#" value="#q_option.options_no#"<cfif listfind(valuelist(q_answersdetail.answer),q_option.options_no)> checked</cfif> id="opt#q_option.questionnaire_id#_#q_option.options_no#"<cfif q_question.settype[q_question_currentrow] eq 1 and q_option.scenario neq ""> onchange="setscenario(#q_option.scenario#,this,'check',groupscenario_ary#q_question.questionnaire_id[q_question_currentrow]#);"
							<cfset initjs=initjs & "setscenario(#q_option.scenario#,document.getelementbyid('opt#q_option.questionnaire_id#_#q_option.options_no#'),'check',groupscenario_ary#q_question.questionnaire_id[q_question_currentrow]#);"></cfif>><label for="opt#q_option.questionnaire_id#_#q_option.options_no#">#viewsequence#&nbsp;&nbsp;#q_option.options_label#</label> <input type="text" name="opt#q_option.questionnaire_id#_text" value="<cfif listfind(valuelist(q_answersdetail.answer),99)>#htmleditformat(othertext)#</cfif>">
							<br>
						</cfcase>
						<cfcase value="3"><!--- チェックボックス --->
							<input type="checkbox" name="opt#q_option.questionnaire_id#" value="#q_option.options_no#"<cfif listfind(valuelist(q_answersdetail.answer),q_option.options_no)> checked</cfif> id="opt#q_option.questionnaire_id#_#q_option.options_no#"<cfif q_question.settype[q_question_currentrow] eq 1 and q_option.scenario neq ""> onchange="setscenario(#q_option.scenario#,this,'check',groupscenario_ary#q_question.questionnaire_id[q_question_currentrow]#);"
							<cfset initjs=initjs & "setscenario(#q_option.scenario#,document.getelementbyid('opt#q_option.questionnaire_id#_#q_option.options_no#'),'check',groupscenario_ary#q_question.questionnaire_id[q_question_currentrow]#);"></cfif>><label for="opt#q_option.questionnaire_id#_#q_option.options_no#">#viewsequence#&nbsp;&nbsp;#q_option.options_label#</label>
							<br>
						</cfcase>
						<cfcase value="4"><!--- テキスト --->
							<cfif listfind(digitcheck,q_question.questionnaire_id[q_question.currentrow]) neq 0>
								<input type="text" name="opt#q_option.questionnaire_id#_text" id="opt#q_option.questionnaire_id#_text" value="#htmleditformat(q_answersdetail.ANSWERTEXT)#" onBlur="digitcheck(this)">
								<font color="##FF0000">※「半角」で入れてください</font>
							<cfelse>
								<input type="text" name="opt#q_option.questionnaire_id#_text" id="opt#q_option.questionnaire_id#_text" value="#htmleditformat(q_answersdetail.ANSWERTEXT)#">
							</cfif>
						</cfcase>
						<cfcase value="5"><!--- テキストエリア --->
							<textarea name="opt#q_option.questionnaire_id#_text" cols="40" rows="4" id="opt#q_option.questionnaire_id#_text">#htmleditformat(q_answersdetail.answertext)#</textarea>
						</cfcase>
					</cfswitch>
					<!--- 
					<cfif q_option.currentrow mod 3 eq 0 and q_option.currentrow neq q_option.recordcount><br><cfelseif q_option.currentrow neq q_option.recordcount>&nbsp;&nbsp;</cfif>
					 --->
				</cfloop>
			</cfif>
		</div>
		</td>
	</tr>
</cfoutput>
</cfloop>
<cfoutput>
	<tr>
		<td align="center">
			<input type="submit" name="entry" value=" 登 録 " style="background-color:##FFCC00;height: 30px;width: 200px;border: none;">
		</td>
	</tr>
</table>
<input type="hidden" name="reserve_code" value="#form.reserve_code#">
<input type="hidden" name="referencenumber" value="#form.referencenumber#">
<cfloop query="q_scenario">
<!--- 
<input type="hidden" name="scenario#q_scenario.scenario#" value="#q_scenario.viewtype#">
 --->				
<input type="hidden" name="scenario#q_scenario.scenario#" value="0">
</cfloop>
</form>
<script language="javascript" type="text/javascript">
	function init(){
		#replace(initjs,";",";" & chr(13) & chr(10) & "		","all")#
	}
</script>
</body>
</html>
</cfoutput>