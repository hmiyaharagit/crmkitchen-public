﻿<cfcomponent name="csrespond" hint="">
	<!---
	*************************************************************************************************
	* Class				:csrespond
	* Method			:getSheetconfiguration
	* Description		:
	* Custom Attributes	:none
	* Return Paramters	:
	* History			:1) Coded by 2006/09/11
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getSheetconfiguration" access="remote" returntype="query" hint="">
		<cfargument name="questionnairesheet_id" type="Numeric" default="">
		<cfargument name="appkey" type="String" default="">
		<cfsetting enablecfoutputonly="yes">	
		<cftry>
			<cfquery datasource="#arguments.appkey#" name="q_question">
				select
					B.questionnaire_id,
					B.questionnairesheet_id,
					B.viewtype,				
					B.questionnaire_text,
					B.leadmessage,
					B.settype,
					B.answertype,
					B.memo as message,
					B.scenario,
					A.questionnaire_nm,
					A.questionnaire_type,
					A.requisite_flg,
					C.questionnairecategory_nm,
					B.labeltext,
					B.labelmemo as memo,
					B.parent
				from 
					mst_questionnaire as A,
					oth_sheetconfiguration as B,
					mst_questionnairecategory as C
				where
					A.questionnaire_id = B.questionnaire_id
				and
					A.questionnairecategory_id = C.questionnairecategory_id
				and
					B.questionnairesheet_id = #arguments.questionnairesheet_id#
				and
					B.Owner = 'Master'				
				and
					B.active_flg = 1
				order by
					B.viewsequence
			</cfquery>
			<cfreturn q_question>
			<cfsetting enablecfoutputonly="No">

		<cfcatch type="any">
			<cfsetting enablecfoutputonly="No">
			<cfrethrow>
		</cfcatch>
		</cftry>
	</cffunction>

	<!---
	*************************************************************************************************
	* Class				:csrespond
	* Method			:getactivesheet
	* Description		:
	* Custom Attributes	:none
	* Return Paramters	:
	* History			:1) Coded by	
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getactivesheet" access="remote" returntype="query" hint="">
		<cfargument name="resort_code" type="Numeric" default="">
		<cfargument name="appkey" type="String" default="">
		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<cfquery datasource="#arguments.appkey#" name="q_sheet">
				select
					questionnairesheet_id
				from
					mst_questionnairesheet
				where
					resort_code = #arguments.resort_code#
				and
					active_flg = 1	
			</cfquery>
			<cfreturn q_sheet>
			<cfsetting enablecfoutputonly="No">

		<cfcatch type="any">
			<cfsetting enablecfoutputonly="No">
			<cfrethrow>
		</cfcatch>
		</cftry>
	</cffunction>

	<!---
	*************************************************************************************************
	* Class				:csrespond
	* Method			:getq_options
	* Description		:
	* Custom Attributes	:none
	* Return Paramters	:
	* History			:1) Coded by	
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getq_options" access="remote" returntype="query" hint="">
		<cfargument name="questionnaire_id" type="Numeric" default="">
		<cfargument name="resort_code" type="Numeric" default="">
		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<cfquery datasource="#arguments.appkey#" name="#dsn#">
				select	 A.questionnaire_id
						,A.options_no
						,A.options_label
						,A.options_type
						,A.addtext
						,A.scenario
						,A.viewsequence
						,B.answertype
				from	 #dbu#.mst_answeroptions as A
						,#dbu#.oth_sheetconfiguration as B
				where	 A.questionnaire_id = #arguments.questionnaire_id#
				and		 A.questionnaire_id = B.questionnaire_id
				and		 B.questionnairesheet_id = (select questionnairesheet_id from mst_questionnairesheet where active_flg = 1)
				and		 A.active_flg = 1
				and		 A.resort_code = #arguments.resort_code#
				and		 B.Owner = 'Master'
				and		 B.active_flg = 1
				order by A.viewsequence
			</cfquery>
			<cfreturn q_option>
			<cfsetting enablecfoutputonly="No">

		<cfcatch type="any">
			<cfsetting enablecfoutputonly="No">
			<cfrethrow>
		</cfcatch>
		</cftry>
	</cffunction>

	<!---
	*************************************************************************************************
	* Class				:csrespond
	* Method			:getq_options
	* Description		:
	* Custom Attributes	:none
	* Return Paramters	:
	* History			:1) Coded by	
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getanswersdetail" access="remote" returntype="query" hint="">
		<cfargument name="questionnaire_id" type="Numeric" default="">
		<cfargument name="questionnairesheet_id" type="Numeric" default="">
		<cfargument name="reserve_code" type="Numeric" default="">
		<cfargument name="appkey" type="String" default="">
		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<cfquery datasource="#arguments.appkey#" name="q_option">
				select
					questionnaire_id,
					answer,
					answertext
				from
					hst_answersdetails
				where
					questionnairesheet_id = #arguments.questionnairesheet_id#
				and
					questionnaire_id = #arguments.questionnaire_id#
				and
					reserve_code = #arguments.reserve_code#
				order by
					questionnaire_id
			</cfquery>
		
			<cfreturn q_option>
			<cfsetting enablecfoutputonly="No">

		<cfcatch type="any">
			<cfsetting enablecfoutputonly="No">
			<cfrethrow>
		</cfcatch>
		</cftry>
	</cffunction>

	<!---
	*************************************************************************************************
	* Class				:csrespond
	* Method			:getq_options
	* Description		:
	* Custom Attributes	:none
	* Return Paramters	:
	* History			:1) Coded by	
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getanswered" access="remote" returntype="query" hint="">
		<cfargument name="reserve_code" type="Numeric" default="">
		<cfargument name="appkey" type="String" default="">
		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<cfquery datasource="#arguments.appkey#" name="q_answered">
				select
					questionnairesheet_id
				from
					hst_answers
				where
					reserve_code = #arguments.reserve_code#
			</cfquery>
			<cfreturn q_answered>
			<cfsetting enablecfoutputonly="No">

		<cfcatch type="any">
			<cfsetting enablecfoutputonly="No">
			<cfrethrow>
		</cfcatch>
		</cftry>
	</cffunction>

	<!---
	*************************************************************************************************
	* Class				:csrespond
	* Method			:getq_options
	* Description		:
	* Custom Attributes	:none
	* Return Paramters	:
	* History			:1) Coded by	
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getactinganswered" access="remote" returntype="query" hint="">
		<cfargument name="reserve_code" type="Numeric" default="">
		<cfargument name="appkey" type="String" default="">
		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<cfquery datasource="#arguments.appkey#" name="q_answered">
				select
					questionnairesheet_id
				from
					hst_answers
				where
					reserve_code = #arguments.reserve_code#
				and
					(answermethod is null) or (answermethod = 0)
			</cfquery>
			<cfreturn q_answered>
			<cfsetting enablecfoutputonly="No">
		<cfcatch type="any">
			<cfsetting enablecfoutputonly="No">
			<cfrethrow>
		</cfcatch>
		</cftry>
	</cffunction>

	<!---
	*************************************************************************************************
	* Class				:csrespond
	* Method			:getanswers_options
	* Description		:
	* Custom Attributes	:none
	* Return Paramters	:
	* History			:1) Coded by	
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getanswers_options" access="remote" returntype="query" hint="">
		<cfargument name="questionnaire_id" type="Numeric" default="">
		<cfargument name="options_no" type="String" default="">
		<cfargument name="resort_code" type="Numeric" default="">
		<cfargument name="appkey" type="String" default="">
		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<cfquery datasource="#arguments.appkey#" name="q_option">
				select
					options_no,
					options_label,
					viewsequence
				from 
					mst_answeroptions
				where
					questionnaire_id = #arguments.questionnaire_id#
				and
					options_no in (#arguments.options_no#)
				and
					active_flg = 1
				and
					resort_code = #arguments.resort_code#
				order by
					viewsequence
			</cfquery>
<!--- 	
			<cfif arguments.options_no eq 0>
				<cfset QueryAddRow(q_option,1)>
				<cfset QuerySetCell(q_option,"options_no", 0)>
				<cfset QuerySetCell(q_option,"viewsequence", 0)>
				<cfset QuerySetCell(q_option,"options_label", "未回答")>
			</cfif>
<cfset QueryAddRow(q_option,1)>
<cfset QuerySetCell(q_option,"options_no", 0)>
<cfset QuerySetCell(q_option,"viewsequence", 0)>
<cfset QuerySetCell(q_option,"options_label", "未回答")>
	 --->
			<cfreturn q_option>
			<cfsetting enablecfoutputonly="No">

		<cfcatch type="any">
			<cfsetting enablecfoutputonly="No">
			<cfrethrow>
		</cfcatch>
		</cftry>
	</cffunction>

	<!---
	*************************************************************************************************
	* Class				:csrespond
	* Method			:getanswers_options
	* Description		:
	* Custom Attributes	:none
	* Return Paramters	:
	* History			:1) Coded by	
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="contradictAnswer" access="remote" returntype="query" hint="">
		<cfargument name="q_question" type="Query" default="">
		<cfargument name="form" type="Struct" default="">
		<cfargument name="appkey" type="String" default="">


		<cfsetting enablecfoutputonly="yes">
		<cftry>

			<cfquery dbtype="query" name="qoq_question">
				select *
				from
					arguments.q_question
				where
					parent is not null
			</cfquery>
			<cfset retQuery = QueryNew("error")>

			<cfloop query="qoq_question">
				<cfif ListFindNoCase( #form.fieldnames# , "opt#qoq_question.parent#") neq 0 and form["opt" & qoq_question.questionnaire_id] neq 0>
					<cfset parentsanswer = #form["opt" & qoq_question.parent]#>
					<cfset childanswer = #form["opt" & qoq_question.questionnaire_id]#>
					<cfif ListFind(parentsanswer,childanswer) EQ 0>
						<cfquery datasource="#arguments.appkey#" name="q_option">
							select
								options_no,
								options_label,
								viewsequence
							from 
								mst_answeroptions
							where
								questionnaire_id = #qoq_question.parent#
							and
								options_no = #childanswer#
							and
								active_flg = 1
							order by
								viewsequence
						</cfquery>
						<cfset QueryAddRow(retQuery,1)>
						<cfset QuerySetCell(retQuery,"error", "「#q_option.options_label#」は前の設問で選択されていません。回答内容を確認してください")>
					</cfif>
				</cfif>
			</cfloop>
<!--- 
<cfdump var=#qoq_question#><cfabort>
 --->
			<cfreturn retQuery>
			<cfsetting enablecfoutputonly="No">

		<cfcatch type="any">
			<cfsetting enablecfoutputonly="No">
			<cfrethrow>
		</cfcatch>
		</cftry>
	</cffunction>

	<!---
	*************************************************************************************************
	* Class				:csrespond
	* Method			:wk_duplicate
	* Description		:
	* Custom Attributes	:none
	* Return Paramters	:
	* History			:1) Coded by	
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="createduplicate" access="remote" returntype="query" hint="">
		<cfsetting enablecfoutputonly="yes">
		<cftry>
		<cfset wk_duplicate = querynew("")>
		<cfset QueryAddColumn(wk_duplicate, "key_qid", "Integer",arraynew(1))>
		<cfset QueryAddColumn(wk_duplicate, "key_ans", "Integer",arraynew(1))>
		<cfset QueryAddColumn(wk_duplicate, "dup_id", "VarChar",arraynew(1))>
		<cfset QueryAddColumn(wk_duplicate, "bse_id", "VarChar",arraynew(1))>
		<cfset queryaddrow(wk_duplicate,1)>
		<cfset querysetcell(wk_duplicate,"key_qid",54)>
		<cfset querysetcell(wk_duplicate,"key_ans",1)>
		<cfset querysetcell(wk_duplicate,"dup_id","55/56/57")>
		<cfset querysetcell(wk_duplicate,"bse_id","55/56/57")>
		<cfset queryaddrow(wk_duplicate,1)>
		<cfset querysetcell(wk_duplicate,"key_qid",54)>
		<cfset querysetcell(wk_duplicate,"key_ans",2)>
		<cfset querysetcell(wk_duplicate,"dup_id","122/123/124")>
		<cfset querysetcell(wk_duplicate,"bse_id","55/56/57")>
		<cfset queryaddrow(wk_duplicate,1)>
		<cfset querysetcell(wk_duplicate,"key_qid",54)>
		<cfset querysetcell(wk_duplicate,"key_ans",3)>
		<cfset querysetcell(wk_duplicate,"dup_id","128/129/130")>
		<cfset querysetcell(wk_duplicate,"bse_id","55/56/57")>
		<cfreturn wk_duplicate>
		<cfsetting enablecfoutputonly="No">

		<cfcatch type="any">
			<cfsetting enablecfoutputonly="No">
			<cfrethrow>
		</cfcatch>
		</cftry>
	</cffunction>
</cfcomponent>
