<h3>BitAnd の例</h3>

<p>2 つの整数をビットごとに AND 演算した結果を返します。
<p>BitAnd(5,255):<cfoutput>#BitAnd(5,255)#</cfoutput>
<p>BitAnd(5,0):<cfoutput>#BitAnd(5,0)#</cfoutput>
<p>BitAnd(128,128):<cfoutput>#BitAnd(128,128)#</cfoutput>