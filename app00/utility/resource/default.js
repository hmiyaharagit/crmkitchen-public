//二重送信不可フラグ
var submitFlg = false;

//共通　未入力の判断
function requiredCheck(field, fieldName) {
	if (getLength(field.value) == 0) {
		alert(fieldName + "が未入力です");
		try {
			field.focus();
		} catch(e) {
		}
		return false;
	}
	return true;
}

function lengthCheck(field, size, fieldName) {
	if (getLength(field.value) > size) {
	
		alert(MSG_LENGTH.replace("{0}",fieldName).replace("{1}",size));
		try {
			field.focus();
		} catch(e) {
		}
		return false;
	}
	return true;
}

//共通　テキストの長さ取得
function getLength(txt) {
	var cnt = 0;
	for (var i = 0; i < txt.length; i++) {
		var c = txt.charCodeAt(i);
		if ( (c >= 0x0 && c < 0x81) || (c == 0xf8f0) || (c >= 0xff61 && c < 0xffa0) || (c >= 0xf8f1 && c < 0xf8f4)) {
			cnt += 1;
		} else {
			cnt += 2;
		}
	}
	return cnt;
}

//共通　数値チェック
function numberCheck(field,fieldName,intLength,decLength){
	var param = field.value;
	var varInteger = "";
	var varDecimal = "";
	var idxSplit = 0;
	var msg = "";

	if(param!=""){
		if(isNaN(param)){
		
			//月報のためのタブ切り替え
    		tmpURL = document.URL.match(/health/i);
    		if (tmpURL != null){
				len = field.name.length-1;
				if (field.name.substr(len,1)==1){
					showtab2(1);
				}else{
					showtab2(2);
				}		
			}
   			 
			alert(MSG_NUMERIC.replace("{0}",fieldName));
			try {
				field.focus();
			} catch(e) {
			}
			return false;
		}    
    
		for(i=0; i<intLength; i++) {
			msg = msg + "9";
		}
          
		if(decLength != undefined && decLength > 0){
			msg = msg + ".";
			for(i=0; i<decLength; i++) {
				msg = msg + "9";
			}
		}
      
		if(param.indexOf(".",0) == -1){
			varInteger = param;
		} else {
			idxSplit = param.indexOf(".",0);
			varInteger = param.substring(0,idxSplit);
		}
		if(((isNaN(param.substring(0,1)) == false) && (getLength(varInteger) > intLength)) ||
		   ((isNaN(param.substring(0,1))) && (getLength(varInteger) > intLength + 1))){
		   
			//月報のためのタブ切り替え
    		tmpURL = document.URL.match(/health/i);
    		if (tmpURL != null){
				len = field.name.length-1;
				if (field.name.substr(len,1)==1){
					showtab2(1);
				}else{
					showtab2(2);
				}		
			}
		   
			alert(MSG_NUM_FORMAT.replace("{0}",fieldName).replace("{1}",msg));
			try {
				field.focus();
			} catch(e) {
			}
			return false;
		}
		if(decLength != undefined){
			if(idxSplit != 0){
				varDecimal = param.substring(idxSplit + 1,getLength(param));
				if(getLength(varDecimal) > decLength){
					
					//月報のためのタブ切り替え
    				tmpURL = document.URL.match(/health/i);
    				if (tmpURL != null){
						len = field.name.length-1;
						if (field.name.substr(len,1)==1){
							showtab2(1);
						}else{
							showtab2(2);
						}		
					}
					
					alert(MSG_NUM_FORMAT.replace("{0}",fieldName).replace("{1}",msg));
					try{
						field.focus();
					} catch(e) {
					}
					return false;
				}
			}
		} else {
			if(param.indexOf(".",0) != -1){
			
				//月報のためのタブ切り替え
    			tmpURL = document.URL.match(/health/i);
    			if (tmpURL != null){
					len = field.name.length-1;
					if (field.name.substr(len,1)==1){
						showtab2(1);
					}else{
						showtab2(2);
					}		
				}	
			
				alert(MSG_NUM_FORMAT.replace("{0}",fieldName).replace("{1}",msg));
				try {
					field.focus();
				} catch(e) {
				}
				return false;
			}
		}
	}
	if(param==" " || param=="　" ){
		param = "";
	}

	field.value = param;
	return true;
}

// 共通　日付チェック
function dateCheck(field, fieldName){
  var param = field.value;
  if(getLength(field.value)==0){
    return true;
  }
  if(param.match(/^(\d{4})(\/)(\d{2})(\/)(\d{2})$/) == null){
  
  	//月報のためのタブ切り替え
    tmpURL = document.URL.match(/health/i);
    if (tmpURL != null){
		len = field.name.length-1;
		if (field.name.substr(len,1)==1){
			showtab2(1);
		}else{
			showtab2(2);
		}		
    }
    
    alert(MSG_DATE_FORMAT.replace("{0}",fieldName));
    field.focus();
    return false;
  }
  if(!dateValueCheck(param)){
  	//月報のためのタブ切り替え
    tmpURL = document.URL.match(/health/i);
    if (tmpURL != null){
		len = field.name.length-1;
		if (field.name.substr(len,1)==1){
			showtab2(1);
		}else{
			showtab2(2);
		}		
    }
    alert(MSG_DATE.replace("{0}",fieldName));
    field.focus();
    return false;
  }
  return true;
}
function dateValueCheck(value){
  var years ;
  var months ;
  var days ;
  var d_months ;
  var d_days ;
  var defDate = new Date();
  var defYears = Math.floor(defDate.getYear() / 100) * 100;
  years = value.substr(0,4);
  months = parseInt(value.substr(5,2),10) - 1;
  days  = value.substr(8,2);

  var dates = new Date(years,months,days);
  var flag = true;
  if (years > 1900 && years < 2000) {
    if (years != dates.getYear() + 1900) { flag = false; }
  } else {
    if (years != dates.getYear()) { flag = false; }
  }
  if (months != dates.getMonth()) { flag = false; }
  if (days != dates.getDate()) { flag = false; }
  return flag;

}

// 共通　パスワードチェック
function passwordCheck(field) {
    var varPassWord = field.value;
    var lenPassWord = field.value.length;
    
    for(i=0;i<lenPassWord;i++) {
      if (varPassWord.charAt(i).match(/[0-9a-zA-Z ]*/) == "") {
        alert(MSG_ERROR_PASSWORD1);
        field.focus();
        return false;
      }
    }
    if (lenPassWord < 8 || lenPassWord > 20) {
      alert(MSG_ERROR_PASSWORD2);
      field.focus();
      return false;
    }
    return true;
}


//登録画面を閉じる
function windowclose(){
	flag = confirm(MSG_CLOSE);
	if(flag == true){
		window.close();
	}
}

//共通　チェックボックスチェックされているかチェック
function checkChecked(form, object){
	var obj;
	obj = document.forms(form).elements(object);
	if(!obj){
		return false;
	}
	if(typeof(obj.length)=='number'){
		for(i=0;i<obj.length;i++){
			if(obj[i].checked){
				return true;
			}
		}
		return false;
	} else {
		return obj.checked;
	}
}

// 共通　年度範囲チェック
function fiscalRangeCheck(form, fmObj, toObj){
	var fm = document.forms(form).elements(fmObj);
	var to = document.forms(form).elements(toObj);
	
	if (fm.value == "0" && to.value == "0") {
		alert(MSG_ERROR_FY_RANGE1);
		to.focus();
		return false;
	} else {
		if (to.value != "0" && fm.value > to.value) {
			alert(MSG_ERROR_FY_RANGE2);
			to.focus();
			return false;
		}
	}

	return true;
}

// 共通　年度Q範囲チェック
function fiscalQRangeCheck(form, fmObj, toObj, fmQObj, toQObj){
	var fm = document.forms(form).elements(fmObj);
	var to = document.forms(form).elements(toObj);
	var fmq = document.forms(form).elements(fmQObj);
	var toq = document.forms(form).elements(toQObj);
	
    if (!fiscalRangeCheck(form, fmObj, toObj)) {
      return false;
    }
    
	if (fmq.value != "0" && fm.value == "0") {
		alert(MSG_ERROR_FYQ_RANGE1);
		fm.focus();
		return false;
	} else if (toq.value != "0" && to.value == "0") {
		alert(MSG_ERROR_FYQ_RANGE1);
		to.focus();
		return false;
	} else if (fm.value == to.value && fmq.value != "0" && toq.value != "0" && fmq.value > toq.value) {
		alert(MSG_ERROR_FYQ_RANGE2);
		to.focus();
		return false;
	}

	return true;
}

//共通　0以上の数字チェック
function zeroCheck(field,fieldName){
	var param = field.value;
    if(isNaN(param.substring(0,1))){
    		//月報のためのタブ切り替え
    		tmpURL = document.URL.match(/health/i);
    		if (tmpURL != null){
				len = field.name.length-1;
				if (field.name.substr(len,1)==1){
					showtab2(1);
				}else{
					showtab2(2);
				}		
			}
			
			alert(MSG_ZERO.replace("{0}",fieldName));
			try {
				field.focus();
			} catch(e) {
			}
			return false;
		}
		return true;
		    
}	

