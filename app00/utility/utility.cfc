﻿<cfcomponent name="utility">
	<!---
	*************************************************************************************************
	* Class				: utility
	* Method			: cleanseData
	* Description		: 回答矛盾データ修正
	* Custom Attributes	: 
	* Return Paramters	: 
	* History			: 1) Coded by	2005/10/19 h.miyahara(NetFusion)
	*					: 2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="cleanseData" access="remote" returntype="any">
		<cftry>
			<cftransaction>
				<!--- 回答者予約番号取得 --->
				<cfquery name="q_reserve" datasource = "#Application.datasource#">
					select
						A.reserve_code
					from
						hst_answers as A
				</cfquery>
				<!--- コンファーム対象取得 --->
				<cfquery name="q_confirm" datasource = "#Application.datasource#">
					select
						questionnaire_id,
						options_no,
						confirm_q
					from
						mst_confirm
				</cfquery>
				<!--- シートＩＤ取得 --->
				<cfquery name="getActivesheet" datasource = "#Application.datasource#">
					select
						questionnairesheet_id
					from
						mst_questionnairesheet
					where
						resort_code = 10
					and
						active_flg = 1	
				</cfquery>
				<!--- 回答詳細情報格納用クエリ生成 --->
				<cfset retQ = QueryNew("referencenumber,questionnairesheet_id,questionnaire_id,answer,answertext")>

				<cfloop query="q_reserve">
					<cfset key_r_code = #q_reserve.referencenumber[currentrow]#>
			
					<cfloop query="q_confirm">
						<cfset key_q_id = #q_confirm.questionnaire_id[currentrow]#>
						<cfset key_c_id = #q_confirm.confirm_q[currentrow]#>
						<cfset key_o_no = #q_confirm.options_no[currentrow]#>
						
						<!--- 対象設問の回答チェック --->
						<cfquery name="q_answers" datasource = "#Application.datasource#">
							select
								HA.questionnaire_id,
								HA.referencenumber,
								HA.answer,
								HA.answertext
							from
								hst_answersdetails as HA
							where
								HA.referencenumber = '#key_r_code#'
							and
								HA.questionnaire_id = #key_q_id#
							and
								HA.answer IN (1,2,3,4,5,6,7)	
						</cfquery>
						<!--- 対象設問に回答アリ --->
						<cfif q_answers.recordcount neq 0>
							<cfquery name="q_confirmanswers" datasource = "#Application.datasource#">
								select
									HA.questionnaire_id,
									HA.referencenumber,
									HA.answer,
									HA.answertext
								from
									hst_answersdetails as HA
								where
									HA.referencenumber = '#key_r_code#'
								and
									HA.questionnaire_id = #key_c_id#
								and
									HA.answer = '#key_o_no#'
							</cfquery>
							
							<!--- 利用サービスの回答が無ければ登録する --->
							<cfif q_confirmanswers.recordcount eq 0>
								<cfquery name="qoq_reccheck" dbtype = "query">
									select
										referencenumber
									from
										retQ
									where
										referencenumber = '#key_r_code#'
									and
										questionnaire_id = #key_c_id#
									and
										answer = #key_o_no#
								</cfquery>
								<cfif qoq_reccheck.recordcount eq 0>
									<!--- 回答候補値テキスト取得 --->
									<cfquery name="q_answeroption" datasource = "#Application.datasource#">
										select
											options_label
										from
											mst_answeroptions
										where
											options_no = #key_o_no#
										and
											questionnaire_id = #key_c_id#
									</cfquery>
									<cfset QueryAddRow(retQ,1)>
									<cfset QuerySetCell(retQ,"referencenumber",#key_r_code#)>
									<cfset QuerySetCell(retQ,"questionnairesheet_id",#getActivesheet.questionnairesheet_id#)>
									<cfset QuerySetCell(retQ,"questionnaire_id",#key_c_id#)>
									<cfset QuerySetCell(retQ,"answer",#key_o_no#)>
									<cfset QuerySetCell(retQ,"answertext",#q_answeroption.options_label#)>
								</cfif>
							</cfif>	
						</cfif>
					</cfloop>	
				</cfloop>
				<!--- 	
				<cfquery name="qoq_reccheck" dbtype = "query">
					select
						*
					from
						retQ
					where
						questionnaire_id = 19
					and
						answer = 3
				</cfquery>
				<cfdump var=#qoq_reccheck#><cfabort>
			 --->
				<!---  --->
				<cfset cnt=0>
				<cfloop query="retQ">
					<cfquery name="q_answersdetails" datasource = "#Application.datasource#">
						select
							referencenumber
						from
							hst_answersdetails
						where
							referencenumber = '#referencenumber#'
						and
							questionnairesheet_id = #questionnairesheet_id#
						and
							questionnaire_id = #questionnaire_id#
						and
							answer = #answer#
					</cfquery>
					<cfif q_answersdetails.recordcount EQ 0>
						<cfset cnt=incrementvalue(cnt)>
						<cfquery name="setq_answersdetails" datasource = "#Application.datasource#">
							insert into
								hst_answersdetails(
									referencenumber,
									questionnairesheet_id,
									questionnaire_id,
									answer,
									answertext,
									othertext
							)values(
								'#referencenumber#',
								#questionnairesheet_id#,
								#questionnaire_id#,
								#answer#,
								'#answertext#',
								'importation'
							)
						</cfquery>
					</cfif>	
				</cfloop>
				
			</cftransaction>

			<!--- 戻値設定 --->
			<cfscript>
				executeinformation = StructNew();
				StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
				StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
				StructInsert(executeinformation,	"compFlg",		1								);
				StructInsert(executeinformation,	"recordcount",	#cnt#							);
			</cfscript>
			<cfreturn executeinformation>			

			<cfcatch type="any">
			
				<!--- 戻値設定 --->
				<cfscript>
					executeinformation = StructNew();
					StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
					StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
					StructInsert(executeinformation,	"compFlg",		0								);
				</cfscript>
				<cfreturn executeinformation>
			</cfcatch>
		</cftry>

	</cffunction>

	<!---
	*************************************************************************************************
	* Class				: utility
	* Method			: csvimport
	* Description		: 回答矛盾データ修正
	* Custom Attributes	: 
	* Return Paramters	: 
	* History			: 1) Coded by	2005/10/25 h.miyahara(NetFusion)
	*					: 2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="csvimport" access="remote" returntype="query" >
		<cfargument name="filepath" type="string" default="">
		<cfsetting enablecfoutputonly="yes">

<!--- 本番環境接続用 --->
<cfset Application.datasource = "hoshinoya">
<cfdump var=#Application#>
<cfabort>

		<cftry>
			<cfhttp method = "Get"
				url = "#arguments.filepath#"
				name = "answers"
				textqualifier = ""
				charset = "shift_jis"
				columns = "reserve_id,time_sart,time_end,key_cord1,key_cord3,qn_1,qn_2,qn_3,qn_4,qn_5,qn_6,qn_7,qn_8,qn_9,qe_9,nn_10,qn_11,nn_12,qn_13,qn_14,qe_14,qn_15,qn_16,qn_17,qn_18,qn_19,qn_20,qn_21,qn_22,qn_23,qn_24,qn_25,qn_26,qn_27,qn_28,qn_29,qn_30,qn_31,qn_32,qa_33,qb_33,qc_33,qd_33,qe_33,qf_33,qg_33,qh_33,qi_33,qj_33,ql_33,qm_33,qn_33,qo_33,qn_34,qn_35,qn_36,qn_37,qn_38,qn_39,qn_40,qn_41,qe_41,qn_42,qn_43,qn_44,qn_45,qn_46,qn_47,qn_48,qn_49,qn_50,qn_51,qn_52,qn_53,qn_54,qe_54,qn_55,qn_56,qn_57,qn_58,qn_59,qn_60,qn_61,qn_62,qn_63,qn_64,qe_64,qn_65,qn_66,qn_67,qn_68,qn_69,qn_70,qn_71,qn_72,qn_73,qn_74,qn_75,qn_76,qn_77,qn_78,qn_79,qn_80,qa_81,qb_81,qc_81,qd_81,qe_81,qf_81,qg_81,qh_81,qj_81,qk_81,ql_81,qm_81,qn_81,qo_81,qp_81,qq_81,qr_81,qn_82,qn_83,qn_84,qn_85,qn_86,qn_87,qn_88,qn_89,qn_90,qn_91,qn_92,qn_93,qn_94,qn_95,qn_96,qn_97,qn_98,qn_99,qn_100,qn_113,qn_114,qn_115,qn_116,qn_117,qn_118,qn_119,qn_120,qn_121,qn_122,qn_123,qn_124,qn_125,nn_101,qn_102,qn_103,qn_104,qn_105,qn_106,qa_107,qb_107,qc_107,qd_107,qe_107,qf_107,qg_107,qh_107,ni_107,qn_108,qn_109,qe_109,qn_110,qn_111,qe_111"
				delimiter = "#chr(9)#">
				<cftransaction action="BEGIN">
					<cfloop query="answers">
						<!--- 予約情報確認 --->
						<cfquery name="getReservedata" datasource = "#Application.datasource#">
							select
								referencenumber,
								arrive_date,
								depart_date
							from
								rtt_reserve
							where
								reserve_id = #Mid(reserve_id, 4, Len(reserve_id)-7)#
						</cfquery>
						
						<!--- 顧客情報確認 --->
						
						
					 	<cfif getReservedata.recordcount EQ 1>
						 		
							<!--- 回答情報テーブル内確認 --->
							<cfquery name="getAnswerdata" datasource = "#Application.datasource#">
								select
									referencenumber
								from
									hst_answers
								where
									referencenumber = '#getReservedata.referencenumber#'
							</cfquery>
						 	<cfif getAnswerdata.recordcount EQ 0>
						 	<!--- 
								予約番号【#Mid(reserve_id, 4, Len(reserve_id)-7)#】<br>
							 	予約コード【#getReservedata.referencenumber#】<br>
							 	到着日【#getReservedata.arrive_date#】<br>
							 	出発日【#getReservedata.depart_date#】<br>
							 	回答日【#DateFormat(Mid(time_end,1,10),'YYYY/MM/DD')#】<br>
							  --->	
							  
								<!--- 回答情報格納--->
								<cfquery name="setAnswersdata" datasource = "#Application.datasource#">
							 		insert into hst_answers(
							 			referencenumber,
							 			questionnairesheet_id,
							 			registrated,
							 			arrive_date,
							 			depart_date
							 		)values(
							 			'#getReservedata.referencenumber#',
							 			2,
							 			'#DateFormat(Mid(time_end,1,10),'YYYY/MM/DD')#',
							 			'#getReservedata.arrive_date#',
							 			'#getReservedata.depart_date#'
							 		)
								</cfquery>
							 
						 	</cfif>
							<cfloop index = "columnIndex" list = "#answers.columnList#">
								<cfset questionid = Mid(columnIndex, 4, Len(columnIndex))>
								<cfset optionno = #Evaluate(columnIndex)#>
								<cfset isnum = Mid(columnIndex, 1, 1)>
								
								<cfif Len(#Evaluate(columnIndex)#) NEQ 0 AND IsNumeric(questionid)>
									<!--- データ整形 --->
									<cfif questionid EQ 33>
										<cfif optionno EQ 10>		<cfset optionno = 13>
										<cfelseif optionno EQ 11>	<cfset optionno = 14>
										<cfelseif optionno EQ 12>	<cfset optionno = 10>
										<cfelseif optionno EQ 13>	<cfset optionno = 11>
										<cfelseif optionno EQ 14>	<cfset optionno = 12>
										<cfelse>					<cfset optionno = optionno>
										</cfif>
									</cfif>

									<cfif questionid EQ 81 AND getReservedata.arrive_date GTE "2004/12/01">
										<cfif optionno EQ 12>		<cfset optionno = 15>
										<cfelseif optionno EQ 13>	<cfset optionno = 16>
										<cfelseif optionno EQ 14>	<cfset optionno = 17>
										<cfelseif optionno EQ 15>	<cfset optionno = 18>
										<cfelseif optionno EQ 16>	<cfset optionno = 12>
										<cfelseif optionno EQ 17>	<cfset optionno = 13>
										<cfelse>					<cfset optionno = optionno>
										</cfif>
									<cfelseif questionid EQ 81>
										<cfif optionno EQ 8>		<cfset optionno = 19>
										<cfelseif optionno EQ 12>	<cfset optionno = 15>
										<cfelseif optionno EQ 13>	<cfset optionno = 16>
										<cfelseif optionno EQ 14>	<cfset optionno = 17>
										<cfelseif optionno EQ 15>	<cfset optionno = 18>
										<cfelseif optionno EQ 16>	<cfset optionno = 12>
										<cfelseif optionno EQ 17>	<cfset optionno = 13>
										<cfelse>					<cfset optionno = optionno>
										</cfif>
									</cfif>

									<cfif IsNumeric(optionno) AND isnum NEQ "N">
					
										<!--- 回答ラベル取得 --->
										<cfquery name="getAnswerOptions" datasource = "#Application.datasource#">
											select
												options_label
											from
												mst_answeroptions
											where
												questionnaire_id = #questionid#
											and
												options_no = #optionno#
										</cfquery>
										<cfset answer = optionno>
										<cfset answerText = getAnswerOptions.options_label>
									<cfelse>
										<cfset answer = 0>
										<cfset illigalchr = """">
										<cfset changechr = "">
										<cfset answerText = ReplaceList(#Evaluate(columnIndex)#,illigalchr,changechr)>
										<!--- <cfset answerText = #Evaluate(columnIndex)#> --->
									</cfif>
									<!--- 
										<cfoutput>
										#isnum#【#questionid#】:【回答値：#answer#】【回答：#answerText#】 #columnIndex#<br>
				 						</cfoutput>
				 					--->
				 					
									<!--- 回答データ格納	--->
									<cfquery name="setAnswers" datasource = "#Application.datasource#">
								 		insert into hst_answersdetails(
								 			referencenumber,
								 			questionnairesheet_id,
								 			questionnaire_id,
								 			answer,
								 			answertext
								 		)values(
								 			'#getReservedata.referencenumber#',
								 			2,
								 			#questionid#,
								 			#answer#,
								 			'#answerText#'
								 		)
									</cfquery>
									
								</cfif>
							</cfloop>
				 		</cfif>
					</cfloop>
				</cftransaction>
			<cfreturn answers>

			<cfsetting enablecfoutputonly="No">
			<cfcatch type="any">
		
				<cfoutput>
					データコンバート中にエラーが発生しました。<br>
					登録が正しく行えませんでした。<br>
					#cfcatch.detail#
				</cfoutput>
				<cf_error_log
					label = "getUserProfile"
					errorType = "#cfcatch.type#"
					message = "#cfcatch.message#"
					detail = "#cfcatch.detail#">
				<cfsetting enablecfoutputonly="No">
				<cfabort>
				<cfrethrow>
			</cfcatch>
		</cftry>

	</cffunction>

	<!---
	*************************************************************************************************
	* Class				: utility
	* Method			: expData
	* Description		: 回答矛盾データ修正
	* Custom Attributes	: 
	* Return Paramters	: 
	* History			: 1) Coded by	2005/10/19 h.miyahara(NetFusion)
	*					: 2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="expData" access="remote" returntype="any">
		<cfargument name="condition" type="numeric" default="">
		<cfargument name="bdate" type="string" default="">
		<cfargument name="edate" type="string" default="">
		<cfargument name="basedata" type="array" default="">
		<cfargument name="referencenumber" type="string" default="">
		<cftry>
			<!--- 回答者予約番号取得 --->
			<cfquery name="q_hst_answersdetails" datasource = "#Application.datasource#">
				select
					ad.answer,
					ad.answertext,
					ad.questionnaire_id,
					a.referencenumber,
					a.registrated,
					a.arrive_date,
					a.depart_date
				from
					hst_answersdetails ad,
					hst_answers a
				where
					a.referencenumber = ad.referencenumber
				and
				<cfif arguments.condition EQ 1>	
					a.depart_date between '#arguments.bdate#' and '#arguments.edate#'
				<cfelse>
					a.referencenumber = '#arguments.referencenumber#'
				</cfif>
			</cfquery>
			<cfif q_hst_answersdetails.recordcount EQ 0>
				<cfreturn q_hst_answersdetails>
			</cfif>
			<cfset temp_rcd = valuelist(q_hst_answersdetails.referencenumber)>
			<cfset list_rcd = "">
			<cfloop list="#temp_rcd#" index="idx">
				<cfif ListFind(list_rcd,LTrim(idx)) EQ 0>
					<cfset list_rcd = ListAppend(list_rcd,idx)>
				</cfif>
			</cfloop>
			<cfquery name="q_enquete" datasource = "#Application.datasource#">
				select
					B.questionnaire_id,
					B.questionnairesheet_id,
					B.viewtype,
					case when B.answertype is not null then B.answertype
					     else '2'
					end  "answertype",
					B.answertype,
					A.questionnaire_nm,
					A.questionnaire_type,
					C.questionnairecategory_nm,
					C.questionnairecategory_id,
					B.viewsequence,
					B.scenario,
					B.active_flg
				from
					mst_questionnaire as A,
					oth_sheetconfiguration as B,
					mst_questionnairecategory as C,
					mst_questionnairesheet as D
				where
					A.questionnaire_id = B.questionnaire_id AND
					A.questionnairecategory_id = C.questionnairecategory_id AND
					A.questionnairecategory_id = C.questionnairecategory_id AND
					A.active_flg = 1 AND
					B.active_flg = 1 AND 
					B.questionnairesheet_id = D.questionnairesheet_id AND
					B.owner = 'Master' AND
					D.active_flg = 1
				order by
					B.viewsequence
			</cfquery>
			<cfset temp_qid = valuelist(q_enquete.questionnaire_id)>
			<cfset temp_qtype = valuelist(q_enquete.answertype)>
			<cfset list_qid = "">
			<cfset list_qtxt = "">
			<cfset cnt=1>
			<cfloop list="#temp_qid#" index="idx">
				<cfif ListFind(list_qid,LTrim(idx)) EQ 0>
					<cfif ListGetAt(temp_qtype,cnt) EQ '0'>
						<cfquery name="q_answeroption" datasource = "#Application.datasource#">
							select
								options_no,
								options_label
							from
								mst_answeroptions
							where
								questionnaire_id = #idx#
							order by
								options_no
						</cfquery>
						<!--- 
						<cfset list_qid = ListAppend(list_qid,"cl_#idx#")>
						 --->
						<cfset list_qid = ListAppend(list_qid,"cl_#idx#_0")>
						<cfloop query="q_answeroption">
							<cfset list_qid = ListAppend(list_qid,"cl_#idx#_#options_no#")>
						</cfloop>
					<cfelse>	
						<cfset list_qid = ListAppend(list_qid,"cl_#idx#")>
					</cfif>
				</cfif>
				<cfset cnt = incrementvalue(cnt)>
			</cfloop>
			<cfset retQ = QueryNew("referencenumber,#list_qid#,endflg")>
<!--- 2006/04/17 h.miyahara(NetFusion) MOD 基幹データ出力 B --->
<!---  --->	
			<cfset reserve_field = "">
			<cfset reserve_headertext = "">
			<cfset customer_field = "">
			<cfset customer_headertext = "">
			<cfset marketing_field = "">
			<cfset marketing_headertext = "">
			<cfset shopresult_field = "">
			<cfset shopresult_headertext = "">
			<cfloop from="1" to="#ArrayLen(arguments.basedata)#" index="i">
				<cfif ListGetAt(arguments.basedata[i],3,'\') EQ "rtt_reserve">
					<cfset reserve_field = ListAppend(reserve_field,#ListGetAt(arguments.basedata[i],1,'\')#)>
					<cfset reserve_headertext = ListAppend(reserve_headertext,#ListGetAt(arguments.basedata[i],2,'\')#)>
				</cfif>
				<cfif ListGetAt(arguments.basedata[i],3,'\') EQ "rtm_customer">
					<cfset customer_field = ListAppend(customer_field,#ListGetAt(arguments.basedata[i],1,'\')#)>
					<cfset customer_headertext = ListAppend(customer_headertext,#ListGetAt(arguments.basedata[i],2,'\')#)>
				</cfif>
				<cfif ListGetAt(arguments.basedata[i],3,'\') EQ "rtt_marketing">
					<cfset marketing_field = ListAppend(marketing_field,#ListGetAt(arguments.basedata[i],1,'\')#)>
					<cfset marketing_headertext = ListAppend(marketing_headertext,#ListGetAt(arguments.basedata[i],2,'\')#)>
				</cfif>
				<cfif ListGetAt(arguments.basedata[i],3,'\') EQ "rtt_shopresult">
					<cfset shopresult_field = ListAppend(shopresult_field,#ListGetAt(arguments.basedata[i],1,'\')#)>
					<cfset shopresult_headertext = ListAppend(shopresult_headertext,#ListGetAt(arguments.basedata[i],2,'\')#)>
				</cfif>
				<cfset QueryAddColumn(retQ,"#ListGetAt(arguments.basedata[i],1,'\')#",ArrayNew(1))>
			</cfloop>
<!--- 
			<cfset QueryAddColumn(retQ,"nights",ArrayNew(1))>
			<cfset QueryAddColumn(retQ,"manpersoncount",ArrayNew(1))>
 --->		

			
<!--- 2006/04/17 h.miyahara(NetFusion) MOD 基幹データ出力 E --->			

			<!--- ＣＳＶ列表示用 --->
			<cfset QueryAddRow(retQ,1)>
			<cfloop list="#temp_qid#" index="idx">
				<cfset QuerySetCell(retQ,"referencenumber","referencenumber")>
				<cfset QuerySetCell(retQ,"endflg","EOF")>
				<cfquery name="qoq_q_enquete" dbtype="query">
					select
						questionnaire_id,
						questionnaire_nm,
						questionnairecategory_nm,
						answertype
					from
						q_enquete
					where
						questionnaire_id = #idx#
				</cfquery>
				<cfif qoq_q_enquete.recordcount neq 0>
					<cfif qoq_q_enquete.answertype EQ 0>
						<cfquery name="q_answeroption" datasource = "#Application.datasource#">
							select
								options_no,
								options_label
							from
								mst_answeroptions
							where
								questionnaire_id = #qoq_q_enquete.questionnaire_id#
							order by
								options_no
						</cfquery>
						<cfset tmpname="<"& qoq_q_enquete.questionnairecategory_nm & ">" & qoq_q_enquete.questionnaire_nm & "未回答">
						<cfset QuerySetCell(retQ,"cl_#idx#_0",tmpname)>
						<cfloop query="q_answeroption">
							<cfset tmpname="<"& qoq_q_enquete.questionnairecategory_nm & ">" & qoq_q_enquete.questionnaire_nm & q_answeroption.options_label>
							<cfset QuerySetCell(retQ,"cl_#idx#_#options_no#",tmpname)>
						</cfloop>
					<cfelse>
						<cfset tmpname="<"& qoq_q_enquete.questionnairecategory_nm & ">" & qoq_q_enquete.questionnaire_nm>
						<cfset QuerySetCell(retQ,"cl_#idx#",tmpname)>
					</cfif>
				</cfif>

<!--- 2006/04/17 h.miyahara(NetFusion) MOD 基幹データ出力 B --->
				<cfloop list="#reserve_field#" index="idx">
					<cfset l_index = ListFind(reserve_field,idx)>
					<cfset QuerySetCell(retQ,"#idx#",#ListGetAt(reserve_headertext,l_index)#)>
				</cfloop>
				<cfloop list="#customer_field#" index="idx">
					<cfset l_index = ListFind(customer_field,idx)>
					<cfset QuerySetCell(retQ,"#idx#",#ListGetAt(customer_headertext,l_index)#)>
				</cfloop>
				<cfloop list="#marketing_field#" index="idx">
					<cfset l_index = ListFind(marketing_field,idx)>
					<cfset QuerySetCell(retQ,"#idx#",#ListGetAt(marketing_headertext,l_index)#)>
				</cfloop>
				<cfloop list="#shopresult_field#" index="idx">
					<cfset l_index = ListFind(shopresult_field,idx)>
					<cfset QuerySetCell(retQ,"#idx#",#ListGetAt(shopresult_headertext,l_index)#)>
				</cfloop>
<!--- 2006/04/17 h.miyahara(NetFusion) MOD 基幹データ出力 E --->
			</cfloop>

			<cfset temp_answers = "">
			<cfset rownum = 2>
			<cfloop list="#list_rcd#" index="r_cd">
				<cfset QueryAddRow(retQ,1)>
				<cfquery name="qoq_hst_answersdetails" dbtype="query">
					select
						answer,
						answertext,
						referencenumber,
						questionnaire_id
					from
						q_hst_answersdetails
					where
						referencenumber = '#r_cd#'
					order by
						questionnaire_id
				</cfquery>
				<cfset QuerySetCell(retQ,"referencenumber",#qoq_hst_answersdetails.referencenumber#)>
				<cfset QuerySetCell(retQ,"endflg","EOF")>
				<cfloop query="qoq_hst_answersdetails">
					<cfset idx = qoq_hst_answersdetails.questionnaire_id[currentrow]>
					<cfif ListFind(temp_qid,idx) NEQ 0>
						<cfif ListFind(temp_qid,questionnaire_id) NEQ 0>
							<cfset tmp_type = ListGetAt(temp_qtype,ListFind(temp_qid, idx))>
							<cfif tmp_type EQ 2>
								<cfset tempcell=#Evaluate("retQ.cl_"&idx&"[rownum+1]")#>
								<cfset QuerySetCell(retQ,"cl_#idx#",ListAppend(tempcell,answer,';'))>
							<cfelseif tmp_type EQ 0>
								<cfset tempcell=#Evaluate("retQ.cl_"&idx&"_"&answer&"[rownum+1]")#>
								<cfset QuerySetCell(retQ,"cl_#idx#_#answer#",ListAppend(tempcell,answer,';'))>
							<cfelse>
								<cfset tempcell=#Evaluate("retQ.cl_"&idx&"[rownum+1]")#>
								<cfset QuerySetCell(retQ,"cl_#idx#",ListAppend(tempcell,replace(answertext,"#chr(44)#","，","ALL"),';'))>
							</cfif>
						<cfelse>
							<cfset QuerySetCell(retQ,"cl_#idx#","-")>
						</cfif>
					</cfif>
				</cfloop>

<!--- 2006/04/17 h.miyahara(NetFusion) MOD 基幹データ出力 B --->
				<!--- 予約テーブルの情報取得＆セット --->
				<cfset temp_reserve_field = "customernumber,room_code,reserve_code">
				<cfif ListLen(reserve_field) NEQ 0>
					<cfset temp_reserve_field = ListAppend(temp_reserve_field,reserve_field)>
				</cfif>
				<cfquery name="q_htt_reserve" datasource = "#Application.datasource#">
					select 
						#temp_reserve_field#
					from
						rtt_reserve
					where
						referencenumber = '#r_cd#'
				</cfquery>
				<cfif ListLen(reserve_field) NEQ 0>
					<cfloop list="#reserve_field#" index="idx">
						<cfset QuerySetCell(retQ,"#idx#",#Evaluate("q_htt_reserve."&idx)#)>
					</cfloop>
				</cfif>	
				<!--- 顧客テーブルの情報取得＆セット --->
				<cfif q_htt_reserve.recordcount NEQ 0 AND ListLen(customer_field) NEQ 0>
					<cfquery name="q_rtm_customer" datasource = "#Application.datasource#">
						select #customer_field# 
						from
							rtm_customer
						where
							customernumber = '#q_htt_reserve.customernumber#'
					</cfquery>
					<cfloop list="#customer_field#" index="idx">
						<cfset QuerySetCell(retQ,"#idx#",#Evaluate("q_rtm_customer."&idx)#)>
					</cfloop>
				</cfif>
				<!--- マーケティング情報取得＆セット --->
				<cfif q_htt_reserve.recordcount NEQ 0 AND ListLen(customer_field) NEQ 0>
					<cfset temp_controldiv_id = "4,5,6">
					<cfquery name="q_rtt_marketing" datasource = "#Application.datasource#">
						select
							m.control_id,
							m.controldiv_id,
							c.control_nm
						from
							rtt_marketing as m,
							mst_control as c
						where
							m.reserve_code = #q_htt_reserve.reserve_code#
						and
							m.room_code = #q_htt_reserve.room_code#
						and
							m.controldiv_id in (#temp_controldiv_id#)	
						and
							m.controldiv_id  = c.controldiv_id
						and
							m.control_id  = c.control_id
					</cfquery>
					<cfif ListFind(marketing_field,"marketing_1")>
						<cfquery name="qoq_rtt_marketing" dbtype = "query">
							select control_nm
							from
								q_rtt_marketing
							where
								controldiv_id = 4
						</cfquery>
						<cfset QuerySetCell(retQ,"marketing_1",#qoq_rtt_marketing.control_nm#)>
					</cfif>
					<cfif ListFind(marketing_field,"marketing_2")>
						<cfquery name="qoq_rtt_marketing" dbtype = "query">
							select control_nm
							from
								q_rtt_marketing
							where
								controldiv_id = 5
						</cfquery>
						<cfset QuerySetCell(retQ,"marketing_2",#qoq_rtt_marketing.control_nm#)>
					</cfif>
					<cfif ListFind(marketing_field,"marketing_3")>
						<cfquery name="qoq_rtt_marketing" dbtype = "query">
							select control_nm
							from
								q_rtt_marketing
							where
								controldiv_id = 6
						</cfquery>
						<cfset QuerySetCell(retQ,"marketing_3",#qoq_rtt_marketing.control_nm#)>
					</cfif>
				</cfif>
				<!--- マーケティング情報取得＆セット --->
				<cfif q_htt_reserve.recordcount NEQ 0 AND ListLen(customer_field) NEQ 0>
					<cfquery name="q_rtt_shopresult" datasource = "#Application.datasource#">
						select
							shop_id,
							convert(varchar,shop_id) + '-' + shop_nm + '-' + convert(varchar,amount) as shop_result
						from
							rtt_shopresult
						where
							reserve_code = #q_htt_reserve.reserve_code#
						and
							room_code = #q_htt_reserve.room_code#
					</cfquery>
					<cfif ListFind(shopresult_field,"shopresult_01")>
						<cfquery name="qoq_rtt_shopresul" dbtype = "query">
							select shop_result
							from
								q_rtt_shopresult
							where
								shop_id = 10
						</cfquery>
						<cfif qoq_rtt_shopresul.recordcount NEQ 0>
							<cfset QuerySetCell(retQ,"shopresult_01",#qoq_rtt_shopresul.shop_result#)>
						</cfif>
					</cfif>
					<cfif ListFind(shopresult_field,"shopresult_02")>
						<cfquery name="qoq_rtt_shopresul" dbtype = "query">
							select shop_result
							from
								q_rtt_shopresult
							where
								shop_id = 20
						</cfquery>
						<cfif qoq_rtt_shopresul.recordcount NEQ 0>
							<cfset QuerySetCell(retQ,"shopresult_02",#qoq_rtt_shopresul.shop_result#)>
						</cfif>
					</cfif>
					<cfif ListFind(shopresult_field,"shopresult_03")>
						<cfquery name="qoq_rtt_shopresul" dbtype = "query">
							select shop_result
							from
								q_rtt_shopresult
							where
								shop_id = 11
						</cfquery>
						<cfif qoq_rtt_shopresul.recordcount NEQ 0>
							<cfset QuerySetCell(retQ,"shopresult_03",#qoq_rtt_shopresul.shop_result#)>
						</cfif>
					</cfif>
					<cfif ListFind(shopresult_field,"shopresult_04")>
						<cfquery name="qoq_rtt_shopresul" dbtype = "query">
							select shop_result
							from
								q_rtt_shopresult
							where
								shop_id = 14
						</cfquery>
						<cfif qoq_rtt_shopresul.recordcount NEQ 0>
							<cfset QuerySetCell(retQ,"shopresult_04",#qoq_rtt_shopresul.shop_result#)>
						</cfif>
					</cfif>
					<cfif ListFind(shopresult_field,"shopresult_05")>
						<cfquery name="qoq_rtt_shopresul" dbtype = "query">
							select shop_result
							from
								q_rtt_shopresult
							where
								shop_id = 12
						</cfquery>
						<cfif qoq_rtt_shopresul.recordcount NEQ 0>
							<cfset QuerySetCell(retQ,"shopresult_05",#qoq_rtt_shopresul.shop_result#)>
						</cfif>
					</cfif>
					<cfif ListFind(shopresult_field,"shopresult_06")>
						<cfquery name="qoq_rtt_shopresul" dbtype = "query">
							select shop_result
							from
								q_rtt_shopresult
							where
								shop_id = 90
						</cfquery>
						<cfif qoq_rtt_shopresul.recordcount NEQ 0>
							<cfset QuerySetCell(retQ,"shopresult_06",#qoq_rtt_shopresul.shop_result#)>
						</cfif>
					</cfif>
					<cfif ListFind(shopresult_field,"shopresult_07")>
						<cfquery name="qoq_rtt_shopresul" dbtype = "query">
							select shop_result
							from
								q_rtt_shopresult
							where
								shop_id = 91
						</cfquery>
						<cfif qoq_rtt_shopresul.recordcount NEQ 0>
							<cfset QuerySetCell(retQ,"shopresult_07",#qoq_rtt_shopresul.shop_result#)>
						</cfif>
					</cfif>
					<cfif ListFind(shopresult_field,"shopresult_08")>
						<cfquery name="qoq_rtt_shopresul" dbtype = "query">
							select shop_result
							from
								q_rtt_shopresult
							where
								shop_id = 92
						</cfquery>
						<cfif qoq_rtt_shopresul.recordcount NEQ 0>
							<cfset QuerySetCell(retQ,"shopresult_08",#qoq_rtt_shopresul.shop_result#)>
						</cfif>
					</cfif>
				</cfif>
<!--- 2006/04/17 h.miyahara(NetFusion) MOD 基幹データ出力 E --->
				<cfset rownum = incrementvalue(rownum)>
			</cfloop>

			<cfset newline=chr(13) & chr(10)>
<!--- 
			<cfset qtemp_list="referencenumber,#list_qid#,endflg">
 --->
			<cfset qtemp_list="referencenumber,#list_qid#">
			<cfset outputdata = "">
			<cfloop query="retQ">
				<cfset tempdata = "">
				<cfloop list="#qtemp_list#" index="idx">
					<cfset tmpstr = Evaluate("retQ."&idx&"[#currentrow#]")&#chr(9)#>
					<cfset tmpstr = #replacelist(tmpstr,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
					<cfset tmpstr = #replace(tmpstr,"#chr(44)#","，","ALL")#>
					<cfset tempdata = ListAppend(tempdata,tmpstr)>
					<cfset tempdata = #replacelist(tempdata,"#chr(9)#","")#>
				</cfloop>
<!--- 2006/04/17 h.miyahara(NetFusion) MOD 基幹データ出力 B --->
				<cfif ListLen(reserve_field) NEQ 0>
					<cfloop list="#reserve_field#" index="idx">
						<cfset tmpstr = Evaluate("retQ."&idx&"[#currentrow#]")&#chr(9)#>
						<cfset tmpstr = #replacelist(tmpstr,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
						<cfset tmpstr = #replace(tmpstr,"#chr(44)#","，","ALL")#>
						<cfset tempdata = ListAppend(tempdata,tmpstr)>
						<cfset tempdata = #replacelist(tempdata,"#chr(9)#","")#>
					</cfloop>
				</cfif>
				<cfif ListLen(customer_field) NEQ 0>
					<cfloop list="#customer_field#" index="idx">
						<cfset tmpstr = Evaluate("retQ."&idx&"[#currentrow#]")&#chr(9)#>
						<cfset tmpstr = #replacelist(tmpstr,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
						<cfset tmpstr = #replace(tmpstr,"#chr(44)#","，","ALL")#>
						<cfset tempdata = ListAppend(tempdata,tmpstr)>
						<cfset tempdata = #replacelist(tempdata,"#chr(9)#","")#>
					</cfloop>
				</cfif>
				<cfif ListLen(marketing_field) NEQ 0>
					<cfloop list="#marketing_field#" index="idx">
						<cfset tmpstr = Evaluate("retQ."&idx&"[#currentrow#]")&#chr(9)#>
						<cfset tmpstr = #replacelist(tmpstr,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
						<cfset tmpstr = #replace(tmpstr,"#chr(44)#","，","ALL")#>
						<cfset tempdata = ListAppend(tempdata,tmpstr)>
						<cfset tempdata = #replacelist(tempdata,"#chr(9)#","")#>
					</cfloop>
				</cfif>
				<cfif ListLen(shopresult_field) NEQ 0>
					<cfloop list="#shopresult_field#" index="idx">
						<cfset tmpstr = Evaluate("retQ."&idx&"[#currentrow#]")&#chr(9)#>
						<cfset tmpstr = #replacelist(tmpstr,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
						<cfset tmpstr = #replace(tmpstr,"#chr(44)#","，","ALL")#>
						<cfset tempdata = ListAppend(tempdata,tmpstr)>
						<cfset tempdata = #replacelist(tempdata,"#chr(9)#","")#>
					</cfloop>
				</cfif>
				<cfset tmpstr = Evaluate("retQ.endflg[#currentrow#]")&#chr(9)#>
				<cfset tmpstr = #replacelist(tmpstr,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
				<cfset tmpstr = #replace(tmpstr,"#chr(44)#","，","ALL")#>
				<cfset tempdata = ListAppend(tempdata,tmpstr)>
				<cfset tempdata = #replacelist(tempdata,"#chr(9)#","")#>
<!--- 2006/04/17 h.miyahara(NetFusion) MOD 基幹データ出力 E --->
				
				
				
				<cfset outputdata = outputdata&newline&tempdata>
			</cfloop>

<cfsetting enablecfoutputonly="yes">
<cfcontent type="text/csv; charset=SHIFT_JIS">
<cfheader name="Content-Disposition" value="filename=answers.csv">
<cfoutput>#outputdata#</cfoutput>
<cfsetting enablecfoutputonly="no">
<cfabort>

			<!--- 戻値設定 --->
			<cfscript>
				executeinformation = StructNew();
				StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
				StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
				StructInsert(executeinformation,	"compFlg",		1								);
				StructInsert(executeinformation,	"retQ",			retQ							);
			</cfscript>
			<cfreturn executeinformation>			

			<cfcatch type="any">
<cfdump var=#cfcatch#>
<cfabort>			
				<!--- 戻値設定 --->
				<cfscript>
					executeinformation = StructNew();
					StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
					StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
					StructInsert(executeinformation,	"compFlg",		0								);
				</cfscript>
				<!--- <cfreturn executeinformation> --->
			</cfcatch>
		</cftry>

	</cffunction>

	<!---
	*************************************************************************************************
	* Class				: utility
	* Method			: expData
	* Description		: 回答矛盾データ修正
	* Custom Attributes	: 
	* Return Paramters	: 
	* History			: 1) Coded by	2005/10/19 h.miyahara(NetFusion)
	*					: 2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="setbaselist" access="remote" returntype="Array">
		<cfargument name="data" type="Struct" default="">
		<cfset temp_fieldnames = arguments.data.fieldnames>
		<cfset temp_setfield = ArrayNew(1)>
		<cfloop list="#temp_fieldnames#" index="idx">
			<cfif mid(idx,1,3) EQ "bd_">
				<cfset temp_data = Evaluate("form."&idx)>
				<cfset ArrayAppend(temp_setfield,temp_data)>
			</cfif>
		</cfloop>

		<cfreturn temp_setfield>

	</cffunction>
</cfcomponent>


