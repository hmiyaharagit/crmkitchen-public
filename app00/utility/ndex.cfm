<!--- password --->
<cfset password="qawsed">
<cfif (isDefined("session.login") eq false or session.login eq false) and right(cgi.SCRIPT_NAME,9) neq "ndex.cfm">
	<cflocation url="ndex.cfm" addtoken="no">
</cfif>
<cfif isdefined("form.pwd")>
	<cfif form.pwd eq password>
		<cfset session.login=true>
	</cfif>
<cfelseif isdefined("form.logout")>
	<cfset session.login=false>
</cfif>

<cfif isdefined("form.pwd")>
	<cfif form.pwd eq password>
		<cfset session.login=true>
	</cfif>
<cfelseif isdefined("form.logout")>
	<cfset session.login=false>
</cfif>

<!--- 初期値設定 --->
<cfset sub_button1_message = "">
<cfset sub_button2_message = "">
<cfset sub_button3_message = "データのエクスポートを行います。">
<cfset onloadjs = "controlInput();">

<cfif isDefined("session.login") eq false or session.login eq false>
	<cfoutput>
	<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
	<title>CRMKitchen ユーティリティ</title>
	<link rel="stylesheet" href="configcss.css">
	</head>
	<body>
	<form method="post" action="ndex.cfm">
	<table width="100%" border="1" cellspacing="0" cellpadding="0" class="header">
		<tr> 
			<td>CRMKitchen ユーティリティ ログイン</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
	</table>
	<p>&nbsp;</p>
	<table width="100%" border="1" cellspacing="0" cellpadding="0" class="elm">
		<tr>
			<td>Password <input type="password" name="pwd"><input type="submit" value="login"></td>
		</tr>
	</table>
	</form>
	</body>
	</html>
	</cfoutput>
<cfelse>

	<!--- データ修正処理 --->
	<cfif IsDefined("form.sub_button1")>
		<cfinvoke component = "utility"
			method			= "cleanseData"
			returnVariable	= "retData">
	</cfif>

	<!--- データコンバート処理 --->
	<cfif IsDefined("form.sub_button2")>
		<!--- エラークエリ --->
		<cfset ErrorQ = QueryNew("ErrNumber,message,detail")>

		<!--- ファイルのアップロード〜読込 --->
		<cfinvoke component="csv_entry" method="csvUpload" returnVariable="loadCheck">
		</cfinvoke>
		
		<!--- ファイルアップロード成功 --->
		<cfif loadCheck.compFlg EQ 1>
			<!--- ファイルの読込 --->
			<cfinvoke component="csv_entry" method="csvRead" returnVariable="readCheck">
				<cfinvokeargument name="fileName" value=#loadCheck.fileName#>
				<cfinvokeargument name="ErrorQ" value=#ErrorQ#>
			</cfinvoke>
			
			<!--- 読込成功 --->
			<cfif readCheck.compFlg EQ 1>
				<!--- データチェック 
				<cfinvoke component="csv_entry" method="csvCheck" returnVariable="readCheck">
					<cfinvokeargument name="fileName" value=#loadCheck.fileName#>
					<cfinvokeargument name="ErrorQ" value=#ErrorQ#>
					<cfinvokeargument name="csvData" value=#readCheck.csvData#>
				</cfinvoke>
				--->
			</cfif>
		<!--- ファイルアップロード失敗 --->
		<cfelse>
			<cfabort>
		</cfif>


		<!--- エラー存在ファイル削除処理 --->
		<cfif ErrorQ.recordcount GT 0>

			<cfif IsDefined("loadCheck.fileName")>
				<cfinvoke component="csv_entry" method="csvDelete" returnVariable="readCheck">
					<cfinvokeargument name="fileName" value=#loadCheck.fileName#>
				</cfinvoke>
			</cfif>

			<cfset onloadjs = "ErrorMsg();">
		
		<cfelse>
			<!---  
			<cfdump var=#readCheck.csvData# label="登録内容"><cfabort>
			--->
			<!--- 登録処理 --->
			<cfinvoke component="csv_entry" method="csvEntry" returnVariable="regCheck">
				<cfinvokeargument name="csvData" value=#readCheck.csvData#>
			</cfinvoke>

			<!--- ファイル削除処理 --->
			<cfinvoke component="csv_entry" method="csvDelete" returnVariable="readCheck">
				<cfinvokeargument name="fileName" value=#loadCheck.fileName#>
			</cfinvoke>
			
			<cfif regCheck.compFlg EQ 1>
				<cfset onloadjs = "alert(MSG_ENTRY_END);window.parent.close();">
			</cfif>
		</cfif>
	</cfif>

	<!--- ＣＳＶエクスポート --->
	<cfif IsDefined("form.sub_button3")>
		<cfset sub_button3_message = "データのエクスポートを行います。">
		<cfset tmpbdate = DateFormat(CreateDate(#form.b_year#,#form.b_month#,#form.b_date#),'YYYY/MM/DD')>
		<cfset tmpedate = DateFormat(CreateDate(#form.e_year#,#form.e_month#,#form.e_date#),'YYYY/MM/DD')>
	
		<cfinvoke component = "utility"
			method			= "expData"
			condition = #form.condition#
			bdate = "#tmpbdate#"
			edate = "#tmpedate#"
			referencenumber = "#form.referencenumber#"
			returnVariable	= "retData">
		<cfif retData.recordcount EQ 0>
			<cfset onloadjs = "controlInput();alert('該当データはありませんでした');">
		</cfif>
	</cfif>

	<!--- データコンバート処理 --->
	<cfif IsDefined("form.sub_button4")>
		<!--- エラークエリ --->
		<cfset ErrorQ = QueryNew("ErrNumber,message,detail")>

		<!--- ファイルのアップロード〜読込 --->
		<cfinvoke component="base_entry" method="csvUpload" returnVariable="loadCheck">
		</cfinvoke>
		
		<!--- ファイルアップロード成功 --->
		<cfif loadCheck.compFlg EQ 1>
			<!--- ファイルの読込 --->
			<cfinvoke component="base_entry" method="csvRead" returnVariable="readCheck">
				<cfinvokeargument name="fileName" value=#loadCheck.fileName#>
				<cfinvokeargument name="ErrorQ" value=#ErrorQ#>
			</cfinvoke>
			
			<!--- 読込成功 --->
			<cfif readCheck.compFlg EQ 1>
			</cfif>
		<!--- ファイルアップロード失敗 --->
		<cfelse>
			<cfabort>
		</cfif>

		<!--- エラー存在ファイル削除処理 --->
		<cfif ErrorQ.recordcount GT 0>

			<cfif IsDefined("loadCheck.fileName")>
				<cfinvoke component="base_entry" method="csvDelete" returnVariable="readCheck">
					<cfinvokeargument name="fileName" value=#loadCheck.fileName#>
				</cfinvoke>
			</cfif>

			<cfset onloadjs = "ErrorMsg();">
		
		<cfelse>
			<!---  --->
			<cfdump var=#readCheck.csvData# label="登録内容"><cfabort>
			
			<!--- 登録処理 --->
			<cfinvoke component="base_entry" method="csvEntry" returnVariable="regCheck">
				<cfinvokeargument name="csvData" value=#readCheck.csvData#>
			</cfinvoke>

			<!--- ファイル削除処理 --->
			<cfinvoke component="base_entry" method="csvDelete" returnVariable="readCheck">
				<cfinvokeargument name="fileName" value=#loadCheck.fileName#>
			</cfinvoke>
			
			<cfif regCheck.compFlg EQ 1>
				<cfset onloadjs = "alert(MSG_ENTRY_END);window.parent.close();">
			</cfif>
		</cfif>
	</cfif>
	
	<cfoutput>
	<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
	<title>CRMKitchen ユーティリティ</title>
	<link rel="stylesheet" href="configcss.css">
	</head>
	<script language="JavaScript" type="text/JavaScript">
		function sub_buttonCheck(msg) {
			return confirm(msg);
		}

		function fileup_msg(){
			tmp = document.forms[0].file_name.value;
			if(tmp.length == 0){
				alert("ファイルを指定して下さい。");
				return false;
			}else{
				flag = confirm(tmp+"を登録します。\nよろしいですか？");
				if(flag == true){
					return true;
					//alert("登録しました。");
					//window.close();
				}else{
					alert("登録をキャンセルしました。");
					return false;
				}
			}
		
		}

		function diffcheck() {
			var s1 = document.exportcsv.b_year;
			var v1 = s1.options(s1.selectedIndex).value;
			var s2 = document.exportcsv.b_month;
			var v2 = s2.options(s2.selectedIndex).value;
			var s3 = document.exportcsv.b_date;
			var v3 = s3.options(s3.selectedIndex).value;
			var s4 = document.exportcsv.e_year;
			var v4 = s4.options(s4.selectedIndex).value;
			var s5 = document.exportcsv.e_month;
			var v5 = s5.options(s5.selectedIndex).value;
			var s6 = document.exportcsv.e_date;
			var v6 = s6.options(s6.selectedIndex).value;

			var tempSStr = v1.toString()+(v2.toString())+(v3.toString())
			var tempEStr = v4.toString()+(v5.toString())+(v6.toString())
			var start = Number(tempEStr)-Number(tempSStr)

			if (start<0) {
				alert("期間指定が矛盾しています。正しく設定して下さい。");
				return false;
			}
		}		
		function controlInput() {
			var chkObj = window.exportcsv("condition");
			if(chkObj != null){
				var chk = chkObj.options(chkObj.selectedIndex).value;
					
			    if (chk==1) {
					document.all.dt.style.display = "block"
					document.all.cn.style.display = "none"
				} else {
					document.all.dt.style.display = "none"
					document.all.cn.style.display = "block"
				}
			}
		}
	</script>
	<body onload="#onloadjs#">
	<table width="100%" border="1" cellspacing="0" cellpadding="0" class="header">
		<tr> 
			<td>□□□ CRMKitchen ユーティリティ □□□</td>
		</tr>
	</table>
	<div>
	<br>■CRMKitchenの標準メニュー以外の、各コンバート処理などの機能を提供しています。<br>
	</div>
<!--- 
	<form name="datas" method="post" action="ndex.cfm">
		<table width="100%" border="1" cellspacing="0" cellpadding="0" class="elm">
			<tr>
				<td width="91%">■インポート回答値矛盾データ修正</td>
				<td width="9%"><input type="submit" name="sub_button1" onClick="return sub_buttonCheck('データの回答矛盾を修正します。宜しいですか？');" value="修正開始"></td>
			</tr>
		</table>
		<table width="100%" border="1" cellspacing="0" cellpadding="0">
			<tr> 
				<td colspan="2">登録データの回答矛盾を修正します。（※利用施設サービス回答が未チェックで対象の設問に回答しているデータに対して、対象利用サービスのフラグをオンにしたデータを登録します。）</td>
			</tr>
		</table>
		<cfif IsDefined("retData")>
		<div>
		#retData.recordcount#件のデータ修正が完了しました。<br>
		</div>
		</cfif>
		<p>&nbsp;</p>
	</form>
 --->
	<form name="upLoad" method="post" enctype="multipart/form-data" action="ndex.cfm">
		<table width="100%" border="1" cellspacing="0" cellpadding="0" class="elm">
			<tr>
				<td width="91%">■ＣＳＶ回答データインポート</td>
				<td width="9%"><input name="file_name" type="file" id="file_name" size="52">&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="sub_button2" onClick="return fileup_msg()" value="登録開始"></td>
			</tr>
		</table>
		<table width="100%" border="1" cellspacing="0" cellpadding="0">
			<tr> 
				<td colspan="2">ファイル選択より対象ファイルを選択して「登録開始」ボタンをクリックして下さい。ＣＳＶデータより回答値をインポート登録します。</td>
			</tr>
		</table>
		<p>&nbsp;</p>
	</form>

	<form name="exportcsv" method="post" action="ndex.cfm">
		<table width="100%" border="1" cellspacing="0" cellpadding="0" class="elm">
			<tr>
				<td>■ＣＳＶ回答データエクスポート&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<select name="condition" OnChange="controlInput()">
						<option value=1>選択期間から抽出</option>
						<option value=2>照会用番号から抽出</option>
					</select>
				</td>
				<td>
					<div id=cn>
					照会用番号：<input type="text" name="referencenumber" size="20">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</div>
					<div id=dt>
						<cfset starty = 2004>
						<cfset nowy = DateFormat(Now(),'YYYY')>
						<cfset nowm = DateFormat(Now(),'MM')>
						<cfset nowd = DateFormat(Now(),'DD')>
						<cfset pasty = nowy-starty>
						<select name="b_year">
							<option value="#starty#">#starty#</option>
							<cfloop from="1" to="#pasty#" index="i">
								<cfif starty+i EQ nowy>
									<option value="#starty+i#" selected>#starty+i#</option>
								<cfelse>
									<option value="#starty+i#">#starty+i#</option>
								</cfif>
							</cfloop>
						</select>年
						<cfset mlist="01,02,03,04,05,06,07,08,09,10,11,12">
						<select name="b_month">
							<cfloop list="#mlist#" index="idx">
								<cfif idx EQ nowm>
									<option value="#idx#" selected>#idx#</option>
								<cfelse>
									<option value="#idx#">#idx#</option>
								</cfif>
							</cfloop>
						</select>月
						<cfset dlist="01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31">
						<select name="b_date">
							<cfloop list="#dlist#" index="idx">
								<cfif idx EQ nowd>
									<option value="#idx#" selected>#idx#</option>
								<cfelse>
									<option value="#idx#">#idx#</option>
								</cfif>
							</cfloop>
						</select>日
						〜
						<select name="e_year">
							<option value="#starty#">#starty#</option>
							<cfloop from="1" to="#pasty#" index="i">
								<cfif starty+i EQ nowy>
									<option value="#starty+i#" selected>#starty+i#</option>
								<cfelse>
									<option value="#starty+i#">#starty+i#</option>
								</cfif>
							</cfloop>
						</select>年
						<cfset mlist="01,02,03,04,05,06,07,08,09,10,11,12">
						<select name="e_month">
							<cfloop list="#mlist#" index="idx">
								<cfif idx EQ nowm>
									<option value="#idx#" selected>#idx#</option>
								<cfelse>
									<option value="#idx#">#idx#</option>
								</cfif>
							</cfloop>
						</select>月
						<cfset dlist="01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31">
						<select name="e_date">
							<cfloop list="#dlist#" index="idx">
								<cfif idx EQ nowd>
									<option value="#idx#" selected>#idx#</option>
								<cfelse>
									<option value="#idx#">#idx#</option>
								</cfif>
							</cfloop>
						</select>日
					</div>
				</td>
				<td align="right">
					<input type="submit" name="sub_button3" onClick="return diffcheck()" value="エクスポート開始">
				</td>
			</tr>
		</table>
		<table width="100%" border="1" cellspacing="0" cellpadding="0">
			<tr> 
				<td colspan="2">#sub_button3_message#</td>
			</tr>
		</table>
		<p>&nbsp;</p>
	</form>
	
	<form name="datas" method="post" action="ndex.cfm">
		<table width="100%" border="1" cellspacing="0" cellpadding="0" class="elm">
			<tr>
				<td width="91%">■インポート回答値矛盾データ修正</td>
				<td width="9%"><input type="submit" name="sub_button1" onClick="return sub_buttonCheck('データの回答矛盾を修正します。宜しいですか？');" value="修正開始"></td>
			</tr>
		</table>
		<table width="100%" border="1" cellspacing="0" cellpadding="0">
			<tr> 
				<td colspan="2">登録データの回答矛盾を修正します。（※利用施設サービス回答が未チェックで対象の設問に回答しているデータに対して、対象利用サービスのフラグをオンにしたデータを登録します。）</td>
			</tr>
		</table>
		<cfif IsDefined("retData")>
		<div>
		#retData.recordcount#件のデータ修正が完了しました。<br>
		</div>
		</cfif>
		<p>&nbsp;</p>
	</form>
	
	
	

	<p>&nbsp;</p>
	</body>
	</html>


	</cfoutput>
</cfif>
