<!--- password --->
<cfset password="qawsed">
<cfif (isDefined("session.login") eq false or session.login eq false) and right(cgi.SCRIPT_NAME,9) neq "index.cfm">
	<cflocation url="index.cfm" addtoken="no">
</cfif>
<cfif isdefined("form.pwd")>
	<cfif form.pwd eq password>
		<cfset session.login=true>
	</cfif>
<cfelseif isdefined("form.logout")>
	<cfset session.login=false>
</cfif>

<cfif isdefined("form.pwd")>
	<cfif form.pwd eq password>
		<cfset session.login=true>
	</cfif>
<cfelseif isdefined("form.logout")>
	<cfset session.login=false>
</cfif>

<!--- 初期値設定 --->
<cfset sub_button1_message = "">
<cfset sub_button2_message = "">
<cfset sub_button3_message = "データのエクスポートを行います。">
<cfset onloadjs = "controlInput();">

<cfif isDefined("session.login") eq false or session.login eq false>
	<cfoutput>
	<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
	<title>CRMKitchen ユーティリティ</title>
	<link rel="stylesheet" href="configcss.css">
	</head>
	<body>
	<form method="post" action="index.cfm">
	<table width="100%" border="1" cellspacing="0" cellpadding="0" class="header">
		<tr> 
			<td>CRMKitchen ユーティリティ ログイン</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
	</table>
	<p>&nbsp;</p>
	<table width="100%" border="1" cellspacing="0" cellpadding="0" class="elm">
		<tr>
			<td>Password <input type="password" name="pwd"><input type="submit" value="login"></td>
		</tr>
	</table>
	</form>
	</body>
	</html>
	</cfoutput>
<cfelse>

	<!--- データ修正処理 --->
	<cfif IsDefined("form.sub_button1")>
		<cfinvoke component = "utility"
			method			= "cleanseData"
			returnVariable	= "retData">
	</cfif>

	<!--- データコンバート処理 --->
	<cfif IsDefined("form.sub_button2")>
		<!--- エラークエリ --->
		<cfset ErrorQ = QueryNew("ErrNumber,message,detail")>

		<!--- ファイルのアップロード〜読込 --->
		<cfinvoke component="csv_entry" method="csvUpload" returnVariable="loadCheck">
		</cfinvoke>
		
		<!--- ファイルアップロード成功 --->
		<cfif loadCheck.compFlg EQ 1>
			<!--- ファイルの読込 --->
			<cfinvoke component="csv_entry" method="csvRead" returnVariable="readCheck">
				<cfinvokeargument name="fileName" value=#loadCheck.fileName#>
				<cfinvokeargument name="ErrorQ" value=#ErrorQ#>
			</cfinvoke>
			
			<!--- 読込成功 --->
			<cfif readCheck.compFlg EQ 1>
				<!--- データチェック 
				<cfinvoke component="csv_entry" method="csvCheck" returnVariable="readCheck">
					<cfinvokeargument name="fileName" value=#loadCheck.fileName#>
					<cfinvokeargument name="ErrorQ" value=#ErrorQ#>
					<cfinvokeargument name="csvData" value=#readCheck.csvData#>
				</cfinvoke>
				--->
			</cfif>
		<!--- ファイルアップロード失敗 --->
		<cfelse>
			<cfabort>
		</cfif>


		<!--- エラー存在ファイル削除処理 --->
		<cfif ErrorQ.recordcount GT 0>

			<cfif IsDefined("loadCheck.fileName")>
				<cfinvoke component="csv_entry" method="csvDelete" returnVariable="readCheck">
					<cfinvokeargument name="fileName" value=#loadCheck.fileName#>
				</cfinvoke>
			</cfif>

			<cfset onloadjs = "ErrorMsg();">
		
		<cfelse>
			<!---  
			<cfdump var=#readCheck.csvData# label="登録内容"><cfabort>
			--->
			<!--- 登録処理 --->
			<cfinvoke component="csv_entry" method="csvEntry" returnVariable="regCheck">
				<cfinvokeargument name="csvData" value=#readCheck.csvData#>
			</cfinvoke>

			<!--- ファイル削除処理 --->
			<cfinvoke component="csv_entry" method="csvDelete" returnVariable="readCheck">
				<cfinvokeargument name="fileName" value=#loadCheck.fileName#>
			</cfinvoke>
			
			<cfif regCheck.compFlg EQ 1>
				<cfset onloadjs = "alert(MSG_ENTRY_END);window.parent.close();">
			</cfif>
		</cfif>
	</cfif>

	<!--- ＣＳＶエクスポート --->
	<cfif IsDefined("form.sub_button3")>
		<cfset sub_button3_message = "データのエクスポートを行います。">
		<cfset tmpbdate = DateFormat(CreateDate(#form.b_year#,#form.b_month#,#form.b_date#),'YYYY/MM/DD')>
		<cfset tmpedate = DateFormat(CreateDate(#form.e_year#,#form.e_month#,#form.e_date#),'YYYY/MM/DD')>

		<!--- 出力対象基幹データリスト生成 --->
		<cfinvoke component = "utility"
			method			= "setbaselist"
			data = #form#
			returnVariable	= "basedata">
	
		<cfinvoke component = "utility"
			method			= "expData"
			condition = #form.condition#
			bdate = "#tmpbdate#"
			edate = "#tmpedate#"
			referencenumber = "#form.referencenumber#"
			basedata = #basedata#
			returnVariable	= "retData">
		<cfif retData.recordcount EQ 0>
			<cfset onloadjs = "controlInput();alert('該当データはありませんでした');">
		</cfif>
	</cfif>

	<!--- データコンバート処理 --->
	<cfif IsDefined("form.sub_button4")>
		<!--- エラークエリ --->
		<cfset ErrorQ = QueryNew("ErrNumber,message,detail")>

		<!--- ファイルのアップロード〜読込 --->
		<cfinvoke component="base_entry" method="csvUpload" returnVariable="loadCheck">
		</cfinvoke>
		
		<!--- ファイルアップロード成功 --->
		<cfif loadCheck.compFlg EQ 1>
			<!--- ファイルの読込 --->
			<cfinvoke component="base_entry" method="csvRead" returnVariable="readCheck">
				<cfinvokeargument name="fileName" value=#loadCheck.fileName#>
				<cfinvokeargument name="ErrorQ" value=#ErrorQ#>
			</cfinvoke>
			
			<!--- 読込成功 --->
			<cfif readCheck.compFlg EQ 1>
			</cfif>
		<!--- ファイルアップロード失敗 --->
		<cfelse>
			<cfabort>
		</cfif>

		<!--- エラー存在ファイル削除処理 --->
		<cfif ErrorQ.recordcount GT 0>

			<cfif IsDefined("loadCheck.fileName")>
				<cfinvoke component="base_entry" method="csvDelete" returnVariable="readCheck">
					<cfinvokeargument name="fileName" value=#loadCheck.fileName#>
				</cfinvoke>
			</cfif>

			<cfset onloadjs = "ErrorMsg();">
		
		<cfelse>
			<!---  --->
			<cfdump var=#readCheck.csvData# label="登録内容"><cfabort>
			
			<!--- 登録処理 --->
			<cfinvoke component="base_entry" method="csvEntry" returnVariable="regCheck">
				<cfinvokeargument name="csvData" value=#readCheck.csvData#>
			</cfinvoke>

			<!--- ファイル削除処理 --->
			<cfinvoke component="base_entry" method="csvDelete" returnVariable="readCheck">
				<cfinvokeargument name="fileName" value=#loadCheck.fileName#>
			</cfinvoke>
			
			<cfif regCheck.compFlg EQ 1>
				<cfset onloadjs = "alert(MSG_ENTRY_END);window.parent.close();">
			</cfif>
		</cfif>
	</cfif>
	
	<cfoutput>
	<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
	<title>CRMKitchen ユーティリティ</title>
	<link rel="stylesheet" href="configcss.css">
	</head>
	<script language="JavaScript" type="text/JavaScript">
		function sub_buttonCheck(msg) {
			return confirm(msg);
		}

		function fileup_msg(){
			tmp = document.forms[0].file_name.value;
			if(tmp.length == 0){
				alert("ファイルを指定して下さい。");
				return false;
			}else{
				flag = confirm(tmp+"を登録します。\nよろしいですか？");
				if(flag == true){
					return true;
					//alert("登録しました。");
					//window.close();
				}else{
					alert("登録をキャンセルしました。");
					return false;
				}
			}
		
		}

		function diffcheck() {
			var s1 = document.exportcsv.b_year;
			var v1 = s1.options(s1.selectedIndex).value;
			var s2 = document.exportcsv.b_month;
			var v2 = s2.options(s2.selectedIndex).value;
			var s3 = document.exportcsv.b_date;
			var v3 = s3.options(s3.selectedIndex).value;
			var s4 = document.exportcsv.e_year;
			var v4 = s4.options(s4.selectedIndex).value;
			var s5 = document.exportcsv.e_month;
			var v5 = s5.options(s5.selectedIndex).value;
			var s6 = document.exportcsv.e_date;
			var v6 = s6.options(s6.selectedIndex).value;

			var tempSStr = v1.toString()+(v2.toString())+(v3.toString())
			var tempEStr = v4.toString()+(v5.toString())+(v6.toString())
			var start = Number(tempEStr)-Number(tempSStr)

			if (start<0) {
				alert("期間指定が矛盾しています。正しく設定して下さい。");
				return false;
			}
		}		
		function controlInput() {
			var chkObj = window.exportcsv("condition");
			if(chkObj != null){
				var chk = chkObj.options(chkObj.selectedIndex).value;
					
			    if (chk==1) {
					document.all.dt.style.display = "block"
					document.all.cn.style.display = "none"
				} else {
					document.all.dt.style.display = "none"
					document.all.cn.style.display = "block"
				}
			}
		}
	</script>
	<body onload="#onloadjs#">
	<table width="100%" border="1" cellspacing="0" cellpadding="0" class="header">
		<tr> 
			<td>□□□ CRMKitchen ユーティリティ □□□</td>
		</tr>
	</table>
	<div>
	<br>■CRMKitchenの標準メニュー以外の、各コンバート処理などの機能を提供しています。<br>
	</div>
<!--- 
	<form name="datas" method="post" action="index.cfm">
		<table width="100%" border="1" cellspacing="0" cellpadding="0" class="elm">
			<tr>
				<td width="91%">■インポート回答値矛盾データ修正</td>
				<td width="9%"><input type="submit" name="sub_button1" onClick="return sub_buttonCheck('データの回答矛盾を修正します。宜しいですか？');" value="修正開始"></td>
			</tr>
		</table>
		<table width="100%" border="1" cellspacing="0" cellpadding="0">
			<tr> 
				<td colspan="2">登録データの回答矛盾を修正します。（※利用施設サービス回答が未チェックで対象の設問に回答しているデータに対して、対象利用サービスのフラグをオンにしたデータを登録します。）</td>
			</tr>
		</table>
		<cfif IsDefined("retData")>
		<div>
		#retData.recordcount#件のデータ修正が完了しました。<br>
		</div>
		</cfif>
		<p>&nbsp;</p>
	</form>

	<form name="upLoad" method="post" enctype="multipart/form-data" action="index.cfm">
		<table width="100%" border="1" cellspacing="0" cellpadding="0" class="elm">
			<tr>
				<td width="91%">■ＣＳＶ回答データインポート</td>
				<td width="9%"><input name="file_name" type="file" id="file_name" size="52">&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="sub_button2" onClick="return fileup_msg()" value="登録開始"></td>
			</tr>
		</table>
		<table width="100%" border="1" cellspacing="0" cellpadding="0">
			<tr> 
				<td colspan="2">ファイル選択より対象ファイルを選択して「登録開始」ボタンをクリックして下さい。ＣＳＶデータより回答値をインポート登録します。</td>
			</tr>
		</table>
		<p>&nbsp;</p>
	</form>
 --->
	<form name="exportcsv" method="post" action="index.cfm">
		<table width="100%" border="1" cellspacing="0" cellpadding="0" class="elm">
			<tr>
				<td>■ＣＳＶ回答データエクスポート&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<select name="condition" OnChange="controlInput()">
						<option value=1>選択期間から抽出</option>
						<option value=2>照会用番号から抽出</option>
					</select>
				</td>
				<td>
					<div id=cn>
					照会用番号：<input type="text" name="referencenumber" size="20">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</div>
					<div id=dt>
						<cfset starty = 2004>
						<cfset nowy = DateFormat(Now(),'YYYY')>
						<cfset nowm = DateFormat(Now(),'MM')>
						<cfset nowd = DateFormat(Now(),'DD')>
						<cfset pasty = nowy-starty>
						<select name="b_year">
							<option value="#starty#">#starty#</option>
							<cfloop from="1" to="#pasty#" index="i">
								<cfif starty+i EQ nowy>
									<option value="#starty+i#" selected>#starty+i#</option>
								<cfelse>
									<option value="#starty+i#">#starty+i#</option>
								</cfif>
							</cfloop>
						</select>年
						<cfset mlist="01,02,03,04,05,06,07,08,09,10,11,12">
						<select name="b_month">
							<cfloop list="#mlist#" index="idx">
								<cfif idx EQ nowm>
									<option value="#idx#" selected>#idx#</option>
								<cfelse>
									<option value="#idx#">#idx#</option>
								</cfif>
							</cfloop>
						</select>月
						<cfset dlist="01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31">
						<select name="b_date">
							<cfloop list="#dlist#" index="idx">
								<cfif idx EQ nowd>
									<option value="#idx#" selected>#idx#</option>
								<cfelse>
									<option value="#idx#">#idx#</option>
								</cfif>
							</cfloop>
						</select>日
						〜
						<select name="e_year">
							<option value="#starty#">#starty#</option>
							<cfloop from="1" to="#pasty#" index="i">
								<cfif starty+i EQ nowy>
									<option value="#starty+i#" selected>#starty+i#</option>
								<cfelse>
									<option value="#starty+i#">#starty+i#</option>
								</cfif>
							</cfloop>
						</select>年
						<cfset mlist="01,02,03,04,05,06,07,08,09,10,11,12">
						<select name="e_month">
							<cfloop list="#mlist#" index="idx">
								<cfif idx EQ nowm>
									<option value="#idx#" selected>#idx#</option>
								<cfelse>
									<option value="#idx#">#idx#</option>
								</cfif>
							</cfloop>
						</select>月
						<cfset dlist="01,02,03,04,05,06,07,08,09,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31">
						<select name="e_date">
							<cfloop list="#dlist#" index="idx">
								<cfif idx EQ nowd>
									<option value="#idx#" selected>#idx#</option>
								<cfelse>
									<option value="#idx#">#idx#</option>
								</cfif>
							</cfloop>
						</select>日
					</div>
				</td>
				<td align="right">
					<input type="submit" name="sub_button3" onClick="return diffcheck()" value="エクスポート開始">
				</td>
			</tr>
		</table>
		<table width="100%" border="1" cellspacing="0" cellpadding="0">
			<tr> 
				<td colspan="12" nowrap bgcolor="##F6F6F6">□基幹データ出力項目&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;出力させたい項目にチェックを入れてください。</td>
			</tr>
			<tr> 
				<td nowrap> <input type="checkbox" name="bd_reserve_code" value="reserve_code\予約番号\rtt_reserve"></td>
				<td nowrap>予約番号</td>
				<td nowrap> <input type="checkbox" name="bd_room_code" value="room_code\部屋番号\rtt_reserve"></td>
				<td nowrap>部屋番号</td>
				<td nowrap> <input type="checkbox" name="bd_arrive_date" value="arrive_date\到着日\rtt_reserve"></td>
				<td nowrap>到着日</td>
				<td nowrap><input type="checkbox" name="bd_nights" value="nights\泊数\rtt_reserve"></td>
				<td nowrap>泊数</td>
				<td nowrap><input type="checkbox" name="bd_depart_date" value="depart_date\出発日\rtt_reserve"></td>
				<td nowrap>出発日</td>
				<td nowrap><input type="checkbox" name="bd_lateout_flg" value="lateout_flg\レイトアウト\rtt_reserve"></td>
				<td nowrap>レイトアウト</td>
			</tr>
			<tr> 
				<td nowrap><input type="checkbox" name="bd_manpersoncount" value="manpersoncount\男\rtt_reserve"></td>
				<td nowrap>男</td>
				<td nowrap><input type="checkbox" name="bd_womenpersoncount" value="womenpersoncount\女\rtt_reserve"></td>
				<td nowrap>女</td>
				<td nowrap><input type="checkbox" name="bd_childpersoncount_a" value="childpersoncount_a\子Ａ\rtt_reserve"></td>
				<td nowrap>子Ａ</td>
				<td nowrap><input type="checkbox" name="bd_childpersoncount_b" value="childpersoncount_b\子Ｂ\rtt_reserve"></td>
				<td nowrap>子Ｂ</td>
				<td nowrap><input type="checkbox" name="bd_childpersoncount_c" value="childpersoncount_c\子Ｃ\rtt_reserve"></td>
				<td nowrap>子Ｃ</td>
				<td nowrap><input type="checkbox" name="bd_childpersoncount_d" value="childpersoncount_d\子Ｄ\rtt_reserve"></td>
				<td nowrap>子Ｄ</td>
			</tr>
			<tr> 
				<td nowrap><input type="checkbox" name="bd_roomtype" value="roomtype\ルームタイプ\rtt_reserve"></td>
				<td nowrap>ルームタイプ</td>
				<td nowrap><input type="checkbox" name="bd_roomcontrol" value="roomcontrol\ルームコントロール\rtt_reserve"></td>
				<td nowrap>ルームコントロール</td>
				<td nowrap><input type="checkbox" name="bd_reservationroute" value="reservationroute\予約経路\rtt_reserve"></td>
				<td nowrap>予約経路</td>
				<td nowrap><input type="checkbox" name="bd_information" value="information\備考\rtt_reserve"></td>
				<td nowrap>備考</td>
				<td nowrap>&nbsp;</td>
				<td nowrap>&nbsp;</td>
				<td nowrap>&nbsp;</td>
				<td nowrap>&nbsp;</td>
			</tr>
			<tr> 
				<td nowrap><input type="checkbox" name="bd_totalsalesamount" value="totalsalesamount\客室総売上\rtt_reserve"></td>
				<td nowrap>客室総売上</td>
				<td nowrap><input type="checkbox" name="bd_totalsalesamountdividedbynights" value="totalsalesamountdividedbynights\客室単価\rtt_reserve"></td>
				<td nowrap>客室単価</td>
				<td nowrap><input type="checkbox" name="bd_personunitprice" value="personunitprice\客単価\rtt_reserve"></td>
				<td nowrap>客単価（一泊）&nbsp;&nbsp;&nbsp;</td>
				<td nowrap><input type="checkbox" name="bd_roomcharge" value="roomcharge\ルームチャージ\rtt_reserve"></td>
				<td nowrap>ルームチャージ（一泊）</td>
				<td nowrap><input type="checkbox" name="bd_optionamount" value="optionamount\オプション料金\rtt_reserve"></td>
				<td nowrap>オプション料金（一泊）</td>
				<td nowrap><input type="checkbox" name="bd_restaurantamount" value="restaurantamount\レストラン料金\rtt_reserve"></td>
				<td nowrap>レストラン料金（一泊）&nbsp;</td>
			</tr>
			<tr> 
				<td colspan="12" nowrap height="1"><hr></td>
			</tr>
			<tr> 
				<td nowrap><input type="checkbox" name="bd_surveymethod" value="surveymethod\調査手段\rtt_reserve"></td>
				<td nowrap>調査手段</td>
				<td nowrap><input type="checkbox" name="bd_marketing_1" value="marketing_1\認知経路\rtt_marketing"></td>
				<td nowrap>認知経路</td>
				<td nowrap><input type="checkbox" name="bd_marketing_2" value="marketing_2\旅行目的１\rtt_marketing"></td>
				<td nowrap>旅行目的１</td>
				<td nowrap><input type="checkbox" name="bd_marketing_3" value="marketing_3\客層区分\rtt_marketing"></td>
				<td nowrap>客層区分</td>
				<td nowrap>&nbsp;</td>
				<td nowrap>&nbsp;</td>
				<td nowrap>&nbsp;</td>
				<td nowrap>&nbsp;</td>
			</tr>
			<tr> 
				<td colspan="12" nowrap height="1"><hr></td>
			</tr>
			<tr> 
				<td nowrap><input type="checkbox" name="bd_customernumber" value="customernumber\顧客番号\rtt_reserve"></td>
				<td nowrap>顧客番号</td>
				<td nowrap><input type="checkbox" name="bd_customercategory" value="customercategory\顧客区分\rtm_customer"></td>
				<td nowrap>顧客区分</td>
				<td nowrap><input type="checkbox" name="bd_customerrank" value="customerrank\客種\rtm_customer"></td>
				<td nowrap>客種</td>
				<td nowrap><input type="checkbox" name="bd_name_kana" value="name_kana\カナ名\rtm_customer"></td>
				<td nowrap>カナ名</td>
				<td nowrap><input type="checkbox" name="bd_name" value="name\漢字名\rtm_customer"></td>
				<td nowrap>漢字名</td>
				<td nowrap><input type="checkbox" name="bd_birth_date" value="birth_date\生年月日\rtm_customer"></td>
				<td nowrap>生年月日</td>
			</tr>
			<tr> 
				<td nowrap><input type="checkbox" name="bd_address_zipcode" value="address_zipcode\郵便番号\rtm_customer"></td>
				<td nowrap>郵便番号</td>
				<td nowrap><input type="checkbox" name="bd_address_segment_1" value="address_segment_1\住所１\rtm_customer"></td>
				<td nowrap>住所１</td>
				<td nowrap><input type="checkbox" name="bd_address_segment_2" value="address_segment_2\住所２\rtm_customer"></td>
				<td nowrap>住所２</td>
				<td nowrap><input type="checkbox" name="bd_address_segment_3" value="address_segment_3\住所３\rtm_customer"></td>
				<td nowrap>住所３</td>
				<td nowrap><input type="checkbox" name="bd_phone_number_1" value="phone_number_1\自宅電話番号\rtm_customer"></td>
				<td nowrap>自宅電話番号</td>
				<td nowrap><input type="checkbox" name="bd_phone_number_2" value="phone_number_2\携帯電話番号\rtm_customer"></td>
				<td nowrap>携帯電話番号</td>
			</tr>
			<tr> 
				<td nowrap><input type="checkbox" name="bd_mail_address_1" value="mail_address_1\Ｅメール\rtm_customer"></td>
				<td nowrap>Ｅメール</td>
				<td nowrap><input type="checkbox" name="bd_married_flg" value="married_flg\結婚\rtm_customer"></td>
				<td nowrap>結婚</td>
				<td nowrap><input type="checkbox" name="bd_gender_code" value="gender_code\性別\rtm_customer"></td>
				<td nowrap>性別</td>
				<td nowrap><input type="checkbox" name="bd_repeater" value="repeater\リピーター\rtm_customer"></td>
				<td nowrap>リピーター</td>
				<td nowrap><input type="checkbox" name="bd_marketingarea" value="marketingarea\マーケティングエリア\rtm_customer"></td>
				<td nowrap>マーケティングエリア</td>
				<td nowrap>&nbsp;</td>
				<td nowrap>&nbsp;</td>
			</tr>
			<tr> 
				<td colspan="12" nowrap height="1"><hr></td>
			</tr>
			<tr> 
				<td nowrap><input type="checkbox" name="bd_shopresult_01" value="shopresult_01\店舗１\rtt_shopresult"></td>
				<td nowrap>店舗１</td>
				<td nowrap><input type="checkbox" name="bd_shopresult_02" value="shopresult_02\店舗２\rtt_shopresult"></td>
				<td nowrap>店舗２</td>
				<td nowrap><input type="checkbox" name="bd_shopresult_03" value="shopresult_03\店舗３\rtt_shopresult"></td>
				<td nowrap>店舗３</td>
				<td nowrap><input type="checkbox" name="bd_shopresult_04" value="shopresult_04\店舗４\rtt_shopresult"></td>
				<td nowrap>店舗４&nbsp;</td>
				<td nowrap><input type="checkbox" name="bd_shopresult_05" value="shopresult_05\店舗５\rtt_shopresult"></td>
				<td nowrap>店舗５&nbsp;</td>
				<td nowrap>&nbsp;</td>
				<td nowrap>&nbsp;</td>
			</tr>
			<tr> 
				<td nowrap><input type="checkbox" name="bd_shopresult_06" value="shopresult_06\店舗６\rtt_shopresult"></td>
				<td nowrap>店舗６&nbsp;</td>
				<td nowrap><input type="checkbox" name="bd_shopresult_07" value="shopresult_07\店舗７\rtt_shopresult"></td>
				<td nowrap>店舗７</td>
				<td nowrap><input type="checkbox" name="bd_shopresult_08" value="shopresult_08\店舗８\rtt_shopresult"></td>
				<td nowrap>店舗８</td>
				<td nowrap><input type="checkbox" name="bd_shopresult_09" value="shopresult_09\店舗９\rtt_shopresult"></td>
				<td nowrap>店舗９</td>
				<td nowrap><input type="checkbox" name="bd_shopresult_10" value="shopresult_10\店舗１０\rtt_shopresult"></td>
				<td nowrap>店舗１０&nbsp;</td>
				<td nowrap>&nbsp;</td>
				<td nowrap>&nbsp;</td>
			</tr>
			<tr> 
				<td nowrap><input type="checkbox" name="bd_shopresult_11" value="shopresult_11\店舗１１\rtt_shopresult"></td>
				<td nowrap>店舗１１</td>
				<td nowrap><input type="checkbox" name="bd_shopresult_12" value="shopresult_12\店舗１２\rtt_shopresult"></td>
				<td nowrap>店舗１２</td>
				<td nowrap><input type="checkbox" name="bd_shopresult_13" value="shopresult_13\店舗１３\rtt_shopresult"></td>
				<td nowrap>店舗１３</td>
				<td nowrap><input type="checkbox" name="bd_shopresult_14" value="shopresult_14\店舗１４\rtt_shopresult"></td>
				<td nowrap>店舗１４</td>
				<td nowrap><input type="checkbox" name="bd_shopresult_15" value="shopresult_15\店舗１５\rtt_shopresult"></td>
				<td nowrap>店舗１５</td>
				<td nowrap>&nbsp;</td>
				<td nowrap>&nbsp;</td>
			</tr>
			<tr> 
				<td nowrap><input type="checkbox" name="bd_shopresult_16" value="shopresult_16\店舗１６\rtt_shopresult"></td>
				<td nowrap>店舗１６</td>
				<td nowrap><input type="checkbox" name="bd_shopresult_17" value="shopresult_17\店舗１７\rtt_shopresult"></td>
				<td nowrap>店舗１７</td>
				<td nowrap><input type="checkbox" name="bd_shopresult_18" value="shopresult_18\店舗１８\rtt_shopresult"></td>
				<td nowrap>店舗１８</td>
				<td nowrap><input type="checkbox" name="bd_shopresult_19" value="shopresult_19\店舗１９\rtt_shopresult"></td>
				<td nowrap>店舗１９</td>
				<td nowrap><input type="checkbox" name="bd_shopresult_20" value="shopresult_20\店舗２０\rtt_shopresult"></td>
				<td nowrap>店舗２０&nbsp;</td>
				<td nowrap>&nbsp;</td>
				<td nowrap>&nbsp;</td>
			</tr>
		</table>
		
<!--- 
		<table width="100%" border="1" cellspacing="0" cellpadding="0">
			<tr> 
				<td>
					<input type="checkbox" name="bd_reserve_code" value="reserve_code\予約番号\rtt_reserve">予約番号&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_room_code" value="room_code\部屋番号\rtt_reserve">部屋番号&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_customernumber" value="customernumber\顧客番号\rtt_reserve">顧客番号&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_arrive_date" value="arrive_date\到着日\rtt_reserve">到着日&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_nights" value="nights\泊数\rtt_reserve">泊数&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_depart_date" value="depart_date\出発日\rtt_reserve">出発日&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_information" value="information\備考\rtt_reserve">備考&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_lateout_flg" value="lateout_flg\レイトアウト\rtt_reserve">レイトアウト&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
					<input type="checkbox" name="bd_manpersoncount" value="manpersoncount\男\rtt_reserve">男&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_womenpersoncount" value="womenpersoncount\女\rtt_reserve">女&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_childpersoncount_a" value="childpersoncount_a\子Ａ\rtt_reserve">子Ａ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_childpersoncount_b" value="childpersoncount_b\子Ｂ\rtt_reserve">子Ｂ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_childpersoncount_c" value="childpersoncount_c\子Ｃ\rtt_reserve">子Ｃ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_childpersoncount_d" value="childpersoncount_d\子Ｄ\rtt_reserve">子Ｄ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_roomtype" value="roomtype\ルームタイプ\rtt_reserve">ルームタイプ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_roomcontrol" value="roomcontrol\ルームコントロール\rtt_reserve">ルームコントロール&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_reservationroute" value="reservationroute\予約経路\rtt_reserve">予約経路&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
					<input type="checkbox" name="bd_totalsalesamount" value="totalsalesamount\客室総売上\rtt_reserve">客室総売上&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_totalsalesamountdividedbynights" value="totalsalesamountdividedbynights\客室単価\rtt_reserve">客室単価&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_personunitprice" value="personunitprice\客単価\rtt_reserve">客単価（一泊）&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_roomcharge" value="roomcharge\ルームチャージ\rtt_reserve">ルームチャージ（一泊）&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_optionamount" value="optionamount\オプション料金\rtt_reserve">オプション料金（一泊）&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_restaurantamount" value="restaurantamount\レストラン料金\rtt_reserve">レストラン料金（一泊）&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
					<input type="checkbox" name="bd_surveymethod" value="surveymethod\調査手段\rtt_reserve">調査手段&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_marketing_1" value="marketing_1\認知経路\rtt_marketing">認知経路&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_marketing_2" value="marketing_2\旅行目的１\rtt_marketing">旅行目的１&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_marketing_3" value="marketing_3\客層区分\rtt_marketing">客層区分&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
					<!--- 
					<input type="checkbox" name="bd_marketing_4" value="marketing_4">顧客番号&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_marketing_5" value="marketing_5">顧客番号&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_marketing_6" value="marketing_6">顧客番号&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_marketing_7" value="marketing_7">顧客番号&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_marketing_8" value="marketing_8">顧客番号&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_marketing_9" value="marketing_9">顧客番号&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_marketing_10" value="marketing_10">顧客番号&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					 --->
					<input type="checkbox" name="bd_customercategory" value="customercategory\顧客区分\rtm_customer">顧客区分&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_customerrank" value="customerrank\客種\rtm_customer">客種&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_name_kana" value="name_kana\カナ名\rtm_customer">カナ名&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_name" value="name\漢字名\rtm_customer">漢字名&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_birth_date" value="birth_date\生年月日\rtm_customer">生年月日&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_address_zipcode" value="address_zipcode\郵便番号\rtm_customer">郵便番号&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_address_segment_1" value="address_segment_1\住所１\rtm_customer">住所１&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_address_segment_2" value="address_segment_2\住所２\rtm_customer">住所２&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_address_segment_3" value="address_segment_3\住所３\rtm_customer">住所３&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
					<input type="checkbox" name="bd_phone_number_1" value="phone_number_1\自宅電話番号\rtm_customer">自宅電話番号&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_phone_number_2" value="phone_number_2\携帯電話番号\rtm_customer">携帯電話番号&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_mail_address_1" value="mail_address_1\Ｅメール\rtm_customer">Ｅメール&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_married_flg" value="married_flg\結婚\rtm_customer">結婚&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_gender_code" value="gender_code\性別\rtm_customer">性別&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
					<input type="checkbox" name="bd_shopresult_01" value="shopresult_01\店舗１\rtt_shopresult">店舗１&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_shopresult_02" value="shopresult_02\店舗２\rtt_shopresult">店舗２&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_shopresult_03" value="shopresult_03\店舗３\rtt_shopresult">店舗３&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_shopresult_04" value="shopresult_04\店舗４\rtt_shopresult">店舗４&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_shopresult_05" value="shopresult_05\店舗５\rtt_shopresult">店舗５&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_shopresult_06" value="shopresult_06\店舗６\rtt_shopresult">店舗６&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_shopresult_07" value="shopresult_07\店舗７\rtt_shopresult">店舗７&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_shopresult_08" value="shopresult_08\店舗８\rtt_shopresult">店舗８&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_shopresult_09" value="shopresult_09\店舗９\rtt_shopresult">店舗９&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_shopresult_10" value="shopresult_10\店舗１０\rtt_shopresult">店舗１０&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
					<input type="checkbox" name="bd_shopresult_11" value="shopresult_11\店舗１１\rtt_shopresult">店舗１１&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_shopresult_12" value="shopresult_12\店舗１２\rtt_shopresult">店舗１２&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_shopresult_13" value="shopresult_13\店舗１３\rtt_shopresult">店舗１３&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_shopresult_14" value="shopresult_14\店舗１４\rtt_shopresult">店舗１４&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_shopresult_15" value="shopresult_15\店舗１５\rtt_shopresult">店舗１５&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_shopresult_16" value="shopresult_16\店舗１６\rtt_shopresult">店舗１６&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_shopresult_17" value="shopresult_17\店舗１７\rtt_shopresult">店舗１７&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_shopresult_18" value="shopresult_18\店舗１８\rtt_shopresult">店舗１８&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_shopresult_19" value="shopresult_19\店舗１９\rtt_shopresult">店舗１９&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_shopresult_20" value="shopresult_20\店舗２０\rtt_shopresult">店舗２０&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br>
					<input type="checkbox" name="bd_repeater" value="repeater\リピーター\rtm_customer">リピーター&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<input type="checkbox" name="bd_marketingarea" value="marketingarea\マーケティングエリア\rtm_customer">マーケティングエリア&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				</td>
			</tr>
		</table>
 --->		
		<table width="100%" border="1" cellspacing="0" cellpadding="0">
			<tr> 
				<td colspan="2">#sub_button3_message#</td>
			</tr>
		</table>
		<p>&nbsp;</p>
	</form>		

	<p>&nbsp;</p>
	</body>
	</html>


	</cfoutput>
</cfif>
