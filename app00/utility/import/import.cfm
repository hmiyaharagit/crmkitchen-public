<!---
*************************************************************************************************
* Description		:基幹データ取込プログラム（初版：星のや）
* History			:1) Coded by	2005/12/01 h.miyahara(NetFusion)
*					:2) Updated by	
*************************************************************************************************
--->
<!--- ログイン処理用ファイル --->
<cfinclude template="login.cfm">

<!--- 初期設定 --->
<cfinclude template="defaultvars.cfm">


<cfif isDefined("session.login") eq false or session.login eq false>
	<cfoutput>
	<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
	<title>CRMKitchen ユーティリティ</title>
	<link rel="stylesheet" href="../configcss.css">
	</head>
	<body>
	<form method="post" action="import.cfm">
	<table width="100%" border="1" cellspacing="0" cellpadding="0" class="header">
		<tr> 
			<td>CRMKitchen ユーティリティ ログイン</td>
		</tr>
		<tr>
			<td>&nbsp;</td>
		</tr>
	</table>
	<p>&nbsp;</p>
	<table width="100%" border="1" cellspacing="0" cellpadding="0" class="elm">
		<tr>
			<td>Password <input type="password" name="pwd"><input type="submit" value="login"></td>
		</tr>
	</table>
	</form>
	</body>
	</html>
	</cfoutput>
<cfelse>

	<!--- データ修正処理 --->
	<cfif IsDefined("form.sub_button1")>
		<cfinvoke component="base_entry" method="csvRead" returnVariable="readCheck">
			<cfinvokeargument name="filePath" value="#form.file_name#">
			<cfinvokeargument name="ErrorQ" value=#ErrorQ#>
		</cfinvoke>

		<!--- ＤＢ登録処理 --->
		<cfif ErrorQ.recordcount EQ 0>
			<cfinvoke component="base_entry" method="import" returnVariable="impCheck">
				<cfinvokeargument name="csvData" value="#readCheck.csvData#">
				<cfinvokeargument name="ErrorQ" value=#ErrorQ#>
			</cfinvoke>
		</cfif>
	</cfif>
	
	<cfoutput>
	<html>
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
	<title>CRMKitchen ユーティリティ</title>
	<link rel="stylesheet" href="../configcss.css">
	</head>
	<script language="JavaScript" type="text/JavaScript">
		function sub_buttonCheck(msg) {
			return confirm(msg);
		}

		function fileup_msg(){
			tmp = document.forms[0].file_name.value;
			if(tmp.length == 0){
				alert("ファイルを指定して下さい。");
				return false;
			}else{
				flag = confirm(tmp+"を登録します。\nよろしいですか？");
				if(flag == true){
					return true;
					//alert("登録しました。");
					//window.close();
				}else{
					alert("登録をキャンセルしました。");
					return false;
				}
			}
		}
	</script>
	<body>
	<table width="100%" border="1" cellspacing="0" cellpadding="0" class="header">
		<tr> 
			<td>□□□ CRMKitchen ユーティリティ □□□</td>
		</tr>
	</table>
	<div>
	<br>■CRMKitchenの標準メニュー以外の、各コンバート処理などの機能を提供しています。<br>
	</div>

	<form name="import" method="post" enctype="multipart/form-data" action="import.cfm">
		<table width="100%" border="1" cellspacing="0" cellpadding="0" class="elm">
			<tr>
				<td width="91%">■基幹データインポート</td>
				<td width="9%"><input name="file_name" type="file" id="file_name" size="52">&nbsp;&nbsp;&nbsp;&nbsp;<input type="submit" name="sub_button1" onClick="return fileup_msg()" value="登録開始"></td>
			</tr>
		</table>
		<table width="100%" border="1" cellspacing="0" cellpadding="0">
			<tr> 
				<td colspan="2">ファイル選択より対象ファイルを選択して「登録開始」ボタンをクリックして下さい。</td>
			</tr>
		</table>
		<p>&nbsp;</p>
	</form>
	<cfif ErrorQ.recordcount EQ 0>
		<cfif isDefined("form.sub_button1")>
		インポート処理が完了しました。
		</cfif>
	<cfelse>
		インポート処理中にエラーが発生しました。<br>
		[#ErrorQ.message#]<br><br>
		--CFErrorMessage<br>
		#ErrorQ.detail#<br>
		#request.Appvars.newline##request.Appvars.newline#<br>
	</cfif>
	<p>&nbsp;</p>
	</body>
	</html>
	</cfoutput>
</cfif>

