<cfcomponent output="false">

	<!---
	*************************************************************************************************
	* Class				:csv_entry
	* Method			:csvUpload
	* Description		:活動量データCSV登録
	* Custom Attributes	:none
	* Return Paramters	:none
	* History			:1) Coded by	2005/08/12 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="csvUpload" output="false">
		<cftry>
		
			<cfset dirpath = ExpandPath(#Application.outputDirectory#)>
			<cffile
				action = "upload"
				fileField = "file_name"
				destination = "#dirpath#"
				accept = "*/*"
				nameConflict = "MakeUnique">


		<!---accept = "application/csv,application/excel,application/octet-stream"--->

			<!--- 戻値設定 --->
			<cfscript>
				executeinformation = StructNew();
				StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
				StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
				StructInsert(executeinformation,	"compFlg",		1								);
				StructInsert(executeinformation,	"fileName",		#cffile.serverFile#				);
			</cfscript>
			<cfreturn executeinformation>
			
			<cfcatch type="any">
			
				<!--- 戻値設定 --->
				<cfscript>
					executeinformation = StructNew();
					StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
					StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
					StructInsert(executeinformation,	"compFlg",		0								);
				</cfscript>
				<cfreturn executeinformation>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<!---
	*************************************************************************************************
	* Class				:csv_entry
	* Method			:csvRead
	* Description		:
	* Custom Attributes	:fileName
	* Return Paramters	:none
	* History			:1) Coded by	2005/08/12 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="csvRead" output="false">
		<cfargument name="fileName"   type="string" required="true">
		<cfargument name="ErrorQ"   type="Query" required="true">

		<cfset ErrorQ = arguments.ErrorQ>
		<cftry>
			<cfset filenm = "http://#CGI.SERVER_NAME##Application.outputDirectory##URLEncodedFormat(arguments.fileName)#">


			<cfhttp method = "Get" url = "#filenm#" name = "csvData" textqualifier = "" charset = "shift_jis" firstrowasheaders="no">

			<!--- 
			<cfhttp method = "Get" url = "#filenm#" name = "csvData" textqualifier = "" charset = "shift_jis"
				columns = "q_1,q_2"
				firstrowasheaders="yes">
			 --->
			<!--- 
			 columns = "reserve_id,time_sart,time_end,key_cord1,key_cord3,qn_1,qn_2,qn_3,qn_4,qn_5,qn_6,qn_7,qn_8,qn_9,qe_9,nn_10,qn_11,nn_12,qn_13,qn_14,qe_14,qn_15,qn_16,qn_17,qn_18,qn_19,qn_20,qn_21,qn_22,qn_23,qn_24,qn_25,qn_26,qn_27,qn_28,qn_29,qn_30,qn_31,qn_32,qa_33,qb_33,qc_33,qd_33,qe_33,qf_33,qg_33,qh_33,qi_33,qm_33,qn_33,qo_33,qn_34,qn_35,qn_36,qn_37,qn_38,qn_39,qn_40,qn_41,qe_41,qn_42,qn_43,qn_44,qn_45,qn_46,qn_47,qn_48,qn_49,qn_50,qn_51,qn_52,qn_53,qn_54,qe_54,qn_55,qn_56,qn_57,qn_58,qn_59,qn_60,qn_61,qn_62,qn_63,qn_64,qe_64,qn_65,qn_66,qn_67,qn_68,qn_69,qn_70,qn_71,qn_72,qn_73,qn_74,qn_75,qn_76,qn_77,qn_78,qn_79,qn_80,qa_81,qb_81,qc_81,qd_81,qe_81,qf_81,qg_81,qh_81,qj_81,qk_81,ql_81,qq_81,qr_81,qn_82,qn_83,qn_84,qn_85,qn_86,qn_87,qn_88,qn_89,qn_90,qn_91,qn_92,qn_93,qn_94,qn_95,qn_96,qn_97,qn_98,qn_99,qn_100,nn_101,qn_102,qn_103,qn_104,qn_105,qn_106,qa_107,qb_107,qc_107,qd_107,qe_107,qf_107,qg_107,qh_107,ni_107,qn_108,qn_109,qe_109,qn_110,qn_111,qe_111"
			 --->
			<!--- 正常終了時 --->
			<cfscript>
				executeinformation = StructNew();
				StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
				StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
				StructInsert(executeinformation,	"compFlg",		1								);
				StructInsert(executeinformation,	"ErrorQ",		#ErrorQ#						);
				StructInsert(executeinformation,	"csvData",		#csvData#						);
				StructInsert(executeinformation,	"fileName",		#arguments.fileName#			);
			</cfscript>
			<cfreturn executeinformation>
			
			<cfcatch type="any">
				<cfset QueryAddRow(ErrorQ,1)>
				<cfset QuerySetCell(ErrorQ,"detail","ファイルが開けませんでした。")>
				<!--- 戻値設定 --->
				<cfscript>
					executeinformation = StructNew();
					StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
					StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
					StructInsert(executeinformation,	"compFlg",		0								);
					StructInsert(executeinformation,	"ErrorQ",		#ErrorQ#						);
					StructInsert(executeinformation,	"fileName",		#arguments.fileName#			);
				</cfscript>
				<cfreturn executeinformation>
			</cfcatch>
		</cftry>
	</cffunction>	

	<!---
	*************************************************************************************************
	* Class				:csv_entry
	* Method			:csvCheck
	* Description		:
	* Custom Attributes	:fileName
	* Return Paramters	:none
	* History			:1) Coded by	2005/08/12 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="csvCheck" output="false">
		<cfargument name="fileName"   type="string" required="true">
		<cfargument name="ErrorQ"   type="Query" required="true">
		<cfargument name="csvData"  type="Query" required="true">

		<cfset ErrorQ = arguments.ErrorQ>
		<cfset csvData = arguments.csvData>
		<cftry>
				
			<!--- データチェック 
			<cfloop query="csvData">
				<!--- 必須チェック 
				<cfif Len(MON_POINT_ID) EQ 0>
					<cfx_Message locale="#session.langukbn#" key="label.upload.message4" return="REQ_MSG">
					<cfx_Message locale="#session.langukbn#" key="action.label.header3" return="MON_POINT_MSG">
					<cfset QueryAddRow(ErrorQ,1)>
					<cfset QuerySetCell(ErrorQ,"ErrNumber",currentrow+1)>
					<cfset QuerySetCell(ErrorQ,"message",MON_POINT_MSG)>
					<cfset QuerySetCell(ErrorQ,"detail",REQ_MSG)>
				--->
				<cfelse>
					<!--- 型チェック 
					<cfif IsNumeric(MON_POINT_ID) EQ false>
						<cfset QueryAddRow(ErrorQ,1)>
						<cfset QuerySetCell(ErrorQ,"ErrNumber",currentrow+1)>
						<cfset QuerySetCell(ErrorQ,"message",MON_POINT_MSG)>
						<cfset QuerySetCell(ErrorQ,"detail",DATA_MSG)>
					</cfif>
					--->
					<!--- 桁数チェック 
					<cfif Len(MON_POINT_ID) GT 6>
						<cfx_Message locale="#session.langukbn#" key="label.upload.message6" return="DATA_MSG">
						<cfx_Message locale="#session.langukbn#" key="action.label.header3" return="MON_POINT_MSG">
						<cfset QueryAddRow(ErrorQ,1)>
						<cfset QuerySetCell(ErrorQ,"ErrNumber",currentrow+1)>
						<cfset QuerySetCell(ErrorQ,"message",MON_POINT_MSG)>
						<cfset QuerySetCell(ErrorQ,"detail",DATA_MSG)>
					</cfif>
					--->
					<!--- マスタ存在チェック 
					<cfquery name="getMonpoint" dataSource="#request.app.dsn#">
						select 
							MON_POINT_ID 
						from
							MON_POINT 
						where 
							MON_POINT_ID = #MON_POINT_ID#
					</cfquery>
					--->
					<cfif getMonpoint.recordcount EQ 0>
						<cfset QueryAddRow(ErrorQ,1)>
						<cfset QuerySetCell(ErrorQ,"ErrNumber",currentrow+1)>
						<cfset QuerySetCell(ErrorQ,"message",MON_POINT_MSG)>
						<cfset QuerySetCell(ErrorQ,"detail",MSTEXSTERR_MSG)>
					</cfif>	
				</cfif>
			</cfloop>
			--->
			
			<!--- エラー存在時 --->
			<cfif ErrorQ.recordcount GT 0>
				<cfscript>
					executeinformation = StructNew();
					StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
					StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
					StructInsert(executeinformation,	"compFlg",		0								);
					StructInsert(executeinformation,	"ErrorQ",		#ErrorQ#						);
					StructInsert(executeinformation,	"fileName",		#arguments.fileName#			);
				</cfscript>
				<cfreturn executeinformation>
			<cfelse>
				<!--- 正常終了時 --->
				<cfscript>
					executeinformation = StructNew();
					StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
					StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
					StructInsert(executeinformation,	"compFlg",		1								);
					StructInsert(executeinformation,	"ErrorQ",		#ErrorQ#						);
					StructInsert(executeinformation,	"csvData",		#csvData#						);
					StructInsert(executeinformation,	"fileName",		#arguments.fileName#			);
				</cfscript>


				<cfreturn executeinformation>
			</cfif>
			
			<cfcatch type="any">

				<cfset QueryAddRow(ErrorQ,1)>
				<cfset QuerySetCell(ErrorQ,"detail",'マスタに存在しないデータの登録情報があります。')>

				<!--- 戻値設定 --->
				<cfscript>
					executeinformation = StructNew();
					StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
					StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
					StructInsert(executeinformation,	"compFlg",		0								);
					StructInsert(executeinformation,	"ErrorQ",		#ErrorQ#						);
					StructInsert(executeinformation,	"fileName",		#arguments.fileName#			);
				</cfscript>
				<cfreturn executeinformation>
			</cfcatch>
		</cftry>
	</cffunction>	

	<!---
	*************************************************************************************************
	* Class				:csv_entry
	* Method			:csvDelete
	* Description		:活動量データCSV登録
	* Custom Attributes	:none
	* Return Paramters	:none
	* History			:1) Coded by	2005/08/12 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="csvDelete" output="false">
		<cfargument name="fileName"   type="string" required="true">
		<cftry>
			<cfset dirpath = ExpandPath(#Application.outputDirectory#)>
			<cffile action = "delete" file = "#dirpath##arguments.fileName#">

			<!--- 戻値設定 --->
			<cfscript>
				executeinformation = StructNew();
				StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
				StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
				StructInsert(executeinformation,	"compFlg",		1								);
			</cfscript>
			<cfreturn executeinformation>
			
			<cfcatch type="any">
				<!--- 戻値設定 --->
				<cfscript>
					executeinformation = StructNew();
					StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
					StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
					StructInsert(executeinformation,	"compFlg",		0								);
				</cfscript>
				<cfreturn executeinformation>
			</cfcatch>
		</cftry>
	</cffunction>


	<!---
	*************************************************************************************************
	* Class				:csv_entry
	* Method			:csvEntry
	* Description		:
	* Custom Attributes	:none
	* Return Paramters	:none
	* History			:1) Coded by	2005/08/12 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="csvEntry" output="false">
		<cfargument name="csvData"   type="Query" required="true">

		<cftry>
			<!--- 回答値格納クエリ作成 --->
			<cfset retQuery = QueryNew("reserve_code,questionnairesheet_id,questionnaire_id,answer,answertext")>

			<cftransaction>
				<cfloop query="csvData">
					<!--- 予約情報確認 --->
					<cfquery name="getReservedata" datasource = "#Application.datasource#">
						select
							reserve_code,
							arrive_date,
							depart_date
						from
							rtt_reserve
						where
							reserve_id = #reserve_id#
					</cfquery>
				 	<cfif getReservedata.recordcount EQ 1>
						<!--- 回答情報テーブル内確認 --->
						<cfquery name="getAnswerdata" datasource = "#Application.datasource#">
							select
								reserve_code
							from
								hst_answers
							where
								reserve_code = #getReservedata.reserve_code#
						</cfquery>
					 	<cfif getAnswerdata.recordcount EQ 0>
					 	<!--- 
							予約番号【#Mid(reserve_id, 4, Len(reserve_id)-7)#】<br>
						 	予約コード【#getReservedata.reserve_code#】<br>
						 	到着日【#getReservedata.arrive_date#】<br>
						 	出発日【#getReservedata.depart_date#】<br>
						 	回答日【#DateFormat(Mid(time_end,1,10),'YYYY/MM/DD')#】<br>
						  --->	
							<!--- 回答情報格納 
							<cfquery name="setAnswersdata" datasource = "#Application.datasource#">
						 		insert into hst_answers(
						 			reserve_code,
						 			questionnairesheet_id,
						 			registrated,
						 			arrive_date,
						 			depart_date
						 		)values(
						 			#getReservedata.reserve_code#,
						 			1,
						 			'#DateFormat(Mid(time_end,1,10),'YYYY/MM/DD')#',
						 			'#getReservedata.arrive_date#',
						 			'#getReservedata.depart_date#'
						 		)
							</cfquery>
							--->
					 	</cfif>

						<cfloop index = "columnIndex" list = "#answers.columnList#">
							<cfset questionid = Mid(columnIndex, 4, Len(columnIndex))>
							<cfset optionno = #Evaluate(columnIndex)#>
							<cfset isnum = Mid(columnIndex, 1, 1)>

							<cfif Len(#Evaluate(columnIndex)#) NEQ 0 AND IsNumeric(questionid)>
								<!--- データ整形 --->
								<cfif questionid EQ 81>
									<cfif optionno EQ 8>		<cfset optionno = 19>
									<cfelseif optionno EQ 3>	<cfset optionno = 14>
									<cfelse>					<cfset optionno = optionno>
									</cfif>
								</cfif>					
								<cfif IsNumeric(optionno) AND isnum NEQ "N">
									<!--- 回答ラベル取得 --->
									<cfquery name="getAnswerOptions" datasource = "#Application.datasource#">
										select
											options_label
										from
											mst_answeroptions
										where
											questionnaire_id = #questionid#
										and
											options_no = #optionno#
									</cfquery>
									<cfset answer = optionno>
									<cfset answerText = getAnswerOptions.options_label>
								<cfelse>
									<cfset answer = 0>
									<!---  --->
									<cfset illigalchr = """">
									<cfset changechr = "">
									<cfset answerText = ReplaceList(#Evaluate(columnIndex)#,illigalchr,changechr)>
									
									<!--- <cfset answerText = #Evaluate(columnIndex)#> --->
								</cfif>
								<cfset QueryAddRow(retQuery,1)>
								<cfset QuerySetColumn(retQuery,"reserve_code",#getReservedata.reserve_code#)>
								<cfset QuerySetColumn(retQuery,"questionnairesheet_id",1)>
								<cfset QuerySetColumn(retQuery,"questionnaire_id",#questionid#)>
								<cfset QuerySetColumn(retQuery,"answer",answer#)>
								<cfset QuerySetColumn(retQuery,"answertext",#answerText#)>
								<!--- 回答データ格納 
								<cfquery name="setAnswers" datasource = "#Application.datasource#">
							 		insert into hst_answersdetails(
							 			reserve_code,
							 			questionnairesheet_id,
							 			questionnaire_id,
							 			answer,
							 			answertext
							 		)values(
							 			#getReservedata.reserve_code#,
							 			1,
							 			#questionid#,
							 			#answer#,
							 			'#answerText#'
							 		)
								</cfquery>
								--->
								<!--- 
								<cfif questionid EQ 107 AND isnum EQ "N">
								#isnum#【#questionid#】:【回答値：#answer#】【回答：#answerText#】 #columnIndex#<br>
								</cfif>
								 --->
							</cfif>
						</cfloop>
				 	</cfif>
				</cfloop>
			</cftransaction>


			<cfdump var=#retQuery#>
			<cfabort>

			<!--- 戻値設定 --->
			<cfscript>
				executeinformation = StructNew();
				StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
				StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
				StructInsert(executeinformation,	"compFlg",		1								);
			</cfscript>
			<cfreturn executeinformation>
			
			<cfcatch type="any">
				<!--- 戻値設定 --->
				<cfscript>
					executeinformation = StructNew();
					StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
					StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
					StructInsert(executeinformation,	"compFlg",		0								);
				</cfscript>
				<cfreturn executeinformation>
			</cfcatch>
		</cftry>
	</cffunction>

</cfcomponent>
