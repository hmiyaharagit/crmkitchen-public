<cfsilent>
<cfsavecontent variable="buttonStyle">
   corner-radius: 2;
    borderThickness: 0;
   fill-colors: #B4E055, #9FD32E;
   color: #ffffff;
</cfsavecontent>
<cfsavecontent variable="progressBarStyle">
   border-thickness:0;
   corner-radius: 0;
	 fill-colors: #ffffff, #DEEC6A;
	 theme-color: #A2DA2C;
	 border-color:#A2DA2C;
	 color:#ffffff;
</cfsavecontent>
<cfsavecontent variable="outputStyle">
	borderStyle:none;
	disabledColor:#333333;
	backgroundAlpha:0;
</cfsavecontent>
<cfsavecontent variable="contentPanelStyle">
	panelBorderStyle:'roundCorners';
	backgroundColor:#EFF7DF;
	headerColors:#CBEC84, #B0D660;
</cfsavecontent>	
</cfsilent>

<cfif isdefined("form.um")>
	<cflocation url="m_loginuser.cfm" addtoken="no">
</cfif>
<cfif GetAuthUser() eq "admin">
	<cfmodule template="header.cfm" title="トップ">
	<p>
	<cfform name="menuform" height="400" width="600" format="Flash" skin="HaloBlue" method="post">
		<cfformgroup type="accordion" height="300" style="marginTop: 0">
<!--- 
			<cfformgroup type="page" label="-- TAPデータCSVインポート">
				<cfformitem type="HTML">
					ＴＡＰから出力されたＣＳＶファイルをインポートします。<br>
				</cfformitem>
			<cfformgroup type="panel" label="With styles" style="#contentPanelStyle#" width="400">
			<cf_flashUpload name="styled" actionFile="upload.cfm">
					<cf_flashUploadInput progressBarStyle="#progressBarStyle#" buttonStyle="#buttonStyle#" inputWidth="150" />
				</cf_flashUpload>	
			</cfformgroup>
			</cfformgroup>
 --->
			<cfformgroup type="page" label="-- ユーザーマスタメンテナンス">
				<cfformitem type="html">
					CRMKitchenログインユーザー情報の新規登録、編集、削除などのメンテナンスを行います。<br>
					登録できる内容は、ユーザーID・パスワード・ユーザー名称・権限（0or2）です。
				</cfformitem>
				<cfinput type = "submit" name="um" width="100" value = "表示">
			</cfformgroup>
<!--- 
			<cfformgroup type="page" label="ユーザーマスタメンテナンス">
				<cfformitem type="HTML">ログインユーザーのマスタメンテナンスを行います。</cfformitem>
			</cfformgroup>
 --->
 	 	</cfformgroup>
		<cfformgroup type="horizontal">
			<cfinput type = "submit" name="logout" width="100" value = "ログアウト">
		</cfformgroup>
	</cfform>
	</body>
	</html>
</cfif>
