﻿<cfcomponent hint="" name="customeredit">
	<cfparam name="session.user_info.resort_id" default="10">
	<cfparam name="session.user_info.user_ip" default="">
	
	<!---
	*************************************************************************************************
	* Class				:specsheet
	* Method			:getCustomerDetail
	* Description		:顧客詳細（スペックシート内データ）検索
	* Custom Attributes	:customer_code			顧客番号
	* Return Paramters	:
	* History			:1) Coded by	2004/10/06 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getCustomerDetail" access="remote" returntype="query" hint="">
		<cfargument name="customer_code" type="numeric" default="1">
		<cfsetting enablecfoutputonly="yes">
		<cftry>
								
			<cfquery name="getCustomerDetail" datasource = "#Application.datasource#">
				select
					C.customer_code,
					C.name as customer_nm,
					C.name_kana as customer_kana,
					--G.name as gender,
					C.gender_code as gender_code,
					C.birth_date as birthday,
					--A.name as area,
					C.area_code as area_code,
					C.address_zipcode as zipcode,
					C.address_segment_1 as address1,
					C.address_segment_2 as address2,
					C.phone_number_1 as tel1,
					C.mail_address_1 as email1,
					C.phone_number_2 as tel2,
					C.mail_address_2 as email2,
					C.information
				from
					#Application.app00.dbu#.rtm_customer as C,
					#Application.app00.dbu#.rtm_gender as G,
					#Application.app00.dbu#.rtm_area as A
				where
					C.customer_code = #arguments.customer_code#
				and
					C.gender_code = G.gender_code
				and
					C.area_code = A.area_code
		 	</cfquery>

			
			<!--- 対象顧客の予約情報を取得 --->
			<cfquery name="getCustomerReserve" datasource = "#Application.datasource#">
				select
					R.reserve_code
				from
					#Application.app00.dbu#.rtt_reserve as R,
					#Application.app00.dbu#.rtm_customer as C
				where
					C.customer_code = #getCustomerDetail.customer_code#
				and
					R.customer_code = C.customer_code
				and
					depart_date >= '#DateFormat(Now(),'YYYY/MM/DD')#'
				and
					cancel_code = 100000000000	
			</cfquery>

			<cfset tmp_reserve_code = ArrayNew(1)>
			<cfset tmp_reserve_code[1] = ValueList(getCustomerReserve.reserve_code)>

			<!--- 対象顧客のエリア情報を取得 --->
			<cfquery name="getArea" datasource = "#Application.datasource#">
				select
					area_code,
					name
				from
					#Application.app00.dbu#.rtm_area
				order by
					area_code
			</cfquery>
			<cfset tmp_alist = ValueList(getArea.area_code)>	
			<cfset tmp_area_code = ArrayNew(1)>
	 		<cfset QueryAddColumn(getCustomerDetail, "areaindex", tmp_area_code)>

			<!--- 対象顧客の性別情報を取得 --->
			<cfquery name="getGender" datasource = "#Application.datasource#">
				select
					gender_code,
					name
				from
					#Application.app00.dbu#.rtm_gender
				order by
					gender_code
			</cfquery>
			<cfset tmp_glist = ValueList(getGender.gender_code)>	
			<cfset tmp_gender_code = ArrayNew(1)>
	 		<cfset QueryAddColumn(getCustomerDetail, "genderindex", tmp_gender_code)>

<!--- 
 --->
 			<cfloop query="getCustomerDetail">
				<cfset tmp_aindex = ListFindNoCase(tmp_alist, getCustomerDetail.area_code)>
				<cfset getCustomerDetail.areaindex = tmp_aindex>
				
				<cfset tmp_gindex = ListFindNoCase(tmp_glist, getCustomerDetail.gender_code)>
				<cfset getCustomerDetail.genderindex = tmp_gindex>

			</cfloop>

			
			<!--- クエリに予約情報をを追加 --->
			<cfset nColumnNumber = QueryAddColumn(getCustomerDetail, "reserve_code", tmp_reserve_code)>
			
			<!--- 対象顧客の顧客満足度情報を取得 --->
			<cfif getCustomerReserve.recordcount NEQ 0>
				<cfquery name="getCustomerSatisfied" datasource = "#Application.datasource#">
					select
						AD.answertext,
						A.depart_date
					from
						#Application.app00.dbu#.hst_answersdetails as AD,
						#Application.app00.dbu#.hst_answers as A
					where
						AD.reserve_code IN (#tmp_reserve_code[1]#)
					and
						AD.questionnaire_id = 6
					and
						AD.reserve_code = A.reserve_code
					order by
						A.depart_date asc;
				</cfquery>
				<!--- <cfdump var=#getCustomerSatisfied#> --->

				<cfset PointArray = ArrayNew(1)>
				<!---<cfset PointArray[1] = "100,50,120">--->
				<cfset PointArray[1] = ValueList(getCustomerSatisfied.answertext)>
				
				<cfset AnswerdArray = ArrayNew(1)>
				<!---<cfset AnswerdArray[1] = "#DateFormat(2004/05/01,'YY/MM/DD')#,#DateFormat(2004/06/15,'YY/MM/DD')#,#DateFormat(2004/08/01,'YY/MM/DD')#">--->
				<cfset AnswerdArray[1] = ValueList(getCustomerSatisfied.depart_date)>
			<cfelse>	
				<cfset PointArray = ArrayNew(1)>
				<cfset PointArray[1] = "">
				<cfset AnswerdArray = ArrayNew(1)>
				<cfset AnswerdArray[1] = "">
			</cfif>
			<!--- クエリに予約情報をを追加 --->
			<cfset nColumnNumber = QueryAddColumn(getCustomerDetail, "satisfied", PointArray)>
			<cfset nColumnNumber = QueryAddColumn(getCustomerDetail, "answerd", AnswerdArray)>
		
			<cfreturn getCustomerDetail>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<!---
	*************************************************************************************************
	* Class				:specsheet
	* Method			:getCustomerReserve
	* Description		:顧客予約情報（スペックシート内データ）検索
	* Custom Attributes	:reserveIDList	予約番号リスト（String）
	* Return Paramters	:
	* History			:1) Coded by	2004/10/06 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getCustomerReserve" access="remote" returntype="query" hint="">
		<cfargument name="reserve_codeList" type="String" default="">
		<cfargument name="customer_code" type="Numeric" default="">

		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<cfset reserve_codeList = ListToArray(arguments.reserve_codeList)>
			<cfset tmp_answer = 0>
			<!--- 回答履歴取得 --->
			<cfquery name="getCustomer" datasource = "#Application.datasource#">
				select
					R.reserve_code
				from
					#Application.app00.dbu#.rtt_reserve as R,
					#Application.app00.dbu#.rtm_customer as C
				where
					R.customer_code = C.customer_code
				and
					R.customer_code = #arguments.customer_code#
	 		</cfquery>
			<cfif getCustomer.recordcount NEQ 0>
				<cfquery name="getCustomerAnswer" datasource = "#Application.datasource#">
					select
						reserve_code
					from
						#Application.app00.dbu#.hst_answers
					where
						reserve_code IN(#ValueList(getCustomer.reserve_code)#)
		 		</cfquery>
				<cfif getCustomerAnswer.recordcount GTE 1>
					<cfset tmp_answer = 1>
				</cfif>
			</cfif>	

			<!--- 戻値クエリ作成 --->
			<cfset retQuery = QueryNew("reserve_code, arrive_date, depart_date, csurl, repeatflg")>

			<cfloop from="1" to=#ArrayLen(reserve_codeList)# index="i">
				<!--- AddRow --->
				<cfset newRow = QueryAddRow(retQuery, 1)>
				<cfquery name="getCustomerDetail" datasource = "#Application.datasource#">
					select
						R.reserve_code,
						R.depart_date,
						R.arrive_date
					from
						#Application.app00.dbu#.rtt_reserve as R
					where
						R.reserve_code = #reserve_codeList[i]#
					order by
						R.depart_date desc;
		 		</cfquery>
		 		<cfset tempCell = QuerySetCell(retQuery, "reserve_code", #getCustomerDetail.reserve_code#)>
		 		<cfset tempCell = QuerySetCell(retQuery, "depart_date", #getCustomerDetail.depart_date#)>
		 		<cfset tempCell = QuerySetCell(retQuery, "arrive_date", #getCustomerDetail.arrive_date#)>
				<cfset wk_cd_s = Left(CreateUUID(),4)>
				<cfset wk_cd_e = Right(CreateUUID(),4)>
				<cfset temp_websccd = #Application.CSURL#&#wk_cd_s#&#getCustomerDetail.reserve_code#&#wk_cd_e# >
		 		<cfset tempCell = QuerySetCell(retQuery, "csurl", #temp_websccd#)>
		 		<cfset tempCell = QuerySetCell(retQuery, "repeatflg", #tmp_answer#)>
			</cfloop>
			<cfreturn retQuery>
			<cfsetting enablecfoutputonly="No">
			
			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>

	</cffunction>


	<!---
	*************************************************************************************************
	* Class				: specsheet
	* Method			: getMaster
	* Description		: マスタ情報取得
	* Custom Attributes	: 
	* 					: 
	* Return Paramters	: getArea(Query)
	* History			: 1) Coded by	2005/06/29 h.miyahara(NetFusion)
	*					: 2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getMaster" access="remote" returntype="any">
		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<!--- 戻値クエリ作成 --->
			<cfset retQuery = QueryNew("rtm_area, rtm_gender")>
			<cfset QueryAddRow(retQuery, 1)>
			<cfquery name="getArea" datasource = "#Application.datasource#">
				select
					area_code,
					name
				from
					#Application.app00.dbu#.rtm_area
				order by
					area_code
			</cfquery>
			<cfquery name="getGender" datasource = "#Application.datasource#">
				select
					gender_code,
					name
				from
					#Application.app00.dbu#.rtm_gender
				order by
					gender_code
			</cfquery>
			<cfset QuerySetCell(retQuery, "rtm_area", #getArea#)>
			<cfset QuerySetCell(retQuery, "rtm_gender", #getGender#)>

			<cfreturn retQuery>
			<cfsetting enablecfoutputonly="No">
			
			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<!---
	*************************************************************************************************
	* Class				:
	* Method			:setCustomerData
	* Description		:顧客情報登録・編集 
	* Custom Attributes	:
	* Return Paramters	:
	* History			:1) Coded by	2005/06/30 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="setCustomerData" access="remote" returntype="any">
		<cfargument name="customer_code" type="Numeric" default="">
		<cfargument name="customer_nm" type="String" default="">
		<cfargument name="customer_kana" type="String" default="">
		<cfargument name="gender" type="Numeric" default="">
		<cfargument name="birthday" type="String" default="">
		<cfargument name="area" type="Numeric" default="">
		<cfargument name="zipcode" type="String" default="">
		<cfargument name="address1" type="String" default="">
		<cfargument name="address2" type="String" default="">
		<cfargument name="tel1" type="String" default="">
		<cfargument name="email1" type="String" default="">
		<cfargument name="tel2" type="String" default="">
		<cfargument name="email2" type="String" default="">
		<cfargument name="information" type="String" default="">
		<cfargument name="arrive_date" type="String" default="">
		<cfargument name="depart_date" type="String" default="">
		<cfargument name="cancel_flg" type="Boolean" default="">
		<cfargument name="update_flg" type="Boolean" default="">
		<cfargument name="reserve_code" type="Numeric" default="">
		<cfargument name="reg_flg" type="Boolean" default="">

		<cftry>

		<cfset tmp_cancel_code = 100000000000>
		<cfif arguments.cancel_flg EQ "true">
			<cfset tmp_cancel_code = 100000000001>
			<cfset arguments.update_flg = "true">
		</cfif>
	
		<!--- レコードチェック --->
		<cfquery name="getCustomer" datasource = "#Application.datasource#">
			select
				customer_code
			from
				#Application.app00.dbu#.rtm_customer
			where
				customer_code = #arguments.customer_code#
		</cfquery>
		<cfquery name="getReserve" datasource = "#Application.datasource#">
			select
				reserve_code
			from
				#Application.app00.dbu#.rtt_reserve
			where
				reserve_code = #arguments.reserve_code#
		</cfquery>
		<cfset tmp_reserve_code = "">
		<cfif getReserve.recordCount EQ 0>
			<cfset tmp_reserve_code = -1>
		</cfif>

		<cftransaction action="BEGIN" isolation = "read_committed">
		<cfif getCustomer.RecordCount EQ 0>
			<cfquery name="setCustomerData" datasource = "#Application.datasource#">
				insert into
					#Application.app00.dbu#.rtm_customer(
						name,
						name_kana,
						gender_code,
						birth_date,
						area_code,
						address_zipcode,
						address_segment_1,
						address_segment_2,
						phone_number_1,
						mail_address_1,
						phone_number_2,
						mail_address_2,
						information
					)
				values(
						'#arguments.customer_nm#',
						'#arguments.customer_kana#',
						#arguments.gender#,
						'#arguments.birthday#',
						#arguments.area#,
						'#arguments.zipcode#',
						'#arguments.address1#',
						'#arguments.address2#',
						'#arguments.tel1#',
						'#arguments.email1#',
						'#arguments.tel2#',
						'#arguments.email2#',
						'#arguments.information#'
				)
			</cfquery>
		<cfelse>
			<cfquery name="setCustomerData" datasource = "#Application.datasource#">
				update
					rtm_customer
				set
						name = '#arguments.customer_nm#',
						name_kana = '#arguments.customer_kana#',
						gender_code = #arguments.gender#,
						birth_date = '#arguments.birthday#',
						area_code = #arguments.area#,
						address_zipcode = '#arguments.zipcode#',
						address_segment_1 = '#arguments.address1#',
						address_segment_2 = '#arguments.address2#',
						phone_number_1 = '#arguments.tel1#',
						mail_address_1 = '#arguments.email1#',
						phone_number_2 = '#arguments.tel2#',
						mail_address_2 = '#arguments.email2#',
						information = '#arguments.information#'
				where
					customer_code = #arguments.customer_code#
			</cfquery>
		</cfif>
		<cfif arguments.reg_flg EQ "true">
			<cfif arguments.update_flg EQ "false" or tmp_reserve_code EQ -1>
				<cfquery name="getTarget" datasource = "#Application.datasource#" maxRows = "1">
					select
						customer_code
					from
						rtm_customer
					order by
						customer_code desc
				</cfquery>
				<cfset arguments.customer_code = getTarget.customer_code>
				<cfquery name="setReserveData" datasource = "#Application.datasource#">
					insert into	rtt_reserve(
						reserve_code,
						customer_code,
						arrive_date,
						depart_date,
						cancel_code,
						information
					)values(
						#arguments.reserve_code#,
						#getTarget.customer_code#,
						'#arguments.arrive_date#',
						'#arguments.depart_date#',
						#tmp_cancel_code#,
						''
					)
				</cfquery>
			<cfelse>
				<cfquery name="setReserveData" datasource = "#Application.datasource#">
					update
						rtt_reserve
					set
						arrive_date = '#arguments.arrive_date#',
						depart_date = '#arguments.depart_date#',
						cancel_code = #tmp_cancel_code#
					where
						reserve_code = #arguments.reserve_code#
				</cfquery>
			</cfif>
		</cfif>
		<!--- 対象顧客の回答情報を取得 --->
		<cfquery name="getCustomerReserve" datasource = "#Application.datasource#">
			select
				R.reserve_code
			from
				rtt_reserve as R,
				rtm_customer as C
			where
				C.customer_code = #arguments.customer_code#
			and
				R.customer_code = C.customer_code
		</cfquery>
		<cfset tmp_reserve_code = ValueList(getCustomerReserve.reserve_code)>
		<!--- 対象顧客の回答値データを更新 --->
		<cfloop index = "r_code" list = "#tmp_reserve_code#"> 
			<cfquery name="setCustomerData" datasource = "#Application.datasource#">
				update
					hst_answers
				set
					area_code = #arguments.area#
				where
					reserve_code = #r_code#
			</cfquery>
		</cfloop>

		</cftransaction>
		<cfset wk_cd_s = Left(CreateUUID(),4)>
		<cfset wk_cd_e = Right(CreateUUID(),4)>
		<cfset temp_websccd = #Application.CSURL#&#wk_cd_s#&#arguments.reserve_code#&#wk_cd_e# >
		
		<!--- 戻値設定 --->		
		<cfscript>
			executeinformation = StructNew();
			StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
			StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
			StructInsert(executeinformation,	"errorFlg",	0	);
			StructInsert(executeinformation,	"reserve_code",		arguments.reserve_code		);
			StructInsert(executeinformation,	"customer_code",	arguments.customer_code		);
			StructInsert(executeinformation,	"csurl",		temp_websccd					);			
			StructInsert(executeinformation,	"errorMsg",		"顧客情報を登録しました"		);
		</cfscript>
		<cfreturn executeinformation>
		<cfcatch type="any">
		<cftransaction action="rollback"/>
 		<cfrethrow>
	</cfcatch>
	</cftry>

	</cffunction>

	<!---
	*************************************************************************************************
	* Class				:
	* Method			:delCustomerData
	* Description		:顧客情報削除（論理削除） 
	* Custom Attributes	:customer_code		顧客コード
	* Return Paramters	:
	* History			:1) Coded by	2005/06/30 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="delCustomerData" access="remote" returntype="any">
		<cfargument name="customer_code" type="Numeric" default="">
		<cftry>

		<cftransaction action="BEGIN">
			<cfquery name="deleteSummary" datasource = "#Application.datasource#">
				UPDATE
					rtm_customer
				SET
					active_flg = 0
				WHERE
					customer_code = #arguments.customer_code#
			</cfquery>
		</cftransaction>
		<!--- 戻値設定 --->		
		<cfscript>
			executeinformation = StructNew();
			StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
			StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
			StructInsert(executeinformation,	"errorMsg",		"顧客情報が削除されました。"	);
		</cfscript>
		<cfreturn executeinformation>
		<cfcatch type="any">
		<cftransaction action="rollback"/>
<!--- 
		<cf_error_log
			label = "setUserAnswers"
			errorType = "#cfcatch.type#"
			message = "#cfcatch.message#"
			detail = "#cfcatch.detail#">
		<cfsetting enablecfoutputonly="No">
 --->
 		<cfrethrow>
	</cfcatch>
	</cftry>
	</cffunction>
</cfcomponent>
