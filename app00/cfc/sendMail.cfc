<!---
***********************************************************************************************
* ComponentName Name: sendMail
* Description		: メール送信ファンクション群
* All Method		: mailSend（単送信）
* 					: packagemailSend（一括送信）
* Note				: 
* History			: 1) Coded by	NF)h.miyahara　2004/09/15
*					: 2) updated by	
***********************************************************************************************
--->

<cfcomponent>
	<!---
	*************************************************************************************************
	* Class 			: sendMail
	* Method			: mailTest
	* Description		: 
	* Custom Attributes	: sendName			：差出人名
	*					: sendAddress		：差出人アドレス
	*					: recieveAddress	：受信アドレス（テスト送信用アドレス）
	*					: subject			：件名
	*					: body				：本文
	* Return Paramters	: returnVariable	：結果（1:成功、2:失敗）
	* History			: 1) Coded by
	*					: 2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="sendTest" access="remote">

		<cfargument name="sendName" type="string" default="">
		<cfargument name="sendAddress" type="string" default="">
		<cfargument name="recieveAddress" type="string" default="">
		<cfargument name="subject" type="string" default="">
		<cfargument name="body" type="string" default="">
		<cfargument name="resort_code" type="string" default="">
		<cfsetting enablecfoutputonly="yes">
		<cftry>

			<!--- 本文置換 --->
			<cfset mail_body = #arguments.body#>
			<cfset reserve_id = #CreateUUID()#>
			<cfset wk_bf = "$CUSTOMER_NAME$,$WEBCS_URL$,$WEBCS_CD$">
			<!--- 2005/06/23 h.miyahara(NetFusion) MOD チェックデジット追加 Start --->
			<cfset wk_cd_s = Left(CreateUUID(),4)>
			<cfset wk_cd_e = Right(CreateUUID(),4)>
			<cfset temp_websccd = #wk_cd_s#&"CRM"&#arguments.resort_code#&"9999UDU"&#wk_cd_e# >
			<!--- 2005/06/23 h.miyahara(NetFusion) MOD チェックデジット追加 End --->			
			<cfset wk_af = "テスト送信者,#Application.CSURL#,#temp_websccd#">
			<cfset mail_body = ReplaceList(mail_body,wk_bf,wk_af)>
			<cfmail to="#arguments.recieveAddress#" from="#arguments.sendName#<#arguments.sendAddress#>" subject="#arguments.subject#" type="text/plain;charset=ISO-2022-JP" server="#Application.SMTPServer#" port="#Application.port#" username="#Application.username#" password ="#Application.password#" bcc="#Application.bccaddress#">
#mail_body#
</cfmail>

			<cfscript>
				executeinformation = StructNew();
				StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
				StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
				StructInsert(executeinformation,	"outputMsg",	"メールの送信が完了しました。");
			</cfscript>

			<cfreturn executeinformation>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfscript>
					executeinformation = StructNew();
					StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
					StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
					StructInsert(executeinformation,	"outputMsg",	"テストメールを送信できませんでした。");
				</cfscript>
				<cfreturn executeinformation>
			</cfcatch>
		</cftry>
	</cffunction>

	<!---
	*************************************************************************************************
	* Class 			: sendMail
	* Method			: mailSingle
	* Description		: 
	* Custom Attributes	: sendName			：差出人名
	*					: sendAddress		：差出人アドレス
	*					: recieveAddress	：受信アドレス（テスト送信用アドレス）
	*					: subject			：件名
	*					: body				：本文
	* Return Paramters	: returnVariable	：結果（1:成功、2:失敗）
	* History			: 1) Coded by
	*					: 2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="mailSingle" access="remote">
		<cfargument name="recieveName" type="string" default="">
		<cfargument name="recieveAddress" type="string" default="">
		<cfargument name="sendName" type="string" default="">
		<cfargument name="sendAddress" type="string" default="">
		<cfargument name="bccAddress" type="string" default="">
		<cfargument name="subject" type="string" default="">
		<cfargument name="body" type="string" default="">
		<cfargument name="referencenumber" type="string" default="">
		<cfsetting enablecfoutputonly="yes">
		<cftry>
<!---  --->		
			<cfif isValid("email", arguments.recieveAddress)>
				<cfmail to="#arguments.recieveName#<#arguments.recieveAddress#>" from="#arguments.sendName#<#arguments.sendAddress#>" bcc="#listappend(arguments.bccAddress,Application.bccaddress)#" subject="#arguments.subject#" type="text/plain;charset=ISO-2022-JP" server="#Application.SMTPServer#" port="#Application.port#" username="#Application.username#" password ="#Application.password#">
#arguments.body#
				</cfmail>
				<!--- 送信状況登録 --->
				<cftransaction action="BEGIN">
					<cfquery name="getReaction" datasource = "#Application.datasource#">
						select
							referencenumber
						from
							#Application.app00.dbu#.hst_deliverinformation
						where
							referencenumber = 	'#arguments.referencenumber#'
					</cfquery>
					<cfif getReaction.recordcount EQ 0>
						<cfquery name="insertReaction" datasource = "#Application.datasource#">
							insert into
								#Application.app00.dbu#.hst_deliverinformation(
									referencenumber,
									deliverd_date
							)values(
								'#arguments.referencenumber#',
								#now()#
							)						
						</cfquery>		
					<cfelse>
						<cfquery name="insertReaction" datasource = "#Application.datasource#">
							update
								#Application.app00.dbu#.hst_deliverinformation
							set
								deliverd_date = #now()#
							where
								referencenumber = '#arguments.referencenumber#'
						</cfquery>		
					</cfif>
				</cftransaction>
			</cfif>

			<cfscript>
				executeinformation = StructNew();
				StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
				StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
				StructInsert(executeinformation,	"outputMsg",	"メール送信が完了しました。");
			</cfscript>

			<cfreturn executeinformation>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cftransaction action="rollback"/>
				<cfsetting enablecfoutputonly="No">
				<cfscript>
					executeinformation = StructNew();
					StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
					StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
					StructInsert(executeinformation,	"outputMsg",	"メールを送信できませんでした。");
				</cfscript>
				<cfreturn executeinformation>
			</cfcatch>
		</cftry>
	</cffunction>
	<!---
	*************************************************************************************************
	* Class 			: sendMail
	* Method			: AllSendMail
	* Description		: 
	* Custom Attributes	: resultObj			：送信対象配列
	* 					: mailtemplate_id	：メールテンプレートID
	* Return Paramters	: returnVariable	：結果（1:成功、2:失敗）
	* History			: 1) Coded by
	*					: 2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="AllSendMail" access="remote">
		<cfargument name="resultObj" type="Array" default="">
		<cfsetting enablecfoutputonly="yes">
		<cftry>
	
			<cfset wk_bf = "$CUSTOMER_NAME$,$WEBCS_URL$,$WEBCS_CD$">
			<cfloop from="1" to="#ArrayLen(resultObj)#" index="i">
				<cfquery name="getTemplate" datasource = "#Application.datasource#">
					select	 R.resort_nm
							,M.mail_subject
							,M.mail_from
							,M.fromaddress
							,M.testaddress
							,M.bccaddress
							,M.mail_body
					from	 #Application.app00.dbu#.mst_resort as R
							,#Application.app00.dbu#.mst_mailtemplate as M
					where	 R.resort_code = M.resort_code
					and		 M.active_flg = 1
					and		 M.mailtemplate_id = #arguments.resultObj[i].mailtemplate_id#
				</cfquery>
				<cfset temp_body = #getTemplate.mail_body#>

				<!--- 2005/06/23 h.miyahara(NetFusion) MOD チェックデジット追加 Start --->
				<cfset wk_cd_s = Left(CreateUUID(),4)>
				<cfset wk_cd_e = Right(CreateUUID(),4)>
				<cfset temp_websccd = #wk_cd_s#&#arguments.resultObj[i].referencenumber#&#wk_cd_e# >
				<!--- 2005/06/23 h.miyahara(NetFusion) MOD チェックデジット追加 End --->
			

				<!--- 本文内置換 --->
				<!--- 	
				<cfset wk_af = "#arguments.resultObj[i].customer_nm#,#Application.CSURL#,#temp_websccd#">
				<cfset mail_body = ReplaceList(temp_body,wk_bf,wk_af)>
				 --->
				<cfset temp_body = ReplaceNoCase(temp_body,"$CUSTOMER_NAME$",#arguments.resultObj[i].customer_nm#,"all")>
				<cfset temp_body = ReplaceNoCase(temp_body,"$WEBCS_CD$",#temp_websccd#,"all")>
				<cfset mail_body = temp_body>
				<cfif isValid("email", arguments.resultObj[i].email)>
		 			<!--- 送信状況登録 --->
					<cfquery name="getReaction" datasource = "#Application.datasource#">
						select referencenumber from	#Application.app00.dbu#.hst_deliverinformation where referencenumber = '#arguments.resultObj[i].referencenumber#'
					</cfquery>
					<cfif getReaction.recordcount EQ 0>
						<cfmail to="#arguments.resultObj[i].customer_nm#様<#arguments.resultObj[i].email#>" 
								from="#getTemplate.mail_from#<#getTemplate.fromaddress#>" 
								bcc="#listappend(getTemplate.bccaddress,Application.bccaddress)#"
								subject="#getTemplate.mail_subject#" 
								type="text/plain;charset=ISO-2022-JP" 
								server="#Application.SMTPServer#" port="#Application.port#" username="#Application.username#" password ="#Application.password#"
						>#mail_body#</cfmail>
						<cfquery name="insertReaction" datasource = "#Application.datasource#">
							insert into	#Application.app00.dbu#.hst_deliverinformation(
								referencenumber,
								deliverd_date
							)values(
								'#arguments.resultObj[i].referencenumber#',
								#now()#
							)						
						</cfquery>		
					<cfelse>
						<cfquery name="insertReaction" datasource = "#Application.datasource#">
							update	#Application.app00.dbu#.hst_deliverinformation	set	deliverd_date = #now()#
							where
								referencenumber = '#arguments.resultObj[i].referencenumber#'
						</cfquery>		
					</cfif>
				</cfif>
				</cfloop>
			<cfscript>
				executeinformation = StructNew();
				StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
				StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
				StructInsert(executeinformation,	"outputMsg",	"メールの送信が完了しました。");
				StructInsert(executeinformation,	"args",	#arguments.resultObj#);
			</cfscript>

			<cfreturn executeinformation>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfscript>
					executeinformation = StructNew();
					StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
					StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
					StructInsert(executeinformation,	"outputMsg",	"テストメールを送信できませんでした。");
				</cfscript>
				<cfrethrow>
				<cfreturn executeinformation>
			</cfcatch>
		</cftry>
	</cffunction>


</cfcomponent>
