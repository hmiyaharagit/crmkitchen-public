﻿<cfcomponent hint="スペックシート用情報取得ＣＦＣ">
	<cfparam name="session.user_info.resort_id" default="10">
	<cfparam name="cgi.remote_addr" default="">
	
	<!---
	*************************************************************************************************
	* Class				:specsheet
	* Method			:getSummaryData
	* Description		:スペックシート登録情報取得
	* Custom Attributes	:none
	* Return Paramters	:
	* History			:1) Coded by	2004/10/06 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getSummaryData" access="remote" returntype="query" hint="">
		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<cfset getSummary = querynew("summary_id,summary_nm,summary_ip,basicinfo,importantinfo,satisfiedinfo,stayinfo,csinfo,owner,")>
			<cfset queryaddrow(getSummary,1)>
			<cfset querysetcell(getSummary,"summary_id",1)>
			<cfset querysetcell(getSummary,"summary_nm","全表示")>
			<cfset querysetcell(getSummary,"basicinfo","1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16")>
			<cfset querysetcell(getSummary,"importantinfo","1")>
			<cfset querysetcell(getSummary,"satisfiedinfo","1")>
			<cfset querysetcell(getSummary,"stayinfo","1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31")>
			<cfset querysetcell(getSummary,"csinfo","1,2")>
			<cfset querysetcell(getSummary,"owner","F9CC3BE2-3472-9F6F-6CB3239010C14712")>
			<cfreturn getSummary>
			<cfsetting enablecfoutputonly="No">

		<cfcatch type="any">
			<cfsetting enablecfoutputonly="No">
			<cfrethrow>
		</cfcatch>
		</cftry>

	</cffunction>
	
	<!---
	*************************************************************************************************
	* Class				:specsheet
	* Method			:getCustomerDetail
	* Description		:顧客詳細（スペックシート内データ）検索
	* Custom Attributes	:customernumber			顧客番号
	* Return Paramters	:
	* History			:1) Coded by	2004/10/06	h.miyahara(NetFusion)
	*					:2) Updated by	2005/12/09	h.miyahara(NetFusion) MOD TAP連動
	*************************************************************************************************
	--->
	<cffunction name="getCustomerDetail" access="remote" returntype="query" hint="">
<!--- 
		<cfargument name="customernumber" type="String" default="SGW0000151">
 --->
		<cfargument name="customernumber" type="String" default="">
		<cfargument name="resort_code" type="string" default="">
		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<!---2011/05/11 有効参照範囲の施設取得 
			<cfif arguments.resort_code neq 0>
				<cfquery name="getloginUser" datasource = "#Application.datasource#">
					select	 resort_code
					from	m_loginuser
					where	login_id = '#session.user_info.login_id#'
					and		login_dv = 's'
				</cfquery>
				<cfset arguments.resort_code = ValueList(getloginUser.resort_code)>
			</cfif>
			--->
			<cfquery name="getCustomerDetail" datasource = "#Application.datasource#">
				select	 c.customernumber
						,c.customercategory
						,case when c.customercategory = 'shk' then '試泊'
						 else 'なし'
						 end  "customercategory"
						,c.customerrank
						,c.name as customer_nm
						,c.name_kana as customer_kana
						,c.gender_code as gender
						,c.birth_date as birthday
						,c.area_code as area
						,c.address_zipcode as zipcode
						,c.address_segment_1 as address1
						,c.address_segment_2 as address2
						,c.phone_number_1 as tel1
						,c.mail_address_1 as email1
						,c.phone_number_2 as tel2
						,c.mail_address_2 as email2
						,c.information
						,case when c.married_flg = 0 then '未婚'
						 when c.married_flg = 1 then '既婚'
						 else '不明'
						 end  "married"
				from	 #application.app00.dbu#.rtm_customer as c
				where	 c.customernumber = '#arguments.customernumber#'
				<cfif arguments.resort_code neq 0>
				and	 	 c.resort_code in ( #arguments.resort_code# )
				</cfif>

		 	</cfquery>
		 	<!--- 客種取得 --->
		 	<cfif Len(getCustomerDetail.customerrank) NEQ 0>
				<cfquery name="q_mst_control" datasource = "#Application.datasource#">
					select	 control_nm
					from	 #Application.app00.dbu#.mst_control
					where	 controldiv_id = 7
					and		 control_id = '#getCustomerDetail.customerrank#'
					<cfif arguments.resort_code neq 0>
					and	 	 resort_code in ( #arguments.resort_code# )
					</cfif>
					and		 active_flg = 1
			 	</cfquery>
			 	<cfset QuerySetCell(getCustomerDetail,"customerrank",q_mst_control.control_nm)>
			<cfelse>
			 	<cfset QuerySetCell(getCustomerDetail,"customerrank","未設定")>
			</cfif>
			<!--- 対象顧客の回答情報を取得 --->
			<cfquery name="getCustomerReserve" datasource = "#Application.datasource#">
				select	 R.referencenumber
				from	 #Application.app00.dbu#.rtt_reserve as R
						,#Application.app00.dbu#.rtm_customer as C
				where	 C.customernumber = '#getCustomerDetail.customernumber#'
				and		 R.customernumber = C.customernumber
				<cfif arguments.resort_code neq 0>
				and	 	 C.resort_code in ( #arguments.resort_code# )
				and	 	 R.resort_code in ( #arguments.resort_code# )
				</cfif>

			</cfquery>
			<cfset tmp_referencenumber = ArrayNew(1)>
			<cfset tmp_referencenumber[1] = ValueList(getCustomerReserve.referencenumber)>

			<cfset tmp_referencenumber_list="">
			<cfloop list="#tmp_referencenumber[1]#" index="idx">
				<cfset temp = "'" & idx & "'">
				<cfset tmp_referencenumber_list = ListAppend(tmp_referencenumber_list,temp)>
			</cfloop>

			<!--- <cfdump var=#tmp_reserve_code#> --->
			
			<!--- クエリに予約情報をを追加 --->
			<cfset nColumnNumber = QueryAddColumn(getCustomerDetail, "referencenumber", tmp_referencenumber)>
			
			<!--- 対象顧客の顧客満足度情報を取得 --->
			<cfif getCustomerReserve.recordcount NEQ 0>

				<cfquery name="getCustomerSatisfied" datasource = "#Application.datasource#">
					select	 AD.answertext
							,A.depart_date
					from	 #Application.app00.dbu#.hst_answersdetails as AD
							,#Application.app00.dbu#.hst_answers as A
					where	 AD.referencenumber IN (#PreserveSingleQuotes(tmp_referencenumber_list)#)
					and		 AD.questionnaire_id = #Application.app00.q_value_id#
					and		 AD.referencenumber = A.referencenumber
					<cfif arguments.resort_code neq 0>
					and	 	 AD.resort_code in ( #arguments.resort_code# )
					</cfif>
					order by A.depart_date asc;
				</cfquery>

				<cfset PointArray = ArrayNew(1)>
				<!---<cfset PointArray[1] = "100,50,120">--->
				<cfset PointArray[1] = ValueList(getCustomerSatisfied.answertext)>
				
				<cfset AnswerdArray = ArrayNew(1)>
				<!---<cfset AnswerdArray[1] = "#DateFormat(2004/05/01,'YY/MM/DD')#,#DateFormat(2004/06/15,'YY/MM/DD')#,#DateFormat(2004/08/01,'YY/MM/DD')#">--->
				<!--- <cfset AnswerdArray[1] = ValueList(getCustomerSatisfied.depart_date)> --->
				<cfset temp_depart_date = "">
				<cfloop list="#ValueList(getCustomerSatisfied.depart_date)#" index="idx">
					<cfset temp_depart_date = ListAppend(temp_depart_date,DateFormat(idx,'YY/MM/DD'))>
				</cfloop>
				<cfset AnswerdArray[1] = temp_depart_date>
			<cfelse>	
				<cfset PointArray = ArrayNew(1)>
				<cfset PointArray[1] = "">
				<cfset AnswerdArray = ArrayNew(1)>
				<cfset AnswerdArray[1] = "">
			</cfif>
			<!--- クエリに予約情報をを追加 --->
			<cfset nColumnNumber = QueryAddColumn(getCustomerDetail, "satisfied", PointArray)>
			<cfset nColumnNumber = QueryAddColumn(getCustomerDetail, "answerd", AnswerdArray)>
		
			<cfreturn getCustomerDetail>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<!---
	*************************************************************************************************
	* Class				:specsheet
	* Method			:getCustomerReserve
	* Description		:顧客予約情報（スペックシート内データ）検索
	* Custom Attributes	:customernumber	顧客番号
	* Return Paramters	:
	* History			:1) Coded by	2004/10/06 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getCustomerReserve" access="remote" returntype="query" hint="">
		<cfargument name="customernumber" type="String" default="">
		<cfargument name="resort_code" type="string" default="">
		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<!---2011/05/11 有効参照範囲の施設取得 --->
			<cfif arguments.resort_code neq 0>
				<cfquery name="getloginUser" datasource = "#Application.datasource#">
					select	 resort_code
					from	m_loginuser
					where	login_id = '#session.user_info.login_id#'
					and		login_dv = 's'
				</cfquery>
				<cfset arguments.resort_code = ValueList(getloginUser.resort_code)>
			</cfif>
			<cfset customernumber = arguments.customernumber>
			<cfquery name="getCustomerDetail" datasource = "#Application.datasource#">
				select	 R.reserve_code
						,R.room_code
						,R.customernumber
						,R.arrive_date
						,R.nights
						,R.depart_date
						,R.cancel_code
						,R.information
						,case when R.lateout_flg = 0 then 'なし'
						 when R.lateout_flg = 1 then 'あり'
						 else '不明'
						 end  "lateout_flg"
						,R.manpersoncount
						,R.womenpersoncount
						,R.childpersoncount_a
						,R.childpersoncount_b
						,R.childpersoncount_c
						,R.childpersoncount_d
						,R.roomtype
						,R.roomcontrol
						,R.reservationroute
						,R.totalsalesamount
						,R.totalsalesamountdividedbynights
						,R.personunitprice
						,R.roomcharge
						,R.optionamount
						,R.restaurantamount
						,R.referencenumber
						,R.surveymethod
						,HA.registrated
				from	 #Application.app00.dbu#.rtt_reserve as R left outer join #Application.app00.dbu#.hst_answers as HA on R.referencenumber = HA.referencenumber
				where	 R.customernumber = '#customernumber#'
				<cfif arguments.resort_code neq 0>
				and	 	 R.resort_code in ( #arguments.resort_code# )
				</cfif>
				order by R.depart_date asc;
	 		</cfquery>

	 		<!--- 項目追加 --->
	 		<cfset QueryAddColumn(getCustomerDetail,"valueinformation",ArrayNew(1))>
	 		<cfset QueryAddColumn(getCustomerDetail,"surveymethod_nm",ArrayNew(1))>
	 		<cfset QueryAddColumn(getCustomerDetail,"marketing_1",ArrayNew(1))>
	 		<cfset QueryAddColumn(getCustomerDetail,"marketing_2",ArrayNew(1))>
	 		<cfset QueryAddColumn(getCustomerDetail,"marketing_3",ArrayNew(1))>
	 		<cfset QueryAddColumn(getCustomerDetail,"shopresult",ArrayNew(1))>
	 		<cfset QueryAddColumn(getCustomerDetail,"cstype",ArrayNew(1))>
<!--- 
			<cfdump var=#getCustomerDetail#><cfabort>
 --->
			<cfset temp_referencenumber_list = "">
		 	<cfloop query="getCustomerDetail">
				<cfquery name="q_hst_valueinformation" datasource = "#Application.datasource#">
					select	 V.valueinfo
					from	 #Application.app00.dbu#.rtt_reserve as R
							,#Application.app00.dbu#.hst_valueinformation as V
					where	 R.customernumber = '#customernumber#'
					and		 R.reserve_code = #reserve_code#
					and		 R.room_code = #room_code#
					and		 R.resort_code = V.resort_code
					and		 R.reserve_code = V.reserve_code
					and		 R.room_code = V.room_code		
					<cfif arguments.resort_code neq 0>
					and	 	 R.resort_code in ( #arguments.resort_code# )
					</cfif>
					order by R.depart_date desc
		 		</cfquery>
		 		<cfset QuerySetCell(getCustomerDetail, "valueinformation", #q_hst_valueinformation.valueinfo#,currentrow)>
				<cfquery name="q_mst_control" datasource = "#Application.datasource#">
					select	 control_nm
					from	 #Application.app00.dbu#.mst_control
					where	 controldiv_id = #application.app00.c_roomtype#
					and		 (resort_code in (#arguments.resort_code#))
					and		 control_id = '#roomtype#'
		 		</cfquery>
		 		<cfset QuerySetCell(getCustomerDetail, "roomtype", #q_mst_control.control_nm#,currentrow)>
				<cfquery name="q_mst_control" datasource = "#Application.datasource#">
					select	 control_nm
					from	 #Application.app00.dbu#.mst_control
					where	 controldiv_id = 2
					and		 (resort_code in (#arguments.resort_code#))
					and		 control_id = '#roomcontrol#'
		 		</cfquery>
		 		<cfset QuerySetCell(getCustomerDetail, "roomcontrol", #q_mst_control.control_nm#,currentrow)>
				<cfquery name="q_mst_control" datasource = "#Application.datasource#">
					select	 control_nm
					from	 #Application.app00.dbu#.mst_control
					where	 controldiv_id = #application.app00.c_reservation#
					and		 (resort_code in (#arguments.resort_code#))
					and		 control_id = '#reservationroute#'
		 		</cfquery>
		 		<cfset QuerySetCell(getCustomerDetail, "reservationroute", #q_mst_control.control_nm#,currentrow)>
				<cfquery name="q_mst_control" datasource = "#Application.datasource#">
					select	 control_nm
					from	 #Application.app00.dbu#.mst_control
					where	 controldiv_id = 8
					and		 (resort_code in (#arguments.resort_code#))
					and		 control_id = '#surveymethod#'
		 		</cfquery>
		 		<cfset QuerySetCell(getCustomerDetail, "surveymethod_nm", #q_mst_control.control_nm#,currentrow)>
				<cfquery name="q_mst_control" datasource = "#Application.datasource#">
					select	 c.control_nm
					from	 #Application.app00.dbu#.mst_control as c
							,#Application.app00.dbu#.rtt_marketing as m
					where	 m.reserve_code = #reserve_code#
					and		 m.room_code = #room_code#
					and		 (c.resort_code in (#arguments.resort_code#))
					and		 c.controldiv_id = #application.app00.c_cognition#
					and		 c.controldiv_id = m.controldiv_id
					and		 c.control_id = m.control_id
			 		</cfquery>
		 		<cfset QuerySetCell(getCustomerDetail, "marketing_1", #q_mst_control.control_nm#,currentrow)>
				<cfquery name="q_mst_control" datasource = "#Application.datasource#">
					select	 c.control_nm
					from	 #Application.app00.dbu#.mst_control as c
							,#Application.app00.dbu#.rtt_marketing as m
					where	 m.reserve_code = #reserve_code#
					and		 m.room_code = #room_code#
					and		 (c.resort_code in (#arguments.resort_code#))
					and		 c.controldiv_id = #application.app00.c_purpose#
					and		 c.controldiv_id = m.controldiv_id
					and		 c.control_id = m.control_id
			 	</cfquery>
		 		<cfset QuerySetCell(getCustomerDetail, "marketing_2", #q_mst_control.control_nm#,currentrow)>
				<cfquery name="q_mst_control" datasource = "#Application.datasource#">
					select	 c.control_nm
					from	 #Application.app00.dbu#.mst_control as c
							,#Application.app00.dbu#.rtt_marketing as m
					where	 m.reserve_code = #reserve_code#
					and		 m.room_code = #room_code#
					and		 (c.resort_code in (#arguments.resort_code#))
					and		 c.controldiv_id = #application.app00.c_together#
					and		 c.controldiv_id = m.controldiv_id
					and		 c.control_id = m.control_id
			 	</cfquery>
		 		<cfset QuerySetCell(getCustomerDetail, "marketing_3", #q_mst_control.control_nm#,currentrow)>
				<cfquery name="q_rtt_shopresult" datasource = "#Application.datasource#">
					select	 shop_id
							,shop_nm
					from	 #Application.app00.dbu#.rtt_shopresult
					where	 reserve_code = #reserve_code#	
					and		 room_code = #room_code#
					and		 amount != 0
					order by shop_id
			 	</cfquery>
			 	<cfset shopidlist = ValueList(q_rtt_shopresult.shop_id,";")>
			 	<cfif q_rtt_shopresult.recordcount EQ 1>
				 	<cfset shopidlist = ListAppend(shopidlist,"",";")>
				 </cfif>	
		 		<cfset QuerySetCell(getCustomerDetail, "shopresult", #shopidlist#,currentrow)>
				<!--- ＣＳ未回答 --->
				<cfif Len(registrated) EQ 0>
			 		<cfset QuerySetCell(getCustomerDetail, "registrated", '0',currentrow)>
			 	</cfif>

				<cfset QuerySetCell(getCustomerDetail, "cstype", 0 ,currentrow)>
				<!--- ＣＳタイプ 2014/06/03 外す
				<cfif ListLen(temp_referencenumber_list) EQ 0>
			 		<cfset QuerySetCell(getCustomerDetail, "cstype", 0 ,currentrow)>
			 	<cfelse>
				 	<cfif ListFind(temp_referencenumber_list,#referencenumber#) EQ 1>
				 		<cfset QuerySetCell(getCustomerDetail, "cstype", 0 ,currentrow)>
				 	<cfelse>
				 		<cfset QuerySetCell(getCustomerDetail, "cstype", 1 ,currentrow)>
					</cfif>
				</cfif>
				<cfset temp_referencenumber_list = ListAppend(temp_referencenumber_list,#referencenumber#)>
				--->
			</cfloop>
			<!--- 日付順ソート --->
			<cfquery dbtype="query" name= "retQuery">
				select * from getCustomerDetail order by depart_date desc;
			</cfquery>
			<cfreturn retQuery>
			<cfsetting enablecfoutputonly="No">
			
			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>

	</cffunction>


	<!---
	*************************************************************************************************
	* Class				:specsheet
	* Method			:getReserveDetail
	* Description		:顧客予約情報（スペックシート内データ）検索
	* Custom Attributes	:reserveID	予約番号（Numeric）
	* Return Paramters	:
	* History			:1) Coded by	2004/10/06 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getReserveDetail" access="remote" returntype="any" hint="">
		<cfargument name="reserve_code" type="numeric" default="">
		<cfargument name="room_code" type="numeric" default="">
		<cfargument name="referencenumber" type="String" default="">
		<cfargument name="MCID" type="numeric" default="0">
		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<!--- 戻値作成 --->
			<cfset retStruct =StructNew()>
			<!--- CS回答 --->
			
				<!--- 
				select	 AD.questionnaire_id
						,AD.answer
						,AD.answertext
						,convert(varchar(10),A.arrive_date,111) as arrive_date
						,convert(varchar(10),A.depart_date,111) as depart_date
						,convert(varchar(10),A.registrated,111) as registrated
				from	 #Application.app00.dbu#.hst_answers as A
						,#Application.app00.dbu#.rtt_reserve as R
						,#Application.app00.dbu#.hst_answersdetails as AD
				where	 R.referencenumber = '#arguments.referencenumber#'
				and		 R.referencenumber = A.referencenumber
				and		 R.referencenumber = AD.referencenumber
				 --->
				 <!--- 
				 
				  --->


			<cfquery name="getAnswer" datasource = "#Application.datasource#">
				select	 a.questionnaire_id
						,a.answer
						,a.answertext
						,convert(varchar(10),a.arrive_date,111) as arrive_date
						,convert(varchar(10),a.depart_date,111) as depart_date
						,convert(varchar(10),b.registrated,111) as registrated
				from	 #Application.app00.dbu#.v_getAnswers as a
						,#Application.app00.dbu#.hst_answers as b
				where	 a.referencenumber = '#arguments.referencenumber#'
				and		 a.referencenumber = b.referencenumber
				group by a.questionnaire_id
						,a.answer
						,a.answertext
						,convert(varchar(10),a.arrive_date,111)
						,convert(varchar(10),a.depart_date,111)
						,convert(varchar(10),b.registrated,111)
			</cfquery>
			<!--- 
			<cfquery name="getShopresult" datasource = "#Application.datasource#">
				select
					shop_id,
					shop_nm,
					amount
				from
					#Application.app00.dbu#.rtt_shopresult
				where
					reserve_code = #arguments.reserve_code#
				and
					room_code = #arguments.room_code#
				and
					amount != 0
				order by
					shop_id
			</cfquery>
			 --->
			<cfset getShopresult = querynew("shop_id,shop_nm,amount")>
			<cfset queryaddrow(getShopresult,1)>
			<cfset StructInsert(retStruct, "answersQuery", #getAnswer#)>
			<cfset StructInsert(retStruct, "useQuery", #getShopresult#)>
			<cfset StructInsert(retStruct, "RecNum", #arguments.MCID#)>

			<cfreturn retStruct>
			<cfsetting enablecfoutputonly="No">
			
			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<!---
	*************************************************************************************************
	* Class				: specsheet
	* Method			: getEnquete
	* Description		: アンケートマスタ情報取得
	* Custom Attributes	: questionnairesheet_id		シートID
	* 					: 
	* Return Paramters	: getEnqueteMaster(Query)
	* History			: 1) Coded by	2004/10/08 h.miyahara(NetFusion)
	*					: 2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getEnquete" access="remote" returntype="query" >
		<cfargument name="resort_code" type="Any" default="14">
		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<cfquery name="getEnquete" datasource = "#Application.datasource#">
				select top 1 questionnairesheet_id as sheet_id
				from 	 #Application.app00.dbu#.mst_questionnairesheet 
				where	 resort_code = #arguments.resort_code#
				and		 start_date <= getdate()
				and		 active_flg = 1
				order by start_date desc
			</cfquery>
				
			<cfreturn getEnquete>
			<cfsetting enablecfoutputonly="No">
			
			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<!---
	*************************************************************************************************
	* Class				: specsheet
	* Method			: getUserEnquete
	* Description		:
	* Custom Attributes	: 
	* 					: 
	* Return Paramters	: getEnqueteMaster(Query)
	* History			: 1) Coded by	2004/10/08 h.miyahara(NetFusion)
	*					: 2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getUserEnquete" access="remote" returntype="query" >
		<cfargument name="SummaryID" type="numeric" default="1">
		<cfargument name="referencenumber" type="String" default="">
		<cfargument name="resort_code" type="Any" default="14">
		
		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<cfquery name="getAnswer" datasource = "#Application.datasource#">
				select	 questionnairesheet_id
				from	 #Application.app00.dbu#.hst_answers
				where	 referencenumber = '#arguments.referencenumber#'
			</cfquery>
			<cfif getAnswer.recordcount NEQ 0>
				<cfset mastersheet = #getAnswer.questionnairesheet_id#>
			<cfelse>
				<cfquery name="getMaster" datasource = "#Application.datasource#">
					select top 1
							 a.questionnairesheet_id
					from 	 #Application.app00.dbu#.mst_questionnairesheet a 
					where	 (select depart_date from #Application.app00.dbu#.rtt_reserve where referencenumber = '#arguments.referencenumber#') between a.start_date and a.end_date
					and		 a.resort_code = #arguments.resort_code#
					and		 a.active_flg = 1
					order by a.start_date desc
				</cfquery>
				<cfset mastersheet = #getMaster.questionnairesheet_id#>
			</cfif>	
			<cfquery name="getUserEnquete" datasource = "#Application.datasource#">
				select	 a.questionnaire_id
						,a.questionnairesheet_id
						,a.viewtype
						,a.questionnaire_nm
						,a.questionnaire_type
						,a.category_nm as questionnairecategory_nm
						,a.category_id as questionnairecategory_id
						,a.seq
				from	 #Application.app00.dbu#.v_getEnquete_all as a
				where	 a.resort_code = #arguments.resort_code#
				and		 a.questionnairesheet_id = #mastersheet#
			</cfquery>
			<cfreturn getUserEnquete>
			<cfsetting enablecfoutputonly="No">
			
			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<!---
	*************************************************************************************************
	* Class				:specsheet
	* Method			:sweeper
	* Description		:ValueInformation登録事前処理
	* Custom Attributes	:reserve_id			予約ID
	* Return Paramters	:
	* History			:1) Coded by	2004/12/13 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="sweeper" access="remote" returntype="any">
		<cftry>

		<cftransaction action="BEGIN">
			<cfquery name="chksweep" datasource = "#Application.datasource#">
				select
					updatedby
				from 
					#Application.app00.dbu#.hst_valueinformation
				where
					updatedby = '#cgi.remote_addr#'
				and
					checkout = 1	
			</cfquery>
			
			<!--- チェックイン処理 --->
			<cfif chksweep.recordCount NEQ 0>
				<cfloop query="chksweep">
					<cfquery name="checkIn" datasource = "#Application.datasource#">
						UPDATE
							#Application.app00.dbu#.hst_valueinformation
						SET
							checkout = 0,
							updatedby = '#cgi.remote_addr#',
							updated = #now()#
						where
							updatedby = '#updatedby#'
					</cfquery>
				</cfloop>
			</cfif>

		</cftransaction>

		<cfscript>
			executeinformation = StructNew();
			StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
			StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
		</cfscript>

		<cfreturn executeinformation>
		<cfcatch type="any">
		<cftransaction action="rollback"/>
<!--- 
		<cf_error_log
			label = "setUserAnswers"
			errorType = "#cfcatch.type#"
			message = "#cfcatch.message#"
			detail = "#cfcatch.detail#">
		<cfsetting enablecfoutputonly="No">
 --->
 		<cfrethrow>
	</cfcatch>
	</cftry>
	</cffunction>

	<!---
	*************************************************************************************************
	* Class				:specsheet
	* Method			:getValueInformation
	* Description		:ValueInformation登録事前処理
	* Custom Attributes	:reserve_id			予約ID
	* Return Paramters	:
	* History			:1) Coded by	2004/12/13 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getValueInformation" access="remote" returntype="any">
		<cfargument name="reserve_code" type="Numeric" default="">
		<cfargument name="room_code" type="Numeric" default="">
		<cfargument name="target_id" type="Numeric" default="">
		<cfargument name="resort_code" type="Numeric" default="">
		<cftry>

		<cftransaction action="BEGIN">
			<cfquery name="chkValueInformation" datasource = "#Application.datasource#">
				select
					reserve_code,
					valueinfo,
					checkout,
					updatedby
				from 
					#Application.app00.dbu#.hst_valueinformation
				where
					reserve_code = #arguments.reserve_code#
				and
					room_code = #arguments.room_code#
				and
					resort_code = #arguments.resort_code#
			</cfquery>

			<!--- チェックアウト確認 編集可能--->
			<cfif chkValueInformation.checkout NEQ 1>
				<cfscript>
					executeinformation = StructNew();
					StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
					StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
					StructInsert(executeinformation,	"executeFlg",	0	);
					StructInsert(executeinformation,	"target_id",	#arguments.target_id#	);
					StructInsert(executeinformation,	"executeItem",	#chkValueInformation.valueinfo#	);
					StructInsert(executeinformation,	"reserve_code",	#arguments.reserve_code#	);
					StructInsert(executeinformation,	"room_code",	#arguments.room_code#	);
				</cfscript>
			<!--- チェックアウト確認 編集不可--->
			<cfelse>
				<cfscript>
					executeinformation = StructNew();
					StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
					StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
					StructInsert(executeinformation,	"target_id",	#arguments.target_id#	);
					StructInsert(executeinformation,	"executeFlg",	1	);
					StructInsert(executeinformation,	"executeItem",	#chkValueInformation.valueinfo#	);
					StructInsert(executeinformation,	"checkoutuser",	#cgi.remote_addr#	);
					StructInsert(executeinformation,	"reserve_code",	#arguments.reserve_code#	);
					StructInsert(executeinformation,	"room_code",	#arguments.room_code#	);
				</cfscript>
			</cfif>
			
			<!--- チェックアウト処理 --->
			<cfif chkValueInformation.recordCount EQ 0>
				<cfquery name="setValueInformation" datasource = "#Application.datasource#">
					INSERT INTO
						#Application.app00.dbu#.hst_valueinformation(
							reserve_code,
							room_code,
							resort_code,
							checkout,
							updatedby,
							updated
						)
					VALUES(
							#arguments.reserve_code#,
							#arguments.room_code#,
							#arguments.resort_code#,
							0,
							'#cgi.remote_addr#',
							#Now()#
					)
				</cfquery>
			<cfelse>
				<cfif chkValueInformation.checkout NEQ 1>
					<cfquery name="checkOut" datasource = "#Application.datasource#">
						UPDATE
							#Application.app00.dbu#.hst_valueinformation
						SET
							checkout = 0,
							updatedby = '#cgi.remote_addr#',
							updated = #now()#
						WHERE
							reserve_code = #arguments.reserve_code#
						and
							room_code = #arguments.room_code#
						and
							resort_code = #arguments.resort_code#
					</cfquery>
				</cfif>
			</cfif>
		</cftransaction>
		<cfreturn executeinformation>
		<cfcatch type="any">
		<cftransaction action="rollback"/>
<!--- 
		<cf_error_log
			label = "setUserAnswers"
			errorType = "#cfcatch.type#"
			message = "#cfcatch.message#"
			detail = "#cfcatch.detail#">
		<cfsetting enablecfoutputonly="No">
 --->
 		<cfrethrow>
	</cfcatch>
	</cftry>
	</cffunction>

	<!---
	*************************************************************************************************
	* Class				:specsheet
	* Method			:checkIn
	* Description		:ValueInformation登録事前処理
	* Custom Attributes	:reserve_id			予約ID
	* Return Paramters	:
	* History			:1) Coded by	2004/12/13 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="checkIn" access="remote" returntype="any">
		<cfargument name="reserve_code" type="Numeric" default="">
		<cfargument name="room_code" type="Numeric" default="">
		<cfargument name="resort_code" type="Numeric" default="">
		<cftry>

		<cftransaction action="BEGIN">
			<cfquery name="chkValueInformation" datasource = "#Application.datasource#">
				select
					reserve_code
				from 
					#Application.app00.dbu#.hst_valueinformation
				where
					reserve_code = #arguments.reserve_code#
				and
					room_code = #arguments.room_code#	
				and
					resort_code = #arguments.resort_code#
			</cfquery>
			<!--- チェックイン処理 --->
			<cfif chkValueInformation.recordCount EQ 0>
				<cfquery name="setValueInformation" datasource = "#Application.datasource#">
					INSERT INTO
						#Application.app00.dbu#.hst_valueinformation(
							reserve_code,
							room_code,
							resort_code,
							checkout,
							updatedby,
							updated
						)
					VALUES(
							#arguments.reserve_code#,
							#arguments.room_code#,
							#arguments.resort_code#,
							0,
							'#cgi.remote_addr#',
							#Now()#
					)
				</cfquery>
			<cfelse>
				<cfquery name="setValueInformation" datasource = "#Application.datasource#">
					UPDATE
						#Application.app00.dbu#.hst_valueinformation
					SET
						checkout = 0,
						updatedby = '#cgi.remote_addr#',
						updated = #now()#
					WHERE
						reserve_code = #arguments.reserve_code#
					and
						room_code = #arguments.room_code#	
					and
						resort_code = #arguments.resort_code#
				</cfquery>
			</cfif>

		</cftransaction>

		<cfscript>
			executeinformation = StructNew();
			StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
			StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
		</cfscript>

		<cfreturn executeinformation>
		<cfcatch type="any">
		<cftransaction action="rollback"/>
<!--- 
		<cf_error_log
			label = "setUserAnswers"
			errorType = "#cfcatch.type#"
			message = "#cfcatch.message#"
			detail = "#cfcatch.detail#">
		<cfsetting enablecfoutputonly="No">
 --->
 		<cfrethrow>
	</cfcatch>
	</cftry>
	</cffunction>



	<!---
	*************************************************************************************************
	* Class				:specsheet
	* Method			:setValueInformation
	* Description		:ValueInformation登録
	* Custom Attributes	:reserve_id			予約ID
	* 					:valueInformation	valueInformation
	* 					:target_id			戻り値対象ＭＣＩＤ
	* Return Paramters	:
	* History			:1) Coded by	2004/12/13 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="setValueInformation" access="remote" returntype="any">
		<cfargument name="reserve_code" type="Numeric" default="">
		<cfargument name="room_code" type="Numeric" default="">
		<cfargument name="valueInformation" type="String" default="">
		<cfargument name="target_id" type="Numeric" default="">
		<cfargument name="resort_code" type="Numeric" default="">
		<cftry>
		<cftransaction action="BEGIN">
			<cfquery name="chkValueInformation" datasource = "#Application.datasource#">
				select
					reserve_code,
					room_code,
					valueinfo
				from 
					#Application.app00.dbu#.hst_valueinformation
				where
					reserve_code = #arguments.reserve_code#
				and
					room_code = #arguments.room_code#
				and
					resort_code = #arguments.resort_code#
			</cfquery>

			<cfif chkValueInformation.recordcount EQ 0>
				<cfquery name="setValueInformation" datasource = "#Application.datasource#">
					INSERT INTO
						#Application.app00.dbu#.hst_valueinformation(
							reserve_code,
							room_code,
							resort_code,
							valueinfo,
							checkout,
							updatedby,
							updated
						)
					VALUES(
							#arguments.reserve_code#,
							#arguments.room_code#,
							#arguments.resort_code#,
							'#arguments.valueInformation#',
							0,
							'#cgi.remote_addr#',
							#Now()#
					)
				</cfquery>
			<cfelse>
			<!---  --->
				<cfquery name="setValueInformation" datasource = "#Application.datasource#">
					UPDATE
						#Application.app00.dbu#.hst_valueinformation
					SET
						valueinfo = '#arguments.valueInformation#',
						lastdata = '#chkValueInformation.valueinfo#',
						checkout = 0,
						updatedby = '#cgi.remote_addr#',
						updated = #now()#
					WHERE
						reserve_code = #arguments.reserve_code#
					and
						room_code = #arguments.room_code#
					and
						resort_code = #arguments.resort_code#
				</cfquery>
			</cfif>
		</cftransaction>
		<!--- 戻値設定 --->		
		<cfscript>
			executeinformation = StructNew();
			StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
			StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
			StructInsert(executeinformation,	"executeItem",	#arguments.valueInformation#	);
			StructInsert(executeinformation,	"target_id",	#arguments.target_id#	);
			StructInsert(executeinformation,	"errorMsg",		"ValueInformationが登録されました"	);
		</cfscript>
		<cfreturn executeinformation>
		<cfcatch type="any">
		<cftransaction action="rollback"/>
<!--- 
		<cf_error_log
			label = "setUserAnswers"
			errorType = "#cfcatch.type#"
			message = "#cfcatch.message#"
			detail = "#cfcatch.detail#">
		<cfsetting enablecfoutputonly="No">
 --->
 		<cfrethrow>
	</cfcatch>
	</cftry>
	</cffunction>
	<!---
	*************************************************************************************************
	* Class				:specsheet
	* Method			:setRollBack
	* Description		:ValueInformationのロールバック
	* Custom Attributes	:reserve_id			予約ID
	* 					:target_id			戻り値対象ＭＣＩＤ
	* Return Paramters	:
	* History			:1) Coded by	2004/12/13 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="setRollBack" access="remote" returntype="any">
		<cfargument name="reserve_code" type="Numeric" default="">
		<cfargument name="room_code" type="Numeric" default="">
		<cfargument name="target_id" type="Numeric" default="">
		<cfargument name="resort_code" type="Numeric" default="">
		<cftry>

		<cftransaction action="BEGIN">
			<cfquery name="chkValueInformation" datasource = "#Application.datasource#">
				select
					reserve_code,
					room_code,
					lastdata,
					valueinfo
				from 
					#Application.app00.dbu#.hst_valueinformation
				where
					reserve_code = #arguments.reserve_code#
				and
					room_code = #arguments.room_code#
				and
					resort_code = #arguments.resort_code#
			</cfquery>
			<cfquery name="setValueInformation" datasource = "#Application.datasource#">
				UPDATE
					#Application.app00.dbu#.hst_valueinformation
				SET
					valueinfo = '#chkValueInformation.lastdata#',
					lastdata = '#chkValueInformation.valueinfo#',
					updatedby = '#cgi.remote_addr#',
					updated = #now()#
				WHERE
					reserve_code = #arguments.reserve_code#
				and
					room_code = #arguments.room_code#
				and
					resort_code = #arguments.resort_code#
			</cfquery>
		</cftransaction>
		<!--- 戻値設定 --->
		<cfscript>
			executeinformation = StructNew();
			StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
			StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
			StructInsert(executeinformation,	"executeItem",	#chkValueInformation.lastdata#	);
			StructInsert(executeinformation,	"target_id",	#arguments.target_id#	);
			StructInsert(executeinformation,	"errorMsg",		"登録前の状態へ戻しました。"	);
		</cfscript>
		<cfreturn executeinformation>
		<cfcatch type="any">
		<cftransaction action="rollback"/>
<!--- 
		<cf_error_log
			label = "setUserAnswers"
			errorType = "#cfcatch.type#"
			message = "#cfcatch.message#"
			detail = "#cfcatch.detail#">
		<cfsetting enablecfoutputonly="No">
 --->
 		<cfrethrow>
	</cfcatch>
	</cftry>
	</cffunction>
	<!---
	*************************************************************************************************
	* Class				:specsheet
	* Method			:getRepeatCS
	* Description		:リピーターＣＳ回答情報取得
	* Custom Attributes	:reserve_code	予約コード
	* Return Paramters	:
	* History			:1) Coded by	2005/07/06 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getRepeatCS" access="remote" returntype="any">
		<cfargument name="referencenumber" type="String" default="">
		<cftry>
			<cfquery name="getUserProfile" datasource = "#Application.datasource#">
				select
					C.area_code,
					R.arrive_date,
					R.depart_date,
					R.referencenumber
				from
					#Application.app00.dbu#.rtm_customer as C,
					#Application.app00.dbu#.rtt_reserve as R
				where
					R.customernumber = C.customernumber
				and
					R.referencenumber = '#arguments.referencenumber#'
			</cfquery>
			<!--- 回答情報取得 --->
			<cfquery name="getAnswers" datasource = "#Application.datasource#">
				select
					referencenumber
				from
					#Application.app00.dbu#.hst_answers
				where
					referencenumber = '#arguments.referencenumber#'
			</cfquery>
			<cfif getAnswers.recordcount NEQ 0>
				<cfquery name="getAnswersdetails" datasource = "#Application.datasource#">
					select
						questionnaire_id,
						answertext
					from
						#Application.app00.dbu#.hst_answersdetails
					where
						referencenumber = '#arguments.referencenumber#'
					and
						questionnairesheet_id = 0
					and
						questionnaire_id IN(#Application.repeatcs_q1#,#Application.repeatcs_q2#,#Application.repeatcs_q3#)
				</cfquery>
				<cfset QueryAddColumn(getUserProfile,"a_1",ArrayNew(1))>
				<cfset QueryAddColumn(getUserProfile,"a_2",ArrayNew(1))>
				<cfset QueryAddColumn(getUserProfile,"a_3",ArrayNew(1))>
				<cfloop query="getAnswersdetails">
					<cfif questionnaire_id EQ Application.repeatcs_q1>
						<cfset QuerySetCell(getUserProfile,"a_1",#answertext#)>
					</cfif>	
					<cfif questionnaire_id EQ Application.repeatcs_q2>
						<cfset QuerySetCell(getUserProfile,"a_2",#answertext#)>
					</cfif>	
					<cfif questionnaire_id EQ Application.repeatcs_q3>
						<cfset QuerySetCell(getUserProfile,"a_3",#answertext#)>
					</cfif>	
				</cfloop>
			<cfelse>
				<cfset tmp_array = ArrayNew(1)>
				<cfset tmp_array[1] = "">
				<cfset QueryAddColumn(getUserProfile,"a_1",tmp_array)>
				<cfset QueryAddColumn(getUserProfile,"a_2",tmp_array)>
				<cfset QueryAddColumn(getUserProfile,"a_3",tmp_array)>
			</cfif>
			
			<cfreturn getUserProfile>
		<cfcatch type="any">
		<cftransaction action="rollback"/>
 		<cfrethrow>
	</cfcatch>
	</cftry>
	</cffunction>
	
	<!---
	*************************************************************************************************
	* Class				:specsheet
	* Method			:setRepeatCS
	* Description		:リピーターＣＳの登録
	* Custom Attributes	:referencenumber	予約コード
	* 					:a1				回答値１
	* 					:a2				回答値２
	* 					:a3				回答値３
	* Return Paramters	:
	* History			:1) Coded by	2005/07/06 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="setRepeatCS" access="remote" returntype="any">
		<cfargument name="referencenumber" type="String" default="">
		<cfargument name="area_code" type="Numeric" default="">
		<cfargument name="arrive_date" type="String" default="">
		<cfargument name="depart_date" type="String" default="">
		<cfargument name="a1" type="String" default="">
		<cfargument name="a2" type="String" default="">
		<cfargument name="a3" type="String" default="">
		<cftry>
		<cfset questionnairesheet_id = 0>
		<cfset tmp_answers = ArrayNew(1)>
		<cfset ArrayAppend(tmp_answers,#Application.repeatcs_q1#)>
		<cfset ArrayAppend(tmp_answers,#Application.repeatcs_q2#)>
		<cfset ArrayAppend(tmp_answers,#Application.repeatcs_q3#)>
		<cfloop from="1" to="#ArrayLen(tmp_answers)#" index="i">
			<cfscript>
			answers = StructNew();
			StructInsert(answers, "questionnaire_id", #tmp_answers[i]#);
			StructInsert(answers, "answertext", #Evaluate("a"&i)#);
			tmp_answers[i] = answers;
			</cfscript>
		</cfloop>
		
		<cftransaction action="BEGIN">
			<!--- 回答情報取得 --->
			<cfquery name="getAnswers" datasource = "#Application.datasource#">
				select
					referencenumber
				from
					#Application.app00.dbu#.hst_answers
				where
					referencenumber = '#arguments.referencenumber#'
			</cfquery>
		
			<cfif getAnswers.recordcount EQ 0>
				<cfquery name="setAnswers" datasource = "#Application.datasource#">
					insert into
						#Application.app00.dbu#.hst_answers(
							referencenumber,
							questionnairesheet_id,
							registrated,
							arrive_date,
							depart_date,
							area_code
					)values(
						'#arguments.referencenumber#',
						#questionnairesheet_id#,
						'#DateFormat(Now(),'YYYY/MM/DD')#',
						'#arguments.arrive_date#',
						'#arguments.depart_date#',
						#arguments.area_code#
					)
				</cfquery>
				<cfloop from="1" to="#ArrayLen(tmp_answers)#" index="i">

					<cfset questionnaire_id = "#tmp_answers[i].questionnaire_id#">
					<cfset answer = "0">
					<cfset answertext = "#tmp_answers[i].answerText#">

					<cfquery name="setAnswersdetails" datasource = "#Application.datasource#">
						insert into
							#Application.app00.dbu#.hst_answersdetails(
								referencenumber,
								questionnairesheet_id,
								questionnaire_id,
								answer,
								answertext
						)values(
							'#arguments.referencenumber#',
							#questionnairesheet_id#,
							#questionnaire_id#,
							#answer#,
							'#answertext#'
						)
					</cfquery>
				</cfloop>				
			<cfelse>
				<cfloop from="1" to="#ArrayLen(tmp_answers)#" index="i">

					<cfset questionnaire_id = "#tmp_answers[i].questionnaire_id#">
					<cfset answer = "0">
					<cfset answertext = "#tmp_answers[i].answerText#">

					<cfquery name="setAnswersdetails" datasource = "#Application.datasource#">
						update
							#Application.app00.dbu#.hst_answersdetails
						set
							answertext = '#answertext#'
						where
							referencenumber = '#arguments.referencenumber#'
						and
							questionnairesheet_id = #questionnairesheet_id#
						and
							questionnaire_id = #questionnaire_id#
					</cfquery>

				</cfloop>
			</cfif>		
		</cftransaction>
		<!--- 戻値設定 --->
		<cfscript>
			executeinformation = StructNew();
			StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
			StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
			StructInsert(executeinformation,	"errorMsg",		"登録しました。"	);
		</cfscript>
		<cfreturn executeinformation>
		<cfcatch type="any">
		<cftransaction action="rollback"/>
<!--- 
		<cf_error_log
			label = "setUserAnswers"
			errorType = "#cfcatch.type#"
			message = "#cfcatch.message#"
			detail = "#cfcatch.detail#">
		<cfsetting enablecfoutputonly="No">
 --->
 		<cfrethrow>
	</cfcatch>
	</cftry>
	</cffunction>


	<!---
	*************************************************************************************************
	* Class				:specsheet
	* Method			:getcsurl
	* Description		:ＷＥＢＣＳＵＲＬ取得
	* Custom Attributes	:referencenumber	照会用番号
	* Return Paramters	:
	* History			:1) Coded by	2005/12/21 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getcsurl" access="remote" returntype="any">
		<cfargument name="referencenumber" type="String" default="">
		<cfset wk_cd_s = Left(CreateUUID(),4)>
		<cfset wk_cd_e = Right(CreateUUID(),4)>
		<cfset csurl = #Application.CSURL#&#wk_cd_s#&#arguments.referencenumber#&#wk_cd_e# >

		<!--- 戻値設定 --->
		<cfscript>
			executeinformation = StructNew();
			StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
			StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
			StructInsert(executeinformation,	"errorMsg",		"登録しました。"	);
			StructInsert(executeinformation,	"csurl",		#csurl#	);
		</cfscript>
		<cfreturn executeinformation>
	</cffunction>

</cfcomponent>
