﻿<cfcomponent hint="ＯＡ検索情報取得ＣＦＣ">
	<!---
	*************************************************************************************************
	* Class				:oasearch
	* Method			:getCategory
	* Description		:カテゴリリスト取得
	* Custom Attributes	:none
	* Return Paramters	:
	* History			:1) Coded by	2004/10/13 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getCategory" access="remote" returntype="query" hint="">
		<cfargument name="resort_code" type="String" default="0">
		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<cfif arguments.resort_code eq 0>
				<!--- 全施設取得 --->
				<cfquery name="getAllresort" datasource = "#Application.datasource#">
					select a.resort_code from  #Application.app00.dbu#.mst_resort as a
				</cfquery>
				<cfset targetresort = valuelist(getAllresort.resort_code)>
			<cfelse>
				<cfset targetresort = arguments.resort_code>
			</cfif>

			<!--- 現在アクティブなカテゴリ --->
			<cfquery name="getcategorylist" datasource = "#Application.datasource#">
				<cfset cnt=1>
				<cfloop list="#targetresort#" index="rcd">
					<cfif cnt eq 1>
						select	 a.questionnairecategory_id
								,a.questionnairecategory_nm
								<cfif listlen(targetresort) eq 1>
								,case
									when	 max(c.cs_qlabel) <> min(c.cs_qlabel) 
									then	 min(c.cs_qlabel) + '.' +  replace(max(c.cs_qlabel),'Q','') 
								 	else	 max(c.cs_qlabel)
								 end cs_qlabel
								<cfelse>
								,'Q' as cs_qlabel
								</cfif>
								,a.seq
						from			 #Application.app00.dbu#.mst_questionnairecategory as a
						inner join	 #Application.app00.dbu#.mst_questionnaire as b
						on			 a.questionnairecategory_id = b.questionnairecategory_id
						inner join	  #Application.app00.dbu#.oth_sheetconfiguration as c
						on			 c.questionnaire_id = b.questionnaire_id
						where		 a.active_flg = 1
						and		 	 b.questionnaire_type = 2
						and			 c.questionnairesheet_id = (select top 1 questionnairesheet_id from #Application.app00.dbu#.mst_questionnairesheet where resort_code = #rcd# and questionnairesheet_id != 0 and getdate() between start_date and end_date )
						and			 c.resort_code = #rcd#
						group by a.questionnairecategory_id,a.questionnairecategory_nm,a.seq
					<cfelse>
						union
						select	 a.questionnairecategory_id
								,a.questionnairecategory_nm
								,'Q' as cs_qlabel
								,a.seq
						from			 #Application.app00.dbu#.mst_questionnairecategory as a
						inner join	 #Application.app00.dbu#.mst_questionnaire as b
						on			 a.questionnairecategory_id = b.questionnairecategory_id
						inner join	  #Application.app00.dbu#.oth_sheetconfiguration as c
						on			 c.questionnaire_id = b.questionnaire_id
						where		 a.active_flg = 1
						and		 	 b.questionnaire_type = 2
						and			 c.questionnairesheet_id = (select top 1 questionnairesheet_id from #Application.app00.dbu#.mst_questionnairesheet where resort_code = #rcd# and questionnairesheet_id != 0 and getdate() between start_date and end_date )
						and			 c.resort_code = #rcd#
						group by a.questionnairecategory_id,a.questionnairecategory_nm,a.seq
					</cfif>
					<cfset cnt = cnt+1>
				</cfloop>
						order by a.seq
			</cfquery>

			<!--- 過去に設定されていたカテゴリ --->
			<cfquery name="getOldCategoryList" datasource = "#Application.datasource#">
				select	 distinct a.questionnairecategory_id
						,a.questionnairecategory_nm
						,'(旧)' as cs_qlabel
				from	 #Application.app00.dbu#.mst_questionnairecategory  as a
						,#Application.app00.dbu#.mst_questionnaire as b
						,#Application.app00.dbu#.oth_sheetconfiguration as c
				where	 a.active_flg = 1
				and		 b.questionnaire_type = 2
				and		 a.questionnairecategory_id = b.questionnairecategory_id
				and		 b.questionnaire_id = c.questionnaire_id
				and		 a.questionnairecategory_id not in (
							#valuelist(getcategorylist.questionnairecategory_id)#
						)
				and		 c.resort_code in (#targetresort#)
				group by	 a.questionnairecategory_id
							,a.questionnairecategory_nm
			</cfquery>

			<cfset retq = querynew("#lcase(getcategorylist.columnlist)#,q_contents")>
			<cfloop query="getCategoryList">
				<cfset queryaddrow(retq,1)>
				<cfset querysetcell(retq,"questionnairecategory_id",questionnairecategory_id)>
				<cfset querysetcell(retq,"questionnairecategory_nm",questionnairecategory_nm)>
				<cfset querysetcell(retq,"cs_qlabel",cs_qlabel)>
				<!--- カテゴリ取得 --->
				<cfinvoke method = "getContents" returnVariable ="r_contents">
					<cfinvokeargument name="questionnairecategory_id" value = #questionnairecategory_id[currentrow]#>
				</cfinvoke>
				<cfset querysetcell(retq,"q_contents",r_contents)>
			</cfloop>

			<cfloop query="getOldCategoryList">
				<cfset queryaddrow(retq,1)>
				<cfset querysetcell(retq,"questionnairecategory_id",questionnairecategory_id)>
				<cfset querysetcell(retq,"questionnairecategory_nm",questionnairecategory_nm)>
				<cfset querysetcell(retq,"cs_qlabel",cs_qlabel)>
				<!--- カテゴリ取得 --->
				<cfinvoke method = "getContents" returnVariable ="r_contents">
					<cfinvokeargument name="questionnairecategory_id" value = #questionnairecategory_id[currentrow]#>
				</cfinvoke>
				<cfset querysetcell(retq,"q_contents",r_contents)>
			</cfloop>

			<cfreturn retq>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>

	</cffunction>
	

	<!---
	*************************************************************************************************
	* Class				:oasearch
	* Method			:getContents
	* Description		:サマリ削除（論理削除） 
	* Custom Attributes	:questionnairecategory_id	カテゴリID
	* Return Paramters	:
	* History			:1) Coded by	2004/10/13 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getContents" access="remote" returntype="any">
		<cfargument name="questionnairecategory_id" type="numeric" default="1">
		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<cfquery name="getContentsList" datasource = "#Application.datasource#">
				select	 questionnaire_id
						,questionnaire_nm
						,questionnairecategory_id
				from 	#Application.app00.dbu#.mst_questionnaire
				where 	questionnairecategory_id = #arguments.questionnairecategory_id#
				and 	questionnaire_type = 2
				and 	active_flg = 1
			</cfquery>
			
			<cfreturn getContentsList>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
	
	</cffunction>	

	<!---
	*************************************************************************************************
	* Class				:oasearch
	* Method			:getOAAnswers
	* Description		:回答値取得
	* Custom Attributes	:questionnaire_id	設問ID
	* Return Paramters	:
	* History			:1) Coded by	2004/10/13 h.miyahara(NetFusion)
	*					:2) Updated by	2005/10/26  h.miyahara(NetFusion) デフォルトソート順変更
	*					:3) Updated by	2005/10/26  h.miyahara(NetFusion) 満足度評価設問追加
	*					:4) Updated by	2005/10/26  h.miyahara(NetFusion) 対象カテゴリ設問追加
	*************************************************************************************************
	--->
	<cffunction name="getOAAnswers" access="remote" returntype="any">
		<cfargument name="dcondtion" type="Boolean" default="false">
		<cfargument name="selecteddays" type="Array" default="">
		<cfargument name="arg_fieldselect" type="Numeric" default="0">	<!--- 基準日 --->

		<cfargument name="questionnaire_id" type="Any" default="">
		<cfargument name="questionnaire_nm" type="Any" default="">
		<cfargument name="questionnairecategory_id" type="numeric" default="">

		<cfargument name="arg_group_id" type="Numeric" default="0">	<!--- 団体（0=全部、1=個人、2=団体 'zzz*****'）--->
		<cfargument name="arg_answermethod_id" type="Numeric" default="0">	<!--- 団体（0=指定なし、1=手渡し、2=Web）--->

		<cfargument name="arg_resort_code" type="Any" default="">
		<cfargument name="arg_cross_id" type="Numeric" default="0">
		<cfargument name="arg_generation" type="Any" default="">	<!--- 年代 --->
		<cfargument name="arg_gender" type="Any" default="">		<!--- 性別 --->
		<cfargument name="arg_job" type="Any" default="">			<!--- 職業 --->
		<cfargument name="arg_times" type="Any" default="">			<!--- 旅行回数 --->

		<cfargument name="arg_nights" type="Any" default="">		<!--- 宿泊数 --->		
		<cfargument name="arg_together" type="Any" default="">		<!--- 同行者 --->
		<cfargument name="arg_area" type="Any" default="">			<!--- 居住県 --->
		<cfargument name="arg_roomtype" type="Any" default="">		<!--- ルームタイプ --->
		<cfargument name="arg_purpose" type="Any" default="">		<!--- 旅行目的 --->
		<cfargument name="arg_reservation" type="Any" default="">	<!--- 予約経路 --->
		<cfargument name="arg_cognition" type="Any" default="">		<!--- 認知経路 --->
		<cfargument name="arg_roomnumber" type="Any" default="">	<!--- ルーム№ --->
		<cfargument name="arg_residence" type="Any" default="">		<!--- INB用居住地 --->

		<cfargument name="viewchange" type="Boolean" default="false">

		<cfsetting enablecfoutputonly="yes">
		<cftry>
			
			<!--- リピーターCSの場合 --->
			<cfif arguments.questionnairecategory_id eq 0>
				<cfset wk_qid = arraynew(1)>
				<cfset arrayappend(wk_qid,200000001)>
				<cfset arrayappend(wk_qid,200100001)>
				<cfset arrayappend(wk_qid,300000001)>
				<cfset arguments.questionnaire_id = wk_qid>
				<cfset wk_qnm = arraynew(1)>
				<cfset arrayappend(wk_qnm,"100点満点")>
				<cfset arrayappend(wk_qnm,"感想と要望")>
				<cfset arrayappend(wk_qnm,"私共にできること")>
				<cfset arguments.questionnaire_nm = wk_qnm>
			</cfif>
				
				
			<!--- ＯＡ条件整形 --->
			<cfif IsArray(arguments.questionnaire_id) and IsArray(arguments.questionnaire_nm)>
				<cfset arguments.questionnaire_id = ArrayToList(arguments.questionnaire_id)>
				<cfset arguments.questionnaire_nm = ArrayToList(arguments.questionnaire_nm)>
				<cfset tmp_questionnaire_nm = "">
				<cfloop from="1" to="#ListLen(arguments.questionnaire_nm)#" index="i">
					<cfset tmp_questionnaire_nm = ListAppend(tmp_questionnaire_nm,"OA"&(i-1))>
				</cfloop>
			<cfelse>
				<cfset tmp_questionnaire_nm = "OA0">
			</cfif>	

			<!--- 評価設問追加 B --->
			<cfinvoke method = "getCategoryQs" questionnairecategory_id="#arguments.questionnairecategory_id#" arg_resort_code="#arg_resort_code#" returnVariable="CategoryQs">
			
			<cfset temp_qlist = ValueList(CategoryQs.questionnaire_id)>
			<cfset temp_nlist = ValueList(CategoryQs.questionnaire_nm)>
			<cfset add_questionnaire_nm = "">
			<cfset add_questionnaire_sortkey = "">
			<cfloop from="1" to="#ListLen(temp_qlist)#" index="i">
				<cfset add_questionnaire_nm = ListAppend(add_questionnaire_nm,"Q"&(i-1))>
				<cfset add_questionnaire_sortkey = ListAppend(add_questionnaire_sortkey,"Sort"&(i-1))>
			</cfloop>
			<!--- 評価設問追加 E --->

			<cfset wk_field = "
				reserve_code, 
				referencenumber,
				customernumber, 
				orgdepart_date, 
				depart_date, 
				gender_nm,
				age_nm,
				companion_nm,
				job_nm,
				trip_nm,
				numPerson,
				nights,
				area, 
				prefecture,
				resort_nm,
				resort_code,
				totalsalesamount,
				roomsalesitemid,">
			<!--- 追加項目 2010/09/27 全項目統一に変更--->
			<cfset wk_field = wk_field & "roomtype,roomnumber,reservationroute,satisfy, ">

			<cfset wk_field = listappend(wk_field,add_questionnaire_nm)>
			<cfset wk_field = listappend(wk_field,add_questionnaire_sortkey)>
			<cfset wk_field = listappend(wk_field,tmp_questionnaire_nm)>				
				
			<!--- 戻値クエリ作成 --->
			<cfset retQuery = QueryNew("#wk_field#")>

			<!--- 対象設問が無い時強制リターン --->
			<cfif listlen(temp_qlist) eq 0>
				<!--- 
				<cfreturn retQuery>
				 --->
				
			</cfif>
	
			<!--- 共通日時抽出条件切替 --->
			<cfinvoke component = "conditionsetting" method = "datecondition" returnVariable ="dcondition">
				<cfinvokeargument name="dcondtion" value = #arguments.dcondtion#>
				<cfinvokeargument name="selecteddays" value = #arguments.selecteddays#>
			</cfinvoke>

			<!--- 基準日変更 --->
			<cfif arguments.arg_fieldselect EQ 0>
				<cfset selectedcondfield = "referencenumber,depart_date">
			<cfelseif arguments.arg_fieldselect EQ 1>
				<cfset dcondition = replace(dcondition,"depart_date","registrated","all")>
				<cfset selectedcondfield = "referencenumber,registrated">
			<cfelse>
				<cfset dcondition = replace(dcondition,"depart_date","arrive_date","all")>
				<cfset selectedcondfield = "referencenumber,arrive_date">
			</cfif>

			<cfif (arraylen(arguments.arg_resort_code) IS 1) and (arguments.arg_resort_code[1] IS 0)>
				<cfset sqlStr = " ">
				<cfset wk_resortcode = "">
			<cfelse>
				<cfset tmp_list = ArrayToList(arguments.arg_resort_code)>
				<cfset tmp_cmd = "">
				<cfloop from="1" to="#ListLen(tmp_list)#" index="i">
					<cfset tmp_cmd = ListAppend(tmp_cmd, arguments.arg_resort_code[i])>
				</cfloop>
				<cfset wk_resortcode = tmp_cmd>

				<cfset sqlStr = " (resort_code in (#tmp_cmd#)) and ">
			</cfif>

			<!--- 共通集計条件絞込み処理 --->
			<cfinvoke 
				component = "conditionsetting" 
				method = "sqlMake1"
				temp_cond = #arguments#
				returnVariable = "strSQL1">
	
			<!--- 共通集計条件絞込み処理 --->
			<cfinvoke 
				component = "conditionsetting" 
				method = "sqlMake2"
				temp_cond = #arguments#
				check_resort_code = #wk_resortcode#
				returnVariable = "strSQL2">
							
			<cfset wk_qlist = "">
			<cfset wk_qlist = ListAppend(wk_qlist,temp_qlist)>
			<cfset wk_qlist = ListAppend(wk_qlist,arguments.questionnaire_id)>
			<cfset wk_qlist = ListAppend(wk_qlist,Application.app00.q_value_id)>
			<cfset wk_qlist = ListAppend(wk_qlist,Application.app00.q_gender)>
			<cfset wk_qlist = ListAppend(wk_qlist,Application.app00.q_generation)>
			<cfset wk_qlist = ListAppend(wk_qlist,Application.app00.q_spouse)>
			<cfset wk_qlist = ListAppend(wk_qlist,Application.app00.q_job)>
			<cfset wk_qlist = ListAppend(wk_qlist,Application.app00.q_trip)>
			<cfset wk_qlist = ListAppend(wk_qlist,Application.app00.q_rest)>

			<!--- 抽出対象期間より期間内の回答者予約番号を抽出 --->
			<cfquery name="getAnswers" datasource = "#Application.datasource#">
				select  * 
				from 
					#Application.app00.dbu#.v_getAnswers
				where 
					#PreserveSingleQuotes(dcondition)# and
					questionnaire_id in (#wk_qlist#)
					#PreserveSingleQuotes(strSQL1)#
					#PreserveSingleQuotes(strSQL2)#
				<!--- 2008/07/16 リピーターCSは除外 --->
				<cfif arguments.questionnairecategory_id neq 0>
				and questionnairesheet_id  != 0
				<cfelse>
				and questionnairesheet_id  = 0
				</cfif>
				<!--- 2011/07/22 件数が会わない件⇒照会用番号毎にサマリしてる為の対応
				order by	 depart_date desc
				 --->
				order by #selectedcondfield# desc
			</cfquery>

			<!--- 対象者無し --->
			<cfif getAnswers.recordcount EQ 0>
				<cfreturn retQuery>
			</cfif>	

			<cfset getReservationList = getAnswers>

			<!--- <cfdump var=#getReservationList#> --->
			<!--- 2005/06/27 h.miyahara(NetFusion) ADD オプション条件 end --->
			<cfset temp_code = "">
			<cfloop query="getReservationList">
				<cfif getReservationList.referencenumber NEQ temp_code AND Len(getReservationList.answertext) NEQ 0>
					<cfset QueryAddRow(retQuery, 1)>
					<cfset temp_code = getReservationList.referencenumber>
				</cfif>
				<!--- 照会用番号 --->
	 			<cfset QuerySetCell(retQuery, "referencenumber", #getReservationList.referencenumber#&#chr(9)#)>
	 			
				<!--- 顧客番号 --->
	 			<cfset QuerySetCell(retQuery, "customernumber", #getReservationList.customernumber#)>
	 			
				<!--- 予約番号 --->
	 			<cfset QuerySetCell(retQuery, "reserve_code", #numberformat(getReservationList.reserve_code,'000000000')#&#chr(9)#)>
				
				<!--- 基準日 --->
				<cfif arguments.arg_fieldselect EQ 0>
		 			<cfset QuerySetCell(retQuery, "depart_date", #dateformat(getReservationList.depart_date,'YY/MM/DD')#&#chr(9)#)>
				<cfelseif arguments.arg_fieldselect EQ 1>
		 			<cfset QuerySetCell(retQuery, "depart_date", #dateformat(getReservationList.registrated,'YY/MM/DD')#&#chr(9)#)>
				<cfelse>
		 			<cfset QuerySetCell(retQuery, "depart_date", #dateformat(getReservationList.arrive_date,'YY/MM/DD')#&#chr(9)#)>
				</cfif>

				<!--- 出発日 --->
	 			<cfset QuerySetCell(retQuery, "orgdepart_date", #dateformat(getReservationList.depart_date,'YY/MM/DD')#&#chr(9)#)>

				<!--- 性別 --->
	 			<cfset QuerySetCell(retQuery, "gender_nm", #getReservationList.gender_nm#&#chr(9)#)>

				<!--- 年代 --->
	 			<cfset QuerySetCell(retQuery, "age_nm", #getReservationList.age_nm#&#chr(9)#)>

				<!--- 同行者 *取得予定 --->
	 			<cfset QuerySetCell(retQuery, "companion_nm", #getReservationList.together_nm#&#chr(9)#)>

				<!--- 職業 --->
	 			<cfset QuerySetCell(retQuery, "job_nm", #getReservationList.job_nm#&#chr(9)#)>

				<!--- 旅行回数 --->
	 			<cfset QuerySetCell(retQuery, "trip_nm", #getReservationList.trip_nm#&#chr(9)#)>

				<!--- 人数 --->
	 			<cfset QuerySetCell(retQuery, "numPerson", #getReservationList.numPerson#&#chr(9)#)>

				<!--- 泊数 --->
	 			<cfset QuerySetCell(retQuery, "nights", #getReservationList.nights#&#chr(9)#)>

				<!--- エリア --->
	 			<cfset QuerySetCell(retQuery, "area", #getReservationList.area#&#chr(9)#)>

				<!--- 都道府県 --->
	 			<cfset QuerySetCell(retQuery, "prefecture", #getReservationList.prefecture#&#chr(9)#)>

				<!--- 施設名称 --->
	 			<cfset QuerySetCell(retQuery, "resort_nm", #getReservationList.resort_nm#&#chr(9)#)>

				<!--- 施設コード --->
	 			<cfset QuerySetCell(retQuery, "resort_code", #getReservationList.resort_code#)>

				<!--- 客室総売上 --->
	 			<cfset QuerySetCell(retQuery, "totalsalesamount", #getReservationList.totalsalesamount#&#chr(9)#)>

				<!--- パッケージ --->
	 			<cfset QuerySetCell(retQuery, "roomsalesitemid", #getReservationList.roomsalesitemid#&#chr(9)#)>
			
				<!--- 追加項目 2010/09/27 全項目統一に変更--->
				<cfset QuerySetCell(retQuery, "roomtype", #getReservationList.roomtype#&#chr(9)#)>
				<cfset QuerySetCell(retQuery, "roomnumber", #getReservationList.roomnumber#&#chr(9)#)>
				<cfset QuerySetCell(retQuery, "reservationroute", #getReservationList.reservationroute_nm#&#chr(9)#)>
				<cfif isnumeric(getReservationList.satisfy)>
					<cfset QuerySetCell(retQuery, "satisfy", #getReservationList.satisfy#*1)>
				</cfif>

				<!--- 2005/10/13 h.miyahara(NetFusion) ADD 対象カテゴリ設問取得 B --->

				<cfloop from="1" to="#ListLen(temp_qlist)#" index="i">
					<cfif getReservationList.questionnaire_id EQ ListGetAt(temp_qlist,i)>
						<cfset point = "">
						<cfif getReservationList.questionnaire_type eq 1>
							<cfif getReservationList.answer EQ 1>
								<cfset point = 3>
								<cfset sortkey = 1>
							<cfelseif getReservationList.answer EQ 2>
								<cfset point = 2>
								<cfset sortkey = 2>
							<cfelseif getReservationList.answer EQ 3>
								<cfset point = 1>
								<cfset sortkey = 3>
							<cfelseif getReservationList.answer EQ 4>
								<cfset point = 0>
								<cfset sortkey = 4>
							<cfelseif getReservationList.answer EQ 5>
								<cfset point = -1>
								<cfset sortkey = 5>
							<cfelseif getReservationList.answer EQ 6>
								<cfset point = -2>
								<cfset sortkey = 6>
							<cfelseif getReservationList.answer EQ 7>
								<cfset point = -3>
								<cfset sortkey = 7>
							</cfif>

						<cfelse>
							<cfif getReservationList.answer EQ 0>
								<cfset point = #getReservationList.answertext#>
							<cfelse>
								<cfset point = #getReservationList.answertext#>
							</cfif>
						</cfif>

						<cfset q_nm=evaluate("retQuery." & ListGetAt(add_questionnaire_nm,i) & "[retQuery.recordcount]")>
						<cfif (#len(q_nm)# neq 0) and (#len(point)# neq 0)>
							<cfset point = #q_nm# & "," & #point#>
						</cfif>

						<cfif #point# neq "">
				 			<cfset QuerySetCell(retQuery, #ListGetAt(add_questionnaire_nm,i)#, listgetat(point,1)&#chr(9)#)>
				 			<cfset QuerySetCell(retQuery, #ListGetAt(add_questionnaire_sortkey,i)#, listgetat(sortkey,1)&#chr(9)#)>
						</cfif>

					</cfif>
				</cfloop>
				<!--- 2005/10/13 h.miyahara(NetFusion) ADD 対象カテゴリ設問取得 E --->
												
				<!--- OA --->
				<cfset temp_check = "">
				<cfloop from="1" to="#ListLen(arguments.questionnaire_id)#" index="i">
					<cfif getReservationList.questionnaire_id EQ ListGetAt(arguments.questionnaire_id,i)>
			 			<cfset QuerySetCell(retQuery, #ListGetAt(tmp_questionnaire_nm,i)#, 
													  #getReservationList.answertext#&#chr(9)#)>
			 			<cfset temp_check = temp_check & #getReservationList.answertext#>
					</cfif>
				</cfloop>
			</cfloop>
	
			<!--- 戻値調整 --->
			<cfif arguments.viewchange eq true>
				<cfquery name="retQuery" dbtype="query">
					select reserve_code,depart_date,#tmp_questionnaire_nm# from retQuery order by	 depart_date desc
				</cfquery>
			<cfelse>
				<cfset wk_field = listdeleteat(retQuery.columnList, listfind(retQuery.columnList,"DEPART_DATE"))>

				<!--- 追加項目 2010/09/27 全項目統一に変更--->
				<cfquery name="retQuery" dbtype="query">
					select
						reserve_code, 
						referencenumber,
						customernumber,
						depart_date, 
						orgdepart_date, 
						gender_nm,
						age_nm,
						companion_nm,
						job_nm,
						trip_nm,
						numPerson,
						nights,
						area, 
						prefecture,
						resort_nm,
						resort_code,
						totalsalesamount,
						roomsalesitemid,
						roomtype,roomnumber,reservationroute,satisfy , 
						<cfif arguments.questionnairecategory_id neq 0>
							<cfif len(add_questionnaire_nm) neq 0>
								#add_questionnaire_nm#, 
								#add_questionnaire_sortkey#, 
							</cfif>
							<cfif len(tmp_questionnaire_nm) neq 0>
								#tmp_questionnaire_nm# 
							</cfif>
						<cfelse>
							<cfif len(tmp_questionnaire_nm) neq 0>
								#tmp_questionnaire_nm# 
							</cfif>
						</cfif>
					from retQuery order by depart_date
				</cfquery>
			</cfif>
			<cfreturn retQuery>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>

	</cffunction>

	<!---
	*************************************************************************************************
	* Class				:oasearch
	* Method			:OAResultOutputCSV
	* Description		:OA検索結果一覧ＣＳＶ出力
	* Custom Attributes	:resultObj	出力リストのデータプロバイダ
	* 					:fieldnames	列名
	* 					:headerText	列のキャプション
	* Return Paramters	:
	* History			:1) Coded by	2004/10/05 h.miyahara(NetFusion)
	*					:2) Updated by	2005/10/26  h.miyahara(NetFusion) 集計条件追加
	*************************************************************************************************
	--->
	<cffunction name="OAResultOutputCSV" access="remote" returntype="any">
		<cfargument name="resultObj" type="Array" default="">
		<cfargument name="fieldnames" type="Array" default="">
		<cfargument name="headerText" type="Array" default="">
		<cfargument name="qstseleted" type="String" default="">
		<cfargument name="selecteddate" type="String" default="">
		<cfargument name="condseleted" type="String" default="">
		<cfargument name="resort_code" type="String" default="0">

		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<!--- リゾート名 --->
			<cfif arguments.resort_code eq 0>
				<cfset Application.resortname = "共通">
			<cfelse>
				<cfquery name="getresort" datasource = "#Application.datasource#">
					select	 resort_nm 
					from	 #Application.app00.dbu#.mst_resort
					where	 resort_code in (#arguments.resort_code#)
				</cfquery>
				<cfset Application.resortname = "#valuelist(getresort.resort_nm,'-')#">
			</cfif>

			<!--- ディレクトリ＆ファイル情報 --->
			<cfset filePath = ExpandPath(#application.outputDirectory#)>
			<cfset Dircheck = DirectoryExists( filePath & "\" & #Replace(session.user_info.user_ip,".","","all")# )>
			
			<!--- ディレクトリ存在チェック --->
			<cfif Dircheck eq "no">
				<cfdirectory action = "create" directory = "#filePath#\#Replace(session.user_info.user_ip,'.','','all')#">
			</cfif>
			
			<cfset UserDir = #Replace(session.user_info.user_ip,".","","all")# & "\">
			<cfset datepart = #DateFormat(Now(),'YYMMDD')# & #TimeFormat(Now(),'HHMMSS')#>
			<cfset filename = filePath & UserDir & datepart & "CRMResultsList.xls">
			<cfset filecheck = FileExists( filename )>
			
			<!--- ディレクトリ内ファイル数チェック --->
			<cfdirectory 
				directory="#filePath#\#Replace(session.user_info.user_ip,'.','','all')#" 
				name="myCSVDirectory"
				sort="DATELASTMODIFIED ASC">
				
			<!--- 指定ファイル数以上は作らない。古いものから削除 --->
			<cfset FileHistory = 10>
			<cfif myCSVDirectory.RecordCount GTE FileHistory>
				<cffile action = "delete" file = "#filePath##UserDir##myCSVDirectory.Name#">
			</cfif>	

			<!--- ファイル存在チェック --->
			<cfif filecheck eq "no">
				<cfset arguments.selecteddate = Replace(arguments.selecteddate,',','|','all')>
				<cfset arguments.condseleted = Replace(arguments.condseleted,',','|','all')>
				<cfset arg_condseleted = iif(len(arguments.condseleted) neq 0,de('【#arguments.condseleted#】'),de(''))>
				
				<!--- xlsブック作成＆スタイル設定 --->
				<cfinclude template="cfpoi_style.cfm">

				<!--- xlsシート作成	--->
				<cfset sheet = wb.createSheet()/>
				<cfset wb.setSheetName(0,"OA検索結果")/>

				<!--- 条件エリア --->
				<cfset row = sheet.createRow(0)/>
				<cfset cell = row.createCell(0)/>
				
				<cfset cell.setCellStyle(strobj)/>
				<cfset cell.setCellValue("【#arguments.qstseleted#】【#arguments.selecteddate#】#arg_condseleted#【出力日時：#dateformat(now(),'YYYY/MM/DD')# #timeformat(now(),'HH:MM')#】")>
				<cfset columnlength = arraylen(arguments.headerText)-1>
				<!--- region(開始上部位置、開始左位置、終了下部位置、終了右位置) --->
				<cfset region = createObject("java","org.apache.poi.hssf.util.Region").init(0,0,2,javacast("int",columnlength))/>
				<cfset sheet.addMergedRegion(region)/>
					
				<!--- ヘッダーエリア --->
				<cfset headerlist=ArrayToList(arguments.headerText)>
				<cfset row = sheet.createRow(3)/>
				<cfset cellseq = 0>
				<cfloop list="#headerlist#" index="h_idx">
					<cfset cell = row.createCell(javacast("int",cellseq))/>
					
					<cfset cell.setCellStyle(hstyleobj)/>
					<cfset cell.setCellValue("#h_idx#")>
					<cfif mid(arguments.fieldnames[cellseq+1],1,1) eq "O">
						<cfset sheet.setColumnWidth(javacast("int" , cellseq), javacast("int" , 40*256 ))/>
					</cfif>
					<cfset cellseq = cellseq+1>
				</cfloop>
					
				<cfset sheet.setColumnWidth(0, javacast("int" , 20*256 ))/>
				<cfset sheet.setColumnWidth(1, javacast("int" , 10*256 ))/>
				<cfset sheet.setColumnWidth(2, javacast("int" , 8*256 ))/>
				<cfset sheet.setColumnWidth(3, javacast("int" , 10*256 ))/>
				<cfset sheet.setColumnWidth(4, javacast("int" , 10*256 ))/>
				<cfset sheet.setColumnWidth(5, javacast("int" , 12*256 ))/>
				<cfset sheet.setColumnWidth(6, javacast("int" , 12*256 ))/>
				<cfset sheet.setColumnWidth(7, javacast("int" , 8*256 ))/>
				<cfset sheet.setColumnWidth(8, javacast("int" , 8*256 ))/>
				<cfset sheet.setColumnWidth(9, javacast("int" , 16*256 ))/>
				<cfset sheet.setColumnWidth(10,javacast("int" , 16*256 ))/>
					
				<!--- 出力内容整形 改行コード除去 --->
				<cfset rowseq = 4>
				<cfloop from="1" to="#ArrayLen(resultObj)#" index="cnt">
					<!--- 行追加 --->
					<cfset row = sheet.createRow(javacast("int",rowseq))/>

					<!--- 各列データ設定 --->
					<cfset cellseq = 0>
					<cfloop from="1" to="#ArrayLen(fieldnames)#" index="c">
						<cfset wk_op = Replace(Evaluate("resultObj[cnt].#fieldnames[c]#"),',','，','all')>
						<cfset wk_op = replacelist(wk_op,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#,#chr(9)#",",,,")>
						<cfset cell = row.createCell(javacast("int",cellseq))/>
						
						<cfset cell.setCellValue("#wk_op#")>
						
						<!--- OA --->
						<cfif mid(arguments.fieldnames[cellseq+1],1,1) eq "O">
							<cfset cell.setCellStyle(oastyleobj)/>
						</cfif>
						<cfif isnumeric(wk_op)>
							<cfset cell.setCellStyle(numstyleobj)/>
							<cfset cell.setCellValue(javacast("double","#wk_op#"))>
						<cfelse>
							<cfset cell.setCellStyle(oastyleobj)/>
						</cfif>
						
						<cfset cellseq = cellseq+1>
					</cfloop>
					
					<cfset rowseq = rowseq+1>
				</cfloop>
				
			</cfif>
			<cfset filename = filePath & UserDir & datepart & "CRMResultsList.xls">
			<cfset fileOut = createObject("java","java.io.FileOutputStream").init("#filename#")/>
				
			<cfset wb.write(fileOut)/>
			<cfset fileOut.close()/>

			<!--- 戻値設定 --->
			<cfscript>
				executeinformation = StructNew();
				StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
				StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
				StructInsert(executeinformation,	"outputMsg",	"正常"						);
				StructInsert(executeinformation,	"outputPath",	#Application.outputDirectory# & #Replace(UserDir,"\","/","all")# & datepart & "CRMResultsList.xls" );
				StructInsert(executeinformation,	"filename",		"http://#cgi.server_name#/#Application.outputDirectory#" & #Replace(UserDir,"\","/","all")# & datepart & "CRMResultsList.xls" );
				StructInsert(executeinformation,	"newname",		"#Application.resortname##datepart#.xls" );
			</cfscript>
			<cfreturn executeinformation>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>

	</cffunction>
	<!---
	*************************************************************************************************
	* Class				: oasearch
	* Method			: getCategoryQs
	* Description		: 対象カテゴリ関連の評価設問
	* Custom Attributes	: none
	* 					: 
	* Return Paramters	: retQuery(Query)
	* History			: 1) Coded by	2005/10/13 h.miyahara(NetFusion)
	*					: 2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getCategoryQs" access="remote" returntype="query" >
		<cfargument name="questionnairecategory_id" type="numeric" default="">
		<cfargument name="arg_resort_code" type="any" default="">
		<cfsetting enablecfoutputonly="yes">
		<cftry>

			<!--- 評価設問追加 B --->
			<cfif arraylen(#arguments.arg_resort_code#) IS 0>
				<cfquery name="CategoryQs" datasource = "#Application.datasource#">
					select	 distinct
							 a.questionnaire_id
							,c.questionnairecategory_nm + ': ' + a.questionnaire_nm as questionnaire_nm
					from	 #Application.app00.dbu#.mst_questionnaire a
							,#Application.app00.dbu#.oth_sheetconfiguration b
							,#Application.app00.dbu#.mst_questionnairecategory c
					where	 a.questionnaire_type <> 2
					and		 b.categoryqs in (

								select categoryqs 
								from #Application.app00.dbu#.oth_sheetconfiguration 
								where questionnaire_id in (
									select questionnaire_id from #Application.app00.dbu#.mst_questionnaire where questionnairecategory_id = #arguments.questionnairecategory_id#
								)

							)
					and		 a.questionnaire_id = b.questionnaire_id
					and		 a.questionnairecategory_id = c.questionnairecategory_id
				</cfquery>
			<cfelse>
				<!--- リゾートコード --->
				<cfset tmp_list = ArrayToList(arguments.arg_resort_code)>
				<cfset tmp_cmd = "">
				<cfloop from="1" to="#ListLen(tmp_list)#" index="i">
					<cfset tmp_cmd = ListAppend(tmp_cmd, arguments.arg_resort_code[i])>
				</cfloop>

				<cfset strSQL = " in (#tmp_cmd#) ">


				<cfquery name="CategoryQs" datasource = "#Application.datasource#">
					select	 distinct
							 a.questionnaire_id
							,c.questionnairecategory_nm + ': ' + a.questionnaire_nm as questionnaire_nm
					from	 #Application.app00.dbu#.mst_questionnaire a
							,#Application.app00.dbu#.oth_sheetconfiguration b
							,#Application.app00.dbu#.mst_questionnairecategory c
					where	 a.questionnaire_type <> 2
					and		 b.categoryqs in (

								select	 top 1 b.categoryqs
								from	 #Application.app00.dbu#.mst_questionnaire a
										,#Application.app00.dbu#.oth_sheetconfiguration b
										,#Application.app00.dbu#.mst_questionnairecategory c
										,#Application.app00.dbu#.mst_questionnairesheet d

								where	 a.questionnaire_type = 2
								and		 a.questionnaire_id = b.questionnaire_id
								and		 a.questionnairecategory_id = c.questionnairecategory_id
								and		 b.questionnairesheet_id = d.questionnairesheet_id
								and		 b.resort_code = d.resort_code
								and		 b.questionnairesheet_id <> 0
								and		 b.resort_code #strSQL#
								and		 c.questionnairecategory_id = #arguments.questionnairecategory_id#
								order by start_date desc

							)
					and		 a.questionnaire_id = b.questionnaire_id
					and		 a.questionnairecategory_id = c.questionnairecategory_id
					and		 b.resort_code #strSQL#
				</cfquery>
			</cfif>
			<cfreturn CategoryQs>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>

	</cffunction>
	<!---
	*************************************************************************************************
	* Class				:oasearch
	* Method			:getCategory
	* Description		:カテゴリリスト取得
	* Custom Attributes	:none
	* Return Paramters	:
	* History			:1) Coded by	2004/10/13 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getrepeaterCategory" access="remote" returntype="query" hint="">
		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<cfset getCategoryList = querynew("questionnairecategory_id,questionnaire_id,questionnaire_nm,cs_qlabel")>
			<cfset queryaddrow(getCategoryList,1)>
			<cfset querysetcell(getCategoryList,"questionnairecategory_id",1)>
			<cfset querysetcell(getCategoryList,"questionnaire_id",0)>
			<cfset querysetcell(getCategoryList,"questionnaire_nm","100点満点＆OA")>
			<cfset querysetcell(getCategoryList,"cs_qlabel","Q1～3")>
			<cfset queryaddrow(getCategoryList,1)>
			<cfset querysetcell(getCategoryList,"questionnairecategory_id",1)>
			<cfset querysetcell(getCategoryList,"questionnaire_id",Application.app00.q_value_id)>
			<cfset querysetcell(getCategoryList,"questionnaire_nm","100点満点分布")>
			<cfset querysetcell(getCategoryList,"cs_qlabel","Q1")>
			<cfreturn getCategoryList>
			<cfsetting enablecfoutputonly="No">
			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>

	</cffunction>		

	<!---
	*************************************************************************************************
	* Class				:oasearch
	* Method			:OAResultOutputCSV
	* Description		:OA検索結果一覧ＣＳＶ出力
	* Custom Attributes	:resultObj	出力リストのデータプロバイダ
	* 					:fieldnames	列名
	* 					:headerText	列のキャプション
	* Return Paramters	:
	* History			:1) Coded by	2004/10/05 h.miyahara(NetFusion)
	*					:2) Updated by	2005/10/26  h.miyahara(NetFusion) 集計条件追加
	*************************************************************************************************
	--->
	<cffunction name="OAResulPreview" access="remote" returntype="any">
		<cfargument name="resultObj" type="Array" default="">
		<cfargument name="fieldnames" type="Array" default="">
		<cfargument name="headerText" type="Array" default="">
		<cfargument name="qstseleted" type="String" default="">
		<cfargument name="selecteddate" type="String" default="">
		<cfargument name="condseleted" type="String" default="">
		<cfargument name="resort_code" type="String" default="0">

		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<!--- リゾート名 --->
			<cfif arguments.resort_code eq 0>
				<cfset Application.resortname = "共通">
			<cfelse>
				<cfquery name="getresort" datasource = "#Application.datasource#">
					select	 resort_nm 
					from	 #Application.app00.dbu#.mst_resort
					where	 resort_code in (#arguments.resort_code#)
				</cfquery>
				<cfset Application.resortname = "#valuelist(getresort.resort_nm,'-')#">
			</cfif>

			<!--- ディレクトリ＆ファイル情報 --->
			<cfset filePath = ExpandPath(#application.outputDirectory#)>
			<cfset Dircheck = DirectoryExists( filePath & "\" & #Replace(session.user_info.user_ip,".","","all")# )>
			
			<!--- ディレクトリ存在チェック --->
			<cfif Dircheck eq "no">
				<cfdirectory action = "create" directory = "#filePath#\#Replace(session.user_info.user_ip,'.','','all')#">
			</cfif>
			
			<cfset UserDir = #Replace(session.user_info.user_ip,".","","all")# & "\">
			<cfset datepart = #DateFormat(Now(),'YYMMDD')# & #TimeFormat(Now(),'HHMMSS')#>
			<cfset filename = filePath & UserDir & datepart & "CRMResultsList.xls">
			<cfset filecheck = FileExists( filename )>
			
			<!--- ディレクトリ内ファイル数チェック --->
			<cfdirectory 
				directory="#filePath#\#Replace(session.user_info.user_ip,'.','','all')#" 
				name="myCSVDirectory"
				sort="DATELASTMODIFIED ASC">
				
			<!--- 指定ファイル数以上は作らない。古いものから削除 --->
			<cfset FileHistory = 10>
			<cfif myCSVDirectory.RecordCount GTE FileHistory>
				<cffile action = "delete" file = "#filePath##UserDir##myCSVDirectory.Name#">
			</cfif>	

			<!--- ファイル存在チェック --->
			<cfif filecheck eq "no">
				<cfset arguments.selecteddate = Replace(arguments.selecteddate,',','|','all')>
				<cfset arguments.condseleted = Replace(arguments.condseleted,',','|','all')>
				<cfset arg_condseleted = iif(len(arguments.condseleted) neq 0,de('【#arguments.condseleted#】'),de(''))>
				
				<!--- xlsブック作成＆スタイル設定 --->
				<cfinclude template="cfpoi_style.cfm">

				<!--- xlsシート作成	--->
				<cfset sheet = wb.createSheet()/>
				<cfset wb.setSheetName(0,"OA検索結果")/>

				<!--- 描画オブジェクト生成 --->
				<cfset patriarch = sheet.createDrawingPatriarch()/>


				<!--- 条件エリア --->
				<cfset row = sheet.createRow(0)/>
				<cfset cell = row.createCell(0)/>
				
				<cfset cell.setCellStyle(strobj)/>
				<cfset cell.setCellValue("【#arguments.qstseleted#】【#arguments.selecteddate#】#arg_condseleted#【出力日時：#dateformat(now(),'YYYY/MM/DD')# #timeformat(now(),'HH:MM')#】")>
				<cfset columnlength = arraylen(arguments.headerText)-1>
				<!--- region(開始上部位置、開始左位置、終了下部位置、終了右位置) --->
				<cfset region = createObject("java","org.apache.poi.hssf.util.Region").init(0,0,2,javacast("int",columnlength))/>
				<cfset sheet.addMergedRegion(region)/>
					
				<!--- ヘッダーエリア --->
				<cfset headerlist=ArrayToList(arguments.headerText)>
				<cfset row = sheet.createRow(3)/>
				<cfset cellseq = 0>

				<cfloop list="#headerlist#" index="h_idx">
					<cfset cell = row.createCell(javacast("int",cellseq))/>
					
					<cfset cell.setCellStyle(hstyleobj)/>
					<cfset cell.setCellValue("#h_idx#")>
					<cfif mid(arguments.fieldnames[cellseq+1],1,1) eq "O">
						<cfset sheet.setColumnWidth(javacast("int" , cellseq), javacast("int" , 40*256 ))/>
					</cfif>
					<cfset cellseq = cellseq+1>
				</cfloop>
					
				<cfset sheet.setColumnWidth(0, javacast("int" , 20*256 ))/>
				<cfset sheet.setColumnWidth(1, javacast("int" , 10*256 ))/>
				<cfset sheet.setColumnWidth(2, javacast("int" , 8*256 ))/>
				<cfset sheet.setColumnWidth(3, javacast("int" , 10*256 ))/>
				<cfset sheet.setColumnWidth(4, javacast("int" , 10*256 ))/>
				<cfset sheet.setColumnWidth(5, javacast("int" , 12*256 ))/>
				<cfset sheet.setColumnWidth(6, javacast("int" , 12*256 ))/>
				<cfset sheet.setColumnWidth(7, javacast("int" , 8*256 ))/>
				<cfset sheet.setColumnWidth(8, javacast("int" , 8*256 ))/>
				<cfset sheet.setColumnWidth(9, javacast("int" , 16*256 ))/>
				<cfset sheet.setColumnWidth(10,javacast("int" , 16*256 ))/>
					
				<!--- 出力内容整形 改行コード除去 --->
				<cfset rowseq = 4>
				<cfloop from="1" to="#ArrayLen(resultObj)#" index="cnt">
					<!--- 行追加 --->
					<cfset row = sheet.createRow(javacast("int",rowseq))/>

					<!--- 各列データ設定 --->
					<cfset cellseq = 0>
					<cfloop from="1" to="#ArrayLen(fieldnames)#" index="c">
						<cfset wk_op = Replace(Evaluate("resultObj[cnt].#fieldnames[c]#"),',','，','all')>
						<cfset wk_op = replacelist(wk_op,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#,#chr(9)#",",,,")>
						<cfset cell = row.createCell(javacast("int",cellseq))/>
						
						<cfset cell.setCellValue("#wk_op#")>
						
						<!--- OA --->
						<cfif mid(arguments.fieldnames[cellseq+1],1,1) eq "O">
							<cfset cell.setCellStyle(oastyleobj)/>

							<!--- 画像ファイル有り --->
							<cfif right(wk_op,3) eq "jpg">
								<!--- 画像をストリームで読み込む --->
								<cfset imgfilepath = "../../#wk_op#">
								<cfset imgfilepath = "#ExpandPath(imgfilepath)#">

								<cfif FileExists(#imgfilepath#)>
									<cffile action="readBinary" file="#imgfilepath#" variable="binaryObject">
									<cfset pictureIdx = wb.addPicture(binaryObject, wb.PICTURE_TYPE_JPEG)/>

									<!--- アンカーの生成 --->
									<cfset anchor = createObject("java","org.apache.poi.hssf.usermodel.HSSFClientAnchor").init()/>
									<cfset anchor.setAnchorType(0) />

									<cfset anchor.setRow1(javacast("int",rowseq)) />
									<cfset anchor.setRow2(javacast("int",rowseq+1)) />

									<cfset anchor.setCol1(javacast("int",cellseq)) />
									<cfset anchor.setCol2(javacast("int",cellseq+1)) />

									<!--- partiachに画像を設定 --->
									<cfset patriarch.createPicture(anchor, pictureIdx) />
								</cfif>
							</cfif>
						</cfif>
						<cfif isnumeric(wk_op)>
							<cfset cell.setCellStyle(numstyleobj)/>
							<cfset cell.setCellValue(javacast("double","#wk_op#"))>
						<cfelse>
							<cfset cell.setCellStyle(oastyleobj)/>
						</cfif>
						
						<cfset cellseq = cellseq+1>
					</cfloop>
					
					<cfset rowseq = rowseq+1>
				</cfloop>
			
			</cfif>
			<cfset filename = filePath & UserDir & datepart & "CRMResultsList.xls">
			<cfset fileOut = createObject("java","java.io.FileOutputStream").init("#filename#")/>
				
			<cfset wb.write(fileOut)/>
			<cfset fileOut.close()/>

			<!--- 戻値設定 --->
			<cfscript>
				executeinformation = StructNew();
				StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
				StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
				StructInsert(executeinformation,	"outputMsg",	"正常"						);
				StructInsert(executeinformation,	"outputPath",	#Application.outputDirectory# & #Replace(UserDir,"\","/","all")# & datepart & "CRMResultsList.xls" );
				StructInsert(executeinformation,	"filename",		"http://#cgi.server_name#/#Application.outputDirectory#" & #Replace(UserDir,"\","/","all")# & datepart & "CRMResultsList.xls" );
				StructInsert(executeinformation,	"newname",		"#Application.resortname##datepart#.xls" );
			</cfscript>
			<cfreturn executeinformation>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">

				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
</cfcomponent>
