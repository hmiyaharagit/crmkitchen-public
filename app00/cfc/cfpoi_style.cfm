﻿<!--- xlsブック作成 --->
<cfset wb = createObject("java","org.apache.poi.hssf.usermodel.HSSFWorkbook").init()/>

<cfset strfont = wb.createFont()/>
<!--- 
<cfset strfont.setFontName("ＭＳ ゴシック")/>
 --->
<cfset strfont.setFontName("Verdana")/>
<cfset strfont.setFontHeightInPoints(9)/>
<cfset numfont = wb.createFont()/>
<cfset numfont.setFontName("Verdana")/>
<cfset numfont.setFontHeightInPoints(9)/>
<cfset ucntfont = wb.createFont()/>
<cfset ucntfont.setFontName("Verdana")/>
<cfset ucntfont.setBoldweight(ucntfont.BOLDWEIGHT_BOLD)/>
<cfset ucntfont.setColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$ROYAL_BLUE").getIndex())/>
<cfset ucntfont.setFontHeightInPoints(9)/>

<!--- 集計条件スタイル --->
<cfset cstrobj = wb.createCellStyle()/>
<cfset cstrobj.setFont(strfont)/>

<!--- 結合用スタイル --->
<cfset styleobj = wb.createCellStyle()/>
<cfset styleobj.setVerticalAlignment(styleobj.VERTICAL_CENTER)/>
<cfset styleobj.setAlignment(styleobj.ALIGN_CENTER)/>
<cfset styleobj.setWrapText(true)/>
<cfset styleobj.setFont(strfont)/>
<cfset styleobj.setBorderBottom(styleobj.BORDER_THIN)/>
<cfset styleobj.setBottomBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
<cfset styleobj.setBorderLeft(styleobj.BORDER_THIN)/>
<cfset styleobj.setLeftBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
<cfset styleobj.setBorderRight(styleobj.BORDER_THIN)/>
<cfset styleobj.setRightBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
<cfset styleobj.setBorderTop(styleobj.BORDER_THIN)/>
<cfset styleobj.setTopBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>

<!--- 設問値用スタイル --->
<cfset strobj = wb.createCellStyle()/>
<cfset strformat = wb.createDataFormat()/>
<cfset strobj.setFont(strfont)/>
<cfset strobj.setBorderBottom(strobj.BORDER_THIN)/>
<cfset strobj.setBottomBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
<cfset strobj.setBorderLeft(strobj.BORDER_THIN)/>
<cfset strobj.setLeftBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
<cfset strobj.setBorderRight(strobj.BORDER_THIN)/>
<cfset strobj.setRightBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
<cfset strobj.setBorderTop(strobj.BORDER_THIN)/>
<cfset strobj.setTopBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>

<!--- サンプル値用スタイル --->
<cfset sampleobj = wb.createCellStyle()/>
<cfset sampleobj.setVerticalAlignment(sampleobj.VERTICAL_CENTER)/>
<cfset sampleobj.setAlignment(sampleobj.ALIGN_CENTER)/>
<cfset samformat = wb.createDataFormat()/>
<cfset sampleobj.setDataFormat(samformat.getFormat("0"))/>
<cfset sampleobj.setFont(numfont)/>
<cfset sampleobj.setBorderBottom(sampleobj.BORDER_THIN)/>
<cfset sampleobj.setBottomBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
<cfset sampleobj.setBorderLeft(sampleobj.BORDER_THIN)/>
<cfset sampleobj.setLeftBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
<cfset sampleobj.setBorderRight(sampleobj.BORDER_THIN)/>
<cfset sampleobj.setRightBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
<cfset sampleobj.setBorderTop(sampleobj.BORDER_THIN)/>
<cfset sampleobj.setTopBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>

<!--- 数値用スタイル --->
<cfset numstyleobj = wb.createCellStyle()/>
<cfset numstyleobj.setAlignment(numstyleobj.ALIGN_RIGHT)/>
<cfset numformat = wb.createDataFormat()/>
<cfset numstyleobj.setDataFormat(numformat.getFormat("0"))/>
<cfset numstyleobj.setFont(numfont)/>
<cfset numstyleobj.setBorderBottom(numstyleobj.BORDER_THIN)/>
<cfset numstyleobj.setBottomBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
<cfset numstyleobj.setBorderLeft(numstyleobj.BORDER_THIN)/>
<cfset numstyleobj.setLeftBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
<cfset numstyleobj.setBorderRight(numstyleobj.BORDER_THIN)/>
<cfset numstyleobj.setRightBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
<cfset numstyleobj.setBorderTop(numstyleobj.BORDER_THIN)/>
<cfset numstyleobj.setTopBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>

<!--- 割合用スタイル --->
<cfset ratestyleobj = wb.createCellStyle()/>
<cfset ratestyleobj.setAlignment(ratestyleobj.ALIGN_RIGHT)/>
<cfset rateformat = wb.createDataFormat()/>
<cfset ratestyleobj.setDataFormat(rateformat.getFormat("0.0"))/>
<cfset ratestyleobj.setFont(numfont)/>
<cfset ratestyleobj.setBorderBottom(ratestyleobj.BORDER_THIN)/>
<cfset ratestyleobj.setBottomBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
<cfset ratestyleobj.setBorderLeft(ratestyleobj.BORDER_THIN)/>
<cfset ratestyleobj.setLeftBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
<cfset ratestyleobj.setBorderRight(ratestyleobj.BORDER_THIN)/>
<cfset ratestyleobj.setRightBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
<cfset ratestyleobj.setBorderTop(ratestyleobj.BORDER_THIN)/>
<cfset ratestyleobj.setTopBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>

<!--- スコア値用スタイル --->
<cfset scoreobj = wb.createCellStyle()/>
<cfset scoreobj.setVerticalAlignment(scoreobj.VERTICAL_CENTER)/>
<cfset scoreobj.setAlignment(scoreobj.ALIGN_CENTER)/>
<cfset scoreobj.setDataFormat(rateformat.getFormat("0.00"))/>
<cfset scoreobj.setFont(numfont)/>
<cfset scoreobj.setBorderBottom(ratestyleobj.BORDER_THIN)/>
<cfset scoreobj.setBottomBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
<cfset scoreobj.setBorderLeft(ratestyleobj.BORDER_THIN)/>
<cfset scoreobj.setLeftBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
<cfset scoreobj.setBorderRight(ratestyleobj.BORDER_THIN)/>
<cfset scoreobj.setRightBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
<cfset scoreobj.setBorderTop(ratestyleobj.BORDER_THIN)/>
<cfset scoreobj.setTopBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>

<!--- Ｎ数項目表示用スタイル --->
<cfset hnsumobj = wb.createCellStyle()/>
<cfset hnsumobj.setAlignment(hnsumobj.ALIGN_RIGHT)/>
<cfset hnsumobj.setFont(ucntfont)/>
<cfset hnsumobj.setBorderBottom(hnsumobj.BORDER_THIN)/>
<cfset hnsumobj.setBottomBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
<cfset hnsumobj.setBorderLeft(hnsumobj.BORDER_THIN)/>
<cfset hnsumobj.setLeftBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
<cfset hnsumobj.setBorderRight(hnsumobj.BORDER_THIN)/>
<cfset hnsumobj.setRightBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
<cfset hnsumobj.setBorderTop(hnsumobj.BORDER_THIN)/>
<cfset hnsumobj.setTopBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>

<!--- Ｎ数表示スタイル --->
<cfset nsumobj = wb.createCellStyle()/>
<cfset nsumobj.setAlignment(scoreobj.ALIGN_RIGHT)/>
<cfset nsumobj.setFont(ucntfont)/>
<cfset nsumobj.setBorderBottom(nsumobj.BORDER_THIN)/>
<cfset nsumobj.setBottomBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
<cfset nsumobj.setBorderLeft(nsumobj.BORDER_THIN)/>
<cfset nsumobj.setLeftBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
<cfset nsumobj.setBorderRight(nsumobj.BORDER_THIN)/>
<cfset nsumobj.setRightBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
<cfset nsumobj.setBorderTop(nsumobj.BORDER_THIN)/>
<cfset nsumobj.setTopBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>

<!--- 利用率項目表示スタイル --->
<cfset hucntobj = wb.createCellStyle()/>
<cfset hucntobj.setAlignment(hucntobj.ALIGN_RIGHT)/>
<cfset hucntobj.setFont(ucntfont)/>
<cfset hucntobj.setBorderBottom(hucntobj.BORDER_THIN)/>
<cfset hucntobj.setBottomBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
<cfset hucntobj.setBorderLeft(hucntobj.BORDER_THIN)/>
<cfset hucntobj.setLeftBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
<cfset hucntobj.setBorderRight(hucntobj.BORDER_THIN)/>
<cfset hucntobj.setRightBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
<cfset hucntobj.setBorderTop(hucntobj.BORDER_THIN)/>
<cfset hucntobj.setTopBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>

<!--- 利用率表示スタイル --->
<cfset ucntobj = wb.createCellStyle()/>
<cfset ucntobj.setAlignment(ucntobj.ALIGN_RIGHT)/>
<cfset ucntobj.setDataFormat(rateformat.getFormat("0.0"))/>
<cfset ucntobj.setFont(ucntfont)/>
<cfset ucntobj.setBorderBottom(ucntobj.BORDER_THIN)/>
<cfset ucntobj.setBottomBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
<cfset ucntobj.setBorderLeft(ucntobj.BORDER_THIN)/>
<cfset ucntobj.setLeftBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
<cfset ucntobj.setBorderRight(ucntobj.BORDER_THIN)/>
<cfset ucntobj.setRightBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
<cfset ucntobj.setBorderTop(ucntobj.BORDER_THIN)/>
<cfset ucntobj.setTopBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>

<!--- ヘッダー用スタイル --->
<cfset hstyleobj = wb.createCellStyle()/>
<cfset hstyleobj.setVerticalAlignment(hstyleobj.VERTICAL_CENTER)/>
<cfset hstyleobj.setAlignment(hstyleobj.ALIGN_CENTER)/>
<cfset hstyleobj.setWrapText(true)/>
<cfset hstyleobj.setFont(strfont)/>
<cfset hstyleobj.setFillForegroundColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$CORNFLOWER_BLUE").getIndex())/>
<cfset hstyleobj.setFillPattern(hstyleobj.SOLID_FOREGROUND)/>
<cfset hstyleobj.setBorderBottom(hstyleobj.BORDER_THIN)/>
<cfset hstyleobj.setBottomBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
<cfset hstyleobj.setBorderLeft(hstyleobj.BORDER_THIN)/>
<cfset hstyleobj.setLeftBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
<cfset hstyleobj.setBorderRight(hstyleobj.BORDER_THIN)/>
<cfset hstyleobj.setRightBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
<cfset hstyleobj.setBorderTop(hstyleobj.BORDER_THIN)/>
<cfset hstyleobj.setTopBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>

<!--- OA値用スタイル --->
<cfset oastyleobj = wb.createCellStyle()/>
<cfset oastyleobj.setVerticalAlignment(hstyleobj.VERTICAL_TOP)/>
<cfset oastyleobj.setWrapText(true)/>
<cfset oastyleobj.setFont(strfont)/>
<cfset oastyleobj.setBorderBottom(oastyleobj.BORDER_THIN)/>
<cfset oastyleobj.setBottomBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
<cfset oastyleobj.setBorderLeft(oastyleobj.BORDER_THIN)/>
<cfset oastyleobj.setLeftBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
<cfset oastyleobj.setBorderRight(oastyleobj.BORDER_THIN)/>
<cfset oastyleobj.setRightBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
<cfset oastyleobj.setBorderTop(oastyleobj.BORDER_THIN)/>
<cfset oastyleobj.setTopBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
