﻿<cfcomponent hint="ＣＳ集計情報取得ＣＦＣ">
	<!---
	*************************************************************************************************
	* Class				:
	* Method			:
	* Description		:ＣＳＶ出力
	* Custom Attributes	:resultObj	出力リストのデータプロバイダ
	* Return Paramters	:
	* History			:1) Coded by	2004/10/05 h.miyahara(NetFusion)
	*					:3) Updated by	2005/11/16 h.miyahara(NetFusion) 出力項目追加
	*************************************************************************************************
	--->
	<cffunction name="CRMResultOutputCSV" access="remote" returntype="any">
		<cfargument name="resultObj" type="Array" default="">
		<cfargument name="selecteddate" type="String" default="">
		<cfargument name="condseleted" type="String" default="">
		<cfargument name="outputtype" type="Numeric" default="">
		<cfargument name="resort_code" type="String" default="">
		
		<cftry>
			<!--- リゾート名 --->
			<cfif arguments.resort_code eq 0>
				<cfset Application.resortname = "共通">
			<cfelse>
				<cfquery name="getresort" datasource = "#Application.datasource#">
					select	 resort_nm 
					from	 #Application.app00.dbu#.mst_resort
					where	 resort_code in (#arguments.resort_code#)
				</cfquery>
				<cfset Application.resortname = "#valuelist(getresort.resort_nm,'-')#">
			</cfif>

			<!--- ディレクトリ＆ファイル情報 --->
			<cfset filePath = ExpandPath(#application.outputDirectory#)>
			<cfset Dircheck = DirectoryExists( filePath & "\" & #Replace(session.user_info.user_ip,".","","all")# )>
			
			<!--- ディレクトリ存在チェック --->
			<cfif Dircheck eq "no">
				<cfdirectory action = "create" directory = "#filePath#\#Replace(session.user_info.user_ip,'.','','all')#">
			</cfif>
			
			<cfset UserDir = #Replace(session.user_info.user_ip,".","","all")# & "\">
			<cfset datepart = #DateFormat(Now(),'YYMMDD')# & #TimeFormat(Now(),'HHMMSS')#>
			<cfset filename = filePath & UserDir & datepart & "CRMResultsList.xls">
			<cfset filecheck = FileExists( filename )>
			
			<!--- ディレクトリ内ファイル数チェック --->
			<cfdirectory 
				directory="#filePath#\#Replace(session.user_info.user_ip,'.','','all')#" 
				name="myCSVDirectory"
				sort="DATELASTMODIFIED ASC">
				
			<!--- 指定ファイル数以上は作らない。古いものから削除 --->
			<cfset FileHistory = 10>
			<cfif myCSVDirectory.RecordCount GTE FileHistory>
				<cffile action = "delete" file = "#filePath##UserDir##myCSVDirectory.Name#">
			</cfif>	
		
			<!--- ファイル存在チェック --->
			<cfif filecheck eq "no">
				<cfset arguments.selecteddate = Replace(arguments.selecteddate,',','|','all')>
				<cfset arguments.condseleted = Replace(arguments.condseleted,',','|','all')>
				<cfset arg_condseleted = iif(len(arguments.condseleted) neq 0,de('【#arguments.condseleted#】'),de(''))>

				<!--- xlsブック作成＆スタイル設定 --->
				<cfinclude template="cfpoi_style.cfm">

				<!--- xlsシート作成 (指定月分)	--->
				<cfset arg_sheetlist="1">
				<cfloop from="1" to="#listlen(arg_sheetlist)#" index="i">
					<cfset sheet = wb.createSheet()/>
					<cfset sheetseq = i-1>
					<cfset wb.setSheetName(javacast("int",sheetseq),"sheet"& #i#,wb.ENCODING_UTF_16)/>
					
					<cfset sheet.setColumnWidth(0, javacast("int" , 8*256 ))/>
					<cfset sheet.setColumnWidth(1, javacast("int" , 23*256 ))/>
					<cfset sheet.setColumnWidth(2, javacast("int" , 6*256 ))/>
					<cfset sheet.setColumnWidth(3, javacast("int" , 8*256 ))/>
					<cfset sheet.setColumnWidth(4, javacast("int" , 7*256 ))/>
					<cfset sheet.setColumnWidth(5, javacast("int" , 5*256 ))/>
					<cfset sheet.setColumnWidth(6, javacast("int" , 5*256 ))/>
					<cfset sheet.setColumnWidth(7, javacast("int" , 6*256 ))/>
					<cfset sheet.setColumnWidth(8, javacast("int" , 6*256 ))/>
					<cfset sheet.setColumnWidth(9, javacast("int" , 6*256 ))/>
					<cfset sheet.setColumnWidth(10,javacast("int" , 6*256 ))/>
					<cfset sheet.setColumnWidth(11,javacast("int" , 6*256 ))/>
					<cfset sheet.setColumnWidth(12,javacast("int" , 6*256 ))/>
					<cfset sheet.setColumnWidth(13,javacast("int" , 6*256 ))/>
					<cfset sheet.setColumnWidth(14,javacast("int" , 6*256 ))/>
					<cfset sheet.setColumnWidth(15,javacast("int" , 6*256 ))/>
					<cfset sheet.setColumnWidth(16,javacast("int" , 6*256 ))/>
					<cfset sheet.setColumnWidth(17,javacast("int" , 6*256 ))/>
					<cfset sheet.setColumnWidth(18,javacast("int" , 6*256 ))/>
					<cfset sheet.setColumnWidth(19,javacast("int" , 6*256 ))/>

					<!--- 条件エリア --->
					<cfset row = sheet.createRow(0)/>
					<cfset cell = row.createCell(0)/>
					<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
					<cfset cell.setCellStyle(strobj)/>
					<cfset cell.setCellValue("【#arguments.selecteddate#】#arg_condseleted#【出力日時：#dateformat(now(),'YYYY/MM/DD')# #timeformat(now(),'HH:MM')#】")>
					<!--- region(開始上部位置、開始左位置、終了下部位置、終了右位置) --->
					<cfset region = createObject("java","org.apache.poi.hssf.util.Region").init(0,0,2,19)/>
					<cfset sheet.addMergedRegion(region)/>
				
					<!--- ヘッダーエリア --->
					<cfswitch expression="#arguments.outputtype#">
						<cfcase value="0">	<cfset captext = "有効回答サンプル/スコア合計">	</cfcase>
						<cfcase value="1">	<cfset captext = "有効回答サンプル">			</cfcase>
						<cfcase value="2">	<cfset captext = "有効回答サンプル">			</cfcase>
					</cfswitch>	
					<cfset headerlist="カテゴリ,設問,全回答Ｎ数,#captext#,平均スコア,ランク,#chr(9)#,＋３,＋２,＋１,０,－１,－２,－３,不明,Top1,Top2+3,Total Negative,未利用,利用率">
					<cfset row = sheet.createRow(3)/>
					<cfset cellseq = 0>
					<cfloop list="#headerlist#" index="h_idx">
						<cfset cell = row.createCell(javacast("int",cellseq))/>
						<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
						<cfset cell.setCellStyle(hstyleobj)/>
						<cfset cell.setCellValue("#h_idx#")>
						<cfset cellseq = cellseq+1>
					</cfloop>
						
					<!--- 結果表示エリア --->	
					<cfset rowseq = 4>
					<cfloop from="1" to="#ArrayLen(resultObj)#" index="cnt">
						<!--- 改行コード除去 --->
						<cfset resultObj[cnt].questionnaire_nm = #replacelist(resultObj[cnt].questionnaire_nm,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
						<cfset resultObj[cnt].samples = #replacelist(resultObj[cnt].samples,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
						<cfset resultObj[cnt].average = #replacelist(resultObj[cnt].average,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
						<cfset resultObj[cnt].rank = #replacelist(resultObj[cnt].rank,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
						<cfset resultObj[cnt].result1_samples = #replacelist(resultObj[cnt].result1_samples,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
						<cfset resultObj[cnt].result1_rate = #replacelist(resultObj[cnt].result1_rate,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
						<cfset resultObj[cnt].result2_samples = #replacelist(resultObj[cnt].result2_samples,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
						<cfset resultObj[cnt].result2_rate = #replacelist(resultObj[cnt].result2_rate,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
						<cfset resultObj[cnt].result3_samples = #replacelist(resultObj[cnt].result3_samples,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
						<cfset resultObj[cnt].result3_rate = #replacelist(resultObj[cnt].result3_rate,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
						<cfset resultObj[cnt].result4_samples = #replacelist(resultObj[cnt].result4_samples,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
						<cfset resultObj[cnt].result4_rate = #replacelist(resultObj[cnt].result4_rate,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
						<cfset resultObj[cnt].result5_samples = #replacelist(resultObj[cnt].result5_samples,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
						<cfset resultObj[cnt].result5_rate = #replacelist(resultObj[cnt].result5_rate,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
						<cfset resultObj[cnt].result6_samples = #replacelist(resultObj[cnt].result6_samples,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
						<cfset resultObj[cnt].result6_rate = #replacelist(resultObj[cnt].result6_rate,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
						<cfset resultObj[cnt].result7_samples = #replacelist(resultObj[cnt].result7_samples,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
						<cfset resultObj[cnt].result7_rate = #replacelist(resultObj[cnt].result7_rate,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
						<cfset resultObj[cnt].result8_samples = #replacelist(resultObj[cnt].result8_samples,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
						<cfset resultObj[cnt].result8_rate = #replacelist(resultObj[cnt].result8_rate,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
						<cfset resultObj[cnt].result0_samples = #replacelist(resultObj[cnt].result0_samples,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
						<cfset resultObj[cnt].result0_rate = #replacelist(resultObj[cnt].result0_rate,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>

						<cfset resultObj[cnt].effective_n = #replacelist(resultObj[cnt].effective_n,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
						<cfset resultObj[cnt].sum_score = #replacelist(resultObj[cnt].sum_score,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
						<cfset resultObj[cnt].top1 = #replacelist(resultObj[cnt].top1,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
						<cfset resultObj[cnt].top2and3 = #replacelist(resultObj[cnt].top2and3,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
						<cfset resultObj[cnt].nagetive = #replacelist(resultObj[cnt].nagetive,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>

						<!--- 行追加 --->
						<cfset row = sheet.createRow(javacast("int",rowseq))/>

						<!--- 設問 --->
						<cfset cell = row.createCell(1)/>
						<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
						<cfset cell.setCellStyle(strobj)/>
						<cfset cell.setCellValue("#resultObj[cnt].questionnaire_nm#")>
							
						<cfif len(resultObj[cnt].questionnaire_nm) eq 0>
							<!--- 【N数】表示 --->
							<cfif len(resultObj[cnt].totalusecount) neq 0>
								<cfset cell.setCellStyle(hnsumobj)/>
								<cfset cell.setCellValue("【N数】")>

								<cfset cell = row.createCell(2)/>
								<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
								<cfset cell.setCellStyle(nsumobj)/>
								<cfset cell.setCellValue(javacast("double","#resultObj[cnt].samples#"))>

								<cfset cell = row.createCell(3)/>
								<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
								<cfset cell.setCellStyle(hucntobj)/>
								<cfset cell.setCellValue("利用率")>

								<cfset cell = row.createCell(4)/>
								<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
								<cfset cell.setCellStyle(ucntobj)/>
								<cfset cell.setCellValue(javacast("double","#resultObj[cnt].totalusecount#"))>
							</cfif>
						<cfelse>
							
							<!--- サンプル数 --->
							<cfset cell = row.createCell(2)/>
							<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
							<cfset cell.setCellStyle(sampleobj)/>
							<cfset cell.setCellValue(javacast("double","#resultObj[cnt].samples#"))>
								
							<!--- 有効回答数 --->
							<cfset cell = row.createCell(3)/>
							<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
							<cfset cell.setCellStyle(numstyleobj)/>
							<cfset cell.setCellValue(javacast("double","#resultObj[cnt].effective_n#"))>

							<!--- 平均スコア --->
							<cfset cell = row.createCell(4)/>
							<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
							<cfset cell.setCellStyle(scoreobj)/>
							<cfset cell.setCellValue(javacast("double","#resultObj[cnt].average#"))>

							<!--- ランク --->
							<cfset cell = row.createCell(5)/>
							<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
<!---  --->
							<cfswitch expression="#resultObj[cnt].rank#">
								<cfcase value="A">
									<cfset rankstyleobj = wb.createCellStyle()/>
									<cfset rankstyleobj.setVerticalAlignment(rankstyleobj.VERTICAL_CENTER)/>
									<cfset rankstyleobj.setAlignment(rankstyleobj.ALIGN_CENTER)/>
									<cfset rankstyleobj.setFillForegroundColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$ROSE").getIndex())/>
									<cfset rankstyleobj.setFillPattern(rankstyleobj.SOLID_FOREGROUND)/>
									<cfset rankstyleobj.setBorderBottom(rankstyleobj.BORDER_THIN)/>
									<cfset rankstyleobj.setBottomBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
									<cfset rankstyleobj.setBorderLeft(rankstyleobj.BORDER_THIN)/>
									<cfset rankstyleobj.setLeftBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
									<cfset rankstyleobj.setBorderRight(rankstyleobj.BORDER_THIN)/>
									<cfset rankstyleobj.setRightBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
									<cfset rankstyleobj.setBorderTop(rankstyleobj.BORDER_THIN)/>
									<cfset rankstyleobj.setTopBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
								
								</cfcase>
								<cfcase value="B">
									<cfset rankstyleobj = wb.createCellStyle()/>
									<cfset rankstyleobj.setVerticalAlignment(rankstyleobj.VERTICAL_CENTER)/>
									<cfset rankstyleobj.setAlignment(rankstyleobj.ALIGN_CENTER)/>
									<cfset rankstyleobj.setFillForegroundColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$LIGHT_ORANGE").getIndex())/>
									<cfset rankstyleobj.setFillPattern(rankstyleobj.SOLID_FOREGROUND)/>
									<cfset rankstyleobj.setBorderBottom(rankstyleobj.BORDER_THIN)/>
									<cfset rankstyleobj.setBottomBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
									<cfset rankstyleobj.setBorderLeft(rankstyleobj.BORDER_THIN)/>
									<cfset rankstyleobj.setLeftBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
									<cfset rankstyleobj.setBorderRight(rankstyleobj.BORDER_THIN)/>
									<cfset rankstyleobj.setRightBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
									<cfset rankstyleobj.setBorderTop(rankstyleobj.BORDER_THIN)/>
									<cfset rankstyleobj.setTopBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
								</cfcase>
								<cfcase value="C">
									<cfset rankstyleobj = wb.createCellStyle()/>
									<cfset rankstyleobj.setVerticalAlignment(rankstyleobj.VERTICAL_CENTER)/>
									<cfset rankstyleobj.setAlignment(rankstyleobj.ALIGN_CENTER)/>
									<cfset rankstyleobj.setFillForegroundColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$LIGHT_YELLOW").getIndex())/>
									<cfset rankstyleobj.setFillPattern(rankstyleobj.SOLID_FOREGROUND)/>
									<cfset rankstyleobj.setBorderBottom(rankstyleobj.BORDER_THIN)/>
									<cfset rankstyleobj.setBottomBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
									<cfset rankstyleobj.setBorderLeft(rankstyleobj.BORDER_THIN)/>
									<cfset rankstyleobj.setLeftBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
									<cfset rankstyleobj.setBorderRight(rankstyleobj.BORDER_THIN)/>
									<cfset rankstyleobj.setRightBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
									<cfset rankstyleobj.setBorderTop(rankstyleobj.BORDER_THIN)/>
									<cfset rankstyleobj.setTopBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
								</cfcase>
								<cfcase value="D">
									<cfset rankstyleobj = wb.createCellStyle()/>
									<cfset rankstyleobj.setVerticalAlignment(rankstyleobj.VERTICAL_CENTER)/>
									<cfset rankstyleobj.setAlignment(rankstyleobj.ALIGN_CENTER)/>
									<cfset rankstyleobj.setFillForegroundColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$LIGHT_GREEN").getIndex())/>
									<cfset rankstyleobj.setFillPattern(rankstyleobj.SOLID_FOREGROUND)/>
									<cfset rankstyleobj.setBorderBottom(rankstyleobj.BORDER_THIN)/>
									<cfset rankstyleobj.setBottomBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
									<cfset rankstyleobj.setBorderLeft(rankstyleobj.BORDER_THIN)/>
									<cfset rankstyleobj.setLeftBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
									<cfset rankstyleobj.setBorderRight(rankstyleobj.BORDER_THIN)/>
									<cfset rankstyleobj.setRightBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
									<cfset rankstyleobj.setBorderTop(rankstyleobj.BORDER_THIN)/>
									<cfset rankstyleobj.setTopBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
								</cfcase>
								<cfcase value="E">
									<cfset rankstyleobj = wb.createCellStyle()/>
									<cfset rankstyleobj.setVerticalAlignment(rankstyleobj.VERTICAL_CENTER)/>
									<cfset rankstyleobj.setAlignment(rankstyleobj.ALIGN_CENTER)/>
									<cfset rankstyleobj.setFillForegroundColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$CORNFLOWER_BLUE").getIndex())/>
									<cfset rankstyleobj.setFillPattern(rankstyleobj.SOLID_FOREGROUND)/>
									<cfset rankstyleobj.setBorderBottom(rankstyleobj.BORDER_THIN)/>
									<cfset rankstyleobj.setBottomBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
									<cfset rankstyleobj.setBorderLeft(rankstyleobj.BORDER_THIN)/>
									<cfset rankstyleobj.setLeftBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
									<cfset rankstyleobj.setBorderRight(rankstyleobj.BORDER_THIN)/>
									<cfset rankstyleobj.setRightBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
									<cfset rankstyleobj.setBorderTop(rankstyleobj.BORDER_THIN)/>
									<cfset rankstyleobj.setTopBorderColor(createObject("java","org.apache.poi.hssf.util.HSSFColor$BLACK").getIndex())/>
								</cfcase>
							</cfswitch>
							<cfset cell.setCellStyle(rankstyleobj)/>
							<cfset cell.setCellValue("#resultObj[cnt].rank#")>

							<!--- 実数・％ --->
							<cfset cell = row.createCell(6)/>
							<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
							<cfset cell.setCellValue("実数")>
							<cfset cell.setCellStyle(styleobj)/>

							<!--- 実数１ --->
							<cfset cell = row.createCell(7)/>
							<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
							<cfif arguments.outputtype neq 2>
								<cfset cell.setCellStyle(numstyleobj)/>
								<cfset cell.setCellValue(javacast("double","#resultObj[cnt].result1_samples#"))>
							<cfelse>
								<cfset cell.setCellStyle(ratestyleobj)/>
								<cfset cell.setCellValue(javacast("double","#resultObj[cnt].result1_rate#"))>
							</cfif>

							<!--- 実数２ --->
							<cfset cell = row.createCell(8)/>
							<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
							<cfif arguments.outputtype neq 2>
								<cfset cell.setCellStyle(numstyleobj)/>
								<cfset cell.setCellValue(javacast("double","#resultObj[cnt].result2_samples#"))>
							<cfelse>
								<cfset cell.setCellStyle(ratestyleobj)/>
								<cfset cell.setCellValue(javacast("double","#resultObj[cnt].result2_rate#"))>
							</cfif>

							<!--- 実数３ --->
							<cfset cell = row.createCell(9)/>
							<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
							<cfif arguments.outputtype neq 2>
								<cfset cell.setCellStyle(numstyleobj)/>
								<cfset cell.setCellValue(javacast("double","#resultObj[cnt].result3_samples#"))>
							<cfelse>
								<cfset cell.setCellStyle(ratestyleobj)/>
								<cfset cell.setCellValue(javacast("double","#resultObj[cnt].result3_rate#"))>
							</cfif>

							<!--- 実数４ --->
							<cfset cell = row.createCell(10)/>
							<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
							<cfif arguments.outputtype neq 2>
								<cfset cell.setCellStyle(numstyleobj)/>
								<cfset cell.setCellValue(javacast("double","#resultObj[cnt].result4_samples#"))>
							<cfelse>
								<cfset cell.setCellStyle(ratestyleobj)/>
								<cfset cell.setCellValue(javacast("double","#resultObj[cnt].result4_rate#"))>
							</cfif>

							<!--- 実数５ --->
							<cfset cell = row.createCell(11)/>
							<cfif arguments.outputtype neq 2>
								<cfset cell.setCellStyle(numstyleobj)/>
								<cfset cell.setCellValue(javacast("double","#resultObj[cnt].result5_samples#"))>
							<cfelse>
								<cfset cell.setCellStyle(ratestyleobj)/>
								<cfset cell.setCellValue(javacast("double","#resultObj[cnt].result5_rate#"))>
							</cfif>

							<!--- 実数６ --->
							<cfset cell = row.createCell(12)/>
							<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
							<cfif arguments.outputtype neq 2>
								<cfset cell.setCellStyle(numstyleobj)/>
								<cfset cell.setCellValue(javacast("double","#resultObj[cnt].result6_samples#"))>
							<cfelse>
								<cfset cell.setCellStyle(ratestyleobj)/>
								<cfset cell.setCellValue(javacast("double","#resultObj[cnt].result6_rate#"))>
							</cfif>

							<!--- 実数７ --->
							<cfset cell = row.createCell(13)/>
							<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
							<cfif arguments.outputtype neq 2>
								<cfset cell.setCellStyle(numstyleobj)/>
								<cfset cell.setCellValue(javacast("double","#resultObj[cnt].result7_samples#"))>
							<cfelse>
								<cfset cell.setCellStyle(ratestyleobj)/>
								<cfset cell.setCellValue(javacast("double","#resultObj[cnt].result7_rate#"))>
							</cfif>

							<!--- 不明 --->
							<cfset cell = row.createCell(14)/>
							<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
							<cfif arguments.outputtype neq 2>
								<cfif isnumeric(resultObj[cnt].result0_samples)>
									<cfset cell.setCellStyle(numstyleobj)/>
									<cfset cell.setCellValue(javacast("double","#resultObj[cnt].result0_samples#"))>
								<cfelse>
									<cfset cell.setCellStyle(styleobj)/>
									<cfset cell.setCellValue("-")>
								</cfif>
							<cfelse>
								<cfif isnumeric(resultObj[cnt].result0_rate)>
									<cfset cell.setCellStyle(ratestyleobj)/>
									<cfset cell.setCellValue(javacast("double","#resultObj[cnt].result0_rate#"))>
								<cfelse>
									<cfset cell.setCellStyle(styleobj)/>
									<cfset cell.setCellValue("-")>
								</cfif>
							</cfif>

							<!--- top1 --->
							<cfset cell = row.createCell(15)/>
							<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
							<cfset cell.setCellStyle(ratestyleobj)/>
							<cfset cell.setCellValue(javacast("double","#resultObj[cnt].top1#"))>

							<!--- top2and3 --->
							<cfset cell = row.createCell(16)/>
							<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
							<cfset cell.setCellStyle(ratestyleobj)/>
							<cfset cell.setCellValue(javacast("double","#resultObj[cnt].top2and3#"))>

							<!--- nagetive --->
							<cfset cell = row.createCell(17)/>
							<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
							<cfset cell.setCellStyle(ratestyleobj)/>
							<cfset cell.setCellValue(javacast("double","#resultObj[cnt].nagetive#"))>

							<!--- 実数８ --->
							<cfset cell = row.createCell(18)/>
							<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
							<cfif arguments.outputtype neq 2>
								<cfif isnumeric(resultObj[cnt].result8_samples)>
									<cfset cell.setCellStyle(numstyleobj)/>
									<cfset cell.setCellValue(javacast("double","#resultObj[cnt].result8_samples#"))>
								<cfelse>
									<cfset cell.setCellStyle(styleobj)/>
									<cfset cell.setCellValue("-")>
								</cfif>
							<cfelse>
								<cfif isnumeric(resultObj[cnt].result8_rate)>
									<cfset cell.setCellStyle(ratestyleobj)/>
									<cfset cell.setCellValue(javacast("double","#resultObj[cnt].result8_rate#"))>
								<cfelse>
									<cfset cell.setCellStyle(styleobj)/>
									<cfset cell.setCellValue("-")>
								</cfif>
							</cfif>

							<!--- 利用率 --->
							<cfset cell = row.createCell(19)/>
							<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
							<cfif isnumeric(resultObj[cnt].usecount)>
								<cfset cell.setCellStyle(ratestyleobj)/>
								<cfset cell.setCellValue(javacast("double","#resultObj[cnt].usecount#"))>
							<cfelse>
								<cfset cell.setCellStyle(styleobj)/>
								<cfset cell.setCellValue("-")>
							</cfif>
								
							<!--- %行追加 --->
							<cfif arguments.outputtype eq 0>
								<!--- カテゴリ結合処理 --->
								<cfif len(resultObj[cnt].questionnairecategory_nm) eq 0>
									<cfset region = createObject("java","org.apache.poi.hssf.util.Region").init(javacast("int",rowseq-1),0,javacast("int",rowseq),0)/>
									<cfset sheet.addMergedRegion(region)/>
								</cfif>
								
								<cfset rowseq = rowseq+1>
								<cfset row = sheet.createRow(javacast("int",rowseq))/>

								<!--- 設問結合 --->
								<cfset cell = row.createCell(1)/>
								<cfset cell.setCellStyle(strobj)/>
								<cfset region = createObject("java","org.apache.poi.hssf.util.Region").init(javacast("int",rowseq-1),1,javacast("int",rowseq),1)/>
								<cfset sheet.addMergedRegion(region)/>

								<!--- サンプル数結合 --->
								<cfset cell = row.createCell(2)/>
								<cfset cell.setCellStyle(scoreobj)/>
								<cfset region = createObject("java","org.apache.poi.hssf.util.Region").init(javacast("int",rowseq-1),2,javacast("int",rowseq),2)/>
								<cfset sheet.addMergedRegion(region)/>

								<!--- スコア合計 --->
								<cfset cell = row.createCell(3)/>
								<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
								<cfset cell.setCellStyle(numstyleobj)/>
								<cfset cell.setCellValue(javacast("double","#resultObj[cnt].sum_score#"))>

								<!--- 平均スコア結合 --->
								<cfset cell = row.createCell(4)/>
								<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
								<cfset cell.setCellStyle(scoreobj)/>
								<cfset region = createObject("java","org.apache.poi.hssf.util.Region").init(javacast("int",rowseq-1),4,javacast("int",rowseq),4)/>
								<cfset sheet.addMergedRegion(region)/>

								<!--- ランク結合 --->
								<cfset rankstyleobj = wb.createCellStyle()/>
								<cfset rankstyleobj.setVerticalAlignment(rankstyleobj.VERTICAL_CENTER)/>
								<cfset rankstyleobj.setAlignment(rankstyleobj.ALIGN_CENTER)/>
								<cfset cell = row.createCell(5)/>
								<cfset cell.setCellStyle(rankstyleobj)/>
								<cfset region = createObject("java","org.apache.poi.hssf.util.Region").init(javacast("int",rowseq-1),5,javacast("int",rowseq),5)/>
								<cfset sheet.addMergedRegion(region)/>

								<!--- 割合 --->
								<cfset cell = row.createCell(6)/>
								<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
								<cfset cell.setCellValue("％")>
								<cfset cell.setCellStyle(styleobj)/>

								<!--- 割合１ --->
								<cfset cell = row.createCell(7)/>
								<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
								<cfset cell.setCellStyle(ratestyleobj)/>
								<cfset cell.setCellValue(javacast("double","#resultObj[cnt].result1_rate#"))>

								<!--- 割合２ --->
								<cfset cell = row.createCell(8)/>
								<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
								<cfset cell.setCellStyle(ratestyleobj)/>
								<cfset cell.setCellValue(javacast("double","#resultObj[cnt].result2_rate#"))>

								<!--- 割合３ --->
								<cfset cell = row.createCell(9)/>
								<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
								<cfset cell.setCellStyle(ratestyleobj)/>
								<cfset cell.setCellValue(javacast("double","#resultObj[cnt].result3_rate#"))>

								<!--- 割合４ --->
								<cfset cell = row.createCell(10)/>
								<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
								<cfset cell.setCellStyle(ratestyleobj)/>
								<cfset cell.setCellValue(javacast("double","#resultObj[cnt].result4_rate#"))>

								<!--- 割合５ --->
								<cfset cell = row.createCell(11)/>
								<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
								<cfset cell.setCellStyle(ratestyleobj)/>
								<cfset cell.setCellValue(javacast("double","#resultObj[cnt].result5_rate#"))>

								<!--- 割合６ --->
								<cfset cell = row.createCell(12)/>
								<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
								<cfset cell.setCellStyle(ratestyleobj)/>
								<cfset cell.setCellValue(javacast("double","#resultObj[cnt].result6_rate#"))>

								<!--- 割合７ --->
								<cfset cell = row.createCell(13)/>
								<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
								<cfset cell.setCellStyle(ratestyleobj)/>
								<cfset cell.setCellValue(javacast("double","#resultObj[cnt].result7_rate#"))>

								<!--- 不明 --->
								<cfset cell = row.createCell(14)/>
								<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
								<cfif isnumeric(resultObj[cnt].result0_rate)>
									<cfset cell.setCellStyle(ratestyleobj)/>
									<cfset cell.setCellValue(javacast("double","#resultObj[cnt].result0_rate#"))>
								</cfif>

								<!--- top1結合 --->
								<cfset cell = row.createCell(15)/>
								<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
								<cfset cell.setCellStyle(ratestyleobj)/>
								<cfset region = createObject("java","org.apache.poi.hssf.util.Region").init(javacast("int",rowseq-1),15,javacast("int",rowseq),15)/>
								<cfset sheet.addMergedRegion(region)/>									

								<!--- top2and3結合 --->
								<cfset cell = row.createCell(16)/>
								<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
								<cfset cell.setCellStyle(ratestyleobj)/>
								<cfset region = createObject("java","org.apache.poi.hssf.util.Region").init(javacast("int",rowseq-1),16,javacast("int",rowseq),16)/>
								<cfset sheet.addMergedRegion(region)/>

								<!--- nagetive結合 --->
								<cfset cell = row.createCell(17)/>
								<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
								<cfset cell.setCellStyle(ratestyleobj)/>
								<cfset region = createObject("java","org.apache.poi.hssf.util.Region").init(javacast("int",rowseq-1),17,javacast("int",rowseq),17)/>
								<cfset sheet.addMergedRegion(region)/>									

								<!--- 割合８ --->
								<cfset cell = row.createCell(18)/>
								<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
								<cfif isnumeric(resultObj[cnt].result8_rate)>
									<cfset cell.setCellStyle(ratestyleobj)/>
									<cfset cell.setCellValue(javacast("double","#resultObj[cnt].result8_rate#"))>
								<cfelse>
									<cfset cell.setCellStyle(styleobj)/>
									<cfset cell.setCellValue("-")>
								</cfif>

								<!--- 未利用率結合 --->
								<cfset cell = row.createCell(19)/>
								<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
								<cfset cell.setCellStyle(ratestyleobj)/>
								<cfset region = createObject("java","org.apache.poi.hssf.util.Region").init(javacast("int",rowseq-1),19,javacast("int",rowseq),19)/>
								<cfset sheet.addMergedRegion(region)/>									
								
							</cfif>
						</cfif>

						<!--- カテゴリ表示 --->
						<cfset cell = row.createCell(0)/>
						<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
						<cfset cell.setCellValue("#resultObj[cnt].questionnairecategory_nm#")>
						<cfset cell.setCellStyle(styleobj)/>

						<!--- カテゴリ結合処理 --->
						<cfif len(resultObj[cnt].questionnairecategory_nm) eq 0>
							<cfset region = createObject("java","org.apache.poi.hssf.util.Region").init(javacast("int",rowseq-1),0,javacast("int",rowseq),0)/>
							<cfset sheet.addMergedRegion(region)/>
						</cfif>
						<cfset rowseq = rowseq+1>
					</cfloop>
				</cfloop>


				<cfset filename = filePath & UserDir & datepart & "CRMResultsList.xls">
				<cfset fileOut = createObject("java","java.io.FileOutputStream").init("#filename#")/>
					
				<cfset wb.write(fileOut)/>
				<cfset fileOut.close()/>
				<!--- xls作成完了 --->
			</cfif>
			
			<!--- 戻値設定 --->
			<cfscript>
				executeinformation = StructNew();
				StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
				StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
				StructInsert(executeinformation,	"outputMsg",	"正常"						);
				StructInsert(executeinformation,	"outputPath",	#Application.outputDirectory# & #Replace(UserDir,"\","/","all")# & datepart & "CRMResultsList.xls" );
				StructInsert(executeinformation,	"filename",		"http://#cgi.server_name#/#Application.outputDirectory#" & #Replace(UserDir,"\","/","all")# & datepart & "CRMResultsList.xls" );
				StructInsert(executeinformation,	"newname",		"#Application.resortname##datepart#.xls" );
			</cfscript>
			<cfreturn executeinformation>
			
			<cfsetting enablecfoutputonly="No">
			<cfcatch type="any">
				<cflog file="csvdebug" text="#session.user_info.user_ip#:#cfcatch.message#:#cfcatch.detail#">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>

	</cffunction>
	<!---
	*************************************************************************************************
	* Class				: cstotal
	* Method			: getArea
	* Description		: エリアマスタ情報取得
	* Custom Attributes	: none
	* 					: 
	* Return Paramters	: retQuery(Query)
	* History			: 1) Coded by	2005/04/26 h.miyahara(NetFusion)
	*					: 2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getArea" access="remote" returntype="query" >
		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<cfquery name="retQuery" datasource = "#Application.datasource#">
				select	 area_code options_no
						,name options_label
				from	 rtm_area
				order by area_code
			</cfquery>
			<cfreturn retQuery>
			<cfsetting enablecfoutputonly="No">
			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	
	<!---
	*************************************************************************************************
	* Class				: cstotal
	* Method			: getTogether
	* Description		: 同行者マスタ情報取得
	* Custom Attributes	: none
	* 					: 
	* Return Paramters	: retQuery(Query)
	* History			: 1) Coded by	2005/04/26 h.miyahara(NetFusion)
	*					: 2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getTogether" access="remote" returntype="query" >
		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<cfquery name="retQuery" datasource = "#Application.datasource#">
				select	 control_id options_no
						,control_nm options_label
				from	 #Application.dbu#.mst_control
				where	 controldiv_id = #Application.c_together_id#
				and		 active_flg = 1
				order by seq
			</cfquery>
			<cfreturn retQuery>
			<cfsetting enablecfoutputonly="No">
			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>

	</cffunction>

	<!---
	*************************************************************************************************
	* Class				: cstotal
	* Method			: getGender
	* Description		: 性別マスタ情報取得
	* Custom Attributes	: none
	* 					: 
	* Return Paramters	: retQuery(Query)
	* History			: 1) Coded by	2005/04/26 h.miyahara(NetFusion)
	*					: 2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getGender" access="remote" returntype="query" >
		<cfsetting enablecfoutputonly="yes">
		<cftry>

			<cfquery name="retQuery" datasource = "#Application.datasource#">
				select
					options_no,
					options_label
				from
					#Application.dbu#.mst_answeroptions
				where
					questionnaire_id = #Application.q_gender_id#
				and
					active_flg = 1
				order by
					options_no
			</cfquery>

			<cfreturn retQuery>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>

	</cffunction>

	<!---
	*************************************************************************************************
	* Class				: cstotal
	* Method			: getEra
	* Description		: 年代マスタ情報取得
	* Custom Attributes	: none
	* 					: 
	* Return Paramters	: retQuery(Query)
	* History			: 1) Coded by	2005/04/26 h.miyahara(NetFusion)
	*					: 2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getEra" access="remote" returntype="query" >
		<cfsetting enablecfoutputonly="yes">
		<cftry>

			<cfquery name="retQuery" datasource = "#Application.datasource#">
				select
					options_no,
					options_label
				from
					#Application.dbu#.mst_answeroptions
				where
					questionnaire_id = #Application.q_era_id#
				and
					active_flg = 1
				order by
					options_no
			</cfquery>

			<cfreturn retQuery>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>

	</cffunction>


	<!---
	*************************************************************************************************
	* Class				: cstotal
	* Method			: getExperienced
	* Description		: 利用経験有無マスタ情報取得
	* Custom Attributes	: none
	* 					: 
	* Return Paramters	: retQuery(Query)
	* History			: 1) Coded by	2005/04/26 h.miyahara(NetFusion)
	*					: 2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getExperienced" access="remote" returntype="query" >
		<cfsetting enablecfoutputonly="yes">
		<cftry>

			<cfquery name="retQuery" datasource = "#Application.datasource#">
				select
					options_no,
					options_label
				from
					#Application.dbu#.mst_answeroptions
				where
					questionnaire_id = #Application.q_experienced_id#
				and
					active_flg = 1
				order by
					options_no
			</cfquery>

			<cfreturn retQuery>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>

	</cffunction>

	<!---
	*************************************************************************************************
	* Class				: cstotal
	* Method			: getStay
	* Description		: 宿泊ホテルマスタ情報取得
	* Custom Attributes	: none
	* 					: 
	* Return Paramters	: retQuery(Query)
	* History			: 1) Coded by	2005/08/14 h.miyahara(NetFusion)
	*					: 2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getStay" access="remote" returntype="query" >
		<cfsetting enablecfoutputonly="yes">
		<cftry>

			<cfquery name="retQuery" datasource = "#Application.datasource#">
				select
					options_no,
					options_label
				from
					#Application.dbu#.mst_answeroptions
				where
					questionnaire_id = #Application.q_stay_id#
				and
					active_flg = 1
 				order by
					options_no
			</cfquery>

			<cfreturn retQuery>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>

	</cffunction>
	<!---
	*************************************************************************************************
	* Class				: conditionsetting_dataselectwork
	* Method			: getcontrolitems
	* Description		: コントロールマスタ情報取得
	* Custom Attributes	: none
	* 					: 
	* Return Paramters	: retQuery(Query)
	* History			: 1) Coded by	2005/04/26 h.miyahara(NetFusion)
	*					: 2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getcontrolitems" access="remote" returntype="query" >
		<cfargument name="controldiv_id" type="numeric" default="">
		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<cfquery name="retQuery" datasource = "#Application.datasource#">
				select	 control_id
						,control_nm
				from	 mst_control
				where	 controldiv_id = #arguments.controldiv_id#
				order by seq
			</cfquery>
			<cfreturn retQuery>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	<!---
	*************************************************************************************************
	* Class				: cstotal
	* Method			: getroomnumber
	* Description		: 部屋番号マスタ情報取得
	* Custom Attributes	: none
	* 					: 
	* Return Paramters	: retQuery(Query)
	* History			: 1) Coded by	2005/06/27 h.miyahara(NetFusion)
	*					: 2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getroomnumber" access="remote" returntype="query" >
		<cfargument name="roomtype" type="numeric" default="">
		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<cfquery name="retQuery" datasource = "#Application.datasource#">
				select	 mr.roomnumber
						,mc.control_nm roomtype
				from	 mst_room mr
						,mst_control mc
				where	 mr.roomtype_id = mc.control_id
				<cfif arguments.roomtype NEQ 0>
				and		 mc.control_id = #arguments.roomtype#
				</cfif>
				order by mc.seq
						,mr.seq
			</cfquery>
			<cfreturn retQuery>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<!---
	*************************************************************************************************
	* Class				:cstotal
	* Method			:getAnswers
	* Description		:回答値取得
	* Custom Attributes	:questionnaire_id	設問ID
	* Return Paramters	:
	* History			:1) Coded by	2004/10/13 h.miyahara(NetFusion)
	*					:2) Updated by	2005/04/26 h.miyahara(NetFusion) MOD	検索条件追加
	*					:3) Updated by	2005/07/20 h.miyahara(NetFusion) MOD	平均スコア算出不正対応
	*************************************************************************************************
	--->
	<cffunction name="getAnswers" access="remote" returntype="any">
		<cfargument name="dcondtion" type="Boolean" default="false">
		<cfargument name="selecteddays" type="Array" default="">

		<cfargument name="arg_category_id" type="Numeric" default="">
		<cfargument name="arg_cross_id" type="Numeric" default="0">

		<cfargument name="arg_group_id" type="Numeric" default="0">	<!--- 団体（0=全部、1=個人、2=団体 'zzz*****'）--->
		<cfargument name="arg_resort_code" type="Any" default="">

		<cfargument name="arg_generation" type="Any" default="">	<!--- 年代 --->
		<cfargument name="arg_gender" type="Any" default="">		<!--- 性別 --->
		<cfargument name="arg_job" type="Any" default="">			<!--- 職業 --->
		<cfargument name="arg_times" type="Any" default="">			<!--- 旅行回数 --->

		<cfargument name="arg_nights" type="Any" default="">		<!--- 宿泊数 --->		
		<cfargument name="arg_together" type="Any" default="">		<!--- 同行者 --->
		<cfargument name="arg_area" type="Any" default="">			<!--- 居住県 --->
		<cfargument name="arg_roomtype" type="Any" default="">		<!--- ルームタイプ --->
		<cfargument name="arg_purpose" type="Any" default="">		<!--- 旅行目的 --->
		<cfargument name="arg_reservation" type="Any" default="">	<!--- 予約経路 --->
		<cfargument name="arg_cognition" type="Any" default="">		<!--- 認知経路 --->
		<cfargument name="arg_roomnumber" type="Any" default="">	<!--- ルーム№ --->
		<cfsetting enablecfoutputonly="yes">
	
		<cftry>
		
		<!--- 戻値クエリ作成 --->
		<cfset retQuery = QueryNew("
			questionnaire_id, 
			group_id, 
			order_id, 
			samples, 
			average, 
			rank, 
			result1_samples, 
			result2_samples, 
			result3_samples, 
			result4_samples, 
			result5_samples, 
			result6_samples, 
			result7_samples, 
			result8_samples, 
			result0_samples, 
			result1_average_txt, 
			result2_average_txt, 
			result3_average_txt, 
			result4_average_txt, 
			result5_average_txt, 
			result6_average_txt, 
			result7_average_txt, 
			result8_average_txt, 
			result0_average_txt,
			usecount_txt,
			effective_n,
			sum_score,
			top1,
			top2and3,
			nagetive")>

		<!--- 戻値クエリ作成 --->
		<cfset getQrecords = QueryNew("
									group_id, 
									order_id, 
									q_id,
									category_id,
									viewtype,
									smp0,
									smp1,
									total,
									asw1,
									asw2,
									asw3,
									asw4,
									asw5,
									asw6,
									asw7,
									asw8,
									asw0")>

		<!--- 共通日時抽出条件切替 --->
		<cfinvoke component = "conditionsetting_dataselectwork" method = "datecondition" returnVariable ="dcondition">
			<cfinvokeargument name="dcondtion" value = #arguments.dcondtion#>
			<cfinvokeargument name="selecteddays" value = #arguments.selecteddays#>
		</cfinvoke>
		
		<cfif (arraylen(arguments.arg_resort_code) IS 1) and (arguments.arg_resort_code[1] IS 0)>
			<cfset sqlStr = " ">
			<cfset wk_resortcode = "">
		<cfelse>
			<cfset tmp_list = ArrayToList(arguments.arg_resort_code)>
			<cfset tmp_cmd = "">
			<cfloop from="1" to="#ListLen(tmp_list)#" index="i">
				<cfset tmp_cmd = ListAppend(tmp_cmd, arguments.arg_resort_code[i])>
			</cfloop>
			<cfset wk_resortcode = tmp_cmd>

			<cfset sqlStr = " (resort_code in (#tmp_cmd#)) and ">
		</cfif>

		<!--- シートID取得 --->
		<cfquery name="getSheet_id" datasource = "#Application.datasource#">
			select top 1 
				questionnairesheet_id as sheet_id
			from 
				#Application.app00.dbu#.mst_questionnairesheet 
			where 
				#sqlStr#
				start_date <= getdate() and 
				active_flg = 1
			order by 
				start_date desc
		</cfquery>

		<!--- 共通集計条件絞込み処理 --->
		<cfinvoke 
			component = "conditionsetting_dataselectwork" 
			method = "sqlMake1"
			temp_cond = #arguments#
			arg_sheet_id = 0
			returnVariable = "strSQL1">

		<!--- 共通集計条件絞込み処理 --->
		<cfinvoke 
			component = "conditionsetting_dataselectwork" 
			method = "sqlMake2"
			temp_cond = #arguments#
			check_resort_code = #wk_resortcode#
			returnVariable = "strSQL2">

		<!--- 抽出対象期間より期間内の回答者予約番号を抽出 --->
		<cfquery name="getAnswers" datasource = "#Application.datasource#">
			select distinct
				referencenumber,
				group_id,
				questionnaire_id,
				answer
			from 
				#Application.app00.dbu#.v_getAnswers
			where 
				#PreserveSingleQuotes(dcondition)#  
				#PreserveSingleQuotes(strSQL1)#
				#PreserveSingleQuotes(strSQL2)#
			<!--- 2008/07/16 リピーターCSは除外 --->
			and questionnairesheet_id  != 0
		</cfquery>

		<cfquery name="getAnswers_smp0" dbtype = "query">
			select 
				referencenumber,
				group_id,
				questionnaire_id,
				answer
			from 
				getAnswers
		</cfquery>
		<cfquery name="getAnswers_smp1" dbtype = "query">
			select 
				referencenumber,
				group_id,
				questionnaire_id,
				answer
			from 
				getAnswers_smp0
			where 
				answer<>0 
		</cfquery>
		<cfquery name="getAnswers_total" dbtype = "query">
			select 
				referencenumber,
				group_id,
				questionnaire_id,
				answer
			from 
				getAnswers_smp1
			where 
				answer in (1,2,3,4,5,6,7,8) 
			<!--- 
				answer in (1,2,3,4,5,6,7) 
			 --->
		</cfquery>
		<cfquery name="getAnswers_asw0" dbtype = "query">
			select 
				referencenumber,
				group_id,
				questionnaire_id
			from 
				getAnswers_smp0
			where 
				answer not in (1,2,3,4,5,6,7,8) 
		</cfquery>
		<cfquery name="getAnswers_asw1" dbtype = "query">
			select 
				referencenumber,
				group_id,
				questionnaire_id
			from 
				getAnswers_total
			where 
				answer = 1 
		</cfquery>
		<cfquery name="getAnswers_asw2" dbtype = "query">
			select 
				referencenumber,
				group_id,
				questionnaire_id
			from 
				getAnswers_total
			where 
				answer = 2 
		</cfquery>
		<cfquery name="getAnswers_asw3" dbtype = "query">
			select 
				referencenumber,
				group_id,
				questionnaire_id
			from 
				getAnswers_total
			where 
				answer = 3 
		</cfquery>
		<cfquery name="getAnswers_asw4" dbtype = "query">
			select 
				referencenumber,
				group_id,
				questionnaire_id
			from 
				getAnswers_total
			where 
				answer = 4 
		</cfquery>
		<cfquery name="getAnswers_asw5" dbtype = "query">
			select 
				referencenumber,
				group_id,
				questionnaire_id
			from 
				getAnswers_total
			where 
				answer = 5 
		</cfquery>
		<cfquery name="getAnswers_asw6" dbtype = "query">
			select 
				referencenumber,
				group_id,
				questionnaire_id
			from 
				getAnswers_total
			where 
				answer = 6 
		</cfquery>
		<cfquery name="getAnswers_asw7" dbtype = "query">
			select 
				referencenumber,
				group_id,
				questionnaire_id
			from 
				getAnswers_total
			where 
				answer = 7 
		</cfquery>
		<cfquery name="getAnswers_asw8" dbtype = "query">
			select 
				referencenumber,
				group_id,
				questionnaire_id
			from 
				getAnswers_smp1
			where 
				answer = 8 
		</cfquery>

		<!--- 対象者無し --->
		<cfif getAnswers_smp0.recordcount EQ 0>
			<cfreturn retQuery>
		</cfif>

		<!--- 夕食 --->
		<cfif #arg_category_id# IS 2>

			<!--- 館内の食事処で：TOTAL --->
			<cfquery name="getCnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 800000001) and (answer=1))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset ans19_1_0_list = "'-1'">
			<cfelse>
				<cfset ans19_1_0_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

<!---			<cfif (#arg_resort_code# IS 0) or (#arg_resort_code# IS 8)> 	<!--- 古牧温泉 ---> --->

				<!--- バイキング/ビュッフェ --->
				<cfquery name="getCnt" dbtype = "query">
					select distinct referencenumber from getAnswers where ((questionnaire_id = 800100001) and (answer=3))
				</cfquery>
		
				<cfif #getCnt.recordcount# IS NOT 0>
					<cfset ans19_1_0_list = #ans19_1_0_list# & ", " & ListQualify(ValueList(getCnt.referencenumber),"'")>
				</cfif>

				<!--- 湯けむり茶屋 --->
				<cfquery name="getCnt" dbtype = "query">
					select distinct referencenumber from getAnswers where ((questionnaire_id = 800100001) and (answer=4))
				</cfquery>
		
				<cfif #getCnt.recordcount# IS NOT 0>
					<cfset ans19_1_0_list = #ans19_1_0_list# & ", " & ListQualify(ValueList(getCnt.referencenumber),"'")>
				</cfif>

				<!--- 和食膳 --->
				<cfquery name="getCnt" dbtype = "query">
					select distinct referencenumber from getAnswers where ((questionnaire_id = 800100001) and (answer=5))
				</cfquery>
		
				<cfif #getCnt.recordcount# IS NOT 0>
					<cfset ans19_1_0_list = #ans19_1_0_list# & ", " & ListQualify(ValueList(getCnt.referencenumber),"'")>
				</cfif>

				<!--- 「みちのく祭りや」 --->
				<cfquery name="getCnt" dbtype = "query">
					select distinct referencenumber from getAnswers where ((questionnaire_id = 800100001) and (answer=6))
				</cfquery>

				<cfif #getCnt.recordcount# IS NOT 0>
					<cfset ans19_1_0_list = #ans19_1_0_list# & ", " & ListQualify(ValueList(getCnt.referencenumber),"'")>
				</cfif>

				<!--- 宴会和食膳 --->
				<cfquery name="getCnt" dbtype = "query">
					select distinct referencenumber from getAnswers where ((questionnaire_id = 800100001) and (answer=7))
				</cfquery>

				<cfif #getCnt.recordcount# IS NOT 0>
					<cfset ans19_1_0_list = #ans19_1_0_list# & ", " & ListQualify(ValueList(getCnt.referencenumber),"'")>
				</cfif>

				<!--- 宴会パーティ --->
				<cfquery name="getCnt" dbtype = "query">
					select distinct referencenumber from getAnswers where ((questionnaire_id = 800100001) and (answer=8))
				</cfquery>

				<cfif #getCnt.recordcount# IS NOT 0>
					<cfset ans19_1_0_list = #ans19_1_0_list# & ", " & ListQualify(ValueList(getCnt.referencenumber),"'")>
				</cfif>


<!---			</cfif> --->
			
			<cfset num=evaluate('ans19_1_0_list')>
			<cfset strWhere = " and referencenumber IN (#PreserveSingleQuotes(num)#) ">

			<!--- 館内の食事処で：和食 --->
			<cfquery name="getCnt" dbtype = "query">
				select distinct referencenumber from getAnswers 
				where ((questionnaire_id = 800100001) and (answer=1)) #PreserveSingleQuotes(strWhere)#
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset ans19_1_1_list = "'-1'">
			<cfelse>
				<cfset ans19_1_1_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 館内の食事処で：洋食 --->
			<cfquery name="getCnt" dbtype = "query">
				select distinct referencenumber from getAnswers 
				where ((questionnaire_id = 800100001) and (answer=2)) #PreserveSingleQuotes(strWhere)#
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset ans19_1_2_list = "'-1'">
			<cfelse>
				<cfset ans19_1_2_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 館内の食事処で：バイキング/ビュッフェ --->
			<cfquery name="getCnt" dbtype = "query">
				select distinct referencenumber from getAnswers 
				where ((questionnaire_id = 800100001) and (answer=3)) #PreserveSingleQuotes(strWhere)#
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset ans19_1_3_list = "'-1'">
			<cfelse>
				<cfset ans19_1_3_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 館内の食事処で：湯けむり茶屋 --->
			<cfquery name="getCnt" dbtype = "query">
				select distinct referencenumber from getAnswers 
				where ((questionnaire_id = 800100001) and (answer=4)) #PreserveSingleQuotes(strWhere)#
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset ans19_1_4_list = "'-1'">
			<cfelse>
				<cfset ans19_1_4_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 館内の食事処で：和食膳 --->
			<cfquery name="getCnt" dbtype = "query">
				select distinct referencenumber from getAnswers 
				where ((questionnaire_id = 800100001) and (answer=5)) #PreserveSingleQuotes(strWhere)#
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset ans19_1_5_list = "'-1'">
			<cfelse>
				<cfset ans19_1_5_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 館内の食事処で：「みちのく祭りや」 --->
			<cfquery name="getCnt" dbtype = "query">
				select distinct referencenumber from getAnswers 
				where ((questionnaire_id = 800100001) and (answer=6)) #PreserveSingleQuotes(strWhere)#
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset ans19_1_6_list = "'-1'">
			<cfelse>
				<cfset ans19_1_6_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 館内の食事処で：宴会和食膳 --->
			<cfquery name="getCnt" dbtype = "query">
				select distinct referencenumber from getAnswers 
				where ((questionnaire_id = 800100001) and (answer=7)) #PreserveSingleQuotes(strWhere)#
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset ans19_1_7_list = "'-1'">
			<cfelse>
				<cfset ans19_1_7_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 館内の食事処で：宴会パーティ --->
			<cfquery name="getCnt" dbtype = "query">
				select distinct referencenumber from getAnswers 
				where ((questionnaire_id = 800100001) and (answer=8)) #PreserveSingleQuotes(strWhere)#
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset ans19_1_8_list = "'-1'">
			<cfelse>
				<cfset ans19_1_8_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 館内の食事処で：その他 --->
			<cfquery name="getCnt" dbtype = "query">
				select distinct referencenumber from getAnswers 
				where ((questionnaire_id = 800100001) and (answer=99)) #PreserveSingleQuotes(strWhere)#
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset ans19_1_99_list = "'-1'">
			<cfelse>
				<cfset ans19_1_99_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 当館と提携しているレストランで：TOTAL --->
			<cfquery name="getCnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 800000001) and (answer=2))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset ans19_2_0_list = "'-1'">
			<cfelse>
				<cfset ans19_2_0_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<cfset num=evaluate('ans19_2_0_list')>
			<cfset strWhere = " and referencenumber IN (#PreserveSingleQuotes(num)#) ">

			<!--- 当館と提携しているレストランで：和食 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers 
				where ((questionnaire_id = 800100001) and (answer=1)) #PreserveSingleQuotes(strWhere)#
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset ans19_2_1_list = "'-1'">
			<cfelse>
				<cfset ans19_2_1_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 当館と提携しているレストランで：洋食 --->
			<cfquery name="getCnt" dbtype = "query">
				select distinct referencenumber from getAnswers 
				where ((questionnaire_id = 800100001) and (answer=2)) #PreserveSingleQuotes(strWhere)#
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset ans19_2_2_list = "'-1'">
			<cfelse>
				<cfset ans19_2_2_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 当館と提携しているレストランで：バイキング・ブッフェ --->
			<cfquery name="getCnt" dbtype = "query">
				select distinct referencenumber from getAnswers 
				where ((questionnaire_id = 800100001) and (answer=3)) #PreserveSingleQuotes(strWhere)#
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset ans19_2_3_list = "'-1'">
			<cfelse>
				<cfset ans19_2_3_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 当館と提携しているレストランで：その他 --->
			<cfquery name="getCnt" dbtype = "query">
				select distinct referencenumber from getAnswers 
				where ((questionnaire_id = 800100001) and (answer=99)) #PreserveSingleQuotes(strWhere)#
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset ans19_2_99_list = "'-1'">
			<cfelse>
				<cfset ans19_2_99_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>
<!---
		</cfif>

		<!--- 朝食 --->
		<cfif #arg_category_id# IS 3>
--->
			<!--- 和食 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 900000001) and (answer=1))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset ans20_1_1_list = "'-1'">
			<cfelse>
				<cfset ans20_1_1_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 洋食 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 900000001) and (answer=2))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset ans20_1_2_list = "'-1'">
			<cfelse>
				<cfset ans20_1_2_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>
	
			<!--- バイキング/ビュッフェ --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 900000001) and (answer=3))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset ans20_1_3_list = "'-1'">
			<cfelse>
				<cfset ans20_1_3_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>
	
			<!--- その他 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 900000001) and (answer=99))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset ans20_1_99_list = "'-1'">
			<cfelse>
				<cfset ans20_1_99_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

		</cfif>


		<!--- 基本クロス：性別 --->
		<cfif #arg_cross_id# IS 1>

			<!--- 男性 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000001) and (answer=1))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross1_1_list = "'-1'">
			<cfelse>
				<cfset cross1_1_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>
	
			<!--- 女性 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000001) and (answer=2))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross1_2_list = "'-1'">
			<cfelse>
				<cfset cross1_2_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

		</cfif>

		<!--- 基本クロス：未既婚 --->
		<cfif #arg_cross_id# IS 2>

			<!--- 有り --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000002) and (answer=1))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross2_1_list = "'-1'">
			<cfelse>
				<cfset cross2_1_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>
	
			<!--- なし --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000002) and (answer=2))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross2_2_list = "'-1'">
			<cfelse>
				<cfset cross2_2_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>
	
			<!--- その他 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000002) and (answer=99))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross2_99_list = "'-1'">
			<cfelse>
				<cfset cross2_99_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

		</cfif>

		<!--- 基本クロス：年代 --->
		<cfif #arg_cross_id# IS 3>

			<!--- 10代 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000003) and (answer=1))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross3_1_list = "'-1'">
			<cfelse>
				<cfset cross3_1_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 20代 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000003) and (answer=2))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross3_2_list = "'-1'">
			<cfelse>
				<cfset cross3_2_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 30代 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000003) and (answer=3))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross3_3_list = "'-1'">
			<cfelse>
				<cfset cross3_3_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 40代 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000003) and (answer=4))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross3_4_list = "'-1'">
			<cfelse>
				<cfset cross3_4_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 50代 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000003) and (answer=5))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross3_5_list = "'-1'">
			<cfelse>
				<cfset cross3_5_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 60代 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000003) and (answer=6))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross3_6_list = "'-1'">
			<cfelse>
				<cfset cross3_6_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 70代以上 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000003) and (answer=7))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross3_7_list = "'-1'">
			<cfelse>
				<cfset cross3_7_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

		</cfif>

		<!--- 基本クロス：職業 --->
		<cfif #arg_cross_id# IS 5>

			<!--- 会社員 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000005) and (answer=1))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross5_1_list = "'-1'">
			<cfelse>
				<cfset cross5_1_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 会社役員 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000005) and (answer=2))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross5_2_list = "'-1'">
			<cfelse>
				<cfset cross5_2_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 自営/自由業 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000005) and (answer=3))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross5_3_list = "'-1'">
			<cfelse>
				<cfset cross5_3_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 専門職（医師/弁護士など） --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000005) and (answer=4))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross5_4_list = "'-1'">
			<cfelse>
				<cfset cross5_4_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 公務員 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000005) and (answer=5))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross5_5_list = "'-1'">
			<cfelse>
				<cfset cross5_5_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- パートアルバイト --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000005) and (answer=6))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross5_6_list = "'-1'">
			<cfelse>
				<cfset cross5_6_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 学生 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000005) and (answer=7))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross5_7_list = "'-1'">
			<cfelse>
				<cfset cross5_7_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 専業主婦 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000005) and (answer=8))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross5_8_list = "'-1'">
			<cfelse>
				<cfset cross5_8_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 働いていない --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000005) and (answer=9))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross5_9_list = "'-1'">
			<cfelse>
				<cfset cross5_9_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- その他 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000005) and (answer=99))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross5_99_list = "'-1'">
			<cfelse>
				<cfset cross5_99_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

		</cfif>

		<!--- 基本クロス：旅行回数 --->
		<cfif #arg_cross_id# IS 6>

			<!--- 0回 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000006) and (answer=1))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross6_1_list = "'-1'">
			<cfelse>
				<cfset cross6_1_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 1回 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000006) and (answer=2))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross6_2_list = "'-1'">
			<cfelse>
				<cfset cross6_2_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 2～3回 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000006) and (answer=3))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross6_3_list = "'-1'">
			<cfelse>
				<cfset cross6_3_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 4～5回 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000006) and (answer=4))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross6_4_list = "'-1'">
			<cfelse>
				<cfset cross6_4_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 6回以上 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000006) and (answer=5))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross6_5_list = "'-1'">
			<cfelse>
				<cfset cross6_5_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 10回以上 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000006) and (answer=6))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross6_6_list = "'-1'">
			<cfelse>
				<cfset cross6_6_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 忘れた --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000006) and (answer=7))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross6_7_list = "'-1'">
			<cfelse>
				<cfset cross6_7_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

		</cfif>

		<cfinvoke component = "cstotal_dataselectwork" method = "getEnquete"
			arg_resort_code=#arg_resort_code#
			arg_category_id=#arg_category_id#
			arg_cross_id=#arg_cross_id#
			returnVariable	="v_getEnquete">

		<cfloop query="v_getEnquete">

			<cfif #v_getEnquete.options_no[v_getEnquete.currentrow]# eq 0>
			
				<cfset strWhere = "group_id=#v_getEnquete.group_id[v_getEnquete.currentrow]# and 
				   questionnaire_id=#v_getEnquete.questionnaire_id[v_getEnquete.currentrow]# ">

				<!--- 食事 --->
				<cfif #arg_category_id# IS 2>
					<cfset strWhere = "questionnaire_id=#v_getEnquete.questionnaire_id[v_getEnquete.currentrow]# ">
				</cfif>

			<cfelse>

				<cfset strWhere = "questionnaire_id=#v_getEnquete.questionnaire_id[v_getEnquete.currentrow]# ">

				<!--- 食事 --->
				<cfif #arg_category_id# IS 2>
					<cfset num=evaluate('ans' & #v_getEnquete.category_id[v_getEnquete.currentrow]# & '_' & 
												#v_getEnquete.options_no[v_getEnquete.currentrow]# & '_' & 
												#v_getEnquete.c_options_no[v_getEnquete.currentrow]# & '_list')>
					<cfset strWhere = strWhere & " and referencenumber IN (#PreserveSingleQuotes(num)#) ">
				</cfif>

			</cfif>

			<!--- 基本クロス --->
			<cfif #arg_cross_id# GREATER THAN 0>
				<cfif #v_getEnquete.cross_options_no[v_getEnquete.currentrow]# GREATER THAN 0>
					<cfset num=evaluate('cross' & #arg_cross_id# & '_' & 
							#v_getEnquete.cross_options_no[v_getEnquete.currentrow]# & '_list')>
					<cfset strWhere = strWhere & " and referencenumber IN (#PreserveSingleQuotes(num)#) ">
				</cfif>
			</cfif>

			<cfset QueryAddRow(getQrecords, 1)>
			<cfset QuerySetCell(getQrecords, "group_id", #v_getEnquete.group_id[v_getEnquete.currentrow]#)>
<!--- 
			<cfset QuerySetCell(getQrecords, "order_id", #v_getEnquete.order_id[v_getEnquete.currentrow]#)>
 --->				
			<cfset QuerySetCell(getQrecords, "order_id", #v_getEnquete.viewcell_id[v_getEnquete.currentrow]#)>
				
				
			<cfset QuerySetCell(getQrecords, "q_id", #v_getEnquete.questionnaire_id[v_getEnquete.currentrow]#)>
			<cfset QuerySetCell(getQrecords, "category_id", #v_getEnquete.category_id[v_getEnquete.currentrow]#)>
			<cfset QuerySetCell(getQrecords, "viewtype", #v_getEnquete.viewtype[v_getEnquete.currentrow]#)>

			<cfquery name="getCnt" dbtype = "query">

				select 
					count(*) as smp0, 0 as smp1, 0 as total,0 as asw0,
					0 as asw1,        0 as asw2, 0 as asw3, 0 as asw4,
					0 as asw5,        0 as asw6, 0 as asw7, 0 as asw8
				from 
					getAnswers_smp0 
				where 
					#PreserveSingleQuotes(strWhere)#

				union select 
					0 as smp0, count(*) as smp1, 0 as total, 0 as asw0,
					0 as asw1, 0 as asw2,        0 as asw3,  0 as asw4,
					0 as asw5, 0 as asw6,        0 as asw7,  0 as asw8
				from 
					getAnswers_smp1 
				where 
					#PreserveSingleQuotes(strWhere)#

				union select 
					0 as smp0, 0 as smp1, count(*) as total, 0 as asw0,
					0 as asw1, 0 as asw2, 0 as asw3,         0 as asw4,
					0 as asw5, 0 as asw6, 0 as asw7,         0 as asw8
				from 
					getAnswers_total 
				where 
					#PreserveSingleQuotes(strWhere)#

				union select 
					0 as smp0, 0 as smp1, 0 as total, count(*) as asw0,
					0 as asw1, 0 as asw2, 0 as asw3,  0 as asw4,
					0 as asw5, 0 as asw6, 0 as asw7,  0 as asw8
				from 
					getAnswers_asw0
				where 
					#PreserveSingleQuotes(strWhere)#

				union select 
					0 as smp0,        0 as smp1, 0 as total, 0 as asw0,
					count(*) as asw1, 0 as asw2, 0 as asw3,  0 as asw4,
					0 as asw5,        0 as asw6, 0 as asw7,  0 as asw8
				from 
					getAnswers_asw1
				where 
					#PreserveSingleQuotes(strWhere)#

				union select 
					0 as smp0, 0 as smp1,        0 as total, 0 as asw0,
					0 as asw1, count(*) as asw2, 0 as asw3,  0 as asw4,
					0 as asw5, 0 as asw6,        0 as asw7,  0 as asw8
				from 
					getAnswers_asw2
				where 
					#PreserveSingleQuotes(strWhere)#

				union select 
					0 as smp0, 0 as smp1, 0 as total,       0 as asw0,
					0 as asw1, 0 as asw2, count(*) as asw3, 0 as asw4,
					0 as asw5, 0 as asw6, 0 as asw7,        0 as asw8
				from 
					getAnswers_asw3 
				where 
					#PreserveSingleQuotes(strWhere)#

				union select 
					0 as smp0, 0 as smp1, 0 as total, 0 as asw0,
					0 as asw1, 0 as asw2, 0 as asw3,  count(*) as asw4,
					0 as asw5, 0 as asw6, 0 as asw7,  0 as asw8
				from 
					getAnswers_asw4
				where 
					#PreserveSingleQuotes(strWhere)#

				union select 
					0 as smp0,        0 as smp1, 0 as total, 0 as asw0,
					0 as asw1,        0 as asw2, 0 as asw3,  0 as asw4,
					count(*) as asw5, 0 as asw6, 0 as asw7,  0 as asw8
				from 
					getAnswers_asw5
				where 
					#PreserveSingleQuotes(strWhere)#

				union select 
					0 as smp0, 0 as smp1,        0 as total, 0 as asw0,
					0 as asw1, 0 as asw2,        0 as asw3,  0 as asw4,
					0 as asw5, count(*) as asw6, 0 as asw7,  0 as asw8
				from 
					getAnswers_asw6
				where 
					#PreserveSingleQuotes(strWhere)#

				union select 
					0 as smp0, 0 as smp1, 0 as total,       0 as asw0,
					0 as asw1, 0 as asw2, 0 as asw3,        0 as asw4,
					0 as asw5, 0 as asw6, count(*) as asw7, 0 as asw8
				from 
					getAnswers_asw7
				where 
					#PreserveSingleQuotes(strWhere)#

				union select 
					0 as smp0, 0 as smp1, 0 as total, 0 as asw0,
					0 as asw1, 0 as asw2, 0 as asw3,  0 as asw4,
					0 as asw5, 0 as asw6, 0 as asw7,  count(*) as asw8
				from 
					getAnswers_asw8
				where 
					#PreserveSingleQuotes(strWhere)#

			</cfquery>
				
			<!--- ドリルダウン用 2008/05/14 Miya --->	
			<cfif isdefined("arguments.questionnaire_id") and questionnaire_id eq arguments.questionnaire_id>
				<cfquery name="getdetail" datasource = "#Application.datasource#">
					select distinct
						 customernumber
						,convert(varchar,depart_date,111) as depart_date
						,convert(varchar,arrive_date,111) as arrive_date
					from	 #Application.app00.dbu#.v_getAnswers
					where
							 #PreserveSingleQuotes(dcondition)#  
							 #PreserveSingleQuotes(strSQL1)#
							 #PreserveSingleQuotes(strSQL2)#
					and		 questionnaire_id = #arguments.questionnaire_id# 
					and		 answer = #arguments.answer#
					order by depart_date desc
				</cfquery>
				<cfreturn getdetail>
			</cfif>	
				
			<!--- ドリルダウン用 2008/05/14 Miya --->
			<cfset QuerySetCell(getQrecords, "smp0", 0)>
			<cfset QuerySetCell(getQrecords, "smp1", 0)>
			<cfset QuerySetCell(getQrecords, "total", 0)>
			<cfset QuerySetCell(getQrecords, "asw0", 0)>
			<cfset QuerySetCell(getQrecords, "asw1", 0)>
			<cfset QuerySetCell(getQrecords, "asw2", 0)>
			<cfset QuerySetCell(getQrecords, "asw3", 0)>
			<cfset QuerySetCell(getQrecords, "asw4", 0)>
			<cfset QuerySetCell(getQrecords, "asw5", 0)>
			<cfset QuerySetCell(getQrecords, "asw6", 0)>
			<cfset QuerySetCell(getQrecords, "asw7", 0)>

			<cfif (arraylen(arguments.arg_resort_code) IS 1) and (arguments.arg_resort_code[1] GREATER THAN 0)>

				<!--- リゾートコードの指定あり --->
				<cfquery name="getAnswers8" datasource = "#Application.datasource#">
					select
						resort_code, 
						count(resort_code) AS answ8
					from 
						oth_answeroptions
					where
							(questionnairesheet_id = #v_getEnquete.questionnairesheet_id[v_getEnquete.currentrow]#) 
						AND (questionnaire_id = #v_getEnquete.questionnaire_id[v_getEnquete.currentrow]#) 
						AND (resort_code = #arguments.arg_resort_code[1]#)
					group by
						resort_code
					having
						(COUNT(resort_code) = 8)
				</cfquery>
	
			<cfelseif (arraylen(arguments.arg_resort_code) IS 1) and (arguments.arg_resort_code[1] IS 0)>
	
				<!--- リゾートコードの指定なし --->
				<cfquery name="getAnswers8" datasource = "#Application.datasource#">
					select
						resort_code, 
						count(resort_code) AS answ8
					from 
						oth_answeroptions
					where
							(questionnairesheet_id = #v_getEnquete.questionnairesheet_id[v_getEnquete.currentrow]#) 
						AND (questionnaire_id = #v_getEnquete.questionnaire_id[v_getEnquete.currentrow]#) 
						AND (resort_code > #arguments.arg_resort_code[1]#)
					group by
						resort_code
					having
						(COUNT(resort_code) = 8)
				</cfquery>
	
			<cfelse>

				<cfset tmp_list = ArrayToList(arguments.arg_resort_code)>
				<cfset tmp_cmd = "">
				<cfloop from="1" to="#ListLen(tmp_list)#" index="i">
					<cfset tmp_cmd = ListAppend(tmp_cmd, arguments.arg_resort_code[i])>
				</cfloop>

				<cfset strSQL = " and (resort_code in (#tmp_cmd#)) ">
	
				<!--- リゾートコードの複数指定 --->
				<cfquery name="getAnswers8" datasource = "#Application.datasource#">
					select
						resort_code, 
						count(resort_code) AS answ8
					from 
						oth_answeroptions
					where
							(questionnairesheet_id = #v_getEnquete.questionnairesheet_id[v_getEnquete.currentrow]#) 
						AND (questionnaire_id = #v_getEnquete.questionnaire_id[v_getEnquete.currentrow]#) 
						#strSQL#
					group by
						resort_code
					having
						(COUNT(resort_code) = 8)
				</cfquery>
	
			</cfif>

			<cfif #getAnswers8.recordcount# GREATER THAN 0>
				<cfset QuerySetCell(getQrecords, "asw8", 0)>
			</cfif>

			<cfif #getCnt.recordcount# neq 0>
				<cfloop query="getCnt">

					<cfif #getCnt.smp0[getCnt.currentrow]# gt 0>
						<cfset QuerySetCell(getQrecords, "smp0", #getCnt.smp0[getCnt.currentrow]#)>
					</cfif>

					<cfif #getCnt.smp1[getCnt.currentrow]# gt 0>
						<cfset QuerySetCell(getQrecords, "smp1", #getCnt.smp1[getCnt.currentrow]#)>
					</cfif>

					<cfif #getCnt.total[getCnt.currentrow]# gt 0>
						<cfset QuerySetCell(getQrecords, "total", #getCnt.total[getCnt.currentrow]#)>
					</cfif>

					<cfif #getCnt.asw0[getCnt.currentrow]# gt 0>
						<cfset QuerySetCell(getQrecords, "asw0", #getCnt.asw0[getCnt.currentrow]#)>
					</cfif>

					<cfif #getCnt.asw1[getCnt.currentrow]# gt 0>
						<cfset QuerySetCell(getQrecords, "asw1", #getCnt.asw1[getCnt.currentrow]#)>
					</cfif>

					<cfif #getCnt.asw2[getCnt.currentrow]# gt 0>
						<cfset QuerySetCell(getQrecords, "asw2", #getCnt.asw2[getCnt.currentrow]#)>
					</cfif>

					<cfif #getCnt.asw3[getCnt.currentrow]# gt 0>
						<cfset QuerySetCell(getQrecords, "asw3", #getCnt.asw3[getCnt.currentrow]#)>
					</cfif>

					<cfif #getCnt.asw4[getCnt.currentrow]# gt 0>
						<cfset QuerySetCell(getQrecords, "asw4", #getCnt.asw4[getCnt.currentrow]#)>
					</cfif>

					<cfif #getCnt.asw5[getCnt.currentrow]# gt 0>
						<cfset QuerySetCell(getQrecords, "asw5", #getCnt.asw5[getCnt.currentrow]#)>
					</cfif>

					<cfif #getCnt.asw6[getCnt.currentrow]# gt 0>
						<cfset QuerySetCell(getQrecords, "asw6", #getCnt.asw6[getCnt.currentrow]#)>
					</cfif>

					<cfif #getCnt.asw7[getCnt.currentrow]# gt 0>
						<cfset QuerySetCell(getQrecords, "asw7", #getCnt.asw7[getCnt.currentrow]#)>
					</cfif>

					<cfif #getCnt.asw8[getCnt.currentrow]# gt 0>
						<cfset QuerySetCell(getQrecords, "asw8", #getCnt.asw8[getCnt.currentrow]#)>
					</cfif>

				</cfloop>

			</cfif>

		</cfloop>

		<cfloop query="getQrecords">

			<!--- 分岐設問の場合母数変更 			--->
			<cfif viewtype EQ 1>
				<cfif ListFind(Application.app00.q_tuning_id,#q_id#) NEQ 0>
					<cfset samples = getQrecords.smp1>
				<cfelse>
					<cfset samples = getQrecords.smp0>
				</cfif>
			<cfelse>
				<cfset samples = getQrecords.smp0>
			</cfif>
			<cfset large_n = IIF(samples EQ 0, DE(1), DE(samples))>
			<cfset tmp_result1_samples = #asw1#>
			<cfset tmp_result2_samples = #asw2#>
			<cfset tmp_result3_samples = #asw3#>
			<cfset tmp_result4_samples = #asw4#>
			<cfset tmp_result5_samples = #asw5#>
			<cfset tmp_result6_samples = #asw6#>
			<cfset tmp_result7_samples = #asw7#>
			<cfset tmp_result8_samples = #asw8#>

			<!---  2005/07/20	h.miyahara(NetFusion) MOD begin--->
			<!--- 有効回答数 --->
			<cfset tmp_total = asw1 + asw2 + asw3 + asw4 + asw5 + asw6 + asw7>
			<cfif tmp_total EQ 0>
				<cfset tmp_average = 0>
				<cfset tmp_sum_score = 0>
			<cfelse>
				<cfset tmp_average = ( (tmp_result1_samples*3)
									 + (tmp_result2_samples*2) 
									 + (tmp_result3_samples*1) 
									 + (tmp_result4_samples*0) 
									 + (tmp_result5_samples*(-1)) 
									 + (tmp_result6_samples*(-2)) 
									 + (tmp_result7_samples*(-3)) )/ #tmp_total#>
									 
				<cfset tmp_sum_score =(tmp_result1_samples*3) 
									+ (tmp_result2_samples*2) 
									+ (tmp_result3_samples*1) 
									+ (tmp_result4_samples*0) 
									+ (tmp_result5_samples*(-1)) 
									+ (tmp_result6_samples*(-2)) 
									+ (tmp_result7_samples*(-3))>
			</cfif>
			<!--- 未回答・不明数 --->
			<cfset tmp_result0_samples = #asw0#>

			<!---  2005/07/20	h.miyahara(NetFusion) MOD end--->
			<cfset wk_total =	iif(isnumeric(asw1) , de(asw1) , 0)
							 +	iif(isnumeric(asw2) , de(asw2) , 0)
							 +	iif(isnumeric(asw3) , de(asw3) , 0)
							 +	iif(isnumeric(asw4) , de(asw4) , 0)
							 +	iif(isnumeric(asw5) , de(asw5) , 0)
							 +	iif(isnumeric(asw6) , de(asw6) , 0)
							 +	iif(isnumeric(asw7) , de(asw7) , 0)
							 +	iif(isnumeric(asw0) , de(asw0) , 0)>

 			<cfif wk_total EQ 0>
				<cfset tmp_result1_rate = 0>
				<cfset tmp_result2_rate = 0>
				<cfset tmp_result3_rate = 0>
				<cfset tmp_result4_rate = 0>
				<cfset tmp_result5_rate = 0>
				<cfset tmp_result6_rate = 0>
				<cfset tmp_result7_rate = 0>
				<cfif isnumeric(#tmp_result8_samples#)>
					<cfset tmp_result8_rate = 0>
				</cfif>
				<cfset tmp_result0_rate = 0>
 			<cfelse>
				<cfset tmp_result1_rate = (#tmp_result1_samples#/#wk_total#)*100>
				<cfset tmp_result2_rate = (#tmp_result2_samples#/#wk_total#)*100>
				<cfset tmp_result3_rate = (#tmp_result3_samples#/#wk_total#)*100>
				<cfset tmp_result4_rate = (#tmp_result4_samples#/#wk_total#)*100>
				<cfset tmp_result5_rate = (#tmp_result5_samples#/#wk_total#)*100>
				<cfset tmp_result6_rate = (#tmp_result6_samples#/#wk_total#)*100>
				<cfset tmp_result7_rate = (#tmp_result7_samples#/#wk_total#)*100>
				<cfif isnumeric(#tmp_result8_samples#)>
					<cfset tmp_result8_rate = (#tmp_result8_samples#/#samples#)*100>
				</cfif>
				<cfset tmp_result0_rate = (#tmp_result0_samples#/#wk_total#)*100>
 			</cfif>

			<!--- 母数調整 --->
			<cfif ListFind(Application.app00.q_smalln_id,#q_id#,',') NEQ 0>
				<cfset samples = tmp_total + #asw8#>
				<cfset small_n = tmp_total>
				<cfset tmp_result0_samples = samples-tmp_total>
				<cfset tmp_result1_rate = (#tmp_result1_samples#/#IIf(small_n EQ 0, DE(1), DE(small_n))#)*100>
				<cfset tmp_result2_rate = (#tmp_result2_samples#/#IIf(small_n EQ 0, DE(1), DE(small_n))#)*100>
				<cfset tmp_result3_rate = (#tmp_result3_samples#/#IIf(small_n EQ 0, DE(1), DE(small_n))#)*100>
				<cfset tmp_result4_rate = (#tmp_result4_samples#/#IIf(small_n EQ 0, DE(1), DE(small_n))#)*100>
				<cfset tmp_result5_rate = (#tmp_result5_samples#/#IIf(small_n EQ 0, DE(1), DE(small_n))#)*100>
				<cfset tmp_result6_rate = (#tmp_result6_samples#/#IIf(small_n EQ 0, DE(1), DE(small_n))#)*100>
				<cfset tmp_result7_rate = (#tmp_result7_samples#/#IIf(small_n EQ 0, DE(1), DE(small_n))#)*100>
				<cfif isnumeric(#tmp_result8_samples#)>
					<cfset tmp_result8_rate = (#tmp_result8_samples#/#IIf(small_n EQ 0, DE(1), DE(large_n))#)*100>
				</cfif>
				<cfset tmp_result0_rate = (#tmp_result0_samples#/#large_n#)*100>

			</cfif>

			<cfset QueryAddRow(retQuery, 1)>
	 		<cfset QuerySetCell(retQuery, "questionnaire_id", #q_id#)>
	 		<cfset QuerySetCell(retQuery, "group_id", #group_id#)>
	 		<cfset QuerySetCell(retQuery, "order_id", #order_id#)>
	 		<cfset QuerySetCell(retQuery, "samples", #samples#)>
	 		<cfset QuerySetCell(retQuery, "effective_n", #tmp_total#)>
	 		<cfset QuerySetCell(retQuery, "sum_score", #tmp_sum_score#)>
	 		<cfset QuerySetCell(retQuery, "average", #NumberFormat(tmp_average,'.__')#)>
	 		<cfset QuerySetCell(retQuery, "result1_samples", tmp_result1_samples)>
	 		<cfset QuerySetCell(retQuery, "result2_samples", tmp_result2_samples)>
	 		<cfset QuerySetCell(retQuery, "result3_samples", tmp_result3_samples)>
	 		<cfset QuerySetCell(retQuery, "result4_samples", tmp_result4_samples)>
	 		<cfset QuerySetCell(retQuery, "result5_samples", tmp_result5_samples)>
	 		<cfset QuerySetCell(retQuery, "result6_samples", tmp_result6_samples)>
	 		<cfset QuerySetCell(retQuery, "result7_samples", tmp_result7_samples)>
			<cfif isnumeric(#tmp_result8_samples#)>
				<cfset QuerySetCell(retQuery, "result8_samples", tmp_result8_samples)>
		 		<!--- 利用率 --->
		 		<cfset use_top = samples - (tmp_result8_samples + tmp_result0_samples)>	
		 		<cfset use_btm = samples>
		 		<cfset use_raete = (#use_top#/#IIf(use_btm EQ 0, DE(1), DE(use_btm))#)*100>
		 		<cfset QuerySetCell(retQuery, "usecount_txt", #NumberFormat(use_raete,'._')#)>
		 	<cfelse>
		 		<cfset use_top = samples - (tmp_result0_samples)>	
		 		<cfset use_btm = samples>
		 		<cfset use_raete = (#use_top#/#IIf(use_btm EQ 0, DE(1), DE(use_btm))#)*100>
		 		<cfset QuerySetCell(retQuery, "usecount_txt", #NumberFormat(use_raete,'._')#)>
			</cfif>
	 		<cfset QuerySetCell(retQuery, "result0_samples", tmp_result0_samples)>
	 		<cfset QuerySetCell(retQuery, "result1_average_txt", #NumberFormat(tmp_result1_rate,'._')#)>
	 		<cfset QuerySetCell(retQuery, "result2_average_txt", #NumberFormat(tmp_result2_rate,'._')#)>
	 		<cfset QuerySetCell(retQuery, "result3_average_txt", #NumberFormat(tmp_result3_rate,'._')#)>
	 		<cfset QuerySetCell(retQuery, "result4_average_txt", #NumberFormat(tmp_result4_rate,'._')#)>
	 		<cfset QuerySetCell(retQuery, "result5_average_txt", #NumberFormat(tmp_result5_rate,'._')#)>
	 		<cfset QuerySetCell(retQuery, "result6_average_txt", #NumberFormat(tmp_result6_rate,'._')#)>
	 		<cfset QuerySetCell(retQuery, "result7_average_txt", #NumberFormat(tmp_result7_rate,'._')#)>
			<cfif isnumeric(#tmp_result8_samples#)>
		 		<cfset QuerySetCell(retQuery, "result8_average_txt", #NumberFormat(tmp_result8_rate,'._')#)>
			</cfif>
	 		<cfset QuerySetCell(retQuery, "result0_average_txt", #NumberFormat(tmp_result0_rate,'._')#)>
	 		<cfset QuerySetCell(retQuery, "top1", #NumberFormat(tmp_result1_rate,'._')#)>
	 		<cfset QuerySetCell(retQuery, "top2and3", #NumberFormat(tmp_result2_rate,'._')#
													 +#NumberFormat(tmp_result3_rate,'._')#)>
	 		<cfset QuerySetCell(retQuery, "nagetive", #NumberFormat(tmp_result4_rate,'._')#
													 +#NumberFormat(tmp_result5_rate,'._')#
													 +#NumberFormat(tmp_result6_rate,'._')#
													 +#NumberFormat(tmp_result7_rate,'._')#)>

			<cfif NumberFormat(tmp_average,'.__') GTE 2.50 AND NumberFormat(tmp_average,'.__') LTE 3.00>
		 		<cfset QuerySetCell(retQuery, "rank", "A")>
			<cfelseif NumberFormat(tmp_average,'.__') GTE 2.00 AND NumberFormat(tmp_average,'.__') LTE 2.49>
	 			<cfset QuerySetCell(retQuery, "rank", "B")>
			<cfelseif NumberFormat(tmp_average,'.__') GTE 1.50 AND NumberFormat(tmp_average,'.__') LTE 1.99>
	 			<cfset QuerySetCell(retQuery, "rank", "C")>
			<cfelseif NumberFormat(tmp_average,'.__') GTE 1.00 AND NumberFormat(tmp_average,'.__') LTE 1.49>
	 			<cfset QuerySetCell(retQuery, "rank", "D")>
			<cfelse>
	 			<cfset QuerySetCell(retQuery, "rank", "E")>
			</cfif>

		</cfloop>
		<cfreturn retQuery>

		<cfsetting enablecfoutputonly="No">

		<cfcatch type="any">
			<cfsetting enablecfoutputonly="No">
			<cfrethrow>
		</cfcatch>

		</cftry>

	</cffunction>	
	<!---
	*************************************************************************************************
	* Class				: cstotal
	* Method			: getEnquete
	* Description		: アンケート設定情報取得
	* Custom Attributes	: questionnairesheet_id		シートID
	* 					: 
	* Return Paramters	: getEnquete(Query)
	* History			: 1) Coded by	2004/10/08 h.miyahara(NetFusion)
	*					: 2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getEnquete" access="remote" returntype="query" >
		<cfargument name="arg_resort_code" type="Any" default="">
		<cfargument name="arg_category_id" type="Numeric" default="">
		<cfargument name="arg_cross_id" type="Numeric" default=0>
		<cftry>
			<cfif (arraylen(arguments.arg_resort_code) IS 1) and (arguments.arg_resort_code[1] IS 0)>
				<cfset sqlStr = " ">
			<cfelse>
				<cfset tmp_list = ArrayToList(arguments.arg_resort_code)>
				<cfset tmp_cmd = "">
				<cfloop from="1" to="#ListLen(tmp_list)#" index="i">
					<cfset tmp_cmd = ListAppend(tmp_cmd, arguments.arg_resort_code[i])>
				</cfloop>

				<cfset sqlStr = " (resort_code in (#tmp_cmd#)) and ">
			</cfif>

			<!--- シートID取得 --->
			<cfquery name="getSheet_id" datasource = "#Application.datasource#">
				select top 1 
					questionnairesheet_id as sheet_id
				from 
					#Application.app00.dbu#.mst_questionnairesheet 
				where 
					#sqlStr#
					start_date <= getdate() and 
					active_flg = 1
				order by 
					start_date desc
			</cfquery>

			<!--- 抽出条件生成 --->
			<cfif (arraylen(arguments.arg_resort_code) IS 1) and (arguments.arg_resort_code[1] GREATER THAN 0)>

				<cfset sqlStr = " (resort_code=#arguments.arg_resort_code[1]#) and ">
	
				<cfif #arg_cross_id# GREATER THAN 0>
	
					<cfswitch expression="#arg_cross_id#"> 
						<!--- 性別 --->
						<cfcase value=1> 
							<cfset sqlStr = sqlStr & " (qn_id=9900000001) and ">
							<cfset viewName = "v_getEnqueteCross1#arg_category_id#">
						</cfcase> 
						<!--- 未既婚 --->
						<cfcase value=2> 
							<cfset sqlStr = sqlStr & " (qn_id=9900000002) and ">
							<cfset viewName = "v_getEnqueteCross1#arg_category_id#">
						</cfcase> 
						<!--- 年代 --->
						<cfcase value=3> 
							<cfset sqlStr = sqlStr & " (qn_id=9900000003) and ">
							<cfset viewName = "v_getEnqueteCross1#arg_category_id#">
						</cfcase> 
						<!--- 職業 --->
						<cfcase value=5> 
							<cfset sqlStr = sqlStr & " (qn_id=9900000005) and ">
							<cfset viewName = "v_getEnqueteCross1#arg_category_id#">
						</cfcase> 
						<!--- 旅行回数 --->
						<cfcase value=6> 
							<cfset sqlStr = sqlStr & " (qn_id=9900000006) and ">
							<cfset viewName = "v_getEnqueteCross1#arg_category_id#">
						</cfcase> 
	
					</cfswitch> 
					
				<cfelse>
					<cfset viewName = "v_getEnquete#arg_category_id#">

						  <!--- 古牧温泉 --->		  <!--- 奥入瀬渓流ホテル --->
					<cfif (#arguments.arg_resort_code[1]# IS 8)
					 or	  (#arguments.arg_resort_code[1]# IS 9)
					 or	  (#arguments.arg_resort_code[1]# IS 11)>

						<cfif (#arg_category_id# IS 2)>
							<cfset viewName = #viewName# & "_" & #arguments.arg_resort_code[1]#>
						</cfif>

					</cfif>

				</cfif>
	
			<cfelseif (arraylen(arguments.arg_resort_code) IS 1) and (arguments.arg_resort_code[1] IS 0)>
	
				<cfif #arg_cross_id# GREATER THAN 0>
	
					<cfswitch expression="#arg_cross_id#"> 
						<!--- 性別 --->
						<cfcase value=1> 
							<cfset sqlStr = " (qn_id = 9900000001) and ">
							<cfset viewName = "v_getEnqueteCross1#arg_category_id#_ad">
						</cfcase> 
						<!--- 未既婚 --->
						<cfcase value=2> 
							<cfset sqlStr = " (qn_id = 9900000002) and ">
							<cfset viewName = "v_getEnqueteCross1#arg_category_id#_ad">
						</cfcase> 
						<!--- 年代 --->
						<cfcase value=3> 
							<cfset sqlStr = " (qn_id = 9900000003) and ">
							<cfset viewName = "v_getEnqueteCross1#arg_category_id#_ad">
						</cfcase> 
						<!--- 職業 --->
						<cfcase value=5> 
							<cfset sqlStr = " (qn_id = 9900000005) and ">
							<cfset viewName = "v_getEnqueteCross1#arg_category_id#_ad">
						</cfcase> 
						<!--- 旅行回数 --->
						<cfcase value=6> 
							<cfset sqlStr = " (qn_id = 9900000006) and ">
							<cfset viewName = "v_getEnqueteCross1#arg_category_id#_ad">
						</cfcase> 
	
					</cfswitch> 
					
				<cfelse>
					<cfset sqlStr = " ">
					<cfset viewName = "v_getEnquete#arg_category_id#_ad">
				</cfif>
	
			<cfelse>
	
				<cfif #arg_cross_id# GREATER THAN 0>
	
					<cfswitch expression="#arg_cross_id#"> 
						<!--- 性別 --->
						<cfcase value=1> 
							<cfset sqlStr = sqlStr & " (qn_id = 9900000001) and ">
							<cfset viewName = "v_getEnqueteCross1#arg_category_id#_ad">
						</cfcase> 
						<!--- 未既婚 --->
						<cfcase value=2> 
							<cfset sqlStr = sqlStr & " (qn_id = 9900000002) and ">
							<cfset viewName = "v_getEnqueteCross1#arg_category_id#_ad">
						</cfcase> 
						<!--- 年代 --->
						<cfcase value=3> 
							<cfset sqlStr = sqlStr & " (qn_id = 9900000003) and ">
							<cfset viewName = "v_getEnqueteCross1#arg_category_id#_ad">
						</cfcase> 
						<!--- 職業 --->
						<cfcase value=5> 
							<cfset sqlStr = sqlStr & " (qn_id = 9900000005) and ">
							<cfset viewName = "v_getEnqueteCross1#arg_category_id#_ad">
						</cfcase> 
						<!--- 旅行回数 --->
						<cfcase value=6> 
							<cfset sqlStr = sqlStr & " (qn_id = 9900000006) and ">
							<cfset viewName = "v_getEnqueteCross1#arg_category_id#_ad">
						</cfcase> 
	
					</cfswitch> 
					
				<cfelse>
					<cfset viewName = "v_getEnquete#arg_category_id#_ad">
				</cfif>
	
			</cfif>

			<!--- 2008/05/09 Miya MOD B --->
			<cfif #arg_cross_id# GREATER THAN 0>
				<cfset strSQL="
					select 
						 #Application.app00.dbu#.#viewName#.*
						,((convert(bigint,order_id)*100) + cross_options_no) as viewcell_id
					from 
						#Application.app00.dbu#.#viewName#
					where 
						#sqlStr#
						questionnairesheet_id=#getSheet_id.sheet_id[getSheet_id.currentrow]#">
			<cfelse>
				<cfset strSQL="
					select 
						 #Application.app00.dbu#.#viewName#.*
						,order_id as viewcell_id
					from 
						#Application.app00.dbu#.#viewName#
					where 
						#sqlStr#
						questionnairesheet_id=#getSheet_id.sheet_id[getSheet_id.currentrow]#">

			</cfif>

			<cfif arg_category_id is 2>
				<cfset strSQL= strSQL & "
					 and	QUESTIONNAIRE_ID in (800200005,900100005)">
			<cfelse>
				<cfset strSQL= strSQL & "
					 and	QUESTIONNAIRE_ID in (100000001,100000003,600000001,700204001,700000001,700011001,700004001,700211001,800200005,900100005)">
			</cfif>


			<!--- 2008/05/09 Miya MOD E --->

			<cfif (arraylen(arguments.arg_resort_code) IS 1) and (arguments.arg_resort_code[1] GREATER THAN 0)>

				<cfquery name="getEnquete" datasource = "#Application.datasource#">
					#ReplaceList(strSQL,"''","'")#
				</cfquery>
	
			<cfelse>
	
				<cfquery name="getEnqueteWork" datasource = "#Application.datasource#">
					#ReplaceList(strSQL,"''","'")#
				</cfquery>
				<cfquery name="getEnquete" dbtype = "query">
					select distinct
						order_id, 
						group_id,
						category_id, 
						category_nm, 
						options_id, 
						options_no,
						c_options_id, 
						c_options_no, 
						cross_options_id, 
						cross_options_no, 
						questionnaire_id, 
						questionnaire_nm, 
						questionnairesheet_id, 
						viewtype,
						seq, 
						qn_id,
						viewcell_id 
					from getEnqueteWork
				</cfquery>

			</cfif>

			<cfreturn getEnquete>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>

	</cffunction>

</cfcomponent>
