﻿<cfcomponent hint="ＯＡ検索情報取得ＣＦＣ">
	<!---
	*************************************************************************************************
	* Class				:oasearch
	* Method			:getCategory
	* Description		:カテゴリリスト取得
	* Custom Attributes	:none
	* Return Paramters	:
	* History			:1) Coded by	2004/10/13 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getCategory" access="remote" returntype="query" hint="">
		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<cfset getCategoryList = querynew("questionnairecategory_id,questionnaire_id,questionnaire_nm,cs_qlabel")>
			<cfset queryaddrow(getCategoryList,1)>
			<cfset querysetcell(getCategoryList,"questionnairecategory_id",1)>
			<cfset querysetcell(getCategoryList,"questionnaire_id",0)>
			<cfset querysetcell(getCategoryList,"questionnaire_nm","100点満点＆OA")>
			<cfset querysetcell(getCategoryList,"cs_qlabel","Q1～3")>
			<cfset queryaddrow(getCategoryList,1)>
			<cfset querysetcell(getCategoryList,"questionnairecategory_id",1)>
			<cfset querysetcell(getCategoryList,"questionnaire_id",6)>
			<cfset querysetcell(getCategoryList,"questionnaire_nm","100点満点分布")>
			<cfset querysetcell(getCategoryList,"cs_qlabel","Q1")>
			<cfreturn getCategoryList>
			<cfsetting enablecfoutputonly="No">
			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>

	</cffunction>
	
	<!---
	*************************************************************************************************
	* Class				:oasearch
	* Method			:getOAAnswers
	* Description		:回答値取得
	* Custom Attributes	:questionnaire_id	設問ID
	* Return Paramters	:
	* History			:1) Coded by	2004/10/13 h.miyahara(NetFusion)
	*					:2) Updated by	2005/10/26  h.miyahara(NetFusion) デフォルトソート順変更
	*					:3) Updated by	2005/10/26  h.miyahara(NetFusion) 満足度評価設問追加
	*					:4) Updated by	2005/10/26  h.miyahara(NetFusion) 対象カテゴリ設問追加
	*************************************************************************************************
	--->
	<cffunction name="getAnswers" access="remote" returntype="any">
		<cfargument name="dcondtion" type="Boolean" default="false">
		<cfargument name="selecteddays" type="Array" default="">
		<cfargument name="questionnaire_id" type="Any" default="">
		<cfargument name="arg_generation" type="Any" default="">	<!--- 年代 --->
		<cfargument name="arg_gender" type="Any" default="">		<!--- 性別 --->
		<cfargument name="arg_job" type="Any" default="">			<!--- 職業 --->
		<cfargument name="arg_times" type="Any" default="">			<!--- 旅行回数 --->
		<cfargument name="arg_nights" type="Any" default="">		<!--- 宿泊数 --->		
		<cfargument name="arg_together" type="Any" default="">		<!--- 同行者 --->
		<cfargument name="arg_area" type="Any" default="">			<!--- 居住県 --->
		<cfargument name="arg_roomtype" type="Any" default="">		<!--- ルームタイプ --->
		<cfargument name="arg_purpose" type="Any" default="">		<!--- 旅行目的 --->
		<cfargument name="arg_reservation" type="Any" default="">	<!--- 予約経路 --->
		<cfargument name="arg_cognition" type="Any" default="">		<!--- 認知経路 --->
		<cfargument name="arg_roomnumber" type="Any" default="">	<!--- ルーム№ --->

		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<!--- リターン形式 --->
			<cfif arguments.questionnaire_id eq 0>
				<cfset wk_qid = arraynew(1)>
				<cfset arrayappend(wk_qid,6)>
				<cfset arrayappend(wk_qid,7)>
				<cfset arrayappend(wk_qid,8)>
				<cfset arguments.questionnaire_id = wk_qid>
				<cfset wk_qnm = arraynew(1)>
				<cfset arrayappend(wk_qnm,"100点満点")>
				<cfset arrayappend(wk_qnm,"感想と要望")>
				<cfset arrayappend(wk_qnm,"私共にできること")>
				<cfset arguments.questionnaire_nm = wk_qnm>
			<cfelse>
				<cfinvoke component="csmulti" method = "getAnswers" returnVariable ="rangeanswers">
					<cfinvokeargument name="dcondtion" value = #arguments.dcondtion#>
					<cfinvokeargument name="selecteddays" value = #arguments.selecteddays#>
					<cfinvokeargument name="questionnaire_id" value = #arguments.questionnaire_id#>
					<cfinvokeargument name="arg_generation" value = #arguments.arg_generation#>
					<cfinvokeargument name="arg_gender" value = #arguments.arg_gender#>
					<cfinvokeargument name="arg_job" value = #arguments.arg_job#>
					<cfinvokeargument name="arg_times" value = #arguments.arg_times#>
					<cfinvokeargument name="arg_nights" value = #arguments.arg_nights#>
					<cfinvokeargument name="arg_together" value = #arguments.arg_together#>
					<cfinvokeargument name="arg_area" value = #arguments.arg_area#>
					<cfinvokeargument name="arg_roomtype" value = #arguments.arg_roomtype#>
					<cfinvokeargument name="arg_purpose" value = #arguments.arg_purpose#>
					<cfinvokeargument name="arg_reservation" value = #arguments.arg_reservation#>
					<cfinvokeargument name="arg_cognition" value = #arguments.arg_cognition#>
					<cfinvokeargument name="arg_roomnumber" value = #arguments.arg_roomnumber#>
					<cfinvokeargument name="arg_rcs" value = true>
				</cfinvoke>
				<cfquery dbtype="query" name="retq">
					select label,samples,rate from rangeanswers
				</cfquery>
				<cfreturn retq>
			</cfif>


			<!--- ＯＡ条件整形 --->
			<cfif IsArray(arguments.questionnaire_id) and IsArray(arguments.questionnaire_nm)>
				<cfset arguments.questionnaire_id = ArrayToList(arguments.questionnaire_id)>
				<cfset arguments.questionnaire_nm = ArrayToList(arguments.questionnaire_nm)>
				<cfset tmp_questionnaire_nm = "">
				<cfloop from="1" to="#ListLen(arguments.questionnaire_nm)#" index="i">
					<cfset tmp_questionnaire_nm = ListAppend(tmp_questionnaire_nm,"OA"&(i-1))>
				</cfloop>
			<cfelse>
				<cfset tmp_questionnaire_nm = "OA0">
			</cfif>	
			<cfset wk_field = "
				referencenumber, 
				depart_date, 
				#tmp_questionnaire_nm#">
			<!--- 戻値クエリ作成 --->
			<cfset retQuery = QueryNew("#wk_field#")>
				
			<!--- 共通日時抽出条件切替 --->
			<cfinvoke component = "conditionsetting" method = "datecondition" returnVariable ="dcondition">
				<cfinvokeargument name="dcondtion" value = #arguments.dcondtion#>
				<cfinvokeargument name="selecteddays" value = #arguments.selecteddays#>
			</cfinvoke>

			<!--- 共通集計条件絞込み処理 --->
			<cfinvoke 
				component = "conditionsetting" 
				method = "sqlMake1"
				temp_cond = #arguments#
				returnVariable = "strSQL1">
	
			<!--- 共通集計条件絞込み処理 --->
			<cfinvoke 
				component = "conditionsetting" 
				method = "sqlMake2"
				temp_cond = #arguments#
				returnVariable = "strSQL2">

			<!--- 抽出対象期間より期間内の回答者予約番号を抽出 --->
			<cfquery name="getAnswers" datasource = "#Application.datasource#">
				select * 
				from 
					#Application.app00.dbu#.v_getAnswers
				where 
					#PreserveSingleQuotes(dcondition)# and
					questionnaire_id in (
						#arguments.questionnaire_id#)
					#PreserveSingleQuotes(strSQL1)#
					#PreserveSingleQuotes(strSQL2)#
				and	questionnairesheet_id = 0
				order by	 depart_date,referencenumber desc
			</cfquery>
			<!--- 対象者無し --->
			<cfif getAnswers.recordcount EQ 0>
				<cfreturn retQuery>
			</cfif>	
			<cfset getReservationList = getAnswers>

			<!--- <cfdump var=#getReservationList#> --->
			<!--- 2005/06/27 h.miyahara(NetFusion) ADD オプション条件 end --->
			<cfset temp_code = "">
			<cfloop query="getReservationList">
				<cfif getReservationList.referencenumber NEQ temp_code>
					<cfset QueryAddRow(retQuery, 1)>
					<cfset temp_code = getReservationList.referencenumber>
				</cfif>
				<!--- 顧客コード --->
	 			<cfset QuerySetCell(retQuery, "referencenumber", #getReservationList.referencenumber#&#chr(9)#)>
				<!--- 滞在日 --->
	 			<cfset QuerySetCell(retQuery, "depart_date", #getReservationList.depart_date#&#chr(9)#)>
				<!--- OA --->
				<cfset temp_check = "">
				<cfloop from="1" to="#ListLen(arguments.questionnaire_id)#" index="i">
					<cfif getReservationList.questionnaire_id EQ ListGetAt(arguments.questionnaire_id,i)>
			 			<cfset QuerySetCell(retQuery, #ListGetAt(tmp_questionnaire_nm,i)#, 
													  #getReservationList.answertext#&#chr(9)#)>
			 			<cfset temp_check = temp_check & #getReservationList.answertext#>
					</cfif>
				</cfloop>
			</cfloop>
			<cfset wk_field = "
				referencenumber, 
				depart_date, 
				#tmp_questionnaire_nm#">
			<cfquery name="retQuery" dbtype="query">select #wk_field# from retQuery</cfquery>
			<cfreturn retQuery>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>

	</cffunction>

	<!---
	*************************************************************************************************
	* Class				:oasearch
	* Method			:OAResultOutputCSV
	* Description		:OA検索結果一覧ＣＳＶ出力
	* Custom Attributes	:resultObj	出力リストのデータプロバイダ
	* 					:fieldnames	列名
	* 					:headerText	列のキャプション
	* Return Paramters	:
	* History			:1) Coded by	2004/10/05 h.miyahara(NetFusion)
	*					:2) Updated by	2005/10/26  h.miyahara(NetFusion) 集計条件追加
	*************************************************************************************************
	--->
	<cffunction name="OAResultOutputCSV" access="remote" returntype="any">
		<cfargument name="resultObj" type="Array" default="">
		<cfargument name="fieldnames" type="Array" default="">
		<cfargument name="headerText" type="Array" default="">
		<cfargument name="qstseleted" type="String" default="">
		<cfargument name="selecteddate" type="String" default="">
		<cfargument name="condseleted" type="String" default="">

		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<!--- ディレクトリ＆ファイル情報 --->
			<cfset filePath = ExpandPath(#application.outputDirectory#)>
			<cfset Dircheck = DirectoryExists( filePath & "\" & #Replace(cgi.remote_addr,".","","all")# )>
			
			<!--- ディレクトリ存在チェック --->
			<cfif Dircheck eq "no">
				<cfdirectory action = "create" directory = "#filePath#\#Replace(cgi.remote_addr,'.','','all')#">
			</cfif>

			<cfset UserDir = #Replace(cgi.remote_addr,".","","all")# & "\">
			<cfset datepart = #DateFormat(Now(),'YYMMDD')# & #TimeFormat(Now(),'HHMMSS')#>
			<cfset filename = filePath & UserDir & datepart & "csresults.csv">
			<cfset filecheck = FileExists( filename )>
			
			<!--- ディレクトリ内ファイル数チェック --->
			<cfdirectory directory="#filePath#\#Replace(cgi.remote_addr,'.','','all')#" name="myCSVDirectory" sort="DATELASTMODIFIED ASC">
				
			<!--- 指定ファイル数以上は作らない。古いものから削除 --->
			<cfset FileHistory = 10>
			<cfif myCSVDirectory.RecordCount GTE FileHistory>
				<cffile action = "delete" file = "#filePath##UserDir##myCSVDirectory.Name#">
			</cfif>	

			<!--- ファイル存在チェック --->
			<cfif filecheck eq "no">
				<cfset arguments.selecteddate = Replace(arguments.selecteddate,',','|','all')>
				<cfset arguments.condseleted = Replace(arguments.condseleted,',','|','all')>
				
				<cfset condData = "-集計条件-" & "," & "" & "【#arguments.qstseleted#】【#arguments.selecteddate#】【出力日時：#dateformat(now(),'YYYY/MM/DD')# #timeformat(now(),'HH:MM')#】【#arguments.condseleted#】"	& "#chr(13)##chr(10)#" & "#chr(13)##chr(10)#">	
				<cfset captionData = condData &  ArrayToList(arguments.headerText)>

				<cffile action = "write" file=#filename# output=#captionData#>

				<!--- 出力内容整形 改行コード除去 --->
				<cfloop from="1" to="#ArrayLen(resultObj)#" index="i">
					<cfset outputData = "">
					<cfloop from="1" to="#ArrayLen(fieldnames)#" index="c">
						<cfset wk_op = Replace(Evaluate("resultObj[i].#fieldnames[c]#"),',','，','all')>
						<cfset outputData = ListAppend( outputData, #replacelist(wk_op,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#,#chr(9)#",",,,")#)>
					</cfloop>
					<cffile action = "append" file=#filename# output=#outputData#>
				</cfloop>
			</cfif>

			<!--- 戻値設定 --->
			<cfscript>
				executeinformation = StructNew();
				StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
				StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
				StructInsert(executeinformation,	"outputMsg",	"正常"						);
				StructInsert(executeinformation,	"outputPath",	#application.outputDirectory# & #Replace(UserDir,"\","/","all")# & datepart & "csresults.csv" );
				StructInsert(executeinformation,	"filename",		"http://#cgi.server_name#/#application.outputDirectory#" & #Replace(UserDir,"\","/","all")# & datepart & "csresults.csv" );
				StructInsert(executeinformation,	"newname",		"#Application.resortname##Application.app00.appname##datepart#.csv" );
			</cfscript>
			<cfreturn executeinformation>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>

	</cffunction>
</cfcomponent>
