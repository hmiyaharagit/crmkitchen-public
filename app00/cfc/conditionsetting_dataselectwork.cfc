﻿<cfcomponent hint="共有ＣＳ集計条件">
	<!---
	*************************************************************************************************
	* Class				: cstotal
	* Method			: getArea
	* Description		: エリアマスタ情報取得
	* Custom Attributes	: none
	* 					: 
	* Return Paramters	: retQuery(Query)
	* History			: 1) Coded by	2005/06/27 h.miyahara(NetFusion)
	*					: 2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getmaster" access="remote" returntype="any">
		<cfargument name="resort_code" type="any" default="">
		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<!--- 年齢 --->
			<cfinvoke component="conditionsetting_dataselectwork" method="getaoptions" returnVariable="m_generation">
				<cfinvokeargument name="questionnaire_id" value="#application.app00.q_generation#">
				<cfinvokeargument name="resort_code" value="#arguments.resort_code#">
			</cfinvoke>
			<!--- 性別 --->
			<cfinvoke component="conditionsetting_dataselectwork" method="getaoptions" returnVariable="m_gender">
				<cfinvokeargument name="questionnaire_id" value="#application.app00.q_gender#">
				<cfinvokeargument name="resort_code" value="#arguments.resort_code#">
			</cfinvoke>
			<!--- 職業 --->
			<cfinvoke component="conditionsetting_dataselectwork" method="getaoptions" returnVariable="m_job">
				<cfinvokeargument name="questionnaire_id" value="#application.app00.q_job#">
				<cfinvokeargument name="resort_code" value="#arguments.resort_code#">
			</cfinvoke>
			<!--- 同行者 --->
			<cfinvoke component="conditionsetting_dataselectwork" method="getcontrolitems" returnVariable="m_together">
				<cfinvokeargument name="controldiv_id" value="#application.app00.c_together#">
				<cfinvokeargument name="dbu" value="#application.app00.dbu#">
				<cfinvokeargument name="resort_code" value="#arguments.resort_code#">
			</cfinvoke>

			
			<!--- エリア --->
			<cfinvoke component="conditionsetting_dataselectwork" method="getcontrolitems" returnVariable="m_area">
				<cfinvokeargument name="controldiv_id" value="#application.app00.c_area#">
				<cfinvokeargument name="dbu" value="#application.app00.dbu#">
				<cfinvokeargument name="resort_code" value="#arguments.resort_code#">
			</cfinvoke>
		

			<!--- ルームタイプ --->
			<cfinvoke component="conditionsetting_dataselectwork" method="getcontrolitems" returnVariable="m_roomtype">
				<cfinvokeargument name="controldiv_id" value="#application.app00.c_roomtype#">
				<cfinvokeargument name="dbu" value="#application.app00.dbu#">
				<cfinvokeargument name="resort_code" value="#arguments.resort_code#">
			</cfinvoke>
			<!--- 旅行目的 --->
			<cfinvoke component="conditionsetting_dataselectwork" method="getcontrolitems" returnVariable="m_purpose">
				<cfinvokeargument name="controldiv_id" value="#application.app00.c_purpose#">
				<cfinvokeargument name="dbu" value="#application.app00.dbu#">
				<cfinvokeargument name="resort_code" value="#arguments.resort_code#">
			</cfinvoke>
			<!--- 旅行回数 --->
			<cfinvoke component="conditionsetting_dataselectwork" method="getaoptions" returnVariable="m_trip">
				<cfinvokeargument name="resort_code" value="#arguments.resort_code#">
				<cfinvokeargument name="questionnaire_id" value="#application.app00.q_trip#">
			</cfinvoke>
			<!--- 予約経路 --->
			<cfinvoke component="conditionsetting_dataselectwork" method="getcontrolitems" returnVariable="m_reservation">
				<cfinvokeargument name="controldiv_id" value="#application.app00.c_reservation#">
				<cfinvokeargument name="dbu" value="#application.app00.dbu#">
				<cfinvokeargument name="resort_code" value="#arguments.resort_code#">
			</cfinvoke>
			<!--- 認知経路 --->
			<cfinvoke component="conditionsetting_dataselectwork" method="getcontrolitems" returnVariable="m_cognition">
				<cfinvokeargument name="controldiv_id" value="#application.app00.c_cognition#">
				<cfinvokeargument name="dbu" value="#application.app00.dbu#">
				<cfinvokeargument name="resort_code" value="#arguments.resort_code#">
			</cfinvoke>
			<!--- 旅館 --->
			<cfquery name="m_resort" datasource = "#Application.datasource#">
				select	 convert(varchar,resort_code) items_id 
						,convert(varchar,resort_nm) items_nm
						,convert(varchar,resort_code) fla_mcname
						,'--' q_nm
				from	 #application.app00.dbu#.mst_resort
				where	 resort_type = 1
				<cfif arguments.resort_code neq 0>
				and	 	 resort_code in (#arguments.resort_code#)
				</cfif>
				order by resort_code
			</cfquery>
				
			<!--- ルームナンバー --->
			<cfquery name="m_roomnumber" datasource = "#Application.datasource#">
				select 	 convert(varchar,roomnumber) as items_id 
				<cfif arguments.resort_code neq 0>
						,convert(varchar,roomnumber) as items_nm
				<cfelse>
						,convert(varchar,roomnumber) + '(' + b.resort_nm + ')' as items_nm
				</cfif>
						,convert(varchar,roomnumber) as fla_mcname
						,'TAP' as q_nm
				from	 #application.app00.dbu#.mst_room a
						,#application.app00.dbu#.mst_resort b
				where	 a.resort_code = b.resort_code
				<cfif arguments.resort_code neq 0>
				and	 a.resort_code in (#arguments.resort_code#)
				</cfif>
				order by a.resort_code,roomnumber
			</cfquery>

			<cfset m_nights = querynew("items_id,items_nm,fla_mcname,q_nm")>
			<cfset queryaddrow(m_nights,1)>
			<cfset querysetcell(m_nights,"items_id","1")>
			<cfset querysetcell(m_nights,"items_nm","1泊")>
			<cfset querysetcell(m_nights,"fla_mcname","1")>
			<cfset querysetcell(m_nights,"q_nm","TAP")>
			<cfset queryaddrow(m_nights,1)>
			<cfset querysetcell(m_nights,"items_id","2")>
			<cfset querysetcell(m_nights,"items_nm","2泊")>
			<cfset querysetcell(m_nights,"fla_mcname","2")>
			<cfset querysetcell(m_nights,"q_nm","TAP")>
			<cfset queryaddrow(m_nights,1)>
			<cfset querysetcell(m_nights,"items_id","3")>
			<cfset querysetcell(m_nights,"items_nm","3泊")>
			<cfset querysetcell(m_nights,"fla_mcname","3")>
			<cfset querysetcell(m_nights,"q_nm","TAP")>
			<cfset queryaddrow(m_nights,1)>
			<cfset querysetcell(m_nights,"items_id","4")>
			<cfset querysetcell(m_nights,"items_nm","4泊")>
			<cfset querysetcell(m_nights,"fla_mcname","4")>
			<cfset querysetcell(m_nights,"q_nm","TAP")>
			<cfset queryaddrow(m_nights,1)>
			<cfset querysetcell(m_nights,"items_id","5")>
			<cfset querysetcell(m_nights,"items_nm","5泊以上")>
			<cfset querysetcell(m_nights,"fla_mcname","5")>
			<cfset querysetcell(m_nights,"q_nm","TAP")>
				
			<cfset rets = structnew()>
			<cfset structinsert(rets, "m_generation", m_generation)>
			<cfset structinsert(rets, "m_nights", m_nights)>
			<cfset structinsert(rets, "m_gender", m_gender)>
			<cfset structinsert(rets, "m_job", m_job)>
			<cfset structinsert(rets, "m_together", m_together)>
			<cfset structinsert(rets, "m_area", m_area)>

			<cfset structinsert(rets, "m_roomtype", m_roomtype)>
			<cfset structinsert(rets, "m_purpose", m_purpose)>
			<cfset structinsert(rets, "m_trip", m_trip)>
			<cfset structinsert(rets, "m_reservation", m_reservation)>
			<cfset structinsert(rets, "m_cognition", m_cognition)>
			<!--- --->
			<cfset structinsert(rets, "m_roomnumber", m_roomnumber)>
			 
			<cfset structinsert(rets, "m_resort", m_resort)>

			<cfreturn rets>
			<cfsetting enablecfoutputonly="No">
			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
<!---
	*************************************************************************************************
	* Class				: cstotal
	* Method			: getcontrolitems
	* Description		: コントロールマスタ情報取得
	* Custom Attributes	: none
	* 					: 
	* Return Paramters	: retQuery(Query)
	* History			: 1) Coded by	2005/04/26 h.miyahara(NetFusion)
	*					: 2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getcontrolitems" access="remote" returntype="query" >
		<cfargument name="controldiv_id" type="numeric" default="">
		<cfargument name="dbu" type="String" default="">
		<cfargument name="resort_code" type="any" default="">
		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<cfif arguments.resort_code neq 0>
				<cfquery name="retQuery" datasource = "#Application.datasource#">
					select	 control_id items_id 
							,control_nm items_nm 
							,control_id fla_mcname
							,control_id wk_id
							,'TAP' q_nm
							,seq
					from	 #arguments.dbu#.mst_control 
					where	 controldiv_id = #arguments.controldiv_id#
					and		 resort_code in ( #arguments.resort_code# )
					union
					select	 control_id items_id 
							,control_nm items_nm 
							,control_id fla_mcname
							,control_id wk_id
							,'TAP' q_nm
							,seq
					from	 #arguments.dbu#.mst_control 
					where	 controldiv_id = #arguments.controldiv_id#
					and		 resort_code = 0
					order by items_id,seq
				</cfquery>
			<cfelse>
				<cfquery name="retQuery" datasource = "#Application.datasource#">
					select	 distinct control_id items_id 
							,control_nm items_nm 
							,control_id fla_mcname
							,control_id wk_id
							,'TAP' q_nm
					from	 #arguments.dbu#.mst_control 
					where	 controldiv_id = #arguments.controldiv_id#
					order by items_id
				</cfquery>
			</cfif>

			<cfreturn retQuery>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

<!---
	*************************************************************************************************
	* Class				: cstotal
	* Method			: getaoptions
	* Description		: 回答候補値情報取得
	* Custom Attributes	: none
	* 					: 
	* Return Paramters	: retQuery(Query)
	* History			: 1) Coded by	2005/04/26 h.miyahara(NetFusion)
	*					: 2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getaoptions" access="remote" returntype="query" >
		<cfargument name="questionnaire_id" type="numeric" default="">
		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<cfquery name="retQuery" datasource = "#Application.datasource#">
				select 	 distinct
						 convert(varchar,a.options_no) items_id 
						,a.options_label items_nm 
						,convert(varchar,c.seq) fla_mcname
						,b.cs_qlabel q_nm
				from	 #Application.app00.dbu#.mst_answeroptions a
						,#Application.app00.dbu#.oth_sheetconfiguration b
						,#Application.app00.dbu#.oth_answeroptions c
				where	 c.questionnaire_id = #arguments.questionnaire_id#
				and		 a.options_id = c.options_id
				and		 c.questionnaire_id = b.questionnaire_id
				and		 c.questionnairesheet_id = (select top 1 questionnairesheet_id from #Application.app00.dbu#.mst_questionnairesheet where start_date <= getdate() and questionnairesheet_id != 0 and active_flg = 1)
			</cfquery>
		
			<!--- 例外処理 --->
			<cfif arguments.questionnaire_id eq Application.app00.q_job>
				<cfset queryaddrow(retQuery,1)>
				<cfset querysetcell(retQuery,"items_id","1,5")>
				<cfset querysetcell(retQuery,"items_nm","会社員・公務員")>
				<cfset querysetcell(retQuery,"fla_mcname","g0")>
				<cfset querysetcell(retQuery,"q_nm",retQuery.q_nm)>
				<cfset queryaddrow(retQuery,1)>
				<cfset querysetcell(retQuery,"items_id","2,3,4")>
				<cfset querysetcell(retQuery,"items_nm","富裕層")>
				<cfset querysetcell(retQuery,"fla_mcname","g1")>
				<cfset querysetcell(retQuery,"q_nm",retQuery.q_nm)>
				<cfset queryaddrow(retQuery,1)>
				<cfset querysetcell(retQuery,"items_id","6,7,8,9,99")>
				<cfset querysetcell(retQuery,"items_nm","無職計")>
				<cfset querysetcell(retQuery,"fla_mcname","g2")>
				<cfset querysetcell(retQuery,"q_nm",retQuery.q_nm)>
			</cfif>
			<cfreturn retQuery>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>


	<!---
	*************************************************************************************************
	* Class				: cstotal
	* Method			: getcontrolitems
	* Description		: コントロールマスタ情報取得
	* Custom Attributes	: none
	* 					: 
	* Return Paramters	: retQuery(Query)
	* History			: 1) Coded by	2005/04/26 h.miyahara(NetFusion)
	*					: 2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="datecondition" access="remote" returntype="string" >
		<cfargument name="dcondtion" type="Boolean" default="false">
		<cfargument name="selecteddays" type="Array" default="">

		<cfsetting enablecfoutputonly="yes">
		<cftry>

			<!--- 日付条件調整 --->
			<cfif arraylen(arguments.selecteddays) eq 1>
				<cfset arguments.selecteddays[2] = arguments.selecteddays[1]>
			</cfif>

			<!--- 抽出条件切替 --->
			<cfif arguments.dcondtion eq true>
				<cfset wk_sdays = "">
				<cfloop from="1" to="#arraylen(arguments.selecteddays)#" index="i">
					<cfset wk_sdays = listappend(wk_sdays,#dateformat(arguments.selecteddays[i],'yyyy/mm/dd')#)>
				</cfloop>
				<cfset wk_sdays = ListQualify(wk_sdays,"'")>
				<cfset dcondition = "depart_date in (#PreserveSingleQuotes(wk_sdays)#)">
			<cfelse>
				<cfset wk_sdays1 = dateformat(arguments.selecteddays[1],'yyyy/mm/dd')>
				<cfset wk_sdays2 = dateformat(arguments.selecteddays[2],'yyyy/mm/dd')>
				<cfset dcondition = "depart_date between '#wk_sdays1#' and '#wk_sdays2#'">
			</cfif>

			<cfreturn dcondition>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<!---
	*************************************************************************************************
	* Class				: conditionsetting_dataselectwork
	* Method			: sqlMake1
	* Description		: sqlMake1
	* Custom Attributes	: none
	* 					: 
	* Return Paramters	: retQuery(Query)
	* History			: 1) Coded by	2005/04/26 h.miyahara(NetFusion)
	*					: 2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="sqlMake1" access="remote" returntype="string" >
		<cfargument name="temp_cond" type="Any" default="">
		<cfargument name="arg_sheet_id" type="numeric" default="0">

		<cfsetting enablecfoutputonly="yes">
		<cftry>

			<cfset StructAppend(arguments, arguments.temp_cond, "true")>

			<cfset strSQL1="">
			<cfset strBase = " and (#Application.app00.dbu#.v_getAnswers.referencenumber in (
				select distinct 
					#Application.app00.dbu#.hst_answers.referencenumber
				from
					#Application.app00.dbu#.hst_answers INNER JOIN #Application.app00.dbu#.hst_answersdetails ON 
					#Application.app00.dbu#.hst_answers.referencenumber = 
					#Application.app00.dbu#.hst_answersdetails.referencenumber AND 
					#Application.app00.dbu#.hst_answers.questionnairesheet_id = 
					#Application.app00.dbu#.hst_answersdetails.questionnairesheet_id 
				where ">

			<cfif #arguments.arg_sheet_id# eq 0>
				<cfset strBase = strBase & 
					"(#Application.app00.dbu#.hst_answersdetails.questionnairesheet_id > #arguments.arg_sheet_id#) AND ">
			<cfelse>
				<cfset strBase = strBase & 
					"(#Application.app00.dbu#.hst_answersdetails.questionnairesheet_id = #arguments.arg_sheet_id#) AND ">
			</cfif>
			
			<cfset addcnt = 0>

			<!--- 年代 (118) --->
			<cfif arraylen(arguments.arg_generation) neq 0>

				<cfset tmp_list = ArrayToList(arguments.arg_generation)>
				<cfset tmp_cmd = "">
				<cfloop from="1" to="#ListLen(tmp_list)#" index="i">
					<cfset tmp_cmd = ListAppend(tmp_cmd,"'" & arguments.arg_generation[i] & "'")>
				</cfloop>
	
				<cfset strSQL1 = strSQL1 & strBase & "
					(#Application.app00.dbu#.hst_answersdetails.questionnaire_id = #Application.app00.q_generation#) AND 
					(#Application.app00.dbu#.hst_answersdetails.answer in (#tmp_cmd#)) ">
				<cfset addcnt = addcnt+1>

			</cfif>

			<!--- 性別 (116) --->
			<cfif arraylen(arguments.arg_gender) neq 0>

				<cfset tmp_list = ArrayToList(arguments.arg_gender)>
				<cfset tmp_cmd = "">
				<cfloop from="1" to="#ListLen(tmp_list)#" index="i">
					<cfset tmp_cmd = ListAppend(tmp_cmd,"'" & arguments.arg_gender[i] & "'")>
				</cfloop>

				<cfset strSQL1 = strSQL1 & strBase & "
					(#Application.app00.dbu#.hst_answersdetails.questionnaire_id = #Application.app00.q_gender#) AND 
					(#Application.app00.dbu#.hst_answersdetails.answer in (#tmp_cmd#)) ">
				<cfset addcnt = addcnt+1>

			</cfif>

			<!--- 職業 (120) --->
			<cfif arraylen(arguments.arg_job) neq 0>

				<cfset tmp_list = ArrayToList(arguments.arg_job)>
				<cfset tmp_cmd = "">
				<cfloop from="1" to="#ListLen(tmp_list)#" index="i">
					<cfset tmp_cmd = ListAppend(tmp_cmd,"'" & arguments.arg_job[i] & "'")>
				</cfloop>
	
				<cfset strSQL1 = strSQL1 & strBase & "
					(#Application.app00.dbu#.hst_answersdetails.questionnaire_id = #Application.app00.q_job#) AND 
					(#Application.app00.dbu#.hst_answersdetails.answer in (#tmp_cmd#)) ">
				<cfset addcnt = addcnt+1>

			</cfif>

			<!--- 旅行回数 (121) --->
			<cfif arraylen(arguments.arg_times) neq 0>

				<cfset tmp_list = ArrayToList(arguments.arg_times)>
				<cfset tmp_cmd = "">
				<cfloop from="1" to="#ListLen(tmp_list)#" index="i">
					<cfset tmp_cmd = ListAppend(tmp_cmd,"'" & arguments.arg_times[i] & "'")>
				</cfloop>
	
				<cfset strSQL1 = strSQL1 & strBase & "
					(#Application.app00.dbu#.hst_answersdetails.questionnaire_id = #Application.app00.q_trip#) AND 
					(#Application.app00.dbu#.hst_answersdetails.answer in (#tmp_cmd#)) ">
				<cfset addcnt = addcnt+1>

			</cfif>

			<cfset strSQL1 = strSQL1 & #RepeatString(")", addcnt*2)#>

			<cfreturn strSQL1>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<!---
	*************************************************************************************************
	* Class				: conditionsetting_dataselectwork
	* Method			: sqlMake2
	* Description		: sqlMake2
	* Custom Attributes	: none
	* 					: 
	* Return Paramters	: retQuery(Query)
	* History			: 1) Coded by	2005/04/26 h.miyahara(NetFusion)
	*					: 2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="sqlMake2" access="remote" returntype="string" >
		<cfargument name="temp_cond" type="Any" default="">

		<cfsetting enablecfoutputonly="yes">
		<cftry>

			<cfset StructAppend(arguments, arguments.temp_cond, "true")>

			<cfset strSQL2="">
	
			<!--- 泊数 --->
			<cfif arraylen(arguments.arg_nights) neq 0>

				<cfset tmp_list = ArrayToList(arguments.arg_nights)>
				<cfset tmp_cmd = "">
				<cfloop from="1" to="#ListLen(tmp_list)#" index="i">
					<cfset tmp_cmd = ListAppend(tmp_cmd, arguments.arg_nights[i])>
				</cfloop>
	
				<cfset strSQL2 = strSQL2 & " and (nights in (#tmp_cmd#)) ">

			</cfif>
	
			<!--- 同行者 --->
			<cfif arraylen(arguments.arg_together) neq 0>

				<cfset tmp_list = ArrayToList(arguments.arg_together)>
				<cfset tmp_cmd = "">
				<cfloop from="1" to="#ListLen(tmp_list)#" index="i">
					<!--- 下位アイテム取得 --->
					<cfquery name="getchild" datasource = "#Application.datasource#">
						select	 control_id
						from	 #application.app00.dbu#.mst_control
						where	 controldiv_id = #application.app00.c_together#
						and		 control_id like '#arguments.arg_together[i]#%'
						<cfif len(arguments.check_resort_code) neq 0>
						and		 resort_code = #arguments.arg_resort_code[1]#
						</cfif>
					</cfquery>
					<cfif getchild.recordcount eq 0>
						<cfset tmp_cmd = ListAppend(tmp_cmd,"'" & arguments.arg_together[i] & "'")>
					<cfelse>
						<cfset wk_list = QuotedValueList(getchild.control_id)>
						<cfset tmp_cmd = ListAppend(tmp_cmd,wk_list)>
					</cfif>
				</cfloop>
				<cfset strSQL2 = strSQL2 & " and (together_id in (#tmp_cmd#)) ">
			</cfif>

			<!--- エリア --->
			<cfif arraylen(arguments.arg_area) neq 0>

				<cfset tmp_list = ArrayToList(arguments.arg_area)>
				<cfset tmp_cmd = "">
				<cfloop from="1" to="#ListLen(tmp_list)#" index="i">
					<cfset tmp_cmd = ListAppend(tmp_cmd,"'" & arguments.arg_area[i] & "'")>
				</cfloop>
	
				<cfset strSQL2 = strSQL2 & " and (area_id in (#tmp_cmd#)) ">

			</cfif>
	
			<!--- ルームタイプ --->
			<cfif arraylen(arguments.arg_roomtype) neq 0>
				<cfset tmp_list = ArrayToList(arguments.arg_roomtype)>
				<cfset tmp_cmd = "">
				<cfloop from="1" to="#ListLen(tmp_list)#" index="i">
					<!--- 下位アイテム取得 --->
					<cfquery name="getchild" datasource = "#Application.datasource#">
						select	 control_id
						from	 #application.app00.dbu#.mst_control
						where	 controldiv_id = #application.app00.c_roomtype#
						and		 control_id like '#arguments.arg_roomtype[i]#%'
						<cfif len(arguments.check_resort_code) neq 0>
						and		 resort_code = #arguments.arg_resort_code[1]#
						</cfif>
					</cfquery>
					<cfif getchild.recordcount eq 0>
						<cfset tmp_cmd = ListAppend(tmp_cmd,"'" & arguments.arg_roomtype[i] & "'")>
					<cfelse>
						<cfset wk_list = QuotedValueList(getchild.control_id)>
						<cfset tmp_cmd = ListAppend(tmp_cmd,wk_list)>
					</cfif>
				</cfloop>
	
				<cfset strSQL2 = strSQL2 & " and ( roomtype_id in (#tmp_cmd#)) ">

			</cfif>
	
			<!--- 旅行目的 --->
			<cfif arraylen(arguments.arg_purpose) neq 0>
				<cfset tmp_list = ArrayToList(arguments.arg_purpose)>
				<cfset tmp_cmd = "">
				<cfloop from="1" to="#ListLen(tmp_list)#" index="i">
					<!--- 下位アイテム取得 --->
					<cfquery name="getchild" datasource = "#Application.datasource#">
						select	 control_id
						from	 #application.app00.dbu#.mst_control
						where	 controldiv_id = #application.app00.c_purpose#
						and		 control_id like '#arguments.arg_purpose[i]#%'
						<cfif len(arguments.check_resort_code) neq 0>
						and		 resort_code = #arguments.arg_resort_code[1]#
						</cfif>
					</cfquery>
					<cfif getchild.recordcount eq 0>
						<cfset tmp_cmd = ListAppend(tmp_cmd,"'" & arguments.arg_purpose[i] & "'")>
					<cfelse>
						<cfset wk_list = QuotedValueList(getchild.control_id)>
						<cfset tmp_cmd = ListAppend(tmp_cmd,wk_list)>
					</cfif>
				</cfloop>

				<cfset strSQL2 = strSQL2 & " and (purpose_id in (#tmp_cmd#)) ">

			</cfif>
	
			<!--- 予約経路 --->
			<cfif arraylen(arguments.arg_reservation) neq 0>
				<cfset tmp_list = ArrayToList(arguments.arg_reservation)>
				<cfset tmp_cmd = "">
				<cfloop from="1" to="#ListLen(tmp_list)#" index="i">
					<!--- 下位アイテム取得 --->
					<cfquery name="getchild" datasource = "#Application.datasource#">
						select	 control_id
						from	 #application.app00.dbu#.mst_control
						where	 controldiv_id = #application.app00.c_reservation#
						and		 control_id like '#arguments.arg_reservation[i]#%'
						<cfif len(arguments.check_resort_code) neq 0>
						and		 resort_code = #arguments.arg_resort_code[1]#
						</cfif>
					</cfquery>
					<cfif getchild.recordcount eq 0>
						<cfset tmp_cmd = ListAppend(tmp_cmd,"'" & arguments.arg_reservation[i] & "'")>
					<cfelse>
						<cfset wk_list = QuotedValueList(getchild.control_id)>
						<cfset tmp_cmd = ListAppend(tmp_cmd,wk_list)>
					</cfif>
				</cfloop>
				<cfset strSQL2 = strSQL2 & " and (reservation_id in (#tmp_cmd#)) ">
			</cfif>
	
			<!--- 認知経路 --->
			<cfif arraylen(arguments.arg_cognition) neq 0>

				<cfset tmp_list = ArrayToList(arguments.arg_cognition)>
				<cfset tmp_cmd = "">
				<cfloop from="1" to="#ListLen(tmp_list)#" index="i">
					<!--- 下位アイテム取得 --->
					<cfquery name="getchild" datasource = "#Application.datasource#">
						select	 control_id
						from	 #application.app00.dbu#.mst_control
						where	 controldiv_id = #application.app00.c_cognition#
						and		 control_id like '#arguments.arg_cognition[i]#%'
						<cfif len(arguments.check_resort_code) neq 0>
						and		 resort_code = #arguments.arg_resort_code[1]#
						</cfif>
					</cfquery>
					<cfif getchild.recordcount eq 0>
						<cfset tmp_cmd = ListAppend(tmp_cmd,"'" & arguments.arg_cognition[i] & "'")>
					<cfelse>
						<cfset wk_list = QuotedValueList(getchild.control_id)>
						<cfset tmp_cmd = ListAppend(tmp_cmd,wk_list)>
					</cfif>
					<!--- 
					<cfset tmp_cmd = ListAppend(tmp_cmd,"'" & arguments.arg_cognition[i] & "'")>
					 --->
				</cfloop>
				<cfset strSQL2 = strSQL2 & " and (cognition_id in (#tmp_cmd#)) ">

			</cfif>
	
			<!--- 部屋番号 --->
			<cfif arraylen(arguments.arg_roomnumber) neq 0>

				<cfset tmp_list = ArrayToList(arguments.arg_roomnumber)>
				<cfset tmp_cmd = "">
				<cfloop from="1" to="#ListLen(tmp_list)#" index="i">
					<cfset tmp_cmd = ListAppend(tmp_cmd, arguments.arg_roomnumber[i])>
				</cfloop>

				<cfset strSQL2 = strSQL2 & " and (roomnumber in (#tmp_cmd#)) ">

			</cfif>
	
			<!--- リゾート --->
			<cfif (arraylen(arguments.arg_resort_code) IS 1) and (arguments.arg_resort_code[1] IS 0)>
			<cfelse>
				<cfif arraylen(arguments.arg_resort_code) neq 0>
	
					<cfset tmp_list = ArrayToList(arguments.arg_resort_code)>
					<cfset tmp_cmd = "">
					<cfloop from="1" to="#ListLen(tmp_list)#" index="i">
						<cfset tmp_cmd = ListAppend(tmp_cmd, arguments.arg_resort_code[i])>
					</cfloop>
	
					<cfset strSQL2 = strSQL2 & " and (resort_code in (#tmp_cmd#)) ">
	
				</cfif>
			</cfif>
				
			<!--- リゾート
			<cfif arguments.arg_resort_code neq 0>
				<cfset strSQL2 = strSQL2 & " and (resort_code = #arguments.arg_resort_code#) ">
			</cfif> --->

			<!--- 団体対応2008/6/17 0=全部、1=個人、2=団体 'zzz*****'--->
			<cfswitch expression=#arguments.arg_group_id#>

				<cfcase value="0">
				</cfcase>

				<cfcase value="1">
					<cfset strSQL2 = strSQL2 & " and (referencenumber not like 'zzz%') ">
				</cfcase>

				<cfcase value="2">
					<cfset strSQL2 = strSQL2 & " and (referencenumber like 'zzz%') ">
				</cfcase>

			</cfswitch>
			
			<cfreturn strSQL2>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

</cfcomponent>
