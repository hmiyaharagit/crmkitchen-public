﻿<cfcomponent hint="ＣＳ集計情報取得">
	<!---
	*************************************************************************************************
	* Class				: cstotal
	* Method			: getArea
	* Description		: エリアマスタ情報取得
	* Custom Attributes	: none
	* 					: 
	* Return Paramters	: retQuery(Query)
	* History			: 1) Coded by	2005/06/27 h.miyahara(NetFusion)
	*					: 2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getResort" access="remote" returntype="any">
		<cfargument name="resort_code" type="any" default="">
		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<!---2011/05/11 16:01 --->
			<!---ResortCode=0の場合、全施設で無い場合もあるので該当ユーザーの対象全施設に絞る --->
			<cfif arguments.resort_code eq 0 and (session.user_info.resort_code neq arguments.resort_code)>
				<cfset arguments.resort_code = session.user_info.resort_code>
			</cfif>

			<!--- 旅館 --->
			<cfquery name="m_resort" datasource = "#Application.datasource#">
				select	 convert(varchar,resort_code) items_id 
						,convert(varchar,resort_nm) items_nm
						,convert(varchar,resort_code) fla_mcname
						,'--' q_nm
				from	 #application.app00.dbu#.mst_resort
				where	 resort_type = 1
				<cfif arguments.resort_code neq 0>
				and	 	 resort_code in (#arguments.resort_code#)
				</cfif>
				order by seq
			</cfquery>
				
			<cfset rets = structnew()>
			<cfset structinsert(rets, "m_resort", m_resort)>

			<cfreturn rets>
			<cfsetting enablecfoutputonly="No">
			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	<!---
	*************************************************************************************************
	* Class				:recoveryrate
	* Method			:getAnswers
	* Description		:回答値取得
	* Custom Attributes	:questionnaire_id	設問ID
	* Return Paramters	:
	* History			:1) Coded by	2004/10/13 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getRequestcount" access="remote" returntype="any">
		<cfargument name="dcondtion" type="Boolean" default="false">
		<cfargument name="selecteddays" type="Array" default="">
		<cfargument name="arg_resort_code" type="Array" default="">
		
		<cfsetting enablecfoutputonly="yes">
		<cftry>

		<cfset retQuery = QueryNew("label, samples, answers, rate")>
		<!--- 抽出対象期間より期間内の回答者予約番号を抽出 --->
		<cfif arraylen(arguments.selecteddays) eq 2>
			<cfset wk_sdays1 = dateformat(arguments.selecteddays[1],'yyyy/mm/dd')>
			<cfset wk_sdays2 = dateformat(arguments.selecteddays[2],'yyyy/mm/dd')>
		<cfelse>
			<cfset wk_sdays1 = dateformat(arguments.selecteddays[1],'yyyy/mm/dd')>
			<cfset wk_sdays2 = dateformat(arguments.selecteddays[1],'yyyy/mm/dd')>
		</cfif>

		<!--- 母数取得 --->
		<cfquery name="total_request" datasource = "#Application.datasource#">
			<!--- 通常：TOTAL --->
			select	 count(a.referencenumber) as count
			from	 #Application.app00.dbu#.rtt_reserve as a
					,#Application.app00.dbu#.v_repeater as b
			where	 a.depart_date between '#wk_sdays1#' and '#wk_sdays2#'
			and		 a.customernumber = b.customernumber
			and		 isnull(a.reservationtype,'1') = '1'
			<cfif arguments.arg_resort_code[1] neq 0>
				and		 a.resort_code in (#arraytolist(arguments.arg_resort_code)#)
			</cfif>
			<cfloop list="#arraytolist(arguments.arg_resort_code)#" index="idx">
				and		 b.r#numberformat(idx,'00')# != 1
			</cfloop>

			<!--- 通常：手渡し --->
			union all
			select	 count(a.referencenumber) as count
			from	 #Application.app00.dbu#.rtt_reserve as a
					,#Application.app00.dbu#.v_repeater as b
			where	 a.depart_date between '#wk_sdays1#' and '#wk_sdays2#'
			and		 a.customernumber = b.customernumber
			and		 isnull(a.reservationtype,'1') = '1'
			<cfif arguments.arg_resort_code[1] neq 0>
				and		 a.resort_code in (#arraytolist(arguments.arg_resort_code)#)
			</cfif>
			<cfloop list="#arraytolist(arguments.arg_resort_code)#" index="idx">
				and		 b.r#numberformat(idx,'00')# != 1
			</cfloop>
			and		 isnull(a.surveymethod,'2') = 1

			<!--- 通常：WEB --->
			union all
			select	 count(a.referencenumber) as count
			from	 #Application.app00.dbu#.rtt_reserve as a
					,#Application.app00.dbu#.v_repeater as b
			where	 a.depart_date between '#wk_sdays1#' and '#wk_sdays2#'
			and		 a.customernumber = b.customernumber
			and		 isnull(a.reservationtype,'1') = '1'
			<cfif arguments.arg_resort_code[1] neq 0>
				and		 a.resort_code in (#arraytolist(arguments.arg_resort_code)#)
			</cfif>
			<cfloop list="#arraytolist(arguments.arg_resort_code)#" index="idx">
				and		 b.r#numberformat(idx,'00')# != 1
			</cfloop>
			and		 isnull(a.surveymethod,'2') = 2

			<!--- 通常：その他 --->
			union all
			select	 count(a.referencenumber) as count
			from	 #Application.app00.dbu#.rtt_reserve as a
					,#Application.app00.dbu#.v_repeater as b
			where	 a.depart_date between '#wk_sdays1#' and '#wk_sdays2#'
			and		 a.customernumber = b.customernumber
			and		 isnull(a.reservationtype,'1') = '1'
			<cfif arguments.arg_resort_code[1] neq 0>
				and		 a.resort_code in (#arraytolist(arguments.arg_resort_code)#)
			</cfif>
			<cfloop list="#arraytolist(arguments.arg_resort_code)#" index="idx">
				and		 b.r#numberformat(idx,'00')# != 1
			</cfloop>
			and		 isnull(a.surveymethod,'2') not in (1,2)

			<!--- リピーター：TOTAL --->
			union all
			select	 count(a.referencenumber) as count
			from	 dbo.rtt_reserve as a
					,dbo.v_repeater as b
			where	 a.depart_date between '#wk_sdays1#' and '#wk_sdays2#'
			and		 a.customernumber = b.customernumber
			and		 isnull(a.reservationtype,'1') = '1'
			<cfif arguments.arg_resort_code[1] neq 0>
				and		 a.resort_code in (#arraytolist(arguments.arg_resort_code)#)
			</cfif>
			<cfloop list="#arraytolist(arguments.arg_resort_code)#" index="idx">
				and		 b.r#numberformat(idx,'00')#  = 1
			</cfloop>

			<!--- リピーター：手渡し --->
			union all
			select	 count(a.referencenumber) as count
			from	 dbo.rtt_reserve as a
					,dbo.v_repeater as b
			where	 a.depart_date between '#wk_sdays1#' and '#wk_sdays2#'
			and		 a.customernumber = b.customernumber
			and		 isnull(a.reservationtype,'1') = '1'
			<cfif arguments.arg_resort_code[1] neq 0>
				and		 a.resort_code in (#arraytolist(arguments.arg_resort_code)#)
			</cfif>
			<cfloop list="#arraytolist(arguments.arg_resort_code)#" index="idx">
				and		 b.r#numberformat(idx,'00')#  = 1
			</cfloop>
			and		 isnull(a.surveymethod,'2') = 1

			<!--- リピーター：WEB --->
			union all
			select	 count(a.referencenumber) as count
			from	 dbo.rtt_reserve as a
					,dbo.v_repeater as b
			where	 a.depart_date between '#wk_sdays1#' and '#wk_sdays2#'
			and		 a.customernumber = b.customernumber
			and		 isnull(a.reservationtype,'1') = '1'
			<cfif arguments.arg_resort_code[1] neq 0>
				and		 a.resort_code in (#arraytolist(arguments.arg_resort_code)#)
			</cfif>
			<cfloop list="#arraytolist(arguments.arg_resort_code)#" index="idx">
				and		 b.r#numberformat(idx,'00')#  = 1
			</cfloop>
			and		 isnull(a.surveymethod,'2') = 2

			<!--- リピーター：その他 --->
			union all
			select	 count(a.referencenumber) as count
			from	 dbo.rtt_reserve as a
					,dbo.v_repeater as b
			where	 a.depart_date between '#wk_sdays1#' and '#wk_sdays2#'
			and		 a.customernumber = b.customernumber
			and		 isnull(a.reservationtype,'1') = '1'
			<cfif arguments.arg_resort_code[1] neq 0>
				and		 a.resort_code in (#arraytolist(arguments.arg_resort_code)#)
			</cfif>
			<cfloop list="#arraytolist(arguments.arg_resort_code)#" index="idx">
				and		 b.r#numberformat(idx,'00')#  = 1
			</cfloop>
			and		 isnull(a.surveymethod,'2') not in (1,2)

		</cfquery>


		<!--- 回答数 --->
		<cfquery name="total_recovery" datasource = "#Application.datasource#">
			<!--- 通常：TOTAL --->
			select	 count(distinct(r.referencenumber)) as count
			from	 #Application.app00.dbu#.rtt_reserve as r
					,#Application.app00.dbu#.hst_answers as a
					,#Application.app00.dbu#.v_repeater as c
			where	 r.depart_date	between '#wk_sdays1#' and '#wk_sdays2#'
			and		 r.customernumber = c.customernumber
			and		 a.questionnairesheet_id != 0
			<cfif arguments.arg_resort_code[1] neq 0>
				and	 r.resort_code in (#arraytolist(arguments.arg_resort_code)#)
			</cfif>
			<cfloop list="#arraytolist(arguments.arg_resort_code)#" index="idx">
				and		 c.r#numberformat(idx,'00')# != 1
			</cfloop>
			and		 r.referencenumber = a.referencenumber

			<!--- 通常：手渡し --->
			union all
			select	 count(distinct(r.referencenumber)) as count
			from	 #Application.app00.dbu#.rtt_reserve as r
					,#Application.app00.dbu#.hst_answers as a
					,#Application.app00.dbu#.v_repeater as c
			where	 r.depart_date	between '#wk_sdays1#' and '#wk_sdays2#'
			and		 r.customernumber = c.customernumber
			and		 a.questionnairesheet_id != 0
			<cfif arguments.arg_resort_code[1] neq 0>
				and	 r.resort_code in (#arraytolist(arguments.arg_resort_code)#)
			</cfif>
			<cfloop list="#arraytolist(arguments.arg_resort_code)#" index="idx">
				and		 c.r#numberformat(idx,'00')# != 1
			</cfloop>
			and		 r.referencenumber = a.referencenumber
			and		 isnull(r.surveymethod,'2') = 1

			<!--- 通常：WEB --->
			union all
			select	 count(distinct(r.referencenumber)) as count
			from	 #Application.app00.dbu#.rtt_reserve as r
					,#Application.app00.dbu#.hst_answers as a
					,#Application.app00.dbu#.v_repeater as c
			where	 r.depart_date	between '#wk_sdays1#' and '#wk_sdays2#'
			and		 r.customernumber = c.customernumber
			and		 a.questionnairesheet_id != 0
			<cfif arguments.arg_resort_code[1] neq 0>
				and	 r.resort_code in (#arraytolist(arguments.arg_resort_code)#)
			</cfif>
			<cfloop list="#arraytolist(arguments.arg_resort_code)#" index="idx">
				and		 c.r#numberformat(idx,'00')# != 1
			</cfloop>
			and		 r.referencenumber = a.referencenumber
			and		 isnull(r.surveymethod,'2') = 2

			<!--- 通常：その他 --->
			union all
			select	 count(distinct(r.referencenumber)) as count
			from	 #Application.app00.dbu#.rtt_reserve as r
					,#Application.app00.dbu#.hst_answers as a
					,#Application.app00.dbu#.v_repeater as c
			where	 r.depart_date	between '#wk_sdays1#' and '#wk_sdays2#'
			and		 r.customernumber = c.customernumber
			and		 a.questionnairesheet_id != 0
			<cfif arguments.arg_resort_code[1] neq 0>
				and	 r.resort_code in (#arraytolist(arguments.arg_resort_code)#)
			</cfif>
			<cfloop list="#arraytolist(arguments.arg_resort_code)#" index="idx">
				and		 c.r#numberformat(idx,'00')# != 1
			</cfloop>
			and		 r.referencenumber = a.referencenumber
			and		 isnull(r.surveymethod,'2') not in (1,2)

				
			<!--- リピーター：TOTAL --->
			union all
			select	 count(distinct(r.referencenumber)) as count
			from	 #Application.app00.dbu#.rtt_reserve as r
					,#Application.app00.dbu#.hst_answers as a
					,#Application.app00.dbu#.v_repeater as c
			where	 r.depart_date	between '#wk_sdays1#' and '#wk_sdays2#'
			and		 r.customernumber = c.customernumber
			and		 a.questionnairesheet_id = 0
			<cfif arguments.arg_resort_code[1] neq 0>
				and	 r.resort_code in (#arraytolist(arguments.arg_resort_code)#)
			</cfif>
			<cfloop list="#arraytolist(arguments.arg_resort_code)#" index="idx">
				and		 c.r#numberformat(idx,'00')#  = 1
			</cfloop>
			and		 r.referencenumber = a.referencenumber

			<!--- リピーター：手渡し --->
			union all
			select	 count(distinct(r.referencenumber)) as count
			from	 #Application.app00.dbu#.rtt_reserve as r
					,#Application.app00.dbu#.hst_answers as a
					,#Application.app00.dbu#.v_repeater as c
			where	 r.depart_date	between '#wk_sdays1#' and '#wk_sdays2#'
			and		 r.customernumber = c.customernumber
			and		 a.questionnairesheet_id = 0
			<cfif arguments.arg_resort_code[1] neq 0>
				and	 r.resort_code in (#arraytolist(arguments.arg_resort_code)#)
			</cfif>
			<cfloop list="#arraytolist(arguments.arg_resort_code)#" index="idx">
				and		 c.r#numberformat(idx,'00')#  = 1
			</cfloop>
			and		 r.referencenumber = a.referencenumber
			and		 isnull(r.surveymethod,'2') = 1

			<!--- リピーター：WEB --->
			union all
			select	 count(distinct(r.referencenumber)) as count
			from	 #Application.app00.dbu#.rtt_reserve as r
					,#Application.app00.dbu#.hst_answers as a
					,#Application.app00.dbu#.v_repeater as c
			where	 r.depart_date	between '#wk_sdays1#' and '#wk_sdays2#'
			and		 r.customernumber = c.customernumber
			and		 a.questionnairesheet_id = 0
			<cfif arguments.arg_resort_code[1] neq 0>
				and	 r.resort_code in (#arraytolist(arguments.arg_resort_code)#)
			</cfif>
			<cfloop list="#arraytolist(arguments.arg_resort_code)#" index="idx">
				and		 c.r#numberformat(idx,'00')#  = 1
			</cfloop>
			and		 r.referencenumber = a.referencenumber
			and		 isnull(r.surveymethod,'2') = 2

			<!--- リピーター：その他 --->
			union all
			select	 count(distinct(r.referencenumber)) as count
			from	 #Application.app00.dbu#.rtt_reserve as r
					,#Application.app00.dbu#.hst_answers as a
					,#Application.app00.dbu#.v_repeater as c
			where	 r.depart_date	between '#wk_sdays1#' and '#wk_sdays2#'
			and		 r.customernumber = c.customernumber
			and		 a.questionnairesheet_id = 0
			<cfif arguments.arg_resort_code[1] neq 0>
				and	 r.resort_code in (#arraytolist(arguments.arg_resort_code)#)
			</cfif>
			<cfloop list="#arraytolist(arguments.arg_resort_code)#" index="idx">
				and		 c.r#numberformat(idx,'00')#  = 1
			</cfloop>
			and		 r.referencenumber = a.referencenumber
			and		 isnull(r.surveymethod,'2') not in (1,2)
		</cfquery>

		<cfset label_list="TOTAL（通常）,手渡し（通常）,WEB（通常）,なし・不明（通常）,TOTAL（リピーター）,手渡し（リピーター）,WEB（リピーター）,なし・不明（リピーター）">
<!--- 
		<cfset label_list="TOTAL（通常）,なし・不明（通常）,TOTAL（リピーター）,なし・不明（リピーター）">
--->
		<cfloop query="total_request">
			<cfif count[currentrow] EQ 0>
				<cfset tmp_rate = 0>
			<cfelse>
				<cfset tmp_rate = (#total_recovery.count[currentrow]#/#count[currentrow]#)*100>
			</cfif>
			<cfset QueryAddRow(retQuery,1)>
	 		<cfset QuerySetCell(retQuery, "samples", #count[currentrow]#)>
	 		<cfset QuerySetCell(retQuery, "answers", #total_recovery.count[currentrow]#)>
			<cfset QuerySetCell(retQuery, "rate", #NumberFormat(tmp_rate,'._')#)>
			<cfset QuerySetCell(retQuery, "label", #ListGetAt(label_list,currentrow)#)>
		</cfloop>

		<cfreturn retQuery>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
	
	</cffunction>
	<!---
	*************************************************************************************************
	* Class				:recoveryrate
	* Method			:OutputCSV
	* Description		:OA検索結果一覧ＣＳＶ出力
	* Custom Attributes	:resultObj	出力リストのデータプロバイダ
	* Return Paramters	:
	* History			:1) Coded by	2004/10/05 h.miyahara(NetFusion)
	*					:2) Updated by	2005/10/11 h.miyahara(NetFusion) 集計条件追加
	*************************************************************************************************
	--->
	<cffunction name="OutputCSV" access="remote" returntype="any">
		<cfargument name="resultObj" type="Array" default="">
		<cfargument name="selecteddate" type="String" default="">
		<cfargument name="condseleted" type="String" default="">
		<cfargument name="resort_code" type="Numeric" default="0">

		<cftry>
			<!--- 改行コード除去 --->
			<cfloop from="1" to="#ArrayLen(resultObj)#" index="i">
				<cfset resultObj[i].label = #replacelist(resultObj[i].label,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
				<cfset resultObj[i].samples = #replacelist(resultObj[i].samples,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
				<cfset resultObj[i].answers = #replacelist(resultObj[i].answers,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
				<cfset resultObj[i].rate = #replacelist(resultObj[i].rate,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
			</cfloop>
			<!--- リゾート名 --->
			<cfif arguments.resort_code eq 0>
				<cfset Application.resortname = "共通">
			<cfelse>
				<cfquery name="getresort" datasource = "#Application.datasource#">
					select	 resort_nm 
					from	 #Application.app00.dbu#.mst_resort
					where	 resort_code = #arguments.resort_code#
				</cfquery>
				<cfset Application.resortname = "#getresort.resort_nm#">
			</cfif>
			<!--- ディレクトリ＆ファイル情報 --->
			<cfset filePath = ExpandPath(#application.outputDirectory#)>
			<cfset Dircheck = DirectoryExists( filePath & "\" & #Replace(session.user_info.user_ip,".","","all")# )>
			
			<!--- ディレクトリ存在チェック --->
			<cfif Dircheck eq "no">
				<cfdirectory action = "create" directory = "#filePath#\#Replace(session.user_info.user_ip,'.','','all')#">
			</cfif>
			
			<cfset UserDir = #Replace(session.user_info.user_ip,".","","all")# & "\">
			<cfset datepart = #DateFormat(Now(),'YYMMDD')# & #TimeFormat(Now(),'HHMMSS')#>
			<cfset filename = filePath & UserDir & datepart & "reults.xls">
			<cfset filecheck = FileExists( filename )>
			
			<!--- ディレクトリ内ファイル数チェック --->
			<cfdirectory 
				directory="#filePath#\#Replace(session.user_info.user_ip,'.','','all')#" 
				name="myCSVDirectory"
				sort="DATELASTMODIFIED ASC">
				
			<!--- 指定ファイル数以上は作らない。古いものから削除 --->
			<cfset FileHistory = 10>
			<cfif myCSVDirectory.RecordCount GTE FileHistory>
				<cffile action = "delete" file = "#filePath##UserDir##myCSVDirectory.Name#">
			</cfif>	
		
			<!--- ファイル存在チェック --->
			<cfif filecheck eq "no">
				<cfset arguments.selecteddate = Replace(arguments.selecteddate,',','|','all')>
				<cfset arguments.condseleted = Replace(arguments.condseleted,',','|','all')>
				<cfset arg_condseleted = iif(len(arguments.condseleted) neq 0,de('【#arguments.condseleted#】'),de(''))>

				<!--- xlsブック作成＆スタイル設定 --->
				<cfinclude template="cfpoi_style.cfm">

				<!--- xlsシート作成	--->
				<cfset sheet = wb.createSheet()/>
				<cfset wb.setSheetName(0,"回収率結果",wb.ENCODING_UTF_16)/>
				<cfset sheet.setColumnWidth(0, javacast("int" , 20*256 ))/>
				<cfset sheet.setColumnWidth(1, javacast("int" , 20*256 ))/>
				<cfset sheet.setColumnWidth(2, javacast("int" , 20*256 ))/>
				<cfset sheet.setColumnWidth(3, javacast("int" , 20*256 ))/>

				<!--- 条件エリア --->
				<cfset row = sheet.createRow(0)/>
				<cfset cell = row.createCell(0)/>
				<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
				<cfset cell.setCellStyle(strobj)/>
				<cfset cell.setCellValue("【#arguments.selecteddate#】#arg_condseleted#【出力日時：#dateformat(now(),'YYYY/MM/DD')# #timeformat(now(),'HH:MM')#】")>
				<cfset columnlength = 3>
				<!--- region(開始上部位置、開始左位置、終了下部位置、終了右位置) --->
				<cfset region = createObject("java","org.apache.poi.hssf.util.Region").init(0,0,2,javacast("int",columnlength))/>
				<cfset sheet.addMergedRegion(region)/>

				<!--- ヘッダーエリア --->
				<cfset headerlist="調査手段,予約数,回答数,回答率">
				<cfset row = sheet.createRow(3)/>
				<cfset cellseq = 0>
				<cfloop list="#headerlist#" index="h_idx">
					<cfset cell = row.createCell(javacast("int",cellseq))/>
					<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
					<cfset cell.setCellStyle(hstyleobj)/>
					<cfset cell.setCellValue("#h_idx#")>
					<cfset cellseq = cellseq+1>
				</cfloop>

				<!--- 出力内容整形 改行コード除去 --->
				<cfset rowseq = 4>
				<cfloop from="1" to="#ArrayLen(resultObj)#" index="cnt">
					<!--- 行追加 --->
					<cfset row = sheet.createRow(javacast("int",rowseq))/>

					<!--- 調査手段 --->
					<cfset cell = row.createCell(javacast("int",0))/>
					<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
					<cfset cell.setCellStyle(styleobj)/>
					<cfset cell.setCellValue("#resultObj[cnt].label#")>

					<!--- 予約数 --->
					<cfset cell = row.createCell(javacast("int",1))/>
					<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
					<cfset cell.setCellStyle(numstyleobj)/>
					<cfset cell.setCellValue("#resultObj[cnt].samples#")>

					<!--- 回答数 --->
					<cfset cell = row.createCell(javacast("int",2))/>
					<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
					<cfset cell.setCellStyle(numstyleobj)/>
					<cfset cell.setCellValue("#resultObj[cnt].answers#")>

					<!--- 回答率 --->
					<cfset cell = row.createCell(javacast("int",3))/>
					<cfset cell.setEncoding(cell.ENCODING_UTF_16)>
					<cfset cell.setCellStyle(ratestyleobj)/>
					<cfset cell.setCellValue("#resultObj[cnt].rate#")>

					<cfset rowseq = rowseq+1>
				</cfloop>
			</cfif>
			<cfset filename = filePath & UserDir & datepart & "reults.xls">
			<cfset fileOut = createObject("java","java.io.FileOutputStream").init("#filename#")/>
				
			<cfset wb.write(fileOut)/>
			<cfset fileOut.close()/>

			
			<!--- 戻値設定 --->
			<cfscript>
				executeinformation = StructNew();
				StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
				StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
				StructInsert(executeinformation,	"outputMsg",	"正常"						);
				StructInsert(executeinformation,	"outputPath",	#Application.outputDirectory# & #Replace(UserDir,"\","/","all")# & datepart & "reults.xls" );
				StructInsert(executeinformation,	"filename",		"http://#cgi.server_name#/#Application.outputDirectory#" & #Replace(UserDir,"\","/","all")# & datepart & "reults.xls" );
				StructInsert(executeinformation,	"newname",		"#Application.resortname##datepart#.xls" );
			</cfscript>
			<cfreturn executeinformation>
			
			<cfsetting enablecfoutputonly="No">
			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
</cfcomponent>
