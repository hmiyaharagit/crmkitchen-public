﻿<cfcomponent hint="顧客取得用ＣＦＣ">
	<!---
	*************************************************************************************************
	* Class				: guestinfo
	* Method			: getGuestList
	* Description		: 顧客リスト取得
	* Custom Attributes	: customernm		顧客名
	* 					: birthday			生年月日
	* 					: tel_no			電話番号
	* 					: address			住所
	* 					: email				E-Mail
	* 					: customernumber	顧客番号
	* 					: reserveid			予約番号
	* Return Paramters	: getGuest(Query)
	* History			: 1) Coded by	2004/09/17 h.miyahara(NetFusion)
	*					: 2) Updated by	2005/12/09 TAP連動	 h.miyahara(NetFusion) MOD
	*************************************************************************************************
	--->
	<cffunction name="getGuestList" access="remote" returntype="query">
		<cfargument name="custmoer_nm" type="string" default="">
		<cfargument name="tel" type="string" default="">
		<cfargument name="birth_date" type="string" default="">
		<cfargument name="address" type="string" default="">
		<cfargument name="EMail" type="string" default="">
		<cfargument name="surveymethod" type="string" default="">

		<cfargument name="reserve_code" type="string" default="">
		<cfargument name="referencenumber" type="string" default="">
		<cfargument name="customernumber" type="string" default="">
		<cfargument name="resort_code" type="string" default="">

		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<!---2011/05/11 有効参照範囲の施設取得 --->
			<cfif arguments.resort_code neq 0>
				<cfquery name="getloginUser" datasource = "#Application.datasource#">
					select	 resort_code
					from	m_loginuser
					where	login_id = '#session.user_info.login_id#'
					and		login_dv = 's'
				</cfquery>
				<cfset arguments.resort_code = ValueList(getloginUser.resort_code)>
			</cfif>

			<cfquery name="getGuest" datasource = "#Application.datasource#">
				select	 distinct a.resort_code
						,a.customernumber
						,a.customer_nm
						,a.customer_kn
						,a.birth_date
						,a.address
						,a.tel
						,a.email
						,a.depart_date
						,a.reserve_code
						,a.room_code
						,a.surveymethod
						,a.referencenumber
						,a.resort_nm
				from	 #Application.app00.dbu#.v_customer as a
				where	 0=0
				<cfif arguments.resort_code neq 0>
					and	 a.resort_code in ( #arguments.resort_code# )
				</cfif>
				<cfif Len(arguments.reserve_code) neq 0>
					and	 a.reserve_code = #arguments.reserve_code#
				</cfif>
				<cfif Len(arguments.custmoer_nm) NEQ 0>
					and	 a.customer_nm like '%#arguments.custmoer_nm#%' or a.customer_kn like '%#arguments.custmoer_nm#%'
				</cfif>
				<cfif Len(arguments.birth_date) NEQ 0>
					and	 a.birth_date = '#dateformat(arguments.birth_date,'yyyy/m/d')#'
				</cfif>
				<cfif Len(arguments.tel) NEQ 0>
					and	(a.phone_number_1 like '#arguments.tel#%' or a.phone_number_2 like '#arguments.tel#%')
				</cfif>
				<cfif Len(arguments.address) NEQ 0>
					and a.address like '%#arguments.address#%'
				</cfif>
				<cfif Len(arguments.EMail) NEQ 0>
					and a.mail_address_1 like '%#arguments.EMail#%' or a.mail_address_2 like '%#arguments.EMail#%'
				</cfif>
<!--- 
				<cfif Len(arguments.reserve_code) NEQ 0>
					and	a.reserve_code like '%#arguments.reserve_code#%'
				</cfif>
 --->
				<cfif Len(arguments.referencenumber) NEQ 0>
					and	a.referencenumber like '%#arguments.referencenumber#%'
				</cfif>
				<cfif Len(arguments.customernumber) NEQ 0>
					and	a.customernumber like '%#arguments.customernumber#%'
				</cfif>
				<cfif Len(arguments.surveymethod) NEQ 0 and arguments.surveymethod NEQ '-1'>
					and a.surveymethod = '#arguments.surveymethod#'
				</cfif>
	 			order by a.depart_date desc
	 		</cfquery>
			<!--- Flash描画速度対応 --->
			<cfif getGuest.recordcount GT 100>
				<cfquery name="qoq_getGuest" dbtype="query" maxrows="100">
					select * from getGuest
				</cfquery>
				<cfset QueryAddColumn(qoq_getGuest,"cnt",ArrayNew(1))>
				<cfset QuerySetCell(qoq_getGuest,"cnt",getGuest.recordcount,1)>
				<cfreturn qoq_getGuest>
			</cfif>
		
			<!--- <cfset Flash.pagesize = 15> --->
			<cfreturn getGuest>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfdump var=#cfcatch#>
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
		
	</cffunction>


	<!---
	*************************************************************************************************
	* Class				:guestinfo
	* Method			:DefaultResultOutputCSV
	* Description		:スペックシート検索【通常】結果一覧ＣＳＶ出力
	* Custom Attributes	:resultObj	出力リストのデータプロバイダ
	* Return Paramters	:
	* History			:1) Coded by	2004/10/05 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="DefaultResultOutputCSV" access="remote" returntype="any">
		<cfargument name="resultObj" type="Array" default="">
		<cfargument name="resort_code" type="String" default="">
		
		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<cfloop from="1" to="#ArrayLen(arguments.resultObj)#" index="i">
				<cfset arguments.resultObj[i].customer_nm = #replace(arguments.resultObj[i].customer_nm,"#chr(44)#","，","ALL")#>
				<cfset arguments.resultObj[i].customer_kn = #replace(arguments.resultObj[i].customer_kn,"#chr(44)#",".","ALL")#>
				<cfset arguments.resultObj[i].tel = #replace(arguments.resultObj[i].tel,"#chr(44)#",".","ALL")#>
				<cfset arguments.resultObj[i].email = #replace(arguments.resultObj[i].email,"#chr(44)#",".","ALL")#>
			</cfloop>
			<!--- リゾート名 --->
			<cfif arguments.resort_code eq 0>
				<cfset Application.resortname = "共通">
			<cfelse>
				<cfquery name="getresort" datasource = "#Application.datasource#">
					select	 resort_nm 
					from	 #Application.app00.dbu#.mst_resort
					where	 resort_code in (#arguments.resort_code#)
				</cfquery>
				<cfset Application.resortname = "#valuelist(getresort.resort_nm,'-')#">
			</cfif>

			<!--- ディレクトリ＆ファイル情報 --->
			<cfset filePath = ExpandPath(#application.outputDirectory#)>
			<cfset Dircheck = DirectoryExists( filePath & "\" & #Replace(session.user_info.user_ip,".","","all")# )>
			
			<!--- ディレクトリ存在チェック --->
			<cfif Dircheck eq "no">
				<cfdirectory action = "create" directory = "#filePath#\#Replace(session.user_info.user_ip,'.','','all')#">
			</cfif>
			
			<cfset UserDir = #Replace(session.user_info.user_ip,".","","all")# & "\">
			<cfset datepart = #DateFormat(Now(),'YYMMDD')# & #TimeFormat(Now(),'HHMMSS')#>
			<cfset filename = filePath & UserDir & datepart & "CRMResultsList.csv">
			<cfset filecheck = FileExists( filename )>
			
			<!--- ディレクトリ内ファイル数チェック --->
			<cfdirectory 
				directory="#filePath#\#Replace(session.user_info.user_ip,'.','','all')#" 
				name="myCSVDirectory"
				sort="DATELASTMODIFIED ASC">
				
			<!--- 指定ファイル数以上は作らない。古いものから削除 --->
			<cfset FileHistory = 10>
			<cfif myCSVDirectory.RecordCount GTE FileHistory>
				<cffile action = "delete" file = "#filePath##UserDir##myCSVDirectory.Name#">
			</cfif>	
		
			<!--- ファイル存在チェック --->
			<cfif filecheck eq "no">
				<cfset captionData = "出発日" & "," & "部屋番号" & "," & "予約番号" & "," & "顧客番号" & "," & "顧客名" & "," & "カナ" & "," & "照会用番号" & "," & "誕生日" & "," & "TEL" & "," & "EMAIL">	
				<cffile action = "write" file=#filename# output=#captionData#>
				<!--- 出力内容整形 --->
				<cfloop from="1" to="#ArrayLen(resultObj)#" index="i">
					<cfset outputData = 
						#resultObj[i].depart_date# & "," & #resultObj[i].room_code# & "," & #resultObj[i].reserve_code# & "," & #resultObj[i].customernumber# & "," & #resultObj[i].customer_nm# & "," & #resultObj[i].customer_kn# & "," & #resultObj[i].referencenumber# & "," & #resultObj[i].birth_date# & "," & #resultObj[i].tel# & "," & #resultObj[i].email#>
					<cffile action = "append" file=#filename# output=#outputData#>
				</cfloop>
			</cfif>
			
			<!--- 戻値設定 --->		
			<cfscript>
				executeinformation = StructNew();
				StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
				StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
				StructInsert(executeinformation,	"outputMsg",	"正常"						);
				StructInsert(executeinformation,	"outputPath",	#Application.outputDirectory# & #Replace(UserDir,"\","/","all")# & datepart & "CRMResultsList.csv" );
				StructInsert(executeinformation,	"filename",		"http://#cgi.server_name#/#Application.outputDirectory#" & #Replace(UserDir,"\","/","all")# & datepart & "CRMResultsList.csv" );
				StructInsert(executeinformation,	"newname",		"#Application.resortname##datepart#.csv" );
			</cfscript>
			<cfreturn executeinformation>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
	<!---
	*************************************************************************************************
	* Class				:guestinfo
	* Method			:RegetValueInfo
	* Description		:ValueInformation再取得
	* Custom Attributes	:reserve_code		予約コード（配列）
	* 					:print_mode			プリントモード
	* Return Paramters	:
	* History			:1) Coded by	2004/09/21 h.miyahara(NetFusion)
	*************************************************************************************************
	--->
	<cffunction name="RegetValueInfo" access="remote" returntype="query" hint="">
		<cfargument name="reserve_code" type="Array" default="">
		<cfargument name="print_mode" type="string" default="">

		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<!--- 戻値クエリ作成 --->
			<cfset retQuery = QueryNew("valueinfo, print_mode")>

			<cfloop from="1" to="#ArrayLen(arguments.reserve_code)#" index="i">
				<cfquery name="getValueInfo" datasource = "#Application.datasource#">
					select
						valueinfo
					from
						hst_valueinformation
					where
						reserve_code = #arguments.reserve_code[i]#
				</cfquery>
				<cfset QueryAddRow(retQuery, 1)>
		 		<cfset QuerySetCell(retQuery, "valueinfo", #getValueInfo.valueinfo#)>
		 		<cfset QuerySetCell(retQuery, "print_mode", #arguments.print_mode#)>

			</cfloop>
			
			<!--- <cfdump var=#getRepeater# label="指定回数宿泊者リピーター"><cfabort> --->

			<!--- <cfset Flash.pagesize = 15> --->
			<cfreturn retQuery>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	
	<!---
	*************************************************************************************************
	* Class				:guestinfo
	* Method			:sweeper
	* Description		:ValueInformation登録事前処理
	* Custom Attributes	:
	* Return Paramters	:
	* History			:1) Coded by	2004/12/13 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="sweeper" access="remote" returntype="any">
		<cftry>

		<cftransaction action="BEGIN">
			<cfquery name="chksweep" datasource = "#Application.datasource#">
				select
					updatedby
				from 
					hst_valueinformation
				where
					updatedby = '#session.user_info.user_ip#'
				and
					checkout = 1	
			</cfquery>
			
			<!--- チェックイン処理 --->
			<cfif chksweep.recordCount NEQ 0>
				<cfloop query="chksweep">
					<cfquery name="checkIn" datasource = "#Application.datasource#">
						UPDATE
							hst_valueinformation
						SET
							checkout = 0,
							updatedby = '#session.user_info.user_ip#',
							updated = #now()#
						where
							updatedby = '#updatedby#'
					</cfquery>
				</cfloop>
			</cfif>

		</cftransaction>

		<cfscript>
			executeinformation = StructNew();
			StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
			StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
		</cfscript>

		<cfreturn executeinformation>
		<cfcatch type="any">
		<cftransaction action="rollback"/>
<!--- 
		<cf_error_log
			label = "setUserAnswers"
			errorType = "#cfcatch.type#"
			message = "#cfcatch.message#"
			detail = "#cfcatch.detail#">
		<cfsetting enablecfoutputonly="No">
 --->
 		<cfrethrow>
	</cfcatch>
	</cftry>
	</cffunction>

	<!---
	*************************************************************************************************
	* Class				: guestinfo
	* Method			: getSurvey
	* Description		: 
	* Custom Attributes	: none
	* 					: 
	* Return Paramters	: retQuery(Query)
	* History			: 1) Coded by	2005/10/13 h.miyahara(NetFusion)
	*					: 2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getSurvey" access="remote" returntype="query" >
		<cfsetting enablecfoutputonly="yes">
		<cftry>

			<cfquery name="retQuery" datasource = "#Application.datasource#">
				select
					control_nm,
					control_id
				from
					mst_control
				where
					controldiv_id = 8
				and
					active_flg = 1
				order by
					control_id
			</cfquery>

			<cfreturn retQuery>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>

	</cffunction>	
</cfcomponent>