﻿<cfcomponent hint="メール一括送信用ＣＦＣ">
	<cfparam name="session.user_info.resort_code" default="">
	<cfparam name="session.user_info.resort_id" default="">
	<cfparam name="session.user_info.user_ip" default="">
	<!---
	*************************************************************************************************
	* Class				:mailPackage
	* Method			:getMailSendList
	* Description		:顧客詳細検索
	* Custom Attributes	:aStartDate			到着予定日（開始日）
	* 					:aEndDate			到着予定日（終了日）
	* 					:dStartDate			出発予定日（開始日）
	* 					:dEndDate			出発予定日（終了日）
	* 					:stayHistory		利用回数
	* Return Paramters	:
	* History			:1) Coded by	2004/09/21 h.miyahara(NetFusion)
	*					:2) Updated by	2005/04/22 h.miyahara(NetFusion)	利用経験の取得条件変更
	*************************************************************************************************
	--->
	<cffunction name="getMailSendList" access="remote" returntype="query" hint="">
		<cfargument name="dStartDate" type="string" default="">
		<cfargument name="dEndDate" type="string" default="">
		<cfargument name="customer_nm" type="string" default="">
		<cfargument name="EMail" type="string" default="">
		<cfargument name="customernumber" type="string" default="">
		<cfargument name="sendflg" type="numeric" default="0">
		<cfargument name="surveymethod" type="string" default="">
		<cfargument name="usedflg" type="any" default="">
		<cfargument name="resort_code" type="string" default="">

		<cfsetting enablecfoutputonly="yes">
		<cftry>

			<!--- 送信状態 --->
			<cfset SendCondStr = "">
			<cfif arguments.sendflg EQ 1>
				<cfset SendCondStr = " and d.deliverd_date is not null">
			<cfelseif arguments.sendflg EQ 2>
				<cfset SendCondStr = " and d.deliverd_date is null">
			</cfif>

			<cfquery name="getMailSendList" datasource = "#Application.datasource#">
				select	 distinct b.customernumber
						,case when len(b.name) > 0 
						 then ( b.name ) 
						 else 'お客'
						 end customer_nm
						,isnull(b.name_kana,'') as customer_kana
						,case when b.mail_address_1 != '' 
						 then b.mail_address_1
						 when b.mail_address_1 = '' 
						 then b.mail_address_2
						 else b.mail_address_1
						 end  email
						,a.depart_date as depature_date
						,a.referencenumber
						,a.resort_code
						,a.reserve_code
						,a.surveymethod
						,case when d.deliverd_date is null then '未送信'
						 else '送信済み'
						 end  sendflg
						,case when isnull(a.repeatcount,0) > 0 then 'R' else '' end repeater
				from	 			#Application.app00.dbu#.rtt_reserve as a

				inner join			#Application.app00.dbu#.rtm_customer as b
				on		 			a.resort_code = b.resort_code
				and		 			a.customernumber = b.customernumber

				left outer join		#Application.app00.dbu#.hst_deliverinformation as d
				on					a.referencenumber = d.referencenumber
				where	 a.depart_date between '#arguments.dstartdate#' and '#arguments.denddate#'
				and		 (b.mail_address_1 != '' or b.mail_address_2 != '')
				<cfif arguments.resort_code neq 0>
				and		 a.resort_code =  #arguments.resort_code# 
				</cfif>
				<cfif len(arguments.customer_nm) neq 0>
				and		 (b.name like '%#arguments.customer_nm#%' or b.name_kana like '%#arguments.customer_nm#%')
				</cfif>
				<cfif len(arguments.email) neq 0>
				and		 (b.mail_address_1 like '%#arguments.email#%' or b.mail_address_2 like '%#arguments.email#%')
				</cfif>
				<cfif len(arguments.customernumber) neq 0>
				and		 b.customernumber like '%#arguments.customernumber#%'
				</cfif>
				<cfif len(arguments.surveymethod) neq 0 and arguments.surveymethod neq '-1'>
				and		 a.surveymethod = '#arguments.surveymethod#'
				</cfif>
				<cfif len(arguments.usedflg) neq 0>
					<cfif arguments.usedflg eq 0>
				and		case when isnull(a.repeatcount,0) > 0 then 'R' else '' end = ''
					<cfelse>
				and		case when isnull(a.repeatcount,0) > 0 then 'R' else '' end = 'R'
					</cfif>
				</cfif>



and a.depart_date < '2015/02/01'



				#sendcondstr#	order by a.depart_date
	 		</cfquery>
			<cfreturn getMailSendList>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>

	</cffunction>


	<!---
	*************************************************************************************************
	* Class				:mailPackage
	* Method			:getmailTemplateList
	* Description		:顧客詳細検索
	* Custom Attributes	:aStartDate			到着予定日（開始日）
	* 					:aEndDate			到着予定日（終了日）
	* 					:dStartDate			出発予定日（開始日）
	* 					:dEndDate			出発予定日（終了日）
	* 					:stayHistory		利用回数
	* Return Paramters	:
	* History			:1) Coded by	2004/09/21 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="gettemplatelist" access="remote" returntype="query">
		<cfargument name="resort_code" type="String" default="">
		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<!--- 2009/12/26 同一アカウント対応 
			<cfif len(session.user_info.resort_code) neq 0>
				<cfset arguments.resort_code = session.user_info.resort_code>
			</cfif>
			--->
			<!--- 送信可能対象施設判定 2010/11/05 ADD Miya --->
			<cfquery name="getSentable" datasource = "#Application.datasource#">
				select resort_code from #Application.app00.dbu#.m_loginuser where login_id = '#session.user_info.login_id#' and login_dv in ('s')
			</cfquery>
			<cfif getSentable.recordcount neq 0>
<!--- 
				<cfset arguments.resort_code =valuelist(getSentable.resort_code)>
 --->
			</cfif>


			<cfset retq = querynew("template,rtemplate")>
			<cfset queryaddrow(retq,1)>
			<cfquery name="getTemplateList" datasource = "#Application.datasource#">
				select	 m.mail_nm as label
						,m.mailtemplate_id as data
						,r.resort_code as resort_code
				from	 #Application.app00.dbu#.mst_resort as r
						,#Application.app00.dbu#.mst_mailtemplate as m
				where	 r.resort_code = m.resort_code
				<cfif arguments.resort_code neq 0>
				and		 R.resort_code in( #arguments.resort_code# )
				</cfif>
				and		 r.resort_type = 1
				and		 m.active_flg = 1
				and		 m.active_flg = 1
				and		 m.mail_div = 0
			</cfquery>
			<cfquery name="getrTemplateList" datasource = "#Application.datasource#">
				select	 m.mail_nm as label
						,m.mailtemplate_id as data
						,r.resort_code as resort_code
				from	 #Application.app00.dbu#.mst_resort as r
						,#Application.app00.dbu#.mst_mailtemplate as m
				where	 r.resort_code = m.resort_code
				<cfif arguments.resort_code neq 0>
				and		 R.resort_code in( #arguments.resort_code# )
				</cfif>
				and		 r.resort_type = 1
				and		 m.active_flg = 1
				and		 m.mail_div = 1
			</cfquery>

			<!--- リピーター用のテンプレートが無ければ通常テンプレートを割り当てる --->
			<cfset querysetcell(retq,"template",getTemplateList)>
			<cfif getrTemplateList.recordcount eq 0>
				<cfset querysetcell(retq,"rtemplate",getTemplateList)>
			<cfelse>
				<cfset querysetcell(retq,"rtemplate",getrTemplateList)>
			</cfif>

			<cfreturn retq>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>

	</cffunction>

	<!---
	*************************************************************************************************
	* Class				:mailPackage
	* Method			:setReaction
	* Description		:対応情報セット
	* Custom Attributes	:referencenumber		照会用番号
	* Return Paramters	:
	* History			:1) Coded by	2004/11/04 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="setReaction" access="remote" returntype="any">
		<cfargument name="referencenumber" type="String" default="">
		<cfargument name="Opt" type="boolean" default="">

		<cfsetting enablecfoutputonly="yes">
		<cftry>
		
			<cfquery name="sweepReaction" datasource = "#Application.datasource#">
				delete
				from
					#Application.app00.dbu#.hst_deliverinformation
				where
					referencenumber = '#arguments.referencenumber#'
			</cfquery>
			
			<cfif Opt EQ true>
				<cfquery name="insertReaction" datasource = "#Application.datasource#">
					insert into
						#Application.app00.dbu#.hst_deliverinformation(
							referencenumber,
							cope_flg
					)values(
						'#arguments.referencenumber#',
						1
					)						
				</cfquery>		
			</cfif>
			
			<!--- 戻値設定 --->
			<cfscript>
				executeinformation = StructNew();
				StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
				StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
				StructInsert(executeinformation,	"outputMsg",	"正常"						);
			</cfscript>
			<cfreturn executeinformation>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>

	</cffunction>

	<!---
	*************************************************************************************************
	* Class				:mailPackage
	* Method			:SendListResultOutputCSV
	* Description		:スペックシート検索【通常】結果一覧ＣＳＶ出力
	* Custom Attributes	:resultObj	出力リストのデータプロバイダ
	* Return Paramters	:
	* History			:1) Coded by	2004/10/05 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="SendListResultOutputCSV" access="remote" returntype="any">
		<cfargument name="resultObj" type="Array" default="">
		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<cfloop from="1" to="#ArrayLen(resultObj)#" index="i">
				<cfset resultObj[i].customer_nm = #replacelist(resultObj[i].customer_nm,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
				<cfset resultObj[i].email = #replacelist(resultObj[i].email,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
				<cfset resultObj[i].customer_nm = #replace(resultObj[i].customer_nm,"#chr(44)#","，","ALL")#>
				<cfset resultObj[i].email = #replace(resultObj[i].email,"#chr(44)#",".","ALL")#>
			</cfloop>
			
			<!--- ディレクトリ＆ファイル情報 --->
			<cfset filePath = ExpandPath(#application.outputDirectory#)>
			<cfset Dircheck = DirectoryExists( filePath & "\" & #Replace(session.user_info.user_ip,".","","all")# )>
			
			<!--- ディレクトリ存在チェック --->
			<cfif Dircheck eq "no">
				<cfdirectory action = "create" directory = "#filePath#\#Replace(session.user_info.user_ip,'.','','all')#">
			</cfif>
			
			<cfset UserDir = #Replace(session.user_info.user_ip,".","","all")# & "\">
			<cfset datepart = #DateFormat(Now(),'YYMMDD')# & #TimeFormat(Now(),'HHMMSS')#>
			<cfset filename = filePath & UserDir & datepart & "mlResultsList.csv">
			<cfset filecheck = FileExists( filename )>
			
			<!--- ディレクトリ内ファイル数チェック --->
			<cfdirectory 
				directory="#filePath#\#Replace(session.user_info.user_ip,'.','','all')#" 
				name="myCSVDirectory"
				sort="DATELASTMODIFIED ASC">
				
			<!--- 指定ファイル数以上は作らない。古いものから削除 --->
			<cfset FileHistory = 10>
			<cfif myCSVDirectory.RecordCount GTE FileHistory>
				<cffile action = "delete" file = "#filePath##UserDir##myCSVDirectory.Name#">
			</cfif>	
		
			<!--- ファイル存在チェック --->
			<cfif filecheck eq "no">
			
				<cfset captionData = "出発日" & "," & "顧客番号" & "," & "氏名（カナ）" & "," & "E-Mail" & "," & "送信状態">	
				<cffile action = "write" file=#filename# output=#captionData#>
				<cfloop from="1" to="#ArrayLen(resultObj)#" index="i">
					<cfset outputData = 
						#resultObj[i].depature_date# & "," & #resultObj[i].customernumber# & "," & #resultObj[i].customer_nm# & "," & #resultObj[i].email# & "," & #resultObj[i].sendflg#>
					<cffile action = "append" file=#filename# output=#outputData#>
				</cfloop>
			
			</cfif>
			
			<!--- 戻値設定 --->
			<cfscript>
				executeinformation = StructNew();
				StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
				StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
				StructInsert(executeinformation,	"outputMsg",	"正常"						);
				StructInsert(executeinformation,	"outputPath",	#Application.outputDirectory# & #Replace(UserDir,"\","/","all")# & datepart & "mlResultsList.csv" );
				StructInsert(executeinformation,	"filename",		"http://#cgi.server_name#/#Application.outputDirectory#" & #Replace(UserDir,"\","/","all")# & datepart & "mlResultsList.csv" );
				StructInsert(executeinformation,	"newname",		"_#datepart#_sendList.csv" );
			</cfscript>
			<cfreturn executeinformation>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>

	</cffunction>
	
	<!---
	*************************************************************************************************
	* Class				: guestinfo
	* Method			: getSurvey
	* Description		: 
	* Custom Attributes	: none
	* 					: 
	* Return Paramters	: retQuery(Query)
	* History			: 1) Coded by	2005/10/13 h.miyahara(NetFusion)
	*					: 2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getSurvey" access="remote" returntype="query" >
		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<cfquery name="retQuery" datasource = "#Application.datasource#">
				select	 distinct control_nm
						,control_id
				from	 #Application.app00.dbu#.mst_control
				where	 controldiv_id = 8
				and		 active_flg = 1
				order by control_id
			</cfquery>

			<cfreturn retQuery>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>

	</cffunction>	

	<!---
	*************************************************************************************************
	* Class				:mailTemplate
	* Method			:gettemplatelist
	* Description		: 
	* Custom Attributes	:mailtemplate_id
	* Return Paramters	:
	* History			:1) Coded by	2004/09/13 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getresort" access="remote" returntype="query">
		<cfargument name="resort_code" type="String" default="">
		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<!--- 2009/12/26 同一アカウント対応 --->
			<cfif len(session.user_info.resort_code) neq 0>
				<cfset arguments.resort_code = session.user_info.resort_code>
			</cfif>
			<!--- 送信可能対象施設判定 2010/11/05 ADD Miya --->
			<cfquery name="getSentable" datasource = "#Application.datasource#">
				select resort_code from #Application.app00.dbu#.m_loginuser where login_id = '#session.user_info.login_id#' and login_dv in ('s')
			</cfquery>
			<cfif getSentable.recordcount neq 0>
				<cfset arguments.resort_code =valuelist(getSentable.resort_code)>
			</cfif>

			<cfquery name="getresort" datasource = "#Application.datasource#">
				select	 resort_code as data
						,resort_nm as label
				from	 #Application.app00.dbu#.mst_resort
				where	 0 = 0
				<cfif arguments.resort_code neq 0>
				and		 resort_code = #arguments.resort_code#
				</cfif>
				and		 resort_type = 1
				order by seq
			</cfquery>
			<cfreturn getresort>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>


</cfcomponent>