﻿<cfcomponent hint="ＣＳ集計情報取得">
	<!---
	*************************************************************************************************
	* Class				:csmulti
	* Method			:getAnswers
	* Description		:回答値取得
	* Custom Attributes	:questionnaire_id	設問ID
	* Return Paramters	:
	* History			:1) Coded by	2004/10/13 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getAnswers" access="remote" returntype="any">
		<cfargument name="sStartDate" type="String" default="">
		<cfargument name="sEndDate" type="String" default="">
		<cfargument name="area_code" type="String" default="">
		<cfargument name="together_code" type="String" default="">
		<cfargument name="roomtype" type="String" default="">
		<cfargument name="nights" type="numeric" default="">
		<cfargument name="purpose_code" type="String" default="">
		<cfargument name="cognition_code" type="String" default="">
		<cfargument name="reservation_code" type="String" default="">
		<cfargument name="room_code" type="numeric" default="">
		<cfargument name="roomcontrol_code" type="String" default="">
		<cfargument name="aging_code" type="String" default="">
						
		<cfsetting enablecfoutputonly="yes">
		<cftry>
		
		<cfset arguments.questionnaire_id = Application.q_value_id>
		
		<!--- 対象設問の回答候補値を取得 --->
		<cfquery name="GetAnsweroptions" datasource = "#Application.datasource#">
			select
				O.options_no,
				O.options_label
			from
				#Application.app00.dbu#.mst_questionnaire as Q,
				#Application.app00.dbu#.mst_answeroptions as O
			where
				O.questionnaire_id = Q.questionnaire_id
			and
				Q.questionnaire_id = #arguments.questionnaire_id#
			and
				O.active_flg = 1
			order by
				O.options_no
		</cfquery>
		
		<cfset tmp_answers = "">
		<cfloop query="GetAnsweroptions">
			<cfset tmp_answers = ListAppend(tmp_answers,"A"&#options_no#)>
			<cfset tmp_answers = ListAppend(tmp_answers,"A"&#options_no#&"_average")>
		</cfloop>

		<!--- 戻値クエリ作成 --->
		<!---<cfset retQuery = QueryNew("questionnaire_id, samples, #tmp_answers#")>--->
		<cfset retQuery = QueryNew(" samples, rate, label, options_no")>
		<!--- 抽出対象期間より期間内の回答者予約番号を抽出 --->
		<cfquery name="tmp_getAnswers" datasource = "#Application.datasource#">
			select
				AD.questionnaire_id,
				AD.answer,
				AD.answertext,
				A.referencenumber,
				A.area_code,
				Q.questionnaire_type,
				R.reserve_code,
				R.reservationroute,
				R.room_code,
				R.roomtype,
				R.nights
			from
				#Application.app00.dbu#.hst_answers as A,
				#Application.app00.dbu#.hst_answersdetails as AD,
				#Application.app00.dbu#.mst_questionnaire as Q,
				#Application.app00.dbu#.rtt_reserve as R
			where
				A.referencenumber = AD.referencenumber
			and
				A.referencenumber = R.referencenumber
			and
				A.questionnairesheet_id = AD.questionnairesheet_id
			and
				AD.questionnairesheet_id = 0
			and
				Q.questionnaire_id = AD.questionnaire_id
			and
				A.depart_date	between '#arguments.sStartDate#' and '#arguments.sEndDate#'
		</cfquery>

		<!--- 共通集計条件絞込み処理 --->
		<cfinvoke
			component		= "conditionsetting"
			method			= "moresearch"
			temp_data = #tmp_getAnswers#
			temp_cond = #arguments#
			returnVariable	="getAnswers">
	
		<!--- 対象者無し --->
		<cfif getAnswers.recordcount EQ 0>
			<cfreturn retQuery>
		</cfif>
		<!--- <cfdump var=#getAnswers#> --->
	
		<!--- 2005/04/28 h.miyahara(NetFusion) ADD オプション条件 end --->
		<cfset QueryAddRow(retQuery, 1)>
		<!--- 2005/08/20 h.miyahara(NetFusion) MOD 母数算出 B --->
		<cfquery name="denominator" dbtype = "query">
			select distinct
				referencenumber
			from
				getAnswers
		</cfquery>
		
		<cfset samples = denominator.recordcount>
		<cfif samples EQ 0>
			<cfreturn retQuery>
		</cfif>
		<cfset temp_0res = 0>
		<cfloop query="denominator">
			<cfquery name="res0Check" dbtype = "query">
				select
					referencenumber,
					questionnaire_id,
					answer
				from
					getAnswers
				where
					questionnaire_id = #arguments.questionnaire_id#
				and
					referencenumber = '#referencenumber#'
			</cfquery>
			
			<cfif res0Check.recordcount EQ 0>
				<cfset temp_0res = temp_0res+1>
			</cfif>
		</cfloop>
		
		<!--- 2005/08/20 h.miyahara(NetFusion) MOD 母数算出 E --->
		<cfset QuerySetCell(retQuery, "samples", #samples#)>
		<cfset QuerySetCell(retQuery, "rate", 100)>
		<cfset QuerySetCell(retQuery, "label", "TOTAL")>
		<cfset QuerySetCell(retQuery, "options_no", "")>

		<!--- 2005/10/11 h.miyahara(NetFusion) MOD 100点満点レンジ機能追加 B --->
			<cfset tmpOptions = QueryNew("range1,range2,range3,range4,range5,range6,range7,range8,range9,range10,range11")>
			<cfloop query="denominator">
				<cfquery name="aCheck" datasource = "#Application.datasource#">
					select
						AD.questionnaire_id,
						AD.answer,
						AD.answertext,
						A.referencenumber
					from
						#Application.app00.dbu#.hst_answers as A,
						#Application.app00.dbu#.hst_answersdetails as AD,
						#Application.app00.dbu#.mst_questionnaire as Q
					where
						A.referencenumber = AD.referencenumber
					and
						A.questionnairesheet_id = AD.questionnairesheet_id
					and
						Q.questionnaire_id = AD.questionnaire_id
					and	
						Q.questionnaire_id = #arguments.questionnaire_id#
					and
						A.referencenumber = '#referencenumber#'
				</cfquery>
				<cfset QueryAddrow(tmpOptions,1)>

				<cfif aCheck.answertext GTE 0 AND aCheck.answertext LTE 9>
			 		<cfset QuerySetCell(tmpOptions, "range1", #aCheck.answertext#)>
				<cfelseif aCheck.answertext GTE 10 AND aCheck.answertext LTE 19>
			 		<cfset QuerySetCell(tmpOptions, "range2", #aCheck.answertext#)>
				<cfelseif aCheck.answertext GTE 20 AND aCheck.answertext LTE 29>
			 		<cfset QuerySetCell(tmpOptions, "range3", #aCheck.answertext#)>
				<cfelseif aCheck.answertext GTE 30 AND aCheck.answertext LTE 39>
			 		<cfset QuerySetCell(tmpOptions, "range4", #aCheck.answertext#)>
				<cfelseif aCheck.answertext GTE 40 AND aCheck.answertext LTE 49>
			 		<cfset QuerySetCell(tmpOptions, "range5", #aCheck.answertext#)>
				<cfelseif aCheck.answertext GTE 50 AND aCheck.answertext LTE 59>
			 		<cfset QuerySetCell(tmpOptions, "range6", #aCheck.answertext#)>
				<cfelseif aCheck.answertext GTE 60 AND aCheck.answertext LTE 69>
			 		<cfset QuerySetCell(tmpOptions, "range7", #aCheck.answertext#)>
				<cfelseif aCheck.answertext GTE 70 AND aCheck.answertext LTE 79>
			 		<cfset QuerySetCell(tmpOptions, "range8", #aCheck.answertext#)>
				<cfelseif aCheck.answertext GTE 80 AND aCheck.answertext LTE 89>
			 		<cfset QuerySetCell(tmpOptions, "range9", #aCheck.answertext#)>
				<cfelseif aCheck.answertext GTE 90 AND aCheck.answertext LTE 99>
			 		<cfset QuerySetCell(tmpOptions, "range10", #aCheck.answertext#)>
				<cfelse>
			 		<cfset QuerySetCell(tmpOptions, "range11", #aCheck.answertext#)>
				</cfif>

			</cfloop>
			
			<cfset tmprange1 = ListToArray(ValueList(tmpOptions.range1))>
			<cfset tmprange2 = ListToArray(ValueList(tmpOptions.range2))>
			<cfset tmprange3 = ListToArray(ValueList(tmpOptions.range3))>
			<cfset tmprange4 = ListToArray(ValueList(tmpOptions.range4))>
			<cfset tmprange5 = ListToArray(ValueList(tmpOptions.range5))>
			<cfset tmprange6 = ListToArray(ValueList(tmpOptions.range6))>
			<cfset tmprange7 = ListToArray(ValueList(tmpOptions.range7))>
			<cfset tmprange8 = ListToArray(ValueList(tmpOptions.range8))>
			<cfset tmprange9 = ListToArray(ValueList(tmpOptions.range9))>
			<cfset tmprange10 = ListToArray(ValueList(tmpOptions.range10))>
			<cfset tmprange11 = ListToArray(ValueList(tmpOptions.range11))>

			<cfset labelList="0～9,10～19,20～29,30～39,40～49,50～59,60～69,70～79,80～89,90～99,100">
			<cfset cnt=1>
			<cfset allsum = 0>
			<cfset allcnt = 0>
			<cfloop list="#labelList#" index="idx">
				<cfset QueryAddrow(retQuery,1)>
				<cfset tmp_cnt= ArrayLen(#Evaluate("tmprange"&cnt)#)>
				<cfset tmp_sum= ArraySum(#Evaluate("tmprange"&cnt)#)>
				<cfset tmp_answer = #IIf(tmp_cnt EQ 0, DE("0"), DE("#tmp_cnt#"))#>
				<cfset tmp_rate = IIF(samples EQ 0, DE(0), DE((#tmp_answer#/#samples#)*100))>
				<cfset QuerySetCell(retQuery, "samples", #tmp_cnt#)>
				<cfset QuerySetCell(retQuery, "rate", #NumberFormat(tmp_rate,'._')#)>
				<cfset QuerySetCell(retQuery, "label", "#idx#")>
				<cfset QuerySetCell(retQuery, "options_no", cnt)>
				<cfset cnt=cnt+1>
				<cfset allsum = allsum+tmp_sum>
				<cfset allcnt = allcnt+tmp_cnt>
			</cfloop>

			<cfset QueryAddRow(retQuery, 1)>
			<cfset tmp_rate = IIF(samples EQ 0, DE(0), DE((#temp_0res#/#samples#)*100))>
			<cfset QuerySetCell(retQuery, "samples", #temp_0res#)>
			<cfset QuerySetCell(retQuery, "rate", #NumberFormat(tmp_rate,'._')#)>
			<cfset QuerySetCell(retQuery, "label", '不明・未回答')>
			<cfset QuerySetCell(retQuery, "options_no", 0)>

			<cfset QueryAddrow(retQuery,1)>
			<cfif allcnt EQ 0>
				<cfset tmp_rate = 0>
			<cfelse>
				<cfset tmp_rate = #allsum#/#allcnt#>
			</cfif>
			<cfset QuerySetCell(retQuery, "samples", #allcnt#)>
			<cfset QuerySetCell(retQuery, "rate", #NumberFormat(tmp_rate,'.__')#)>
			<cfset QuerySetCell(retQuery, "label", '平均値')>
			<cfset QuerySetCell(retQuery, "options_no", '')>

			<cfset QueryAddrow(retQuery,1)>
			<cfset QuerySetCell(retQuery, "samples", #allsum#)>
			<cfset QuerySetCell(retQuery, "rate", '-')>
			<cfset QuerySetCell(retQuery, "label", '累計値')>
			<cfset QuerySetCell(retQuery, "options_no", '')>

			<cfdump var=#retQuery#>
			<cfreturn retQuery>
		<!--- 2005/10/11 h.miyahara(NetFusion) MOD 100点満点レンジ機能追加 E --->

		<!--- 各回答数取得 --->
		<cfloop query="GetAnsweroptions">
	
			<cfset QueryAddRow(retQuery, 1)>
			<cfquery dbtype="query" name="answerCheck">
				select
					count(answer) as answer
				from
					getAnswers
				where
					questionnaire_id = #arguments.questionnaire_id#
				and
					answer = '#options_no#'
			</cfquery>
			<cfset tmp_answer = #IIf(answerCheck.recordCount EQ 0, DE("0"), DE("#answerCheck.answer#"))#>
			<cfset tmp_rate = (#tmp_answer#/#samples#)*100>
			<cfset QuerySetCell(retQuery, "samples", #tmp_answer#)>
			<cfset QuerySetCell(retQuery, "rate", #NumberFormat(tmp_rate,'._')#)>
			<cfset QuerySetCell(retQuery, "label", #options_label#)>
			<cfset QuerySetCell(retQuery, "options_no", #options_no#)>
		</cfloop>
		<!--- 未回答・不明 --->
		<cfset QueryAddRow(retQuery, 1)>
		<cfset tmp_rate = (#temp_0res#/#samples#)*100>
		<cfset QuerySetCell(retQuery, "samples", #temp_0res#)>
		<cfset QuerySetCell(retQuery, "rate", #NumberFormat(tmp_rate,'._')#)>
		<cfset QuerySetCell(retQuery, "label", '不明・未回答')>
		<cfset QuerySetCell(retQuery, "options_no", 0)>

		<cfreturn retQuery>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
	
	</cffunction>

	<!---
	*************************************************************************************************
	* Class				:oasearch
	* Method			:getOAAnswers
	* Description		:回答値取得
	* Custom Attributes	:questionnaire_id	設問ID
	* Return Paramters	:
	* History			:1) Coded by	2004/10/13 h.miyahara(NetFusion)
	*					:2) Updated by	2005/10/11  h.miyahara(NetFusion) デフォルトソート順変更
	*					:3) Updated by	2005/10/11  h.miyahara(NetFusion) 満足度評価設問追加
	*					:4) Updated by	2005/10/13  h.miyahara(NetFusion) 対象カテゴリ設問追加
	*************************************************************************************************
	--->
	<cffunction name="getOAAnswers" access="remote" returntype="any">
		<cfargument name="sStartDate" type="numeric" default="">
		<cfargument name="sEndDate" type="numeric" default="">
		<cfargument name="area_code" type="String" default="">
		<cfargument name="together_code" type="String" default="">
		<cfargument name="roomtype" type="String" default="">
		<cfargument name="nights" type="numeric" default="">
		<cfargument name="purpose_code" type="String" default="">
		<cfargument name="cognition_code" type="String" default="">
		<cfargument name="reservation_code" type="String" default="">
		<cfargument name="room_code" type="numeric" default="">
		<cfargument name="roomcontrol_code" type="String" default="">
		<cfargument name="aging_code" type="String" default="">
				
		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<!--- 戻値クエリ作成 --->
			<cfset retQuery = QueryNew("customernumber, depart_date, gender, age, married, job, history, evalpoint, oa1, oa2, retFlg")>
			
			<!--- 抽出対象期間より期間内の回答者予約番号を抽出 --->
			<cfquery name="tmp_getAnswers" datasource = "#Application.datasource#">
				select
					A.referencenumber,
					A.depart_date,
					A.area_code,
					AD.answer,
					AD.answertext,
					AD.questionnaire_id,
					C.customernumber,
					R.reserve_code,
					R.reservationroute,
					R.room_code,
					R.roomtype,
					R.nights
				from
					#Application.app00.dbu#.hst_answers as A,
					#Application.app00.dbu#.hst_answersdetails as AD,
					#Application.app00.dbu#.rtt_reserve as R,
					#Application.app00.dbu#.rtm_customer as C
				where
					A.depart_date between '#arguments.sStartDate#' and '#arguments.sEndDate#'
				and
					A.referencenumber = AD.referencenumber
				and
					A.referencenumber = R.referencenumber
				and
					R.customernumber = C.customernumber
				and
					A.questionnairesheet_id = AD.questionnairesheet_id
				and
					AD.questionnairesheet_id = 0
				and
					AD.questionnaire_id IN(#Application.repeatcs_q1#,#Application.repeatcs_q2#,#Application.repeatcs_q3#)
				order by
					A.depart_date desc
			</cfquery>

			<!--- 共通集計条件絞込み処理 --->
			<cfinvoke
				component		= "conditionsetting"
				method			= "moresearch"
				temp_data = #tmp_getAnswers#
				temp_cond = #arguments#
				returnVariable	="getReservationList">

			<!--- <cfdump var=#getReservationList#><cfabort> --->
			<!--- 2005/06/27 h.miyahara(NetFusion) ADD オプション条件 end --->
			<cfset temp_code = "">
			<cfset temp_idlist = "#Application.repeatcs_q2#,#Application.repeatcs_q3#">
			<cfset temp_nmlist = "OA1,OA2">
			<cfloop query="getReservationList">
				<cfif getReservationList.referencenumber NEQ temp_code AND Len(getReservationList.answertext) NEQ 0>
					<cfset QueryAddRow(retQuery, 1)>
					<cfset temp_code = getReservationList.referencenumber>
				</cfif>
				<!--- 顧客コード --->
	 			<cfset QuerySetCell(retQuery, "customernumber", #getReservationList.customernumber#&#chr(9)#)>
				
				<!--- 滞在日 --->
	 			<cfset QuerySetCell(retQuery, "depart_date", #getReservationList.depart_date#&#chr(9)#)>
				
				<!--- 満足度評価 --->
				<cfif getReservationList.questionnaire_id EQ #Application.repeatcs_q1#>
		 			<cfset QuerySetCell(retQuery, "evalpoint", #getReservationList.answertext#&#chr(9)#)>
				</cfif>

				<!--- OA --->
				<cfset temp_check = "">
				<cfloop from="1" to="#ListLen(temp_idlist)#" index="i">
					<cfif getReservationList.questionnaire_id EQ ListGetAt(temp_idlist,i)>
			 			<cfset QuerySetCell(retQuery, #ListGetAt(temp_nmlist,i)#, #getReservationList.answertext#&#chr(9)#)>
			 			<cfset temp_check = temp_check & #getReservationList.answertext#>
					</cfif>
				</cfloop>
				<cfif Len(temp_check) NEQ 0>
		 			<cfset QuerySetCell(retQuery, "retFlg", 1)>
				</cfif>

			</cfloop>
			
			<!--- 全未回答除去 --->
			<cfquery name="retQuery" dbtype="query">
				select 
					customernumber,depart_date,evalpoint,#temp_nmlist#,retFlg
				from
					retQuery
				where
					retFlg is not null
			</cfquery>

			<cfreturn retQuery>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>

	</cffunction>

	<!---
	*************************************************************************************************
	* Class				:csmulti
	* Method			:OutputCSV
	* Description		:OA検索結果一覧ＣＳＶ出力
	* Custom Attributes	:resultObj	出力リストのデータプロバイダ
	* 					:fieldnames	列名
	* 					:headerText	列のキャプション
	* Return Paramters	:
	* History			:1) Coded by	2004/10/05 h.miyahara(NetFusion)
	*					:2) Updated by	2005/10/11 h.miyahara(NetFusion) 集計条件追加
	*************************************************************************************************
	--->
	<cffunction name="OutputCSV" access="remote" returntype="any">
		<cfargument name="resultObj" type="Array" default="">
		<cfargument name="category" type="String" default="">
		<cfargument name="sStartDate" type="String" default="">
		<cfargument name="sEndDate" type="String" default="">
		<cfargument name="area" type="String" default="">
		<cfargument name="nights" type="String" default="">
		<cfargument name="together" type="String" default="">
		<cfargument name="roomtype" type="String" default="">
		<cfargument name="purpose" type="String" default="">
		<cfargument name="cognition" type="String" default="">
		<cfargument name="reservation" type="String" default="">
		<cfargument name="room_code" type="String" default="">
		<cfargument name="roomcontrol" type="String" default="">
		<cfargument name="aging" type="String" default="">		

		<cftry>
		
			<!--- 改行コード除去 --->
			<cfloop from="1" to="#ArrayLen(resultObj)#" index="i">
				<cfset resultObj[i].label = #replacelist(resultObj[i].label,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
				<cfset resultObj[i].samples = #replacelist(resultObj[i].samples,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
				<cfset resultObj[i].average = #replacelist(resultObj[i].average,"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#>
			</cfloop>

			<!--- ディレクトリ＆ファイル情報 --->
			<cfset filePath = ExpandPath(#application.outputDirectory#)>
			<cfset Dircheck = DirectoryExists( filePath & "\" & #Replace(cgi.remote_addr,".","","all")# )>
			
			<!--- ディレクトリ存在チェック --->
			<cfif Dircheck eq "no">
				<cfdirectory action = "create" directory = "#filePath#\#Replace(cgi.remote_addr,'.','','all')#">
			</cfif>
			
			<cfset UserDir = #Replace(cgi.remote_addr,".","","all")# & "\">
			<cfset datepart = #DateFormat(Now(),'YYMMDD')# & #TimeFormat(Now(),'HHMMSS')#>
			<cfset filename = filePath & UserDir & datepart & "CRMResultsList.csv">
			<cfset filecheck = FileExists( filename )>
			
			<!--- ディレクトリ内ファイル数チェック --->
			<cfdirectory 
				directory="#filePath#\#Replace(cgi.remote_addr,'.','','all')#" 
				name="myCSVDirectory"
				sort="DATELASTMODIFIED ASC">
				
			<!--- 指定ファイル数以上は作らない。古いものから削除 --->
			<cfset FileHistory = 10>
			<cfif myCSVDirectory.RecordCount GTE FileHistory>
				<cffile action = "delete" file = "#filePath##UserDir##myCSVDirectory.Name#">
			</cfif>	
		
			<!--- ファイル存在チェック --->
			<cfif filecheck eq "no">
				<cfset condData = "-集計条件-" & "," & "" & "【集計対象：#arguments.category#】【集計期間：#arguments.sStartDate#～#arguments.sEndDate#】【エリア：#arguments.area#】【ルームタイプ：#arguments.roomtype#】【同行者：#arguments.together#】【泊数：#arguments.nights#】【旅行目的：#arguments.purpose#】【認知経路：#arguments.cognition#】【予約経路：#arguments.reservation#】【部屋番号：#arguments.room_code#】【ブロック：#arguments.roomcontrol#】【見た目年齢：#arguments.aging#】" & "#chr(13)##chr(10)#" & "#chr(13)##chr(10)#">	
				<cfset captionData = condData & "回答データ" & "," & "実数" & "," & "％">
				<cffile action = "write" file=#filename# output=#captionData#>

				<!--- 出力内容整形 --->
				<cfloop from="1" to="#ArrayLen(resultObj)#" index="i">
					<cfset outputData = #resultObj[i].label# & ","  & #resultObj[i].samples# &  "," & #resultObj[i].average#>
					<cffile action = "append" file=#filename# output=#outputData#>
				</cfloop>
			
			</cfif>
			
			<!--- 戻値設定 --->
			<cfscript>
				executeinformation = StructNew();
				StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
				StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
				StructInsert(executeinformation,	"outputMsg",	"正常"						);
				StructInsert(executeinformation,	"outputPath",	#Application.outputDirectory# & #Replace(UserDir,"\","/","all")# & datepart & "CRMResultsList.csv" );
				StructInsert(executeinformation,	"filename",		"http://#cgi.server_name#/#Application.outputDirectory#" & #Replace(UserDir,"\","/","all")# & datepart & "CRMResultsList.csv" );
				StructInsert(executeinformation,	"newname",		"_#datepart#_resultList.csv" );
			</cfscript>
			<cfreturn executeinformation>
			
			<cfsetting enablecfoutputonly="No">
			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

	<!---
	*************************************************************************************************
	* Class				:oasearch
	* Method			:OAResultOutputCSV
	* Description		:OA検索結果一覧ＣＳＶ出力
	* Custom Attributes	:resultObj	出力リストのデータプロバイダ
	* 					:fieldnames	列名
	* 					:headerText	列のキャプション
	* Return Paramters	:
	* History			:1) Coded by	2004/10/05 h.miyahara(NetFusion)
	*					:2) Updated by	2005/10/11  h.miyahara(NetFusion) 集計条件追加
	*************************************************************************************************
	--->
	<cffunction name="OAResultOutputCSV" access="remote" returntype="any">
		<cfargument name="resultObj" type="Array" default="">
		<cfargument name="fieldnames" type="Array" default="">
		<cfargument name="headerText" type="Array" default="">
		<cfargument name="qtype" type="String" default="">
		<cfargument name="sStartDate" type="String" default="">
		<cfargument name="sEndDate" type="String" default="">
		<cfargument name="area" type="String" default="">
		<cfargument name="nights" type="String" default="">
		<cfargument name="together" type="String" default="">
		<cfargument name="roomtype" type="String" default="">
		<cfargument name="purpose" type="String" default="">
		<cfargument name="cognition" type="String" default="">
		<cfargument name="reservation" type="String" default="">
		<cfargument name="room_code" type="String" default="">
		<cfargument name="roomcontrol" type="String" default="">
		<cfargument name="aging" type="String" default="">		
				
		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<!--- ディレクトリ＆ファイル情報 --->
			<cfset filePath = ExpandPath(#application.outputDirectory#)>
			<cfset Dircheck = DirectoryExists( filePath & "\" & #Replace(cgi.remote_addr,".","","all")# )>
			
			<!--- ディレクトリ存在チェック --->
			<cfif Dircheck eq "no">
				<cfdirectory action = "create" directory = "#filePath#\#Replace(cgi.remote_addr,'.','','all')#">
			</cfif>
			
			<cfset UserDir = #Replace(cgi.remote_addr,".","","all")# & "\">
			<cfset datepart = #DateFormat(Now(),'YYMMDD')# & #TimeFormat(Now(),'HHMMSS')#>
			<cfset filename = filePath & UserDir & datepart & "CRMResultsList.csv">
			<cfset filecheck = FileExists( filename )>
			
			<!--- ディレクトリ内ファイル数チェック --->
			<cfdirectory 
				directory="#filePath#\#Replace(cgi.remote_addr,'.','','all')#" 
				name="myCSVDirectory"
				sort="DATELASTMODIFIED ASC">
				
			<!--- 指定ファイル数以上は作らない。古いものから削除 --->
			<cfset FileHistory = 10>
			<cfif myCSVDirectory.RecordCount GTE FileHistory>
				<cffile action = "delete" file = "#filePath##UserDir##myCSVDirectory.Name#">
			</cfif>	
		
			<!--- ファイル存在チェック --->
			<cfif filecheck eq "no">
				<cfset condData = "-集計条件-" & "," & "" & "【集計対象：#arguments.qtype#】【集計期間：#arguments.sStartDate#～#arguments.sEndDate#】【エリア：#arguments.area#】【ルームタイプ：#arguments.roomtype#】【同行者：#arguments.together#】【泊数：#arguments.nights#】【旅行目的：#arguments.purpose#】【認知経路：#arguments.cognition#】【予約経路：#arguments.reservation#】【部屋番号：#arguments.room_code#】【ブロック：#arguments.roomcontrol#】【見た目年齢：#arguments.aging#】" & "#chr(13)##chr(10)#" & "#chr(13)##chr(10)#">	

				<cfset captionData = condData &  ArrayToList(arguments.headerText)>

				<cffile action = "write" file=#filename# output=#captionData#>

				<!--- 出力内容整形 改行コード除去 --->
				<cfloop from="1" to="#ArrayLen(resultObj)#" index="i">
					<cfset outputData = "">
					<cfloop from="1" to="#ArrayLen(fieldnames)#" index="c">
						<cfset outputData = ListAppend( outputData, #replacelist(Evaluate("resultObj[i].#fieldnames[c]#"),"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#",",,")#)>
					</cfloop>
					<cffile action = "append" file=#filename# output=#outputData#>
				</cfloop>
			</cfif>

			<!--- 戻値設定 --->
			<cfscript>
				executeinformation = StructNew();
				StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
				StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
				StructInsert(executeinformation,	"outputMsg",	"正常"						);
				StructInsert(executeinformation,	"outputPath",	#Application.outputDirectory# & #Replace(UserDir,"\","/","all")# & datepart & "CRMResultsList.csv" );
				StructInsert(executeinformation,	"filename",		"http://#cgi.server_name#/#Application.outputDirectory#" & #Replace(UserDir,"\","/","all")# & datepart & "CRMResultsList.csv" );
				StructInsert(executeinformation,	"newname",		"_#datepart#_resultList.csv" );
			</cfscript>
			<cfreturn executeinformation>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>

	</cffunction>
	
	<!---
	*************************************************************************************************
	* Class				:csmulti
	* Method			:getSetting
	* Description		:OA検索結果一覧ＣＳＶ出力
	* Custom Attributes	:resultObj	出力リストのデータプロバイダ
	* 					:fieldnames	列名
	* 					:headerText	列のキャプション
	* Return Paramters	:
	* History			:1) Coded by	2004/10/05 h.miyahara(NetFusion)
	*					:2) Updated by	2005/10/11 h.miyahara(NetFusion) 集計条件追加
	*************************************************************************************************
	--->
	<cffunction name="getSetting" access="remote" returntype="any">
		<cftry>
			<!--- 戻値設定 --->
			<cfscript>
				executeinformation = StructNew();
				StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
				StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
				StructInsert(executeinformation,	"outputMsg",	"正常"						);
			</cfscript>
			<cfreturn executeinformation>
			<cfsetting enablecfoutputonly="No">
			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>
</cfcomponent>
