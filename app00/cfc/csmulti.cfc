﻿<cfcomponent hint="ＣＳ集計情報取得">
	<!---
	*************************************************************************************************
	* Class				:csmulti
	* Method			:getCategory
	* Description		:カテゴリリスト取得
	* Custom Attributes	:none
	* Return Paramters	:
	* History			:1) Coded by	2004/10/13 h.miyahara(NetFusion)
	*					:2) Updated by	2005/07/22 h.miyahara(NetFusion) 数値入力形式の設問除外
	*************************************************************************************************
	--->
	<cffunction name="getCategory" access="remote" returntype="query" hint="">
		<cfargument name="resort_code" type="String" default="0">
		<cfsetting enablecfoutputonly="yes">
		<cftry>
			<!--- 関連施設追加 --->
			<cfquery name="getAddResortcode" datasource = "#Application.datasource#">
				select	parent_code as resort_code
				from	mst_resort
				where	resort_code in (#arguments.resort_code#)
				union
				select	resort_code as resort_code
				from	mst_resort
				where	resort_code in (#arguments.resort_code#)
			</cfquery>
			<cfif getAddResortcode.recordcount neq 0>
				<cfset arguments.resort_code = ValueList(getAddResortcode.resort_code)>
			</cfif>


			<cfif arguments.resort_code eq 0>
				<cfset sqlstr1 = "">
				<cfset sqlstr2 = "">
			<cfelse>
				<cfset sqlstr1 = "and a.resort_code in ( #arguments.resort_code# )">
				<cfset sqlstr2 = "and b.resort_code in ( #arguments.resort_code# )">
			</cfif>


			<!--- シートID取得 --->
			<cfquery name="getSheet_id" datasource = "#Application.datasource#">

				select	 a.questionnairesheet_id as sheet_id
						,a.resort_code
						,a.questionnairesheet_nm
						,a.start_date
				from #Application.app00.dbu#.mst_questionnairesheet as a,
				(
					select	 distinct
							 a.resort_code
							,max(a.start_date) as start_max
					from	 #Application.app00.dbu#.mst_questionnairesheet as a
					where	 active_flg = 1 
					group by a.resort_code
					having	 max(a.start_date) <= getdate()
				)wktbl
				where a.resort_code =  wktbl.resort_code
				and	a.start_date =  wktbl.start_max
				#sqlstr1#
			</cfquery>

			<cfset strSQL= "">
			<cfset strCond = "">

			<cfset strSQL= strSQL & "
				select	 distinct
						 a.questionnairecategory_id
						,b.seq as viewseq
						,b.questionnaire_id as questionnaire_id
						,b.questionnaire_nm
						,c.cs_qlabel
						,convert(int,substring(c.cs_qlabel,2,len(c.cs_qlabel))) as qlabel_code1
						,substring(c.cs_qlabel,1,1) as qlabel_code2
				from	 #Application.app00.dbu#.mst_questionnairecategory  as a
						,#Application.app00.dbu#.mst_questionnaire as b
						,#Application.app00.dbu#.oth_sheetconfiguration as c
				where	 a.active_flg = 1
				and		 b.questionnaire_type = 0
				and		 a.questionnairecategory_id = b.questionnairecategory_id
				and		 b.questionnaire_id = c.questionnaire_id
				and		 c.questionnairesheet_id != 0
				and		(
			">

			<cfset strCond = "">
			<cfloop query="getSheet_id">
				<cfset strCond = strCond & " (c.resort_code = #getSheet_id.resort_code[currentrow]# and c.questionnairesheet_id = #getSheet_id.sheet_id[currentrow]#) or">
			</cfloop>
			<cfset strCond = strCond & " 0<>0 )">
			<cfset strSQL= strSQL & strCond>

			<cfquery name="getNewestCategoryList" datasource = "#Application.datasource#">
				#ReplaceList(strSQL,"''","'")#
			</cfquery>
			<cfset newestquestionnaire_idlist = valuelist(getNewestCategoryList.questionnaire_id)>

			<cfset strSQL= strSQL & "
				union 
				select	 distinct
						 a.questionnairecategory_id as questionnairecategory_id
						,(9000000 + b.seq) as viewseq
						,b.questionnaire_id as questionnaire_id
						,'(旧) ' + b.questionnaire_nm as questionnaire_nm
						,c.cs_qlabel
						,convert(int,substring(c.cs_qlabel,2,len(c.cs_qlabel))) as qlabel_code1
						,substring(c.cs_qlabel,1,1) as qlabel_code2
				from	 #Application.app00.dbu#.mst_questionnairecategory  as a
						,#Application.app00.dbu#.mst_questionnaire as b
						,#Application.app00.dbu#.oth_sheetconfiguration as c
				where	 a.active_flg = 1
				and		 b.questionnaire_type = 0
				and		 a.questionnairecategory_id = b.questionnairecategory_id
				and		 b.questionnaire_id = c.questionnaire_id
				and		 c.questionnairesheet_id != 0
				and		 b.questionnaire_id not in (
							#newestquestionnaire_idlist#
			)">

			<cfset strSQL= strSQL & " order by viewseq,questionnaire_id,qlabel_code1 ">

			<cfquery name="getCategoryList" datasource = "#Application.datasource#">
				#ReplaceList(strSQL,"''","'")#
			</cfquery>

			<cfset retq = querynew("#lcase(getCategoryList.columnlist)#")>
			<cfset wk_label = "">
			<cfloop query="getCategoryList">
				<!--- 同一設問ID・別番号の対応 --->
				<cfset wk_c1 = getCategoryList.questionnaire_id[currentrow-1]>
				<cfset wk_c2 = getCategoryList.questionnaire_id[currentrow]>

				<cfif wk_c1 neq wk_c2>
					<cfset wk_label = "">
					<cfset queryaddrow(retq,1)>
					<cfset querysetcell(retq,"questionnairecategory_id",questionnairecategory_id)>
					<cfset querysetcell(retq,"questionnaire_id",questionnaire_id)>
					<cfset querysetcell(retq,"questionnaire_nm",questionnaire_nm)>
				<cfelse>
					<cfif ListFind(wk_label,getCategoryList.cs_qlabel[currentrow-1],"・") is 0>
						<cfset wk_label = listappend( wk_label , getCategoryList.cs_qlabel[currentrow-1],"・")>
					</cfif>
				</cfif>
				<cfif ListFind(wk_label,getCategoryList.cs_qlabel[currentrow],"・") is 0>
					<cfset wk_label = listappend( wk_label , getCategoryList.cs_qlabel[currentrow],"・")>
				</cfif>
				<cfset querysetcell(retq,"cs_qlabel",wk_label)>

			</cfloop>
			<cfreturn retq>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>

	</cffunction>	

	<!---
	*************************************************************************************************
	* Class				:csmulti
	* Method			:getContents
	* Description		:サマリ削除（論理削除） 
	* Custom Attributes	:questionnairecategory_id	カテゴリID
	* Return Paramters	:
	* History			:1) Coded by	2004/10/13 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="getContents" access="remote" returntype="any">
		<cfargument name="questionnairecategory_id" type="numeric" default="1">
		<cfsetting enablecfoutputonly="yes">
		<cftry>
		
			<cfquery name="getContentsList" datasource = "#Application.datasource#">
				select	 questionnaire_id
						,questionnaire_nm
				from	 #Application.app00.dbu#.mst_questionnaire
				where	 questionnairecategory_id = #arguments.questionnairecategory_id#
				and		 questionnaire_type = 0
				and		 active_flg = 1
			</cfquery>
			
			<cfreturn getContentsList>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
	
	</cffunction>	

	<!---
	*************************************************************************************************
	* Class				:csmulti
	* Method			:getOAAnswers
	* Description		:回答値取得
	* Custom Attributes	:questionnaire_id	設問ID
	* Return Paramters	:
	* History			:1) Coded by	2004/10/13 h.miyahara(NetFusion)
	*					:2) Updated by	2005/10/26 h.miyahara(NetFusion) 
	*************************************************************************************************
	--->
	<cffunction name="getAnswers" access="remote" returntype="any">
		<cfargument name="dcondtion" type="Boolean" default="false">
		<cfargument name="selecteddays" type="Array" default="">
		<cfargument name="questionnaire_id" type="Any" default="">

		<cfargument name="arg_resort_code" type="Any" default="">
		<cfargument name="arg_cross_id" type="Numeric" default="0">

		<cfargument name="arg_group_id" type="Numeric" default="0">	<!--- 団体（0=全部、1=個人、2=団体 'zzz*****'）--->
		<cfargument name="arg_answermethod_id" type="Numeric" default="0">	<!--- 団体（0=指定なし、1=手渡し、2=Web）--->
		
		<cfargument name="arg_generation" type="Any" default="">	<!--- 年代 --->
		<cfargument name="arg_gender" type="Any" default="">		<!--- 性別 --->
		<cfargument name="arg_job" type="Any" default="">			<!--- 職業 --->
		<cfargument name="arg_times" type="Any" default="">			<!--- 旅行回数 --->

		<cfargument name="arg_nights" type="Any" default="">		<!--- 宿泊数 --->		
		<cfargument name="arg_together" type="Any" default="">		<!--- 同行者 --->
		<cfargument name="arg_area" type="Any" default="">			<!--- 居住県 --->
		<cfargument name="arg_roomtype" type="Any" default="">		<!--- ルームタイプ --->
		<cfargument name="arg_purpose" type="Any" default="">		<!--- 旅行目的 --->
		<cfargument name="arg_reservation" type="Any" default="">	<!--- 予約経路 --->
		<cfargument name="arg_cognition" type="Any" default="">		<!--- 認知経路 --->
		<cfargument name="arg_roomnumber" type="Any" default="">	<!--- ルーム№ --->
		<cfargument name="arg_rcs" type="boolean" default="false">	<!--- リピーターCS経由 --->
		<cfargument name="arg_fieldselect" type="Numeric" default="0">	<!--- 基準日 --->
		<cfargument name="arg_residence" type="Any" default="">		<!--- INB用居住地 --->

		<cfsetting enablecfoutputonly="yes">
		<cftry>

			<cfinvoke component = "csmulti" method = "getAnswers_sub" returnVariable ="retVal">
				<cfinvokeargument name="dcondtion" value = #arguments.dcondtion#>
				<cfinvokeargument name="selecteddays" value = #arguments.selecteddays#>
				<cfinvokeargument name="questionnaire_id" value = #arguments.questionnaire_id#>

				<cfinvokeargument name="arg_resort_code" value = #arguments.arg_resort_code#>
				<cfinvokeargument name="arg_cross_id" value = #arguments.arg_cross_id#>

				<cfinvokeargument name="arg_group_id" value = #arguments.arg_group_id#>
				<cfinvokeargument name="arg_answermethod_id" value = #arguments.arg_answermethod_id#>

				<cfinvokeargument name="arg_generation" value = #arguments.arg_generation#>
				<cfinvokeargument name="arg_gender" value = #arguments.arg_gender#>
				<cfinvokeargument name="arg_job" value = #arguments.arg_job#>
				<cfinvokeargument name="arg_times" value = #arguments.arg_times#>

				<cfinvokeargument name="arg_nights" value = #arguments.arg_nights#>
				<cfinvokeargument name="arg_together" value = #arguments.arg_together#>
				<cfinvokeargument name="arg_area" value = #arguments.arg_area#>
				<cfinvokeargument name="arg_roomtype" value = #arguments.arg_roomtype#>
				<cfinvokeargument name="arg_purpose" value = #arguments.arg_purpose#>
				<cfinvokeargument name="arg_reservation" value = #arguments.arg_reservation#>
				<cfinvokeargument name="arg_cognition" value = #arguments.arg_cognition#>
				<cfinvokeargument name="arg_roomnumber" value = #arguments.arg_roomnumber#>
				<cfinvokeargument name="arg_rcs" value = #arguments.arg_rcs#>
				<cfinvokeargument name="arg_fieldselect" value = #arguments.arg_fieldselect#>
				<cfinvokeargument name="arg_residence" value = #arguments.arg_residence#>
			</cfinvoke>

			<cfset getcnt=#retVal#>
<!---
			<cfif #arguments.questionnaire_id# IS 1200000001  OR  #arguments.questionnaire_id# IS 1400000001>
 --->
			<cfif #arguments.questionnaire_id# IS 1200000001>
				<cfset options_qid = #arguments.questionnaire_id# + 100000>

				<cfinvoke component = "csmulti" method = "getAnswers_sub" returnVariable ="retVal">
					<cfinvokeargument name="dcondtion" value = #arguments.dcondtion#>
					<cfinvokeargument name="selecteddays" value = #arguments.selecteddays#>
					<cfinvokeargument name="questionnaire_id" value = #options_qid#>
	
					<cfinvokeargument name="arg_resort_code" value = #arguments.arg_resort_code#>
					<cfinvokeargument name="arg_cross_id" value = #arguments.arg_cross_id#>
	
					<cfinvokeargument name="arg_group_id" value = #arguments.arg_group_id#>
					<cfinvokeargument name="arg_answermethod_id" value = #arguments.arg_answermethod_id#>
	
					<cfinvokeargument name="arg_generation" value = #arguments.arg_generation#>
					<cfinvokeargument name="arg_gender" value = #arguments.arg_gender#>
					<cfinvokeargument name="arg_job" value = #arguments.arg_job#>
					<cfinvokeargument name="arg_times" value = #arguments.arg_times#>
	
					<cfinvokeargument name="arg_nights" value = #arguments.arg_nights#>
					<cfinvokeargument name="arg_together" value = #arguments.arg_together#>
					<cfinvokeargument name="arg_area" value = #arguments.arg_area#>
					<cfinvokeargument name="arg_roomtype" value = #arguments.arg_roomtype#>
					<cfinvokeargument name="arg_purpose" value = #arguments.arg_purpose#>
					<cfinvokeargument name="arg_reservation" value = #arguments.arg_reservation#>
					<cfinvokeargument name="arg_cognition" value = #arguments.arg_cognition#>
					<cfinvokeargument name="arg_roomnumber" value = #arguments.arg_roomnumber#>
					<cfinvokeargument name="arg_rcs" value = #arguments.arg_rcs#>
					<cfinvokeargument name="arg_fieldselect" value = #arguments.arg_fieldselect#>
					<cfinvokeargument name="arg_residence" value = #arguments.arg_residence#>
				</cfinvoke>

				<cfquery name="getcnt" dbtype = "query">
					select * from getcnt
					<cfif retVal.recordcount neq 0>
						union all
						select * from retVal
						where label_1 not in ('TOTAL')
					</cfif>
				</cfquery>

			</cfif>

			<cfset retQuery=#getcnt#>

			<cfreturn retQuery>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>

		</cftry>
	
	</cffunction>
	<!---
	*************************************************************************************************
	* Class				:csmulti
	* Method			:getOAAnswers
	* Description		:回答値取得
	* Custom Attributes	:questionnaire_id	設問ID
	* Return Paramters	:
	* History			:1) Coded by	2004/10/13 h.miyahara(NetFusion)
	*					:2) Updated by	2005/10/26 h.miyahara(NetFusion) 
	*************************************************************************************************
	--->
	<cffunction name="getAnswers_sub" access="remote" returntype="any">
		<cfargument name="dcondtion" type="Boolean" default="false">
		<cfargument name="selecteddays" type="Array" default="">
		<cfargument name="questionnaire_id" type="Any" default="">

		<cfargument name="arg_resort_code" type="Any" default="">
		<cfargument name="arg_cross_id" type="Numeric" default="0">

		<cfargument name="arg_group_id" type="Numeric" default="0">	<!--- 団体（0=全部、1=個人、2=団体 'zzz*****'）--->
		<cfargument name="arg_answermethod_id" type="Numeric" default="0">	<!--- 団体（0=指定なし、1=手渡し、2=Web）--->

		<cfargument name="arg_generation" type="Any" default="">	<!--- 年代 --->
		<cfargument name="arg_gender" type="Any" default="">		<!--- 性別 --->
		<cfargument name="arg_job" type="Any" default="">			<!--- 職業 --->
		<cfargument name="arg_times" type="Any" default="">			<!--- 旅行回数 --->

		<cfargument name="arg_nights" type="Any" default="">		<!--- 宿泊数 --->		
		<cfargument name="arg_together" type="Any" default="">		<!--- 同行者 --->
		<cfargument name="arg_area" type="Any" default="">			<!--- 居住県 --->
		<cfargument name="arg_roomtype" type="Any" default="">		<!--- ルームタイプ --->
		<cfargument name="arg_purpose" type="Any" default="">		<!--- 旅行目的 --->
		<cfargument name="arg_reservation" type="Any" default="">	<!--- 予約経路 --->
		<cfargument name="arg_cognition" type="Any" default="">		<!--- 認知経路 --->
		<cfargument name="arg_roomnumber" type="Any" default="">	<!--- ルーム№ --->
		<cfargument name="arg_rcs" type="Boolean" default="false">	<!--- リピーターCS経由 --->
		<cfargument name="arg_fieldselect" type="Numeric" default="0">	<!--- 基準日 --->

		<cfsetting enablecfoutputonly="yes">
		<cftry>

		<!--- 塚原さんに仕様確認？？？？ --->
<!--- 
		<cfoutput>
			<cfset aaa=arguments.questionnaire_id \ 100000>
			<cfset bbb=arguments.questionnaire_id / 100000>
			#arguments.questionnaire_id# / 100000 = #aaa#(#Int(bbb)#)
		</cfoutput>
		<cfabort>
 --->		
		<cfif ((int(#arguments.questionnaire_id# / 100000)) mod 1000) IS 0>
			<cfset options_qid = #arguments.questionnaire_id#>
		<cfelse>
			<cfset options_qid = #arguments.questionnaire_id# - 100000>
		</cfif>
		<cfset tmp_cmd = 0>

		<!--- リゾートコード --->
		<cfif arraylen(arguments.arg_resort_code) neq 0>

			<cfset tmp_list = ArrayToList(arguments.arg_resort_code)>
			<cfset tmp_cmd = "">
			<cfloop from="1" to="#ListLen(tmp_list)#" index="i">
				<cfset tmp_cmd = ListAppend(tmp_cmd, arguments.arg_resort_code[i])>
			</cfloop>

			<cfset strSQL = " in (#tmp_cmd#) ">

		</cfif>

		<!--- 最新シートID取得 --->
		<cfquery name="getSheet_id" datasource = "#Application.datasource#">

			select	 a.questionnairesheet_id as sheet_id
					,a.resort_code
			from #Application.app00.dbu#.mst_questionnairesheet as a,
			(
				select	 distinct
						 a.resort_code
						,max(a.start_date) as start_max
				from	 #Application.app00.dbu#.mst_questionnairesheet as a
				where	 active_flg = 1 
				group by a.resort_code
				having	 max(a.start_date) <= getdate()
			)wktbl
			where a.resort_code =  wktbl.resort_code
			and	a.start_date =  wktbl.start_max
			<cfif tmp_cmd neq 0>
				and	a.resort_code #strSQL#
			</cfif>
		</cfquery>

		<!--- 対象設問の回答候補値を取得 最新のもののみ 2011/11/30 --->
		<cfquery name="GetAnsweroptions" datasource = "#Application.datasource#">
			select distinct
				  <!--- #Application.app00.dbu#.oth_answeroptions.questionnairesheet_id --->
				 	 #Application.app00.dbu#.oth_answeroptions.questionnaire_id
					,#Application.app00.dbu#.oth_answeroptions.seq
					,#Application.app00.dbu#.mst_answeroptions.options_no
					,#Application.app00.dbu#.oth_answeroptions.cs_alabel as options_label
			from	 #Application.app00.dbu#.oth_answeroptions
			inner join 
					 #Application.app00.dbu#.mst_answeroptions 
			on		 #Application.app00.dbu#.oth_answeroptions.options_id = #Application.app00.dbu#.mst_answeroptions.options_id
			where	 #Application.app00.dbu#.oth_answeroptions.questionnaire_id = #options_qid#
			and		 #Application.app00.dbu#.mst_answeroptions.active_flg = 1
			and 	 (
			<cfset strCond="">
			<cfloop query="getSheet_id">
				<cfset strCond = ListAppend( strCond ,"#Application.app00.dbu#.oth_answeroptions.resort_code = #getSheet_id.resort_code[currentrow]# and #Application.app00.dbu#.oth_answeroptions.questionnairesheet_id = #getSheet_id.sheet_id[currentrow]#","/")>
				<cfset strCond = ReplaceNoCase( strCond ,"/"," or ","all")>
			</cfloop>
			#strCond#
			) or 0 <> 0
			order by 
				#Application.app00.dbu#.oth_answeroptions.seq
		</cfquery>


		<!--- 共通日時抽出条件切替 --->
		<cfinvoke component = "conditionsetting" method = "datecondition" returnVariable ="dcondition">
			<cfinvokeargument name="dcondtion" value = #arguments.dcondtion#>
			<cfinvokeargument name="selecteddays" value = #arguments.selecteddays#>
		</cfinvoke>

		<!--- 基準日変更 --->
		<cfif arguments.arg_fieldselect EQ 0>
			<cfset selectedcondfield = "referencenumber,depart_date">
		<cfelseif arguments.arg_fieldselect EQ 1>
			<cfset dcondition = replace(dcondition,"depart_date","registrated","all")>
			<cfset selectedcondfield = "referencenumber,registrated">
		<cfelse>
			<cfset dcondition = replace(dcondition,"depart_date","arrive_date","all")>
			<cfset selectedcondfield = "referencenumber,arrive_date">
		</cfif>



		<cfset tmp_answers = "">
		<cfloop query="GetAnsweroptions">
			<cfset tmp_answers = ListAppend(tmp_answers,"A"&#options_no#)>
			<cfset tmp_answers = ListAppend(tmp_answers,"A"&#options_no#&"_average")>
		</cfloop>

		<!--- 戻値クエリ作成 --->
		<cfset itemName = "samples, rate, label, options_no">
		<cfif #arg_cross_id# GREATER THAN 0>

			<cfset cross_questionnaire_id = 9900000000 + #arg_cross_id#>

			<!--- クロス値の回答数取得 --->
			<cfquery name="getLoopCnt" datasource = "#Application.datasource#">
				select distinct
					oth_answeroptions.questionnairesheet_id, 
					oth_answeroptions.questionnaire_id, 
					oth_answeroptions.seq, 
					mst_answeroptions.options_no, 
					oth_answeroptions.cs_alabel as options_label, 
					mst_answeroptions.active_flg
				from oth_answeroptions INNER JOIN mst_answeroptions ON oth_answeroptions.options_id = mst_answeroptions.options_id
				where (
					((oth_answeroptions.questionnaire_id)=#cross_questionnaire_id#) and 
					((mst_answeroptions.active_flg)=1)
				)
				union 
					select	 0  as questionnairesheet_id, 
							'0' as questionnaire_id, 
							 0  as seq, 
							 0  as options_no, 
							'未回答' as options_label, 
							 1  as active_flg
					order by questionnaire_id desc, options_no
			</cfquery>

			<cfloop query="getLoopCnt">
				<cfset itemName = itemName & ", samples_" & #getLoopCnt.options_no[getLoopCnt.currentrow]#>
			</cfloop>

		</cfif>

		<cfset retQuery = QueryNew(#itemName#)>

		<!--- 共通集計条件絞込み処理 --->
		<cfinvoke 
			component = "conditionsetting" 
			method = "sqlMake1"
			temp_cond = #arguments#
			returnVariable = "strSQL1">
		
		<cfif (arraylen(arguments.arg_resort_code) IS 1) and (arguments.arg_resort_code[1] IS 0)>
			<cfset sqlStr = " ">
			<cfset wk_resortcode = "">
		<cfelse>
			<cfset tmp_list = ArrayToList(arguments.arg_resort_code)>
			<cfset tmp_cmd = "">
			<cfloop from="1" to="#ListLen(tmp_list)#" index="i">
				<cfset tmp_cmd = ListAppend(tmp_cmd, arguments.arg_resort_code[i])>
			</cfloop>
			<cfset wk_resortcode = tmp_cmd>

			<cfset sqlStr = " (resort_code in (#tmp_cmd#)) and ">
		</cfif>


		<!--- 共通集計条件絞込み処理 --->
		<cfinvoke 
			component = "conditionsetting" 
			method = "sqlMake2"
			temp_cond = #arguments#
			check_resort_code = #wk_resortcode#
			returnVariable = "strSQL2">

		<cfquery name="getcnt" datasource = "#Application.datasource#">
			select distinct referencenumber 
			from	 #Application.app00.dbu#.v_getAnswers
			where	 #PreserveSingleQuotes(dcondition)# and
					 questionnaire_id = #arguments.questionnaire_id#
					 #PreserveSingleQuotes(strSQL2)#
					<!--- 2008/07/16 リピーターCSは除外 --->
					<cfif arguments.arg_rcs eq false>
					and questionnairesheet_id  != 0
					<cfelse>
					and questionnairesheet_id   = 0
					</cfif>
		</cfquery>

		<cfif #getCnt.recordcount# eq 0>
			<cfset strSQL3 = " and referencenumber IN ('-1') ">
		<cfelse>
			<cfset w_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			<cfset num=evaluate('w_list')>
			<cfset strSQL3 = " and referencenumber IN (#PreserveSingleQuotes(w_list)#) ">
		</cfif>

			
		<!--- 抽出対象期間より期間内の回答者予約番号を抽出 --->
		<cfquery name="getAnswers" datasource = "#Application.datasource#">
			select * 
			from	 #Application.app00.dbu#.v_getAnswers
			where	 #PreserveSingleQuotes(dcondition)# and
					 questionnaire_id in (
						#arguments.questionnaire_id#,
						#Application.app00.q_value_id#,
						#Application.app00.q_gender#,
						#Application.app00.q_generation#,
						#Application.app00.q_spouse#,
						#Application.app00.q_job#,
						#Application.app00.q_trip#)

					 #PreserveSingleQuotes(strSQL1)#
					 #PreserveSingleQuotes(strSQL2)#
					 #PreserveSingleQuotes(strSQL3)#
					<!--- 2008/07/16 リピーターCSは除外 --->
					<cfif arguments.arg_rcs eq false>
					and questionnairesheet_id  != 0
					<cfelse>
					and questionnairesheet_id   = 0
					</cfif>
		</cfquery>

		<!--- 対象者無し --->
		<cfif getAnswers.recordcount EQ 0>	<cfreturn retQuery>	</cfif>	

		<!--- 基本クロス：性別 --->
		<cfif #arg_cross_id# IS 1>

			<!--- 男性 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000001) and (answer=1))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross1_1_list = "'-1'">
			<cfelse>
				<cfset cross1_1_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>
	
			<!--- 女性 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000001) and (answer=2))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross1_2_list = "'-1'">
			<cfelse>
				<cfset cross1_2_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 未回答 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000001) and (answer=0))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross1_0_list = "'-1'">
			<cfelse>
				<cfset cross1_0_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

		</cfif>

		<!--- 基本クロス：未既婚 --->
		<cfif #arg_cross_id# IS 2>

			<!--- 有り --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000002) and (answer=1))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross2_1_list = "'-1'">
			<cfelse>
				<cfset cross2_1_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>
	
			<!--- なし --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000002) and (answer=2))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross2_2_list = "'-1'">
			<cfelse>
				<cfset cross2_2_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>
	
			<!--- その他 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000002) and (answer=99))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross2_99_list = "'-1'">
			<cfelse>
				<cfset cross2_99_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 未回答 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000002) and (answer=0))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross2_0_list = "'-1'">
			<cfelse>
				<cfset cross2_0_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

		</cfif>

		<!--- 基本クロス：年代 --->
		<cfif #arg_cross_id# IS 3>

			<!--- 10代 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000003) and (answer=1))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross3_1_list = "'-1'">
			<cfelse>
				<cfset cross3_1_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 20代 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000003) and (answer=2))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross3_2_list = "'-1'">
			<cfelse>
				<cfset cross3_2_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 30代 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000003) and (answer=3))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross3_3_list = "'-1'">
			<cfelse>
				<cfset cross3_3_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 40代 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000003) and (answer=4))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross3_4_list = "'-1'">
			<cfelse>
				<cfset cross3_4_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 50代 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000003) and (answer=5))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross3_5_list = "'-1'">
			<cfelse>
				<cfset cross3_5_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 60代 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000003) and (answer=6))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross3_6_list = "'-1'">
			<cfelse>
				<cfset cross3_6_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 70代以上 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000003) and (answer=7))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross3_7_list = "'-1'">
			<cfelse>
				<cfset cross3_7_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 未回答 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000003) and (answer=0))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross3_0_list = "'-1'">
			<cfelse>
				<cfset cross3_0_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

		</cfif>

		<!--- 基本クロス：職業 --->
		<cfif #arg_cross_id# IS 5>

			<!--- 会社員 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000005) and (answer=1))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross5_1_list = "'-1'">
			<cfelse>
				<cfset cross5_1_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 会社役員 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000005) and (answer=2))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross5_2_list = "'-1'">
			<cfelse>
				<cfset cross5_2_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 自営/自由業 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000005) and (answer=3))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross5_3_list = "'-1'">
			<cfelse>
				<cfset cross5_3_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 専門職（医師/弁護士など） --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000005) and (answer=4))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross5_4_list = "'-1'">
			<cfelse>
				<cfset cross5_4_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 公務員 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000005) and (answer=5))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross5_5_list = "'-1'">
			<cfelse>
				<cfset cross5_5_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- パートアルバイト --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000005) and (answer=6))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross5_6_list = "'-1'">
			<cfelse>
				<cfset cross5_6_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 学生 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000005) and (answer=7))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross5_7_list = "'-1'">
			<cfelse>
				<cfset cross5_7_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 専業主婦 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000005) and (answer=8))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross5_8_list = "'-1'">
			<cfelse>
				<cfset cross5_8_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 働いていない --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000005) and (answer=9))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross5_9_list = "'-1'">
			<cfelse>
				<cfset cross5_9_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- その他 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000005) and (answer=99))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross5_99_list = "'-1'">
			<cfelse>
				<cfset cross5_99_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 未回答 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000005) and (answer=0))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross5_0_list = "'-1'">
			<cfelse>
				<cfset cross5_0_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

		</cfif>

		<!--- 基本クロス：旅行回数 --->
		<cfif #arg_cross_id# IS 6>

			<!--- 0回 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000006) and (answer=1))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross6_1_list = "'-1'">
			<cfelse>
				<cfset cross6_1_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 1回 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000006) and (answer=2))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross6_2_list = "'-1'">
			<cfelse>
				<cfset cross6_2_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 2～3回 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000006) and (answer=3))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross6_3_list = "'-1'">
			<cfelse>
				<cfset cross6_3_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 4～5回 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000006) and (answer=4))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross6_4_list = "'-1'">
			<cfelse>
				<cfset cross6_4_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 6回以上 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000006) and (answer=5))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross6_5_list = "'-1'">
			<cfelse>
				<cfset cross6_5_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 10回以上 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000006) and (answer=6))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross6_6_list = "'-1'">
			<cfelse>
				<cfset cross6_6_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 忘れた --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000006) and (answer=7))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross6_7_list = "'-1'">
			<cfelse>
				<cfset cross6_7_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

			<!--- 未回答 --->
			<cfquery name="getcnt" dbtype = "query">
				select distinct referencenumber from getAnswers where ((questionnaire_id = 9900000006) and (answer=0))
			</cfquery>
	
			<cfif #getCnt.recordcount# eq 0>
				<cfset cross6_0_list = "'-1'">
			<cfelse>
				<cfset cross6_0_list = ListQualify(ValueList(getCnt.referencenumber),"'")>
			</cfif>

		</cfif>

		<cfset QueryAddRow(retQuery, 1)>
		<!--- 2005/10/26 h.miyahara(NetFusion) MOD 母数算出 B --->
		<cfquery name="denominator" dbtype = "query">
			select	 distinct referencenumber
			from	 getAnswers
		</cfquery>

		<cfset samples = denominator.recordcount>
		<cfif samples EQ 0>	<cfreturn retQuery>	</cfif>
		<cfset temp_0res = 0>
		<cfset temp_1res = 0>
		<cfset temp_2res = 0>
		<cfset temp_3res = 0>
		<cfset temp_4res = 0>
		<cfset temp_5res = 0>
		<cfset temp_6res = 0>
		<cfset temp_7res = 0>
		<cfset temp_8res = 0>
		<cfset temp_9res = 0>
		<cfset temp_99res = 0>
		<cfset temp_0res = 0>

		<cfloop query="denominator">

			<cfquery name="res0Check" dbtype = "query">
				select	 referencenumber
						,questionnaire_id
						,answer
				from	 getAnswers
				where	 questionnaire_id = #arguments.questionnaire_id#
				and		 referencenumber = '#referencenumber#'
			</cfquery>

			<cfif res0Check.recordcount EQ 0>
				<cfset temp_0res = temp_0res+1>
			</cfif>
		
			<cfif #arg_cross_id# GREATER THAN 0>

				<cfloop query="getLoopCnt">
	
					<cfset num=evaluate('cross' & #arg_cross_id# & '_' & #getLoopCnt.options_no[getLoopCnt.currentrow]# & '_list')>
					<cfset strWhere = " referencenumber IN (#PreserveSingleQuotes(num)#) ">
			
					<cfquery name="resCheck" dbtype = "query">
						select	 referencenumber
								,questionnaire_id
								,answer
						from	 getAnswers
						where	 referencenumber = '#denominator.referencenumber[denominator.currentrow]#'
						and		#PreserveSingleQuotes(strWhere)#
					</cfquery>

					<cfif #resCheck.recordCount# GREATER THAN 0>		
						<cfquery name="resCheck" dbtype = "query">
							select	 referencenumber
									,questionnaire_id
									,answer
							from	 getAnswers
							where	 questionnaire_id = #arguments.questionnaire_id#
							and		 referencenumber = '#denominator.referencenumber[denominator.currentrow]#'
							and		#PreserveSingleQuotes(strWhere)#
						</cfquery>
				
						<cfif resCheck.recordcount IS 0>
							<cfif 1 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
								<cfset temp_1res = temp_1res+1>
							<cfelseif 2 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
								<cfset temp_2res = temp_2res+1>
							<cfelseif 3 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
								<cfset temp_3res = temp_3res+1>
							<cfelseif 4 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
								<cfset temp_4res = temp_4res+1>
							<cfelseif 5 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
								<cfset temp_5res = temp_5res+1>
							<cfelseif 6 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
								<cfset temp_6res = temp_6res+1>
							<cfelseif 7 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
								<cfset temp_7res = temp_7res+1>
							<cfelseif 8 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
								<cfset temp_8res = temp_8res+1>
							<cfelseif 9 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
								<cfset temp_9res = temp_9res+1>
							<cfelseif 99 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
								<cfset temp_99res = temp_99res+1>
							<cfelseif 0 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
								<cfset temp_0res = temp_0res+1>
							</cfif>
						</cfif>
					</cfif>
		
				</cfloop>
	
			</cfif>

		</cfloop>

		<!--- 2005/10/26 h.miyahara(NetFusion) MOD 母数算出 E --->
		<cfif ListFind(Application.app00.q_tuning_id,arguments.questionnaire_id) NEQ 0>
			<cfset samples = samples-temp_0res>
			<cfset temp_0res = 0>
		</cfif>
		<cfset QuerySetCell(retQuery, "samples", #samples# & "/" & 100)>
		<cfset QuerySetCell(retQuery, "rate", 100)>
		<cfset QuerySetCell(retQuery, "label", "TOTAL")>
		<cfset QuerySetCell(retQuery, "options_no", "")>

		<cfif #arg_cross_id# GREATER THAN 0>

			<cfloop query="getLoopCnt">

				<cfset num=evaluate('cross' & #arg_cross_id# & '_' & #getLoopCnt.options_no[getLoopCnt.currentrow]# & '_list')>
				<cfset strWhere = " referencenumber IN (#PreserveSingleQuotes(num)#) ">

				<cfquery dbtype="query" name="answerCheck">
					select	count( distinct referencenumber) as answer
					from	getAnswers
					where	questionnaire_id = #arguments.questionnaire_id#
					and		#PreserveSingleQuotes(strWhere)#
				</cfquery>
		
				<cfif 1 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
					<cfif #answerCheck.recordCount# IS 0>
						<cfset samples_1 = 0>
					<cfelse>
						<cfset samples_1 = answerCheck.answer>
					</cfif>
				<cfelseif 2 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
					<cfif #answerCheck.recordCount# IS 0>
						<cfset samples_2 = 0>
					<cfelse>
						<cfset samples_2 = answerCheck.answer>
					</cfif>
				<cfelseif 3 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
					<cfif #answerCheck.recordCount# IS 0>
						<cfset samples_3 = 0>
					<cfelse>
						<cfset samples_3 = answerCheck.answer>
					</cfif>
				<cfelseif 4 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
					<cfif #answerCheck.recordCount# IS 0>
						<cfset samples_4 = 0>
					<cfelse>
						<cfset samples_4 = answerCheck.answer>
					</cfif>
				<cfelseif 5 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
					<cfif #answerCheck.recordCount# IS 0>
						<cfset samples_5 = 0>
					<cfelse>
						<cfset samples_5 = answerCheck.answer>
					</cfif>
				<cfelseif 6 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
					<cfif #answerCheck.recordCount# IS 0>
						<cfset samples_6 = 0>
					<cfelse>
						<cfset samples_6 = answerCheck.answer>
					</cfif>
				<cfelseif 7 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
					<cfif #answerCheck.recordCount# IS 0>
						<cfset samples_7 = 0>
					<cfelse>
						<cfset samples_7 = answerCheck.answer>
					</cfif>
				<cfelseif 8 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
					<cfif #answerCheck.recordCount# IS 0>
						<cfset samples_8 = 0>
					<cfelse>
						<cfset samples_8 = answerCheck.answer>
					</cfif>
				<cfelseif 9 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
					<cfif #answerCheck.recordCount# IS 0>
						<cfset samples_9 = 0>
					<cfelse>
						<cfset samples_9 = answerCheck.answer>
					</cfif>
				<cfelseif 99 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
					<cfif #answerCheck.recordCount# IS 0>
						<cfset samples_99 = 0>
					<cfelse>
						<cfset samples_99 = answerCheck.answer>
					</cfif>
				<cfelseif 0 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
					<cfif #answerCheck.recordCount# IS 0>
						<cfset samples_0 = 0>
					<cfelse>
						<cfset samples_0 = answerCheck.answer>
					</cfif>
				</cfif>
				
				<cfset num=evaluate('samples_' & #getLoopCnt.options_no[getLoopCnt.currentrow]#)>
				<cfset QuerySetCell(retQuery, "samples_" & #getLoopCnt.options_no[getLoopCnt.currentrow]#, #num# & "/" & 100)>
			</cfloop>

		</cfif>

		<!--- 2005/10/26 h.miyahara(NetFusion) MOD 100点満点レンジ機能追加 B --->
		<cfif arguments.questionnaire_id EQ Application.app00.q_value_id>
			<cfset tmpOptions = QueryNew("range1,range2,range3,range4,range5,range6,range7,range8,range9,range10,range11")>

			<cfif #arg_cross_id# GREATER THAN 0>

				<cfloop query="getLoopCnt">
					<cfif 1 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset tmpOptions_1 = QueryNew("range1,range2,range3,range4,range5,range6,range7,range8,range9,range10,range11")>
					<cfelseif 2 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset tmpOptions_2 = QueryNew("range1,range2,range3,range4,range5,range6,range7,range8,range9,range10,range11")>
					<cfelseif 3 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset tmpOptions_3 = QueryNew("range1,range2,range3,range4,range5,range6,range7,range8,range9,range10,range11")>
					<cfelseif 4 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset tmpOptions_4 = QueryNew("range1,range2,range3,range4,range5,range6,range7,range8,range9,range10,range11")>
					<cfelseif 5 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset tmpOptions_5 = QueryNew("range1,range2,range3,range4,range5,range6,range7,range8,range9,range10,range11")>
					<cfelseif 6 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset tmpOptions_6 = QueryNew("range1,range2,range3,range4,range5,range6,range7,range8,range9,range10,range11")>
					<cfelseif 7 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset tmpOptions_7 = QueryNew("range1,range2,range3,range4,range5,range6,range7,range8,range9,range10,range11")>
					<cfelseif 8 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset tmpOptions_8 = QueryNew("range1,range2,range3,range4,range5,range6,range7,range8,range9,range10,range11")>
					<cfelseif 9 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset tmpOptions_9 = QueryNew("range1,range2,range3,range4,range5,range6,range7,range8,range9,range10,range11")>
					<cfelseif 99 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset tmpOptions_99 = QueryNew("range1,range2,range3,range4,range5,range6,range7,range8,range9,range10,range11")>
					<cfelseif 0 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset tmpOptions_0 = QueryNew("range1,range2,range3,range4,range5,range6,range7,range8,range9,range10,range11")>
					</cfif>
				</cfloop>
	
			</cfif>
			<cfloop query="denominator">
				<cfquery name="aCheck" datasource = "#Application.datasource#">
					select
						AD.questionnaire_id,
						AD.answer,
						AD.answertext,
						A.referencenumber
					from
						#Application.app00.dbu#.hst_answers as A,
						#Application.app00.dbu#.hst_answersdetails as AD,
						#Application.app00.dbu#.mst_questionnaire as Q
					where
						A.referencenumber = AD.referencenumber
					and
						A.questionnairesheet_id = AD.questionnairesheet_id
					and
						Q.questionnaire_id = AD.questionnaire_id
					and	
						Q.questionnaire_id = #arguments.questionnaire_id#
					and
						A.referencenumber = '#referencenumber#'
				</cfquery>
				<cfset QueryAddrow(tmpOptions,1)>

				<!--- 点数整形 --->
				<cfset aCheck.answertext=ReplaceList(aCheck.answertext,"０,1,２,３,４,５,６,７,８,９","0,1,2,3,4,5,6,7,8,9")>
				<cfset aCheck.answertext=REReplace(aCheck.answertext,"[^0-9]","","ALL")>
				<cfset aCheck.answertext=left(aCheck.answertext,3)>
				
				<cfif aCheck.answertext GTE 0 AND aCheck.answertext LTE 9>
			 		<cfset QuerySetCell(tmpOptions, "range1", #aCheck.answertext#)>
				<cfelseif aCheck.answertext GTE 10 AND aCheck.answertext LTE 19>
			 		<cfset QuerySetCell(tmpOptions, "range2", #aCheck.answertext#)>
				<cfelseif aCheck.answertext GTE 20 AND aCheck.answertext LTE 29>
			 		<cfset QuerySetCell(tmpOptions, "range3", #aCheck.answertext#)>
				<cfelseif aCheck.answertext GTE 30 AND aCheck.answertext LTE 39>
			 		<cfset QuerySetCell(tmpOptions, "range4", #aCheck.answertext#)>
				<cfelseif aCheck.answertext GTE 40 AND aCheck.answertext LTE 49>
			 		<cfset QuerySetCell(tmpOptions, "range5", #aCheck.answertext#)>
				<cfelseif aCheck.answertext GTE 50 AND aCheck.answertext LTE 59>
			 		<cfset QuerySetCell(tmpOptions, "range6", #aCheck.answertext#)>
				<cfelseif aCheck.answertext GTE 60 AND aCheck.answertext LTE 69>
			 		<cfset QuerySetCell(tmpOptions, "range7", #aCheck.answertext#)>
				<cfelseif aCheck.answertext GTE 70 AND aCheck.answertext LTE 79>
			 		<cfset QuerySetCell(tmpOptions, "range8", #aCheck.answertext#)>
				<cfelseif aCheck.answertext GTE 80 AND aCheck.answertext LTE 89>
			 		<cfset QuerySetCell(tmpOptions, "range9", #aCheck.answertext#)>
				<cfelseif aCheck.answertext GTE 90 AND aCheck.answertext LTE 99>
			 		<cfset QuerySetCell(tmpOptions, "range10", #aCheck.answertext#)>
				<cfelse>
			 		<cfset QuerySetCell(tmpOptions, "range11", #aCheck.answertext#)>
				</cfif>

				<cfif #arg_cross_id# GREATER THAN 0>

					<cfloop query="getLoopCnt">

						<cfset num=evaluate('tmpOptions_' & #getLoopCnt.options_no[getLoopCnt.currentrow]#)>
						<cfset QueryAddrow(#num#,1)>
	
						<cfset num=evaluate('cross' & #arg_cross_id# & '_' & #getLoopCnt.options_no[getLoopCnt.currentrow]# & '_list')>
						<cfset strWhere = " and A.referencenumber IN (#PreserveSingleQuotes(num)#) ">
		
						<cfquery name="aCheck" datasource = "#Application.datasource#">
							select
								AD.questionnaire_id,
								AD.answer,
								AD.answertext,
								A.referencenumber
							from
								#Application.app00.dbu#.hst_answers as A,
								#Application.app00.dbu#.hst_answersdetails as AD,
								#Application.app00.dbu#.mst_questionnaire as Q
							where	A.referencenumber = AD.referencenumber
							and		A.questionnairesheet_id = AD.questionnairesheet_id
							and		Q.questionnaire_id = AD.questionnaire_id
							and		Q.questionnaire_id = #arguments.questionnaire_id#
							and		A.referencenumber = '#denominator.referencenumber[denominator.currentrow]#'
							#PreserveSingleQuotes(strWhere)#
						</cfquery>

						<cfset num=evaluate('tmpOptions_' & #getLoopCnt.options_no[getLoopCnt.currentrow]#)>

						<!--- 点数整形 --->
						<cfset aCheck.answertext=ReplaceList(aCheck.answertext,"０,1,２,３,４,５,６,７,８,９","0,1,2,3,4,5,6,7,8,9")>
						<cfset aCheck.answertext=REReplace(aCheck.answertext,"[^0-9]","","ALL")>
						<cfset aCheck.answertext=left(aCheck.answertext,3)>
							
						<cfif aCheck.answertext GTE 0 AND aCheck.answertext LTE 9>
							<cfset QuerySetCell(#num#, "range1", #aCheck.answertext#)>
						<cfelseif aCheck.answertext GTE 10 AND aCheck.answertext LTE 19>
							<cfset QuerySetCell(#num#, "range2", #aCheck.answertext#)>
						<cfelseif aCheck.answertext GTE 20 AND aCheck.answertext LTE 29>
							<cfset QuerySetCell(#num#, "range3", #aCheck.answertext#)>
						<cfelseif aCheck.answertext GTE 30 AND aCheck.answertext LTE 39>
							<cfset QuerySetCell(#num#, "range4", #aCheck.answertext#)>
						<cfelseif aCheck.answertext GTE 40 AND aCheck.answertext LTE 49>
							<cfset QuerySetCell(#num#, "range5", #aCheck.answertext#)>
						<cfelseif aCheck.answertext GTE 50 AND aCheck.answertext LTE 59>
							<cfset QuerySetCell(#num#, "range6", #aCheck.answertext#)>
						<cfelseif aCheck.answertext GTE 60 AND aCheck.answertext LTE 69>
							<cfset QuerySetCell(#num#, "range7", #aCheck.answertext#)>
						<cfelseif aCheck.answertext GTE 70 AND aCheck.answertext LTE 79>
							<cfset QuerySetCell(#num#, "range8", #aCheck.answertext#)>
						<cfelseif aCheck.answertext GTE 80 AND aCheck.answertext LTE 89>
							<cfset QuerySetCell(#num#, "range9", #aCheck.answertext#)>
						<cfelseif aCheck.answertext GTE 90 AND aCheck.answertext LTE 99>
							<cfset QuerySetCell(#num#, "range10", #aCheck.answertext#)>
						<cfelse>
							<cfset QuerySetCell(#num#, "range11", #aCheck.answertext#)>
						</cfif>
	
					</cfloop>
		
				</cfif>

			</cfloop>

			<cfset tmprange1 = ListToArray(ValueList(tmpOptions.range1))>
			<cfset tmprange2 = ListToArray(ValueList(tmpOptions.range2))>
			<cfset tmprange3 = ListToArray(ValueList(tmpOptions.range3))>
			<cfset tmprange4 = ListToArray(ValueList(tmpOptions.range4))>
			<cfset tmprange5 = ListToArray(ValueList(tmpOptions.range5))>
			<cfset tmprange6 = ListToArray(ValueList(tmpOptions.range6))>
			<cfset tmprange7 = ListToArray(ValueList(tmpOptions.range7))>
			<cfset tmprange8 = ListToArray(ValueList(tmpOptions.range8))>
			<cfset tmprange9 = ListToArray(ValueList(tmpOptions.range9))>
			<cfset tmprange10 = ListToArray(ValueList(tmpOptions.range10))>
			<cfset tmprange11 = ListToArray(ValueList(tmpOptions.range11))>

			<cfif #arg_cross_id# GREATER THAN 0>

				<cfloop query="getLoopCnt">

					<cfif 1 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset tmprange1_1 = ListToArray(ValueList(tmpOptions_1.range1))>
						<cfset tmprange1_2 = ListToArray(ValueList(tmpOptions_1.range2))>
						<cfset tmprange1_3 = ListToArray(ValueList(tmpOptions_1.range3))>
						<cfset tmprange1_4 = ListToArray(ValueList(tmpOptions_1.range4))>
						<cfset tmprange1_5 = ListToArray(ValueList(tmpOptions_1.range5))>
						<cfset tmprange1_6 = ListToArray(ValueList(tmpOptions_1.range6))>
						<cfset tmprange1_7 = ListToArray(ValueList(tmpOptions_1.range7))>
						<cfset tmprange1_8 = ListToArray(ValueList(tmpOptions_1.range8))>
						<cfset tmprange1_9 = ListToArray(ValueList(tmpOptions_1.range9))>
						<cfset tmprange1_10 = ListToArray(ValueList(tmpOptions_1.range10))>
						<cfset tmprange1_11 = ListToArray(ValueList(tmpOptions_1.range11))>
					<cfelseif 2 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset tmprange2_1 = ListToArray(ValueList(tmpOptions_2.range1))>
						<cfset tmprange2_2 = ListToArray(ValueList(tmpOptions_2.range2))>
						<cfset tmprange2_3 = ListToArray(ValueList(tmpOptions_2.range3))>
						<cfset tmprange2_4 = ListToArray(ValueList(tmpOptions_2.range4))>
						<cfset tmprange2_5 = ListToArray(ValueList(tmpOptions_2.range5))>
						<cfset tmprange2_6 = ListToArray(ValueList(tmpOptions_2.range6))>
						<cfset tmprange2_7 = ListToArray(ValueList(tmpOptions_2.range7))>
						<cfset tmprange2_8 = ListToArray(ValueList(tmpOptions_2.range8))>
						<cfset tmprange2_9 = ListToArray(ValueList(tmpOptions_2.range9))>
						<cfset tmprange2_10 = ListToArray(ValueList(tmpOptions_2.range10))>
						<cfset tmprange2_11 = ListToArray(ValueList(tmpOptions_2.range11))>
					<cfelseif 3 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset tmprange3_1 = ListToArray(ValueList(tmpOptions_3.range1))>
						<cfset tmprange3_2 = ListToArray(ValueList(tmpOptions_3.range2))>
						<cfset tmprange3_3 = ListToArray(ValueList(tmpOptions_3.range3))>
						<cfset tmprange3_4 = ListToArray(ValueList(tmpOptions_3.range4))>
						<cfset tmprange3_5 = ListToArray(ValueList(tmpOptions_3.range5))>
						<cfset tmprange3_6 = ListToArray(ValueList(tmpOptions_3.range6))>
						<cfset tmprange3_7 = ListToArray(ValueList(tmpOptions_3.range7))>
						<cfset tmprange3_8 = ListToArray(ValueList(tmpOptions_3.range8))>
						<cfset tmprange3_9 = ListToArray(ValueList(tmpOptions_3.range9))>
						<cfset tmprange3_10 = ListToArray(ValueList(tmpOptions_3.range10))>
						<cfset tmprange3_11 = ListToArray(ValueList(tmpOptions_3.range11))>
					<cfelseif 4 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset tmprange4_1 = ListToArray(ValueList(tmpOptions_4.range1))>
						<cfset tmprange4_2 = ListToArray(ValueList(tmpOptions_4.range2))>
						<cfset tmprange4_3 = ListToArray(ValueList(tmpOptions_4.range3))>
						<cfset tmprange4_4 = ListToArray(ValueList(tmpOptions_4.range4))>
						<cfset tmprange4_5 = ListToArray(ValueList(tmpOptions_4.range5))>
						<cfset tmprange4_6 = ListToArray(ValueList(tmpOptions_4.range6))>
						<cfset tmprange4_7 = ListToArray(ValueList(tmpOptions_4.range7))>
						<cfset tmprange4_8 = ListToArray(ValueList(tmpOptions_4.range8))>
						<cfset tmprange4_9 = ListToArray(ValueList(tmpOptions_4.range9))>
						<cfset tmprange4_10 = ListToArray(ValueList(tmpOptions_4.range10))>
						<cfset tmprange4_11 = ListToArray(ValueList(tmpOptions_4.range11))>
					<cfelseif 5 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset tmprange5_1 = ListToArray(ValueList(tmpOptions_5.range1))>
						<cfset tmprange5_2 = ListToArray(ValueList(tmpOptions_5.range2))>
						<cfset tmprange5_3 = ListToArray(ValueList(tmpOptions_5.range3))>
						<cfset tmprange5_4 = ListToArray(ValueList(tmpOptions_5.range4))>
						<cfset tmprange5_5 = ListToArray(ValueList(tmpOptions_5.range5))>
						<cfset tmprange5_6 = ListToArray(ValueList(tmpOptions_5.range6))>
						<cfset tmprange5_7 = ListToArray(ValueList(tmpOptions_5.range7))>
						<cfset tmprange5_8 = ListToArray(ValueList(tmpOptions_5.range8))>
						<cfset tmprange5_9 = ListToArray(ValueList(tmpOptions_5.range9))>
						<cfset tmprange5_10 = ListToArray(ValueList(tmpOptions_5.range10))>
						<cfset tmprange5_11 = ListToArray(ValueList(tmpOptions_5.range11))>
					<cfelseif 6 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset tmprange6_1 = ListToArray(ValueList(tmpOptions_6.range1))>
						<cfset tmprange6_2 = ListToArray(ValueList(tmpOptions_6.range2))>
						<cfset tmprange6_3 = ListToArray(ValueList(tmpOptions_6.range3))>
						<cfset tmprange6_4 = ListToArray(ValueList(tmpOptions_6.range4))>
						<cfset tmprange6_5 = ListToArray(ValueList(tmpOptions_6.range5))>
						<cfset tmprange6_6 = ListToArray(ValueList(tmpOptions_6.range6))>
						<cfset tmprange6_7 = ListToArray(ValueList(tmpOptions_6.range7))>
						<cfset tmprange6_8 = ListToArray(ValueList(tmpOptions_6.range8))>
						<cfset tmprange6_9 = ListToArray(ValueList(tmpOptions_6.range9))>
						<cfset tmprange6_10 = ListToArray(ValueList(tmpOptions_6.range10))>
						<cfset tmprange6_11 = ListToArray(ValueList(tmpOptions_6.range11))>
					<cfelseif 7 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset tmprange7_1 = ListToArray(ValueList(tmpOptions_7.range1))>
						<cfset tmprange7_2 = ListToArray(ValueList(tmpOptions_7.range2))>
						<cfset tmprange7_3 = ListToArray(ValueList(tmpOptions_7.range3))>
						<cfset tmprange7_4 = ListToArray(ValueList(tmpOptions_7.range4))>
						<cfset tmprange7_5 = ListToArray(ValueList(tmpOptions_7.range5))>
						<cfset tmprange7_6 = ListToArray(ValueList(tmpOptions_7.range6))>
						<cfset tmprange7_7 = ListToArray(ValueList(tmpOptions_7.range7))>
						<cfset tmprange7_8 = ListToArray(ValueList(tmpOptions_7.range8))>
						<cfset tmprange7_9 = ListToArray(ValueList(tmpOptions_7.range9))>
						<cfset tmprange7_10 = ListToArray(ValueList(tmpOptions_7.range10))>
						<cfset tmprange7_11 = ListToArray(ValueList(tmpOptions_7.range11))>
					<cfelseif 8 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset tmprange8_1 = ListToArray(ValueList(tmpOptions_8.range1))>
						<cfset tmprange8_2 = ListToArray(ValueList(tmpOptions_8.range2))>
						<cfset tmprange8_3 = ListToArray(ValueList(tmpOptions_8.range3))>
						<cfset tmprange8_4 = ListToArray(ValueList(tmpOptions_8.range4))>
						<cfset tmprange8_5 = ListToArray(ValueList(tmpOptions_8.range5))>
						<cfset tmprange8_6 = ListToArray(ValueList(tmpOptions_8.range6))>
						<cfset tmprange8_7 = ListToArray(ValueList(tmpOptions_8.range7))>
						<cfset tmprange8_8 = ListToArray(ValueList(tmpOptions_8.range8))>
						<cfset tmprange8_9 = ListToArray(ValueList(tmpOptions_8.range9))>
						<cfset tmprange8_10 = ListToArray(ValueList(tmpOptions_8.range10))>
						<cfset tmprange8_11 = ListToArray(ValueList(tmpOptions_8.range11))>
					<cfelseif 9 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset tmprange9_1 = ListToArray(ValueList(tmpOptions_9.range1))>
						<cfset tmprange9_2 = ListToArray(ValueList(tmpOptions_9.range2))>
						<cfset tmprange9_3 = ListToArray(ValueList(tmpOptions_9.range3))>
						<cfset tmprange9_4 = ListToArray(ValueList(tmpOptions_9.range4))>
						<cfset tmprange9_5 = ListToArray(ValueList(tmpOptions_9.range5))>
						<cfset tmprange9_6 = ListToArray(ValueList(tmpOptions_9.range6))>
						<cfset tmprange9_7 = ListToArray(ValueList(tmpOptions_9.range7))>
						<cfset tmprange9_8 = ListToArray(ValueList(tmpOptions_9.range8))>
						<cfset tmprange9_9 = ListToArray(ValueList(tmpOptions_9.range9))>
						<cfset tmprange9_10 = ListToArray(ValueList(tmpOptions_9.range10))>
						<cfset tmprange9_11 = ListToArray(ValueList(tmpOptions_9.range11))>
					<cfelseif 99 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset tmprange99_1 = ListToArray(ValueList(tmpOptions_99.range1))>
						<cfset tmprange99_2 = ListToArray(ValueList(tmpOptions_99.range2))>
						<cfset tmprange99_3 = ListToArray(ValueList(tmpOptions_99.range3))>
						<cfset tmprange99_4 = ListToArray(ValueList(tmpOptions_99.range4))>
						<cfset tmprange99_5 = ListToArray(ValueList(tmpOptions_99.range5))>
						<cfset tmprange99_6 = ListToArray(ValueList(tmpOptions_99.range6))>
						<cfset tmprange99_7 = ListToArray(ValueList(tmpOptions_99.range7))>
						<cfset tmprange99_8 = ListToArray(ValueList(tmpOptions_99.range8))>
						<cfset tmprange99_9 = ListToArray(ValueList(tmpOptions_99.range9))>
						<cfset tmprange99_10 = ListToArray(ValueList(tmpOptions_99.range10))>
						<cfset tmprange99_11 = ListToArray(ValueList(tmpOptions_99.range11))>
					<cfelseif 0 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset tmprange0_1 = ListToArray(ValueList(tmpOptions_0.range1))>
						<cfset tmprange0_2 = ListToArray(ValueList(tmpOptions_0.range2))>
						<cfset tmprange0_3 = ListToArray(ValueList(tmpOptions_0.range3))>
						<cfset tmprange0_4 = ListToArray(ValueList(tmpOptions_0.range4))>
						<cfset tmprange0_5 = ListToArray(ValueList(tmpOptions_0.range5))>
						<cfset tmprange0_6 = ListToArray(ValueList(tmpOptions_0.range6))>
						<cfset tmprange0_7 = ListToArray(ValueList(tmpOptions_0.range7))>
						<cfset tmprange0_8 = ListToArray(ValueList(tmpOptions_0.range8))>
						<cfset tmprange0_9 = ListToArray(ValueList(tmpOptions_0.range9))>
						<cfset tmprange0_10 = ListToArray(ValueList(tmpOptions_0.range10))>
						<cfset tmprange0_11 = ListToArray(ValueList(tmpOptions_0.range11))>
					</cfif>

				</cfloop>
	
			</cfif>

			<cfset labelList="0～9,10～19,20～29,30～39,40～49,50～59,60～69,70～79,80～89,90～99,100">
			<cfset cnt=1>
			<cfset allsum = 0>
			<cfset allcnt = 0>
			<cfset allsum_1 = 0>
			<cfset allcnt_1 = 0>
			<cfset allsum_2 = 0>
			<cfset allcnt_2 = 0>
			<cfset allsum_3 = 0>
			<cfset allcnt_3 = 0>
			<cfset allsum_4 = 0>
			<cfset allcnt_4 = 0>
			<cfset allsum_5 = 0>
			<cfset allcnt_5 = 0>
			<cfset allsum_6 = 0>
			<cfset allcnt_6 = 0>
			<cfset allsum_7 = 0>
			<cfset allcnt_7 = 0>
			<cfset allsum_8 = 0>
			<cfset allcnt_8 = 0>
			<cfset allsum_9 = 0>
			<cfset allcnt_9 = 0>
			<cfset allsum_99 = 0>
			<cfset allcnt_99 = 0>
			<cfset allsum_0 = 0>
			<cfset allcnt_0 = 0>

			<cfloop list="#labelList#" index="idx">
				<cfset QueryAddrow(retQuery,1)>
				<cfset tmp_cnt= ArrayLen(#Evaluate("tmprange"&cnt)#)>
				<cfset tmp_sum= ArraySum(#Evaluate("tmprange"&cnt)#)>
				<cfset tmp_answer = #IIf(tmp_cnt EQ 0, DE("0"), DE("#tmp_cnt#"))#>
				<cfset tmp_rate = IIF(samples EQ 0, DE(0), DE((#tmp_answer#/#samples#)*100))>
				<cfset QuerySetCell(retQuery, "samples", #tmp_cnt# & "/" & #NumberFormat(tmp_rate,'._')#)>
				<cfset QuerySetCell(retQuery, "rate", #NumberFormat(tmp_rate,'._')#)>
				<cfset QuerySetCell(retQuery, "label", "#idx#")>
				<cfset QuerySetCell(retQuery, "options_no", cnt)>

				<cfset allsum = allsum+tmp_sum>
				<cfset allcnt = allcnt+tmp_cnt>

				<cfif #arg_cross_id# GREATER THAN 0>

					<cfloop query="getLoopCnt">
	
						<cfif 1 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
							<cfset tmp_cnt_1= ArrayLen(#Evaluate("tmprange1_"&cnt)#)>
							<cfset tmp_sum_1= ArraySum(#Evaluate("tmprange1_"&cnt)#)>
							<cfset tmp_answer_1 = #IIf(tmp_cnt_1 EQ 0, DE("0"), DE("#tmp_cnt_1#"))#>
							<cfset tmp_rate_1 = IIF(samples_1 EQ 0, DE(0), DE((#tmp_answer_1#/#samples_1#)*100))>
							<cfset QuerySetCell(retQuery, "samples_1", #tmp_cnt_1# & "/" & #NumberFormat(tmp_rate_1,'._')#)>
							<cfset allsum_1 = allsum_1+tmp_sum_1>
							<cfset allcnt_1 = allcnt_1+tmp_cnt_1>
						<cfelseif 2 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
							<cfset tmp_cnt_2= ArrayLen(#Evaluate("tmprange2_"&cnt)#)>
							<cfset tmp_sum_2= ArraySum(#Evaluate("tmprange2_"&cnt)#)>
							<cfset tmp_answer_2 = #IIf(tmp_cnt_2 EQ 0, DE("0"), DE("#tmp_cnt_2#"))#>
							<cfset tmp_rate_2 = IIF(samples_2 EQ 0, DE(0), DE((#tmp_answer_2#/#samples_2#)*100))>
							<cfset QuerySetCell(retQuery, "samples_2", #tmp_cnt_2# & "/" & #NumberFormat(tmp_rate_2,'._')#)>
							<cfset allsum_2 = allsum_2+tmp_sum_2>
							<cfset allcnt_2 = allcnt_2+tmp_cnt_2>
						<cfelseif 3 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
							<cfset tmp_cnt_3= ArrayLen(#Evaluate("tmprange3_"&cnt)#)>
							<cfset tmp_sum_3= ArraySum(#Evaluate("tmprange3_"&cnt)#)>
							<cfset tmp_answer_3 = #IIf(tmp_cnt_3 EQ 0, DE("0"), DE("#tmp_cnt_3#"))#>
							<cfset tmp_rate_3 = IIF(samples_3 EQ 0, DE(0), DE((#tmp_answer_3#/#samples_3#)*100))>
							<cfset QuerySetCell(retQuery, "samples_3", #tmp_cnt_3# & "/" & #NumberFormat(tmp_rate_3,'._')#)>
							<cfset allsum_3 = allsum_3+tmp_sum_3>
							<cfset allcnt_3 = allcnt_3+tmp_cnt_3>
						<cfelseif 4 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
							<cfset tmp_cnt_4= ArrayLen(#Evaluate("tmprange4_"&cnt)#)>
							<cfset tmp_sum_4= ArraySum(#Evaluate("tmprange4_"&cnt)#)>
							<cfset tmp_answer_4 = #IIf(tmp_cnt_4 EQ 0, DE("0"), DE("#tmp_cnt_4#"))#>
							<cfset tmp_rate_4 = IIF(samples_4 EQ 0, DE(0), DE((#tmp_answer_4#/#samples_4#)*100))>
							<cfset QuerySetCell(retQuery, "samples_4", #tmp_cnt_4# & "/" & #NumberFormat(tmp_rate_4,'._')#)>
							<cfset allsum_4 = allsum_4+tmp_sum_4>
							<cfset allcnt_4 = allcnt_4+tmp_cnt_4>
						<cfelseif 5 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
							<cfset tmp_cnt_5= ArrayLen(#Evaluate("tmprange5_"&cnt)#)>
							<cfset tmp_sum_5= ArraySum(#Evaluate("tmprange5_"&cnt)#)>
							<cfset tmp_answer_5 = #IIf(tmp_cnt_5 EQ 0, DE("0"), DE("#tmp_cnt_5#"))#>
							<cfset tmp_rate_5 = IIF(samples_5 EQ 0, DE(0), DE((#tmp_answer_5#/#samples_5#)*100))>
							<cfset QuerySetCell(retQuery, "samples_5", #tmp_cnt_5# & "/" & #NumberFormat(tmp_rate_5,'._')#)>
							<cfset allsum_5 = allsum_5+tmp_sum_5>
							<cfset allcnt_5 = allcnt_5+tmp_cnt_5>
						<cfelseif 6 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
							<cfset tmp_cnt_6= ArrayLen(#Evaluate("tmprange6_"&cnt)#)>
							<cfset tmp_sum_6= ArraySum(#Evaluate("tmprange6_"&cnt)#)>
							<cfset tmp_answer_6 = #IIf(tmp_cnt_6 EQ 0, DE("0"), DE("#tmp_cnt_6#"))#>
							<cfset tmp_rate_6 = IIF(samples_6 EQ 0, DE(0), DE((#tmp_answer_6#/#samples_6#)*100))>
							<cfset QuerySetCell(retQuery, "samples_6", #tmp_cnt_6# & "/" & #NumberFormat(tmp_rate_6,'._')#)>
							<cfset allsum_6 = allsum_6+tmp_sum_6>
							<cfset allcnt_6 = allcnt_6+tmp_cnt_6>
						<cfelseif 7 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
							<cfset tmp_cnt_7= ArrayLen(#Evaluate("tmprange7_"&cnt)#)>
							<cfset tmp_sum_7= ArraySum(#Evaluate("tmprange7_"&cnt)#)>
							<cfset tmp_answer_7 = #IIf(tmp_cnt_7 EQ 0, DE("0"), DE("#tmp_cnt_7#"))#>
							<cfset tmp_rate_7 = IIF(samples_7 EQ 0, DE(0), DE((#tmp_answer_7#/#samples_7#)*100))>
							<cfset QuerySetCell(retQuery, "samples_7", #tmp_cnt_7# & "/" & #NumberFormat(tmp_rate_7,'._')#)>
							<cfset allsum_7 = allsum_7+tmp_sum_7>
							<cfset allcnt_7 = allcnt_7+tmp_cnt_7>
						<cfelseif 8 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
							<cfset tmp_cnt_8= ArrayLen(#Evaluate("tmprange8_"&cnt)#)>
							<cfset tmp_sum_8= ArraySum(#Evaluate("tmprange8_"&cnt)#)>
							<cfset tmp_answer_8 = #IIf(tmp_cnt_8 EQ 0, DE("0"), DE("#tmp_cnt_8#"))#>
							<cfset tmp_rate_8 = IIF(samples_8 EQ 0, DE(0), DE((#tmp_answer_8#/#samples_8#)*100))>
							<cfset QuerySetCell(retQuery, "samples_8", #tmp_cnt_8# & "/" & #NumberFormat(tmp_rate_8,'._')#)>
							<cfset allsum_8 = allsum_8+tmp_sum_8>
							<cfset allcnt_8 = allcnt_8+tmp_cnt_8>
						<cfelseif 9 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
							<cfset tmp_cnt_9= ArrayLen(#Evaluate("tmprange9_"&cnt)#)>
							<cfset tmp_sum_9= ArraySum(#Evaluate("tmprange9_"&cnt)#)>
							<cfset tmp_answer_9 = #IIf(tmp_cnt_9 EQ 0, DE("0"), DE("#tmp_cnt_9#"))#>
							<cfset tmp_rate_9 = IIF(samples_9 EQ 0, DE(0), DE((#tmp_answer_9#/#samples_9#)*100))>
							<cfset QuerySetCell(retQuery, "samples_9", #tmp_cnt_9# & "/" & #NumberFormat(tmp_rate_9,'._')#)>
							<cfset allsum_9 = allsum_9+tmp_sum_9>
							<cfset allcnt_9 = allcnt_9+tmp_cnt_9>
						<cfelseif 99 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
							<cfset tmp_cnt_99= ArrayLen(#Evaluate("tmprange99_"&cnt)#)>
							<cfset tmp_sum_99= ArraySum(#Evaluate("tmprange99_"&cnt)#)>
							<cfset tmp_answer_99 = #IIf(tmp_cnt_99 EQ 0, DE("0"), DE("#tmp_cnt_99#"))#>
							<cfset tmp_rate_99 = IIF(samples_99 EQ 0, DE(0), DE((#tmp_answer_99#/#samples_99#)*100))>
							<cfset QuerySetCell(retQuery, "samples_99", #tmp_cnt_99# & "/" & #NumberFormat(tmp_rate_99,'._')#)>
							<cfset allsum_99 = allsum_99+tmp_sum_99>
							<cfset allcnt_99 = allcnt_99+tmp_cnt_99>
						<cfelseif 0 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
							<cfset tmp_cnt_0= ArrayLen(#Evaluate("tmprange0_"&cnt)#)>
							<cfset tmp_sum_0= ArraySum(#Evaluate("tmprange0_"&cnt)#)>
							<cfset tmp_answer_0 = #IIf(tmp_cnt_0 EQ 0, DE("0"), DE("#tmp_cnt_0#"))#>
							<cfset tmp_rate_0 = IIF(samples_0 EQ 0, DE(0), DE((#tmp_answer_0#/#samples_0#)*100))>
							<cfset QuerySetCell(retQuery, "samples_0", #tmp_cnt_0# & "/" & #NumberFormat(tmp_rate_0,'._')#)>
							<cfset allsum_0 = allsum_0+tmp_sum_0>
							<cfset allcnt_0 = allcnt_99+tmp_cnt_0>
						</cfif>
	
					</cfloop>
		
				</cfif>
				<cfset cnt=cnt+1>
			</cfloop>

			<cfset QueryAddRow(retQuery, 1)>
			<cfset tmp_rate = IIF(samples EQ 0, DE(0), DE((#temp_0res#/#samples#)*100))>
			<cfset QuerySetCell(retQuery, "samples", #temp_0res# & "/" & #NumberFormat(tmp_rate,'._')#)>
			<cfset QuerySetCell(retQuery, "rate", #NumberFormat(tmp_rate,'._')#)>
			<cfset QuerySetCell(retQuery, "label", '不明・未回答')>
			<cfset QuerySetCell(retQuery, "options_no", 0)>

			<cfif #arg_cross_id# GREATER THAN 0>

				<cfloop query="getLoopCnt">

					<cfif 1 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset tmp_rate_1 = IIF(samples_1 EQ 0, DE(0), DE((#temp_1res#/#samples_1#)*100))>
						<cfset QuerySetCell(retQuery, "samples_1", #temp_1res# & "/" & #NumberFormat(tmp_rate_1,'._')#)>
					<cfelseif 2 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset tmp_rate_2 = IIF(samples_2 EQ 0, DE(0), DE((#temp_2res#/#samples_2#)*100))>
						<cfset QuerySetCell(retQuery, "samples_2", #temp_2res# & "/" & #NumberFormat(tmp_rate_2,'._')#)>
					<cfelseif 3 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset tmp_rate_3 = IIF(samples_3 EQ 0, DE(0), DE((#temp_3res#/#samples_3#)*100))>
						<cfset QuerySetCell(retQuery, "samples_3", #temp_3res# & "/" & #NumberFormat(tmp_rate_3,'._')#)>
					<cfelseif 4 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset tmp_rate_4 = IIF(samples_4 EQ 0, DE(0), DE((#temp_4res#/#samples_4#)*100))>
						<cfset QuerySetCell(retQuery, "samples_4", #temp_4res# & "/" & #NumberFormat(tmp_rate_4,'._')#)>
					<cfelseif 5 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset tmp_rate_5 = IIF(samples_5 EQ 0, DE(0), DE((#temp_5res#/#samples_5#)*100))>
						<cfset QuerySetCell(retQuery, "samples_5", #temp_5res# & "/" & #NumberFormat(tmp_rate_5,'._')#)>
					<cfelseif 6 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset tmp_rate_6 = IIF(samples_6 EQ 0, DE(0), DE((#temp_6res#/#samples_6#)*100))>
						<cfset QuerySetCell(retQuery, "samples_6", #temp_6res# & "/" & #NumberFormat(tmp_rate_6,'._')#)>
					<cfelseif 7 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset tmp_rate_7 = IIF(samples_7 EQ 0, DE(0), DE((#temp_7res#/#samples_7#)*100))>
						<cfset QuerySetCell(retQuery, "samples_7", #temp_7res# & "/" & #NumberFormat(tmp_rate_7,'._')#)>
					<cfelseif 8 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset tmp_rate_8 = IIF(samples_8 EQ 0, DE(0), DE((#temp_8res#/#samples_8#)*100))>
						<cfset QuerySetCell(retQuery, "samples_8", #temp_8res# & "/" & #NumberFormat(tmp_rate_8,'._')#)>
					<cfelseif 9 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset tmp_rate_9 = IIF(samples_9 EQ 0, DE(0), DE((#temp_9res#/#samples_9#)*100))>
						<cfset QuerySetCell(retQuery, "samples_9", #temp_9res# & "/" & #NumberFormat(tmp_rate_9,'._')#)>
					<cfelseif 99 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset tmp_rate_99 = IIF(samples_99 EQ 0, DE(0), DE((#temp_99res#/#samples_99#)*100))>
						<cfset QuerySetCell(retQuery, "samples_99", #temp_99res# & "/" & #NumberFormat(tmp_rate_99,'._')#)>
					<cfelseif 0 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset tmp_rate_0 = IIF(samples_0 EQ 0, DE(0), DE((#temp_0res#/#samples_0#)*100))>
						<cfset QuerySetCell(retQuery, "samples_0", #temp_0res# & "/" & #NumberFormat(tmp_rate_0,'._')#)>
					</cfif>

				</cfloop>
	
			</cfif>

			<cfset QueryAddrow(retQuery,1)>
			<cfif allcnt EQ 0>
				<cfset tmp_rate = 0>
			<cfelse>
				<cfset tmp_rate = #allsum#/#allcnt#>
			</cfif>
			<cfset QuerySetCell(retQuery, "samples", #allcnt# & "/" & #NumberFormat(tmp_rate,'.__')#)>
			<cfset QuerySetCell(retQuery, "rate", #NumberFormat(tmp_rate,'.__')#)>
			<cfset QuerySetCell(retQuery, "label", '平均値')>
			<cfset QuerySetCell(retQuery, "options_no", '')>

			<cfif #arg_cross_id# GREATER THAN 0>

				<cfloop query="getLoopCnt">

					<cfif 1 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfif allcnt_1 EQ 0>
							<cfset tmp_rate_1 = 0>
						<cfelse>
							<cfset tmp_rate_1 = #allsum_1#/#allcnt_1#>
						</cfif>
						<cfset QuerySetCell(retQuery, "samples_1", #allcnt_1# & "/" & #NumberFormat(tmp_rate_1,'.__')#)>
					<cfelseif 2 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfif allcnt_2 EQ 0>
							<cfset tmp_rate_2 = 0>
						<cfelse>
							<cfset tmp_rate_2 = #allsum_2#/#allcnt_2#>
						</cfif>
						<cfset QuerySetCell(retQuery, "samples_2", #allcnt_2# & "/" & #NumberFormat(tmp_rate_2,'.__')#)>
					<cfelseif 3 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfif allcnt_3 EQ 0>
							<cfset tmp_rate_3 = 0>
						<cfelse>
							<cfset tmp_rate_3 = #allsum_3#/#allcnt_3#>
						</cfif>
						<cfset QuerySetCell(retQuery, "samples_3", #allcnt_3# & "/" & #NumberFormat(tmp_rate_3,'.__')#)>
					<cfelseif 4 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfif allcnt_4 EQ 0>
							<cfset tmp_rate_4 = 0>
						<cfelse>
							<cfset tmp_rate_4 = #allsum_4#/#allcnt_4#>
						</cfif>
						<cfset QuerySetCell(retQuery, "samples_4", #allcnt_4# & "/" & #NumberFormat(tmp_rate_4,'.__')#)>
					<cfelseif 5 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfif allcnt_5 EQ 0>
							<cfset tmp_rate_5 = 0>
						<cfelse>
							<cfset tmp_rate_5 = #allsum_5#/#allcnt_5#>
						</cfif>
						<cfset QuerySetCell(retQuery, "samples_5", #allcnt_5# & "/" & #NumberFormat(tmp_rate_5,'.__')#)>
					<cfelseif 6 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfif allcnt_6 EQ 0>
							<cfset tmp_rate_6 = 0>
						<cfelse>
							<cfset tmp_rate_6 = #allsum_6#/#allcnt_6#>
						</cfif>
						<cfset QuerySetCell(retQuery, "samples_6", #allcnt_6# & "/" & #NumberFormat(tmp_rate_6,'.__')#)>
					<cfelseif 7 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfif allcnt_7 EQ 0>
							<cfset tmp_rate_7 = 0>
						<cfelse>
							<cfset tmp_rate_7 = #allsum_7#/#allcnt_7#>
						</cfif>
						<cfset QuerySetCell(retQuery, "samples_7", #allcnt_7# & "/" & #NumberFormat(tmp_rate_7,'.__')#)>
					<cfelseif 8 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfif allcnt_8 EQ 0>
							<cfset tmp_rate_8 = 0>
						<cfelse>
							<cfset tmp_rate_8 = #allsum_8#/#allcnt_8#>
						</cfif>
						<cfset QuerySetCell(retQuery, "samples_8", #allcnt_8# & "/" & #NumberFormat(tmp_rate_8,'.__')#)>
					<cfelseif 9 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfif allcnt_9 EQ 0>
							<cfset tmp_rate_9 = 0>
						<cfelse>
							<cfset tmp_rate_9 = #allsum_9#/#allcnt_9#>
						</cfif>
						<cfset QuerySetCell(retQuery, "samples_9", #allcnt_9# & "/" & #NumberFormat(tmp_rate_9,'.__')#)>
					<cfelseif 99 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfif allcnt_99 EQ 0>
							<cfset tmp_rate_99 = 0>
						<cfelse>
							<cfset tmp_rate_99 = #allsum_99#/#allcnt_99#>
						</cfif>
						<cfset QuerySetCell(retQuery, "samples_99", #allcnt_99# & "/" & #NumberFormat(tmp_rate_99,'.__')#)>
					<cfelseif 0 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfif allcnt_0 EQ 0>
							<cfset tmp_rate_0 = 0>
						<cfelse>
							<cfset tmp_rate_0 = #allsum_0#/#allcnt_0#>
						</cfif>
						<cfset QuerySetCell(retQuery, "samples_0", #allcnt_0# & "/" & #NumberFormat(tmp_rate_0,'.__')#)>
					</cfif>

				</cfloop>
	
			</cfif>

			<cfset QueryAddrow(retQuery,1)>
			<cfset QuerySetCell(retQuery, "samples", #allsum# & "/" & '-')>
			<cfset QuerySetCell(retQuery, "rate", '-')>
			<cfset QuerySetCell(retQuery, "label", '累計値')>
			<cfset QuerySetCell(retQuery, "options_no", '')>

			<cfif #arg_cross_id# GREATER THAN 0>

				<cfloop query="getLoopCnt">

					<cfif 1 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset QuerySetCell(retQuery, "samples_1", #allsum_1# & "/" & '-')>
					<cfelseif 2 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset QuerySetCell(retQuery, "samples_2", #allsum_2# & "/" & '-')>
					<cfelseif 3 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset QuerySetCell(retQuery, "samples_3", #allsum_3# & "/" & '-')>
					<cfelseif 4 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset QuerySetCell(retQuery, "samples_4", #allsum_4# & "/" & '-')>
					<cfelseif 5 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset QuerySetCell(retQuery, "samples_5", #allsum_5# & "/" & '-')>
					<cfelseif 6 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset QuerySetCell(retQuery, "samples_6", #allsum_6# & "/" & '-')>
					<cfelseif 7 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset QuerySetCell(retQuery, "samples_7", #allsum_7# & "/" & '-')>
					<cfelseif 8 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset QuerySetCell(retQuery, "samples_8", #allsum_8# & "/" & '-')>
					<cfelseif 9 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset QuerySetCell(retQuery, "samples_9", #allsum_9# & "/" & '-')>
					<cfelseif 99 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset QuerySetCell(retQuery, "samples_99", #allsum_99# & "/" & '-')>
					<cfelseif 0 IS #getLoopCnt.options_no[getLoopCnt.currentrow]#>
						<cfset QuerySetCell(retQuery, "samples_0", #allsum_0# & "/" & '-')>
					</cfif>

				</cfloop>
	
			</cfif>
			<!--- 2005/10/26 h.miyahara(NetFusion) MOD 100点満点レンジ機能追加 E --->

		<cfelse>

			<!--- 各回答数取得 --->
			<cfloop query="GetAnsweroptions">
	
				<cfset QueryAddRow(retQuery, 1)>
	
				<cfquery dbtype="query" name="answerCheck">
					select	 count( distinct referencenumber) as answer
					from	 getAnswers
					where	 questionnaire_id = #arguments.questionnaire_id#
					and		 answer = #GetAnsweroptions.options_no[GetAnsweroptions.currentrow]#
				</cfquery>
	
				<cfset tmp_answer = #IIf(answerCheck.recordCount EQ 0, DE("0"), DE("#answerCheck.answer#"))#>
	
				<cfif samples EQ 0>
					<cfset tmp_rate = 0>
				<cfelse>
					<cfset tmp_rate = (#tmp_answer#/#samples#)*100>
				</cfif>
	
				<cfset QuerySetCell(retQuery, "samples", #tmp_answer# & "/" & #NumberFormat(tmp_rate,'._')#)>
				<cfset QuerySetCell(retQuery, "rate", #NumberFormat(tmp_rate,'._')#)>
				<cfset QuerySetCell(retQuery, "label", #options_label#)>
				<cfset QuerySetCell(retQuery, "options_no", #options_no#)>
	
				<cfif #arg_cross_id# GREATER THAN 0>

					<cfloop query="getLoopCnt">
	
						<cfset num=evaluate('cross' & #arg_cross_id# & '_' & #getLoopCnt.options_no[getLoopCnt.currentrow]# & '_list')>
						<cfset strWhere = " and referencenumber IN (#PreserveSingleQuotes(num)#) ">
		
						<cfquery dbtype="query" name="checkLoop">
							select	count( distinct referencenumber) as answer
							from	getAnswers
							where	questionnaire_id = #arguments.questionnaire_id#
							and		answer = #GetAnsweroptions.options_no[GetAnsweroptions.currentrow]# 
							#PreserveSingleQuotes(strWhere)#
						</cfquery>
	
						<cfset tmp_answer = #IIf(checkLoop.recordCount EQ 0, DE("0"), DE("#checkLoop.answer#"))#>
			
						<cfset sam=evaluate('samples_' & #getLoopCnt.options_no[getLoopCnt.currentrow]#)>
						<cfif #sam# EQ 0>
							<cfset tmp_rate = 0>
						<cfelse>
							<cfset tmp_rate = (#tmp_answer#/#sam#)*100>
						</cfif>
						<cfset QuerySetCell(retQuery, "samples_" & #getLoopCnt.options_no[getLoopCnt.currentrow]#, #tmp_answer# & "/" & #NumberFormat(tmp_rate,'._')#)>
	
					</cfloop>
		
				</cfif>
	
			</cfloop>

			<!--- 未回答・不明 --->
			<cfset QueryAddRow(retQuery, 1)>
	
			<cfquery dbtype="query" name="answerCheck">
				select	 count( distinct referencenumber) as answer
				from	 getAnswers
				where	 questionnaire_id = #arguments.questionnaire_id#
				and		 answer = 0
			</cfquery>
	
			<cfset tmp_answer = #IIf(answerCheck.recordCount EQ 0, DE("0"), DE("#answerCheck.answer#"))#>
	
			<cfif samples EQ 0>
				<cfset tmp_rate = 0>
			<cfelse>
				<cfset tmp_rate = (#tmp_answer#/#samples#)*100>
			</cfif>
	
			<cfset QuerySetCell(retQuery, "options_no", 0)>
			<cfset QuerySetCell(retQuery, "samples", #tmp_answer# & "/" & #NumberFormat(tmp_rate,'._')#)>
			<cfset QuerySetCell(retQuery, "rate", #NumberFormat(tmp_rate,'._')#)>
			<cfset QuerySetCell(retQuery, "label", '不明・未回答')>
	
			<cfif #arg_cross_id# GREATER THAN 0>

				<cfloop query="getLoopCnt">
	
					<cfset num=evaluate('cross' & #arg_cross_id# & '_' & #getLoopCnt.options_no[getLoopCnt.currentrow]# & '_list')>
					<cfset strWhere = " and referencenumber IN (#PreserveSingleQuotes(num)#) ">
	
					<cfquery dbtype="query" name="checkLoop">
						select	count( distinct referencenumber) as answer
						from	getAnswers
						where	questionnaire_id = #arguments.questionnaire_id#
						and		answer = 0 
						#PreserveSingleQuotes(strWhere)#
					</cfquery>
	
					<cfset tmp_answer = #IIf(checkLoop.recordCount EQ 0, DE("0"), DE("#checkLoop.answer#"))#>
		
					<cfset sam=evaluate('samples_' & #getLoopCnt.options_no[getLoopCnt.currentrow]#)>
					<cfif #sam# EQ 0>
						<cfset tmp_rate = 0>
					<cfelse>
						<cfset tmp_rate = (#tmp_answer#/#sam#)*100>
					</cfif>
					<cfset QuerySetCell(retQuery, "samples_" & #getLoopCnt.options_no[getLoopCnt.currentrow]#, #tmp_answer# & "/" & #NumberFormat(tmp_rate,'._')#)>
	
				</cfloop>
	
			</cfif>

		</cfif>

		<cfset cnt = 0>
		<cfloop query="retQuery">
			<cfif #cnt# IS 0>
				<cfset retLabel = "label_" & #cnt#>
			<cfelse>
				<cfset retLabel = #retLabel# & ", label_" & #cnt#>
			</cfif>
			<cfset cnt = #cnt# + 1>
		</cfloop>	
		<cfset retLabel = #retLabel# & ", label_" & #cnt#>
		<cfset retValue = #retQuery#>

		<!--- 戻値クエリ作成 --->
		<cfset cnt = 0>
		<cfif #arg_cross_id# GREATER THAN 0>
			<cfset cnt = #getLoopCnt.recordCount#>
		</cfif>

		<cfset retQuery = QueryNew(#retLabel#)>
		<cfset QueryAddRow(retQuery, 2 + #cnt#)>

		<cfif #arg_cross_id# GREATER THAN 0>

			<cfset cnt = 0>

			<cfquery name="getQuestionnaire_nm" datasource = "#Application.datasource#">
				select distinct
					mst_questionnaire.questionnaire_nm
				from mst_questionnaire
				where (
					((mst_questionnaire.questionnaire_id)=#arguments.questionnaire_id#) and 
					((mst_questionnaire.active_flg)=1)
				)
			</cfquery>

			<cfset QuerySetCell(retQuery, "label_" & #cnt#, #getQuestionnaire_nm.questionnaire_nm#, 1)>
			<cfset QuerySetCell(retQuery, "label_" & #cnt#, #getQuestionnaire_nm.questionnaire_nm# & "TOTAL", 2)>

			<cfset curRow = 1>
			<cfloop query="getLoopCnt">
				<cfset QuerySetCell(retQuery, "label_" & #cnt#, #getLoopCnt.options_label[getLoopCnt.currentrow]#, 2 + curRow)>
				<cfset curRow = #curRow# + 1>
			</cfloop>

		</cfif>

		<cfset cnt = 1>

		<cfloop query="retValue">
			<cfif arguments.arg_cross_id eq 0>
				<cfset QuerySetCell(retQuery, "label_0", "", 1)>
				<cfset QuerySetCell(retQuery, "label_0", "TOTAL", 2)>
			</cfif>

			<cfset QuerySetCell(retQuery, "label_" & #cnt#, #retValue.label#, 1)>
			<cfset QuerySetCell(retQuery, "label_" & #cnt#, #retValue.samples#, 2)>

			<cfif #arg_cross_id# GREATER THAN 0>

				<cfset curRow = 1>
				<cfloop query="getLoopCnt">
					<cfset sam=evaluate("retValue.samples_#getLoopCnt.options_no[getLoopCnt.currentrow]#[#retValue.currentrow#]")>
					<cfset QuerySetCell(retQuery, "label_" & #cnt#, #sam#, 2 + curRow)>
					<cfset curRow = #curRow# + 1>
				</cfloop>
			</cfif>
			<cfset cnt = #cnt# + 1>

		</cfloop>	

		<cfreturn retQuery>
			<cfsetting enablecfoutputonly="No">

			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
	
	</cffunction>
	<!---
	*************************************************************************************************
	* Class				:csmulti
	* Method			:OutputCSV
	* Description		:OA検索結果一覧ＣＳＶ出力
	* Custom Attributes	:resultObj	出力リストのデータプロバイダ
	* 					:fieldnames	列名
	* 					:headerText	列のキャプション
	* Return Paramters	:
	* History			:1) Coded by	2004/10/05 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="OutputCSV" access="remote" returntype="any">
		<cfargument name="resultObj" type="Array" default="">
		<cfargument name="fieldnames" type="Array" default="">
		<cfargument name="qstseleted" type="String" default="">
		<cfargument name="selecteddate" type="String" default="">
		<cfargument name="condseleted" type="String" default="">
		<cfargument name="resort_code" type="String" default="0">
		<cfargument name="outputtype" type="Numeric" default="0">
		<cftry>
			<!--- リゾート名 --->
			<cfif arguments.resort_code eq 0>
				<cfset Application.resortname = "共通">
			<cfelse>
				<cfquery name="getresort" datasource = "#Application.datasource#">
					select	 resort_nm 
					from	 #Application.app00.dbu#.mst_resort
					where	 resort_code in (#arguments.resort_code#)
				</cfquery>
				<cfset Application.resortname = "#valuelist(getresort.resort_nm,'-')#">
			</cfif>
			<!--- ディレクトリ＆ファイル情報 --->
			<cfset filePath = ExpandPath(#application.outputDirectory#)>
			<cfset Dircheck = DirectoryExists( filePath & "\" & #Replace(session.user_info.user_ip,".","","all")# )>
			
			<!--- ディレクトリ存在チェック --->
			<cfif Dircheck eq "no">
				<cfdirectory action = "create" directory = "#filePath#\#Replace(session.user_info.user_ip,'.','','all')#">
			</cfif>
			
			<cfset UserDir = #Replace(session.user_info.user_ip,".","","all")# & "\">
			<cfset datepart = #DateFormat(Now(),'YYMMDD')# & #TimeFormat(Now(),'HHMMSS')#>
			<cfset filename = filePath & UserDir & datepart & "CRMResultsList.xls">
			<cfset filecheck = FileExists( filename )>
			
			<!--- ディレクトリ内ファイル数チェック --->
			<cfdirectory 
				directory="#filePath#\#Replace(session.user_info.user_ip,'.','','all')#" 
				name="myCSVDirectory"
				sort="DATELASTMODIFIED ASC">
				
			<!--- 指定ファイル数以上は作らない。古いものから削除 --->
			<cfset FileHistory = 10>
			<cfif myCSVDirectory.RecordCount GTE FileHistory>
				<cffile action = "delete" file = "#filePath##UserDir##myCSVDirectory.Name#">
			</cfif>	
		
			<!--- ファイル存在チェック --->
			<cfif filecheck eq "no">
				<cfset arguments.selecteddate = Replace(arguments.selecteddate,',','|','all')>
				<cfset arguments.condseleted = Replace(arguments.condseleted,',','|','all')>
				<cfset arg_condseleted = iif(len(arguments.condseleted) neq 0,de('【#arguments.condseleted#】'),de(''))>
				
				<!--- xlsブック作成＆スタイル設定 --->
				<cfinclude template="cfpoi_style.cfm">

				<!--- xlsシート作成	--->
				<cfset sheet = wb.createSheet()/>
				<cfset wb.setSheetName(0,"MKT集計結果")/>
				<cfset sheet.setColumnWidth(0, javacast("int" , 20*256 ))/>

				<!--- 条件エリア --->
				<cfset row = sheet.createRow(0)/>
				<cfset cell = row.createCell(0)/>
				<!--- --->
				<cfset cell.setCellStyle(strobj)/>
				<cfset cell.setCellValue("【#arguments.qstseleted#】【#arguments.selecteddate#】#arg_condseleted#【出力日時：#dateformat(now(),'YYYY/MM/DD')# #timeformat(now(),'HH:MM')#】")>
				<cfset columnlength = arraylen(arguments.fieldnames)-1>
				<!--- region(開始上部位置、開始左位置、終了下部位置、終了右位置) --->
				<cfset region = createObject("java","org.apache.poi.hssf.util.Region").init(0,0,2,javacast("int",columnlength))/>
				<cfset sheet.addMergedRegion(region)/>

				<!--- ヘッダーエリア --->
				<cfset headerlist=ArrayToList(arguments.fieldnames)>
				<cfset row = sheet.createRow(3)/>
				<cfset cellseq = 0>
				<cfloop list="#headerlist#" index="h_idx">
					<cfset cell = row.createCell(javacast("int",cellseq))/>
					
					<cfset cell.setCellStyle(hstyleobj)/>
					<cfset wk_idx = evaluate("arguments.resultObj[1]." & h_idx )>
					<cfset cell.setCellValue("#wk_idx#")>
					<cfset cellseq = cellseq+1>
				</cfloop>

				<!--- 出力内容整形 改行コード除去 --->
				<cfset rowseq = 4>
				<cfloop from="2" to="#ArrayLen(resultObj)#" index="cnt">
					<cfset num_txt = "">
					<cfset rto_txt = "">

					<!--- 行追加 --->
					<cfset row = sheet.createRow(javacast("int",rowseq))/>
					<cfset rowseq = rowseq+1>
					<cfset raterow = sheet.createRow(javacast("int",rowseq))/>

					<cfset cellseq = 0>
					<cfloop from="1" to="#ArrayLen(fieldnames)#" index="c">
						<cfset wk_num = replacelist(Evaluate("resultObj[cnt].#fieldnames[c]#"),"#chr(13)##chr(10)#,#chr(13)#,#chr(10)#,#chr(9)#",",,,")>

						<!--- 左ラベル（クロス項目）設定 --->
						<cfif c eq 1>
							<cfset cell = row.createCell(javacast("int",cellseq))/>
							
							<cfset cell.setCellStyle(styleobj)/>
							<cfset cell.setCellValue("#wk_num#")>

							<cfset cell = raterow.createCell(javacast("int",cellseq))/>
							<cfset cell.setCellStyle(styleobj)/>

							<cfset region = createObject("java","org.apache.poi.hssf.util.Region").init(javacast("int",rowseq-1),0,javacast("int",rowseq),0)/>
							<cfset sheet.addMergedRegion(region)/>
							
						<!--- 各列データ設定 --->
						<cfelse>
							<cfset num_txt = listgetat(wk_num,1,"/")>
							<cfif listlen(wk_num,"/") lt 2>
								<cfset rto_txt = "#chr(9)#">
							<cfelse>
								<cfset rto_txt = listgetat(wk_num,2,"/")>
							</cfif>
							
							<cfif arguments.outputtype eq 0>
								<!--- 実数 --->
								<cfset cell = row.createCell(javacast("int",cellseq))/>
								
								<cfset cell.setCellStyle(numstyleobj)/>
								<cfset cell.setCellValue(javacast("double","#num_txt#"))>

								<!--- 割合 --->
								<cfif isnumeric(rto_txt)>
									<cfset cell = raterow.createCell(javacast("int",cellseq))/>
									
									<cfset cell.setCellStyle(ratestyleobj)/>
									<cfset cell.setCellValue(javacast("double","#rto_txt#"))>
								<cfelse>
									<cfset cell = row.createCell(javacast("int",cellseq))/>
									
									<cfset cell.setCellStyle(styleobj)/>
									<cfset cell.setCellValue("#rto_txt#")>
								</cfif>

							<!--- TOTAL＋％表示 --->
							<cfelse>
								<cfif cellseq eq 1>
									<cfset cell = row.createCell(javacast("int",cellseq))/>
									
									<cfset cell.setCellStyle(numstyleobj)/>
									<cfset cell.setCellValue(javacast("double","#num_txt#"))>
								<cfelse>
									<cfif isnumeric(rto_txt)>
										<cfset cell = row.createCell(javacast("int",cellseq))/>
										
										<cfset cell.setCellStyle(ratestyleobj)/>
										<cfset cell.setCellValue(javacast("double","#rto_txt#"))>
									<cfelse>
										<cfset cell = row.createCell(javacast("int",cellseq))/>
										
										<cfset cell.setCellStyle(styleobj)/>
										<cfset cell.setCellValue("#rto_txt#")>
									</cfif>
								</cfif>
								<cfset cell = raterow.createCell(javacast("int",cellseq))/>
								
								<cfset cell.setCellStyle(styleobj)/>
								<cfset region = createObject("java","org.apache.poi.hssf.util.Region").init(javacast("int",rowseq-1),javacast("int",cellseq),javacast("int",rowseq),javacast("int",cellseq))/>
								<cfset sheet.addMergedRegion(region)/>

							</cfif>
							
						</cfif>
						<cfset cellseq = cellseq+1>
					</cfloop>
					<cfset rowseq = rowseq+1>
				</cfloop>
			</cfif>
			<cfset filename = filePath & UserDir & datepart & "CRMResultsList.xls">
			<cfset fileOut = createObject("java","java.io.FileOutputStream").init("#filename#")/>
				
			<cfset wb.write(fileOut)/>
			<cfset fileOut.close()/>
			
			<!--- 戻値設定 --->
			<cfscript>
				executeinformation = StructNew();
				StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
				StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
				StructInsert(executeinformation,	"outputMsg",	"正常"						);
				StructInsert(executeinformation,	"outputPath",	#Application.outputDirectory# & #Replace(UserDir,"\","/","all")# & datepart & "CRMResultsList.xls" );
				StructInsert(executeinformation,	"filename",		"http://#cgi.server_name#/#Application.outputDirectory#" & #Replace(UserDir,"\","/","all")# & datepart & "CRMResultsList.xls" );
				StructInsert(executeinformation,	"newname",		"#Application.resortname##datepart#.xls" );
			</cfscript>
			<cfreturn executeinformation>
			
			<cfsetting enablecfoutputonly="No">
			<cfcatch type="any">
				<cfsetting enablecfoutputonly="No">
				<cfrethrow>
			</cfcatch>
		</cftry>
	</cffunction>

</cfcomponent>
