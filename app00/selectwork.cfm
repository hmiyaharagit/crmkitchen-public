<cftry>
<cfset wrk_id = ArrayNew(1)>

<cfset wk_resort_code = arraynew(1)>
<cfset ArrayAppend( wk_resort_code , 1)>

<cfset date_id = ArrayNew(1)>

<!--- 戻値クエリ作成 --->
<cfset getQrecords = QueryNew("
			 SAMPLES
			,EFFECTIVE_N
			,AVERAGE
			,RANK
			,TOP1
			,TOP2AND3
			,NAGETIVE
			,RESULT0_AVERAGE_TXT
			,QUESTIONNAIRE_ID
			,TMONTH")>

<cfset ArrayAppend( date_id , "2009/01/01")>
<cfset ArrayAppend( date_id , "2009/12/31")>


<cfloop from="1" to="12" index="i">
	<cfset date_id[1] = DateAdd("m", 1, date_id[1])>
	<cfset date_id[2] = CreateDate( datepart("yyyy",date_id[1]) , datepart("m",date_id[1]), DaysInMonth(date_id[1]) )>

	<cfinvoke component = "cfc/cstotal_dataselectwork" method = "getAnswers"
		dcondtion=false
		selecteddays		=#date_id#
		arg_area			=#wrk_id#
		arg_cognition		=#wrk_id#
		arg_gender			=#wrk_id#
		arg_generation		=#wrk_id#
		arg_job				=#wrk_id#
		arg_purpose			=#wrk_id#
		arg_reservation		=#wrk_id#
		arg_roomtype		=#wrk_id#
		arg_spouse			=#wrk_id#
		arg_times			=#wrk_id#
		arg_together		=#wrk_id#
		arg_nights			=#wrk_id#
		arg_roomnumber		=#wrk_id#
		arg_category_id		=1
		arg_resort_code		=#wk_resort_code#
		arg_cross_id=0
		arg_group_id=0
	returnVariable	="retVal">
	<cfinvoke component = "cfc/cstotal" method = "getAnswers"
		dcondtion=false
		selecteddays		=#date_id#
		arg_area			=#wrk_id#
		arg_cognition		=#wrk_id#
		arg_gender			=#wrk_id#
		arg_generation		=#wrk_id#
		arg_job				=#wrk_id#
		arg_purpose			=#wrk_id#
		arg_reservation		=#wrk_id#
		arg_roomtype		=#wrk_id#
		arg_spouse			=#wrk_id#
		arg_times			=#wrk_id#
		arg_together		=#wrk_id#
		arg_nights			=#wrk_id#
		arg_roomnumber		=#wrk_id#
		arg_category_id		=2
		arg_resort_code		=#wk_resort_code#
		arg_cross_id=0
		arg_group_id=0
	returnVariable	="retVal2">

	<cfquery name="worksql" dbtype="query">
		select	 SAMPLES
				,EFFECTIVE_N
				,AVERAGE
				,RANK
				,TOP1
				,TOP2AND3
				,NAGETIVE
				,RESULT0_AVERAGE_TXT
				,QUESTIONNAIRE_ID
		from	retVal
		where	(QUESTIONNAIRE_ID =100000001
			or	 QUESTIONNAIRE_ID =100000003
			or	 QUESTIONNAIRE_ID =600000001
			or	 QUESTIONNAIRE_ID =700000001
				)
	</cfquery>
	<cfloop query="worksql">
		<cfset QueryAddRow(getQrecords, 1)>
		<cfset QuerySetCell(getQrecords, "SAMPLES", #worksql.SAMPLES[worksql.currentrow]#)>
		<cfset QuerySetCell(getQrecords, "EFFECTIVE_N", #worksql.EFFECTIVE_N[worksql.currentrow]#)>
		<cfset QuerySetCell(getQrecords, "AVERAGE", #worksql.AVERAGE[worksql.currentrow]#)>
		<cfset QuerySetCell(getQrecords, "RANK", #worksql.RANK[worksql.currentrow]#)>
		<cfset QuerySetCell(getQrecords, "TOP1", #worksql.TOP1[worksql.currentrow]#)>
		<cfset QuerySetCell(getQrecords, "TOP2AND3", #worksql.TOP2AND3[worksql.currentrow]#)>
		<cfset QuerySetCell(getQrecords, "NAGETIVE", #worksql.NAGETIVE[worksql.currentrow]#)>
		<cfset QuerySetCell(getQrecords, "RESULT0_AVERAGE_TXT", #worksql.RESULT0_AVERAGE_TXT[worksql.currentrow]#)>
		<cfset QuerySetCell(getQrecords, "QUESTIONNAIRE_ID", #worksql.QUESTIONNAIRE_ID[worksql.currentrow]#)>
		<cfset QuerySetCell(getQrecords, "TMONTH", #dateformat(date_id[1],'yyyy/m')#)>
	</cfloop>
	<cfquery name="worksql" dbtype="query">
		select	 SAMPLES
				,ORDER_ID
				,EFFECTIVE_N
				,AVERAGE
				,RANK
				,TOP1
				,TOP2AND3
				,NAGETIVE
				,RESULT0_AVERAGE_TXT
				,QUESTIONNAIRE_ID
		from	retVal2
		where	(QUESTIONNAIRE_ID =100000001
			or	 QUESTIONNAIRE_ID =100000003
			or	 QUESTIONNAIRE_ID =600000001
			or	 QUESTIONNAIRE_ID =800200005
			or	 QUESTIONNAIRE_ID =900100005
			or	 QUESTIONNAIRE_ID =700000001
				)
		and		(ORDER_ID = 611011905
			or	 ORDER_ID = 701002005
				)
	</cfquery>


	<cfloop query="worksql">
		<cfset QueryAddRow(getQrecords, 1)>
		<cfset QuerySetCell(getQrecords, "SAMPLES", #worksql.SAMPLES[worksql.currentrow]#)>
		<cfset QuerySetCell(getQrecords, "EFFECTIVE_N", #worksql.EFFECTIVE_N[worksql.currentrow]#)>
		<cfset QuerySetCell(getQrecords, "AVERAGE", #worksql.AVERAGE[worksql.currentrow]#)>
		<cfset QuerySetCell(getQrecords, "RANK", #worksql.RANK[worksql.currentrow]#)>
		<cfset QuerySetCell(getQrecords, "TOP1", #worksql.TOP1[worksql.currentrow]#)>
		<cfset QuerySetCell(getQrecords, "TOP2AND3", #worksql.TOP2AND3[worksql.currentrow]#)>
		<cfset QuerySetCell(getQrecords, "NAGETIVE", #worksql.NAGETIVE[worksql.currentrow]#)>
		<cfset QuerySetCell(getQrecords, "RESULT0_AVERAGE_TXT", #worksql.RESULT0_AVERAGE_TXT[worksql.currentrow]#)>
		<cfset QuerySetCell(getQrecords, "QUESTIONNAIRE_ID", #worksql.QUESTIONNAIRE_ID[worksql.currentrow]#)>
		<cfset QuerySetCell(getQrecords, "TMONTH", #dateformat(date_id[1],'yyyy/m')#)>
	</cfloop>

</cfloop>
	<cftable query = "getQrecords" border="1" colheaders HTMLTable>
		<cfcol header="AVERAGE" text  = "#AVERAGE#">
		<cfcol header="EFFECTIVE_N" text  = "#EFFECTIVE_N#">
		<cfcol header="NAGETIVE" text  = "#NAGETIVE#">
		<cfcol header="QUESTIONNAIRE_ID" text  = "#QUESTIONNAIRE_ID#">
		<cfcol header="RANK" text  = "#RANK#">
		<cfcol header="RESULT0_AVERAGE_TXT" text  = "#RESULT0_AVERAGE_TXT#">
		<cfcol header="SAMPLES" text  = "#SAMPLES#">
		<cfcol header="TMONTH" text  = "#TMONTH#">
		<cfcol header="TOP1" text  = "#TOP1#">
		<cfcol header="TOP2AND3" text  = "#TOP2AND3#">
	</cftable>


<cfcatch>
	<cfdump var=#cfcatch#>
</cfcatch>
</cftry>
<cfabort>
	
