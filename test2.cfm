<!--- Try to connect to the Excel application object --->
<CFTRY>
    <!--- If it exists, connect to it --->
    <CFOBJECT
        ACTION="CONNECT"
        CLASS="Excel.Application"
        NAME="objExcel"
        TYPE="COM">
  <CFCATCH>
    <!--- The object doesn't exist, so create it --->
    <CFOBJECT
        ACTION="CREATE"
        CLASS="Excel.Application"
        NAME="objExcel"
        TYPE="COM">
  </CFCATCH>
</CFTRY>
<CFSCRIPT>
    // Open Excel in the background
    objExcel.Visible = false;
     
    // Disable client alerts such as: 'Save this workbook?'
    objExcel.DisplayAlerts = false;    
     
    // Define the workbooks object 
    objWorkBook = objExcel.Workbooks;

    // Add a new workbook
    objOpenedBook = objWorkBook.Add();
    
    
    
    
      
    // Get the WorkSheets' collection 
    objWorkSheets = objExcel.WorkSheets;
    
    // Add a new worksheet (this will contain our data)
    objWorkSheet = objWorkSheets;
    


    // Add a new worksheet (this will contain our data)
    objWorkSheet =  objWorkSheets.Add();
/*

    objWorkSheet =  objWorkBook.Sheets.Select(Val(1));
*/
	
ttt = objOpenedBook.ActiveSheet;
ttd = (ttt.Index)+1;

	test =  objExcel.Worksheets;
    
    //Set header - ERROR LOG
    objRange = objExcel.Range("A1:A1");
    objF = objRange.Font;
    objF.Size = 16;
    objF.Bold = True;
    objRange.HorizontalAlignment = 1;
    objRange.value = "ERROR LOG - PROJECT";
    
    //Create object Interior for solid field some color
    objRange = objExcel.Range("A1:K1");
    objI = objRange.Interior;
    objI.ColorIndex = 6;
    
    //Set variables from A2 to K2
    objRange = objExcel.Range("A2:Q2");
    objIn_V = objRange.Interior;
    objIn_V.ColorIndex = 15;
    objV = objRange.Font;
    objV.Size = 08;
    objRange.HorizontalAlignment = 3;
    objRange.VerticalAlignment = 1;
    objLine = objRange.Borders;
    objLine.LineStyle = 1;
    objRange.WrapText = True;
    objRange.ShrinkToFit = True;
    
    //Set variables for error log records
    objRange = objExcel.Range("A3:Q6");
    objD = objRange.Font;
    objD.Size = 08;
    objRange.HorizontalAlignment = 3;
    objRange.VerticalAlignment = 1;
    objLine = objRange.Borders;
    objLine.LineStyle = 1;
    objRange.WrapText = True;
    objRange.ShrinkToFit = True;
    
    //Create field N
    objRange = objExcel.Range("A2:A2");
    objRange.ColumnWidth = 4.00;
    objRange.Value = "N";
    
    //Create field Date of information
    objRange = objExcel.Range("B2:B2");
    objRange.ColumnWidth = 10.57;
    objRange.Value = "Date of information";
    
    //Create field Error type Options available:- Requirements error- Implementation error- Issue
    objRange = objExcel.Range("C2:C2");
    objRange.ColumnWidth = 12.14;
    objRange.Value = "Error type    Options available:      -Requirements error -Implementation error-Issue";

    //Create field Product/ Subsystem/ Component name
    objRange = objExcel.Range("D2:D2");
    objRange.ColumnWidth = 10.57;
    objRange.Value = "Product/ Subsystem/ Component name";

    //Create field Product/ Subsystem/ Component version
    objRange = objExcel.Range("E2:E2");
    objRange.ColumnWidth = 09.29;
    objRange.Value = "Product/ Subsystem/ Component version";

    //Create field Sender (Customer/ Manager/ Tester/ etc.)
    objRange = objExcel.Range("F2:F2");
    objRange.ColumnWidth = 10.71;
    objRange.Value = "Sender (Customer/ Manager/ Tester/ etc.)";

    //Create field Error description
    objRange = objExcel.Range("G2:G2");
    objRange.ColumnWidth = 37.57;
    objRange.Value = "ERROR DESCRIPTION";
    
    //Create field Error description Severity level:- critical - major - average - minor 
    objRange = objExcel.Range("H2:H2");
    objRange.ColumnWidth = 7.43;
    objRange.Value = "Severity level:
        - critical
        - major 
        - average
        - minor ";

    //Create field Priority:- High- Normal- Low
    objRange = objExcel.Range("I2:I2");
    objRange.ColumnWidth = 6.14;
    objRange.Value = "Priority:
        - High
        - Normal
        - Low ";
    
    //Create field Error reason
    objRange = objExcel.Range("J2:J2");
    objRange.ColumnWidth = 12.00;
    objRange.Value = "Error reason";
    
    //Create field Programmer
    objRange = objExcel.Range("K2:K2");
    objRange.ColumnWidth = 11.86;
    objRange.Value = "Programmer";
    
    //Create field Fixed deadline
    objRange = objExcel.Range("L2:L2");
    objRange.ColumnWidth = 11.14;
    objRange.Value = "Fixed deadline";
    
    //Create field Error status:- Attention!- Done- Cancelled- To be retested
    objRange = objExcel.Range("M2:M2");
    objRange.ColumnWidth = 12.14;
    objRange.Value = "Error status:
        - Attention!
        - Done
        - Cancelled
        - To be retested";		
	
	//Create field Fixed date
	objRange = objExcel.Range("N2:N2");
	objRange.ColumnWidth = 12.29;
	objRange.Value = "Fixed date";		
	
	//Create field Fixed by (Programmer)
	objRange = objExcel.Range("O2:O2");
	objRange.ColumnWidth = 12.43;
	objRange.Value = "Fixed by (Programmer)";		
    
    //Create field Tested by (Tester)
    objRange = objExcel.Range("P2:P2");
    objRange.ColumnWidth = 15.00;
    objRange.Value = "Tested by (Tester)";
    
    //Create field Remark
    objRange = objExcel.Range("Q2:Q2");
    objRange.ColumnWidth = 15.00;
    objRange.Value = "Remark";
    
    // SaveAs() does not work with workbooks, only with worksheets
    objWorkSheet.SaveAs("c:\error_log.xls",Val(1));
    
ddewa = objExcel.ActiveSheet();
//sweq = objExcel.WorkSheets(2);
//sweqsel = sweq.Select;	
ddewassa = ddewa.Name;

    // Close the document
    objWorkBook.Close();

    // Quit Excel
    objExcel.Quit();

    // Release the object
    //objExcel = "Nothing";

    
</CFSCRIPT>
	
<cfdump var=#objWorkBook#>