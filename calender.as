﻿import mx.transitions.easing.*;
import mx.transitions.Tween;
import mx.controls.NumericStepper;

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	Name			: DateChoose
	Description		: カレンダー表示による入力補助日付設定
	Usage			: var INSTANCENAME:DateChoose = new DateChoose( path , txt, disableDate );
	Attributes		: path:MovieClip	（AttachMovieClipTarget）
					: txt:MovieClip		（TextInsertTarget）
					: disableDate:String（disableDate）
	Method			: setCalender( setDate:Date )
					: initialized()
					: daysInMonth()
					: dateFormat()
	Note			: none
	History			: 1) Coded by	nf)h.miyahara	2004/07/09
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
class calender extends MovieClip{
	var today_date:Date;		//今日
	var fitst_date:Date;		//今日の存在する月の初日
	var datearray:Array;		//選択日配列
	var gdp:MovieClip;			//パス
	var parents:MovieClip;		//パス
	
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Constructor
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function calender(){
		gdp = this
		today_date = new Date()
		//gdp._y=100;
		//gdp._x=-800;
		initializedData();
	}
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: initializedData
		Description		: 初期設定
		Usage			: initializedData();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function initializedData(){
		gdp.calender_enable.onRelease = function(){}
		gdp.calender_enable.enabled = false
		gdp.calender_enable._visible = false
		var owner = this
		/*
		var _xtween:Object = new Tween(gdp, "_x", Back.easeOut, gdp._x, 0, 1, true);
		_xtween.onMotionFinished = function() {
			owner.setCalender(owner.today_date)
		};
		trace("today_date"+today_date)
		*/
		setCalender(today_date)
	
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setCalender
		Description		: 初期設定
		Usage			: setCalender(setDate);
		Attributes		: setDate	(カレンダー表示開始日)
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function setCalender( sd ){
		for(var i=0; i<42; i++){
			gdp[ "day" + i ].daytxt.text = ""
			gdp[ "day" + i ].dayscreen.enabled = false
			gdp[ "day" + i ]._visible = false
			gdp[ "day" + i ].dayscreen.gotoAndStop(1)
		}
		gdp.yeartxt.text = ""

		var owner = this
		var setDate = sd
		fitst_date = new Date(setDate.getFullYear(),setDate.getMonth(),1)
		//DaysInMonth
		var daysNum = daysInMonth( fitst_date.getMonth(), fitst_date.getFullYear() );  
		var count = 1;
		
		var tmp_month = dateFormat( fitst_date.getFullYear(), fitst_date.getMonth()+1, 1 );
		gdp.yeartxt.text = fitst_date.getFullYear() + "年" + (fitst_date.getMonth()+1) + "月"

//		trace(fitst_date.getMonth()+1)
//		trace(gdp.ns_month._name)

		//	--日付設定

		gdp.year_txt.fontSize = gdp.month_txt.fontSize = 42;
		gdp.year_txt.color = gdp.month_txt.color = 0x544C2A;
		gdp.year_txt.fontStyle = gdp.month_txt.fontStyle = "italic";
		gdp.year_txt.fontWeight = gdp.month_txt.fontWeight = "bold";
		gdp.year_txt.borderStyle = gdp.month_txt.borderStyle = "none";

		gdp.year_txt.text = fitst_date.getFullYear() ;
		gdp.month_txt.text = (fitst_date.getMonth()+1);

		//	--指定月フリー入力
		gdp.month_txt.onChanged = function(){
			var new_month = this.text-1
			if( new_month < 0){this.text = 1}
			if( new_month > 11){this.text = 12}
			if( new_month >= 0 && new_month < 12){
				owner.initialized();
				var temp_date = new Date(owner.gdp.year_txt.text , new_month , 1 )
				owner.setCalender(temp_date);
			}
		}

		//	--指定月未入力対応
		gdp.month_txt.onKillFocus = function(){
			if(this.text.length==0){
				owner.initialized();
				var resetdate = new Date();
				owner.parents.slctddate = new Array();
				owner.parents.myCond.slctddatedel();
				var temp_date = new Date(resetdate.getFullYear() , resetdate.getMonth() , 1 )
				owner.setCalender(temp_date);
			}
		}

		//	--リセットボタン
		gdp.resetbtn.onRelease = function(){
			owner.initialized();
			var resetdate = new Date();
			owner.parents.slctddate = new Array();
			owner.parents.myCond.slctddatedel();
			var temp_date = new Date(resetdate.getFullYear() , resetdate.getMonth() , 1 )
			owner.setCalender(temp_date);
		}

		gdp.yearforwardBtn.onRelease = function(){
			owner.initialized();
			var temp_date = new Date(setDate.getFullYear()+1 , setDate.getMonth() , 1 )
			owner.setCalender(temp_date);
		}

		gdp.yearbackBtn.onRelease = function(){
			owner.initialized();
			var temp_date = new Date(setDate.getFullYear()-1 , setDate.getMonth() , 1 )
			owner.setCalender(temp_date);
		}

		gdp.forwardBtn.onRelease = function(){
			owner.initialized();
			var temp_date = new Date(setDate.getFullYear() , setDate.getMonth()+1 , 1 )
			owner.setCalender(temp_date);
		}

		gdp.backBtn.onRelease = function(){
			owner.initialized();
			var temp_date = new Date(setDate.getFullYear() , setDate.getMonth()-1 , 1 )
			owner.setCalender(temp_date);
		}

		//日付設定
		var wk_tday = today_date.getFullYear() + "" + (today_date.getMonth()+1) + "" + today_date.getDate()
		var wk_add = 0
		for(var i=0; i<daysNum; i++){
			if(fitst_date.getDay()-1+i + wk_add<0){
				wk_add = 7
			}
			var wk_start = fitst_date.getDay()-1+i + wk_add

			var wk_day = new Date(fitst_date.getFullYear(),fitst_date.getMonth(),(i+1))
			gdp[ "day" + wk_start ].day = fitst_date.getFullYear() + "" + (fitst_date.getMonth()+1) + "" + (i+1)
			gdp[ "day" + wk_start ].thisdate=wk_day
			gdp[ "day" + wk_start ].dateid= new Date(fitst_date.getFullYear(),(fitst_date.getMonth()),(i+1))
			gdp[ "day" + wk_start ].date=wk_day.getDay()
			gdp[ "day" + wk_start ].daytxt.text = i+1
			gdp[ "day" + wk_start ].dayscreen.enabled = true
			gdp[ "day" + wk_start ]._visible = true
			gdp[ "day" + wk_start ].dayscreen.dtype = 1
			//	--土曜日
			if(gdp[ "day" + wk_start ].date == 6){
				gdp[ "day" + wk_start ].dayscreen.dtype = "weekend"
			}
			//	--日曜日
			if(gdp[ "day" + wk_start ].date == 0){
				gdp[ "day" + wk_start ].dayscreen.dtype = "holyday"
			}
			//	--本日
			if(gdp[ "day" + wk_start ].day == wk_tday){
				gdp[ "day" + wk_start ].dayscreen.dtype = "today"
			}

			//	--選択日
			for(var c=0;c<parents.slctddate.length;c++){
				if(parents.slctddate[c].toString() == gdp[ "day" + wk_start ].thisdate.toString()){
					gdp[ "day" + wk_start ].dayscreen.dtype = "selectedday"
				}
			}
			//	--期間選択の場合
			
			var dsel = parents.GroundPath.dselection.selected
			if(parents.GroundPath == undefined){
				var dsel = parents.gpath.dselection.selected
			}
			if(!dsel && parents.slctddate.length > 0 &&  parents.slctddate[0] > gdp[ "day" + wk_start ].dateid){
				gdp[ "day" + wk_start ].dayscreen.enabled = false
				gdp[ "day" + wk_start ].daytxt.text = ""
			} else if(!dsel && parents.slctddate.length > 1 && parents.slctddate[1] > gdp[ "day" + wk_start ].dateid && parents.slctddate[0] < gdp[ "day" + wk_start ].dateid){
				gdp[ "day" + wk_start ].dayscreen.dtype = "selectedday"
			}

			gdp[ "day" + wk_start ].dayscreen.gotoAndStop(gdp[ "day" + wk_start ].dayscreen.dtype)

			gdp[ "day" + wk_start ].dayscreen.onRelease = function(){
				var chk = owner.parents.myCond.slctddateadd(this._parent.thisdate)
				var temp_date = new Date(this._parent.thisdate.getFullYear() , this._parent.thisdate.getMonth() , 1 )
				owner.setCalender(temp_date);
			}
		}

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: daysInMonth
		Description		: 該当月の日数取得
		Usage			: daysInMonth(month, year);
		Attributes		: month
						: year
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function daysInMonth(month, year) {
		return (new Date(year, month+1, 0).getDate());
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: dateFormat
		Description		: YYYY/MM/DD形式への日付書式フォーマット
		Usage			: dateFormat(year , month , date);
		Attributes		: month
						: year
						: date
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function dateFormat(year , month , date) {
		if( month < 10 ){
			month = "0" + month
		}
		if( date < 10 ){
			date = "0" + date
		}
		var formatedDate = year + "/" + month + "/" + date
		return formatedDate;
	}

}
