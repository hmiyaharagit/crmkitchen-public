﻿/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	Name			: conditionsetting
	Description		: CRM用検索画面
	Usage			: var XXXXXX:conditionsetting = new conditionsetting( path );
	Attributes		: path:MovieClip			（基準パス）
	Extends			: CommonFunction(Class)
	Method			: initializedData		初期設定
					: getCRMGuestList_Result	検索実行正常終了時イベント
					: getCRMGuestList_Fault	検索実行不正終了時イベント
	Note			: none
	History			: 1) Coded by	nf)h.miyahara	2004/07/09
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
import mx.remoting.Service;
import mx.services.Log;
import mx.rpc.RelayResponder;
import mx.remoting.PendingCall;
import mx.remoting.debug.NetDebug;
import mx.remoting.debug.NetDebugConfig;
import mx.remoting.RecordSet;
import mx.rpc.ResultEvent;
import mx.rpc.FaultEvent;
import mx.controls.ComboBox
import mx.data.binding.*;
import mx.data.components.DataSet
import mx.containers.ScrollPane
import mx.transitions.easing.*;
import mx.transitions.Tween;
import flash.filters.DropShadowFilter;
//import mx.controls.Alert;

class searchcondition {

	var gpath:MovieClip			//基準ムービークリップパス
	var apath:MovieClip			//参照先ムービーパス
	var gdepth:Number			//Depth
	var sobject:Object			//CFCsobject
	var outputPath:String		//CSVOutputPath
	var sresult:DataSet			//検索結果退避用オブジェクト
	var psource:MovieClip		//ScrollPane内MovieClip
	var condtionitems:Array		//
	var overflowlength:Number
	var parents:Object
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: conditionsetting
		Description		: Constructor
		Usage			: conditionsetting( myPath:MovieClip );
		Attributes		: myGrid:DataGrid	（結果格納用データグリッドコンポーネント名）
						: myPath:MovieClip	（基準ＭＣパス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function searchcondition() {
		gdepth = gpath.getNextHighestDepth();
		condtionitems = new Array();
		overflowlength = 94
		var myDebug = mx.remoting.debug.NetDebug.initialize();
		sobject = new Service( "http://" + _global.hostAddress + "/flashservices/gateway", null , _global.servicePath + ".app00.cfc.conditionsetting" , null , null ); 
		
		//NetDebbuger設定
		var config:NetDebugConfig = sobject.connection.getDebugConfig();
		config.app_server.trace = true;
		config.client.trace = true;
		
	}

}