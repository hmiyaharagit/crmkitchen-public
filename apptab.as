﻿import mx.transitions.easing.*;
import mx.transitions.Tween;
/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	Name			: apptab
	Description		: 
	Usage			: 
	Attributes		: 
	Method			: 
	Note			: 
	History			: 1) Coded by
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
class apptab extends MovieClip{
	var gpath:MovieClip			//基準ムービークリップパス
	var gdepth:Number			//Depth
	var sobject:Object			//CFCService
	var applicode:String		//基本アプリ（00）：二次（01）：三次（02）
	var menucategory:String		//ＣＳ（cs）：メール（ml）：顧客（cm）
	var menuid:String
	var hidepos:Number
	var viewpos:Number
	var initflg:Boolean

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Constructor
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function apptab(){
		initflg = true
		gpath = this
		hidepos = -600;
		viewpos = 27.1;
		initializedData();
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: menucontrol
		Description		: Flaファイルオブジェクト内からコール
		Usage			: menucontrol();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function menucontrol(){
		//trace("applicode:" + applicode + " menucategory:" + menucategory + " menuid:" + menuid)
		var owner = this
		gpath.menu_cs.viewflg = false
		gpath.menu_ml.viewflg = false
		gpath.menu_cm.viewflg = false

		gpath.menu_cs._y = gpath.tub_cs._y = hidepos
		gpath.menu_ml._y = gpath.tub_ml._y = hidepos
		gpath.menu_cm._y = gpath.tub_cm._y = hidepos

		gpath.menu_cs._x = gpath.csmenuheader._x - 10
		gpath.menu_ml._x = gpath.mlmenuheader._x - 10
		gpath.menu_cm._x = gpath.cmmenuheader._x - 10


		gpath.csmenuheader.onRelease = function(){
			this._parent.menu_cs.viewflg = !this._parent.menu_cs.viewflg
			this._parent.menu_ml.viewflg = false
			this._parent.menu_cm.viewflg = false
			owner.menucategory = "cs"
			owner.buttoncontrol(this)
		}

		var wk_mlenb = false
		var wk_cmenb = false
		switch (applicode) { 
			case "00" :
				//	--基本
				if(_global.UserInfo.login_dv == 'a'){
					wk_mlenb = true
					wk_cmenb = true
				}
				//	--基本（メール）
				if(_global.UserInfo.login_dv == 'b'){
				//	wk_mlenb = true
					wk_cmenb = true
				}
				break; 
			default : 
				break; 
		 } 
		if(_global.UserInfo.login_dv == 's'){
			wk_mlenb = true
			wk_cmenb = true
		}
		gpath.mlmenuheader.enabled = wk_mlenb
		gpath.cmmenuheader.enabled = wk_cmenb

		gpath.mlmenuheader.onRelease = function(){
			this._parent.menu_ml.viewflg = !this._parent.menu_ml.viewflg
			this._parent.menu_cs.viewflg = false
			this._parent.menu_cm.viewflg = false
			owner.menucategory = "ml"
			owner.buttoncontrol(this)
			owner.gpath._parent.dconditionbox._x = -1000
			owner.gpath._parent.conditionbox._x = -1000
		}

		gpath.cmmenuheader.onRelease = function(){
			this._parent.menu_cm.viewflg = !this._parent.menu_cm.viewflg
			this._parent.menu_cs.viewflg = false
			this._parent.menu_ml.viewflg = false
			owner.menucategory = "cm"
			owner.buttoncontrol(this)
			owner.gpath._parent.dconditionbox._x = -1000
			owner.gpath._parent.conditionbox._x = -1000
		}

		//初回トップページからのみ
		if(initflg){
			tubcontrol(applicode,menucategory,menuid)
		}

		//	--表示ファイル設定
		switch (applicode) { 
			case "00" :
				gpath.menu_cs["01btn"].loadfile = "app00/oa.swf";
				gpath.menu_cs["02btn"].loadfile = "app00/cs.swf";
	//			gpath.menu_cs["03btn"].loadfile = "app00/cs.swf";
				gpath.menu_cs["03btn"].enabled = false;
				gpath.menu_cs["03btn"]._visible = false;
				
				gpath.menu_cs["04btn"].loadfile = "app00/mkt.swf";
				gpath.menu_cs["05btn"].loadfile = "app00/rpt.swf";
				gpath.menu_cs["06btn"].loadfile = "";
				gpath.menu_cs["06btn"].webcs 	= "app00/webcs/index.cfm";
				gpath.menu_cs["07btn"].loadfile	= "app00/paper.swf";
		//			gpath.menu_cs["08btn"].loadfile	= "app00/return.swf";
				gpath.menu_ml["01btn"].loadfile = "app00/mailPackage.swf";
				gpath.menu_ml["02btn"].loadfile = "app00/mailTemplate.swf";
				gpath.menu_cm["01btn"].loadfile = "app00/defaultSearch.swf";
				break; 
			default : 
				break; 
		 } 
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: cscontrol
		Description		: Flaファイルオブジェクト内からコール
		Usage			: cscontrol();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function buttoncontrol(probj){
		var menuarray = new Array("cs","ml","cm")
		for(var i=0; i<menuarray.length; i++){
			var tgtmenuobj = gpath[ "menu_" + menuarray[i] ]
			if(tgtmenuobj.viewflg){
				tgtmenuobj._y = probj._y + probj._height + 10
				var _atween:Object = new Tween(tgtmenuobj, "_alpha", Back.easeOut, 0, 100, 1.25, true);
			}else{
				tgtmenuobj._y = hidepos
			}
			setbtnevent(tgtmenuobj)
		}
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setbtnevent
		Description		: 
		Usage			: 
		Attributes		: 
		Note			: 
		History			: 1) Coded by
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function setbtnevent(tgt){
		var owner = this
		var sourcemc = tgt

		var checkmc = gpath._parent
		for (var wkName in checkmc) {
			var objnm = wkName.substr(wkName.length-6,wkName.length)
			if(objnm=="screen"){
				checkmc[wkName]._visible = checkmc[wkName].enabled = false
			}
		}

		for (var myName in sourcemc) {
			var btnnm = sourcemc[myName]._name.substr(2,3)
			if (btnnm == "btn") {
				sourcemc[myName].onRelease = function(){
					var app = owner.applicode
					var menu = owner.menucategory
					var cd = this._name.substr(0,2)
					var loadfile = owner.getloadfiles(app,menu,cd)
					//var toppage:LoadImage = new LoadImage( loadfile , owner.gpath._parent.screen );
					var toppage:LoadImage = new LoadImage( loadfile , owner.gpath._parent.screen.screenobj );
					owner.tubcontrol(app,menu,cd)
					this._parent._y = owner.hidepos
				}
			}
		}
	}

	function getloadfiles(app,menu,cd){
		return gpath["menu_" + menu][cd + "btn"].loadfile
	}

	function getwebcsurl(app,menu,cd){
		//return gpath["area_" + app]["menu_" + menu][cd + "btn"].webcs
		return gpath["menu_" + menu][cd + "btn"].webcs
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: tubcontrol
		Description		: 
		Usage			: 
		Attributes		: 
		Note			: 
		History			: 1) Coded by
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	private function tubcontrol(app,menu,cd){
		var owner = this
		applicode = app
		menucategory = menu
		menuid = cd
		gpath.tab_cs._y = gpath.tab_ml._y = gpath.tab_cm._y = hidepos
		var sourcemc = gpath["tab_" + menucategory]
		sourcemc._y = viewpos
//trace("app : " + app + " menu : " + menu + " cd : " + cd)
		var checkmc = gpath._parent
		for (var wkName in checkmc) {
			var objnm = wkName.substr(wkName.length-6,wkName.length)
			if(objnm=="screen"){
				checkmc[wkName]._visible = checkmc[wkName].enabled = false
			}
		}

		for (var myName in sourcemc) {
			var btnnm = sourcemc[myName]._name.substr(2,3)
			var ctgnm = sourcemc._name.substr(4,2)
			var wk_menuid = sourcemc[myName]._name.substr(0,2)
			if (btnnm == "tab") {
				sourcemc[myName].onRelease = function(){
					owner.tubcontrol(owner.applicode,ctgnm,this._name.substr(0,2))
				}
				if(wk_menuid == menuid){
					sourcemc[myName]._visible = sourcemc[myName].enabled = false
				} else {
					sourcemc[myName]._visible = sourcemc[myName].enabled = true
				}
			}
		}
		//対象ファイル読込
		var loadfile = getloadfiles(app,menu,cd)
		if(loadfile.length==0){
			var webcsurl = getwebcsurl(app,menu,cd)
			gpath.getURL(webcsurl,"_blank" )
		}else{
			gpath.applicode = applicode
			gpath.menucategory = menucategory
			gpath.menuid = menuid
			gpath._parent[ menucategory + menuid + "screen" ]._visible = gpath._parent[ menucategory + menuid + "screen" ].enabled = true

			//	--オブジェクト位置設定
			var wk_channel = menucategory + menuid
			//	--カレント変更
			_global.currentscreen = gpath._parent[ menucategory + menuid + "screen" ].screenobj.contentobj

			//	--検索ボタン設定
			_global.currentscreen.searchbtnevent();

			//	--リセットボタン設定
			_global.currentscreen.resetbtnevent();

			if(wk_channel=="cs01" || wk_channel=="cs02" || wk_channel=="cs03" || wk_channel=="cs04" || wk_channel=="cs05" || wk_channel=="cs08"){
				//	--集計条件設定
				_global.currentscreen.myCond.setselectedlabel();

				//	--日付設定
				var wk_slctddate = gpath._parent.dconditionbox.parents.slctddate
				var wk_dselection = gpath._parent.dconditionbox.parents.gpath.dselection.selected
				gpath._parent.dconditionbox.parents = _global.currentscreen
				_global.currentscreen.slctddate = wk_slctddate
				_global.currentscreen.gpath.dselection.selected = wk_dselection
				_global.currentscreen.myCond.dcondtionlabel(_global.currentscreen.gpath.dselection.selected);

				if((wk_channel=="cs01" || wk_channel=="cs04" || wk_channel=="cs05") && _global.currentscreen.gpath.qselection._x > 0){
					gpath._parent.dconditionbox._x = -1000
				}
			} else {
				gpath._parent.dconditionbox._x = -1000
				gpath._parent.conditionbox._x = -1000
			}
			gpath._parent.conditionbox._x = -1000
			//var toppage:LoadImage = new LoadImage( loadfile , gpath._parent.screen.screenobj );
			gpath._x = 0
			gpath.gotoAndStop("scene" + app)
		}
		initflg = false
	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: initializedData
		Description		: 初期設定
		Usage			: initializedData();
		Attributes		: none
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function initializedData(){
		var owner = this
		//	--宿泊
		gpath.tab00.onRelease = function(){
			owner.applicode = "00"
			this._parent.gotoAndStop("scene00")
		}
	}

}
