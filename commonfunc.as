﻿/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	Name			: commonfunc
	Description		: CRM用検索画面
	Usage			: var XXXXXX:commonfunc = new commonfunc( path );
	Attributes		: path:MovieClip			（基準パス）
	Extends			: CommonFunction(Class)
	Method			: initializedData		初期設定
					: getCRMGuestList_Result	検索実行正常終了時イベント
					: getCRMGuestList_Fault	検索実行不正終了時イベント
	Note			: none
	History			: 1) Coded by	nf)h.miyahara	2004/07/09
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
import mx.remoting.Service;
import mx.services.Log;
import mx.rpc.RelayResponder;
import mx.remoting.PendingCall;
import mx.remoting.debug.NetDebug;
import mx.remoting.debug.NetDebugConfig;
import mx.remoting.RecordSet;
import mx.rpc.ResultEvent;
import mx.rpc.FaultEvent;
import mx.controls.ComboBox
import mx.data.binding.*;
import mx.data.components.DataSet
import mx.containers.ScrollPane
import mx.transitions.easing.*;
import mx.transitions.Tween;
import mx.controls.Alert;
import flash.filters.DropShadowFilter;

//class sanretsu.commonfunc extends sanretsu.CommonFunction {
class commonfunc extends MovieClip {

	var gpath:MovieClip			//基準ムービークリップパス
	var gdepth:Number			//Depth
	var sobject:Object			//CFCsobject
	var outputPath:String		//CSVOutputPath
	var SearchResult:DataSet	//検索結果退避用オブジェクト
	var PaneSource:MovieClip	//ScrollPane内MovieClip
	var DataSourceArr:Array		//データソース事前指定用配列
	var condparamarr:Array
	var condnamearr:Array
	var overflowlength:Number
	var styleObj:Object
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: commonfunc
		Description		: Constructor
		Usage			: commonfunc( myPath:MovieClip );
		Attributes		: myGrid:DataGrid	（結果格納用データグリッドコンポーネント名）
						: myPath:MovieClip	（基準ＭＣパス）
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function commonfunc() {
	}
	function loadingdelete(mc){
		for (var coverobj in mc) {
			if (typeof (mc[coverobj]) == "movieclip" && mc[coverobj].loadingmc) {

				var wk_mc = mc[coverobj]
				var alpha_tween:Object = new Tween(wk_mc, "_alpha", Strong.easeOut, wk_mc._alpha, 0, 2, true);
				alpha_tween.delmc = wk_mc
				alpha_tween.onMotionFinished = function() {
				};
			}
		}
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: SearchWait
		Description		: 検索待ち画面制御
		Usage			: SearchWait(myPath:MovieClip);
		Attributes		: myPath	Attach先MovieClip
						: flg		待ちor解除
		Note			: 共通関数
		History			: 1) Coded by	nf)h.miyahara	2004/10/18
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function SearchWait( myPath:MovieClip, flg:Boolean ){
		if(flg){
			myPath.createEmptyMovieClip("waitScreen", gdepth,{_x:0,_y:Stage.height/2});
			gdepth++;

			myPath.waitScreen.beginFill(0x999999);
			myPath.waitScreen.moveTo(0,0);
			myPath.waitScreen.lineTo(Stage.width, 0);
			myPath.waitScreen.lineTo(Stage.width, Stage.height);
			myPath.waitScreen.lineTo(0, Stage.height);
			myPath.waitScreen.lineTo(0, 0);
			myPath.waitScreen.endFill();
			myPath.waitScreen._alpha = 40;

			myPath.waitScreen.enabled = false
			myPath.waitScreen.onRollOver = function(){}
			myPath.waitScreen.endFill();
			myPath.attachMovie("waitingMessage","waitingMessage",	gdepth, {_x:Stage.width/2,_y:Stage.height/2});
			gdepth++;
			myPath.waitingMessage._x = myPath.waitingMessage._x-myPath.waitingMessage._width/2;
			myPath.waitingMessage._y = myPath.waitingMessage._y-myPath.waitingMessage._height;

			myPath.changeBtn.attachMovie("disabledButton","dummy",gdepth)
			gdepth++;
			myPath.changeBtn.dummy._alpha = 25;
			myPath.changeBtn.enabled = false;

		} else {
			myPath.changeBtn.enabled = true;
			myPath.changeBtn.dummy.removeMovieClip();
			myPath.waitScreen.removeMovieClip();
			myPath.waitingMessage.removeMovieClip();
		}

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: ShowConfirm
		Description		: 画面制御用ＭＣ作成＆ウインドウ表示
		Usage			: ShowConfirm();
		Attributes		: myPath	Attach先MovieClip
						: myStr		ウインドウ内表示メッセージ
						: myOwnr	関数呼出元インスタンス
						: myFunc	確認時実行ファンクション
		Note			: 共通関数
		History			: 1) Coded by	nf)h.miyahara	2004/09/27
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function ShowConfirm( myPath:MovieClip, myStr:String, myOwnr:Object, myFunc:Function ){

		//ボタン無効用カバーＭＣ
		myPath.createEmptyMovieClip("Alldisable_mc", gdepth);
		gdepth++;
		myPath.Alldisable_mc.beginFill(0x999999);
		myPath.Alldisable_mc.moveTo(0,0);
		myPath.Alldisable_mc.lineTo(Stage.width, 0);
		myPath.Alldisable_mc.lineTo(Stage.width, Stage.height);
		myPath.Alldisable_mc.lineTo(0, Stage.height);
		myPath.Alldisable_mc.lineTo(0, 0);
		myPath.Alldisable_mc.endFill();
		myPath.Alldisable_mc._alpha = 40;
		myPath.Alldisable_mc.enabled = false
		myPath.Alldisable_mc.onRollOver = function(){}
		myPath.Alldisable_mc.onEnterFrame = function(){
			if( this._parent.confirmwindow == undefined ){
				this.removeMovieClip();
			}
		}
		
		//Confirm表示
		myPath.attachMovie("sqconfirm","confirmwindow",gdepth,{_x:Stage.width/2,_y:Stage.height/2,_alpha:95});
		gdepth++
		myPath.confirmwindow._x = myPath.confirmwindow._x-myPath.confirmwindow._width/2;
		myPath.confirmwindow._y = myPath.confirmwindow._y-myPath.confirmwindow._height;
		var viewtween:Object = new Tween(myPath.confirmwindow, "_alpha", Strong.easeOut, myPath.confirmwindow._alpha, 100, 0.5, true);
		myPath.confirmwindow.confirm_txt.text = myStr;


		if( myFunc == null ){ //Alert

			//実行時処理
			myPath.confirmwindow.OKBtn.owner = this;
			myPath.confirmwindow.OKBtn.onRelease = function(){
				myPath.confirmwindow.removeMovieClip();
			}
			
			myPath.confirmwindow.OKBtn._x = 124;
			myPath.confirmwindow.CancelBtn.enabled = false;
			myPath.confirmwindow.CancelBtn._visible = false;

		} else { //Confirm

			//実行時処理
			myPath.confirmwindow.OKBtn.owner = this;
			myPath.confirmwindow.OKBtn.onRelease = function(){
				//this.owner.myFunc( this.owner.myOwnr );
				myPath.confirmwindow.removeMovieClip();
				myFunc( myOwnr );
			}
			
			//キャンセル時処理（共通）
			myPath.confirmwindow.CancelBtn.owner = this;
			myPath.confirmwindow.CancelBtn.onRelease = function(){
				myPath.confirmwindow.removeMovieClip();
			}

		}
		//Selection.setFocus(null)
		Selection.setFocus(myPath.confirmwindow.OKBtn)
		//trace("AAA: " + Selection.getFocus())

	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: ChangeEnabled
		Description		: ＭＣ有効無効切替
		Usage			: 
		Attributes		: targetPath:MovieClip	（ターゲットパス）
		Note			: 共通関数
		History			: 1) Coded by	nf)h.miyahara	2004/09/17
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function ChangeEnabled( targetPath:MovieClip, targetMC:String, flg:Boolean ){
		//対象ＭＣリストを配列へ
		var targetMC_arr = new Array
		targetMC_arr = targetMC.split(",")
		//機能切替＆カバーＭＣ複製
		for(var i=0; i<targetMC_arr.length; i++ ){
			targetPath[ targetMC_arr[i] ].enabled = targetPath[ targetMC_arr[i] ]._visible = flg
		}
	}
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setComboBoxStyles
		Description		: コンボボックスコンポーネントのスタイル設定
		Usage			: setComboBoxStyles();
		Attributes		: none
		Note			: 共通関数
		History			: 1) Coded by	nf)h.miyahara	2004/09/27
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function setComboBoxStyles(){
		styleObj = _global.styles.ComboBox = new mx.styles.CSSStyleDeclaration();
		styleObj.setStyle("themeColor", "haloblue");
		styleObj.fontFamily = "_ゴシック";
		styleObj.borderStyle = "solid";
		styleObj.fontSize = 10;
		//styleObj.fontWeight = "bold";
		styleObj.textDecoration = "none";
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: setTextAreaStyles
		Description		: テキストエリアコンポーネントのスタイル設定
		Usage			: setTextAreaStyles();
		Attributes		: none
		Note			: 共通関数
		History			: 1) Coded by	nf)h.miyahara	2004/09/27
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function setTextAreaStyles(){
		styleObj = _global.styles.TextArea = new mx.styles.CSSStyleDeclaration();
		styleObj.fontFamily = "_ゴシック";
		styleObj.fontSize = 20;
		styleObj.color = 0x666666;
	}


	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: initGridStyle
		Description		: 
		Usage			: initGridStyle(FltEvt:FaultEvent);
		Attributes		: 
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function bridalGridStyle(targetGrid):Void {
		targetGrid.setStyle("styleName", "labelTableau");
		targetGrid.setStyle("alternatingRowColors", Array(0xFFFFFF, 0xECEDE4));
		targetGrid.setRowHeight(22);
		
		targetGrid.setStyle("hGridLines", true);
		//targetGrid.setStyle("hGridLineColor", 0x80945A);
		targetGrid.setStyle("hGridLineColor", 0xFD7B7B);

		targetGrid.setStyle("vGridLines", true);
		//targetGrid.setStyle("vGridLineColor", 0x80945A);
		targetGrid.setStyle("vGridLineColor", 0xFD7B7B);
		
		targetGrid.setStyle("borderStyle", "solid");
		targetGrid.setStyle("buttonColor", 0xFFFFFF);
		targetGrid.setStyle("highlightColor", 0xFFFFFF);
		//targetGrid.setStyle("borderCapColor", 0xD2D7C4);
		targetGrid.setStyle("borderCapColor", 0xFED1D1);
		targetGrid.setStyle("shadowCapColor", 0xFFFFFF);
		targetGrid.setStyle("shadowColor", 0x336699);
		//targetGrid.setStyle("borderColor", 0xD2D7C4);
		targetGrid.setStyle("borderColor", 0xFED1D1);
		//targetGrid.setStyle("themeColor", 0xD2D7C4);
		targetGrid.setStyle("themeColor", 0xFED1D1);
		targetGrid.setStyle("useRollOver", false);
		targetGrid.setStyle("textSelectedColor", 0x333333);
		targetGrid.setStyle("rollOverColor", 0xEEEEE9);
		targetGrid.setStyle("fontSize", 12);
		targetGrid.setVScrollPolicy("auto");

	}

	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Name			: initGridStyle
		Description		: 
		Usage			: initGridStyle(FltEvt:FaultEvent);
		Attributes		: 
		Note			: none
		History			: 1) Coded by	nf)h.miyahara	2004/09/13
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function defaultGridStyle(targetGrid):Void {
		targetGrid.setStyle("styleName", "labelTableau");
		targetGrid.setStyle("alternatingRowColors", Array(0xFFFFFF, 0xECEDE4));
		targetGrid.setRowHeight(22);
		
		targetGrid.setStyle("hGridLines", true);
		targetGrid.setStyle("hGridLineColor", 0x80945A);
		targetGrid.setStyle("vGridLines", true);
		targetGrid.setStyle("vGridLineColor", 0x80945A);
		
		targetGrid.setStyle("borderStyle", "solid");
		targetGrid.setStyle("buttonColor", 0xFFFFFF);
		targetGrid.setStyle("highlightColor", 0xFFFFFF);
		targetGrid.setStyle("borderCapColor", 0xD2D7C4);
		targetGrid.setStyle("shadowCapColor", 0xFFFFFF);
		targetGrid.setStyle("shadowColor", 0x336699);
		targetGrid.setStyle("borderColor", 0xD2D7C4);
		targetGrid.setStyle("themeColor", 0xD2D7C4);
		targetGrid.setStyle("useRollOver", false);
		targetGrid.setStyle("textSelectedColor", 0x333333);
		targetGrid.setStyle("rollOverColor", 0xEEEEE9);
		targetGrid.setStyle("fontSize", 12);
		targetGrid.setVScrollPolicy("auto");

	}		 		 
}