﻿// ActionScript 2.0 class.
class MultiLineCell extends mx.core.UIComponent
{
    private var multiLineLabel; // テキストに使用するラベル。
    private var owner; // このセルを保持する行。
    private var listOwner; // このセルを保持するリスト、データグリッド、またはツリー。

    // 列の高さの合計および指定したセル幅からオフセットされたセルの高さ。
    private static var PREFERRED_HEIGHT_OFFSET = 4; 
    private static var PREFERRED_WIDTH = 100;
    // 最初の深度。
    private var startDepth:Number = 1;

    // コンストラクタ。空にしておくこと。
    public function MultiLineCell()
    {
    }

    /* createChildren には、初期化時に必要なムービークリップアセットをすべてインスタンス化するコードを記述する必要がある。ここでは、1 つのラベルを作成する。*/
    public function createChildren():Void
    {
        // createLabel メソッドは UIObject の有用なメソッドであり、
        // コンポーネントにラベルを容易に作成できる。
        var c = multiLineLabel = this.createLabel("multiLineLabel", startDepth);
        // ラベルのスタイルをグリッドのスタイルに対応させる。
        c.styleName = listOwner;
        c.selectable = true;
        c.tabEnabled = false;
        c.background = false;
        c.border = false;
        c.multiline = true;
        c.wordWrap = true;

		var s=this.createClassObject(mx.controls.UIScrollBar, "my_sb", 20);

		// スクロールバーのターゲットのテキストフィールドを設定する。
		s.setScrollTarget(c);



    }

    public function size():Void
    {
/* UIObject を読み込む UIComponent を継承している場合、setSize を自分で用意する必要はないが、size() は自分で実装する必要がある。ここでは、__width と __height は設定されているものとする。rowHeight 全体に合うようにセルを拡張する。rowHeight は、これからセルをレンダリングしようとしているリストタイプコンポーネントのプロパティである。この cellRenderer クラスを使用してリストタイプコンポーネントを作成するときに rowHeight を 2 行に合わせるには、2 行のテキストをレンダリングできるだけの十分な値を rowHeight プロパティに設定する必要がある。*/
    
/* __width と __height は、.width および .height の getter と setter の基になっている変数。*/
        var c = multiLineLabel;
        c.setSize(__width, __height);
    }

    // セルの高さを設定する。継承されたメソッド。
    public function getPreferredHeight():Number
    {
/* セルには、行を参照するための "owner" というプロパティが用意されている。常にセルが行の高さの大部分を占めるのが望ましい。ここでは、セルを若干小さくする。*/
        return owner.__height - PREFERRED_HEIGHT_OFFSET;
    }

    // セルに値を設定するために所有者によって呼び出される。継承されたメソッド。
    public function setValue(suggestedValue:String, item:Object, selected:Boolean):Void
    {
/* アイテムが未定義の場合、セルには何も描画されないため、ラベルを非表示に設定する。メモ : スクロールするリストタイプコンポーネント (スクロールする DataGrid など) では、スクロールして表示領域外に出たセルはすぐに空にされ、新しい値を設定して再利用されることになっている。スクロールのアニメーション効果はそのようにして実現される。したがって、セルに保持または表示されるデータの値が不変であると仮定することはできない。*/
        if (item==undefined){
            multiLineLabel.text._visible = false;
        }
        multiLineLabel.text = suggestedValue;
    }
    // 関数 getPreferredWidth :: メニューおよび DataGrid ヘッダーについてのみ
    // 関数 getCellIndex :: このセルレンダラーでは使用しない
    // 関数 getDataLabel :: このセルレンダラーでは使用しない
}
