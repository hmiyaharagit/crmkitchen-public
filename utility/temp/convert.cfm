<cfoutput>
<cfif IsDefined("form.regist")>

<cfif IsDefined("form.regist")>
	<!--- エラークエリ --->
	<cfset ErrorQ = QueryNew("ErrNumber,message,detail")>

	<!--- ファイルのアップロード〜読込 --->
	<cfinvoke component="csv_entry" method="csvUpload" returnVariable="loadCheck">
	</cfinvoke>
	
	<!--- ファイルアップロード成功 --->
	<cfif loadCheck.compFlg EQ 1>
		<!--- ファイルの読込 --->
		<cfinvoke component="csv_entry" method="csvRead" returnVariable="readCheck">
			<cfinvokeargument name="fileName" value=#loadCheck.fileName#>
			<cfinvokeargument name="ErrorQ" value=#ErrorQ#>
		</cfinvoke>
		
		<!--- 読込成功 --->
		<cfif readCheck.compFlg EQ 1>
			<!--- データチェック 
			<cfinvoke component="csv_entry" method="csvCheck" returnVariable="readCheck">
				<cfinvokeargument name="fileName" value=#loadCheck.fileName#>
				<cfinvokeargument name="ErrorQ" value=#ErrorQ#>
				<cfinvokeargument name="csvData" value=#readCheck.csvData#>
			</cfinvoke>
			--->
		</cfif>
	<!--- ファイルアップロード失敗 --->
	<cfelse>
		<cfabort>
	</cfif>


	<!--- エラー存在ファイル削除処理 --->
	<cfif ErrorQ.recordcount GT 0>

		<cfif IsDefined("loadCheck.fileName")>
			<cfinvoke component="csv_entry" method="csvDelete" returnVariable="readCheck">
				<cfinvokeargument name="fileName" value=#loadCheck.fileName#>
			</cfinvoke>
		</cfif>

		<cfset onloadjs = "ErrorMsg();">
	
	<cfelse>
	
		<cfdump var=#readCheck.csvData# label="登録内容"><cfabort>

		<!--- 登録処理 --->
		<cfinvoke component="csv_entry" method="csvEntry" returnVariable="regCheck">
			<cfinvokeargument name="csvData" value=#readCheck.csvData#>
		</cfinvoke>

		<!--- ファイル削除処理 --->
		<cfinvoke component="csv_entry" method="csvDelete" returnVariable="readCheck">
			<cfinvokeargument name="fileName" value=#loadCheck.fileName#>
		</cfinvoke>
		
		<cfif regCheck.compFlg EQ 1>
			<cfset onloadjs = "alert(MSG_ENTRY_END);window.parent.close();">
		</cfif>
	</cfif>
</cfif>

<!--- 

	<cfset targetFile = #Application.ServerName# & #form.file_name#>
	<cfif Len(form.file_name) EQ 0>
		テキストファイルを指定して下さい。<br>
		<input type="button" value="戻る" onclick="JavaScript:history.back();">
	<cfelse>
		<cfdump var=#form#>
		<cfabort>
		<cfinvoke
			component		= "Convert"
			method			= "getoldDataFile"
			filepath 		= "#targetFile#"
			returnVariable	= "answers">
		#answers.recordcount#件の回答者データを登録しました。<br>
		<input type="button" value="戻る" onclick="JavaScript:history.back();">
	</cfif>
 --->
<cfelse>

<p>ＣＳ回答データＣＳＶインポート</p>
・ファイル選択より対象ファイルを選択して「コンバート開始」ボタンをクリックして下さい。<br>
<form name="upLoad" method="post" enctype="multipart/form-data">
<table border="0" cellspacing="0" cellpadding="2" class="b1">
<tr>
	<td width="400" height="60" valign="middle" nowrap>
		<input name="file_name" type="file" id="file_name" size="52">
	</td>
	<td>
		<input name="regist" type="Submit" onClick="return fileup_msg()" value="コンバート開始">
	</td>
</tr>
</table>
</form>

<script>
//CSVファイル登録メッセージ
function fileup_msg(){
	tmp = document.forms[0].file_name.value;
	if(tmp.length == 0){
		alert("ファイルを指定して下さい。");
		return false;
	}else{
		flag = confirm(tmp+"を登録します。\nよろしいですか？");
		if(flag == true){
			return true;
			//alert("登録しました。");
			//window.close();
		}else{
			alert("登録をキャンセルしました。");
			return false;
		}
	}
}
</script>

</cfif>
</cfoutput>
	
<!--- <cfdump var=#maydata#> --->
