<cfcomponent output="false">
	<!---
	*************************************************************************************************
	* Class				:base_entry
	* Method			:csvRead
	* Description		:ファイル読込処理
	* Custom Attributes	:fileName
	* Return Paramters	:none
	* History			:1) Coded by	2005/12/01 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="csvRead" output="false">
		<cfargument name="filePath"   type="string" required="true">
		<cfargument name="ErrorQ"   type="Query" required="true">

		<cftry>
			<cfset ErrorQ = arguments.ErrorQ>
			<!--- ファイル存在チェック --->
			<cfif FileExists( arguments.filePath ) EQ false>
				<cfset queryaddrow(ErrorQ,1)>
				<cfset QuerySetCell(ErrorQ,'ErrNumber',#ErrorQ.recordcount#)>
				<cfset QuerySetCell(ErrorQ,'message',#request.ErrorMSG.notexists#)>

				<!--- 戻値設定 --->
				<cfscript>
					executeinformation = StructNew();
					StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
					StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
					StructInsert(executeinformation,	"compFlg",		0								);
					StructInsert(executeinformation,	"ErrorQ",		#ErrorQ#						);
					StructInsert(executeinformation,	"filePath",		#arguments.filePath#			);
				</cfscript>
				<cfreturn executeinformation>
			</cfif>
			
			<!--- ファイル読込 --->
			<cflock name="getfile" timeout="300">
				<cffile action = "read" file = "#arguments.filePath#" variable = "csvData" charset = "shift_jis" >
			</cflock>

			<!--- 行セパレータでテンポラリ配列生成 --->
			<cfset request.Appvars.newline=chr(13) & chr(10)>
			<cfset temp= ArrayNew(1)>
			<cfloop list="#csvData#" index="idx" DELIMITERS="#request.Appvars.newline#">
				<cfset ArrayAppend(temp,idx)>
			</cfloop>
			
			<!--- カラム設定 --->
			<cfset columnslist=#temp[1]#>
			<cfset columnslist = #replacelist(temp[1],"""","")#>
			<cfset columnslist = #replacelist(columnslist,"-","_")#>

			<!--- フォーマットチェック --->
			<cfif ListLen(columnslist) NEQ request.Appvars.columncnt>
				<cfset queryaddrow(ErrorQ,1)>
				<cfset QuerySetCell(ErrorQ,'ErrNumber',#ErrorQ.recordcount#)>
				<cfset QuerySetCell(ErrorQ,'message',#request.ErrorMSG.illegal#)>

				<!--- 戻値設定 --->
				<cfscript>
					executeinformation = StructNew();
					StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
					StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
					StructInsert(executeinformation,	"compFlg",		0								);
					StructInsert(executeinformation,	"ErrorQ",		#ErrorQ#						);
					StructInsert(executeinformation,	"filePath",		#arguments.filePath#			);
				</cfscript>
				<cfreturn executeinformation>
			</cfif>

			<!--- クエリオブジェクト生成 --->
			<cfset qcsvdata = QueryNew("#columnslist#")>
			<cfset er = 0>

			<!--- データセット --->
			<cfloop from=2 to="#ArrayLen(temp)#" index="i">
				<cfset rowslist=#temp[i]#>
				<cfset queryaddrow(qcsvdata,1)>
				<cfloop from=1 to="#ListLen(columnslist)#" index="idx">
		 			<cfset celldata = #ListGetAt(rowslist,idx)#>
		 			<cfset celldata = #replacelist(celldata,"""","")#>
					<cfset QuerySetCell(qcsvdata,'#ListGetAt(columnslist,idx)#',#celldata#)>
				</cfloop>
				<cfset er = i>
			</cfloop>

			<!--- 正常終了時 --->
			<cfscript>
				executeinformation = StructNew();
				StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
				StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
				StructInsert(executeinformation,	"compFlg",		1								);
				StructInsert(executeinformation,	"ErrorQ",		#ErrorQ#						);
				StructInsert(executeinformation,	"csvData",		#qcsvdata#						);
			</cfscript>
			<cfreturn executeinformation>
			
			<cfcatch type="any">

				<cfset queryaddrow(ErrorQ,1)>
				<cfset QuerySetCell(ErrorQ,'ErrNumber',#ErrorQ.recordcount#)>
				<cfset QuerySetCell(ErrorQ,'message',#request.ErrorMSG.notread#&"（ＣＳＶファイル内#er+1#行目のデータ内容をチェックして下さい。）")>
				<cfset QuerySetCell(ErrorQ,'detail',#cfcatch.message#&#request.Appvars.newline#&#cfcatch.detail#)>

				<!--- 戻値設定 --->
				<cfscript>
					executeinformation = StructNew();
					StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
					StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
					StructInsert(executeinformation,	"compFlg",		0								);
					StructInsert(executeinformation,	"ErrorQ",		#ErrorQ#						);
					StructInsert(executeinformation,	"filePath",		#arguments.filePath#			);
				</cfscript>
				<cfreturn executeinformation>
			</cfcatch>
		</cftry>
	</cffunction>	
	
	<!---
	*************************************************************************************************
	* Class				:base_entry
	* Method			:import
	* Description		:インポート実行処理
	* Custom Attributes	:fileName
	* Return Paramters	:none
	* History			:1) Coded by	2005/12/09 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="import" output="false">
		<cfargument name="csvData"   type="Query" required="true">
		<cfargument name="ErrorQ"   type="Query" required="true">

		<cftry>
			<cfset ErrorQ = arguments.ErrorQ>

			<cftransaction action="BEGIN">

			<cfloop query="arguments.csvData">
				<!--- 性別コードをマスタ値変換 --->
				<cfif sex EQ "M">
					<cfset gender_code = 100000000001>
				<cfelseif sex EQ "W">	
					<cfset gender_code = 100000000002>
				<cfelse>
					<cfset gender_code = 100000000000>
				</cfif>
				
				<!--- メールアドレス分割 --->
				<cfif ListLen(e_mail,';') GT 1>
					<cfset e_mail1 = #ListGetAt(e_mail,1,';')#>
					<cfset e_mail2 = #ListGetAt(e_mail,2,';')#>
				<cfelse>
					<cfset e_mail1 = #e_mail#>
					<cfset e_mail2 = ''>
				</cfif>	

				<!--- 顧客情報確認 --->
				<cfquery name="q_rtm_customer" datasource = "#Application.datasource#">
					select
						customernumber
					from
						rtm_customer
					where
						customernumber = '#customernumber#'
				</cfquery>
				<cfif q_rtm_customer.recordcount EQ 0>
					<!--- 顧客情報格納 --->
					<cfquery name="q_ist_rtm_custmoer" datasource = "#Application.datasource#">
				 		insert into rtm_customer(
				 			customernumber,
				 			customercategory,
				 			customerrank,
				 			name,
				 			name_kana,
				 			gender_code,
				 			birth_date,
				 			address_zipcode,
				 			address_segment_1,
				 			address_segment_2,
				 			address_segment_3,
				 			phone_number_1,
				 			phone_number_2,
				 			mail_address_1,
				 			mail_address_2,
				 			post_permission_code,
				 			mail_permission_code,
				 			information,
				 			married_flg,
				 			repeater,
				 			marketingarea
				 		)values(
				 			'#customernumber#',
				 			'#customercategory#',
				 			'#customerrank#',
				 			'#kanjiname#',
				 			'#kananame#',
				 			#gender_code#,
				 			'#birthdate#',
				 			'',
				 			'#address1#',
				 			'#address2#',
				 			'#address3#',
				 			'#homephonenumber#',
				 			'#portablephonenumber#',
				 			'#e_mail1#',
				 			'#e_mail2#',
				 			'',
				 			'',
				 			'#note#',
				 			#married#,
				 			#repeater#,
				 			'#marketingarea#'
				 		)
					</cfquery>
				<cfelse>
					<cfquery name="q_ist_rtm_custmoer" datasource = "#Application.datasource#">
				 		update rtm_customer update
 							customercategory = '#customercategory#',
				 			customerrank = '#customerrank#',
				 			name = '#kanjiname#',
				 			name_kana = '#kananame#',
				 			gender_code = #gender_code#,
				 			birth_date ='#birthdate#',
				 			address_zipcode = '',
				 			address_segment_1 = '#address1#',
				 			address_segment_2 = '#address2#',
				 			address_segment_3 = '#address3#',
				 			phone_number_1 = '#homephonenumber#',,
				 			phone_number_2 = '#portablephonenumber#',
				 			mail_address_1 = '#e_mail1#',
				 			mail_address_2 = '#e_mail2#',
				 			post_permission_code = '',
				 			mail_permission_code = '',
				 			information = '#note#',
				 			married_flg = #married#,
				 			repeater = #repeater#,
				 			marketingarea = '#marketingarea#'
				 		where
				 			customernumber = '#customernumber#'
					</cfquery>
				</cfif>
				
				<!--- 予約情報確認 --->
				<cfquery name="q_rtt_reserve" datasource = "#Application.datasource#">
					select
						reserve_code
					from
						rtt_reserve
					where
						reserve_code = #reservationnumber#
					and
						room_code = #roomnumber#
				</cfquery>
				<cfif q_rtt_reserve.recordcount EQ 0>
					<!--- 予約情報格納 --->
					<cfquery name="q_ist_rtt_reserve" datasource = "#Application.datasource#">
				 		insert into rtt_reserve(
				 			reserve_code,
				 			room_code,
				 			customernumber,
				 			arrive_date,
				 			nights,
				 			depart_date,
				 			information,
				 			lateout_flg,
				 			manpersoncount,
				 			womenpersoncount,
				 			childpersoncount_a,
				 			childpersoncount_b,
				 			childpersoncount_c,
				 			childpersoncount_d,
				 			roomtype,
				 			roomcontrol,
				 			reservationroute,
				 			totalsalesamount,
				 			totalsalesamountdividedbynights,
				 			personunitprice,
				 			roomcharge,
				 			optionamount,
				 			restaurantamount,
				 			referencenumber,
				 			surveymethod
				 		)values(
				 			#reservationnumber#,
				 			#roomnumber#,
				 			'#customernumber#',
				 			'#DateFormat(arrivaldate,'YYYY/MM/DD')#',
				 			#IIF(Len(nights) EQ 0, DE('null'), DE(nights))#,
				 			'#DateFormat(departuredate,'YYYY/MM/DD')#',
				 			'#note#',
				 			#IIF(Len(lateout) EQ 0, DE('null'), DE(lateout))#,
				 			#IIF(Len(manpersoncount) EQ 0, DE('null'), DE(manpersoncount))#,
				 			#IIF(Len(womenpersoncount) EQ 0, DE('null'), DE(womenpersoncount))#,
				 			#IIF(Len(childpersoncount_a) EQ 0, DE('null'), DE(childpersoncount_a))#,
				 			#IIF(Len(childpersoncount_b) EQ 0, DE('null'), DE(childpersoncount_b))#,
				 			#IIF(Len(childpersoncount_c) EQ 0, DE('null'), DE(childpersoncount_c))#,
				 			#IIF(Len(childpersoncount_d) EQ 0, DE('null'), DE(childpersoncount_d))#,
				 			'#roomtype#',
				 			'#roomcontrol#',
				 			'#reservationroute#',
				 			#IIF(Len(totalsalesamount) EQ 0, DE('null'), DE(totalsalesamount))#,
				 			#IIF(Len(totalsalesamountdividedbynights) EQ 0, DE('null'), DE(totalsalesamountdividedbynights))#,
				 			#IIF(Len(personunitprice) EQ 0, DE('null'), DE(personunitprice))#,
				 			#IIF(Len(roomcharge) EQ 0, DE('null'), DE(roomcharge))#,
				 			#IIF(Len(optionamount) EQ 0, DE('null'), DE(optionamount))#,
				 			#IIF(Len(restaurantamount) EQ 0, DE('null'), DE(restaurantamount))#,
				 			'#referencenumber#',
				 			#IIF(Len(surveymethod) EQ 0, DE(0), DE(surveymethod))#
				 		)
					</cfquery>
				</cfif>

				<!--- マーケティング情報取得 --->
				<cfset lst_marketing_id = "4,5,6">
				<cfset lst_marketing = "1,2,5">

				<cfset cnt = 1>
				<cfloop list="#lst_marketing_id#" index="idx">
					<cfset tmp_control_id = #Evaluate("marketing_" & ListGetAt(lst_marketing,cnt))#>
					
					<cfquery name="q_mst_control" datasource = "#Application.datasource#">
						select
							controldiv_id,
							control_id
						from
							mst_control
						where
							controldiv_id = #idx#
						and	
							control_id = '#tmp_control_id#'
					</cfquery>
					<cfif q_mst_control.recordcount NEQ 0>
						<cfquery name="q_rtt_marketing" datasource = "#Application.datasource#">
							select
								reserve_code,
								room_code
							from
								rtt_marketing
							where
								reserve_code = #reservationnumber#
							and
								room_code = #roomnumber#	
							and
								controldiv_id = #q_mst_control.controldiv_id#
<!---
							and
								control_id = '#q_mst_control.control_id#'
--->
						</cfquery>
						<cfif q_rtt_marketing.recordcount EQ 0>
							<!--- マーケティング情報格納 --->
							<cfquery name="q_ist_rtt_marketing" datasource = "#Application.datasource#">
						 		insert into rtt_marketing(
						 			reserve_code,
						 			room_code,
						 			controldiv_id,
						 			control_id
						 		)values(
						 			#reservationnumber#,
						 			#roomnumber#,
						 			#q_mst_control.controldiv_id#,
						 			'#q_mst_control.control_id#'
						 		)
							</cfquery>
							
						</cfif>
					</cfif>
					<cfset cnt = incrementvalue(cnt)>
				</cfloop>

				<!--- 店舗情報取得 --->
				<cfloop from="1" to="20" index="i">
					<cfset tmp_key = #IIF(Len(i) EQ 1, DE("0"&i) ,DE(i))#>
					<cfset tmp_column = #Evaluate("shopresult_" & tmp_key)#>
					<cfif Len(tmp_column) NEQ 0>
						<cfset tmp_shop_id = ListGetAt(tmp_column,1,("-"))>
						<cfset tmp_shop_nm = ListGetAt(tmp_column,2,("-"))>
						<cfset tmp_amount = ListGetAt(tmp_column,3,("-"))>
						<cfif tmp_amount GT 0>
							<cfquery name="q_rtt_shopresult" datasource = "#Application.datasource#">
								select
									reserve_code,
									room_code
								from
									rtt_shopresult
								where
									reserve_code = #reservationnumber#
								and
									room_code = #roomnumber#	
								and
									shop_id = #tmp_shop_id#
							</cfquery>
							<cfif q_rtt_shopresult.recordcount EQ 0>
								<!--- 店舗情報格納 --->
								<cfquery name="q_ist_rtt_shopresult" datasource = "#Application.datasource#">
							 		insert into rtt_shopresult(
										reserve_code,
										room_code,
							 			shop_id,
							 			shop_nm,
							 			amount
							 		)values(
							 			#reservationnumber#,
							 			#roomnumber#,
							 			#tmp_shop_id#,
							 			'#tmp_shop_nm#',
							 			'#tmp_amount#'
							 		)
								</cfquery>
							</cfif>
						</cfif>
					</cfif>
				</cfloop>

			</cfloop>

			</cftransaction>

			<!--- 正常終了時 --->
			<cfscript>
				executeinformation = StructNew();
				StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
				StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
				StructInsert(executeinformation,	"compFlg",		1								);
				StructInsert(executeinformation,	"ErrorQ",		#ErrorQ#						);
			</cfscript>
			<cfreturn executeinformation>
			
			<cfcatch type="any">
				<cftransaction action="rollback"/>
				<cfset QueryAddRow(ErrorQ,1)>
				<cfset QuerySetCell(ErrorQ,'ErrNumber',#ErrorQ.recordcount#)>
				<cfset QuerySetCell(ErrorQ,'message',#request.ErrorMSG.notinsert#)>
				<cfset QuerySetCell(ErrorQ,'detail',#cfcatch.message#&#request.Appvars.newline#&#cfcatch.detail#)>

				<!--- 戻値設定 --->
				<cfscript>
					executeinformation = StructNew();
					StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
					StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
					StructInsert(executeinformation,	"compFlg",		0								);
					StructInsert(executeinformation,	"ErrorQ",		#ErrorQ#						);
				</cfscript>
				<cfreturn executeinformation>
			</cfcatch>
		</cftry>
	</cffunction>	

	<!---
	*************************************************************************************************
	* Class				:base_entry
	* Method			:entryReport
	* Description		:ＣＳＶインポート実行結果報告処理
	* Custom Attributes	:fileName
	* Return Paramters	:none
	* History			:1) Coded by	2005/12/09 h.miyahara(NetFusion)
	*					:2) Updated by	
	*************************************************************************************************
	--->
	<cffunction name="entryReport" output="false">
		<cfargument name="ErrorQ" type="Query" required="true">
		<cfargument name="compflg" type="Numeric" required="true">

		<cftry>
			<cfset ErrorQ = arguments.ErrorQ>
			<!--- メール情報設定 --->
			<cfquery name="q_mst_mailtemplate" datasource = "#Application.datasource#">
				select 
					fromaddress
				from
					mst_mailtemplate
				where
					resort_code = 10
				and
					active_flg = 1
			</cfquery>
			<cfset sendaddress = q_mst_mailtemplate.fromaddress>

<!---*** ForTest ***--->
<cfset sendaddress = "test@nftest.jp">
<!---*** ForTest ***--->

			<cfif ErrorQ.recordcount EQ 0>
				<cfmail to="#request.Appvars.manager_email#" from="#sendaddress#" subject="#request.Appvars.report_nm#" type="text/plain;charset=ISO-2022-JP" server="#Application.SMTPServer#" port="#Application.port#" username="#Application.username#" password ="#Application.password#">
#request.Appvars.manager_nm#
#request.Appvars.filepath##request.Appvars.compmsg#
				</cfmail>

			<cfelse>
				<cfmail to="#request.Appvars.manager_email#" from="#sendaddress#" subject="#request.Appvars.report_nm#" type="text/plain;charset=ISO-2022-JP" server="#Application.SMTPServer#" port="#Application.port#" username="#Application.username#" password ="#Application.password#">
#request.Appvars.manager_nm#
#request.Appvars.filepath##request.Appvars.irglmsg#

-=-=-=-=-=-=-=-=-=-=-=-=-=-=
以下、エラー内容です。
-=-=-=-=-=-=-=-=-=-=-=-=-=-=
<cfloop query="ErrorQ">
[#ErrorQ.message#]

--CFErrorMessage
#ErrorQ.detail#
#request.Appvars.newline##request.Appvars.newline#
</cfloop>
				</cfmail>

			</cfif>
		
			<!--- 正常終了時 --->
			<cfscript>
				executeinformation = StructNew();
				StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
				StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
				StructInsert(executeinformation,	"compFlg",		1								);
				StructInsert(executeinformation,	"ErrorQ",		#ErrorQ#						);
			</cfscript>
			<cfreturn executeinformation>
			
			<cfcatch type="any">

				<!--- 戻値設定 --->
				<cfscript>
					executeinformation = StructNew();
					StructInsert(executeinformation,	"executeDate",	dateformat(now(),'YYYY/MM/DD')	);
					StructInsert(executeinformation,	"executeTime",	timeformat(now(),'hh,mm,ss')	);
					StructInsert(executeinformation,	"compFlg",		0								);
					StructInsert(executeinformation,	"ErrorQ",		#ErrorQ#						);
				</cfscript>
				<cfreturn executeinformation>
			</cfcatch>
		</cftry>
	</cffunction>	

</cfcomponent>
