<!---
*************************************************************************************************
* Description		:基幹データ取込プログラム（初版：星のや）
* History			:1) Coded by	2005/12/01 h.miyahara(NetFusion)
*					:2) Updated by	
*************************************************************************************************
--->
<!--- 初期設定 --->
<cfinclude template="defaultvars.cfm">

<!--- ファイル読込〜ＴＥＭＰクエリ作成処理 --->
<cfinvoke component="base_entry" method="csvRead" returnVariable="readCheck">
	<cfinvokeargument name="filePath" value="#request.Appvars.filedir##request.Appvars.filenm#">
	<cfinvokeargument name="ErrorQ" value=#ErrorQ#>
</cfinvoke>

<!--- ＤＢ登録処理 --->
<cfif ErrorQ.recordcount EQ 0>
	<cfinvoke component="base_entry" method="import" returnVariable="impCheck">
		<cfinvokeargument name="csvData" value="#readCheck.csvData#">
		<cfinvokeargument name="ErrorQ" value=#ErrorQ#>
	</cfinvoke>
</cfif>
<cfif ErrorQ.recordcount EQ 0>
	インポートが完了しました。
<cfelse>
	<cfdump var=#ErrorQ#>
</cfif>
<!--- 
<!--- 正常完了時 --->
<cfif ErrorQ.recordcount EQ 0>
	<cfinvoke component="base_entry" method="entryReport" returnVariable="compflg">
		<cfinvokeargument name="ErrorQ" value=#ErrorQ#>
		<cfinvokeargument name="compflg" value=1>
	</cfinvoke>
<cfelse>
<!--- エラー存在時 --->
	<cfinvoke component="base_entry" method="entryReport" returnVariable="compflg">
		<cfinvokeargument name="ErrorQ" value=#ErrorQ#>
		<cfinvokeargument name="compflg" value=0>
	</cfinvoke>
</cfif>
 --->