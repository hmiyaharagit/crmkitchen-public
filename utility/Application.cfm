<cfapplication name="crmutility" sessionmanagement="Yes" loginStorage="Session">
<cfinclude template="g_vars.cfm">
<cfset retmsg = "">
<cfif IsDefined("form.logout")>
    <cflogout>
    <cfset StructClear(session)>
</cfif>
<cflogin>
  <cfif NOT IsDefined("cflogin")>
	<cfinclude template="login.cfm">
	<cfabort>
  <cfelse>
     <cfif cflogin.password neq "qawsed">
		<cfset retmsg = "ユーザーを特定できませんでした。">
        <cfinclude template="login.cfm">
		<cfabort>
     <cfelse>
           <cfloginuser name="#cflogin.name#" Password = "#cflogin.password#" roles="Admin">
     </cfif>   
  </cfif>
</cflogin>
