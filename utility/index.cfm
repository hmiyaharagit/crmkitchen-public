<cfif isdefined("form.um")>
	<cflocation url="m_loginuser.cfm" addtoken="no">
</cfif>
<cfif GetAuthUser() eq "admin">
	<cfmodule template="header.cfm" title="トップ">
	<p>
	<cfform name="menuform" height="400" width="600" format="Flash" skin="HaloBlue" method="post">
		<cfformgroup type="accordion" height="240" style="marginTop: 0">
			<cfformgroup type="page" label="-- ユーザーマスタメンテナンス">
				<cfformitem type="html">
					CRMKitchenログインユーザー情報の新規登録、編集、削除などのメンテナンスを行います。<br>
					登録できる内容は、ユーザーID・パスワード・ユーザー名称・権限です。<br>
					権限設定できるコードは以下の通りです。
					<li>s ---- [ 管理者 ] 全機能参照可能
					<li>a ---- [ 宿泊 ] 宿泊の顧客・メールのみ利用可能
					<li>b ---- [ 宿泊メール ] 宿泊のメールのみ利用可能
					<li>c ---- [ 新婦 ] 新婦の顧客・メールのみ利用可能
					<li>d ---- [ 新婦メール ] 新婦のメールのみ利用可能
					<li>z ---- [ CSユーザー ] 各CSデータのみ参照可能
				</cfformitem>
				<cfinput type = "submit" name="um" width="100" value = "表示">
			</cfformgroup>
<!--- 
			<cfformgroup type="page" label="ユーザーマスタメンテナンス">
				<cfformitem type="HTML">ログインユーザーのマスタメンテナンスを行います。</cfformitem>
			</cfformgroup>
			<cfformgroup type="page" label="ユーザーマスタメンテナンス">
				<cfformitem type="HTML">ログインユーザーのマスタメンテナンスを行います。</cfformitem>
			</cfformgroup>
 --->
 	 	</cfformgroup>
		<cfformgroup type="horizontal">
			<cfinput type = "submit" name="logout" width="100" value = "ログアウト">
		</cfformgroup>
	</cfform>
	</body>
	</html>
</cfif>
