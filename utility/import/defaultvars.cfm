<!---
*************************************************************************************************
* Description		:基幹データ取込プログラム（初版：星のや）・変数設定用ファイル
* History			:1) Coded by	2005/12/01 h.miyahara(NetFusion)
*					:2) Updated by	
*************************************************************************************************
--->

<!--- 戻値用エラーオブジェクト --->
<cfset ErrorQ = QueryNew("ErrNumber,message,detail")>

<cfscript>
	/*------------------------
		共通変数	
	------------------------*/
	request.Appvars = StructNew();
	request.Appvars.filedir	 		= 'D:\temp\CSSYSTEMIF\';
	//request.Appvars.filedir	 		= 'H:\temp\';
	request.Appvars.filenm	 		= '#DateFormat(NOW(),'YYYYMMDD')#.csv';
	request.Appvars.filepath 		= '#DateFormat(NOW(),'YYYYMMDD')#.csv';
	request.Appvars.compmsg 		= 'のデータインポートが完了しました。';
	request.Appvars.irglmsg 		= 'のデータインポート時にエラーが発生しました。';
	request.Appvars.manager_nm 		= '担当者様';
	request.Appvars.manager_email 	= 'a@nftest.jp';
	request.Appvars.report_nm 		= 'データインポート結果';
	request.Appvars.columncnt 		= 71;
	request.Appvars.newline 		= chr(13) & chr(10);


	/*------------------------
		エラーメッセージ群	
	------------------------*/
	request.ErrorMSG = StructNew();
	request.ErrorMSG.notexists	 	= 'ＣＳＶファイルが見つかりませんでした。#request.Appvars.filedir#内に対象ファイルが存在するか確認して下さい。';
	request.ErrorMSG.notread 		= 'ＣＳＶファイルを開く事ができませんでした。データ内に不正文字（,または改行）が含まれている可能性があります。';
	request.ErrorMSG.illegal		= 'ＣＳＶファイルのフォーマットが正しくありません（列数不正）。正しいフォーマットは#request.Appvars.columncnt#の列で出力されます。ファイル内容を再度ご確認ください。';
	request.ErrorMSG.notinsert 		= 'データベース登録時にエラーが発生しました。';
</cfscript>