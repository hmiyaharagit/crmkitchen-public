<!---
*************************************************************************************************
* Description		:基幹データ取込プログラム（初版：星のや）
* History			:1) Coded by	2005/12/01 h.miyahara(NetFusion)
*					:2) Updated by	
*************************************************************************************************
--->
<cftry>
	<cftransaction action="BEGIN">
		<cfquery name="q_hst_answers" datasource = "#Application.datasource#">
			select
				arrive_date,
				depart_date,
				referencenumber,
				reserve_code
			from hst_answers
			where
				depart_date between '2006/08/01' and '2006/08/31'
			and
				reserve_code is not null	
			order by depart_date
		</cfquery>
		
		<cfoutput>
		<cfloop query="q_hst_answers">
			<cfquery name="q_rtt_reserve" datasource = "#Application.datasource#">
				select
					customernumber,
					arrive_date,
					depart_date,
					referencenumber,
					reserve_code
				from rtt_reserve
				where
					reserve_code = #q_hst_answers.reserve_code#
				and
					referencenumber != '#q_hst_answers.reserve_code#'
				order by customernumber
			</cfquery>
			<cfif q_rtt_reserve.recordcount NEQ 0>
				[#currentrow#]:［顧客番号:#q_rtt_reserve.customernumber#］［予約番号:#q_rtt_reserve.reserve_code#］［照会用番号:#q_rtt_reserve.referencenumber#］<br>
				<!--- --->
				<cfquery name="q_chk_hst_answers" datasource = "#Application.datasource#">
					select referencenumber
					from hst_answers
					where
						referencenumber = '#q_rtt_reserve.referencenumber#'
				</cfquery>
				<cfif q_chk_hst_answers.recordcount EQ 0>
					<cfquery name="q_upd_hst_answers" datasource = "#Application.datasource#">
						update hst_answers
						set
							referencenumber = '#q_rtt_reserve.referencenumber#'
						where
							reserve_code = #q_rtt_reserve.reserve_code#
					</cfquery>
					<cfquery name="q_upd_hst_answersdetails" datasource = "#Application.datasource#">
						update hst_answersdetails
						set
							referencenumber = '#q_rtt_reserve.referencenumber#'
						where
							reserve_code = #q_rtt_reserve.reserve_code#
					</cfquery>
				</cfif>
			</cfif>
		</cfloop>
		</cfoutput>
		<!--- 
		<cfdump var=#q_hst_answers#>
		<cfabort>
		 --->
		</cftransaction>
	<cfcatch>
		エラー発生；
		<cftransaction action="rollback"/>
	</cfcatch>
</cftry>