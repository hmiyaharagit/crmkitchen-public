<cfparam name="errormsg" default="">
<cftry>
<cfif IsDefined("form.gridEntered")>
	<cftransaction action="begin">
	<cfloop index="i" from="1" to="#ArrayLen(form.g_login.rowstatus.action)#">
		<cfif len(form.g_login.login_dv[i]) gt 0 and ListFind('a,b,c,d,z,s,',form.g_login.login_dv[i]) eq 0>
			<cfset errormsg="登録変更処理ができませんでした。登録内容を確認して再度編集をお願いします。">
			<cfbreak>
		</cfif>
		<cfswitch expression="#form.g_login.rowstatus.action[i]#">
			<cfcase value="U">
				<cfquery datasource="#request.app.dsn#" name="u_m_login">
					update m_loginuser set
						 login_id = '#form.g_login.login_id[i]#'
						,login_pw = '#form.g_login.login_pw[i]#'
						,login_nm = '#form.g_login.login_nm[i]#'
						,login_dv = '#form.g_login.login_dv[i]#'
					where login_id = '#form.g_login.original.login_id[i]#'
				</cfquery>
			</cfcase>
			<cfcase value="D">
				<cfquery datasource="#request.app.dsn#" name="u_m_login">
					delete from m_loginuser
					where login_id = '#form.g_login.original.login_id[i]#'
				</cfquery>
			</cfcase>
			<cfcase value="I">
				<cfif len(form.g_login.login_dv[i]) neq 0>
					<cfquery datasource="#request.app.dsn#" name="u_m_login">
						insert into m_loginuser(
							 login_id
							,login_pw
							,login_nm
							,login_dv
						)values(
							 '#form.g_login.login_id[i]#'
							,'#form.g_login.login_pw[i]#'
							,'#form.g_login.login_nm[i]#'
							,'#form.g_login.login_dv[i]#'
						)
					</cfquery>
				<cfelse>
					<cfset errormsg="権限が不正の為登録変更処理ができませんでした。">
				</cfif>
			</cfcase>
		</cfswitch>
	</cfloop>
	</cftransaction>
 </cfif>

<cfquery name="q_loginuser" datasource = "#request.app.dsn#">
select	 login_id
		,login_pw
		,login_nm
		,login_dv
from	 m_loginuser
order by login_dv
		,login_id
</cfquery>
<cfmodule template="header.cfm" title="ユーザーマスタメンテナンス">
<cfif len(errormsg) neq 0>
	<cfoutput><font color="##FF0000">#errormsg#</font></cfoutput>
</cfif>
<cfform name="m_login" action="m_loginuser.cfm" method="post" format="Flash" skin="haloBlue">
	<cfgrid name = "g_login" format="Flash" height = "420" width="600" query="q_loginuser" 
			selectMode = "Edit" font="Verdana" fontSize="12"
			insert = "yes" delete = "yes" gridLines = "yes" rowHeight = "28" colHeaders = "yes"
			maxRows = "120" toolTip = "Tip text">
	</cfgrid>
	<cfformgroup type="horizontal">
		<cfinput type="submit" name="gridEntered" value="スプレッドを反映">
		<cfinput type="submit" name="gridCancele" value="キャンセル">
		<cfinput type="button" name="gohome" value="トップへ戻る" onclick="getURL('index.cfm','_self','post')">
	</cfformgroup>
</cfform>
<cfcatch>
	<cftransaction action="rollback"/>
	<cfoutput>登録変更処理ができませんでした。登録内容を確認して再度編集をお願いします。</cfoutput>
	<cfabort>
</cfcatch>
</cftry>