﻿/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
	Name			: Loadswf
	Description		: 指定ＭＣへの画像読込
	Usage			: var XXXXXX:Loadswf = new Loadswf( imagefile, targetmc );
	Attributes		: imagefile:String					（読込み画像ファイル名）
					: targetmc:MovieClip				（読込みベースMovieClip）
	Method			: instanceName.deleteClip()			（読込みベースMovieClip）
	Note			: none
	History			: 1) Coded by	nf)h.miyahara	2004/07/09
-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
class Loadswf {
	var imgFile:String;
	var tgtMC:MovieClip;
	var mcLoader:MovieClipLoader;
	
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		UnLoad
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function deleteClip(){
		mcLoader.unloadClip(tgtMC)
	}
	/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
		Constructor
	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=*/
	function Loadswf( loadimg:String, targetMC:MovieClip ){
		imgFile = loadimg;
	    tgtMC = targetMC;
		deleteClip()
		var GroundPath = targetMC
		var evtListener = new Object();
		var tmp_mcLoader = new MovieClipLoader();
		mcLoader = tmp_mcLoader;
		mcLoader.loadClip( imgFile, tgtMC );
		mcLoader.addListener( evtListener );
		evtListener.GroundPath = GroundPath
		evtListener.onLoadStart = function (target_mc){
			var LIDepth = 100;
			target_mc._parent.loadingcover_mc._alpha = 100
			target_mc._parent.loadingcover_mc.loadingmc = true
		}
		
		evtListener.onLoadProgress = function (target_mc, loadedBytes, totalBytes){
			//trace("総数" + totalBytes + "を"+ target_mc + "に" + loadedBytes + "ロード中･･･")
		}
		
		evtListener.onLoadComplete = function (target_mc){
			target_mc._parent._x = 0
			trace("ロード完了")
		}
		
		evtListener.onLoadInit = function (target_mc)
		{
			mcLoader.unloadClip(target_mc);
			//trace("ロード初期化");
		} 

		evtListener.onLoadError = function (target_mc, errorCode) 
		{
			//trace("ロードが不完全で完了しました。")
			//trace ("ERROR CODE = " + errorCode);
			//trace ("Your load failed on movie clip = " + target_mc + "n");
		} 
	}


}
